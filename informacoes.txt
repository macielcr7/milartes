Instalado o PHP
  php-5.3.9-nts-Win32-VC9-x86.msi
  IIS FastCGI
  Extens�es Habilitadas
   Curl
   EXIF
   GD2
   IMAP
   MySQL
   MySQLi
   OpenSSL
   PDO
    MySQL
    ODBC
    SQLite
   PostgreSQL
   SQLite3

* No PHP
	Verificar o magic_quotes_gpc

* Para Query String muito grande
	executar CMD como administrador
	You can use %windir%\system32\inetsrv\appcmd to change it.
	
	"appcmd list config /section:requestFiltering -text:requestLimits.maxQueryString" list the current value at server level.
	
	"appcmd set config /section:requestFiltering /requestLimits.maxQueryString:4096" changes the value to 4096.


* MySql Tabela: orcamento
  Campo: Tipo de Or�amento: 0=geral/tradicional, 1=forro, 2=persianas, 3=pisos, 4=rodap�s


para anderson fazer:
verificar nas tabelas de `produtos_orcamentos_forro` / `produtos_orcamentos_piso` os campos NULL
nas observacoes dos ambientes, orcamentos, itens....
mb_strtoupper($item['observacoes'], 'UTF-8')
e remover espacos em branco

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

ALTER TABLE `formas_pagto`
	CHANGE COLUMN `categoria` `categoria` TINYINT(2) UNSIGNED NULL DEFAULT NULL COMMENT 'Categoria' AFTER `tipo`;

UPDATE `formas_pagto` SET `cod_convenio`='0' WHERE  `id`=1;

ALTER TABLE `vendas_pagamentos`
	ADD COLUMN `nro_parcelas_cartao` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT 'Nro de Parcelas Cart�o' AFTER `valor_pago`;

ALTER TABLE `caixa_itens`
	ADD COLUMN `nro_parcelas_cartao` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT 'Nro de Parcelas Cart�o' AFTER `valor`;

