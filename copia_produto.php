<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
	@session_start();
	header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Adicionar Novo Produto como Cópia</title>
	<link rel="stylesheet" type="text/css" href="resources/css/sandri.css"/>
</head>
<body bgcolor="#FFFFFF">

<?php
if($_POST){
	date_default_timezone_set('America/Sao_Paulo');
	define("DB_NAME", "gestao");
	define("DB_NAME2", "milartes");
	define("DB_USER", "root");
	define("DB_PASS", "root");
	define("DB_HOST", "127.0.0.1");

	$db = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ("Erro de Conexão");
	define("BANCO_DADOS", $db);
	mysql_select_db(DB_NAME2);
	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET character_set_connection=utf8');
	mysql_query('SET character_set_client=utf8');
	mysql_query('SET character_set_results=utf8');
	mysql_query("SET lc_time_names = 'pt_BR'");


	$user_id = 2;
	$id_produto = $_POST['id_produto'];


//produto
	$sql = "SELECT descricao_completa,
					descricao_abreviada,
					categoria,
					fornecedor,
					setor_estoque,
					fabricante_marca,
					unidade_medida,
					ncm,
					peso_bruto,
					peso_liquido,
					prazo_garantia,
					desconto_maximo,
					tipo_produto,
					comissionar_vendedor,
					comissionar_tipo,
					valor_comissao,
					controlar_estoque,
					quantidade_minima,
					quantidade_disponivel,
					possui_composicao,
					ativo,
					preco_custo,
					preco_custo_servico 
				FROM produtos produtos WHERE id = '{$id_produto}'";

	$resp = mysql_query($sql,BANCO_DADOS);
	echo mysql_error(BANCO_DADOS);

	if (mysql_num_rows($resp) > 0) {
		$valor = mysql_fetch_array($resp);
		$descricao_completa = $valor["descricao_completa"]." - Cópia";
		$descricao_abreviada = $valor["descricao_abreviada"];


		$sql0 = "INSERT INTO produtos 
					(
						descricao_completa,
						descricao_abreviada,
						categoria,
						fornecedor,
						setor_estoque,
						fabricante_marca,
						unidade_medida,
						ncm,
						peso_bruto,
						peso_liquido,
						prazo_garantia,
						desconto_maximo,
						tipo_produto,
						comissionar_vendedor,
						comissionar_tipo,
						valor_comissao,
						controlar_estoque,
						quantidade_minima,
						quantidade_disponivel,
						possui_composicao,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						ativo,
						preco_custo,
						preco_custo_servico
					) 
				VALUES 
					(
						'$descricao_completa',
						'$descricao_abreviada',
						'{$valor['categoria']}',
						'{$valor['fornecedor']}',
						'{$valor['setor_estoque']}',
						'{$valor['fabricante_marca']}',
						'{$valor['unidade_medida']}',
						'{$valor['ncm']}',
						'{$valor['peso_bruto']}',
						'{$valor['peso_liquido']}',
						'{$valor['prazo_garantia']}',
						'{$valor['desconto_maximo']}',
						'{$valor['tipo_produto']}',
						'{$valor['comissionar_vendedor']}',
						'{$valor['comissionar_tipo']}',
						'{$valor['valor_comissao']}',
						'{$valor['controlar_estoque']}',
						'{$valor['quantidade_minima']}',
						'{$valor['quantidade_disponivel']}',
						'{$valor['possui_composicao']}',
						NOW(),
						$user_id,
						$user_id,
						'{$valor['ativo']}',
						'{$valor['preco_custo']}',
						'{$valor['preco_custo_servico']}'
					)
		";
		$resp0 = mysql_query($sql0,BANCO_DADOS);
		echo mysql_error(BANCO_DADOS);


		$sqlXX = "SELECT MAX(id) AS maximo FROM produtos";
		$respXX = mysql_query($sqlXX,BANCO_DADOS);
		$novo_id = mysql_result($respXX,0,"maximo");







		//atributos do produto
		$sql1 = "SELECT id_atributo_categoria, valor FROM atributos_produto WHERE id_produto = '{$id_produto}' ORDER BY id_atributos_produto";
		$resp1 = mysql_query($sql1,BANCO_DADOS);
		echo mysql_error(BANCO_DADOS);


		if (mysql_num_rows($resp1) > 0) {
			while ($valor1 = mysql_fetch_array($resp1)) {
				$sql2 = "INSERT INTO atributos_produto 
								(
									id_atributo_categoria,
									id_produto,
									valor, 
									data_cadastro, 
									cadastrado_por, 
									alterado_por, 
									data_alteracao
								) 
							VALUES 
								(
									'{$valor1['id_atributo_categoria']}',
									'$novo_id',
									'{$valor1['valor']}',
									NOW(),
									'$user_id',
									'$user_id',
									NOW()
								)
				";
				$resp2 = mysql_query($sql2,BANCO_DADOS);
				echo mysql_error(BANCO_DADOS);
			}
		}






		//composiçõe do produto
		$sql3 = "SELECT id_composicao, quantidade FROM produto_composicoes WHERE id_produto = '{$id_produto}'";
		$resp3 = mysql_query($sql3,BANCO_DADOS);
		echo mysql_error(BANCO_DADOS);

		if (mysql_num_rows($resp3) > 0) {
			while ($valor3 = mysql_fetch_array($resp3)) {
				$sql4 = "INSERT INTO produto_composicoes 
								(
									id_produto, 
									id_composicao, 
									quantidade, 
									data_cadastro, 
									cadastrado_por, 
									alterado_por, 
									data_alteracao
								) 
							VALUES 
								(
									'$novo_id', 
									'{$valor3['id_composicao']}', 
									'{$valor3['quantidade']}', 
									NOW(),
									'$user_id',
									'$user_id',
									NOW()
								)
				";
				$resp4 = mysql_query($sql4,BANCO_DADOS);
				echo mysql_error(BANCO_DADOS);
			}
		}




		//preços do produto
		$sql5 = "SELECT id_categoria_precos, valor FROM produto_precos WHERE id_produto = '{$id_produto}'";
		$resp5 = mysql_query($sql5,BANCO_DADOS);
		echo mysql_error(BANCO_DADOS);

		if (mysql_num_rows($resp5) > 0) {
			while ($valor5 = mysql_fetch_array($resp5)) {
				$sql6 = "INSERT INTO produto_precos 
									(
										id_produto, 
										id_categoria_precos, 
										valor, 
										data_cadastro, 
										cadastrado_por, 
										alterado_por, 
										data_alteracao
									) 
								VALUES 
									(
										'$novo_id', 
										'{$valor5['id_categoria_precos']}', 
										'{$valor5['valor']}', 
										NOW(),
										'$user_id',
										'$user_id',
										NOW()
									)
				";
				$resp6 = mysql_query($sql6,BANCO_DADOS);
			echo mysql_error(BANCO_DADOS);
			}
		}





		//cores do produto
		$sql7 = "SELECT id_cor FROM produtos_cores WHERE id_produto = '{$id_produto}'";
		$resp7 = mysql_query($sql7,BANCO_DADOS);
		echo mysql_error(BANCO_DADOS);


		if (mysql_num_rows($resp7) > 0) {
			while ($valor7 = mysql_fetch_array($resp7)) {
				$sql8 = "INSERT INTO produtos_cores
									(
										id_produto, 
										id_cor, 
										data_cadastro, 
										cadastrado_por, 
										alterado_por, 
										data_alteracao
									) 
								VALUES 
									(
										'$novo_id', 
										'{$valor7['id_cor']}', 
										NOW(), 
										'$user_id', 
										'$user_id', 
										NOW()
									)
				";
				$resp8 = mysql_query($sql8,BANCO_DADOS);
				echo mysql_error(BANCO_DADOS);
			}
		}






		//imagens do produto
		$sql9 = "SELECT src FROM produto_imagens WHERE id_produto = '{$id_produto}'";
		$resp9 = mysql_query($sql9,BANCO_DADOS);
		echo mysql_error(BANCO_DADOS);

		if (mysql_num_rows($resp9) > 0) {
			while ($valor9 = mysql_fetch_array($resp9)) {
				$sql10 = "INSERT INTO produto_imagens
									(
										id_produto, 
										src
									) 
								VALUES 
									(
										'$novo_id', 
										'{$valor9['$prod_img->src']}'
									)
				";
				$resp10 = mysql_query($sql10,BANCO_DADOS);
				echo mysql_error(BANCO_DADOS);
			}
		}
	}
	else {
		echo "ID de Produto: ".$id_produto." não encontrado";
	}

echo "Feito....";
}
else {
?>
<form action="copia_produto.php" method="POST">
	<fieldset>
		<legend>Adicionar Novo Produto como Cópia:</legend>
		ID do Produto à Copiar:<br>
		<input type="text" name="id_produto" value="">
		<br><br>
		<input type="submit" value="Enviar">
	</fieldset>
</form>

<?php
}
?>
</body>
</html>