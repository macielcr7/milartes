<?php
/**
 * Smart PHP Calendar
 *
 * @category   Spc
 * @package    Calendar
 * @copyright  Copyright (c) 2008-2011 Yasin Dagli (http://www.smartphpcalendar.com)
 * @license    http://www.smartphpcalendar.com/license
 */

require 'SpcEngine.php';

$reminderModel = new Calendar_Model_Reminder;
$reminderModel->checkEmailReminder();