<?php

	error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors', 'on');

	if (isset($_POST["submit"]) || defined('WP_PLUGIN')) {
	    //db connection parameters
		include_once '../SpcEngine.php';
        Spc::initLanguage('en');

        $db = new SpcDb;

        $username = mysql_real_escape_string($_POST['username']);
        $password = mysql_real_escape_string($_POST['password']);
        $email = mysql_real_escape_string($_POST['email']);

        $spcLogin = new Core_Controller_Login;
        $hashPassword = $spcLogin->hashPassword($username, $password);

	    $timezone = SPC_DEFAULT_TIMEZONE;

		$queries = array();

		$queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_admin_user_cals` (
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0',
  `cal_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`admin_id`,`cal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_calendars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('user','group','url') DEFAULT 'user',
  `user_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `color` varchar(7) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `status` enum('on','off') DEFAULT 'on',
  `show_in_list` enum('0','1') DEFAULT NULL,
  `public` tinyint(3) unsigned DEFAULT '0',
  `reminder_message_email` text,
  `reminder_message_popup` text,
  `access_key` varchar(32) DEFAULT NULL COMMENT 'ical subscribe access key',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=233 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_default_reminders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `cal_id` int(11) unsigned DEFAULT NULL,
  `type` enum('email','popup') DEFAULT NULL,
  `time` smallint(6) DEFAULT NULL,
  `time_type` enum('minute','hour','day','week') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `cal_id` (`cal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cal_id` int(10) unsigned DEFAULT NULL,
  `type` enum('standard','multi_day') DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` char(5) DEFAULT NULL,
  `start_timestamp` int(10) unsigned DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` char(5) DEFAULT NULL,
  `end_timestamp` int(10) unsigned DEFAULT NULL,
  `repeat_type` enum('none','daily','weekly','monthly','yearly') DEFAULT NULL,
  `repeat_interval` tinyint(3) unsigned DEFAULT NULL,
  `repeat_count` tinyint(3) unsigned DEFAULT '0',
  `repeat_end_date` date DEFAULT '0000-01-01',
  `repeat_data` varchar(30) DEFAULT NULL,
  `repeat_deleted_indexes` varchar(255) DEFAULT NULL,
  `title` text,
  `description` longblob,
  `color` varchar(15) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `available` enum('0','1') DEFAULT '1',
  `public` enum('0','1') DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `invitation` enum('1','0') DEFAULT '0',
  `invitation_event_id` int(10) unsigned DEFAULT '0',
  `invitation_creator_id` int(10) unsigned DEFAULT '0',
  `invitation_response` enum('yes','no','maybe','pending') DEFAULT 'pending',
  `created_by` varchar(30) DEFAULT NULL,
  `modified_by` varchar(30) DEFAULT NULL,
  `created_on` varchar(19) DEFAULT NULL,
  `updated_on` varchar(19) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cal_id` (`cal_id`,`type`,`start_date`),
  KEY `cal_id_2` (`cal_id`,`type`,`end_date`),
  KEY `cal_id_3` (`cal_id`,`type`,`start_date`,`end_date`),
  KEY `cal_id_4` (`cal_id`,`start_date`),
  KEY `cal_id_5` (`cal_id`,`end_date`),
  KEY `cal_id_6` (`cal_id`,`start_date`,`end_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=180712 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_guests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `response` enum('yes','no','maybe','pending') DEFAULT 'pending',
  `note` varchar(255) DEFAULT NULL,
  `user_guest_count` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned DEFAULT NULL,
  `is_repeating_event` enum('0','1') DEFAULT '0',
  `type` enum('email','popup') DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `time_unit` enum('minute','hour','day','week') DEFAULT NULL,
  `ts` timestamp NULL DEFAULT NULL,
  `remind_time` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_event_id` (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `admin_id` int(10) unsigned DEFAULT NULL,
  `shortdate_format` varchar(20) DEFAULT NULL,
  `longdate_format` varchar(20) DEFAULT NULL,
  `timeformat` enum('core','standard') DEFAULT NULL,
  `custom_view` tinyint(3) unsigned DEFAULT NULL,
  `start_day` enum('Saturday','Sunday','Monday') DEFAULT NULL,
  `default_view` varchar(20) DEFAULT NULL,
  `wysiwyg` enum('1','0') DEFAULT '0',
  `staff_mode` enum('0','1') DEFAULT '0',
  `calendar_mode` enum('vertical','timeline') DEFAULT 'vertical',
  `timeline_day_width` mediumint(8) unsigned DEFAULT '360',
  `timeline_row_height` mediumint(8) unsigned DEFAULT '28',
  `timeline_show_hours` tinyint(3) unsigned DEFAULT '1',
  `timeline_mode` enum('horizontal','vertical') DEFAULT 'horizontal',
  `week_cal_timeslot_min` mediumint(8) unsigned DEFAULT '30',
  `timeslot_height` tinyint(3) unsigned DEFAULT '20',
  `week_cal_start_time` char(5) DEFAULT '00:00',
  `week_cal_end_time` char(5) DEFAULT '23:00',
  `week_cal_show_hours` tinyint(3) unsigned DEFAULT '1',
  `event_tooltip` tinyint(3) unsigned DEFAULT '1',
  `left_side_visible` tinyint(3) unsigned DEFAULT '1',
  `overlap_management` enum('simple','advanced') DEFAULT 'simple',
  `overlap_prevented_cals` varchar(255) DEFAULT NULL,
  `check_overlap_prevented_cals_method` enum('independent','merge') DEFAULT 'independent',
  `prevent_overlap_cal_groups` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_shared_calendars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('user','group','url') DEFAULT 'user',
  `user_id` int(11) unsigned DEFAULT NULL,
  `cal_id` int(11) unsigned DEFAULT NULL,
  `shared_user_id` int(11) DEFAULT NULL,
  `permission` enum('see','change') DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `color` varchar(7) DEFAULT NULL,
  `status` enum('on','off') DEFAULT 'on',
  `show_in_list` enum('0','1') DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cal_id` (`cal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_calendar_user_share_free_busy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `shared_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `shared_user_id` (`shared_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_event_calendar_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_mobile_calendar_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `theme` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_key` varchar(32) DEFAULT NULL,
  `activated` tinyint(3) unsigned DEFAULT '1',
  `admin_id` int(10) unsigned DEFAULT NULL,
  `role` enum('super','admin','user') DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `timezone` varchar(30) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `theme` varchar(20) DEFAULT NULL,
  `kbd_shortcuts` tinyint(3) unsigned DEFAULT '1',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `i_username` (`username`),
  KEY `fk_admin_id` (`admin_id`),
  KEY `access_key` (`access_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

        $queries[] = "CREATE TABLE IF NOT EXISTS `spc_user_permissions` (
  `user_id` int(10) unsigned NOT NULL,
  `permission` varchar(100) NOT NULL,
  `value` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`user_id`,`permission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $queries[] = "ALTER TABLE `spc_calendar_admin_user_cals`
  ADD CONSTRAINT `spc_calendar_admin_user_cals_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_calendars`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_default_reminders`
  ADD CONSTRAINT `spc_calendar_default_reminders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spc_calendar_default_reminders_ibfk_2` FOREIGN KEY (`cal_id`) REFERENCES `spc_calendar_calendars` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_events`
  ADD CONSTRAINT `spc_calendar_events_ibfk_1` FOREIGN KEY (`cal_id`) REFERENCES `spc_calendar_calendars` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_guests`
  ADD CONSTRAINT `spc_calendar_guests_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `spc_calendar_events` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_reminders`
  ADD CONSTRAINT `fk_event_id` FOREIGN KEY (`event_id`) REFERENCES `spc_calendar_events` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spc_calendar_reminders_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `spc_calendar_events` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_settings`
  ADD CONSTRAINT `spc_calendar_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spc_calendar_settings_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `spc_users` (`id`);";

        $queries[] = "ALTER TABLE `spc_calendar_shared_calendars`
  ADD CONSTRAINT `fk_cal_id` FOREIGN KEY (`cal_id`) REFERENCES `spc_calendar_calendars` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_calendar_user_share_free_busy`
  ADD CONSTRAINT `spc_calendar_user_share_free_busy_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spc_calendar_user_share_free_busy_ibfk_2` FOREIGN KEY (`shared_user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_event_calendar_settings`
  ADD CONSTRAINT `spc_event_calendar_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_mobile_calendar_settings`
  ADD CONSTRAINT `spc_mobile_calendar_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_users`
  ADD CONSTRAINT `fk_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        $queries[] = "ALTER TABLE `spc_user_permissions`
  ADD CONSTRAINT `spc_user_permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spc_users` (`id`) ON DELETE CASCADE;";

        try {
            $db->begin();

            //write tables
            foreach ($queries as $query) {
                $db->query($query);
            }

            //create user
            $userModel = new Core_Model_User();
            $user['activated'] = 1;
            $user['role'] = 'super';
            $user['admin_id'] = 1;
            $user['username'] = $username;
            $user['password'] = $password;
            $userModel->createUser($user);

            //create directories
            $imageDir = SPC_SYSPATH . "/apps/calendar/files/user-images";

            //create user image directory
            $userImageDir = "{$imageDir}/{$username}";
            if (!file_exists($userImageDir)) {
                mkdir($userImageDir);
            }

            $userOrgImageDir = $userImageDir . '/org';
            if (!file_exists($userOrgImageDir)) {
                mkdir($userOrgImageDir);
            }

            $userThumbImageDir = $userImageDir . '/thumb';
            if (!file_exists($userThumbImageDir)) {
                mkdir($userThumbImageDir);
            }

            //mobile calendar settings
            $db->query("INSERT INTO spc_mobile_calendar_settings SET user_id = 1, theme = 'c'");

            $db->commit();
        } catch (Excaption $e) {
            echo 'Installation failed! <br />' . $db->getErrorMsg();
        }

        echo "Your Account Created Successfuly. You can use Smart PHP Calendar now. <a href='../login.php'>login</a>";
        exit;
	}
?>

<!doctype html>
<html>
<head>
<title>SmartPHPCalendar - Install</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="../js/jquery.js"></script>

<style>
	/*** Smart PHP Calendar Install ***/
    * {
        margin: 0;
        padding: 0;
    }

    body {
        text-align: center;
    }

    img {
        border: 0;
    }

    #container {
        left: 0px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        color:#4D4D4D;
		font-size:13px;
		background-image: url('../img/install-content.png');
        width: 359px;
		margin: 200px auto 0;
    }

    input {
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
        border: 1px solid #ccc;
        width: 170px;
        padding: 6px 4px;
        font-size: 12px;
        font-weight: bold;
        color: #1D2A5B;
    }

    #statusMsg {
        text-align: center;
        font-size: 10px;
    }

    .error {
       	-moz-border-radius: 3px;
		-webkit-border-radius: 3px;
		border-radius: 3px;
		background-color:#FEF5C9;
		border:1px solid #E8D15C;
		padding: 6px 0;
    }

    #login-table {
		width: 330px;
		margin: 0 auto;
    }

    #login-table td {
        padding: 5px;
    }

	#pass{
		position: absolute;
		bottom: 5px;
		right: 5px;
		font-size: 11px;
	}

</style>

<script type="text/javascript">
	$(function() {
		$('#submit').bind("click", function(){
			var username = $('#username').val(),
				password = $('#password').val(),
				rePassword = $("#re-password").val(),
				email = $("#email").val();

			if (username == "" || password == "") {
				$('#errorMsg').text("Username and Password Required").slideDown('slow');
				return false;
			} else if (password !== rePassword) {
				$('#errorMsg').text("Passwords don't match").slideDown('slow');
				return false;
			} else {
				return true;
			}
			return false;
		});
	});
</script>
</head>

<body>
	<div id="container">
		<form id="install-form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
			<table id="login-table">
				<thead>
					<tr>
						<th colspan="2" style="text-align: left; padding: 10px 0; font-family: corbel, arial, sans-serif; font-size: 15px;">Smart PHP Calendar</th>
					</tr>
				</thead>
				<tfoot>
					<tr><td colspan="2">&nbsp;</td></tr>
				</tfoot>
				<tbody>
					<tr> <td colspan="2"><div id="errorMsg" class="error" style="width: 100%; display: none; text-align: center; font-size: 11px;"></div> </td> </tr>
					<tr> <td style="text-align: left;">Username: </td> <td> <input name="username" id="username" type="text" /> </td> </tr>
					<tr> <td style="text-align: left;">Email: </td> <td> <input name="email" id="email" type="text" /> </td> </tr>
					<tr> <td style="text-align: left;">Password: </td> <td> <input name="password" id="password" type="password" /> </td> </tr>
					<tr> <td style="text-align: left;">Re-type password: </td> <td> <input name="re-password" id="re-password" type="password" /> </td> </tr>
					<tr> <td></td> <td><input style="background-color: #f5f5f5; cursor: pointer; font-family: corbel, arial, sans-serif; font-size: 14px;" name="submit" id="submit" type="submit" value="Install" /></td></tr>
				</tbody>
			</table>
		</form>
	</div>
	<div id="foot" style="background-image: url('../img/install-foot.png'); width: 359px; height: 8px; margin: 0 auto;"></div>
</body>
</html>