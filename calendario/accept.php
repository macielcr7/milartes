<?php

require_once 'SpcEngine.php';

//activate account
if (isset($_GET['aa'])) {
    $accessKey = $_GET['aa'];
    if (Spc::activateAccount($accessKey)) {
        header('Location: ' . SPC_ROOT);
    }
    exit;
}

$id = (int)$_GET['id'];
$udigest = $_GET['digest'];

$eventModel = new Calendar_Model_Event;
$select = $eventModel->select(null, 'spc_calendar_guests')->where("id = $id");
$invitation = $eventModel->fetchRow($select);
$eventId = $invitation['event_id'];
$guestEmail = $invitation['email'];

$response = isset($_GET['response']) ? $_GET['response'] : $invitation['response'];

$invitationDigest = md5("spc-invitation-digest: {$guestEmail}");

if ($udigest !== $invitationDigest) {
    exit('Invalid digest');
}

//get invitation owner calendar settings
$coreUserModel = new Core_Model_User();
$select = $eventModel->select()
                    ->from('spc_users')
                    ->join('spc_calendar_calendars', 'spc_calendar_calendars.user_id = spc_users.id')
                    ->join('spc_calendar_events', 'spc_calendar_events.cal_id = spc_calendar_calendars.id')
                    ->where("spc_calendar_events.id = {$eventId}");

$invitationOwnerUserId = $eventModel->fetchColumn($select);
$invitationOwnerUserPrefs = $coreUserModel->getUserPrefs($invitationOwnerUserId);
$_SESSION['spcUserPrefs'] = $invitationOwnerUserPrefs;
Spc::initLanguage();
?>

<!doctype html>
<html lang="en">
<head>
	<title>Smart PHP Calendar</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />

	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

	<link rel="stylesheet" type="text/css" href="css/smartphpcalendar.css?v=1.8" />
    <link rel="stylesheet" type="text/css" href="css/theme/<?php echo Spc::getUserPref('theme'); ?>/jquery-ui.css" />

	<script src="js/jquery.js"></script>
    <script src="js/handlebars.js"></script>
    
    <script><?php echo Spc::getAppJSProps(); ?></script>
	<script>
        SPC.eventId = <?php echo $eventId; ?>;
        SPC.inviteeId = <?php echo $id; ?>;
        SPC.inviteeResponse = "<?php echo $response; ?>";
	</script>
    
    <script src="js/jquery-ui.js"></script>
    <script src="js/ui-i18n/jquery.ui.datepicker-<?php echo Spc::getUserPref('language'); ?>.js"></script>

    <script src="js/spc.core.js?v=<?php echo time(); ?>"></script>
    <script src="js/spc.utils.js?v=<?php echo time(); ?>"></script>

    <script src="js/invitation.js"></script>
</head>
    <body>
        <div id="spc-logo-wrapper">
            <h1 id="spc-logo">Smart PHP Calendar</h1>
            <h2 id="sub-logo">Ajax Based PHP Calendar</h2>
        </div>
        <div id="public-invitation-content"></div>

        <?php Spc::requireFile('files/templates.php', 'calendar'); ?>
    </body>
</html>