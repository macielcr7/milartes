<?php

class Calendar_Model_Schedule extends SpcDbTable {
    protected $_name = 'calendar_user_share_free_busy';

    /**
     * Gets users' schedules
     *
     * @param int $adminId
     * @param string $date
     * @param bool $getAllSchedules
     * @return array
     */
	public function getUserSchedules($adminId, $date, $getAllSchedules = false) {
		$schedule = array();
        $calendarModel = new Calendar_Model_Calendar;
        $userModel = new Core_Model_User;
        $eventModel = new Calendar_Model_Event;

		//group-users
		$users = $userModel->getAdminUsers($adminId, true, $getAllSchedules);

		//other-groups-users | shared free/busy information
		$users = $users + $this->getSharedFreeBusyUsers($this->_userId);

		foreach ($users as $userId => $user) {
			$cals = $calendarModel->getUserCals($userId, false, false, true); //get only private cals not shared
			$schedule[$user['username']] = $eventModel->getEvents('week', $cals, $date, $date);
			$schedule[$user['username']]['role'] = $user['role'];
		}

		return $schedule;
	}

    /**
     * Gets users that share their free/busy information with you
     *
     * @param int $userId
     * @return array
     */
    public function getSharedFreeBusyUsers($userId) {
        $select = $this->select(array('user_id AS id'))
                        ->join('spc_users',
                                'spc_calendar_user_share_free_busy.user_id = spc_users.id',
                                array('username', 'role'))
                        ->where("spc_calendar_user_share_free_busy.shared_user_id = $userId");

		$users = array();
		foreach ($this->fetchAll($select) as $user) {
			$users[$user['id']] = $user;
		}

		return $users;
	}

    /**
     * Gets users that you share your free/busy information with them
     *
     * @param int $userId
     * @return array
     */
    public function getFreeBusySharedUsers($userId) {
        $select = $this->select(array('shared_user_id'))
                        ->join('spc_users',
                                'spc_calendar_user_share_free_busy.shared_user_id = spc_users.id',
                                array('username'))
                        ->where("spc_calendar_user_share_free_busy.user_id = $userId");

		$users = array();
		foreach ($this->fetchAll($select) as $user) {
			$users[$user['shared_user_id']] = $user['username'];
		}

		return $users;
	}

    /**
     * Share your free/busy information with other group's users
     *
     * @param string $username
     */
    public function shareFreeBusy($username) {
		$userRole = SPC_USER_ROLE;
		$adminId = SPC_ADMINID;
		if ($userRole == 'admin') {
			$adminId = SPC_USERID;
		}

        //check if there is a user account wanted to be shared
        $select = $this->select(array('id', 'admin_id'), 'spc_users')
                        ->where("username = '$username'");

        $sharedUser = $this->fetchRow($select, 'row');

        if (!$sharedUser) {
            throw new Exception('User account could not be found!');
        }

		list ($sharedUserId, $sharedUserAdminId) = $sharedUser;

		if ($sharedUserAdminId == $adminId) {
			throw new Exception('You cannot share your free/busy information with users in the same group with you!');
		}

        //check already shared
        $select = $this->select(array('shared_user_id'))
                        ->where("shared_user_id = $sharedUserId");

        if ($this->numRows($select)) {
            throw new Exception('You have already shared your free/busy information with this user!');
        }

        $this->insert(array(
            'user_id' => $this->_userId,
            'shared_user_id' => $sharedUserId
        ));
	}
}