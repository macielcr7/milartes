<?php
/**
 * Smart PHP Calendar
 *
 * @category   Spc
 * @package    Calendar
 * @copyright  Copyright (c) 2008-2011 Yasin Dagli (http://www.smartphpcalendar.com)
 * @license    http://www.smartphpcalendar.com/license
 */

/**
 * Calendar Rss Controller
 */
class Calendar_Controller_Rss extends SpcController {

    /**
     * Spc Rss model
     *
     * @var object
     */
    public $rss;

    public function __construct($options) {
        parent::__construct();

        $this->rss = new Calendar_Model_Rss($options);
    }

    public function getRssFeeds() {
        return $this->rss->getRssFeeds();
    }
}