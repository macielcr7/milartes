<?php

class Calendar_Controller_Schedule extends SpcController {
    public $schedule;

    public function __construct() {
        parent::__construct();
        $this->schedule = new Calendar_Model_Schedule;
    }

    public function getUserSchedules($adminId, $date, $getAllSchedules = false) {
        $schedule = $this->schedule->getUserSchedules($adminId, $date, $getAllSchedules);
        echo Spc::jsonEncode(array('schedule' => $schedule));
    }

    public function getFreeBusySharedUsers($userId) {
        $users = $this->schedule->getFreeBusySharedUsers($userId);
        echo Spc::jsonEncode(array('users' => $users));
    }

    public function shareFreeBusy($username) {
        $this->schedule->shareFreeBusy($username);
    }

    public function deleteFreeBusySharedUser($sharedUserId) {
        $this->schedule->delete("user_id = {$this->_userId} AND shared_user_id = $sharedUserId");
    }
}