<?php

/**
 * Gets User Preferences
 */
class Core_Model_System extends SpcDbTable {

    /**
     * spc_users table
     *
     * @var string
     */
    protected $_name = 'users';

    public function updateApp() {
        $sqls = array();

        $sqls[] = "ALTER TABLE spc_calendar_settings ADD overlap_management enum('simple','advanced') DEFAULT 'simple'";

        $sqls[] = "ALTER TABLE spc_calendar_settings ADD overlap_prevented_cals varchar(255) DEFAULT NULL";

        $sqls[] = "ALTER TABLE spc_calendar_settings ADD check_overlap_prevented_cals_method enum('independent','merge') DEFAULT 'independent'";

        $sqls[] = "ALTER TABLE spc_calendar_settings ADD prevent_overlap_cal_groups varchar(20) DEFAULT NULL";

        foreach ($sqls as $sql) {
            $this->query($sql);
        }
    }
}