<div class="small-icon">
    <?php if (SPC_USER_ROLE != 'user'): ?>
        <div
            id="account-settings"
            class="btn username"
            title="<?php echo Spc::t('Account Settings'); ?>">
                <?php echo SPC_USERNAME; ?>
        </div>
    <?php endif; ?>
    <?php if (SPC_USER_ROLE == 'user'): ?>
        <div class="btn username"><?php echo SPC_USERNAME; ?></div>
    <?php endif; ?>

    <div
        id="spc-refresh"
        class="btn ui-button ui-widget ui-state-default ui-corner-left"
        title="Refresh">
            <span class="ui-icon ui-icon-refresh"></span>
    </div>
    <a
        id="print-calendar"
        class="btn ui-button ui-widget ui-state-default"
        href="#"
        target="_blank"
        title="<?php echo Spc::t('Print Calendar'); ?>">
            <span class="ui-icon ui-icon-print"></span>
    </a>
    <?php if (SPC_USER_ROLE != 'user'): ?>
    <div
        id="keyboard-shortcuts"
        class="btn ui-button ui-widget ui-state-default"
        title="<?php echo Spc::t('Keyboard shortcuts'); ?>">
            <span class="ui-icon ui-icon-calculator"></span>
    </div>
    <?php endif; ?>
    
    <?php if (SPC_USER_ROLE != 'user'): ?>
    <div
        id="system-users"
        title="<?php echo Spc::t('Manage Users'); ?>"
        class="btn ui-button ui-widget ui-state-default">
            <span class="ui-icon ui-icon-person"></span>
    </div>
    <?php endif; ?>

    <?php if (SPC_USER_ROLE != 'user'): ?>
    <div
        id="system-settings"
        title="<?php echo Spc::t('Calendar Settings'); ?>"
        class="btn ui-button ui-widget ui-state-default">
            <span class="ui-icon ui-icon-wrench"></span>
    </div>
    <a
        id="help"
        class="btn ui-button ui-widget ui-state-default"
        href="http://smartphpcalendar.com/support"
        target="_blank"
        title="<?php echo Spc::t('help'); ?>">
            <span class="ui-icon ui-icon-help"></span>
    </a>
    <a
        id="public-cal"
        class="btn ui-button ui-widget ui-state-default"
        href="?p=1&u=<?php echo SPC_USERNAME; ?>"
        target="_blank"
        title="<?php echo Spc::t('Public calendar'); ?>">
            <span class="ui-icon ui-icon-copy"></span>
    </a>
    <div
        id="logout"
        title="<?php echo Spc::t('logout'); ?>"
        class="btn ui-button ui-widget ui-state-default ui-corner-right">
            <span class="ui-icon ui-icon-power"></span>
    </div>
    <?php endif; ?>
</div>