<?php
/**
 * Smart PHP Calendar
 *
 * @category   Spc
 * @package    Core
 * @copyright  Copyright (c) 2008-2011 Yasin Dagli (http://www.smartphpcalendar.com)
 * @license    http://www.smartphpcalendar.com/license
 */

/**
 * Core Application Class
 *
 * Holds global application methods and initializers
 */
class Spc {

    private static $_sender;

    protected static $_dbConn;

    private static $_user;

    private static $_modules;

    private static $_plugins;

    private static $_translate;

    private static $_timezones;

    private static $_languages;

    private static $_themes;

    public static $_userPrefs;

    /**
     * Hold old system permissions list
     *
     * @var array
     */
    private static $_systemPermissions;

    /**
     * Requires a file from modules
     *
     * @param string $filepath
     * @param string $module
     * @return mixed
     */
    public static function requireFile($filepath, $module) {
        return require SPC_SYSPATH . '/apps/' . $module . '/' . $filepath;
    }

    public static function getDomain() {
        return SPC_DOMAIN;
    }

    public static function getLicenseKey() {
        return SPC_LICENSE_KEY;
    }

    public static function startSession() {
        if (!isset($_SESSION)) {
            $sessionName = session_name();
            if (isset($_POST[$sessionName])) {
                session_id($_POST[$sessionName]);
            }
            session_start();
        }
    }

    public static function initModules() {
        define('SPC_MODULES', 'calendar');
    }

    public static function initPlugins() {
        $plugins = array();
        if (file_exists(SPC_APP_DIR . '/m')) {
            $plugins[] = 'mobile_calendar';
        }

        if (file_exists(SPC_APP_DIR . '/services/event-calendar')) {
            $plugins[] = 'event_calendar';
        }

        define('SPC_PLUGINS', join(',', $plugins));
    }

    public static function initApp() {
        if (!defined('SPC_OUTPUT_BUFFER_OFF')) {
            ob_start();
        }

        self::startSession();

        if (isset($_SESSION['spcUserPrefs'])) {
            Spc::setUserPrefs($_SESSION['spcUserPrefs']);
        }

        self::initModules();
        self::initPlugins();

        //application modules (calendar, contacts, tasks, file-manager, etc.)
        self::$_modules = SPC_MODULES ? explode(',', SPC_MODULES) : array();

        //application plugins (events_calendar, mobile_calendar, etc.)
        self::$_plugins = SPC_PLUGINS ? explode(',', SPC_PLUGINS) : array();

        //user permissions
        self::$_systemPermissions = array(
            'create-users',
            'edit-users',
            'delete-users',

            'create-calendars',
            'edit-calendars',
            'delete-calendars',
            'share-calendars',

            'create-events',
            'edit-events',
            'delete-events'
        );

        if (!defined('SPC_REST')) {
            //run
            self::run();
        }
    }

    public static function initAutoload() {
        require SPC_SYSPATH . '/core/SpcAutoLoader.php';
        $spcAutoLoader = new SpcAutoLoader;
        spl_autoload_register(array($spcAutoLoader, 'autoloader'));
    }

    public static function run() {
        //init database to sanitize post
        new SpcDb;

        //ajax engine run
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                && (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {

            self::setSender(isset($_POST['sender']) ? $_POST['sender'] : $_GET['sender']);
            if (self::getSender() === 'private') {
                self::checkLogin();
            }

            //if user is set define app global constants
            if (isset($_SESSION['spcUserPrefs'])) {
                $userModel = new Core_Model_User;
                $userModel->initAppConstants();
            }

            if (self::getSender() == 'event-calendar') {
                self::setEventsCalUserPrefs($_POST['accessKey']);
            }

            $afc = new SpcAjaxFrontController();
            $afc->dispatch();
        }
    }

    public static function getSystemPermissions() {
        return self::$_systemPermissions;
    }

    public static function checkLogin() {
        if (!isset($_SESSION['spcUserLoggedIn'])
                || (isset($_SESSION['SPC_VERSION']) && ($_SESSION['SPC_VERSION'] != SPC_VERSION))) {

            unset($_SESSION['spcUserLoggedIn']);
            if (isset($_GET['signup'])) {
                header('Location: ' . SPC_ROOT . '/login.php?signup=true');
            } else {
                header('Location: ' . SPC_ROOT . '/login.php');
            }

            exit;
        }

        self::setSender('private');

        $userModel = new Core_Model_User;
        $_SESSION['spcUserPrefs'] = $userModel->getUserPrefs($_SESSION['spcUserPrefs']['id']);
        $userModel->initAppConstants();
        Spc::initLanguage();
        Spc::initUserPermissions(SPC_USERID);
        SpcCalendar::initUserCalendars();
    }

    public static function getLanguages() {
        //init languages
        if (!self::$_languages) {
            $langTitles = array(
                'de' => 'Deutsch',
                'en' => 'English',
                'es' => 'Español',
                'fr' => 'Français',
                'el' => 'Greek',
                'it' => 'Italiano',
                'nl' => 'Nederlands',
                'pt-BR' => 'Português Brasil',
                'tr' => 'Türkçe',
            );

            $dirIt = new DirectoryIterator(SPC_SYSPATH . '/i18n');
            foreach ($dirIt as $file) {
                if ($file->isFile()) {
                    list($langId, $ext) = explode('.', $file->getFileName());
                    if (isset($langTitles[$langId])) {
                        self::$_languages[$langId] = $langTitles[$langId];
                    } else {
                        self::$_languages[$langId] = $langId;
                    }
                }
            }
        }

        return self::$_languages;
    }

    public static function getTimezones() {
        global $t_z;
        require_once SPC_SYSPATH . '/apps/calendar/files/timezones.php';
        self::$_timezones = $t_z;
        return self::$_timezones;
    }

    /**
     * Gets application modules
     *
     * @return array
     */
    public static function getModules() {
        return self::$_modules;
    }

    /**
     * Gets application plugins
     *
     * @return array
     */
    public static function getPlugins() {
        return self::$_plugins;
    }

    public static function getSender() {
        return self::$_sender;
    }

    public static function setSender($sender) {
        self::$_sender = $sender;
    }

    public static function setUserPrefs($userPrefs) {
        self::$_userPrefs = $userPrefs;
    }

    public static function getUserPrefs() {
        return self::$_userPrefs;
    }

    public static function getAppJSProps($userId = null) {

        $userModel = new Core_Model_User();

        $appProps = $userModel->getAppProps();

        $userPrefs = self::jsonEncode($appProps['userPrefs'], false, false);
        $i18n = self::jsonEncode($appProps['i18n'], false, false);
        $userCals = self::jsonEncode($appProps['userCals'], false, false);

        return "window.SPC = {};
                window.SPC_DOMAIN = '{$appProps['domain']}';
              window.SPC_LICENSE_KEY = '{$appProps['licenseKey']}'
                SPC.userPrefs = {$userPrefs};
            window.SPCi18n = {$i18n};
            SPC.currentDate = '{$appProps['curDate']}';
            SPC.PHP_SESSNAME = '{$appProps['sessName']}';
            SPC.eventReminderCount = '{$appProps['eventReminderCount']}';
            SPC.USER_CALENDARS = {$userCals};
            SPC.userCals = SPC.USER_CALENDARS;
            SPC.ROOT = '{$appProps['root']}';
              ";
    }

    public static function getUserPref($pref, $module = null) {
        $userPrefs = self::$_userPrefs;
        //get module's preference
        if ($module) {
             $pref = $userPrefs[$module][$pref];
        } else {
            //get core application preference
            $pref = $userPrefs[$pref];
        }

        return $pref;
    }

    /**
     * Get application themes
     * (theme folder names in css/theme folder)
     *
     * @return array
     */
    public static function getAppThemes() {
        if (self::$_themes) {
            return self::$_themes;
        }

        $themeDir = SPC_APP_DIR . '/css/theme';
        $dirIt = new DirectoryIterator($themeDir);
        $themes = array();
        foreach ($dirIt as $f) {
            if ($f->isDir() && !$f->isDot()) {
                $themes[] = $f->getFilename();
            }
        }

        self::$_themes = $themes;

        return $themes;
    }

    /**
     * Get application icons directory
     * (global app icons placed in img folder)
     *
     * @return string
     */
    public static function getGlobalAppIconsDirName() {
        $userTheme = self::getUserPref('theme');
        $blackIconsDir = 'black-icons';
        $blueIconsDir = 'blue-icons';
        $blueThemes = array('smart-blue', 'flick', 'cupertino', 'hot-sneaks', 'redmond', 'smart-space');
        if (array_search($userTheme, $blueThemes) !== false) {
            return $blueIconsDir;
        }

        return $blackIconsDir;
    }

    /**
     * Gets Main App Icons (top-right)
     */
    public static function getAppIcons() {
        if (self::getUserPref('big_icons') == 1) {
            return require SPC_SYSPATH . '/apps/core/files/big-icons.php';
        }

        return require SPC_SYSPATH . '/apps/core/files/small-icons.php';
    }

    /**
     * Encodes variables to JSON string
     *
     * @param mixed         $v value
     * @param bool          $success
     * @return string       encoded json string
     */
    public static function jsonEncode($v = array(), $success = true, $addSuccessMsg = true) {
        if ($addSuccessMsg) {
            $v['success'] = $success;
        }

        if (version_compare(PHP_VERSION, '5.2.0', '<')) {
            require_once SPC_SYSPATH . '/libs/JSON.php';
			$j = new Services_JSON();
            $jsonRes = $j->encode($v);
		}

		$jsonRes = json_encode($v);

        //jsonp request
        //jQuery sends its unique callback function name and it calls this
        //callback function with the ajax success method
        if (isset($_GET['callback'])) {
            $callback = $_GET['callback'];
            $jsonRes = "$callback($jsonRes)";
        }

        return $jsonRes;
    }

    public static function initLanguage($lang = null) {
        //system language
        global $spcI18n;
        if ($lang === null) {
            require_once SPC_SYSPATH . '/i18n/' . self::getUserPref('language') . '.php';
        } else {
            require_once SPC_SYSPATH . "/i18n/$lang.php";
        }

        self::$_translate = $spcI18n;
    }

    public static function initUserPermissions($userId) {
        $userModel = new Core_Model_User;
        $userPermissions = $userModel->getUserPermissions($userId);
        $_SESSION['spcUserPrefs']['permissions'] = $userPermissions;
        Spc::setUserPrefs($_SESSION['spcUserPrefs']);
    }

    public static function getLanguageTranslationArray() {
        return self::$_translate;
    }

    public static function translate($key, $capitalize = true) {
        $translation = $key;
        if (isset(self::$_translate[$key])) {
            $translation = self::$_translate[$key];
        }

        if ($capitalize) {
            return self::mb_ucfirst($translation);
        }

        return $translation;
    }

    public static function t($key, $capitalize = true) {
        return self::translate($key, $capitalize);
    }

    public static function mb_ucfirst($str) {
		if (function_exists('mb_strtoupper')) {
			return mb_strtoupper(mb_substr($str, 0, 1, 'utf-8'), 'utf-8')
                   . mb_substr($str, 1, (mb_strlen($str, 'utf-8') - 1), 'utf-8');
		}

		return ucfirst($str);
	}

    public static function substr($str, $start, $lenght) {
        if (function_exists('mb_substr')) {
            return mb_substr($str, $start, $lenght, 'utf-8');
        }

        return substr($str, $start, $lenght);
    }

    public static function getEventCalAddr() {
        $accessKey = self::getUserPref('access_key');
        $eventCalAddr = SPC_ROOT . '/services/event-calendar/event-calendar.php?accessKey=' . $accessKey;
        return $eventCalAddr;
    }

    public static function activateAccount($accessKey) {
        $userModel = new Core_Model_User();
        return $userModel->update(array(
            'activated' => '1'
        ), "access_key = '{$accessKey}'");
    }

    //--------------------------------------------------------------------------
    // Application Integration Methods
    //--------------------------------------------------------------------------

    public static function importUsers($users) {
        //get default admin id
        $coreUserModel = new Core_Model_User();
        $select = $coreUserModel->select('id')->where('role = "admin"')->order('id');
        $defaultAdminId = $coreUserModel->fetchColumn($select);

        foreach ($users as $user) {
            $adminId = $defaultAdminId;
            if (isset($user['adminId'])) {
                if (is_int($user['adminId'])) {
                    $adminId = $user['adminId'];
                } else {
                    $select = $coreUserModel->select('id')->where("username = '{$user['adminId']}'");
                    $adminId = $coreUserModel->fetchColumn($select);
                }
            }
            $spcUser = array(
                'username' => $user['username'],
                'admin_id' => $adminId,
                'password' => $user['username'],
                'email' => $user['email'],
                'role' => 'user'
            );
            $coreUserModel->createUser($spcUser);
        }
    }

    public static function login($username, $userLoggedIn = true) {
        //init user session
        $userModel = new Core_Model_User;
        $_SESSION['spcUserPrefs'] = $userModel->getUserPrefs(null, $username);
        Spc::setUserPrefs($_SESSION['spcUserPrefs']);

        $userModel->initAppConstants();

        //init user calendars
        SpcCalendar::initUserCalendars();

        if (isset($_SESSION['spcUserLoggedIn']) && $_SESSION['spcUserPrefs']['username'] == $username) {
            return;
        }

        if ($userLoggedIn) {
            //key for whole application login
            $_SESSION['spcUserLoggedIn'] = true;
            $_SESSION['SPC_VERSION'] = SPC_VERSION;
        } else {
            unset($_SESSION['spcUserLoggedIn']);
        }
    }

    public static function createUser($user) {
        $user = array($user);
        self::importUsers($user);
    }

    public static function deleteUser($username) {
        $coreUserModel = new Core_Model_User();
        $select = $coreUserModel->select('id')->where("username = '{$username}'");
        $userId = $coreUserModel->fetchColumn($select);

        $coreUserModel->deleteUser($userId);
    }
}