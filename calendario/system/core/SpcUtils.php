<?php

class SpcUtils {
    public static function init() {

    }

    public static function camelize(&$v, $ret = false) {
        if (is_string($v)) {
            return preg_replace_callback('/((_)(.))/', create_function('$m', 'return strtoupper($m[3]);'), $v);
        }

        if (is_array($v)) {
            foreach ($v as $x => &$y) {
                if (is_array(($y))) {
                    self::camelize($y);
                } else {
                    $v[self::camelize($x)] = $y;
                }
            }
        }
    }
}