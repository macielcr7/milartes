<?php
/**
 * Smart PHP Calendar
 *
 * @category   Spc
 * @package    Calendar
 * @copyright  Copyright (c) 2008-2011 Yasin Dagli (http://www.smartphpcalendar.com)
 * @license    http://www.smartphpcalendar.com/license
 */

require 'SpcEngine.php';

if (!isset($_GET['u'])) {
    exit('Undefined User!');
}

$user = $_GET['u'];

$userModel = new Core_Model_User;
$userPrefs = $userModel->getUserPrefs(null, $user);

Spc::initLanguage($userPrefs['language']);

//define your rss options
$options = array(
    'channelTitle' => 'Smart PHP Calendar Events',
    'channelDescription' => 'Smart PHP Calendar Events RSS',
    'channelLink' => SPC_ROOT . '/rss.php',
    'calendarDir' => SPC_ROOT
);

$rssOptions = array_merge($options, $_GET);

$rssCtrl = new Calendar_Controller_Rss($rssOptions);
echo $rssCtrl->getRssFeeds();