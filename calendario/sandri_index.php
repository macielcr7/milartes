<?php
session_start();
if(!isset($_SESSION['SESSION_USUARIO'])) {
    header("Location: ../login.php");
    exit;
}
else{
    $sessao = unserialize($_SESSION['SESSION_USUARIO']);

    $id = $sessao['id'];
    $nome = $sessao['nome'];
    $username = $sessao['login'];

    require 'SpcEngine.php';
    try {
        Spc::login($username);
        header("Location: index.php");
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
?>