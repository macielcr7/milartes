<?php

/**
 * Smart PHP Calendar
 *
 * @category   Spc
 * @package    Core
 * @copyright  Copyright (c) 2008-2012 Yasin Dagli (http://www.smartphpcalendar.com)
 * @license    http://www.smartphpcalendar.com/license
 */

define('SPC_OUTPUT_BUFFER_OFF', true);

require_once 'SpcEngine.php';

$module = $_GET['m'];
$action = $_GET['a'];

switch ($module) {
    case 'calendar':
        $exportFileName = 'calendar.ics';
        $fileStr = SpcCalendar::export($action, $_GET);
        break;
}

ob_end_clean();
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header("Content-Disposition: attachment; filename=\"{$exportFileName}\"");
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . strlen($fileStr));
echo $fileStr;
exit;

/*
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"calendar.ics\"");
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . strlen($fileStr));
echo $fileStr;
*/

ob_end_flush();