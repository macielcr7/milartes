<?php
    if (isset($_SESSION['spcUserLoggedIn'])) {
        header("Location: index.php");
        exit;
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Smart PHP Calendar - Mobile</title>

        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="stylesheet" href="css/jquery.mobile.css" />
		<link rel="stylesheet" href="css/spc.mobile.css?v=<?php echo time(); ?>" />

		<script src="../js/jquery.js"></script>

		<script type="text/javascript">
            window.SPC = {};
            window.SPCM = {};
            SPCM.USER_THEME = "c";
            $(function() {
                $("#username")
                    .on("focus", function(e) {
                        if (this.value == "Username") {
                            this.value = "";
                        }
                    })
                    .on("blur", function(e) {
                        if (this.value == "") {
                            this.value = "Username";
                        }
                    });

                    $("#mobile-login-form")
                        .on("focus", "#password", function(e) {
                            if (this.value == "Password") {
                                $("#password").remove();
                                $("#password-row").html("<input type='password' id='password' value='' />");
                                $("#password").textinput().trigger("focus");
                            }
                        })
                        .on("blur", "#password", function(e) {
                            if (this.value == "") {
                                $("#password").remove();
                                $("#password-row").html("<input type='text' id='password' value='Password' />");
                                $("#password").textinput();
                            }
                        });

                $("#login").on("click", function(e) {
                    $.mobile.showPageLoadingMsg();
                    var username = $("#username").val(),
                        password = $("#password").val();

                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: '../SpcEngine.php',
                        data: {
                            sender: "login",
                            doLogin: true,
                            spcAppRequest: "core/login/checkLogin",
                            params: [username, password]
                        },
                        success: function(res) {
                            $.mobile.hidePageLoadingMsg();
                            if (res.success === false) {
                                alert(res.errorMsg);
                            } else {
                                window.location.href = "index.php";
                            }
                        }
                    });
                    return false;
                });
            });
		</script>

		<script type="text/javascript" src="js/jquery.mobile.js?v=<?php echo time(); ?>"></script>

        <style>
            #mobile-login-form {
                width: 99%;
                margin: 0 auto;
            }
            .row {
                list-style-type: none;
                padding: 10px;
            }

            .row input {
                color: #666 !important;
                margin: 0 auto;
            }
            h4 {
                font-size: 12px !important;
                color: #666;
            }
        </style>
	</head>

	<body>
		<div data-role="page">
			<div data-role="header">
                <h1>Login</h1>
			</div>

			<div data-role="content" style="text-align: center;">
                <div id="mobile-login-form">
                    <div class="row">
                            <input type="text" id="username" value="Username" />
                            </div>
                        <div id="password-row" class="row">
                            <input type="text" id="password" value="Password" />
                        </div>
                        <div class="row">
                            <button id="login">Login</button>
                        </div>
                </div>
			</div>

            <div data-role="footer">
                <h4>Smart PHP Calendar Mobile</h4>
            </div>
        </div>
	</body>
</html>