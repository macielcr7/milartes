<?php
    if (!isset($_GET['u'])) {
        exit('Undefined user!');
    }

    require_once '../SpcEngine.php';

    $coreUserModel = new Core_Model_User;
    $u = mysql_real_escape_string($_GET['u']);
    $_SESSION['spcUserPrefs'] = $coreUserModel->getUserPrefs(null, $u);
    $coreUserModel->initAppConstants();
    //init user calendars
    SpcCalendar::initUserCalendars();

    Spc::initLanguage();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Smart PHP Calendar - Mobile</title>

        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="stylesheet" href="css/jquery.mobile.structure.css" />
        <link rel="stylesheet" href="css/jquery.mobile.theme.css" />
		<link rel="stylesheet" href="css/spc.mobile.css?v=<?php echo time(); ?>" />

		<script type="text/javascript" src="../js/jquery.js"></script>

		<script type="text/javascript">
        var SPC_DOMAIN = "<?php echo Spc::getDomain(); ?>",
			SPC_LICENSE_KEY = "<?php echo Spc::getLicenseKey(); ?>";

			window.SPC = {};
            window.SPCM = {};

			SPC.ROOT = "<?php echo SPC_ROOT; ?>";
            SPC.ENGINE_PATH = "../SpcEngine.php";

			SPC.sender = "public|mobile-calendar";
			SPC.currentDate = "<?php echo date('Y-m-d'); ?>";

			SPC.userPrefs = <?php echo Spc::jsonEncode(Spc::getUserPrefs()); ?>;
			SPCi18n = <?php echo Spc::jsonEncode(Spc::getLanguageTranslationArray()); ?>;
            
            if (!$.datepicker) {
                $.datepicker = {
                    regional: {},
                    setDefaults: function() {}
                };
            }

            SPC.USER_CALENDARS = $.parseJSON('<?php echo Spc::jsonEncode($_SESSION['calendars']); ?>');
            SPCM.USER_THEME = SPC.userPrefs.mobile_calendar.theme;
		</script>

		<script type="text/javascript" src="js/jquery.mobile.js?v=<?php echo time(); ?>"></script>
        <script type='text/javascript' src='../js/ui-i18n/jquery.ui.datepicker-<?php echo Spc::getUserPref('language'); ?>.js'></script>
		<script type="text/javascript" src="../js/spc.core.js?v=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="../js/spc.utils.js?v=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="js/spc.calendar.mobile.public.js?v=<?php echo time(); ?>"></script>
	</head>

	<body>
		<div data-role="page">
			<div data-role="header">
				<div id="top-nav" class="ui-helper-clearfix ui-overlay-shadow">
					<div id="view-buttons">
						<div data-role="controlgroup" data-type="horizontal">
							<a
							   id="day-view"
							   class="spcm-cal-view"
							   href="#"
							   data-role="button">
                                <?php echo Spc::translate('day'); ?>
                            </a>
                            <a
							   id="month-view"
							   class="spcm-cal-view"
							   href="#"
							   data-role="button">
                                <?php echo Spc::translate('month'); ?>
                            </a>
                            <a
							   id="agenda-view"
							   class="spcm-cal-view"
							   href="#"
							   data-role="button">
                                <?php echo Spc::translate('agenda'); ?>
                            </a>
						</div>
					</div>
				</div>

				<div id="cal-status" class="ui-helper-clearfix">
					<div id="curdate"></div>
					<div id="pager">
						<div data-role="controlgroup" data-type="horizontal" >
							<a
							   id="today"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="back"
							   data-iconpos="notext">
                                <?php echo Spc::translate('today'); ?>
                            </a>
							<a
							   id="prev"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="arrow-l"
							   data-iconpos="notext">
                                <?php echo Spc::translate('previous'); ?>
                            </a>
							<a
							   id="next"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="arrow-r"
							   data-iconpos="notext">
                                <?php echo Spc::translate('next'); ?>
                            </a>
						</div>
					</div>
				</div>
			</div>

			<div data-role="content" id="main-cal-grid">
				<div id="calendar-content"></div>
				<div id="events-list"></div>
			</div>
        </div>

        <!--- DIALOGS -->

        <div id="calendars-dialog" data-role="page">
            <div data-role="header">
                <h1>
                    <?php echo Spc::translate('calendars') ?>
                </h1>
            </div>

            <div data-role="content">
                <select id="calendars-dialog-calendars" multiple="multiple">
                    <option><?php echo Spc::translate('calendars'); ?></option>

                    <optgroup label="<?php echo Spc::translate('My Calendars'); ?>">
                        <?php
                            $userCals = '';
                            foreach ($_SESSION['spcUserCalendars'] as $calId => $cal) {
                                $selected = '';
                                if ($cal['status'] == 'on') {
                                    $selected = 'selected="selected"';
                                }
                                $userCals .= "<option value='{$calId}' {$selected}>{$cal['name']}</option>";
                            }
                            echo $userCals;
                        ?>
                    </optgroup>
                </select>
            </div>
        </div>

        <a
            id="open-display-event-dialog"
            href="#display-event-dialog"
            data-rel="dialog"
            data-transition="pop"
            class="hidden">open display-event-dialog</a>

        <div id="display-event-dialog" data-role="page">
            <div data-role="header">
                <h1></h1>
            </div>

            <div data-role="content">
                <div id="display-event-dialog-title"></div>
                <div id="display-event-dialog-datetime"></div>
                <div id="display-event-dialog-description"></div>
                <div id="display-event-dialog-location"></div>
                <div id="display-event-dialog-repeat"></div>
            </div>
        </div>

        <!-- /DIALOGS -->
	</body>
</html>