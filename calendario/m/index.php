<?php
    require_once '../SpcEngine.php';
    Spc::checkLogin();

    $userTheme = Spc::getUserPref('theme', 'mobile_calendar');
    $themes = array(
        'a' => 'black',
        'b' => 'blue',
        'c' => 'silver',
        'd' => 'metalic',
        'e' => 'yellow'
    );
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Smart PHP Calendar - Mobile</title>

        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="stylesheet" href="css/jquery.mobile.css" />
		<link rel="stylesheet" href="css/spc.mobile.css?v=<?php echo time(); ?>" />

		<script type="text/javascript" src="../js/jquery.js"></script>

		<script type="text/javascript">
        var SPC_DOMAIN = "<?php echo Spc::getDomain(); ?>",
			SPC_LICENSE_KEY = "<?php echo Spc::getLicenseKey(); ?>";

			window.SPC = {};
            window.SPCM = {};

			SPC.ROOT = "<?php echo SPC_ROOT; ?>";
            SPC.ajaxURL = "../SpcEngine.php";

			SPC.sender = "private|mobile-calendar";
			SPC.currentDate = "<?php echo date('Y-m-d'); ?>";

			SPC.userPrefs = $.parseJSON('<?php echo Spc::jsonEncode(Spc::getUserPrefs()); ?>');
			SPCi18n = <?php echo Spc::jsonEncode(Spc::getLanguageTranslationArray()); ?>;
            if (!$.datepicker) {
                $.datepicker = {
                    regional: {},
                    setDefaults: function() {}
                };
            }

            SPC.USER_CALENDARS = $.parseJSON('<?php echo Spc::jsonEncode($_SESSION['calendars']); ?>');
            SPCM.USER_THEME = SPC.userPrefs.mobile_calendar.theme;

            $(document).bind("mobileinit", function(){
                $.extend($.mobile, {
                    defaultDialogTransition: "flip",
                    defaultPageTransition: "flip"
                });
            });
		</script>

		<script type="text/javascript" src="js/jquery.mobile.js?v=<?php echo time(); ?>"></script>
        <script type='text/javascript' src='../js/ui-i18n/jquery.ui.datepicker-<?php echo Spc::getUserPref('language'); ?>.js'></script>
		<script type="text/javascript" src="../js/spc.core.js?v=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="../js/spc.utils.js?v=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="js/spc.calendar.mobile.js?v=<?php echo time(); ?>"></script>
	</head>

	<body>
		<div data-role="page">
			<div data-role="header">
				<div id="top-nav" class="ui-helper-clearfix ui-overlay-shadow">
					<div id="view-buttons">
						<div data-role="controlgroup" data-type="horizontal">
							<a
							   id="day-view"
							   class="spcm-cal-view"
							   href="#"
							   data-role="button">
                                <?php echo Spc::translate('day'); ?>
                            </a>
                            <a
							   id="month-view"
							   class="spcm-cal-view"
							   href="#"
							   data-role="button">
                                <?php echo Spc::translate('month'); ?>
                            </a>
                            <a
							   id="agenda-view"
							   class="spcm-cal-view"
							   href="#"
							   data-role="button">
                                <?php echo Spc::translate('agenda'); ?>
                            </a>
						</div>
					</div>

					<div id="cal-utils">
						<div data-role="controlgroup" data-type="horizontal">
							<a
							   id="refresh"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="refresh"
							   data-iconpos="notext">
                                <?php echo Spc::translate('Refresh'); ?>
                            </a>
							<a
							   id="create-event-dialog-open"
							   class="spc-icon-btn"
							   href="#create-event-dialog"
							   data-role="button"
							   data-icon="plus"
                               data-rel="dialog"
                               data-transition="pop"
							   data-iconpos="notext">
                                <?php echo Spc::translate('Create Event'); ?>
                            </a>
						</div>
					</div>
				</div>

				<div id="cal-status" class="ui-helper-clearfix">
					<div id="curdate"></div>
					<div id="pager">
						<div data-role="controlgroup" data-type="horizontal" >
							<a
							   id="today"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="back"
							   data-iconpos="notext">
                                <?php echo Spc::translate('today'); ?>
                            </a>
							<a
							   id="prev"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="arrow-l"
							   data-iconpos="notext">
                                <?php echo Spc::translate('previous'); ?>
                            </a>
							<a
							   id="next"
							   class="spc-icon-btn"
							   href="#"
							   data-role="button"
							   data-icon="arrow-r"
							   data-iconpos="notext">
                                <?php echo Spc::translate('next'); ?>
                            </a>
						</div>
					</div>
				</div>
			</div>

			<div data-role="content" style="border: 0;">
				<div id="calendar-content"></div>
				<div id="events-list"></div>
			</div>

			<div data-role="footer">
                <div data-role="navbar" data-iconpos="top">
                    <ul>
                        <li>
                            <a
                                id="calendar-dialog-btn"
                                href="#calendars-dialog"
                                data-role="button"
                                data-icon="grid"
                                data-transition="pop"
                                data-rel="dialog">
                                    <?php echo Spc::translate('calendars'); ?>
                            </a>
                        </li>
                        <li>
                            <a
                                id="theme"
                                href="#themes-dialog"
                                data-role="button"
                                data-icon="star"
                                data-transition="pop"
                                data-rel="dialog">
                                    <?php echo Spc::translate('theme'); ?>
                            </a>
                        </li>
                        <li>
                            <a id="logout" href="#" data-role="button" data-icon="delete">
                                <?php echo Spc::translate('logout'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="username" class="t-a-center ui-bar-<?php echo $userTheme; ?>">
                <?php echo SPC_USERNAME; ?>
            </div>
        </div>

        <!--- DIALOGS -->

        <div id="create-event-dialog" data-role="page">
            <div data-role="content">
                <table id="create-event-dialog-table">
                    <tbody>
                        <tr>
                            <td colspan="3">
                                <select id="create-event-dialog-calendar">
                                    <?php
                                        $cals = '';
                                        //personal calendars
                                        $cals .= '<optgroup label="' . Spc::translate('My Calendars') . '">';
                                            foreach ($_SESSION['spcUserCalendars'] as $calId => $cal) {
                                                $cals .= "<option value='{$calId}'>{$cal['name']}</option>";
                                            }
                                        $cals .= '</optgroup>';

                                        //shared calendars
                                        $cals .= '<optgroup label="' . Spc::translate('Other Calendars') . '">';
                                            foreach ($_SESSION['spcUserSharedCalendars'] as $calId => $cal) {
                                                if ($cal['permission'] === 'change') {
                                                    $cals .= "<option value='$calId'>{$cal['name']}</option>";
                                                }
                                            }
                                        $cals .= '</optgroup>';

                                        //group calendars
                                        $cals .= '<optgroup label="' . Spc::translate('Group Calendars') . '">';
                                            foreach ($_SESSION['spcUserGroupCalendars'] as $calId => $cal) {
                                                $cals .= "<option value='$calId'>{$cal['name']}</option>";
                                            }
                                        $cals .= '</optgroup>';

                                        echo $cals;
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 60%;">
                                <input
                                    type="button"
                                    id="create-event-dialog-start-date"
                                    class="date-input" value="start-date" />
                            </td>
                            <td>
                                <input
                                    type="button"
                                    id="create-event-dialog-start-time"
                                    class="time-input" value="start-time" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 60%;">
                                <input
                                    type="button"
                                    id="create-event-dialog-end-date"
                                    class="date-input" value="end-date" />
                            </td>
                            <td>
                                <input
                                    type="button"
                                    id="create-event-dialog-end-time"
                                    class="time-input" value="end-time" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <input
                                    id="create-event-dialog-title"
                                    class="spc-ghost-txt"
                                    type="text"
                                    value="<?php echo Spc::translate('title'); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <input
                                    id="create-event-dialog-location"
                                    class="spc-ghost-txt"
                                    type="text"
                                    value="<?php echo Spc::translate('location'); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea
                                    id="create-event-dialog-description"
                                    class="spc-ghost-txt">
                                        <?php echo Spc::translate('description'); ?>
                                </textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div data-role="navbar" class="spc-dialog-buttons">
                    <ul>
                        <li>
                            <a id="create-event-dialog-cancel" href="#" class="ui-corner-right">
                                <?php echo Spc::translate('cancel'); ?>
                            </a>
                        </li>
                        <li>
                            <a id="create-event-dialog-create" href="#" class="ui-corner-left">
                                <?php echo Spc::translate('Create Event'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <a
            id="open-edit-event-dialog"
            href="#edit-event-dialog"
            data-rel="dialog"
            data-transition="pop"
            class="hidden">open edit-event-dialog</a>

        <div id="edit-event-dialog" data-role="page">
            <div data-role="content">
                <table id="edit-event-dialog-table">
                    <tbody>
                        <tr>
                            <td colspan="3">
                                <select id="edit-event-dialog-calendar">
                                    <?php
                                        $cals = '';
                                        //personal calendars
                                        $cals .= '<optgroup label="' . Spc::translate('My Calendars') . '">';
                                            foreach ($_SESSION['spcUserCalendars'] as $calId => $cal) {
                                                $cals .= "<option value='{$calId}'>{$cal['name']}</option>";
                                            }
                                        $cals .= '</optgroup>';

                                        //shared calendars
                                        $cals .= '<optgroup label="' . Spc::translate('Other Calendars') . '">';
                                            foreach ($_SESSION['spcUserSharedCalendars'] as $calId => $cal) {
                                                if ($cal['permission'] === 'change') {
                                                    $cals .= "<option value='$calId'>{$cal['name']}</option>";
                                                }
                                            }
                                        $cals .= '</optgroup>';

                                        //group calendars
                                        $cals .= '<optgroup label="' . Spc::translate('Group Calendars') . '">';
                                            foreach ($_SESSION['spcUserGroupCalendars'] as $calId => $cal) {
                                                $cals .= "<option value='$calId'>{$cal['name']}</option>";
                                            }
                                        $cals .= '</optgroup>';

                                        echo $cals;
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 60%;">
                                <input
                                    type="button"
                                    id="edit-event-dialog-start-date"
                                    class="date-input" value="start-date" />
                            </td>
                            <td>
                                <input
                                    type="button"
                                    id="edit-event-dialog-start-time"
                                    class="time-input" value="start-time" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 60%;">
                                <input
                                    type="button"
                                    id="edit-event-dialog-end-date"
                                    class="date-input" value="end-date" />
                            </td>
                            <td>
                                <input
                                    type="button"
                                    id="edit-event-dialog-end-time"
                                    class="time-input" value="end-time" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <input
                                    id="edit-event-dialog-title"
                                    class="spc-ghost-txt"
                                    type="text"
                                    value="<?php echo Spc::translate('title'); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <input
                                    id="edit-event-dialog-location"
                                    class="spc-ghost-txt"
                                    type="text"
                                    value="<?php echo Spc::translate('location'); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea
                                    id="edit-event-dialog-description"
                                    class="spc-ghost-txt">
                                        <?php echo Spc::translate('description'); ?>
                                </textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div data-role="navbar" class="spc-dialog-buttons">
                    <ul>
                        <li>
                            <a href="#" id="edit-event-dialog-cancel" class="ui-corner-right">
                                <?php echo Spc::translate("cancel"); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="edit-event-dialog-delete">
                                <?php echo Spc::translate("delete"); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="edit-event-dialog-save" class="ui-corner-left">
                                <?php echo Spc::translate("save"); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <a href="#datepicker" data-rel="dialog" class="hidden" id="open-datepicker">open datepicker</a>
        <div id="datepicker" data-role="page">
            <div id="header" data-role="header" class="invisible"></div>
            <div data-role="content">
                <div id="datepicker-curdate" class="f-left"></div>
                <div id="datepicker-pager" data-role="controlgroup" data-type="horizontal" class="t-a-right">
                    <a
                       id="datapicker-today"
                       class="spc-icon-btn"
                       href="#"
                       data-dir="today"
                       data-role="button"
                       data-icon="back"
                       data-iconpos="notext">
                        <?php echo Spc::translate('today'); ?>
                    </a>
                    <a
                       id="datapicker-prev"
                       class="spc-icon-btn"
                       href="#"
                       data-dir="prev"
                       data-role="button"
                       data-icon="arrow-l"
                       data-iconpos="notext">
                        <?php echo Spc::translate('previous'); ?>
                    </a>
                    <a
                       id="datapicker-next"
                       class="spc-icon-btn"
                       data-dir="next"
                       href="#"
                       data-role="button"
                       data-icon="arrow-r"
                       data-iconpos="notext">
                        <?php echo Spc::translate('next'); ?>
                    </a>
                </div>

                <div id="datepicker-content"></div>

                <div data-role="controlgroup" data-type="horizontal" class="t-a-center">
                    <a id="datepicker-done" href="#" data-role="button">
                        <?php echo Spc::translate('done'); ?>
                    </a>
                    <a id="datepicker-cancel" href="#" data-role="button">
                        <?php echo Spc::translate('cancel'); ?>
                    </a>
                </div>
            </div>
        </div>

        <a href="#timepicker" data-rel="dialog" class="hidden" id="open-timepicker">open timepicker</a>
        <div id="timepicker" data-role="page">
            <div data-role="header" class="invisible"></div>

            <div data-role="content">
                <table id="timepicker-table">
                    <tbody>
                        <tr>
                            <td>
                                <a
                                    id="timepicker-hour-up"
                                    class="timepicker-hour-pager"
                                    href="#"
                                    data-role="button"
                                    data-icon="arrow-u"
                                    data-iconpos="notext">up</a>
                            </td>
                            <td>
                                <a
                                    id="timepicker-minute-up"
                                    class="timepicker-minute-pager"
                                    href="#"
                                    data-role="button"
                                    data-icon="arrow-u"
                                    data-iconpos="notext">up</a>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="timepicker-hour" />
                            </td>
                            <td>
                                <input type="text" id="timepicker-minute" />
                            </td>
                            <td>
                                <select id="timepicker-am-pm">
                                    <option value="am">am</option>
                                    <option value="pm">pm</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a
                                    id="timepicker-hour-down"
                                    class="timepicker-hour-pager"
                                    href="#"
                                    data-role="button"
                                    data-icon="arrow-d"
                                    data-iconpos="notext">up</a>
                            </td>
                            <td>
                                <a
                                    id="timepicker-minute-down"
                                    class="timepicker-minute-pager"
                                    href="#"
                                    data-role="button"
                                    data-icon="arrow-d"
                                    data-iconpos="notext">up</a>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="t-a-center">
                                <div data-role="controlgroup" data-type="horizontal" class="t-a-center">
                                    <a id="timepicker-done" href="#" data-role="button">
                                        <?php echo Spc::translate('done'); ?>
                                    </a>
                                    <a id="timepicker-cancel" href="#" data-role="button">
                                        <?php echo Spc::translate('cancel'); ?>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="calendars-dialog" data-role="page">
             <div data-role="header">
                <h1>
                    <?php echo Spc::translate('calendars') ?>
                </h1>
            </div>

            <div data-role="content">
                    <select id="calendars-dialog-calendars" multiple="multiple">
                        <option><?php echo Spc::translate('calendars'); ?></option>

                        <optgroup label="<?php echo Spc::translate('My Calendars'); ?>">
                            <?php
                                $userCals = '';
                                foreach ($_SESSION['spcUserCalendars'] as $calId => $cal) {
                                    $selected = '';
                                    if ($cal['status'] == 'on') {
                                        $selected = 'selected="selected"';
                                    }
                                    $userCals .= "<option value='{$calId}' {$selected}>{$cal['name']}</option>";
                                }
                                echo $userCals;
                            ?>
                        </optgroup>

						<?php if (SPC_VERSION != 'START'): ?>
                        <optgroup label="<?php echo Spc::translate('Other Calendars'); ?>">
                            <?php
								$userSharedCals = '';
								foreach ($_SESSION['spcUserSharedCalendars'] as $calId => $cal) {
									$selected = '';
                                    if ($cal['status'] == 'on') {
                                        $selected = 'selected="selected"';
                                    }
									$userSharedCals .= "<option value='$calId' {$selected}>{$cal['name']}</option>";
								}
                                echo $userSharedCals;
                            ?>
                        </optgroup>
						<?php endif; ?>

                        <?php if (SPC_VERSION == 'PREMIUM' && SPC_USER_ROLE != 'super'): ?>
                        <optgroup label="<?php echo Spc::translate('Group Calendars'); ?>">
                            <?php
                                $userGroupCals = '';
                                foreach ($_SESSION['spcUserGroupCalendars'] as $calId => $cal) {
									$selected = '';
                                    if ($cal['status'] == 'on') {
                                        $selected = 'selected="selected"';
                                    }
                                    $userGroupCals .= "<option value='$calId' {$selected}>{$cal['name']}</option>";
                                }
                                echo $userGroupCals;
                            ?>
                        </optgroup>
                        <?php endif; ?>
                    </select>
            </div>
        </div>

        <div id="themes-dialog" data-role="page">
            <div data-role="header">
                <h1><?php echo Spc::translate('theme'); ?></h1>
            </div>

            <div data-role="content">
                <select id="themes-dialog-themes">
                    <option data-placeholder="true">
                        <?php echo Spc::translate('theme'); ?>
                    </option>
                    <?php
                        $themeOptions = '';
                        //Globals: $themes, $userTheme
                        foreach ($themes as $themeLetter => $themeName) {
                            $selected = '';
                            if ($themeLetter == $userTheme) {
                                $selected = 'selected="selected"';
                            }

                            $themeOptions .= "<option value='$themeLetter' $selected>$themeName</option>";
                        }

                        echo $themeOptions;
                    ?>
                </select>

                <input type="button" id="save-theme" value="<?php echo Spc::translate('save'); ?>" />
            </div>
        </div>

        <!--- /DIALOGS -->
	</body>
</html>