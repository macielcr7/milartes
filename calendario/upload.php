<?php
require_once 'SpcEngine.php';
Spc::checkLogin();
Spc::initLanguage();

//------------------------------------------------------------------------------
// Calendar Event Image Upload
//------------------------------------------------------------------------------

if (isset($_POST['image-upload'])) {
    $uploadOptions = array(
        'fileFieldName' => 'spc-image-file',
        'uploadPath' => SPC_SYSPATH . '/apps/calendar/files/user-images/' . SPC_USERNAME . '/org'
    );

    $uploader = new SpcUpload($uploadOptions);

    if (!$uploader->uploadFile()) {
        throw new Exception($uploader->getError());
    }

    //crop image
    $uploadedFileName = $uploader->getUploadedFilename();
    $uploadedFilePath = $uploader->getUploadedFilePath();

    $thumbPath = realpath($uploadOptions['uploadPath'] . '/../thumb');
    $thumbFullPath = $thumbPath . '/' . $uploadedFileName;
    if (!extension_loaded('gd')) {
        copy($uploadedFilePath, $thumbFullPath);
    } else {
        require_once SPC_SYSPATH . '/libs/phpthumb/ThumbLib.inc.php';

        $thumb = PhpThumbFactory::create($uploadedFilePath);
        $thumb->resize(150, 90)->save($thumbFullPath);
    }

    echo Spc::jsonEncode();
}

//------------------------------------------------------------------------------
// Calendar iCal Import
//------------------------------------------------------------------------------

if (isset($_POST['import-ical'])) {
    $uploadOptions = array(
        'fileFieldName' => 'spc-ical-file',
        'uploadPath' => SPC_SYSPATH . '/apps/calendar/files/'
    );

    $uploader = new SpcUpload($uploadOptions);

    if (!$uploader->uploadFile()) {
        throw new Exception($uploader->getError());
    }

    $uploadedFilePath = $uploader->getUploadedFilePath();

    $icalController = new Calendar_Controller_Ical;
    $calId = $_POST['calId'];
    $icalController->import($calId, $uploadedFilePath);

    //remove the uploaded ics file after importing
    unlink($uploadedFilePath);

    echo Spc::jsonEncode();
}

if (isset($_POST['contactPhotoUpload'])) {
    $uploadOptions = array(
        'fileFieldName' => 'Filedata',
        'uploadPath' => SPC_SYSPATH . '/tmp'
    );

    $uploader = new SpcUpload($uploadOptions);

    if (!$uploader->uploadFile()) {
        throw new Exception($uploader->getError());
    }
    //crop image
    $uploadedFileName = $uploader->getUploadedFilename();
    $uploadedFilePath = $uploader->getUploadedFilePath();

    $filePathInfo = pathinfo($uploadedFileName);
    $ext = $filePathInfo['extension'];
    $uniqueFileName = $_POST['contactEmail'];

    $thumbPath = realpath($uploadOptions['uploadPath']);
    $thumbFullPath = $thumbPath . '/' . $uniqueFileName . '.' . $ext;
    if (!extension_loaded('gd')) {
        #copy($uploadedFilePath, $thumbFullPath);
    } else {
        require_once SPC_SYSPATH . '/libs/phpthumb/ThumbLib.inc.php';

        $thumb = PhpThumbFactory::create($uploadedFilePath);
        $thumbFullPath = $thumbPath . '/' . $uniqueFileName . '.png';
        $thumb->resize(100, 100)->save($thumbFullPath, 'png');
    }

    //remove original image
    unlink($uploadedFilePath);
    echo Spc::jsonEncode(array(
        'photo' => $uploader->getUploadedFilename()
    ));
}

if (isset($_FILES['import-contacts-file'])) {
    $uploadOptions = array(
        'fileFieldName' => 'import-contacts-file',
        'uploadPath' => SPC_SYSPATH . '/tmp'
    );

    $uploader = new SpcUpload($uploadOptions);

    if (!$uploader->uploadFile()) {
        throw new Exception($uploader->getError());
    }

    $uploadedFilePath = $uploader->getUploadedFilePath();

    $outlookParser = new Contacts_Controller_OutlookParser();
    $groupId = (int)$_POST['groupId'];
    $outlookParser->import($groupId, $uploadedFilePath);

    echo Spc::jsonEncode();
}

if (isset($_POST['filesAppUpload'])) {
    $parentFolderId = $_POST['parentFolderId'];
    //uploadify bug: uplaadify can't send empty strings
    $description = $_POST['description'] == "null" ? "" : $_POST['description'];
    $fileModel = new Files_Model_File();
    $folderInfo = $fileModel->getFile($parentFolderId);
    $parentFolderPath = $folderInfo['path'];
    $parentFolderRelativePath = $folderInfo['relative_path'];

    //upload file to filesystem
    $uploadOptions = array(
        'fileFieldName' => 'Filedata',
        'uploadPath' => $parentFolderPath,
        'uploadToAwsS3' => FILES_USE_AWS_S3
    );

    $uploader = new SpcUpload($uploadOptions);

    if (!$uploader->uploadFile()) {
        throw new Exception($uploader->getError());
    }

    $fileName = $uploader->getUploadedFilename();
    $fileSystemPath = $uploader->getUploadedFilePath();
    $fileUploadTmpName = $uploader->getUploadedFileTmpName();
    $filePath = "{$parentFolderPath}/{$fileName}";
    $fileRelativePath = ltrim("{$parentFolderRelativePath}/{$fileName}", '/');
    $fileSize = $fileModel->getFileSize($fileUploadTmpName);
    $createTime = date('Y-m-d H:i:s');
    $fileModel->createFile(array(
        'parent_id' => $parentFolderId,
        'user_id' => SPC_USERID,
        'name' => $fileName,
        'description' => $description,
        'type' => 'file',
        'path' => $filePath,
        'relative_path' => $fileRelativePath,
        'size' => $fileSize,
        'created_on' => $createTime,
        'modified_on' => $createTime
    ));

    echo 1; //for uploadify
}