<?php
header('Content-type: application/json');

define('SPC_REST', true);

require_once '../../SpcEngine.php';

Spc::setSender('public');

$userModel = new Core_Model_User;
$eventModel = new Calendar_Model_Event();
$calModel = new Calendar_Model_Calendar();
$searchModel = new Core_Controller_Search();

if (!isset($_REQUEST['apiKey'])) {
    throw new Exception('Missing Api key!');
}

if (!isset($_REQUEST['method'])) {
    throw new Exception('Missing Api method!');
}

$apiKey = $_REQUEST['apiKey'];
$method = $_REQUEST['method'];

$userInfo = $userModel->getUser($apiKey, 'access_key');
$userId = $userInfo['id'];

$userPrefs = $userModel->getUserPrefs($userId);

Spc::initLanguage($userPrefs['language']);
$dict = Spc::getLanguageTranslationArray();

$userCals = $calModel->getUserCals($userId, true, true);

Spc::setUserPrefs($userPrefs);

switch ($method) {
    case 'getEvent':
        $eventId = $_REQUEST['eventId'];
        $apiRes = $eventModel->getEvent($eventId);
        break;

    case 'getEvents':
        $cals = array_keys($userCals);
        if ($_REQUEST['cals']) {
            $cals = explode(',', $_REQUEST['cals']);
        }

        $startDate = $_REQUEST['startDate'];
        $endDate = $_REQUEST['endDate'];
        $apiRes = $eventModel->getEvents('event-calendar', $cals, $startDate, $endDate);
        break;

    case 'getUserPrefs':
        $apiRes = $userPrefs;
        break;

    case 'getDict':
        $apiRes = $dict;
        break;

    case 'getCals':
        $apiRes = $userCals;
        break;

    case 'search':
        $apiRes = $searchModel->search($_REQUEST['word'], 'calendar', true);
        break;

    default:
        exit('Invalid method!');
}

$apiJsonRes = Spc::jsonEncode($apiRes, false, false);

echo $apiJsonRes;