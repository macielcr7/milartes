<script type="text/x-handlebars-template" id="spc-upcoming-events-tmpl">
    <h1>Upcoming Events</h1>
    {{#listEvents events}}
        <div>
            {{#dateEvents}}
                <div class="spc-event" {{eventData}}>{{date}} {{monthText}} {{start_time}} {{title}}</div>
            {{/dateEvents}}
        </div>
    {{/listEvents}}
</script>

<script type="text/x-handlebars-template" id="spc-event-list-tmpl">
    {{#listEvents events}}
        <div>
            <h3>{{date}}</h3>
            {{#dateEvents}}
                <div class="spc-event" {{eventData}}>{{date}} {{monthText}} {{start_time}} {{title}}</div>
            {{/dateEvents}}
        </div>
    {{/listEvents}}
</script>

<script type="text/x-handlebars-template" id="spc-event-list-tmpl-1">
    <table id="spc-event-list-1">
        <tbody>
            {{#listEvents events}}
                {{#dateEvents}}
                    <tr class="spc-event" {{eventData}}">
                        <td class="date-cell">
                            <div class="date"> <!-- border: 1px solid {{#if borderColor}} {{borderColor}} {{else}} #ddd {{/if}}; -->
                                <div class="day-name" style="background-color: {{color}}; ">{{dayShortName}}</div>
                                <div class="day">{{day}}</div>
                            </div>
                        </td>
                        <td class="main-cell">
                            <div class="title">{{title}}</div>
                            <div class="time">{{userTime}}</div>
                            <div class="description">{{description}}</div>
                        </td>
                        <td class="img-cell">
                            <div class="img-wrapper">
                                {{#if thumbPath}}
                                    <img class="img" src="{{thumbPath}}" />
                                {{/if}}
                            </div>
                        </td>
                    </tr>
                {{/dateEvents}}
            {{/listEvents}}
        </tbody>
    </table>
</script>