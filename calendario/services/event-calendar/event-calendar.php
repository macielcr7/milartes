<?php

require_once '../../SpcEngine.php';

header('Content-type: text/javascript');

$accessKey = $_GET['accessKey'];

if (!isset($_GET['accessKey'])) {
    exit('alert("SPC access key is missing!");');
}

$userModel = new Core_Model_User();
$user = $userModel->getUser($accessKey, 'access_key');
if (!$user) {
    exit("alert('Wrong access key!');");
}

#Spc::login($user['username'], false);
$spcRoot = SPC_ROOT;
$userPrefs = $userModel->getUserPrefs($user['id']);
Spc::setUserPrefs($userPrefs);

$userModel->initAppConstants();
SpcCalendar::initUserCalendars();

$appJsProps = Spc::getAppJSProps();

$tmpl = '<script type="text/x-handlebars-template" id="spc-event-list-tmpl">\
    {{#listEvents events}}\
        <div>\
            <h3>{{date}}</h3>\
            {{#dateEvents}}\
                <div class="spc-event" {{eventData}}>{{date}} {{monthText}} {{start_time}} {{title}}</div>\
            {{/dateEvents}}\
        </div>\
    {{/listEvents}}\
</script>\
<script type="text/x-handlebars-template" id="spc-event-list-tmpl-1">\
    <table id="spc-event-list-1">\
        <tbody>\
            {{#listEvents events}}\
                {{#dateEvents}}\
                    <tr class="spc-event" {{eventData}}">\
                        <td class="date-cell">\
                            <div class="date" style="border: 1px solid {{color}};"> <!-- border: 1px solid {{#if borderColor}} {{borderColor}} {{else}} #ddd {{/if}}; -->\
                                <div class="day-name" style="background-color: {{color}}; ">{{dayShortName}}</div>\
                                <div class="day">{{day}}</div>\
                            </div>\
                        </td>\
                        <td class="main-cell">\
                            <div class="title">{{title}}</div>\
                            <div class="time">{{userTime}}</div>\
                        </td>\
                        <td class="img-cell">\
                            <div class="img-wrapper">\
                                {{#if thumbPath}}\
                                    <img class="img" src="{{thumbPath}}" />\
                                {{/if}}\
                            </div>\
                        </td>\
                    </tr>\
                {{/dateEvents}}\
            {{/listEvents}}\
        </tbody>\
    </table>\
</script>';

echo <<<EOF
    function loadScript(url, callback) {
        var script = document.createElement("script");
        script.type = "text/javascript";

        if (script.readyState) {  //IE
            script.onreadystatechange = function() {
                if (script.readyState == "loaded" || script.readyState == "complete") {

                    script.onreadystatechange = null;

                    if (callback) {
                        callback();
                    }
                }
            };
        } else {  //Others
            script.onload = function() {
                if (callback) {
                    callback();
                }
            };
        }

        script.src = url + "?v=" + (new Date()).getTime();
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    function loadScripts(scripts, callback) {
        loadScript(scripts.shift(), (scripts.length ? function() { loadScripts(scripts, callback); } : callback));
    }

    window.SPCEventCalendarEnabled = true;
    window.SPCAjaxURL = "{$spcRoot}/services/event-calendar/ajax-engine.php";
    window.SPCSender = "public";
    window.SPCAccessKey = "{$accessKey}";
    window.SPC_API_KEY = "{$accessKey}";

    {$appJsProps}

    window.initSPCWidgets = function(callback) {
        window.initSPCWidgets.init = callback;
    };

    /**
     * default init method
     * if there is no init method defined by the user activates month calendar by default
     */
    window.initSPCWidgets.init = function() {
        $("#spc-event-calendar-script").parent().spcMonthCalendar();
    };

    var scripts = [
            "{$spcRoot}/js/handlebars.js",
            "{$spcRoot}/js/spc.core.js",
            "{$spcRoot}/js/spc.utils.js",
            "{$spcRoot}/js/spc.event-calendar.js"
        ];

    if (!window.jQuery) {
        scripts.unshift("{$spcRoot}/js/jquery-ui.js");
        scripts.unshift("{$spcRoot}/js/jquery.js");
    }

    loadScripts(scripts, function() {
        initSpcEventCalendar();
    });

    function initSpcEventCalendar() {
        $(function() {
        $(document.body).prepend('{$tmpl}');
            loadScript("{$spcRoot}/js/ui-i18n/jquery.ui.datepicker-" + SPC.userPrefs.language + ".js", function() {
                //load styles
                $("head")
                    .append('<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" />')
                    .append('<link rel="stylesheet" href="{$spcRoot}/css/spc.event-calendar.css?v=' + (new Date()).getTime() + '" />')
                    .append('<link rel="stylesheet" href="{$spcRoot}/css/theme/smart-white/jquery-ui.css" />');

                SPC.Calendar.Plugins.EventsCalendar.options = $.extend({
                    eventColor: "background"
                }, SPC.eventCalendarOptions);

                window.initSPCWidgets.init();
            });
        });
    }
EOF;
?>