$(function() {
    SPC.Calendar.drawInvitation = function(eventId) {
        eventId = eventId || SPC.eventId;
        SPC.ajax("calendar/event/getEvent", [eventId], function(res) {
            var event = res.event;
            event.invitees = {
                "yes": [],
                "no": [],
                "maybe": [],
                "pending": [],
                "guestCount": 0
            };

            SPC.Array.foreach(event.guests, function(i, guest) {
                event.invitees[guest["response"]].push(guest);
                event.invitees["guestCount"]++;
            });

            event.date = SPC.Calendar.Event.getPublicEventDateTitle(event)
                       + " (" + SPC.getUserPref("timezone") + ")";
            event.invitationOwnerName = SPC.USERNAME;
            event.invitationOwnerEmail = SPC.USER_EMAIL;
            event.yesCount = event.invitees.yes.length;
            event.noCount = event.invitees.no.length;
            event.maybeCount = event.invitees.maybe.length;
            event.pendingCount = event.invitees.pending.length;

            if (!SPC.invitationGridTmpl) {
                SPC.invitationGridTmpl = Handlebars.compile($("#public-invitation-grid-tmpl").html());
            }

            $("#public-invitation-content").html(SPC.invitationGridTmpl(event));

            $(".response-row :radio[value='" + SPC.inviteeResponse + "']").attr("checked", true);
        });
    };

    SPC.Calendar.drawInvitation();

    SPC.Calendar.updateInviteeResponse = function(inviteeId, response) {
        SPC.ajax("calendar/event/updateInvitationResponse", [inviteeId, response], function(res) {
            SPC.Calendar.drawInvitation();
        });
    };

    $("#public-invitation-content")
        .delegate(".response-row :radio", "click", function(e) {
            SPC.inviteeResponse = $(this).val();
            SPC.Calendar.updateInviteeResponse(SPC.inviteeId, SPC.inviteeResponse);
        });

    SPC.Calendar.updateInviteeResponse(SPC.inviteeId, SPC.inviteeResponse);
});