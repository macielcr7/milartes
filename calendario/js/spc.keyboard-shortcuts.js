/**
 * Smart PHP Calendar
 *
 * Copyright (c) 2013 Yasin Dagli, Smart PHP Calendar, All rights reserved.
 *
 * This file is protected by international laws. Reverse engineering this file is strictly prohibited.
 */
$(function() {
    if (SPC.getUserPref("kbd_shortcuts") == 0) {
        return;
    }

    //--------------------------------------------------------------------------
    // Keyboard Shortcuts
    //--------------------------------------------------------------------------

    $("#keyboard-shortcuts").click(function(e) {
        $("#keyboard-shortcuts-dialog").dialog("open");
    });

    $("#keyboard-shortcuts-dialog").dialog({
        title: SPC.translate("Keyboard shortcuts"),
        width: 700,
        height: 600
    });

    //
    // Application Global Shortcuts
    //

	//logout
	SPC.jQuery.document
		.add("input, textarea, select")
		.bind("keydown", "Ctrl+q", function(e) {
			$("#logout").click();
		});

    //SmartCancel(Escape)
    SPC.jQuery.document
        .on("keyup", function(e) {
            if (e.keyCode == 27) {
                $("[data-smart-cancel]:eq(0)").trigger("click");
            }
        });

	SPC.jQuery.document
    .add($("input, textarea", "#edit-calendar-dialog-tabs-1"))
        //save calendar
        .bind('keydown', 'Ctrl+s', function(e) {
            var $activeDialog = SPC.jQueryUI.getActiveDialog(),
                activeTabIndex = $("#edit-calendar-dialog-tabs li.ui-tabs-selected").index();

            if (!$activeDialog) {
                return false;
            }

            if ($activeDialog.attr("id") == "edit-calendar-dialog" && activeTabIndex == 0) {
                $("#edit-calendar-dialog-tabs-1-save").click();
                return false;
            }
        })
        //delete calendar
        .bind('keydown', 'Ctrl+del', function(e) {
            var $activeDialog = SPC.jQueryUI.getActiveDialog(),
                activeTabIndex = $("#edit-calendar-dialog-tabs li.ui-tabs-selected").index();

            if (!$activeDialog) {
                return false;
            }

            if ($activeDialog.attr("id") == "edit-calendar-dialog" && activeTabIndex == 0) {
                $("#edit-calendar-dialog-tabs-1-delete").click();
                return false;
            }
        })
        //delete calendar alternative
        .bind('keydown', 'Ctrl+backspace', function(e) {
            var $activeDialog = SPC.jQueryUI.getActiveDialog(),
                activeTabIndex = $("#edit-calendar-dialog-tabs li.ui-tabs-selected").index();

            if (!$activeDialog) {
                return;
            }

            if ($activeDialog.attr("id") == "edit-calendar-dialog" && activeTabIndex == 0) {
                $("#edit-calendar-dialog-tabs-1-delete").click();
                return false;
            }
        });

    //open event details
    SPC.Calendar.Dialogs.addEventDialog
        .find("input, textarea")
        .add(SPC.jQuery.document)
            //open event details
            .bind('keydown', 'Ctrl+down', function(e) {
                if (SPC.Calendar.Dialogs.addEventDialog.dialog("isOpen")) {
                    e.preventDefault();
                    $("#add-event-dialog-event-details").click();
                }
                return false;
            });

    SPC.Calendar.Dialogs.editEventDialog
        .find("input, textarea")
        .add(SPC.jQuery.document)
            //open event details
            .bind('keydown', 'Ctrl+down', function(e) {
                if (SPC.Calendar.Dialogs.editEventDialog.dialog("isOpen")) {
                    e.preventDefault();
                    $("#edit-event-dialog-event-details").click();
                }
                return false;
            });

    //
    // Calendar Keyboard Shortcuts
    //

    SPC.jQuery.document
        //pager next, prev, today
        .bind('keydown', 'right', function(e) {
            if (e.keyCode == 39) {
                $("#cal-pager-next").click();
            }
        })
        .bind('keydown', 'left', function(e) {
            if (SPC.curApp == "calendar") {
                $("#cal-pager-prev").click();
            }
        })
        .bind('keydown', 'up', function(e) {
            if (SPC.curApp == "calendar") {
                $("#cal-pager-today").click();
            }
        })

        //open views
        //resources
        .bind("keydown", "0", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("resources");
            }
        })

        //timeline-vertical
        .bind("keydown", "v", function(e) {
            if (SPC.curApp == "calendar") {
                if (SPC.Calendar.mode == "timeline") {
                    if (SPC.Calendar.Timeline.mode == "horizontal") {
                        $(".timeline-change-mode").trigger("click");
                    }
                }
            }
        })

        //timeline-horizontal
        .bind("keydown", "h", function(e) {
            if (SPC.curApp == "calendar") {
                if (SPC.Calendar.mode == "timeline") {
                    if (SPC.Calendar.Timeline.mode == "vertical") {
                        $(".timeline-change-mode").trigger("click");
                    }
                }
            }
        })

        //day
        .bind("keydown", "d", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("day");
            }
        })
        .bind("keydown", "1", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("day");
            }
        })

        //week
        .bind("keydown", "w", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("week");
            }
        })
        .bind("keydown", "2", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("week");
            }
        })

        //month
        .bind("keydown", "m", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("month");
            }
        })
        .bind("keydown", "3", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("month");
            }
        })

        //agenda
        .bind("keydown", "a", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("agenda");
            }
        })
        .bind("keydown", "4", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("agenda");
            }
        })

        //custom
        .bind("keydown", "x", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("custom");
            }
        })
        .bind("keydown", "5", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("custom");
            }
        })

        //year
        .bind("keydown", "y", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("year");
            }
        })
        .bind("keydown", "6", function(e) {
            if (SPC.curApp == "calendar") {
                SPC.Calendar.openView("year");
            }
        })

        //refresh calendar
        .bind("keydown", "r", function(e) {
            SPC.Calendar.refresh();
        })

        //create a new event or appointment
        .bind("keydown", "c", function(e) {
            var createAppt = false;
            if (SPC.curApp == "contacts") {
                //create appt if appointments tab selected
                if ($("#contacts-nav-buttons input:radio:checked").attr("id") == "contacts-nav-appointments") {
                    createAppt = true;

                //create a new contact if contact-details tab selected
                } else {
                    $("#sidebar-add-contact").trigger("click");
                    return false;
                }
            }
            SPC.Calendar.Event.smartCreate(createAppt);
            return false;
        })

        //create calendar
        .bind("keydown", "Ctrl+x", function(e) {
            if (SPC.curApp == "calendar") {
                $("#create-calendar").trigger("click");
            } else if (SPC.curApp == "contacts") {
                $(".add-contact-group:eq(0)").trigger("click");
            }
        })

        //print calendars
        .bind("keydown", "p", function(e) {
            window.location.href = $("#print-calendar").attr("href");
            return false;
        })

        //calendar settings
        .bind("keydown", "s", function(e) {
            $("#system-settings").trigger("click");
            return false;
        })

        //open users-management dialog;
        .bind("keydown", "u", function(e) {
            $("#system-users").trigger("click");
            return false;
        });

    //edit current contact
    SPC.jQuery.document
        .on("keyup", function(e) {
            if (e.keyCode == 69
                    && $(".display-contact-form").length
                    && e.target.tagName != "INPUT") {

                $(".contact-form-edit-contact:eq(0)").trigger("click");
            }
        });

    //SmartDelete ??
    /*
    SPC.Calendar.Dialogs.editEventDialog
        .find("input, textarea")
        .add(SPC.jQuery.document)
            //delete on ctrl+delete
            .bind('keydown', 'Ctrl+del', function(e) {
                if (SPC.Calendar.Dialogs.editEventDialog.dialog("isOpen")) {
                    SPC.editEventDialogButtons[SPC.translate("Delete")]();
                }
            })
            //delete on ctrl+backspace
            .bind('keydown', 'Ctrl+backspace', function(e) {
                if (SPC.Calendar.Dialogs.editEventDialog.dialog("isOpen")) {
                    SPC.editEventDialogButtons[SPC.translate("Delete")]();
                }
            });
*/

        //SmartSave
        SPC.jQuery.document
            .on('keydown', function(e) {
                //Ctrl+s or Ctrl+return
                if (e.ctrlKey && (e.keyCode == 8 || e.keyCode == 46)) {
                    if (SPC.Calendar.Dialogs.editEventDialog.dialog("isOpen")) {
                        SPC.editEventDialogButtons[SPC.translate("Delete")]();
                    } else if ($("[data-smart-delete]").length) {
                        $("[data-smart-delete]:eq(0)").trigger("click");
                    }


                }
            });

        //SmartSave
        SPC.jQuery.document
            .on('keydown', function(e) {
                //Ctrl+s or Ctrl+return
                if (e.ctrlKey && (e.keyCode == 83 || e.keyCode == 13)) {
                    if (SPC.SmartSave() === false) {
                        return false;
                    }
                }
            });

        /**
         * Saves the current dialog
         */
        SPC.SmartSave = function($smartSaveBtn) {
            var $activeDialog = SPC.jQueryUI.getActiveDialog();
            if (!$activeDialog) {
                $smartSaveBtn = $smartSaveBtn || $("[data-smart-save]:eq(0)");
                if ($smartSaveBtn.length) {
                    $smartSaveBtn.trigger("click");
                }
                return false;
            }

            var activeDialogButtons = $activeDialog.dialog("option", "buttons"),
                activeDialogButtonLabels = "",
                originalKey,
                saveFunction,
                re,
                smartSave = false;

                SPC.Array.foreach(activeDialogButtons, function(btnLabel, fn) {
                    activeDialogButtonLabels += btnLabel;
                });

                SPC.Array.foreach(SPC.SmartSave.saveKeywords, function(i, smartSaveKeyword) {
                    re = new RegExp(SPCi18n[smartSaveKeyword], "ig");

                    SPC.Array.foreach(activeDialogButtons, function(i18nBtnLabel, btnFn) {
                        if (re.test(i18nBtnLabel)) {
                            btnFn();
                            smartSave = true;
                            return false;
                        }
                    });

                    if (smartSave) {
                        return false;
                    }
                });

            return false;
        };

    SPC.SmartSave.saveKeywords = ["save", "done", "ok", "create", "edit", "Create Event"];
});