<?php

/**
 * Smart PHP Calendar 3.2
 *
 * @copyright  Copyright (c) 2008-2012 Yasin Dagli (http://www.smartphpcalendar.com)
 * @license    http://www.smartphpcalendar.com/license
 */

//------------------------------------------------------------------------------
// Smart PHP Calendar - System Settings
//------------------------------------------------------------------------------

define('SPC_VER', '3.5');

define('SPC_VERSION', 'PREMIUM');

/**
 * Application environment
 * values: development | production
 */
define('SPC_ENV', 'development');

//------------------------------------------------------------------------------
// Domain Name and License Key
//------------------------------------------------------------------------------

// Exact calendar address
// example: http://yourhost.com/your-calendar-directory (without trailing slash)
define('SPC_ROOT', 'http://143.198.188.210/calendario');

// domain name | without protocol (http, https), www and subdomain
// example: yourhost.com
define('SPC_DOMAIN', 'Maciel');

define('SPC_LICENSE_KEY', '5038W3060B2828F2252G2644H19100S540975687563162N1836K2252J19100F3060I1388S11104L2644L1224Q162I2828B3060F19100H2644I1768I27108B3060I1388M2252L164');

//------------------------------------------------------------------------------
// Database Configuration
//------------------------------------------------------------------------------

define('SPC_DB_HOST', '143.198.188.210');
define('SPC_DB_USERNAME', 'root');
define('SPC_DB_PASSWORD', 'Fsj@2144');
define('SPC_DB_DBNAME', 'smartphpcalendar');

//use Smart PHP Calendar as Wordpress Plugin
#define('WP_PLUGIN', true);

//------------------------------------------------------------------------------
// Default timezone
//------------------------------------------------------------------------------

// when superuser or admin create user this will be user's default timezone
// users can change it in their calendar settings
define('SPC_DEFAULT_TIMEZONE', 'America/Sao_Paulo');

#define('EVENTS_CAL_USER', 'Jenny');

//------------------------------------------------------------------------------
// Calendar Sharing
//------------------------------------------------------------------------------

//show input box or usernames dropdown menu in calendar sharing dialog

// none: text input box, you have to type username
// group: usernames dropdown menu that shows all usernames in a group
// all: usernames dropdown menu that shows all usernames

define('CAL_SHARE_SHOW_USERNAME', 'all');

//------------------------------------------------------------------------------
// Calendar Import/Export
//------------------------------------------------------------------------------

//ical default event privacy
// private: all imported events will be private
// public: all imported events will be public
define('ICAL_IMPORT_EVENT_PRIVACY', 'private');

//------------------------------------------------------------------------------
// Email and Popup Reminders
//------------------------------------------------------------------------------

//reminder count for each event
define('EVENT_REMINDER_COUNT', 10);

// use phpmailer for sending email notifications
// if false native PHP mail() function will be used
define('USE_PHP_MAILER', true);

define('EMAIL_REMINDER_FROM', 'noreply@smartphpcalendar.com');
define('EMAIL_REMINDER_SUBJECT', 'Smart PHP Calendar');

//PHP Mailer Configurations
//default configured for Gmail
define('PHP_MAILER_PROTOCOL', 'smtp');
define('PHP_MAILER_SECURITY', 'ssl');
define('PHP_MAILER_HOST', 'smtp.gmail.com');
define('PHP_MAILER_PORT', 465);
define('PHP_MAILER_USERNAME', '');
define('PHP_MAILER_PASSWORD', '');

//------------------------------------------------------------------------------
// Signup
//------------------------------------------------------------------------------

//email template to be sent after signup
define('SIGNUP_EMAIL_TITLE', 'Smart PHP Calendar - Signup');
define(
    'SIGNUP_EMAIL_TMPL',
    'Thank you for registering. Please click the following link to activate your account: ' . SPC_ROOT . '/accept.php?aa=%accessKey%'
);
