<?php

/**
 * Smart PHP Calendar - Update Kit
 * updates from 3.x to 3.2
 *
 * Copyright (c) 2013 Yasin Dagli, smartphpcalendar.com
 */
require 'SpcEngine.php';

$coreUserModel = new Core_Model_System();

$coreUserModel->updateApp();

echo 'Smart PHP Calendar successfully updated.';