<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'usuarios_empresas_sistema';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM usuarios_empresas_sistema
				WHERE controle=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			
			$pdo = $connection->prepare("
				SELECT empresas_sistema.cod_empresa AS id, CONCAT(UPPER(empresas_sistema.razao_social), ' - ', UPPER(empresas_sistema.nome_fantasia)) AS descricao 
					FROM empresas_sistema 
				WHERE empresas_sistema.ativo='S' 
				AND NOT EXISTS 
					(SELECT usuarios_empresas_sistema.*
						FROM usuarios_empresas_sistema
						WHERE usuarios_empresas_sistema.id_usuario = '$_POST[id_usuario]' AND usuarios_empresas_sistema.id_empresa = empresas_sistema.cod_empresa
					)
				ORDER BY descricao
			");
			$pdo->execute();
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			$buscar->setBusca(array('id_usuario', 'usuarios_empresas_sistema.id_usuario'), $_POST['id_usuario']);
			
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('id_empresa', 'usuarios_empresas_sistema.id_empresa'), $_POST['id_empresa']);
			}
			
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("
				SELECT count(*) as total 
				FROM usuarios_empresas_sistema INNER JOIN usuarios ON
					(usuarios_empresas_sistema.id_usuario=usuarios.id) INNER JOIN empresas_sistema ON
					(usuarios_empresas_sistema.id_empresa=empresas_sistema.cod_empresa) 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$pdo = $connection->prepare("
				SELECT usuarios_empresas_sistema.data_cadastro, usuarios_empresas_sistema.id_empresa
					DATE_FORMAT(usuarios_empresas_sistema.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(usuarios_empresas_sistema.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					CONCAT(UPPER(empresas_sistema.razao_social), ' - ', UPPER(empresas_sistema.nome_fantasia)) AS razao_social  
				FROM usuarios_empresas_sistema 
				INNER JOIN usuarios ON (usuarios_empresas_sistema.id_usuario=usuarios.id) 
				INNER JOIN empresas_sistema ON (usuarios_empresas_sistema.id_empresa=empresas_sistema.cod_empresa) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>