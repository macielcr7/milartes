<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'usuarios_empresas_sistema';
	try {
		
		if($_POST['action'] == 'EDITAR'){
		
			$user->getAcao($tabela, 'editar');
		
			$pdo = $connection->prepare("
					UPDATE usuarios_empresas_sistema SET 
							id_usuario = ?,
							id_empresa = ?,
							alterado_por = ?
 					WHERE controle = ?
			");
			$params = array(
				$_POST['id_usuario'],
				$_POST['id_empresa'],
				$user_id,
				$_POST['controle']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
		
			$user->getAcao($tabela, 'adicionar');
		
			$pdo = $connection->prepare("
				INSERT INTO usuarios_empresas_sistema 
					(
						id_usuario,
						id_empresa,
						cadastrado_por,
						data_cadastro,
						alterado_por
					) 
				VALUES 
					(
						?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['id_usuario'],
				$_POST['id_empresa'],
				$user_id,
				date('Y-m-d H:i:s'),
				$user_id
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}