<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'clientes_endereco';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM clientes_endereco
				WHERE controle=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			$buscar->setBusca(array('cod_cliente', 'clientes_endereco.cod_cliente'), $_POST['cod_cliente']);
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('estado', 'clientes_endereco.estado'), $_POST['estado']);
				$buscar->setBusca(array('cidade', 'clientes_endereco.cidade'), $_POST['cidade']);
				$buscar->setBusca(array('bairro', 'clientes_endereco.bairro'), $_POST['bairro']);
				$buscar->setBusca(array('logradouro', 'clientes_endereco.logradouro'), $_POST['logradouro']);
				$buscar->setBusca(array('num_end', 'clientes_endereco.num_end'), $_POST['num_end'], 'like');
				$buscar->setBusca(array('complemento', 'clientes_endereco.complemento'), $_POST['complemento'], 'like');
				$buscar->setBusca(array('loteamento', 'clientes_endereco.loteamento'), $_POST['loteamento'], 'like');
				$buscar->setBusca(array('cep', 'clientes_endereco.cep'), $_POST['cep'], 'like');
				$buscar->setBusca(array('cx_postal', 'clientes_endereco.cx_postal'), $_POST['cx_postal'], 'like');
			}
			
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("
				SELECT count(*) as total 
				FROM clientes_endereco LEFT JOIN correios_estados ON
					(clientes_endereco.estado=correios_estados.uf) 
					LEFT JOIN correios_localidades ON
					(clientes_endereco.cidade=correios_localidades.id) LEFT JOIN correios_bairros ON
					(clientes_endereco.bairro=correios_bairros.id) LEFT JOIN correios_enderecos ON
					(clientes_endereco.logradouro=correios_enderecos.id) 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$pdo = $connection->prepare("
				SELECT clientes_endereco.controle, clientes_endereco.cep, clientes_endereco.cx_postal,
					UPPER(clientes_endereco.estado) AS estado, 
					UPPER(clientes_endereco.tipo_endereco) AS tipo_endereco, 
					UPPER(clientes_endereco.ponto_referencia) AS ponto_referencia, 
					clientes_endereco.cidade, clientes_endereco.bairro, clientes_endereco.logradouro, clientes_endereco.num_end, clientes_endereco.data_cadastro, 
						CASE WHEN
							clientes_endereco.complemento != '' THEN CONCAT(UPPER(correios_enderecos.tipo), ' ', UPPER(correios_enderecos.nome), ', ', num_end, ' (', UPPER(clientes_endereco.complemento), ')') 
						ELSE
							CONCAT(UPPER(correios_enderecos.tipo), ' ', UPPER(correios_enderecos.nome), ', ', num_end)
						END AS nome,
					CONCAT(UPPER(correios_localidades.loc_nome), ' / ', correios_estados.uf) AS loc_nome, 
						CASE WHEN
							clientes_endereco.loteamento != '' THEN CONCAT(UPPER(correios_bairros.bairro_nome), ' (', UPPER(clientes_endereco.loteamento), ')') 
						ELSE
							UPPER(correios_bairros.bairro_nome)
						END AS bairro_nome
				FROM clientes_endereco 
				LEFT JOIN correios_estados ON (clientes_endereco.estado=correios_estados.uf) 
				LEFT JOIN correios_localidades ON (clientes_endereco.cidade=correios_localidades.id) 
				LEFT JOIN correios_bairros ON (clientes_endereco.bairro=correios_bairros.id) 
				LEFT JOIN correios_enderecos ON (clientes_endereco.logradouro=correios_enderecos.id) 

				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;

			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>