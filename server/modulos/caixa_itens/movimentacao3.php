<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
	@session_start();
	header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Impressão Caixa</title>
	<link rel="stylesheet" type="text/css" href="../../../resources/css/sandri.css"/>
</head>
<body bgcolor="#FFFFFF">

<?php
require('../../autoLoad.php');
$pdo = $connection->prepare("SET lc_time_names = 'pt_BR'");
$pdo->execute();

//$nro_caixa = "103,104";
$nro_caixa = "103,105,106,107";

$where = "";
$where2 = "";
$where3 = "";
$ids = explode(',', $nro_caixa);
$total_ids = count($ids);
$contador = 1;

foreach ($ids as $index => $id) {
	if (empty($where)) {
		if ($total_ids > 1) {
			$where .= "(";
			$where2 .= "(";
			$where3 .= "(";
		}

		$where .= "caixa_nro=".$id;
		$where2 .= "CI.caixa_nro=".$id;
		$where3 .= "cx_nro=".$id;
	}
	else {
		$where .=" OR caixa_nro=".$id;
		$where2 .=" OR CI.caixa_nro=".$id;
		$where3 .=" OR cx_nro=".$id;

		if ($contador == $total_ids) {
			$where .= ")";
			$where2 .= ")";
			$where3 .= ")";
		}
	}
	$contador++;
}





echo "<br><P>RESUMO DE VENDAS</P>";
//RESUMO DE VENDAS
$total_vendas = 0;
?>
<table width="100%" align="left" border="0" cellpadding="10" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
		<tr bgcolor="#F4F4F4">
			<td width="40" class="titulo_grid4" align="center">ID</td>
			<td width="120" class="titulo_grid4" align="center">DATA/HORA</td>
			<td width="190" class="titulo_grid4" align="left">FORMA PAGTO</td>
			<td width="60" class="titulo_grid4" align="right">VALOR</td>
			<td class="titulo_grid4" align="left">OBSERVAÇÕES</td>
		</tr>
<?php
//CI.data_hora > '2017-04-03 17:00:00' AND 
	$sql = "SELECT CI.id, FP.descricao, CONCAT(DATE_FORMAT(CI.data_hora, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(CI.data_hora, '%H:%i:%s')) AS data_hora, CI.valor, CI.observacoes 
			FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto)
			WHERE ".$where2." AND (CI.tipo_lancamento=2 OR CI.tipo_lancamento=3) AND CI.cancelado='N' ORDER BY CI.observacoes ASC, CI.id ASC";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetchAll(PDO::FETCH_OBJ);
	foreach ($query as $key => $value) {
		$rows[$key] = $value;
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="center"><?php echo $rows[$key]->id; ?></td>
			<td align="center"><?php echo $rows[$key]->data_hora; ?></td>
			<td align="left"><?php echo $rows[$key]->descricao; ?></td>
			<td align="right"><?php echo number_format($rows[$key]->valor, 2, ',', '.');?></td>
			<td align="left"><?php echo $rows[$key]->observacoes; ?></td>
		</tr>
<?php
		$total_vendas = $total_vendas + $rows[$key]->valor;
	}
?>
		<tr bgcolor="#F4F4F4">
			<td colspan="3" class="titulo_grid4" align="right">&nbsp;</td>
			<td colspan="2" class="titulo_grid4" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($total_vendas, 2, ',', '.'); ?></td>
		</tr>

</table>
&nbsp;
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="33%" valign="top">
			<table width="500" align="left" border="0" cellpadding="10" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
				<tr bgcolor="#F4F4F4">
					<td width="40" class="titulo_grid4" align="center">VENDAS/ADIANTAMENTOS - FORMA DE PAGAMENTO</td>
					<td class="titulo_grid4" align="center">VALOR</td>
				</tr>
		<?php
			$total_rec_vendas = 0;
			$sql = "SELECT SUM(CI.valor) AS valor, FP.descricao FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto) WHERE ".$where2." AND (CI.tipo_lancamento=2 OR CI.tipo_lancamento=3) AND CI.cancelado='N' GROUP BY CI.forma_pagto ORDER BY FP.descricao ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			foreach ($query as $key => $value) {
				$rows[$key] = $value;
		?>
				<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
					<td width="350" align="left"><?php echo $rows[$key]->descricao; ?></td>
					<td align="right"><?php echo number_format($rows[$key]->valor, 2, ',', '.');?></td>
				</tr>
		<?php
				$total_rec_vendas += $rows[$key]->valor;
			}
		?>
				<tr  bgcolor="#F8F8F8">
					<td align="right"><b>TOTAL RECEBIDO</b></td>
					<td align="right"><b><?php echo number_format($total_rec_vendas, 2, ',', '.');?></b></td>
				</tr>
			</table>
		</td>

		<td width="33%" valign="top">
			<table width="500" align="left" border="0" cellpadding="10" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
				<tr bgcolor="#F4F4F4">
					<td width="40" class="titulo_grid4" align="center">SUPRIMENTOS - FORMA DE PAGAMENTO</td>
					<td class="titulo_grid4" align="center">VALOR</td>
				</tr>
		<?php
			$total_rec_vendas = 0;
			$sql = "SELECT SUM(CI.valor) AS valor, FP.descricao FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto) WHERE ".$where2." AND CI.tipo_lancamento=0 AND CI.cancelado='N' GROUP BY CI.forma_pagto ORDER BY FP.descricao ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			foreach ($query as $key => $value) {
				$rows[$key] = $value;
		?>
				<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
					<td width="300" align="left"><?php echo $rows[$key]->descricao; ?></td>
					<td align="right"><?php echo number_format($rows[$key]->valor, 2, ',', '.');?></td>
				</tr>
		<?php
				$total_rec_vendas += $rows[$key]->valor;
			}
		?>
				<tr  bgcolor="#F8F8F8">
					<td align="right" width="350"><b>TOTAL RECEBIDO</b></td>
					<td align="right"><b><?php echo number_format($total_rec_vendas, 2, ',', '.');?></b></td>
				</tr>
			</table>
		</td>

		<td width="33%" valign="top">&nbsp;</td>
	</tr>
</table>


<br>&nbsp;<br><br><br>
	<table width="100%" align="left" border="0" cellpadding="10" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
		<tr bgcolor="#F4F4F4">
			<td width="40" class="titulo_grid4" align="center">ID</td>
			<td width="120" class="titulo_grid4" align="center">DATA/HORA</td>
			<td width="190" class="titulo_grid4" align="left">FORMA PAGTO</td>
			<td width="60" class="titulo_grid4" align="right">VALOR</td>
			<td class="titulo_grid4" align="left">OBSERVAÇÕES</td>
		</tr>
<?php
	$sql = "SELECT CI.id, FP.descricao, CONCAT(DATE_FORMAT(CI.data_hora, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(CI.data_hora, '%H:%i:%s')) AS data_hora, CI.valor, CI.observacoes 
			FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto)
			WHERE ".$where2." AND CI.tipo_lancamento=1 AND CI.cancelado='N' ORDER BY CI.id ASC";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetchAll(PDO::FETCH_OBJ);
	$total_ret = 0;
	//echo $sql;
	foreach ($query as $key => $value) {
		$rows[$key] = $value;
		$total_ret = $total_ret + $rows[$key]->valor;
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="center"><?php echo $rows[$key]->id; ?></td>
			<td align="center"><?php echo $rows[$key]->data_hora; ?></td>
			<td align="left"><?php echo $rows[$key]->descricao; ?></td>
			<td align="right"><?php echo number_format($rows[$key]->valor, 2, ',', '.');?></td>
			<td align="left"><?php echo $rows[$key]->observacoes; ?></td>
		</tr>
<?php
	}
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="right" colspan="3"><b>TOTAL RETIRADO</b></td>
			<td align="left" colspan="2"><b><?php echo number_format($total_ret, 2, ',', '.');?></b></td>
		</tr>
	</table>

<?php
//FIM RETIRADAS
?>



</body>
</html>