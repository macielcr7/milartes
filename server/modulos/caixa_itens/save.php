<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'caixa_itens';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		if (isset($_POST['data_venda_cartao'])) {
			$data_venda_cartao = implode('-', array_reverse(explode('/', $_POST['data_venda_cartao'])));
		}

		//verifica se a forma de pagto é a vista ou a prazo
		$sql = "SELECT tipo, categoria FROM formas_pagto WHERE id = '{$_POST['forma_pagto']}'";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetch(PDO::FETCH_OBJ);
		$vista_prazo = $query->tipo;
		$categoria = $query->categoria;
		$observacoes = trim(mb_strtoupper($_POST['observacoes'], 'UTF-8'));

		if($_POST['action'] == 'EDITAR_VALORES_SUPRIMENTO'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE caixa_itens SET 
							valor = ?,
							forma_pagto = ?,
							nro_parcelas_cartao = ?,
							tipo_venda = ?,
							observacoes = ?,
							caixa_nro = ? 
					WHERE id = ?
			");
			$params = array(
				$_POST['valor'],
				$_POST['forma_pagto'],
				$_POST['nro_parcelas'],
				$query->tipo,
				$observacoes,
				$_POST['caixa_nro'],
				$_POST['id']
			);
			$pdo->execute($params);
			$valor_cartao = $_POST['valor'];
			$tipo_lancamento_cartao = 3;
		}
		else if($_POST['action'] == 'EDITAR_VALORES_RETIRADA'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE caixa_itens SET 
							valor = ?,
							forma_pagto = ?,
							nro_parcelas_cartao = ?,
							tipo_venda = ?,
							observacoes = ?,
							caixa_nro = ? 
					WHERE id = ?
			");
			$params = array(
				-$_POST['valor_a_retirar'],
				$_POST['forma_pagto'],
				$_POST['nro_parcelas'],
				$query->tipo,
				$observacoes,
				$_POST['caixa_nro'],
				$_POST['id']
			);
			$pdo->execute($params);
			$valor_cartao = $_POST['valor_a_retirar'];
			$tipo_lancamento_cartao = 4;
		}
		else if($_POST['action'] == 'ALTERA_VALOR_VENDA'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE caixa_itens SET 
							valor = ?,
							forma_pagto = ?,
							nro_parcelas_cartao = ?,
							tipo_venda = ?,
							observacoes = ?,
							caixa_nro = ? 
					WHERE id = ?
			");
			$params = array(
				$_POST['valor_venda'],
				$_POST['forma_pagto'],
				$_POST['nro_parcelas'],
				$query->tipo,
				$observacoes,
				$_POST['caixa_nro'],
				$_POST['id']
			);
			$pdo->execute($params);
			$valor_cartao = $_POST['valor_venda'];
			$tipo_lancamento_cartao = 1;

			//altera na tabela das vendas_pagtos tbm
			$pdo2 = $connection->prepare("
					UPDATE vendas_pagamentos SET 
							valor_pago = ?,
							forma_pagto = ?,
							nro_parcelas_cartao = ?,
							observacao = ? 
					WHERE id = ?
			");
			$params2 = array(
				$_POST['valor_venda'],
				$_POST['forma_pagto'],
				$_POST['nro_parcelas'],
				$observacoes,
				$_POST['id_tipo']
			);
			$pdo2->execute($params2);

			//se for cartão ajusta os valores nos gestao antigo...
			//sandriiii
		}
		else if ($_POST['action'] == 'ADD_VALORES'){
			//tipo lancamento == 0 SUPRIMENTO (ADICIONAR DINHEIRO) / tipo 1 == retirada
			$user->getAcao($tabela, 'add_din_caixa');
			$pdo = $connection->prepare("
				INSERT INTO caixa_itens 
					(
						caixa_nro,
						data_hora,
						tipo_lancamento,
						cadastrado_por,
						id_tipo,
						valor,
						cancelado,
						forma_pagto,
						nro_parcelas_cartao,
						tipo_venda,
						observacoes

					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['caixa_nro'],
				$data_hora_atual,
				0,
				$user_id,
				0,
				$_POST['valor'],
				'N',
				$_POST['forma_pagto'],
				$_POST['nro_parcelas'],
				$query->tipo,
				mb_strtoupper($observacoes, 'UTF-8')
			);
			$pdo->execute($params);
			$valor_cartao = $_POST['valor'];
			$tipo_lancamento_cartao = 3;
		}
		else if ($_POST['action'] == 'RETIRA_VALORES'){
			$user->getAcao($tabela, 'add_din_caixa');
			$pdo = $connection->prepare("
				INSERT INTO caixa_itens 
					(
						caixa_nro,
						data_hora,
						tipo_lancamento,
						cadastrado_por,
						id_tipo,
						valor,
						cancelado,
						forma_pagto,
						nro_parcelas_cartao,
						tipo_venda,
						observacoes
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['caixa_nro'],
				$data_hora_atual,
				1,
				$user_id,
				0,
				-$_POST['valor_a_retirar'],
				'N',
				$_POST['forma_pagto'],
				$_POST['nro_parcelas'],
				$query->tipo,
				mb_strtoupper($observacoes, 'UTF-8')
			);
			$pdo->execute($params);
			$valor_cartao = $_POST['valor_a_retirar'];
			$tipo_lancamento_cartao = 4;
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

		if ($categoria == 3) {
			//É Cartão
			/*
			$sql2 = "SELECT cod_venda FROM vendas_pagamentos WHERE id='{$_POST['id_tipo']}'";
			$pdo2 = $connection->prepare($sql2);
			$pdo2->execute();
			$BB = $pdo2->fetch(PDO::FETCH_OBJ);
			*/

			if (empty($_POST['id_tipo'])){
				//$BB->cod_venda = 0;
				$_POST['id_tipo'] = 0;
			}

			//pega calcula o valor que vai ficar a parcela liquida
			$sql3 = "SELECT * FROM contratos_cartao INNER JOIN formas_pagto ON (formas_pagto.cod_convenio=contratos_cartao.id) WHERE formas_pagto.id='{$_POST['forma_pagto']}'";
			$pdo3 = $connection->prepare($sql3);
			$pdo3->execute();
			$CC = $pdo3->fetch(PDO::FETCH_OBJ);

			$taxa = 'taxa'.$_POST['nro_parcelas'];

			$valor_da_parcela = $valor_cartao / $_POST['nro_parcelas'];
			$valor_da_parcela = round($valor_da_parcela, 2);

			$valor_venda_liquida = $valor_cartao - ($CC->$taxa * $valor_cartao / 100);
			$valor_parcela_liquida = $valor_da_parcela - ($CC->$taxa * $valor_da_parcela / 100);

			//Tipos de Lancamentos: 1=Venda, 2=Adiantamento, 3=Suprimento, 4=Retiradas, 9=Taxas
			$pdo = $connection->prepare("
			INSERT INTO cartoes_vendas 
				(
					tipo_lancamento,
					id_tipo_lancamento,
					valor_venda_bruta,
					numero_parcelas,
					valor_venda_liquida,
					cod_cartao,
					observacoes,
					data_venda,
					estornada,
					data_estorno,
					motivo_estorno,
					tipo_taxa,
					cod_empresa,
					data_cadastro,
					cadastrado_por,
					alterado_por,
					data_alteracao
				) 
			VALUES 
				(
					?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
				)
			");
			$params = array(
				$tipo_lancamento_cartao,
				$_POST['id_tipo'],
				$valor_cartao,
				$_POST['nro_parcelas'],
				$valor_venda_liquida,
				$_POST['forma_pagto'],
				$observacoes,
				$data_venda_cartao,
				'N',
				NULL,
				NULL,
				'N',
				$_COOKIE['id_empresa_atual'],
				$data_hora_atual,
				$user_id,
				$user_id,
				$data_hora_atual
			);
			$pdo->execute($params);
			$ultimo = $connection->lastInsertId();

			$dias_previsto = 0;
			//$partes = explode("-", date('Y-m-d'));
			$partes = explode("-", $data_venda_cartao);
			for ($i = 1; $i <= $_POST['nro_parcelas']; $i++) {
				$texto_descricao = "Parcela ".$i." de ".$_POST['nro_parcelas'];

				if ($_POST['nro_parcelas'] == 1) {
					$dias_previsto = $CC->dias_credito_parc_vista;
					$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
					$data_prevista_recebto = feriado($dt_prevista);
				}
				else {
					$dias_previsto = $CC->dias_credito_parc_estab;

					if ($CC->tipo_calculo == "MASTERCARD") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							//pega o dia do mes que recebeu a primeira parcela
							$partes3 = explode("-",$dt_prevista);
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes3[1] + ($i - 1), $partes3[2], $partes3[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
					elseif ($CC->tipo_calculo == "VISA") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
					elseif ($CC->tipo_calculo == "HIPERCARD") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							//pega o dia do mes que recebeu a primeira parcela
							$partes3 = explode("-",$dt_prevista);
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes3[1] + ($i - 1), $partes3[2], $partes3[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
					elseif ($CC->tipo_calculo == "AMERICAN") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
					elseif ($CC->tipo_calculo == "BANRICOMPRAS") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
					elseif ($CC->tipo_calculo == "NILSON") {
						$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
						$data_prevista_recebto = feriado($dt_prevista);
					}
					elseif ($CC->tipo_calculo == "CONSTRUCARD") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
					elseif ($CC->tipo_calculo == "SANTANDER") {
						if ($i == 1) {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
			
							//pega o dia do mes que recebeu a primeira parcela
							$partes3 = explode("-",$data_prevista_recebto);
						}
						else {
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
					}
				}


				$pdo = $connection->prepare("
				INSERT INTO cartoes_vendas_itens 
					(
						cod_venda_cartao,
						descricao,
						parcela,
						valor_parcela_bruta,
						valor_parcela_liquida,
						data_prevista_recebto
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?
					)
				");
				$params = array(
					$ultimo,
					$texto_descricao,
					$i,
					$valor_da_parcela,
					$valor_parcela_liquida,
					$data_prevista_recebto
				);
				$pdo->execute($params);
			}
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>