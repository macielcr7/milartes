<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
	@session_start();
	header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Impressão Caixa</title>
	<link rel="stylesheet" type="text/css" href="../../../resources/css/sandri.css"/>
</head>
<body bgcolor="#FFFFFF">

<?php
require('../../autoLoad.php');
$pdo = $connection->prepare("SET lc_time_names = 'pt_BR'");
$pdo->execute();

$nro_caixa = $_GET['nro_caixa'];

if($_GET['tipo'] == "resumo"){
	$sql = "SELECT saldo_inicial, CONCAT(DATE_FORMAT(data_abertura, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(data_abertura, '%H:%i:%s')) AS data_abertura FROM caixas WHERE cx_nro = '{$nro_caixa}'";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$caixa = $pdo->fetch(PDO::FETCH_OBJ);

	//verificando Suprimentos (valores adicionados manualmente como troco)
	$sql1 = "SELECT SUM(valor) AS total_adicionado FROM caixa_itens WHERE caixa_nro='{$nro_caixa}' AND tipo_lancamento=0 AND cancelado='N'";
	$pdo1 = $connection->prepare($sql1);
	$pdo1->execute();
	$valor1 = $pdo1->fetch(PDO::FETCH_OBJ);

	//verificando Retiradas
	$sql2 = "SELECT SUM(valor) AS total_retirado FROM caixa_itens WHERE caixa_nro='{$nro_caixa}' AND tipo_lancamento=1 AND cancelado='N'";
	$pdo2 = $connection->prepare($sql2);
	$pdo2->execute();
	$valor2 = $pdo2->fetch(PDO::FETCH_OBJ);

	//verificando os Cancelamentos
	$sql3 = "SELECT SUM(valor) AS total_cancelado FROM caixa_itens WHERE caixa_nro='{$nro_caixa}' AND cancelado='S'";
	$pdo3 = $connection->prepare($sql3);
	$pdo3->execute();
	$valor3 = $pdo3->fetch(PDO::FETCH_OBJ);

	//verificando Vendas
	$sql4 = "SELECT SUM(valor) AS total_vendas FROM caixa_itens WHERE caixa_nro='{$nro_caixa}' AND tipo_lancamento=2 AND cancelado='N'";
	$pdo4 = $connection->prepare($sql4);
	$pdo4->execute();
	$valor4 = $pdo4->fetch(PDO::FETCH_OBJ);

	//verificando Adiantamentos
	$sql5 = "SELECT SUM(valor) AS total_adiantamentos FROM caixa_itens WHERE caixa_nro='{$nro_caixa}' AND tipo_lancamento=3 AND cancelado='N'";
	$pdo5 = $connection->prepare($sql5);
	$pdo5->execute();
	$valor5 = $pdo5->fetch(PDO::FETCH_OBJ);

	//verificando as Devoluções
	$sql6 = "SELECT SUM(valor) AS total_devolucoes FROM caixa_itens WHERE caixa_nro='{$nro_caixa}' AND tipo_lancamento=4 AND cancelado='N'";
	$pdo6 = $connection->prepare($sql6);
	$pdo6->execute();
	$valor6 = $pdo6->fetch(PDO::FETCH_OBJ);

	$valor_final_em_caixa = ($caixa->saldo_inicial+$valor1->total_adicionado+$valor2->total_retirado+$valor4->total_vendas+$valor5->total_adiantamentos+$valor6->total_devolucoes);
?>
	<table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="texto3">
		<tr>
			<td align="left" valign="middle">
				<table width="400" align="left" border="0" cellpadding="10" cellspacing="1" class="texto3" bgcolor="#CCCCCC">
					<tr bgcolor="#F4F4F4">
						<td colspan="2" class="titulo_grid3" align="center">CAIXA ABERTO EM: <?php echo $caixa->data_abertura; ?></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left"><font color="red">Cancelamentos</font></td>
						<td align="right" width="80">&nbsp;<font color="red"><?php echo number_format($valor3->total_cancelado, 2, ',', '.');?></font></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left">Saldo Inicial</td>
						<td align="right">&nbsp;<?php echo number_format($caixa->saldo_inicial, 2, ',', '.');?></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left">+ Dinheiro Adicionado</td>
						<td align="right">&nbsp;<?php echo number_format($valor1->total_adicionado, 2, ',', '.');?></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left">- Dinheiro Retirado</td>
						<td align="right">&nbsp;<?php echo number_format($valor2->total_retirado, 2, ',', '.');?></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left">Total de Vendas</td>
						<td align="right">&nbsp;<?php echo number_format($valor4->total_vendas, 2, ',', '.');?></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left"><font color="red">Total de Devolvido</font></td>
						<td align="right">&nbsp;<font color="red"><?php echo number_format($valor6->total_devolucoes, 2, ',', '.');?></font></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left">Total de Adiantamentos</td>
						<td align="right">&nbsp;<?php echo number_format($valor5->total_adiantamentos, 2, ',', '.');?></td>
					</tr>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left"><b>Saldo Final no Caixa</b></td>
						<td align="right">&nbsp;<b><?php echo number_format($valor_final_em_caixa, 2, ',', '.');?></b></td>
					</tr>
				</table>
			</td>
			<td align="left" valign="top">
				<table width="400" align="left" border="0" cellpadding="10" cellspacing="1" class="texto3" bgcolor="#CCCCCC">
					<tr bgcolor="#F4F4F4">
						<td colspan="2" class="titulo_grid3" align="center">RESUMO POR FORMA DE PAGAMENTO</td>
					</tr>
<?php
				$sql = "SELECT SUM(CI.valor) AS total, FP.descricao FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto) WHERE CI.caixa_nro='{$nro_caixa}' AND CI.cancelado='N' GROUP BY CI.forma_pagto";

				$pdo = $connection->prepare($sql);
				$pdo->execute();
				$query = $pdo->fetchAll(PDO::FETCH_OBJ);
				//$valor_total = 0;

				foreach ($query as $key => $value) {
					$rows[$key] = $value;
					//$valor_total = $valor_total + $rows[$key]->total;

					if ($rows[$key]->descricao == "DINHEIRO") {
						$rows[$key]->total = $rows[$key]->total + $caixa->saldo_inicial;
					}
?>
					<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
						<td align="left"><?php echo $rows[$key]->descricao; ?></td>
						<td align="right" width="80">&nbsp;<?php echo number_format($rows[$key]->total, 2, ',', '.');?></td>
					</tr>
<?php
				}
?>
				</table>
			</td>
		</tr>
	</table>


 <br>

<?php
}

if($_GET['tipo'] == "meios_pagtos"){
	$valor_total = 0;
?>
	<table width="500" align="left" border="0" cellpadding="10" cellspacing="1" class="texto3" bgcolor="#CCCCCC">
		<tr bgcolor="#F4F4F4">
			<td class="titulo_grid3" align="left">MEIO DE PAGAMENTO (VENDAS E ADIANTAMENTOS)</td>
			<td class="titulo_grid3" align="right">TOTAL</td>
		</tr>

<?php
	$sql = "SELECT SUM(CI.valor) AS total, FP.descricao FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto) WHERE CI.caixa_nro='{$nro_caixa}' AND CI.tipo_lancamento!=0 AND CI.tipo_lancamento!=1 AND CI.tipo_lancamento!=4 AND CI.cancelado='N' GROUP BY CI.forma_pagto";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetchAll(PDO::FETCH_OBJ);

	foreach ($query as $key => $value) {
		$rows[$key] = $value;
		$valor_total = $valor_total + $rows[$key]->total;
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="left"><?php echo $rows[$key]->descricao; ?></td>
			<td align="right" width="80">&nbsp;<?php echo number_format($rows[$key]->total, 2, ',', '.');?></td>
		</tr>
<?php
	}
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="left"><b>TOTAL GERAL</b></td>
			<td align="right">&nbsp;<b><?php echo number_format($valor_total, 2, ',', '.');?></b></td>
		</tr>
	</table>
<?php
}



if($_GET['tipo'] == "resumo_vendas"){
	$total_das_vendas = 0;
?>
	<table width="100%" align="left" border="0" cellpadding="10" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
		<tr bgcolor="#F4F4F4">
			<td width="40" class="titulo_grid4" align="center">ID</td>
			<td width="120" class="titulo_grid4" align="center">DATA/HORA</td>
			<td width="190" class="titulo_grid4" align="left">FORMA PAGTO</td>
			<td width="60" class="titulo_grid4" align="right">VALOR</td>
			<td class="titulo_grid4" align="left">OBSERVAÇÕES</td>
		</tr>
<?php
	$sql = "SELECT CI.id, FP.descricao, CONCAT(DATE_FORMAT(CI.data_hora, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(CI.data_hora, '%H:%i:%s')) AS data_hora, CI.valor, CI.observacoes 
			FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto)
			WHERE CI.caixa_nro='{$nro_caixa}' AND CI.tipo_lancamento=2 AND CI.cancelado='N' ORDER BY CI.observacoes ASC";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetchAll(PDO::FETCH_OBJ);

	foreach ($query as $key => $value) {
		$rows[$key] = $value;
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="center"><?php echo $rows[$key]->id; ?></td>
			<td align="center"><?php echo $rows[$key]->data_hora; ?></td>
			<td align="left"><?php echo $rows[$key]->descricao; ?></td>
			<td align="right">&nbsp;<?php echo number_format($rows[$key]->valor, 2, ',', '.');?></td>
			<td align="left"><?php echo $rows[$key]->observacoes; ?></td>
		</tr>
<?php
		$total_das_vendas = $total_das_vendas + $rows[$key]->valor;
	}
?>


	</table>

 <br>

<?php
echo $total_das_vendas;
}

if($_GET['tipo'] == "retiradas"){
?>
	<table width="100%" align="left" border="0" cellpadding="10" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
		<tr bgcolor="#F4F4F4">
			<td width="40" class="titulo_grid4" align="center">ID</td>
			<td width="120" class="titulo_grid4" align="center">DATA/HORA</td>
			<td width="190" class="titulo_grid4" align="left">FORMA PAGTO</td>
			<td width="60" class="titulo_grid4" align="right">VALOR</td>
			<td class="titulo_grid4" align="left">OBSERVAÇÕES</td>
		</tr>
<?php
	$sql = "SELECT CI.id, FP.descricao, CONCAT(DATE_FORMAT(CI.data_hora, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(CI.data_hora, '%H:%i:%s')) AS data_hora, CI.valor, CI.observacoes 
			FROM caixa_itens AS CI INNER JOIN formas_pagto AS FP ON (FP.id=CI.forma_pagto)
			WHERE CI.caixa_nro='{$nro_caixa}' AND CI.tipo_lancamento=1 AND CI.cancelado='N' ORDER BY CI.id ASC";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetchAll(PDO::FETCH_OBJ);
	$total_ret = 0;

	foreach ($query as $key => $value) {
		$rows[$key] = $value;
		$total_ret = $total_ret + $rows[$key]->valor;
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="center"><?php echo $rows[$key]->id; ?></td>
			<td align="center"><?php echo $rows[$key]->data_hora; ?></td>
			<td align="left"><?php echo $rows[$key]->descricao; ?></td>
			<td align="right">&nbsp;<?php echo number_format($rows[$key]->valor, 2, ',', '.');?></td>
			<td align="left"><?php echo $rows[$key]->observacoes; ?></td>
		</tr>
<?php
	}
?>
		<tr onMouseMove="this.style.background='#FFFFFF'" onMouseOut="this.style.background='#F8F8F8'" bgcolor="#F8F8F8">
			<td align="right" colspan="3"><b>TOTAL RETIRADO</b></td>
			<td align="left" colspan="2">&nbsp;<b><?php echo number_format($total_ret, 2, ',', '.');?></b></td>
		</tr>
	</table>

<?php
}
?>



</body>
</html>