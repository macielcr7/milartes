<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'caixa_itens';

		//pega o caixa do usuario atual
		$pdo = $connection->prepare("SELECT caixa_atual FROM usuarios WHERE id = $user_id");
		$pdo->execute();
		$linhas = $pdo->fetch(PDO::FETCH_OBJ);
		$caixa_atual_usuario = $linhas->caixa_atual;
		if ($caixa_atual_usuario == 0){
			$caixa_atual_usuario = -1;
		}

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT * FROM caixa_itens WHERE id=:id");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif(isset($_POST['action']) AND $_POST['action'] == 'USER_CAIXA_ATUAL'){
			$pdo = $connection->prepare("SELECT caixa_atual FROM usuarios WHERE id = '{$_POST['user']}'");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode(array('success'=>true, 'dados'=>$linhas));
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();
			$resumo = "N";
			$agrupa = "";
			$sqlTexto = "";

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('caixa_nro', 'caixa_itens.caixa_nro'), $_POST['caixa_nro']);
				$buscar->setBusca(array('tipo_lancamento', 'caixa_itens.tipo_lancamento'), $_POST['tipo_lancamento']);
				$buscar->setBusca(array('id_tipo', 'caixa_itens.id_tipo'), $_POST['id_tipo']);
				$buscar->setBusca(array('forma_pagto', 'caixa_itens.forma_pagto'), $_POST['forma_pagto']);
				$buscar->setBusca(array('valor', 'caixa_itens.valor'), $_POST['valor'], 'like');
				$buscar->setBusca(array('cancelado', 'caixa_itens.cancelado'), $_POST['cancelado'], 'like');
				$buscar->setBusca(array('cancelado_por', 'caixa_itens.cancelado_por'), $_POST['cancelado_por']);
				$buscar->setBusca(array('tipo_venda', 'caixa_itens.tipo_venda'), $_POST['tipo_venda'], 'like');
				$buscar->setBusca(array('cod_cliente', 'caixa_itens.cod_cliente'), $_POST['cod_cliente']);
				$buscar->setBusca(array('observacoes', 'caixa_itens.observacoes'), $_POST['observacoes'], 'like');
			}

			$where2 = '';
			if(isset($_POST['action']) AND $_POST['action'] == 'GRID_RESUMO_SUPRIMENTOS'){
				$buscar->setBusca(array('tipo_lancamento', 'caixa_itens.tipo_lancamento'), '0');
				$buscar->setBusca(array('cancelado', 'caixa_itens.cancelado'), 'N');
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'GRID_RETIRADAS'){
				//$where2 = " AND caixa_itens.tipo_lancamento='1' AND caixa_itens.cancelado='N'";
				$buscar->setBusca(array('tipo_lancamento', 'caixa_itens.tipo_lancamento'), '1');
				$buscar->setBusca(array('cancelado', 'caixa_itens.cancelado'), 'N');
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'GRID_RESUMO_VENDAS'){
				//$where2 = " AND caixa_itens.tipo_lancamento='2' AND caixa_itens.cancelado='N'";
				$buscar->setBusca(array('tipo_lancamento', 'caixa_itens.tipo_lancamento'), '2');
				$buscar->setBusca(array('cancelado', 'caixa_itens.cancelado'), 'N');

				$sqlTexto .= " vendas_pagamentos.cod_venda,";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'GRID_ADIANTAMENTOS'){
				//$where2 = " AND caixa_itens.tipo_lancamento='3' AND caixa_itens.cancelado='N'";
				$buscar->setBusca(array('tipo_lancamento', 'caixa_itens.tipo_lancamento'), '3');
				$buscar->setBusca(array('cancelado', 'caixa_itens.cancelado'), 'N');
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'GRID_MEIOS_PAGTOS'){
				//$where2 = " AND caixa_itens.tipo_lancamento='3' AND caixa_itens.cancelado='N'";
				$buscar->setBusca(array('cancelado', 'caixa_itens.cancelado'), 'N');
				$buscar->setBusca(array('tipo_lancamento', 'caixa_itens.tipo_lancamento'), '2', 'AndOr', '3');
				$resumo = "S";
				$agrupa = "GROUP BY caixa_itens.forma_pagto";
			}

			if (!empty($_POST['caixa_nro'])) {
				$buscar->setBusca(array('caixa_nro', 'caixa_itens.caixa_nro'), $_POST['caixa_nro']);
				//$caixa_nro = $_POST['caixa_nro'];
			}
			else {
				$buscar->setBusca(array('caixa_nro', 'caixa_itens.caixa_nro'), $caixa_atual_usuario);
				//$caixa_nro = $caixa_atual_usuario;
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			if ($sort == 'forma_pagto_descricao') {
				//$sort = 'forma_pagto_descricao '.$order.', caixa_itens.data_hora '.$order;
				$sort = 'forma_pagto_descricao '.$order.', caixa_itens.data_hora ';
			} elseif ($sort == 'zzzzzxczxczxzxzx') {
				$sort = 'os.usuario_abertura '.$order;
			}

			$filtro = $buscar->getSql();

			if ($resumo == "N") {
				$sqlContagem = "count(*)";
				$sqlTexto = "caixa_itens.*,";
			}
			else {
				$sqlContagem = "count(DISTINCT(forma_pagto))";
				$sqlTexto = "caixa_itens.*, SUM(caixa_itens.valor) AS valor_soma,";
			}

			$pdo = $connection->prepare("SELECT {$sqlContagem} AS total FROM caixa_itens {$filtro}");
			$pdo->execute($buscar->getArrayExecute());
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT {$sqlTexto} 
					formas_pagto.categoria, 
					UPPER(U.nome) AS cadastrado_por_nome, 
					UPPER(U2.nome) AS cancelado_por_nome, 
					CASE WHEN
						 clientes.tipo_cliente = 'J'
						 THEN
							 CASE WHEN clientes.nome_fantasia != ''
								 THEN 
									 CONCAT(UPPER(clientes.razao_social), ' (', UPPER(clientes.nome_fantasia), ')')
								 ELSE
									 UPPER(clientes.razao_social)
							 END
						 ELSE
							 UPPER(clientes.nome_completo)
					 END AS nome_cliente, 
					formas_pagto.descricao AS forma_pagto_descricao, 
					vendas_pagamentos.cod_venda 
				FROM caixa_itens 
				LEFT JOIN usuarios AS U ON (U.id=caixa_itens.cadastrado_por) 
				LEFT JOIN usuarios AS U2 ON (U2.id=caixa_itens.cancelado_por) 
				LEFT JOIN clientes ON (clientes.cod_cliente=caixa_itens.cod_cliente) 
				LEFT JOIN formas_pagto ON (formas_pagto.id=caixa_itens.forma_pagto) 
				LEFT JOIN vendas_pagamentos ON (vendas_pagamentos.id=caixa_itens.id_tipo) 
				{$filtro} {$agrupa} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute($buscar->getArrayExecute());
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			//echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'total'=>$countRow, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>