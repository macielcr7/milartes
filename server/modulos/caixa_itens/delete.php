<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'caixa_itens';
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if($_POST['action'] == 'CANCELAR_ITEM'){

			$user->getAcao($tabela, 'deletar');
			$sql = "UPDATE caixa_itens SET cancelado='S', data_cancelamento='$data_hora_atual', cancelado_por='$user_id' WHERE id = {$_POST['id_item']}";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			echo json_encode(array('success'=>true, 'sql'=>$sql, 'msg'=>'Item Cancelado com Sucesso!'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>