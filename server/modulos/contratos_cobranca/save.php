<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'contratos_cobranca';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		$taxa_boleto = $_POST['taxa_boleto'];
		$taxa_boleto = str_replace('R$ ','',$taxa_boleto);
		$taxa_boleto = str_replace('.','',$taxa_boleto);
		$taxa_boleto = str_replace(',','.',$taxa_boleto);
		if ($taxa_boleto == "") { $taxa_boleto = 0.00; }

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE contratos_cobranca SET 
				descricao = ?,
				layout_cobranca = ?,
				conta_bancaria = ?,
				operacao = ?,
				convenio = ?,
				convenio_digito = ?,
				contrato = ?,
				carteira = ?,
				carteira_variacao = ?,
				carteira_tipo = ?,
				taxa_boleto = ?,
				taxa_modalidade = ?,
				ativo = ?,
				alterado_por = ? 
				WHERE id = ?
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				mb_strtoupper($_POST['layout_cobranca'], 'UTF-8'),
				$_POST['conta_bancaria'],
				$_POST['operacao'],
				$_POST['convenio'],
				$_POST['convenio_digito'],
				$_POST['contrato'],
				$_POST['carteira'],
				$_POST['carteira_variacao'],
				$_POST['carteira_tipo'],
				$taxa_boleto,
				$_POST['taxa_modalidade'],
				$_POST['ativo'],
				$user_id,
				$_POST['id']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO contratos_cobranca 
					(
						descricao,
						layout_cobranca,
						conta_bancaria,
						operacao,
						convenio,
						convenio_digito,
						contrato,
						carteira,
						carteira_variacao,
						carteira_tipo,
						taxa_boleto,
						taxa_modalidade,
						ativo,
						data_cadastro,
						cadastrado_por,
						alterado_por
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				mb_strtoupper($_POST['layout_cobranca'], 'UTF-8'),
				$_POST['conta_bancaria'],
				$_POST['operacao'],
				$_POST['convenio'],
				$_POST['convenio_digito'],
				$_POST['contrato'],
				$_POST['carteira'],
				$_POST['carteira_variacao'],
				$_POST['carteira_tipo'],
				$taxa_boleto,
				$_POST['taxa_modalidade'],
				$_POST['ativo'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}