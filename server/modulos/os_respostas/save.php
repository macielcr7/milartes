<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'os_respostas';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');

			$pdo2 = $connection->prepare("SELECT conteudo FROM os_respostas WHERE id='{$_POST['id']}'");
			$pdo2->execute();
			$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
			$conteudo_antigo = $linhas->conteudo;

			$pdo = $connection->prepare("
					UPDATE os_respostas SET 
							usuario = ?, 
							os_nro = ?, 
							conteudo = ?,  
							alterado_por = ? 
					WHERE id = ?
			");
			$params = array(
				$_POST['usuario'], 
				$_POST['os_nro'], 
				$_POST['conteudo'], 
				$user_id, 
				$_POST['id']
			);
			$pdo->execute($params);

			if ($conteudo_antigo != $_POST['conteudo']){
				$descricao = strtoupper($_POST['conteudo']);
				$pdo3 = $connection->prepare("INSERT INTO logs_sistema 
					(chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
				VALUES 
					(md5(NOW()), 'os', '{$_POST['os_nro']}', 'Alterou a Resposta', 'Descrição', '$conteudo_antigo', '$descricao', '$user_id', '$data_hora_atual')");
				$pdo3->execute();
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO os_respostas 
					(
						usuario, 
						os_nro, 
						conteudo, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por 
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['usuario'], 
				$_POST['os_nro'], 
				$_POST['conteudo'], 
				$data_hora_atual, 
				$user_id, 
				$user_id
			);
			$pdo->execute($params);

			if (!empty($_POST['conteudo'])){
				$descricao = strtoupper($_POST['conteudo']);
				$pdo4 = $connection->prepare("INSERT INTO logs_sistema 
					(chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
				VALUES 
					(md5(NOW()), 'os', '{$_POST['os_nro']}', 'Cadastrou a Resposta', 'Descrição', '', '$descricao', '$user_id', '$data_hora_atual')");
				$pdo4->execute();
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}