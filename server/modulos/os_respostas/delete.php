<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'os_respostas';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		if($_POST['action'] == 'DELETAR'){
			$user->getAcao($tabela, 'deletar');

			$pdo2 = $connection->prepare("SELECT conteudo, os_nro FROM os_respostas WHERE id='{$_POST['id']}'");
			$pdo2->execute();
			$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
			$conteudo_antigo = $linhas->conteudo;

			$pdo3 = $connection->prepare("INSERT INTO logs_sistema 
				(chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
			VALUES 
				(md5(NOW()), 'os', '$linhas->os_nro', 'Removeu a Resposta', 'Descrição', '$conteudo_antigo', '', '$user_id', '$data_hora_atual')");
			$pdo3->execute();

			$pdo = $connection->prepare("DELETE FROM os_respostas WHERE id = ?");
			$pdo->execute(array(
				$_POST['id']
			));

			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>