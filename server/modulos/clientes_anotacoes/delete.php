<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'clientes_anotacoes';
	try {
		if($_POST['action'] == 'DELETAR'){

			$data_hora_atual = date('Y-m-d H:i:s');
			$pdo = $connection->prepare("SELECT controle, anotacao FROM clientes_anotacoes WHERE controle='$_POST[id]'");
			$pdo->execute();
	
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$descricao = $linhas->controle." - ".$linhas->anotacao;
			$nome_tabela = utf8_encode('Clientes - Anota��es');
			$nome_campo = utf8_encode('Anota��o');
			$pdo = $connection->prepare("
					INSERT INTO logs_sistema (controle, chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
						VALUES 
					(NULL, md5('$data_hora_atual'), '$nome_tabela', '$_POST[id]', 'Excluiu', '$nome_campo', '$descricao', '', '$user_id', '$data_hora_atual')
			");
			$pdo->execute();

			$pdo = $connection->prepare("DELETE FROM clientes_anotacoes WHERE controle = ?");
			$pdo->execute(array(
				$_POST['id']
			));
			
			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA.'<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}