<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'clientes_anotacoes';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM clientes_anotacoes
				WHERE controle=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'VEIRIFICA_MENSAGENS' ){
			$cod_cliente = $_POST['cod_cliente'];
			$sql = "SELECT anotacao FROM clientes_anotacoes 
					WHERE cod_cliente = {$cod_cliente} ORDER BY controle ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$mensagens = array();
			foreach ($query as $key => $value) {
				$mensagens[] = $value->anotacao;
			}
			echo json_encode( array('success'=>true, 'mensagem'=>implode('</br>', $mensagens)) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			$buscar->setBusca(array('cod_cliente', 'clientes_anotacoes.cod_cliente'), $_POST['cod_cliente']);
			
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('anotacao', 'clientes_anotacoes.anotacao'), $_POST['anotacao'], 'like');
				$buscar->setBusca(array('ativo', 'clientes_anotacoes.ativo'), $_POST['ativo'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("
				SELECT count(*) as total 
				FROM clientes_anotacoes 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("
				SELECT controle, 
					UPPER(anotacao) AS anotacao, 
					UPPER(ativo) AS ativo, 
					data_cadastro, 
					DATE_FORMAT(clientes_anotacoes.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(clientes_anotacoes.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM clientes_anotacoes 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>