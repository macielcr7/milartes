<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'instaladores';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if ($_POST['fone1'] == "(__) ____-____") {
			$fone1 = "";
		} else {
			$fone1 = $_POST['fone1'];
		}
		if ($_POST['fone2'] == "(__) ____-____") {
			$fone2 = "";
		} else {
			$fone2 = $_POST['fone2'];
		}
		if ($_POST['fone3'] == "(__) ____-____") {
			$fone3 = "";
		} else {
			$fone3 = $_POST['fone3'];
		}
		if ($_POST['fone4'] == "(__) ____-____") {
			$fone4 = "";
		} else {
			$fone4 = $_POST['fone4'];
		}
		if ($_POST['fone5'] == "(__) ____-____") {
			$fone5 = "";
		} else {
			$fone5 = $_POST['fone5'];
		}

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE instaladores SET 
				nome_completo = ?,
				apelido = ?,
				endereco = ?,
				fone1 = ?,
				fone2 = ?,
				fone3 = ?,
				fone4 = ?,
				fone5 = ?,
				obs_fone1 = ?,
				obs_fone2 = ?,
				obs_fone3 = ?,
				obs_fone4 = ?,
				obs_fone5 = ?,
				produtos_que_instala = ?,
				alterado_por = ?,
				ativo = ? 
				WHERE cod_instalador = ?
			");
			$params = array(
				mb_strtoupper($_POST['nome_completo'], 'UTF-8'),
				mb_strtoupper($_POST['apelido'], 'UTF-8'),
				mb_strtoupper($_POST['endereco'], 'UTF-8'),
				$fone1,
				$fone2,
				$fone3,
				$fone4,
				$fone5,
				mb_strtoupper($_POST['obs_fone1'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone2'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone3'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone4'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone5'], 'UTF-8'),
				mb_strtoupper($_POST['produtos_que_instala'], 'UTF-8'),
				$user_id,
				$_POST['ativo'],
				$_POST['cod_instalador']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO instaladores 
					(
						nome_completo,
						apelido,
						endereco,
						fone1,
						fone2,
						fone3,
						fone4,
						fone5,
						obs_fone1,
						obs_fone2,
						obs_fone3,
						obs_fone4,
						obs_fone5,
						produtos_que_instala,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						ativo
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['nome_completo'], 'UTF-8'),
				mb_strtoupper($_POST['apelido'], 'UTF-8'),
				mb_strtoupper($_POST['endereco'], 'UTF-8'),
				$fone1,
				$fone2,
				$fone3,
				$fone4,
				$fone5,
				mb_strtoupper($_POST['obs_fone1'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone2'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone3'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone4'], 'UTF-8'),
				mb_strtoupper($_POST['obs_fone5'], 'UTF-8'),
				mb_strtoupper($_POST['produtos_que_instala'], 'UTF-8'),
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				$_POST['ativo']
			);
		}

		$pdo->execute($params);
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Instalador Adicionado com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}