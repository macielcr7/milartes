﻿<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'instaladores';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT * FROM instaladores WHERE cod_instalador=:id "; 

			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";

			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE I.nome_completo like '%$_POST[query]%' OR I.apelido like '%$_POST[query]%' OR I.produtos_que_instala like '%$_POST[query]%'";
				$where2 = " AND (I.nome_completo like '%$_POST[query]%' OR I.apelido like '%$_POST[query]%' OR I.produtos_que_instala like '%$_POST[query]%')";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_instalador'])) {
					$where = "WHERE I.ativo='S'".$where2;
				}
				elseif ($_POST['status_instalador'] == 'X') {
					$where = "".$where1;
				}
				else{
					$where = "WHERE I.ativo='{$_POST['status_instalador']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM instaladores AS I $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);

			$countRow = $query->total;

			$sql = "SELECT I.*, DATE_FORMAT(I.data_cadastro, '%d/%m/%Y ?s %H:%i:%s') as data_cadastro, 
				UPPER(U.nome) AS cadastrado_por_nome 
				FROM instaladores AS I INNER JOIN usuarios AS U ON (U.id=I.cadastrado_por) 
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$rows = array();
			foreach ($query as $key => $value) {
				$rows[$key]->fones = "";
				$rows[$key] = $value;
				if ($rows[$key]->fone1 != "") {
					$rows[$key]->fones .= "(".substr($rows[$key]->fone1, 0, 2).") ".substr($rows[$key]->fone1, 2, 4)."-".substr($rows[$key]->fone1, 6, 4)." ".$rows[$key]->obs_fone1;
				}
				if ($rows[$key]->fone2 != "") {
					$rows[$key]->fones .= "<br>(".substr($rows[$key]->fone2, 0, 2).") ".substr($rows[$key]->fone2, 2, 4)."-".substr($rows[$key]->fone2, 6, 4)." ".$rows[$key]->obs_fone2;
				}
				if ($rows[$key]->fone3 != "") {
					$rows[$key]->fones .= "<br>(".substr($rows[$key]->fone3, 0, 2).") ".substr($rows[$key]->fone3, 2, 4)."-".substr($rows[$key]->fone3, 6, 4)." ".$rows[$key]->obs_fone3;
				}
				if ($rows[$key]->fone4 != "") {
					$rows[$key]->fones .= "<br>(".substr($rows[$key]->fone4, 0, 2).") ".substr($rows[$key]->fone4, 2, 4)."-".substr($rows[$key]->fone4, 6, 4)." ".$rows[$key]->obs_fone4;
				}
				if ($rows[$key]->fone5 != "") {
					$rows[$key]->fones .= "<br>(".substr($rows[$key]->fone5, 0, 2).") ".substr($rows[$key]->fone5, 2, 4)."-".substr($rows[$key]->fone5, 6, 4)." ".$rows[$key]->obs_fone5;
				}
				if (empty($rows[$key]->produtos_que_instala)) {
					$rows[$key]->produtos_que_instala = "";
				}
			}

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$rows, 'sql'=>$sql) );
			//echo json_encode($result);
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}
}
?>