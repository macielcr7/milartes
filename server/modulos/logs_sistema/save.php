<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'logs_sistema';
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');

			$pdo = $connection->prepare("
					UPDATE logs_sistema SET 
							chave = ?,							
							tabela = ?,							
							tabela_cod = ?,							
							acao = ?,							
							campo = ?,							
							alterado_de = ?,							
							alterado_para = ?,							
							usuario = ?,							
							data = ?							
 					WHERE controle = ?
			");
			$params = array(
				$_POST['chave'],
				$_POST['tabela'],
				$_POST['tabela_cod'],
				$_POST['acao'],
				$_POST['campo'],
				$_POST['alterado_de'],
				$_POST['alterado_para'],
				$_POST['usuario'],
				implode('-', array_reverse(explode('/', $_POST['data_date'])))." ".$_POST['data_time'],
				$_POST['controle']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');

			$pdo = $connection->prepare("
				INSERT INTO logs_sistema 
					(
						chave,						
						tabela,						
						tabela_cod,						
						acao,						
						campo,						
						alterado_de,						
						alterado_para,						
						usuario,						
						data						
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?			
					)
			");
			$params = array(
				$_POST['chave'],		
				$_POST['tabela'],		
				$_POST['tabela_cod'],		
				$_POST['acao'],		
				$_POST['campo'],		
				$_POST['alterado_de'],		
				$_POST['alterado_para'],		
				$_POST['usuario'],		
				implode('-', array_reverse(explode('/', $_POST['data_date'])))." ".$_POST['data_time']		
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}