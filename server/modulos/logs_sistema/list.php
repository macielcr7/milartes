<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'logs_sistema';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql = "SELECT *, 
					DATE_FORMAT(data, '%H:%i:%s') as data_time, 
					DATE_FORMAT(data, '%Y-%m-%d') as data_date 
				FROM logs_sistema
				WHERE controle=:id
			";
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM logs_sistema 
						WHERE tabela='{$_POST['modulo']}' AND tabela_cod='{$_POST['tabela_cod']}' 
						ORDER BY {$sort} {$order} 
						LIMIT {$start}, {$limit}
					";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT LOG.controle, 
					LOG.chave AS chave, 
					LOG.tabela AS tabela, 
					LOG.tabela_cod AS tabela_cod, 
					LOG.acao AS acao, 
					LOG.campo AS campo, 
					LOG.data AS data, 
					UPPER(LOG.alterado_de) AS alterado_de, 
					UPPER(LOG.alterado_para) AS alterado_para, 
					UPPER(usuarios.nome) AS usuario2 
				FROM logs_sistema AS LOG
				LEFT JOIN usuarios ON (LOG.usuario=usuarios.id) 
				WHERE LOG.tabela='{$_POST['modulo']}' AND LOG.tabela_cod='{$_POST['tabela_cod']}' 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);


			if ($_POST['modulo'] == 'Clientes'){
				foreach ($query as $key => $value) {
					$query[$key] = $value;

					if($query[$key]->campo == "Tipo Cliente") {
						if ($query[$key]->alterado_de != "") {
							if ($query[$key]->alterado_de == "F") {
								$query[$key]->alterado_de = "PESSOA FÍSICA";
							}
							else {
								$query[$key]->alterado_de = "PESSOA JURÍDICA";
							}
						}
						elseif ($query[$key]->alterado_para != "") {
							if ($query[$key]->alterado_para == "F") {
								$query[$key]->alterado_para = "PESSOA FÍSICA";
							}
							else {
								$query[$key]->alterado_para = "PESSOA JURÍDICA";
							}
						}
					}
					elseif($query[$key]->campo == "Situação Cadastral") {
						if ($query[$key]->alterado_de != "") {
							if ($query[$key]->alterado_de == "N") {
								$query[$key]->alterado_de = "NORMAL";
							}
							elseif ($query[$key]->alterado_de == "R") {
								$query[$key]->alterado_de = "RECUSADO";
							}
							elseif ($query[$key]->alterado_de == "P") {
								$query[$key]->alterado_de = "PENDÊNCIA FINANCEIRA";
							}
							elseif ($query[$key]->alterado_de == "E") {
								$query[$key]->alterado_de = "EXCLUÍDO";
							}
						}
						elseif ($query[$key]->alterado_para != "") {
							if ($query[$key]->alterado_para == "N") {
								$query[$key]->alterado_para = "NORMAL";
							}
							elseif ($query[$key]->alterado_para == "R") {
								$query[$key]->alterado_para = "RECUSADO";
							}
							elseif ($query[$key]->alterado_para == "P") {
								$query[$key]->alterado_para = "PENDÊNCIA FINANCEIRA";
							}
							elseif ($query[$key]->alterado_para == "E") {
								$query[$key]->alterado_para = "EXCLUÍDO";
							}
						}
					}
				}
			}
			elseif ($_POST['modulo'] == 'os'){
				foreach ($query as $key => $value) {
					$query[$key] = $value;

					if($query[$key]->campo == "Cidade") {
						if ($query[$key]->alterado_de != ''){
							$codigo_cidade = $query[$key]->alterado_de;
						}
						else{
							$codigo_cidade = $query[$key]->alterado_para;
						}

						$pdo2 = $connection->prepare("SELECT UPPER(loc_nome) AS loc_nome FROM correios_localidades WHERE id='$codigo_cidade'");
						$pdo2->execute();
						$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
						$nome_cidade = $linhas->loc_nome;

						if ($query[$key]->alterado_de != "") {
							$query[$key]->alterado_de = $nome_cidade;
						}
						elseif ($query[$key]->alterado_para != "") {
							$query[$key]->alterado_para = $nome_cidade;
						}
					}
					elseif($query[$key]->campo == "Bairro") {
						if ($query[$key]->alterado_de != ''){
							$codigo_bairro = $query[$key]->alterado_de;
						}
						else{
							$codigo_bairro = $query[$key]->alterado_para;
						}

						$pdo2 = $connection->prepare("SELECT UPPER(bairro_nome) AS bairro_nome FROM correios_bairros WHERE id='$codigo_bairro'");
						$pdo2->execute();
						$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
						$nome_bairro = $linhas->bairro_nome;

						if ($query[$key]->alterado_de != "") {
							$query[$key]->alterado_de = $nome_bairro;
						}
						elseif ($query[$key]->alterado_para != "") {
							$query[$key]->alterado_para = $nome_bairro;
						}
					}
					elseif($query[$key]->campo == "Endereço") {
						if ($query[$key]->alterado_de != ''){
							$codigo_endereco = $query[$key]->alterado_de;
						}
						else{
							$codigo_endereco = $query[$key]->alterado_para;
						}

						$pdo2 = $connection->prepare("SELECT UPPER(nome) AS nome FROM correios_enderecos WHERE id='$codigo_endereco'");
						$pdo2->execute();
						$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
						$nome_endereco = $linhas->nome;

						if ($query[$key]->alterado_de != "") {
							$query[$key]->alterado_de = $nome_endereco;
						}
						elseif ($query[$key]->alterado_para != "") {
							$query[$key]->alterado_para = $nome_endereco;
						}
					}
					elseif($query[$key]->campo == "Usuário Responsável") {
						if ($query[$key]->alterado_de != ''){
							$codigo_responsavel = $query[$key]->alterado_de;
						}
						else{
							$codigo_responsavel = $query[$key]->alterado_para;
						}

						$pdo2 = $connection->prepare("SELECT UPPER(nome) AS nome FROM usuarios WHERE id='$codigo_responsavel'");
						$pdo2->execute();
						$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
						$nome_usuario = $linhas->nome;

						if ($query[$key]->alterado_de != "") {
							$query[$key]->alterado_de = $nome_usuario;
						}
						elseif ($query[$key]->alterado_para != "") {
							$query[$key]->alterado_para = $nome_usuario;
						}
					}
					elseif($query[$key]->campo == "Urgência/Prioridade") {
						if ($query[$key]->alterado_de != "") {
							if ($query[$key]->alterado_de == "1") {
								$query[$key]->alterado_de = "NORMAL";
							}
							elseif ($query[$key]->alterado_de == "2") {
								$query[$key]->alterado_de = "URGÊNTE";
							}
						}
						elseif ($query[$key]->alterado_para != "") {
							if ($query[$key]->alterado_para == "1") {
								$query[$key]->alterado_para = "NORMAL";
							}
							elseif ($query[$key]->alterado_para == "2") {
								$query[$key]->alterado_para = "URGÊNTE";
							}
						}
					}
					elseif($query[$key]->campo == "Departamento") {
						if ($query[$key]->alterado_de != ''){
							$codigo_departamento = $query[$key]->alterado_de;
						}
						else{
							$codigo_departamento = $query[$key]->alterado_para;
						}

						$pdo2 = $connection->prepare("SELECT UPPER(descricao) AS descricao FROM produtos_categorias WHERE cod_categoria='$codigo_departamento'");
						$pdo2->execute();
						$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
						$nome_departamento = $linhas->descricao;

						if ($query[$key]->alterado_de != "") {
							$query[$key]->alterado_de = $nome_departamento;
						}
						elseif ($query[$key]->alterado_para != "") {
							$query[$key]->alterado_para = $nome_departamento;
						}
					}
					elseif($query[$key]->campo == "Categoria") {
						if ($query[$key]->alterado_de != ''){
							$codigo_categoria = $query[$key]->alterado_de;
						}
						else{
							$codigo_categoria = $query[$key]->alterado_para;
						}

						$pdo2 = $connection->prepare("SELECT UPPER(descricao) AS descricao FROM os_categorias WHERE id='$codigo_categoria'");
						$pdo2->execute();
						$linhas = $pdo2->fetch(PDO::FETCH_OBJ);
						$nome_categoria = $linhas->descricao;

						if ($query[$key]->alterado_de != "") {
							$query[$key]->alterado_de = $nome_categoria;
						}
						elseif ($query[$key]->alterado_para != "") {
							$query[$key]->alterado_para = $nome_categoria;
						}
					}




				}				
			}


			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA.'<br><br>'.$e->getMessage(), 'sql'=>$sql, 'erro'=>$e->getMessage()));
	}	
}
?>