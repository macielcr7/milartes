<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'orcamentos_observacoes';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		$descricao = trim(mb_strtoupper($_POST['descricao'], 'UTF-8'));
		$descricao = html_entity_decode($descricao);
		$descricao = str_replace('&NBSP;', '&nbsp;', $descricao);

		if($_POST['action'] == 'EDITAR'){
			//$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE orcamentos_observacoes SET 
							orcamento_id = ?, 
							nro_item = ?, 
							descricao = ?, 
							tipo = ?, 
							alterado_por = ?, 
							data_alteracao = ? 
 					WHERE id = ?
			");
			$params = array(
				$_POST['orcamento_id'],
				$_POST['nro_item'],
				$descricao,
				$_POST['tipo'],
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			//$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO orcamentos_observacoes 
					(
						orcamento_id, 
						ambiente_id, 
						nro_item, 
						descricao, 
						tipo, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['orcamento_id'],
				'0',
				$_POST['nro_item'],
				$descricao,
				$_POST['tipo'],
				date('Y-m-d H:i:s'), 
				$user_id,
				$user_id,
				date('Y-m-d H:i:s')
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}