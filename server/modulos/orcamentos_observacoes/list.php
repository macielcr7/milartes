<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'orcamentos_observacoes';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT * 
				FROM orcamentos_observacoes
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			$pdo = $connection->prepare("SELECT count(*) AS total FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['orcamento_id']}'");
			$pdo->execute();

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;
			
			$pdo = $connection->prepare("SELECT * FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['orcamento_id']}' ORDER BY nro_item ASC");

			$pdo->execute();
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>