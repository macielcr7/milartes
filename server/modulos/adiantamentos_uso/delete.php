<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'adiantamentos_uso';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		//não usado aqui.... mas no save.pnp
		if($_POST['action'] == 'DESVINCULARXXX'){
			$pdo = $connection->prepare("
				UPDATE adiantamentos_uso SET 
				cancelado = 'S', 
				desvinculado_por = ?, 
				data_desvinculo = ?,
				desvinculo_motivo = ? 
				WHERE id = ?
			");
			$params = array(
				$user_id,
				$data_hora_atual, 
				mb_strtoupper($_POST['desvinculo_motivo'], 'UTF-8'),
				$_POST['id']
			);
			$pdo->execute($params);

			//atualiza os saldos do adiantamento
			$pdo2 = $connection->prepare("
				UPDATE adiantamentos SET 
						valor_utilizado = valor_utilizado-{$_POST['valor_vinculado']},
						saldo = saldo+{$_POST['valor_vinculado']},
						data_alteracao = ?,
						alterado_por = ? 
				WHERE id = ?
			");
			$params2 = array(
				$data_hora_atual,
				$user_id,
				$_POST['cod_adiantamento']
			);
			$pdo2->execute($params2);

			echo json_encode(array('success'=>true, 'msg'=>'Adiantamento Desvinculado com Sucesso!'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>$mensagem, 'erro'=>$e->getMessage()));
	}
}
?>