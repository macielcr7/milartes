<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'adiantamentos_uso';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		if($_POST['action'] == 'DESVINCULAR'){
			$user->getAcao($tabela, 'desvincular_valor');
			$pdo = $connection->prepare("
				UPDATE adiantamentos_uso SET 
				cancelado = 'S', 
				desvinculado_por = ?, 
				data_desvinculo = ?,
				desvinculo_motivo = ? 
				WHERE id = ?
			");
			$params = array(
				$user_id,
				$data_hora_atual, 
				mb_strtoupper($_POST['desvinculo_motivo'], 'UTF-8'),
				$_POST['id']
			);
			$pdo->execute($params);

			//atualiza os saldos do adiantamento
			$pdo2 = $connection->prepare("
				UPDATE adiantamentos SET 
						valor_utilizado = valor_utilizado-{$_POST['valor_vinculado']},
						saldo = saldo+{$_POST['valor_vinculado']},
						data_alteracao = ?,
						alterado_por = ? 
				WHERE id = ?
			");
			$params2 = array(
				$data_hora_atual,
				$user_id,
				$_POST['cod_adiantamento']
			);
			$pdo2->execute($params2);

			$mensagem = "Adiantamento Desvinculado com Sucesso!";
		}
		elseif ($_POST['action'] == 'VINCULAR'){
			//verfifica se tem caixa aberto
			$sql = "SELECT caixa_atual FROM usuarios WHERE id = '{$user_id}'";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$caixa_atual = $query->caixa_atual;

			if ($caixa_atual == 0){
				echo json_encode(array('success'=>false, 'msg'=>'Você deve Primeiro Abrir o Caixa para poder receber valores', 'erro'=>'Precia Abrir o Caixa Primeiro'));
			}
			else{
				$user->getAcao($tabela, 'vincular_valor');
				$pdo = $connection->prepare("
					INSERT INTO adiantamentos_uso 
						(
							cod_adiantamento,
							cod_venda,
							valor_vinculado,
							data_vinculo,
							vinculado_por
						) 
					VALUES 
						(
							?,	?,	?,	?,	?
						)
				");
				$params = array(
					$_POST['id_adiantamento'],
					$_POST['id_venda'],
					$_POST['valor'],
					$data_hora_atual,
					$user_id
				);
				$pdo->execute($params);
				$ultimo_id = $connection->lastInsertId();

				//atualiza os saldos do adiantamento
				$pdo2 = $connection->prepare("
						UPDATE adiantamentos SET 
								valor_utilizado = valor_utilizado+{$_POST['valor']},
								saldo = saldo-{$_POST['valor']},
								data_alteracao = ?,
								alterado_por = ? 
						WHERE id = ?
				");
				$params2 = array(
					$data_hora_atual,
					$user_id,
					$_POST['id_adiantamento']
				);
				$pdo2->execute($params2);

				$mensagem = "Adiantamento Vinculado com Sucesso";
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>$mensagem));


	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>