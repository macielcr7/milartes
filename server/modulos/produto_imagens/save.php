<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	require_once('../../lib/Upload.class.php');
	$tabela = 'produto_imagens';
	try {
		$up = upload('../../../uploads/produtos_imagens', $_FILES['src']);

		if(isset($_POST['id_cor'])){
			$id_cor = $_POST['id_cor'];
			$id_cor_sql = ' id_cor = "'.$id_cor.'",';

			$id_cor_sql1 = ' id_cor, ';
			$id_cor_sql2 = "'{$id_cor}', ";
		}
		else{
			$id_cor_sql = NULL;
		}

		if($_POST['action'] == 'EDITAR'){
			if($up['erro']==0){
				$pdo = $connection->prepare("
						UPDATE produto_imagens SET 
							".$preco_custo_sql1."
							src = '{$up['msg']}'
	 					WHERE id_produto_imagens = ?
				");
				$params = array(
					//$_POST['src'],
					$_POST['id_produto_imagens']
				);
				$pdo->execute($params);
			}
			else{
				throw new PDOException(utf8_encode($up['msg']));
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			if($up['erro']==0){
				$pdo = $connection->prepare("
					INSERT INTO produto_imagens 
						(
							id_produto,
							".$id_cor_sql1."
							src
						) 
					VALUES 
						(
							?,	".$id_cor_sql2."	?
						)
				");
				$params = array(
					$_POST['id_produto'],		
					$up['msg']		
				);
				$pdo->execute($params);				
			}
			else{
				throw new PDOException(utf8_encode($up['msg']));	
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}