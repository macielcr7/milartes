<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'produto_precos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT * FROM produto_precos WHERE id_produto_precos=:id");
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$buscar->setBusca(array('id_produto', 'PP.id_produto'), $_POST['id_produto']);

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('id_categoria_precos', 'PP.id_categoria_precos'), $_POST['id_categoria_precos']);
				$buscar->setBusca(array('valor', 'PP.valor'), $_POST['valor'], 'like');
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT PP.id_produto_precos, PP.id_categoria_precos, PP.id_produto, PP.valor, CP.descricao, 
				P.descricao_completa, P.preco_custo, P.preco_custo_servico, UM.sigla 
				FROM produto_precos AS PP 
				INNER JOIN categoria_precos AS CP ON (PP.id_categoria_precos=CP.id_categoria_precos) 
				INNER JOIN produtos AS P ON (PP.id_produto=P.id) 
				INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida)
				{$filtro}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			//$cod_produtos = array(14, 20, 21, 26, 28, 29, 30, 166, 167, 268, 271, 272, 273, 345, 346, 347, 348, 349, 350);
			if(count($query) > 0){
				foreach ($query as $key => $value) {
					$rows[$key] = $value;
					$total_custo = $rows[$key]->preco_custo+$rows[$key]->preco_custo_servico;
					$valor_lucro = $rows[$key]->valor-$total_custo;
					$rows[$key]->margem_lucro = number_format(($valor_lucro/$total_custo)*100, 2, ',', '.');
					$rows[$key]->margem_lucro .= " % (R$ ".number_format($valor_lucro, 2, ',', '.')." POR ".$rows[$key]->sigla.")";
					$rows[$key]->total_custo2 = number_format($total_custo, 2, ',', '.');
					/*
					if (in_array($rows[$key]->id_produto, $cod_produtos) AND $_COOKIE['id_empresa_atual'] == 2){
						$rows[$key]->valor = $rows[$key]->valor + 1.30;
					}
					*/
				}

				$result["dados"] = $query;
			}
			else{
				$sql = "SELECT * FROM categoria_precos";
				$pdo = $connection->prepare($sql);
				$pdo->execute();
				$query = $pdo->fetchAll(PDO::FETCH_OBJ);

				$pdo2 	= $connection->prepare("SELECT * FROM produtos WHERE id = {$_POST['id_produto']}");
				$pdo2->execute();
				$query2 = $pdo2->fetch(PDO::FETCH_OBJ);

				$data = array();
				foreach ($query as $key => $item) {
					if($query2->preco_venda > 0){
						$valor = (float) $query2->preco_venda;
					}
					else{
							$valor = (float) $query2->preco_custo;
					}

					$valor_porcento  = ( 50 / 100 ) * $valor;
					$valor = $valor + $valor_porcento;

					$pdo = $connection->prepare("
					INSERT INTO produto_precos 
						(
							id_categoria_precos,
							id_produto,
							valor,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?
						)
					");

					$params = array(
						$item->id_categoria_precos,
						$query2->id,
						$valor,
						date('Y-m-d H:i:s'), 
						$user_id,
						$user_id,
						date('Y-m-d H:i:s')
					);
					$pdo->execute($params);

					$data[] = array(
						'id_produto_precos' => $connection->lastInsertId(),
						'descricao' => $item->descricao,
						'id_categoria_precos' => $item->id_categoria_precos,
						'descricao_completa' => $query2->descricao_completa,
						'id_produto' => $query2->id,
						'valor' => $valor
					);
				}

				$result["dados"] = $data;
			}

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql, 'data'=> $data) );
		}
	} 
	catch (PDOException $e) {
		$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
		echo json_encode(array('dados'=>array(),'total'=>0, 'sql'=> $sql, 'erro'=>$e->getMessage()));
	}
}
?>