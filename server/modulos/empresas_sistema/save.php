<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'empresas_sistema';
	try {
		if ($_POST['fone1'] == "(__) _____-____") {
			$_POST['fone1'] = NULL;
		}
		if ($_POST['fone2'] == "(__) _____-____") {
			$_POST['fone2'] = NULL;
		}
		if ($_POST['fone3'] == "(__) _____-____") {
			$_POST['fone3'] = NULL;
		}
		if ($_POST['fone4'] == "(__) _____-____") {
			$_POST['fone4'] = NULL;
		}
		if ($_POST['fone5'] == "(__) _____-____") {
			$_POST['fone5'] = NULL;
		}

		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');

			$pdo = $connection->prepare("
					UPDATE empresas_sistema SET 
							razao_social = ?,							
							nome_fantasia = ?,							
							cnpj = ?,							
							pessoa_contato = ?,							
							ramo_atividade = ?,							
							endereco = ?,							
							num_end = ?,							
							complemento = ?,							
							bairro = ?,							
							loteamento = ?,							
							cidade = ?,							
							estado = ?,							
							cep = ?,							
							email = ?,							
							site = ?,							
							fone1 = ?,							
							fone2 = ?,							
							fone3 = ?,							
							fone4 = ?,							
							observacoes = ?,
							alterado_por = ?							
 					WHERE cod_empresa = ?
			");
			$params = array(
				$_POST['razao_social'],
				$_POST['nome_fantasia'],
				$_POST['cnpj'],
				$_POST['pessoa_contato'],
				$_POST['ramo_atividade'],
				$_POST['endereco'],
				$_POST['num_end'],
				$_POST['complemento'],
				$_POST['bairro'],
				$_POST['loteamento'],
				$_POST['cidade'],
				$_POST['estado'],
				$_POST['cep'],
				$_POST['email'],
				$_POST['site'],
				$_POST['fone1'],
				$_POST['fone2'],
				$_POST['fone3'],
				$_POST['fone4'],
				$_POST['observacoes'],
				$user_id,
				$_POST['cod_empresa']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');

			$pdo = $connection->prepare("
				INSERT INTO empresas_sistema 
					(
						razao_social,						
						nome_fantasia,						
						cnpj,						
						pessoa_contato,						
						ramo_atividade,						
						endereco,						
						num_end,						
						complemento,						
						bairro,						
						loteamento,						
						cidade,						
						estado,						
						cep,						
						email,						
						site,						
						fone1,						
						fone2,						
						fone3,						
						fone4,						
						observacoes,						
						data_cadastro,						
						ativo,						
						cadastrado_por						
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['razao_social'],		
				$_POST['nome_fantasia'],		
				$_POST['cnpj'],		
				$_POST['pessoa_contato'],		
				$_POST['ramo_atividade'],		
				$_POST['endereco'],		
				$_POST['num_end'],		
				$_POST['complemento'],		
				$_POST['bairro'],		
				$_POST['loteamento'],		
				$_POST['cidade'],		
				$_POST['estado'],		
				$_POST['cep'],		
				$_POST['email'],		
				$_POST['site'],		
				$_POST['fone1'],		
				$_POST['fone2'],		
				$_POST['fone3'],		
				$_POST['fone4'],		
				$_POST['observacoes'],		
				date('Y-m-d H:i:s'),		
				'S',		
				$user_id		
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}