<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'empresas_sistema';
		$userMain = $user->getUser();
		$user_id = null;
		if($userMain){
			$user_id = $userMain['id'];
		}
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql = "SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM empresas_sistema 
				WHERE cod_empresa=:id";
			$pdo = $connection->prepare($sql);
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('success'=>true, 'dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$sql = "SELECT cod_empresa AS id, CONCAT(UPPER(nome_fantasia), ' / ', UPPER(razao_social)) AS descricao FROM empresas_sistema";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'COMBO_CONTAS_BANCARIAS'){
			$sql = "SELECT 
						empresas_sistema.cod_empresa AS id, CONCAT(cnpj, ' - ', UPPER(razao_social), ' / ', UPPER(nome_fantasia)) AS descricao  
					FROM 
						usuarios_empresas_sistema INNER JOIN empresas_sistema ON (usuarios_empresas_sistema.id_empresa=empresas_sistema.cod_empresa) 
					WHERE 
						empresas_sistema.ativo='S' AND usuarios_empresas_sistema.id_usuario = '$user_id' ORDER BY descricao ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'COMBO_CONTAS_PAGAR'){
			$sql = "SELECT 
						empresas_sistema.cod_empresa AS id, CONCAT(cnpj, ' - ', UPPER(razao_social), ' / ', UPPER(nome_fantasia)) AS descricao  
					FROM 
						usuarios_empresas_sistema INNER JOIN empresas_sistema ON (usuarios_empresas_sistema.id_empresa=empresas_sistema.cod_empresa) 
					WHERE 
						empresas_sistema.ativo='S' AND usuarios_empresas_sistema.id_usuario = '$user_id' ORDER BY descricao ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'COMBO_OS'){
			$pdo = $connection->prepare("SELECT 
						empresas_sistema.cod_empresa AS id, UPPER(nome_fantasia) AS descricao  
					FROM 
						usuarios_empresas_sistema INNER JOIN empresas_sistema ON (usuarios_empresas_sistema.id_empresa=empresas_sistema.cod_empresa) 
					WHERE 
						empresas_sistema.ativo='S' AND usuarios_empresas_sistema.id_usuario = '$user_id' ORDER BY descricao ASC");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('razao_social', 'empresas_sistema.razao_social'), $_POST['razao_social'], 'like');
				$buscar->setBusca(array('nome_fantasia', 'empresas_sistema.nome_fantasia'), $_POST['nome_fantasia'], 'like');
				$buscar->setBusca(array('cnpj', 'empresas_sistema.cnpj'), $_POST['cnpj'], 'like');
				$buscar->setBusca(array('pessoa_contato', 'empresas_sistema.pessoa_contato'), $_POST['pessoa_contato'], 'like');
				$buscar->setBusca(array('ramo_atividade', 'empresas_sistema.ramo_atividade'), $_POST['ramo_atividade'], 'like');
				$buscar->setBusca(array('endereco', 'empresas_sistema.endereco'), $_POST['endereco'], 'like');
				$buscar->setBusca(array('num_end', 'empresas_sistema.num_end'), $_POST['num_end'], 'like');
				$buscar->setBusca(array('complemento', 'empresas_sistema.complemento'), $_POST['complemento'], 'like');
				$buscar->setBusca(array('bairro', 'empresas_sistema.bairro'), $_POST['bairro'], 'like');
				$buscar->setBusca(array('loteamento', 'empresas_sistema.loteamento'), $_POST['loteamento'], 'like');
				$buscar->setBusca(array('cidade', 'empresas_sistema.cidade'), $_POST['cidade'], 'like');
				$buscar->setBusca(array('estado', 'empresas_sistema.estado'), $_POST['estado'], 'like');
				$buscar->setBusca(array('cep', 'empresas_sistema.cep'), $_POST['cep'], 'like');
				$buscar->setBusca(array('email', 'empresas_sistema.email'), $_POST['email'], 'like');
				$buscar->setBusca(array('site', 'empresas_sistema.site'), $_POST['site'], 'like');
				$buscar->setBusca(array('fone1', 'empresas_sistema.fone1'), $_POST['fone1'], 'like');
				$buscar->setBusca(array('fone2', 'empresas_sistema.fone2'), $_POST['fone2'], 'like');
				$buscar->setBusca(array('fone3', 'empresas_sistema.fone3'), $_POST['fone3'], 'like');
				$buscar->setBusca(array('fone4', 'empresas_sistema.fone4'), $_POST['fone4'], 'like');
				$buscar->setBusca(array('observacoes', 'empresas_sistema.observacoes'), $_POST['observacoes'], 'like');
			}
			
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("
				SELECT count(*) AS total 
				FROM empresas_sistema 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$pdo = $connection->prepare("
				SELECT empresas_sistema.*, 
					DATE_FORMAT(empresas_sistema.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(empresas_sistema.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM empresas_sistema 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>