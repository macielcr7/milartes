<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'fornecedores_anotacoes';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		
		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
					UPDATE fornecedores_anotacoes SET 
							cod_fornecedor = ?,							
							anotacao = ?,							
							alterado_por = ?,							
							ativo = ?							
 					WHERE controle = ?
			");
			$params = array(
				$_POST['cod_fornecedor'],
				$_POST['anotacao'],
				$user_id,
				$_POST['ativo'],
				$_POST['controle']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
		
			$pdo = $connection->prepare("
				INSERT INTO fornecedores_anotacoes 
					(
						cod_fornecedor,						
						anotacao,						
						cadastrado_por,						
						data_cadastro,						
						alterado_por,						
						ativo						
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?			
					)
			");
			$params = array(
				$_POST['cod_fornecedor'],		
				$_POST['anotacao'],		
				$user_id,
				date('Y-m-d H:i:s'),		
				$user_id,
				$_POST['ativo']		
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}