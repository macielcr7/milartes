<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'vendas_custos';

		if (isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES'){
			$pdo = $connection->prepare("SELECT * FROM vendas_custos WHERE id=:id");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif (isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_CUSTO'){
			//$query = "WHERE (P.tipo_produto='A' OR P.tipo_produto='P' OR P.tipo_produto='S') AND 1 = 1";
			$query = "WHERE P.possui_composicao='N' AND 1 = 1";

			if (isset($_POST['query']) AND !empty($_POST['query'])) {
				$cada_palavra = addslashes(htmlspecialchars($_POST['query']));
				$palavras = explode(' ',$cada_palavra);
				$total_palavras = count($palavras);

				$i = 0;
				while ($i != $total_palavras)
				{
					$query .= " AND P.descricao_completa LIKE '%$palavras[$i]%'";
					$i++;
				}
			}

			$sql1 = "SELECT P.id, P.tipo_produto, P.preco_custo, P.dobra_quantidade, CONCAT(UPPER(P.descricao_completa), ' - ', UPPER(UM.sigla)) AS descricao 
				FROM produtos AS P 
				INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
				{$query} AND P.ativo='S' ORDER BY descricao ASC
			";
			$pdo1 = $connection->prepare($sql1);
			$pdo1->execute();
			$linhas = $pdo1->fetchAll(PDO::FETCH_ASSOC);

			foreach ($linhas as $key => $value) {
				//cores do produto
				$sql4 = "SELECT PC.id_cor, C.descricao, C.codigo FROM produtos_cores AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_produto) 
					INNER JOIN cores AS C ON (C.id_cores=PC.id_cor) 
					WHERE PC.id_produto = {$value['id']}
				";
				$pdo4 = $connection->prepare($sql4);
				$pdo4->execute();
				$linhas[$key]['produto_cores'] = $pdo4->fetchAll(PDO::FETCH_ASSOC);
			}

			$sql1 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql1);
			$sql4 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql4);
			echo json_encode(array('dados'=>$linhas, 'sql1'=>$sql1, 'sql4'=>$sql4));
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			$buscar->setBusca(array('cod_venda', 'vendas_custos.cod_venda'), $_POST['cod_venda']);

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM vendas_custos {$filtro}";
			$pdo = $connection->prepare($sql);
			$pdo->execute($buscar->getArrayExecute());

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql2 = "SELECT vendas_custos.*, UPPER(produtos.descricao_completa) AS produto, 
				UPPER(unidades_medidas.sigla) AS sigla_um, 
				CONCAT(REPLACE(REPLACE(REPLACE(FORMAT(vendas_custos.quantidade, 2), '.', '|'), ',', '.'), '|', ','), ' ', UPPER(unidades_medidas.sigla)) AS quantidade2, 
				cores.descricao AS cor, 
				cores.codigo AS css_cor, 
				SUM(vendas_custos.valor_unitario+vendas_custos.acrescimo_mo+vendas_custos.acrescimo_produto) AS valor_unitario, 
				SUM((vendas_custos.valor_unitario+vendas_custos.acrescimo_mo+vendas_custos.acrescimo_produto)*vendas_custos.quantidade) AS valor_total 
				FROM vendas_custos 
				INNER JOIN produtos ON (produtos.id=vendas_custos.cod_produto) 
				INNER JOIN unidades_medidas ON (unidades_medidas.id=produtos.unidade_medida) 
				LEFT JOIN produtos_cores ON (vendas_custos.cod_cor=produtos_cores.id_produtos_cores) 
				LEFT JOIN cores ON (produtos_cores.id_cor=cores.id_cores) 
				{$filtro} 
				GROUP BY vendas_custos.id 
				ORDER BY vendas_custos.id ASC";
			$pdo = $connection->prepare($sql2);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$rows[$key] = $value;
			}

			$pdo = $connection->prepare("SELECT valor_total, venda_cod_instalador, venda_data_instalacao_inicio, venda_data_instalacao_fim FROM orcamentos WHERE id_orcamento = '{$_POST['cod_venda']}'");
			$pdo->execute();
			$venda = $pdo->fetch(PDO::FETCH_OBJ);

			$query[$key]->valor_liquido_venda = $venda->valor_total;
			$query[$key]->cod_instalador = $venda->venda_cod_instalador;
			$query[$key]->data_instalacao_inicio = $venda->venda_data_instalacao_inicio;
			$query[$key]->data_instalacao_fim = $venda->venda_data_instalacao_fim;

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), $sql2);
			//echo json_encode( array('dados'=>$query, 'total'=>$countRow, 'sql2'=>$sql2) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>