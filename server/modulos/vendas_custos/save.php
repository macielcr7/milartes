<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'vendas_custos';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_atual = date('Y-m-d H:i:s');
	try {
		if(empty($_POST['quantidade'])) {
			$quantidade = 0;
		} else {
			$quantidade = implode('.', explode(',', $_POST['quantidade']));
		}

		if(empty($_POST['acrescimo_mo'])) {
			$acrescimo_mo = 0;
		} else {
			$acrescimo_mo = implode('.', explode(',', $_POST['acrescimo_mo']));
		}

		if(empty($_POST['acrescimo_produto'])) {
			$acrescimo_produto = 0;
		} else {
			$acrescimo_produto = implode('.', explode(',', $_POST['acrescimo_produto']));
		}

		if(empty($_POST['id_cor'])) {
			$_POST['id_cor'] = 0;
		}

		$valor_unitario = implode('.', explode(',', $_POST['valor_unitario']));
		$valor_total = ($valor_unitario * $quantidade);

		$nova_observacao = mb_strtoupper($_POST['observacoes'], 'UTF-8');
		$nova_observacao = ReformatString($nova_observacao);

		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE vendas_custos SET 
							cod_venda = ?, 
							cod_item_venda = ?, 
							cod_produto = ?, 
							cod_cor = ?, 
							quantidade = ?,
							valor_unitario = ?, 
							valor_total = ?, 
							acrescimo_mo = ?, 
							acrescimo_produto = ?, 
							cod_instalador = ?, 
							alterado_por = ?, 
							data_alteracao = ?, 
							observacoes = ? 
 					WHERE id = ?
			");
			$params = array(
				$_POST['cod_venda'],
				$_POST['cod_item_venda'],
				$_POST['cod_produto'],
				$_POST['id_cor'],
				$quantidade,
				$valor_unitario,
				$valor_total,
				$acrescimo_mo,
				$acrescimo_produto,
				$_POST['cod_instalador'],
				$user_id,
				date('Y-m-d H:i:s'),
				$nova_observacao,
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			//$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO vendas_custos 
					(
						cod_venda, 
						cod_item_venda, 
						cod_produto, 
						cod_cor, 
						quantidade, 
						valor_unitario, 
						valor_total, 
						acrescimo_mo, 
						acrescimo_produto, 
						cod_instalador, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao, 
						observacoes
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_venda'],
				$_POST['cod_item_venda'],
				$_POST['cod_produto'],
				$_POST['id_cor'],
				$quantidade,
				$valor_unitario,
				$valor_total,
				$acrescimo_mo,
				$acrescimo_produto,
				$_POST['cod_instalador'],
				date('Y-m-d H:i:s'), 
				$user_id,
				$user_id,
				date('Y-m-d H:i:s'),
				$nova_observacao
			);
			$pdo->execute($params);
			$ultimo_id = $connection->lastInsertId();
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}