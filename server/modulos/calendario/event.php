<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_GET){
	try {
		require('../../autoLoad.php');
		$tabela = 'eventos';

		$json = file_get_contents('php://input');
    	$event = json_decode($json, TRUE);

		if(isset($_GET['action']) AND $_GET['action'] == 'load'){
			$start_dt = isset($_REQUEST['startDate']) ? strtolower($_REQUEST['startDate']) : null;
   			$end_dt = isset($_REQUEST['endDate']) ? strtolower($_REQUEST['endDate']) : null;

			if (isset($end_dt)) {
				$endDate = new DateTime($end_dt);
				$endDate->modify('+1 day');
				$end_dt = $endDate->format('Y-m-d');
			}

			$pdo = $connection->prepare("
				SELECT *, operador_id as calendar_id FROM eventos 
				WHERE ((start >= :start AND start <= :end)
				 OR (end >= :start AND end <= :end)       
				 OR (start <= :start AND end >= :end))
			");
			$pdo->execute(array(
				':start'  => $start_dt,
				':end'    => $end_dt
			));
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_GET['action']) AND $_GET['action'] == 'add'){
			$json = file_get_contents('php://input');
    		$event = json_decode($json, TRUE);
			
			$event['start'] = explode('T', $event['start']);
			$event['start'][1] = explode('-', $event['start'][1])[0];
			$event['start'] = implode(' ', $event['start']);

			$event['end'] = explode('T', $event['end']);
			$event['end'][1] = explode('-', $event['end'][1])[0];
			$event['end'] = implode(' ', $event['end']);

			$pdo = $connection->prepare("
				INSERT INTO eventos (
					title,
					start,
					end,
					all_day,
					operador_id,
					location,
					notes,
					reminder,
					url
				) 
				VALUES (
					?, ?, ?, ?, ?, ?, ?, ?, ?
				);
			");
			$pdo->execute(
				array(
					$event['title'],
					$event['start'],
					$event['end'],
					$event['all_day'] == true ? 1 : 0,
					$event['calendar_id'],
					$event['location'],
					$event['notes'],
					$event['reminder'],
					$event['url']
				)
			);
			echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS, 'data'=> $connection->lastInsertId()));
		}
		else  if(isset($_GET['action']) AND $_GET['action'] == 'update'){
			$json = file_get_contents('php://input');
    		$event = json_decode($json, TRUE);
			
			$event['start'] = explode('T', $event['start']);
			$event['start'][1] = explode('-', $event['start'][1])[0];
			$event['start'] = implode(' ', $event['start']);

			$event['end'] = explode('T', $event['end']);
			$event['end'][1] = explode('-', $event['end'][1])[0];
			$event['end'] = implode(' ', $event['end']);

			$pdo = $connection->prepare("
				UPDATE eventos SET
					title = ?,
					start = ?,
					end = ?,
					all_day = ?,
					operador_id = ?,
					location = ?,
					notes = ?,
					reminder = ?,
					url = ?
				WHERE id = ?
			");
			$pdo->execute(
				array(
					$event['title'],
					$event['start'],
					$event['end'],
					$event['all_day'] == true ? 1 : 0,
					$event['calendar_id'],
					$event['location'],
					$event['notes'],
					$event['reminder'],
					$event['url'],
					$event['id']
				)
			);
			echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS, 'data'=> $connection->lastInsertId()));
		}
		else  if(isset($_GET['action']) AND $_GET['action'] == 'delete'){
			$json = file_get_contents('php://input');
    		$event = json_decode($json, TRUE);
			
			$event['start'] = explode('T', $event['start']);
			$event['start'][1] = explode('-', $event['start'][1])[0];
			$event['start'] = implode(' ', $event['start']);

			$event['end'] = explode('T', $event['end']);
			$event['end'][1] = explode('-', $event['end'][1])[0];
			$event['end'] = implode(' ', $event['end']);

			$pdo = $connection->prepare("
				DELETE FROM eventos WHERE id = ?
			");
			$pdo->execute(
				array(
					$event['id']
				)
			);
			echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS, 'data'=> $connection->lastInsertId()));
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>