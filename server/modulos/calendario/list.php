<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_GET){
	try {
		require('../../autoLoad.php');
		$tabela = 'operadores';

		$result = array();
		$sql = "SELECT * from {$tabela}";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetchAll(PDO::FETCH_OBJ);

		$result["total"] = count($query);
		$result["dados"] = $query;

		echo json_encode($result);
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>