<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'agenda_telefonica';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT AT.cod_contato, 
					AT.tipo_contato,
					AT.nome_completo,
					AT.razao_social, 
					AT.nome_fantasia,
					AT.pessoa_contato,
					AT.sexo,
					AT.profissao,
					AT.estado,
					AT.cidade,
					AT.bairro,
					AT.endereco,
					AT.nro_end,
					AT.loteamento,
					AT.complemento,
					AT.ponto_ref,
					AT.cx_postal,
					AT.cep,
					AT.ativo,
					AT.observacoes,
					AT.palavras_chaves, 
					CONCAT(UPPER(U.nome), ' EM: ', DATE_FORMAT(AT.data_cadastro, '%d/%m/%Y'), ' ', DATE_FORMAT(AT.data_cadastro, '%H:%i:%s')) as cadastrado_por,
					CONCAT(UPPER(U2.nome), ' EM: ', DATE_FORMAT(AT.data_alteracao, '%d/%m/%Y'), ' ', DATE_FORMAT(AT.data_alteracao, '%H:%i:%s')) as alterado_por 
				FROM agenda_telefonica AS AT
				LEFT JOIN usuarios AS U ON (U.id=AT.cadastrado_por)
				LEFT JOIN usuarios AS U2 ON (U2.id=AT.alterado_por)
				WHERE cod_contato=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_PROFISSAO'){
			$buscar->setBusca(array('profissao', 'AT.profissao'), $_POST['query'], 'like');

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT AT.profissao AS id, AT.profissao AS descricao 
				FROM agenda_telefonica AS AT
				{$filtro} 
				GROUP BY AT.profissao
				ORDER BY AT.descricao ASC
			");
			$pdo->execute($buscar->getArrayExecute());
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['query']) and !empty($_POST['query'])){
				$buscar->setBusca(array('cod_contato', 'AT.cod_contato'), $_POST['query'], 'like');
				$buscar->setBusca(array('nome_completo', 'AT.nome_completo'), $_POST['query'], 'like');
				$buscar->setBusca(array('razao_social', 'AT.razao_social'), $_POST['query'], 'like');
				$buscar->setBusca(array('nome_fantasia', 'AT.nome_fantasia'), $_POST['query'], 'like');
				$buscar->setBusca(array('estado', 'CUF.uf'), $_POST['query'], 'like');
				$buscar->setBusca(array('cidade', 'CL.loc_nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('bairro', 'CB.bairro_nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('endereco', 'CE.nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('palavras_chaves', 'AT.palavras_chaves'), $_POST['query'], 'like');
				$filtro = $buscar->filtro." (".implode($buscar->sql, " OR ").") AND AT.ativo = 'S' ";
			}
			else {
				$filtro = $buscar->filtro." AT.ativo = 'S' ";
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			if ($sort == 'nome_completo') {
				$sort = 'AT.nome_completo '.$order.', AT.razao_social '.$order.', AT.nome_fantasia '.$order;
			}
			else {
				$sort = $sort.' '.$order;
			}


			$pdo = $connection->prepare("SELECT count(*) AS total 
				FROM agenda_telefonica AS AT 
				LEFT JOIN correios_estados AS CUF ON (AT.estado=CUF.uf) 
				LEFT JOIN correios_localidades AS CL ON (AT.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (AT.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (AT.endereco=CE.id) 
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT AT.cod_contato, 
					AT.tipo_contato, 
					UPPER(AT.nome_completo) AS nome_completo, 
					UPPER(AT.razao_social) AS razao_social, 
					UPPER(AT.nome_fantasia) AS nome_fantasia, 
					AT.nro_end, 
					AT.loteamento, 
					AT.ativo,
					AT.palavras_chaves, 
					DATE_FORMAT(AT.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(AT.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(AT.data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(AT.data_alteracao, '%Y-%m-%d') as data_alteracao_date, 
					CONCAT(UPPER(CL.loc_nome), ' / ', CUF.uf) AS loc_nome, 

					CASE WHEN
						AT.complemento != '' THEN CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end, ' (', UPPER(AT.complemento), ')')
					ELSE
						CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end)
					END AS nome,

					CASE WHEN
						AT.loteamento != '' THEN CONCAT(UPPER(CB.bairro_nome), ' (', UPPER(AT.loteamento), ')') 
					ELSE
						UPPER(CB.bairro_nome)
					END AS bairro_nome, 
					(SELECT GROUP_CONCAT(CONCAT(ATC.descricao, ' ', UPPER(ATC.observacao)) SEPARATOR '|') AS fone FROM agenda_telefonica_contato AS ATC WHERE ATC.cod_contato=AT.cod_contato AND (ATC.tipo_contato='TELEFONE' OR ATC.tipo_contato='DDG') ORDER BY ATC.controle ASC) AS fones 
				FROM agenda_telefonica AS AT
				LEFT JOIN correios_estados AS CUF ON (AT.estado=CUF.uf) 
				LEFT JOIN correios_localidades AS CL ON (AT.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (AT.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (AT.endereco=CE.id) 
				{$filtro} 
				ORDER BY {$sort} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			foreach ($query as $key => $value) {
				$query[$key] = $value;
				if($query[$key]->fones != "") {
					$palavras = explode('|' , $query[$key]->fones);
					$total_palavras = count($palavras);

					$fones = "";
					foreach($palavras as $i => $key2) {
						$nro = explode(' ' , $key2);
						$caracteres = strlen($nro[0]);

						if ($caracteres == 10){
							$fones .= '(' . substr($key2, 0, 2) . ') ' . substr($key2, 2, 4) . '-' . substr($key2, 6);
						}
						else{
							$fones .= substr($key2, 0, 4) . ' ' . substr($key2, 4, 3) . '-' . substr($key2, 7);
						}

						if ($i != $total_palavras-1) {
							$fones .= "</br>";
							$caracteres = 0;
						}
					}
					$query[$key]->fones = $fones;
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>