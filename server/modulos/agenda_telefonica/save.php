<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'agenda_telefonica';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {

		if ($_POST['cep'] == "__.___-___") {
			$cep = "";
		} else {
			$cep = $_POST['cep'];
		}
		if (empty($_POST['estado'])) {
			$estado = NULL;
		} else {
			$estado = $_POST['estado'];
		}
			if (empty($_POST['bairro'])) {
			$bairro = NULL;
		} else {
			$bairro = $_POST['bairro'];
		}

		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE agenda_telefonica SET 
							tipo_contato = ?, 
							nome_completo = ?, 
							razao_social = ?, 
							nome_fantasia = ?, 
							pessoa_contato = ?, 
							sexo = ?, 
							profissao = ?, 
							estado = ?, 
							cidade = ?, 
							bairro = ?, 
							endereco = ?, 
							nro_end = ?, 
							loteamento = ?, 
							complemento = ?, 
							ponto_ref = ?, 
							cx_postal = ?,
							cep = ?, 
							palavras_chaves = ?,
							ativo = ?,
							alterado_por = ?, 
							data_alteracao = ?, 
							observacoes = ? 
 					WHERE cod_contato = ?
			");
			$params = array(
				mb_strtoupper($_POST['tipo_contato'], 'UTF-8'),
				mb_strtoupper($_POST['nome_completo'], 'UTF-8'),
				mb_strtoupper($_POST['razao_social'], 'UTF-8'),
				mb_strtoupper($_POST['nome_fantasia'], 'UTF-8'),
				mb_strtoupper($_POST['pessoa_contato'], 'UTF-8'),
				mb_strtoupper($_POST['sexo'], 'UTF-8'),
				mb_strtoupper($_POST['profissao'], 'UTF-8'),
				$estado,
				$_POST['cidade'], 
				$bairro, 
				$_POST['endereco'], 
				$_POST['nro_end'],
				mb_strtoupper($_POST['loteamento'], 'UTF-8'),
				mb_strtoupper($_POST['complemento'], 'UTF-8'),
				mb_strtoupper($_POST['ponto_ref'], 'UTF-8'),
				mb_strtoupper($_POST['cx_postal'], 'UTF-8'),
				$cep, 
				$_POST['palavras_chaves'],
				$_POST['ativo'],
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['observacoes'],
				$_POST['cod_contato']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO agenda_telefonica 
					(
						tipo_contato, 
						nome_completo, 
						razao_social, 
						nome_fantasia, 
						pessoa_contato, 
						sexo, 
						profissao, 
						estado, 
						cidade, 
						bairro, 
						endereco, 
						nro_end, 
						loteamento, 
						complemento, 
						ponto_ref, 
						cx_postal, 
						cep,
						palavras_chaves,
						ativo,
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao, 
						observacoes
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['tipo_contato'], 'UTF-8'),
				mb_strtoupper($_POST['nome_completo'], 'UTF-8'),
				mb_strtoupper($_POST['razao_social'], 'UTF-8'),
				mb_strtoupper($_POST['nome_fantasia'], 'UTF-8'),
				mb_strtoupper($_POST['pessoa_contato'], 'UTF-8'),
				mb_strtoupper($_POST['sexo'], 'UTF-8'),
				mb_strtoupper($_POST['profissao'], 'UTF-8'),
				$estado,
				$_POST['cidade'], 
				$bairro, 
				$_POST['endereco'], 
				$_POST['nro_end'],
				mb_strtoupper($_POST['loteamento'], 'UTF-8'),
				mb_strtoupper($_POST['complemento'], 'UTF-8'),
				mb_strtoupper($_POST['ponto_ref'], 'UTF-8'),
				mb_strtoupper($_POST['cx_postal'], 'UTF-8'),
				$cep, 
				$_POST['palavras_chaves'],
				$_POST['ativo'],
				date('Y-m-d H:i:s'), 
				$user_id,
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['observacoes']
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS, 'cod_contato'=> $connection->lastInsertId()));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA."<br><br>".$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}