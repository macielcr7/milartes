
<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_atual = date('Y-m-d H:i:s');
	try {
		if(empty($_POST['quantidade'])) {
			$quantidade = 0;
		} else {
			$quantidade = implode('.', explode(',', $_POST['quantidade']));
		}

		if(empty($_POST['id_produtos_orcamentos_forro_pai']) OR $_POST['id_produtos_orcamentos_forro_pai'] == '') {
			$_POST['id_produtos_orcamentos_forro_pai'] = 0;
		}

		if(empty($_POST['id_cor'])) {
			$_POST['id_cor'] = 0;
		}

		if(empty($_POST['possui_ambiente'])) {
			$_POST['possui_ambiente'] = 'N';
		}

		//pega os valores dos custos do produto
		$pdoX = $connection->prepare("SELECT tipo_produto, possui_composicao, preco_custo AS preco_custo_produto, preco_custo_servico, dobra_quantidade FROM produtos WHERE id = '{$_POST['id_produto']}'");
		$pdoX->execute();
		$custos = $pdoX->fetch(PDO::FETCH_OBJ);
		$preco_custo_produto = $custos->preco_custo_produto;
		//$preco_custo_servico = $custos->preco_custo_servico;

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
					UPDATE produtos_orcamentos_forro SET 
							id_produtos_orcamentos_forro_pai = ?, 
							tipo_produto = ?, 
							id_produto = ?, 
							id_cor = ?, 
							quantidade = ?, 
							valor_unitario = ?, 
							valor_bruto = ?, 
							valor_total = ?, 
							possui_ambiente = ?, 
							item_nro = ? 
 					WHERE id_produtos_orcamentos_forro = ?
			");
			$params = array(
				$_POST['id_produtos_orcamentos_forro_pai'],
				$_POST['tipo_produto'],
				$_POST['id_produto'],
				$_POST['id_cor'],
				$quantidade,
				$_POST['valor_unitario'],
				$_POST['valor_bruto'],
				$_POST['valor_total'],
				$_POST['possui_ambiente'],
				$_POST['item_nro'],
				$_POST['id_produtos_orcamentos_forro']
			);
			$pdo->execute($params);

			//atualiza os custos
			$sql4 = "DELETE FROM vendas_custos WHERE cod_item_venda = '{$_POST['id_produtos_orcamentos_forro']}'";
				$pdo4 = $connection->prepare($sql4);
				$pdo4->execute();

			if ($custos->possui_composicao == 'S') {
				$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo 
					FROM produto_composicoes AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
					INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida)
					WHERE PC.id_produto = '{$_POST['id_produto']}'";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();

				while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
				{
					//calcula a quantidade
					if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
						$nova_qtde = ceil($quantidade * $item['quantidade']);
					} else {
						$nova_qtde = ($quantidade * $item['quantidade']);
					}

					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$_POST['id_orcamento'],
						$_POST['id_produtos_orcamentos_forro'],
						$item['id_composicao'],
						$_POST['id_cor'],
						$nova_qtde,
						$item['preco_custo'],
						0,
						0,
						($nova_qtde * $item['preco_custo']),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			} else {
				$pdo2 = $connection->prepare("
					INSERT INTO vendas_custos 
						(
							cod_venda, 
							cod_item_venda, 
							cod_produto, 
							cod_cor, 
							quantidade, 
							valor_unitario, 
							acrescimo_mo, 
							acrescimo_produto, 
							valor_total,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
				");
				$params2 = array(
					$_POST['id_orcamento'],
					$_POST['id_produtos_orcamentos_forro'],
					$_POST['id_produto'],
					$_POST['id_cor'],
					$quantidade,
					$preco_custo_produto,
					0,
					0,
					($quantidade * $preco_custo_produto),
					date('Y-m-d H:i:s'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'),
					$observacoes
				);
				$pdo2->execute($params2);
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			$sqlC = "SELECT count(*) FROM produtos_orcamentos_forro WHERE id_orcamento='{$_POST['id_orcamento']}'";
			$pdoC = $connection->prepare($sqlC);
			$pdoC->execute();
			$num_rowsC = $pdoC->fetchColumn();

			if($num_rowsC == 0){
				$nro_item = 1;
			} else {
				$nro_item = $num_rowsC + 1;
			}

			$pdo = $connection->prepare("
				INSERT INTO produtos_orcamentos_forro 
					(
						id_produtos_orcamentos_forro_pai,
						tipo_produto,
						id_orcamento, 
						id_produto, 
						id_cor, 
						quantidade, 
						valor_unitario, 
						valor_bruto, 
						valor_total, 
						possui_ambiente, 
						item_nro
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['id_produtos_orcamentos_forro_pai'],
				$_POST['tipo_produto'],
				$_POST['id_orcamento'],
				$_POST['id_produto'],
				$_POST['id_cor'],
				$quantidade,
				$_POST['valor_unitario'],
				$_POST['valor_bruto'],
				$_POST['valor_total'],
				$_POST['possui_ambiente'],
				$nro_item
			);
			$pdo->execute($params);

			$pdo = $connection->prepare("
				SELECT * FROM produtos_orcamentos_forro ORDER BY id_produtos_orcamentos_forro DESC LIMIT 1
			");
			$pdo->execute();

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$_POST['id_produtos_orcamentos_forro'] = $query->id_produtos_orcamentos_forro;
			$_POST['item_nro'] = $nro_item;

			//gera os custos
			if ($custos->possui_composicao == 'S') {
				$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo 
					FROM produto_composicoes AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
					INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida)
					WHERE PC.id_produto = '{$_POST['id_produto']}'";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();

				while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
				{
					//calcula a quantidade
					if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
						$nova_qtde = ceil($quantidade * $item['quantidade']);
					} else {
						$nova_qtde = ($quantidade * $item['quantidade']);
					}

					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$_POST['id_orcamento'],
						$query->id_produtos_orcamentos_forro,
						$item['id_composicao'],
						$_POST['id_cor'],
						$nova_qtde,
						$item['preco_custo'],
						0,
						0,
						($nova_qtde * $item['preco_custo']),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			} else {
				$pdo2 = $connection->prepare("
					INSERT INTO vendas_custos 
						(
							cod_venda, 
							cod_item_venda, 
							cod_produto, 
							cod_cor, 
							quantidade, 
							valor_unitario, 
							acrescimo_mo, 
							acrescimo_produto, 
							valor_total,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
				");
				$params2 = array(
					$_POST['id_orcamento'],
					$query->id_produtos_orcamentos_forro,
					$_POST['id_produto'],
					$_POST['id_cor'],
					$quantidade,
					$preco_custo_produto,
					0,
					0,
					($quantidade * $preco_custo_produto),
					date('Y-m-d H:i:s'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'),
					$observacoes
				);
				$pdo2->execute($params2);
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

		if(in_array($_POST['action'], array('INSERIR', 'EDITAR')) and isset($_POST['ambientes'])){
			//$ambientes = json_decode(stripcslashes('['.$_POST['ambientes'].']'), true);
			$ambientes = json_decode('['.$_POST['ambientes'].']', true);
			foreach ($ambientes as $key => $item) {
				$nome_ambiente = mb_strtoupper(trim($item['ambiente']));
				$nome_ambiente = mb_strtoupper($nome_ambiente, 'UTF-8');
				$obs_ambiente = mb_strtoupper(trim($item['observacoes']));
				$obs_ambiente = mb_strtoupper($obs_ambiente, 'UTF-8');

				$sqlA = "SELECT count(*) FROM ambientes_nomes WHERE descricao='$nome_ambiente'";
				$pdoA = $connection->prepare($sqlA);
				$pdoA->execute();
				$num_rows = $pdoA->fetchColumn();

				if($num_rows < 1){
					$sqlB = "INSERT INTO ambientes_nomes (descricao, cadastrado_por, alterado_por, data_cadastro, data_alteracao) VALUES ('$nome_ambiente', $user_id, $user_id, '$data_atual', '$data_atual')";
					$pdoB = $connection->prepare($sqlB);
					$pdoB->execute();
				}


				if($item['id_ambientes_orcamentos_forro']=='0'){
					if($item['sn_l']=='S'){
						$pdoAmbientePai = $connection->prepare("
							SELECT id_ambientes_orcamentos_forro FROM ambientes_orcamentos_forro
							WHERE sn_l = 'N' AND id_produtos_orcamentos_forro = {$_POST['id_produtos_orcamentos_forro']} ORDER BY id_ambientes_orcamentos_forro DESC LIMIT 1;
						");		
						$pdoAmbientePai->execute();		
						$ambientePai = $pdoAmbientePai->fetch(PDO::FETCH_OBJ);
						if($ambientePai and $ambientePai->id_ambientes_orcamentos_forro){
							$item['id_ambiente_pai'] = $ambientePai->id_ambientes_orcamentos_forro;
						}
						else{
							$item['id_ambiente_pai'] = 0;
							$item['sn_l'] = 'N';
						}
					}
					else{
						$item['id_ambiente_pai'] = 0;
						$item['sn_l'] = 'N';
					}


					$pdo = $connection->prepare("
						INSERT INTO ambientes_orcamentos_forro 
							(
								id_produtos_orcamentos_forro, 
								tipo_acessorio, 
								ambiente, 
								qtd_pecas, 
								qtd_metros, 
								comprimento, 
								comprimento_real, 
								largura,
								largura_real,
								area,
								sn_l,
								id_ambiente_pai,
								descricao_pecas,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params = array(
						$_POST['id_produtos_orcamentos_forro'],
						$item['tipo_acessorio'],
						$nome_ambiente,
						$item['qtd_pecas'],
						$item['qtd_metros'],
						$item['comprimento'],
						$item['comprimento_real'],
						$item['largura'],
						$item['largura_real'],
						$item['area'],
						$item['sn_l'],
						$item['id_ambiente_pai'],
						$item['descricao_pecas'],
						date('Y-m-d H:i:s'), 
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$obs_ambiente
					);
					$pdo->execute($params);


					$pdo = $connection->prepare("
						SELECT * FROM ambientes_orcamentos_forro order by id_ambientes_orcamentos_forro desc LIMIT 1
					");
					$pdo->execute();

					$query = $pdo->fetch(PDO::FETCH_OBJ);
					$_POST['id_ambientes_orcamentos_forro'] = $query->id_ambientes_orcamentos_forro;

					if (!empty($item['observacoes'])){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$obs_ambiente;
						$sqlD = "INSERT INTO orcamentos_observacoes 
							(
								orcamento_id, 
								ambiente_id, 
								nro_item, 
								descricao, 
								tipo, 
								cadastrado_por, 
								alterado_por, 
								data_cadastro, 
								data_alteracao
							) 
						VALUES 
							(
								'{$_POST['id_orcamento']}', 
								'{$_POST['id_ambientes_orcamentos_forro']}', 
								'{$_POST['item_nro']}', 
								'{$novo_texto_obs}', 
								'G', 
								'{$user_id}', 
								'{$user_id}', 
								'{$data_atual}', 
								'{$data_atual}'
							)";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}
				}	
				else{
					//pega a observacao antiga
					$sqlE = "SELECT descricao FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND ambiente_id={$item['id_ambientes_orcamentos_forro']}";
					$pdoE = $connection->prepare($sqlE);
					$pdoE->execute();
					$obs_antigo = $pdoE->fetchColumn();

					$nova_observacao = $obs_ambiente;
					$pdo = $connection->prepare("
							UPDATE ambientes_orcamentos_forro SET 
									tipo_acessorio = ?,
									ambiente = ?,
									qtd_pecas = ?,
									qtd_metros = ?,
									comprimento = ?,
									comprimento_real = ?,
									largura = ?,
									largura_real = ?,
									area = ?,
									sn_l = ?,
									id_ambiente_pai = ?,
									descricao_pecas = ?,
									alterado_por = ?, 
									data_alteracao = ?, 
									observacoes = ? 
		 					WHERE id_ambientes_orcamentos_forro = ?
					");
					$params = array(
						$item['tipo_acessorio'],
						$nome_ambiente,
						$item['qtd_pecas'],
						$item['qtd_metros'],
						$item['comprimento'],
						$item['comprimento_real'],
						$item['largura'],
						$item['largura_real'],
						$item['area'],
						$item['sn_l'],
						$item['id_ambiente_pai'],
						$item['descricao_pecas'],
						$user_id,
						date('Y-m-d H:i:s'),
						$nova_observacao,
						$item['id_ambientes_orcamentos_forro']
					);
					$pdo->execute($params);

					//atualiza a observaçao
					//se a obs antiga for em branco e a nova tem algum valor.... insere
					if (empty($obs_antigo) AND !empty($nova_observacao)){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$nova_observacao;
						$sqlD = "INSERT INTO orcamentos_observacoes 
							(
								orcamento_id, 
								ambiente_id, 
								nro_item, 
								descricao, 
								tipo, 
								cadastrado_por, 
								alterado_por, 
								data_cadastro, 
								data_alteracao
							) 
						VALUES 
							(
								'{$_POST['id_orcamento']}', 
								'{$item['id_ambientes_orcamentos_forro']}', 
								'{$_POST['item_nro']}', 
								'{$novo_texto_obs}', 
								'G', 
								'{$user_id}', 
								'{$user_id}', 
								'{$data_atual}', 
								'{$data_atual}'
							)";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}

					//se a antiga tiver algum valor e for diferente da nova... update
					if (!empty($obs_antigo) AND ($obs_antigo != $nova_observacao)){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$nova_observacao;
						$sqlD = "UPDATE orcamentos_observacoes SET descricao='{$novo_texto_obs}', alterado_por='{$user_id}', data_alteracao='{$data_atual}' WHERE orcamento_id='{$_POST['id_orcamento']}' AND ambiente_id={$item['id_ambientes_orcamentos_forro']}";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}

					//se a antiga tiver algum valor e a nova for em branco.... apaga
					if (!empty($obs_antigo) AND empty($nova_observacao)){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$nova_observacao;
						$sqlD = "DELETE FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND ambiente_id={$item['id_ambientes_orcamentos_forro']}";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}
				}
			}
		}

		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}