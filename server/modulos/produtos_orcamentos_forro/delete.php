<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	
	try {
		if($_POST['action'] == 'DELETAR'){
			$pdo = $connection->prepare("DELETE FROM ambientes_orcamentos_forro WHERE id_produtos_orcamentos_forro = ?");
			$pdo->execute(array(
				$_POST['id']
			));

			$pdo2 = $connection->prepare("DELETE FROM produtos_orcamentos_forro WHERE id_produtos_orcamentos_forro = ?");
			$pdo2->execute(array(
				$_POST['id']
			));

			$pdo3 = $connection->prepare("DELETE FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND nro_item='{$_POST['item_nro']}'");
			$pdo3->execute();

			$pdo4 = $connection->prepare("SELECT item_nro FROM produtos_orcamentos_forro WHERE id_orcamento = '{$_POST['id_orcamento']}' ORDER BY item_nro ASC");
			$pdo4->execute();
			$item_novo = 1;

			while ($row4 = $pdo4->fetch(PDO::FETCH_ASSOC)) {
				$item_antigo = $row4['item_nro'];

				$pdo5 = $connection->prepare("UPDATE produtos_orcamentos_forro SET item_nro='{$item_novo}' WHERE id_orcamento = '{$_POST['id_orcamento']}' AND item_nro = '$item_antigo'");
				$pdo5->execute();

				$pdo6 = $connection->prepare("UPDATE orcamentos_observacoes SET nro_item='{$item_novo}' WHERE orcamento_id = '{$_POST['id_orcamento']}' AND nro_item = '$item_antigo'");
				$pdo6->execute();

				$item_novo++;
			}

			$pdo6 = $connection->prepare("DELETE FROM vendas_custos WHERE cod_venda='{$_POST['id_orcamento']}' AND cod_item_venda='{$_POST['id']}'");
			$pdo6->execute();

			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}