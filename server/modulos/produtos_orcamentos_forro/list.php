<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'produtos_orcamentos_forro';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT * 
				FROM produtos_orcamentos_forro
				WHERE id_orcamento=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$buscar->setBusca(array('id_orcamento', 'produtos_orcamentos_forro.id_orcamento'), $_POST['id_orcamento']);

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) as total FROM produtos_orcamentos_forro {$filtro}");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SELECT produtos_orcamentos_forro.*, 
				UPPER(unidades_medidas.sigla) AS sigla_um, 
				produtos.descricao_completa as produto,
				p2.descricao_completa as produto_pai,
				cores.descricao as cor, 
				cores.codigo as css_cor 
				FROM produtos_orcamentos_forro 
				INNER JOIN produtos ON (produtos_orcamentos_forro.id_produto=produtos.id) 
				INNER JOIN unidades_medidas ON (unidades_medidas.id=produtos.unidade_medida) 
				LEFT  JOIN produtos_cores ON (produtos_orcamentos_forro.id_cor=produtos_cores.id_produtos_cores) 
				LEFT  JOIN cores ON (produtos_cores.id_cor=cores.id_cores) 
				LEFT  JOIN produtos_orcamentos_forro as po2 ON (po2.id_produtos_orcamentos_forro=produtos_orcamentos_forro.id_produtos_orcamentos_forro_pai) 
				LEFT  JOIN produtos as p2 ON (po2.id_produto=p2.id)
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );

			//soma para poder calular madeira ou meia cana com emenda....
			$data = $pdo->fetchAll(PDO::FETCH_ASSOC);
			foreach ($data as $key => $item) {
				if($item['tipo_produto']=='P' || $item['tipo_produto']=='I'){
					$pdo2 = $connection->prepare("
						SELECT sum(comprimento) as comprimento, sum(largura) as largura
						FROM ambientes_orcamentos_forro 
						WHERE id_produtos_orcamentos_forro = {$item['id_produtos_orcamentos_forro']};
					");
					$pdo2->execute();
					$soma = $pdo2->fetch(PDO::FETCH_ASSOC);
					$data[$key]['soma'] = (float) $soma['comprimento'] + (float) $soma['largura'];
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $data;
			echo json_encode($result);
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>