<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'adiantamentos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT * FROM adiantamentos WHERE id=:id");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			$buscar->setBusca(array('cliente', 'adiantamentos.cliente'), $_POST['cod_cliente']);
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('cliente', 'adiantamentos.cliente'), $_POST['cliente']);
				$buscar->setBusca(array('valor', 'adiantamentos.valor'), $_POST['valor'], 'like');
				$buscar->setBusca(array('forma_pagto', 'adiantamentos.forma_pagto'), $_POST['forma_pagto']);
				$buscar->setBusca(array('valor_utilizado', 'adiantamentos.valor_utilizado'), $_POST['valor_utilizado'], 'like');
				$buscar->setBusca(array('saldo', 'adiantamentos.saldo'), $_POST['saldo'], 'like');
				$buscar->setBusca(array('observacao', 'adiantamentos.observacao'), $_POST['observacao'], 'like');
				$buscar->setBusca(array('data_cadastro', 'adiantamentos.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('data_alteracao', 'adiantamentos.data_alteracao'), implode('-', array_reverse(explode('/', $_POST['data_alteracao_date'])))." ".$_POST['data_alteracao_time'], 'like');
				$buscar->setBusca(array('cadastrado_por', 'adiantamentos.cadastrado_por'), $_POST['cadastrado_por']);
				$buscar->setBusca(array('alterado_por', 'adiantamentos.alterado_por'), $_POST['alterado_por']);
				$buscar->setBusca(array('estornado', 'adiantamentos.estornado'), $_POST['estornado'], 'like');
				$buscar->setBusca(array('data_estorno', 'adiantamentos.data_estorno'), implode('-', array_reverse(explode('/', $_POST['data_estorno_date'])))." ".$_POST['data_estorno_time'], 'like');
				$buscar->setBusca(array('estornado_por', 'adiantamentos.estornado_por'), $_POST['estornado_por']);
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("
				SELECT count(*) AS total 
				FROM adiantamentos INNER JOIN formas_pagto ON
					(adiantamentos.forma_pagto=formas_pagto.id) 
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT adiantamentos.*, 
					DATE_FORMAT(adiantamentos.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(adiantamentos.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(adiantamentos.data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(adiantamentos.data_alteracao, '%Y-%m-%d') as data_alteracao_date, 
					DATE_FORMAT(adiantamentos.data_estorno, '%H:%i:%s') as data_estorno_time, 
					DATE_FORMAT(adiantamentos.data_estorno, '%Y-%m-%d') as data_estorno_date, 
					formas_pagto.descricao AS descricao_pagto, formas_pagto.categoria AS categoria_pagamento, 
					UPPER(U.nome) AS cadastrado_por_desc, 
					UPPER(U2.nome) AS estornado_por_desc 
				FROM adiantamentos 
				INNER JOIN formas_pagto ON (adiantamentos.forma_pagto=formas_pagto.id) 
				LEFT JOIN usuarios AS U ON (U.id=adiantamentos.cadastrado_por) 
				LEFT JOIN usuarios AS U2 ON (U2.id=adiantamentos.estornado_por) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('total'=>$countRow, 'dados'=>$query, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>