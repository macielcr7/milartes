<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'veiculos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(cadastrado_por, '%H:%i:%s') as cadastrado_por_time, 
					DATE_FORMAT(cadastrado_por, '%Y-%m-%d') as cadastrado_por_date, 
					DATE_FORMAT(data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(data_alteracao, '%Y-%m-%d') as data_alteracao_date FROM veiculos WHERE id=:id");
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
	
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('descricao', 'veiculos.descricao'), $_POST['descricao'], 'like');
				$buscar->setBusca(array('grupo', 'veiculos.grupo'), $_POST['grupo'], 'like');
				$buscar->setBusca(array('placa', 'veiculos.placa'), $_POST['placa'], 'like');
				$buscar->setBusca(array('dt_inicio_controle', 'veiculos.dt_inicio_controle'), implode('-', array_reverse(explode('/', $_POST['dt_inicio_controle']))), 'like');
				$buscar->setBusca(array('unidade_medida', 'veiculos.unidade_medida'), $_POST['unidade_medida']);
				$buscar->setBusca(array('hodometro_inicial', 'veiculos.hodometro_inicial'), $_POST['hodometro_inicial']);
				$buscar->setBusca(array('combustivel1', 'veiculos.combustivel1'), $_POST['combustivel1'], 'like');
				$buscar->setBusca(array('capacidade1', 'veiculos.capacidade1'), $_POST['capacidade1'], 'like');
				$buscar->setBusca(array('consumo1', 'veiculos.consumo1'), $_POST['consumo1'], 'like');
				$buscar->setBusca(array('combustivel2', 'veiculos.combustivel2'), $_POST['combustivel2'], 'like');
				$buscar->setBusca(array('capacidade2', 'veiculos.capacidade2'), $_POST['capacidade2'], 'like');
				$buscar->setBusca(array('consumo2', 'veiculos.consumo2'), $_POST['consumo2'], 'like');
				$buscar->setBusca(array('combustivel3', 'veiculos.combustivel3'), $_POST['combustivel3'], 'like');
				$buscar->setBusca(array('capacidade3', 'veiculos.capacidade3'), $_POST['capacidade3'], 'like');
				$buscar->setBusca(array('consumo3', 'veiculos.consumo3'), $_POST['consumo3'], 'like');
				$buscar->setBusca(array('capacidade_transporte', 'veiculos.capacidade_transporte'), $_POST['capacidade_transporte'], 'like');
				$buscar->setBusca(array('tara', 'veiculos.tara'), $_POST['tara'], 'like');
				$buscar->setBusca(array('ativo', 'veiculos.ativo'), $_POST['ativo'], 'like');
				$buscar->setBusca(array('data_cadastro', 'veiculos.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('cadastrado_por', 'veiculos.cadastrado_por'), implode('-', array_reverse(explode('/', $_POST['cadastrado_por_date'])))." ".$_POST['cadastrado_por_time'], 'like');
				$buscar->setBusca(array('alterado_por', 'veiculos.alterado_por'), $_POST['alterado_por']);
				$buscar->setBusca(array('data_alteracao', 'veiculos.data_alteracao'), implode('-', array_reverse(explode('/', $_POST['data_alteracao_date'])))." ".$_POST['data_alteracao_time'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total FROM veiculos INNER JOIN unidades_medidas ON
					(veiculos.unidade_medida=unidades_medidas.id) {$filtro}");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_veiculo'])) {
					$filtro = "WHERE veiculos.ativo='S'";
				}
				elseif ($_POST['status_veiculo'] == 'X') {
					$filtro = "";
				}
				else{
					$filtro = "WHERE veiculos.ativo='{$_POST['status_veiculo']}'";
				}
			}

			$sql = "SELECT veiculos.id, 
					UPPER(veiculos.descricao) AS descricao, 
					UPPER(veiculos.grupo) AS grupo, 
					UPPER(veiculos.placa) AS placa, 
					veiculos.dt_inicio_controle, 
					veiculos.renavam, 
					veiculos.hodometro_inicial, 
					veiculos.combustivel1, 
					veiculos.ativo, 
					veiculos.data_cadastro, 
					unidades_medidas.sigla, 
					usuarios.nome AS cadastrado_por_nome 
				FROM veiculos 
					INNER JOIN unidades_medidas ON (veiculos.unidade_medida=unidades_medidas.id) 
					INNER JOIN usuarios ON (veiculos.cadastrado_por=usuarios.id)
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			//echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>