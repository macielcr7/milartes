<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'veiculos';
	$data_atual = date('Y-m-d');
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	if ($_POST['capacidade1'] == "0,00"){
		$_POST['capacidade1'] = 0;
	}

	if ($_POST['capacidade2'] == "0,00"){
		$_POST['capacidade2'] = 0;
	}

	if ($_POST['capacidade3'] == "0,00"){
		$_POST['capacidade3'] = 0;
	}

	if ($_POST['consumo1'] == "0,00"){
		$_POST['consumo1'] = 0;
	}

	if ($_POST['consumo2'] == "0,00"){
		$_POST['consumo2'] = 0;
	}

	if ($_POST['consumo3'] == "0,00"){
		$_POST['consumo3'] = 0;
	}

	if ($_POST['capacidade_transporte'] == "0.000"){
		$_POST['capacidade_transporte'] = 0;
	}

	if ($_POST['tara'] == "0.000"){
		$_POST['tara'] = 0;
	}

	if ($_POST['hodometro_inicial'] == "0.000"){
		$_POST['hodometro_inicial'] = 0;
	}
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE veiculos SET 
							descricao = ?,
							grupo = ?,
							renavam = ?,
							placa = ?,
							unidade_medida = ?,
							hodometro_inicial = ?,
							combustivel1 = ?,
							capacidade1 = ?,
							consumo1 = ?,
							combustivel2 = ?,
							capacidade2 = ?,
							consumo2 = ?,
							combustivel3 = ?,
							capacidade3 = ?,
							consumo3 = ?,
							capacidade_transporte = ?,
							tara = ?,
							ativo = ?,
							alterado_por = ?,
							data_alteracao = ?
					WHERE id = ?
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$_POST['grupo'],
				$_POST['renavam'],
				mb_strtoupper($_POST['placa'], 'UTF-8'),
				$_POST['unidade_medida'],
				$_POST['hodometro_inicial'],
				$_POST['combustivel1'],
				$_POST['capacidade1'],
				$_POST['consumo1'],
				$_POST['combustivel2'],
				$_POST['capacidade2'],
				$_POST['consumo2'],
				$_POST['combustivel3'],
				$_POST['capacidade3'],
				$_POST['consumo3'],
				$_POST['capacidade_transporte'],
				$_POST['tara'],
				$_POST['ativo'],
				$user_id,
				$data_hora_atual,
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO veiculos 
					(
						descricao,
						grupo,
						renavam,
						placa,
						dt_inicio_controle,
						unidade_medida,
						hodometro_inicial,
						combustivel1,
						capacidade1,
						consumo1,
						combustivel2,
						capacidade2,
						consumo2,
						combustivel3,
						capacidade3,
						consumo3,
						capacidade_transporte,
						tara,
						ativo,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$_POST['grupo'],
				$_POST['renavam'],
				mb_strtoupper($_POST['placa'], 'UTF-8'),
				$data_atual,
				$_POST['unidade_medida'],
				$_POST['hodometro_inicial'],
				$_POST['combustivel1'],
				$_POST['capacidade1'],
				$_POST['consumo1'],
				$_POST['combustivel2'],
				$_POST['capacidade2'],
				$_POST['consumo2'],
				$_POST['combustivel3'],
				$_POST['capacidade3'],
				$_POST['consumo3'],
				$_POST['capacidade_transporte'],
				$_POST['tara'],
				$_POST['ativo'],
				$data_hora_atual,
				$user_id,
				$user_id,
				$data_hora_atual
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}