<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'os_departamentos_responsavel';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
		
			$pdo = $connection->prepare("
					UPDATE os_departamentos_responsavel SET 
							id_depto = ?, 
							id_usuario = ?, 
							id_empresa = ?,	
							data_inclusao = ?,
							alterado_por = ?, 
					WHERE id = ?
			");
			$params = array(
				$_POST['id_depto'],
				$_POST['id_usuario'],
				(count($_POST['id_empresa'])>0 ? $_POST['id_empresa'][0] : ''),
				$user_id,
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');

			foreach ($_POST['id_empresa'] AS $key => $item) {
				$pdo2 = $connection->prepare("SELECT count(*) AS total FROM os_departamentos_responsavel WHERE id_depto='{$_POST['id_depto']}' AND id_empresa='{$_POST['id_empresa'][$key]}'");
				$pdo2->execute();
				$query2 = $pdo2->fetch(PDO::FETCH_OBJ);
				$countRow = $query2->total;

				if ($countRow == 0){
					$pdo = $connection->prepare("
						INSERT INTO os_departamentos_responsavel 
							(
								id_depto, 
								id_usuario, 
								id_empresa, 
								data_inclusao, 
								cadastrado_por, 
								alterado_por
							) 
						VALUES 
							(
								?,	?,	?,	NOW(),	?,	?
							)
					");
					$params = array(
						$_POST['id_depto'], 
						$_POST['id_usuario'], 
						$_POST['id_empresa'][$key], 
						$user_id, 
						$user_id
					);
					$pdo->execute($params);
				}
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}