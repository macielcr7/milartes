<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'os_departamentos_responsavel';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_inclusao, '%H:%i:%s') as data_inclusao_time, 
					DATE_FORMAT(data_inclusao, '%Y-%m-%d') as data_inclusao_date 
				FROM os_departamentos_responsavel
				WHERE id=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'RESPONSAVEL_EM_OS' ){
			$sql = "SELECT ODR.id, 
					CONCAT(DATE_FORMAT(ODR.data_inclusao, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(ODR.data_inclusao, '%H:%i:%s')) AS data_cadastro, 
					UPPER(PC.descricao) AS nome_departamento, UPPER(U.nome) AS responsavel, 
					UPPER(ES.nome_fantasia) AS nome_fantasia, UPPER(U2.nome) AS cadastrado_por 
				FROM os_departamentos_responsavel AS ODR 
				INNER JOIN produtos_categorias AS PC ON (ODR.id_depto=PC.cod_categoria) 
				INNER JOIN usuarios AS U ON (ODR.id_usuario=U.id) 
				INNER JOIN empresas_sistema AS ES ON (ODR.id_empresa=ES.cod_empresa) 
				INNER JOIN usuarios AS U2 ON (ODR.cadastrado_por=U2.id) 
				WHERE PC.cod_categoria=:id_depto 
				ORDER BY PC.descricao ASC, ES.nome_fantasia ASC";
			$pdo = $connection->prepare($sql);
			
			$pdo->bindParam(':id_depto', $_POST['id_depto']);
			$pdo->execute();
		
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			//echo json_encode( array('success'=>true, 'dados'=>$linhas) );
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
	
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('id_depto', 'os_departamentos_responsavel.id_depto'), $_POST['id_depto']);
				$buscar->setBusca(array('id_usuario', 'os_departamentos_responsavel.id_usuario'), $_POST['id_usuario']);
				$buscar->setBusca(array('id_empresa', 'os_departamentos_responsavel.id_empresa'), $_POST['id_empresa']);
				$buscar->setBusca(array('data_inclusao', 'os_departamentos_responsavel.data_inclusao'), implode('-', array_reverse(explode('/', $_POST['data_inclusao_date'])))." ".$_POST['data_inclusao_time'], 'like');
			}
			
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("
				SELECT count(*) AS total 
				FROM os_departamentos_responsavel AS ODR
				INNER JOIN produtos_categorias AS PC ON (ODR.id_depto=PC.cod_categoria) 
				INNER JOIN usuarios AS U ON (ODR.id_usuario=U.id) 
				INNER JOIN empresas_sistema AS ES ON (ODR.id_empresa=ES.cod_empresa) 
				INNER JOIN usuarios AS U2 ON (ODR.cadastrado_por=U2.id) 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;
			
			$sql = "SELECT ODR.id, 
					CONCAT(DATE_FORMAT(ODR.data_inclusao, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(ODR.data_inclusao, '%H:%i:%s')) AS data_cadastro, 
					UPPER(PC.descricao) AS nome_departamento, UPPER(U.nome) AS responsavel, 
					UPPER(ES.nome_fantasia) AS nome_fantasia, UPPER(U2.nome) AS cadastrado_por
				FROM os_departamentos_responsavel AS ODR
				INNER JOIN produtos_categorias AS PC ON (ODR.id_depto=PC.cod_categoria) 
				INNER JOIN usuarios AS U ON (ODR.id_usuario=U.id) 
				INNER JOIN empresas_sistema AS ES ON (ODR.id_empresa=ES.cod_empresa) 
				INNER JOIN usuarios AS U2 ON (ODR.cadastrado_por=U2.id) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			//echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>