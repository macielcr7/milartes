<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'agenda_telefonica_contato';
	try {
		if($_POST['action'] == 'DELETAR'){
			$data_hora_atual = date('Y-m-d H:i:s');
			$pdo = $connection->prepare("SELECT controle, tipo_contato, descricao, observacao FROM agenda_telefonica_contato WHERE controle='$_POST[id]'");
			$pdo->execute();
	
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$descricao_banco = $linhas->controle." - ".$linhas->tipo_contato.": ".$linhas->descricao." ".$linhas->observacao;
			$nome_tabela = utf8_encode('agenda_telefonica_contato');
			$nome_campo = utf8_encode('Contato');
			$pdo = $connection->prepare("
					INSERT INTO logs_sistema (controle, chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
						VALUES 
					(NULL, md5('$data_hora_atual'), '$nome_tabela', '$_POST[id]', 'Excluiu', '$nome_campo', '$descricao_banco', '', '$user_id', '$data_hora_atual')
			");
			$pdo->execute();

			$pdo = $connection->prepare("DELETE FROM agenda_telefonica_contato WHERE controle = ?");
			$pdo->execute(array(
				$_POST['id']
			));
			
			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}