<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'agenda_telefonica_contato';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM agenda_telefonica_contato
				WHERE controle=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			$buscar->setBusca(array('cod_contato', 'ATC.cod_contato'), $_POST['cod_contato']);
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('tipo_contato', 'ATC.tipo_contato'), $_POST['tipo_contato'], 'like');
				$buscar->setBusca(array('descricao', 'ATC.descricao'), $_POST['descricao'], 'like');
				$buscar->setBusca(array('observacao', 'ATC.observacao'), $_POST['observacao'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total FROM agenda_telefonica_contato AS ATC {$filtro}");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SELECT ATC.controle, 
					ATC.cod_contato, UPPER(ATC.tipo_contato) AS tipo_contato,
					ATC.descricao, 
					UPPER(ATC.observacao) AS observacao, 
					ATC.data_cadastro, 
					DATE_FORMAT(ATC.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(ATC.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM agenda_telefonica_contato AS ATC 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>