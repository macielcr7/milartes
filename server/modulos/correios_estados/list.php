<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'correios_estados';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_estados
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$pdo = $connection->prepare("SELECT UPPER(uf) AS id, UPPER(descricao) AS descricao 
				FROM correios_estados WHERE ativo='S' ORDER BY descricao ASC
			");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['query']) and !empty($_POST['query'])){
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$pdo = $connection->prepare("SELECT CE.id, 
					UPPER(CE.uf) AS uf, UPPER(CE.descricao) AS descricao, CE.ativo, UPPER(U.nome) AS cadastrado_por, 
					CONCAT(DATE_FORMAT(CE.data_cadastro, '%d/%m/%Y'), ' ', DATE_FORMAT(CE.data_cadastro, '%H:%i:%s')) AS data_cadastro2
				FROM correios_estados AS CE INNER JOIN usuarios AS U ON (U.id=CE.cadastrado_por)
				WHERE CE.uf like '%$_POST[query]%' OR CE.descricao like '%$_POST[query]%'
				ORDER BY {$sort} {$order}
				LIMIT $_POST[start], $_POST[limit]
			");
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('uf', 'correios_estados.uf'), $_POST['uf'], 'like');
				$buscar->setBusca(array('descricao', 'correios_estados.descricao'), $_POST['descricao'], 'like');
				$buscar->setBusca(array('ativo', 'correios_estados.ativo'), $_POST['ativo'], 'like');
				$buscar->setBusca(array('cadastrado_por', 'correios_estados.cadastrado_por'), $_POST['cadastrado_por']);
				$buscar->setBusca(array('data_cadastro', 'correios_estados.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('alterado_por', 'correios_estados.alterado_por'), $_POST['alterado_por']);
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total FROM correios_estados AS CE {$filtro}");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SELECT CE.id, 
					UPPER(CE.uf) AS uf, UPPER(CE.descricao) AS descricao, CE.ativo, UPPER(U.nome) AS cadastrado_por, 
					CONCAT(DATE_FORMAT(CE.data_cadastro, '%d/%m/%Y'), ' ', DATE_FORMAT(CE.data_cadastro, '%H:%i:%s')) AS data_cadastro2
				FROM correios_estados AS CE INNER JOIN usuarios AS U ON (U.id=CE.cadastrado_por) 
				ORDER BY {$sort} {$order}
				LIMIT {$start}, {$limit}
			");
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>