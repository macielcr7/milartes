
<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	$data_atual = date('Y-m-d H:i:s');
	try {
		if(empty($_POST['quantidade'])) {
			$quantidade = 0;
		} else {
			$quantidade = implode('.', explode(',', $_POST['quantidade']));
		}

		if(empty($_POST['qtd_metros_porcento'])) {
			$qtd_metros_porcento = 0;
		} else {
			$qtd_metros_porcento = implode('.', explode(',', $_POST['qtd_metros_porcento']));
		}

		if(empty($_POST['total_area'])) {
			$total_area = 0;
		} else {
			$total_area = implode('.', explode(',', $_POST['total_area']));
		}

		if(empty($_POST['id_cor'])) {
			$_POST['id_cor'] = 0;
		}

		if(empty($_POST['qtd_barras'])) {
			$_POST['qtd_barras'] = 0;
		}

		if(empty($_POST['possui_ambiente'])) {
			$_POST['possui_ambiente'] = 'N';
		}

		//pega os valores dos custos do produto
		$pdoX = $connection->prepare("SELECT tipo_produto, possui_composicao, preco_custo AS preco_custo_produto, preco_custo_servico, dobra_quantidade FROM produtos WHERE id = '{$_POST['id_produto']}'");
		$pdoX->execute();
		$custos = $pdoX->fetch(PDO::FETCH_OBJ);
		$preco_custo_produto = $custos->preco_custo_produto;
		//$preco_custo_servico = $custos->preco_custo_servico;

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
					UPDATE produtos_orcamentos_rodape SET 
							tipo_produto = ?,
							id_produto = ?,
							id_cor = ?,
							qtd_barras = ?,
							qtd_metros_porcento = ?,
							total_area = ?,
							quantidade = ?,
							valor_unitario = ?,
							valor_bruto = ?,
							valor_total = ?,
							possui_ambiente = ?, 
							item_nro = ? 
					WHERE id_produtos_orcamentos_rodape = ?
			");
			$params = array(
				$_POST['tipo_produto'],
				$_POST['id_produto'],
				$_POST['id_cor'],
				$_POST['qtd_barras'],
				$qtd_metros_porcento,
				$total_area,
				$quantidade,
				$_POST['valor_unitario'],
				$_POST['valor_bruto'],
				$_POST['valor_total'],
				$_POST['possui_ambiente'],
				$_POST['item_nro'],
				$_POST['id_produtos_orcamentos_rodape']
			);
			$pdo->execute($params);


			//atualiza os custos
			$sql4 = "DELETE FROM vendas_custos WHERE cod_item_venda = '{$_POST['id_produtos_orcamentos_rodape']}'";
				$pdo4 = $connection->prepare($sql4);
				$pdo4->execute();

			if ($custos->possui_composicao == 'S') {
				$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo 
					FROM produto_composicoes AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
					INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida)
					WHERE PC.id_produto = '{$_POST['id_produto']}'";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();

				while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
				{
					//calcula a quantidade
					if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
						$nova_qtde = ceil($quantidade * $item['quantidade']);
					} else {
						$nova_qtde = ($quantidade * $item['quantidade']);
					}

					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$_POST['id_orcamento'],
						$_POST['id_produtos_orcamentos_rodape'],
						$item['id_composicao'],
						$_POST['id_cor'],
						$nova_qtde,
						$item['preco_custo'],
						0,
						0,
						($nova_qtde * $item['preco_custo']),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			} else {
				$pdo2 = $connection->prepare("
					INSERT INTO vendas_custos 
						(
							cod_venda, 
							cod_item_venda, 
							cod_produto, 
							cod_cor, 
							quantidade, 
							valor_unitario, 
							acrescimo_mo, 
							acrescimo_produto, 
							valor_total,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
				");
				$params2 = array(
					$_POST['id_orcamento'],
					$_POST['id_produtos_orcamentos_rodape'],
					$_POST['id_produto'],
					$_POST['id_cor'],
					$quantidade,
					$preco_custo_produto,
					0,
					0,
					($quantidade * $preco_custo_produto),
					date('Y-m-d H:i:s'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'),
					$observacoes
				);
				$pdo2->execute($params2);
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			$sqlC = "SELECT count(*) FROM produtos_orcamentos_rodape WHERE id_orcamento='{$_POST['id_orcamento']}'";
			$pdoC = $connection->prepare($sqlC);
			$pdoC->execute();
			$num_rowsC = $pdoC->fetchColumn();

			if($num_rowsC == 0){
				$nro_item = 1;
			} else {
				$nro_item = $num_rowsC + 1;
			}

			$pdo = $connection->prepare("
				INSERT INTO produtos_orcamentos_rodape 
					(
						tipo_produto,
						id_orcamento, 
						id_produto, 
						id_cor, 
						qtd_barras, 
						qtd_metros_porcento, 
						total_area, 
						quantidade, 
						valor_unitario, 
						valor_bruto, 
						valor_total, 
						possui_ambiente,
						item_nro
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['tipo_produto'],
				$_POST['id_orcamento'],
				$_POST['id_produto'],
				$_POST['id_cor'],
				$_POST['qtd_barras'],
				$qtd_metros_porcento,
				$total_area,
				$quantidade,
				$_POST['valor_unitario'],
				$_POST['valor_bruto'],
				$_POST['valor_total'],
				$_POST['possui_ambiente'],
				$nro_item
			);
			$pdo->execute($params);

			$pdo = $connection->prepare("
				SELECT * FROM produtos_orcamentos_rodape order by id_produtos_orcamentos_rodape desc LIMIT 1
			");
			$pdo->execute();
		
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$_POST['id_produtos_orcamentos_rodape'] = $query->id_produtos_orcamentos_rodape;
			$_POST['item_nro'] = $nro_item;

			//gera os custos
			if ($custos->possui_composicao == 'S') {
				$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo 
					FROM produto_composicoes AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
					INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida)
					WHERE PC.id_produto = '{$_POST['id_produto']}'";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();

				while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
				{
					//calcula a quantidade
					if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
						$nova_qtde = ceil($quantidade * $item['quantidade']);
					} else {
						$nova_qtde = ($quantidade * $item['quantidade']);
					}

					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$_POST['id_orcamento'],
						$query->id_produtos_orcamentos_rodape,
						$item['id_composicao'],
						$_POST['id_cor'],
						$nova_qtde,
						$item['preco_custo'],
						0,
						0,
						($nova_qtde * $item['preco_custo']),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			} else {
				$pdo2 = $connection->prepare("
					INSERT INTO vendas_custos 
						(
							cod_venda, 
							cod_item_venda, 
							cod_produto, 
							cod_cor, 
							quantidade, 
							valor_unitario, 
							acrescimo_mo, 
							acrescimo_produto, 
							valor_total,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
				");
				$params2 = array(
					$_POST['id_orcamento'],
					$query->id_produtos_orcamentos_rodape,
					$_POST['id_produto'],
					$_POST['id_cor'],
					$quantidade,
					$preco_custo_produto,
					0,
					0,
					($quantidade * $preco_custo_produto),
					date('Y-m-d H:i:s'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'),
					$observacoes
				);
				$pdo2->execute($params2);
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

		if(in_array($_POST['action'], array('INSERIR', 'EDITAR')) and isset($_POST['ambientes'])){
			//$ambientes = json_decode(stripcslashes('['.$_POST['ambientes'].']'), true);
			$ambientes = json_decode('['.$_POST['ambientes'].']', true);
			foreach ($ambientes as $key => $item) {
				$nome_ambiente = mb_strtoupper(trim($item['ambiente']));
				$nome_ambiente = mb_strtoupper($nome_ambiente, 'UTF-8');
				$obs_ambiente = mb_strtoupper(trim($item['observacoes']));
				$obs_ambiente = mb_strtoupper($obs_ambiente, 'UTF-8');

				$sqlA = "SELECT count(*) FROM ambientes_nomes WHERE descricao='$nome_ambiente'";
				$pdoA = $connection->prepare($sqlA);
				$pdoA->execute();
				$num_rows = $pdoA->fetchColumn();

				if($num_rows < 1){
					$sqlB = "INSERT INTO ambientes_nomes (descricao, cadastrado_por, alterado_por, data_cadastro, data_alteracao) VALUES ('$nome_ambiente', $user_id, $user_id, '$data_atual', '$data_atual')";
					$pdoB = $connection->prepare($sqlB);
					$pdoB->execute();
				}

				if($item['id_ambientes_orcamentos_rodape']=='0'){
					$pdo = $connection->prepare("
						INSERT INTO ambientes_orcamentos_rodape 
							(
								id_produtos_orcamentos_rodape, 
								ambiente, 
								qtd_paredes, 
								comprimento_1, 
								comprimento_real_1, 
								comprimento_2,
								comprimento_real_2,
								comprimento_3,
								comprimento_real_3,
								comprimento_4,
								comprimento_real_4,
								comprimento_5,
								comprimento_real_5,
								comprimento_6,
								comprimento_real_6,
								comprimento_7,
								comprimento_real_7,
								comprimento_8,
								comprimento_real_8,
								comprimento_9,
								comprimento_real_9,
								comprimento_10,
								comprimento_real_10,
								comprimento_11,
								comprimento_real_11,
								comprimento_12,
								comprimento_real_12,
								comprimento_13,
								comprimento_real_13,
								comprimento_14,
								comprimento_real_14,
								comprimento_15,
								comprimento_real_15,
								comprimento_16,
								comprimento_real_16,
								comprimento_17,
								comprimento_real_17,
								comprimento_18,
								comprimento_real_18,
								comprimento_19,
								comprimento_real_19,
								comprimento_20,
								comprimento_real_20,
								comprimento_21,
								comprimento_real_21,
								observacoes,
								area,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params = array(
						$_POST['id_produtos_orcamentos_rodape'],
						$nome_ambiente,
						$item['qtd_paredes'],
						$item['comprimento_1'],
						$item['comprimento_real_1'],
						$item['comprimento_2'],
						$item['comprimento_real_2'],
						$item['comprimento_3'],
						$item['comprimento_real_3'],
						$item['comprimento_4'],
						$item['comprimento_real_4'],
						$item['comprimento_5'],
						$item['comprimento_real_5'],
						$item['comprimento_6'],
						$item['comprimento_real_6'],
						$item['comprimento_7'],
						$item['comprimento_real_7'],
						$item['comprimento_8'],
						$item['comprimento_real_8'],
						$item['comprimento_9'],
						$item['comprimento_real_9'],
						$item['comprimento_10'],
						$item['comprimento_real_10'],
						$item['comprimento_11'],
						$item['comprimento_real_11'],
						$item['comprimento_12'],
						$item['comprimento_real_12'],
						$item['comprimento_13'],
						$item['comprimento_real_13'],
						$item['comprimento_14'],
						$item['comprimento_real_14'],
						$item['comprimento_15'],
						$item['comprimento_real_15'],
						$item['comprimento_16'],
						$item['comprimento_real_16'],
						$item['comprimento_17'],
						$item['comprimento_real_17'],
						$item['comprimento_18'],
						$item['comprimento_real_18'],
						$item['comprimento_19'],
						$item['comprimento_real_19'],
						$item['comprimento_20'],
						$item['comprimento_real_20'],
						$item['comprimento_21'],
						$item['comprimento_real_21'],
						$obs_ambiente,
						$item['area'],
						date('Y-m-d H:i:s'), 
						$user_id,
						$user_id,
						date('Y-m-d H:i:s')
					);
					$pdo->execute($params);


					$pdo = $connection->prepare("
						SELECT * FROM ambientes_orcamentos_rodape ORDER BY id_ambientes_orcamentos_rodape desc LIMIT 1
					");
					$pdo->execute();

					$query = $pdo->fetch(PDO::FETCH_OBJ);
					$_POST['id_ambientes_orcamentos_rodape'] = $query->id_ambientes_orcamentos_rodape;

					if (!empty($item['observacoes'])){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$obs_ambiente;
						$sqlD = "INSERT INTO orcamentos_observacoes 
							(
								orcamento_id, 
								ambiente_id, 
								nro_item, 
								descricao, 
								tipo, 
								cadastrado_por, 
								alterado_por, 
								data_cadastro, 
								data_alteracao
							) 
						VALUES 
							(
								'{$_POST['id_orcamento']}', 
								'{$_POST['id_ambientes_orcamentos_rodape']}', 
								'{$_POST['item_nro']}', 
								'{$novo_texto_obs}', 
								'G', 
								'{$user_id}', 
								'{$user_id}', 
								'{$data_atual}', 
								'{$data_atual}'
							)";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}
				}	
				else{
					//pega a observacao antiga
					$sqlE = "SELECT descricao FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND ambiente_id={$item['id_ambientes_orcamentos_rodape']}";
					$pdoE = $connection->prepare($sqlE);
					$pdoE->execute();
					$obs_antigo = $pdoE->fetchColumn();

					$nova_observacao = $obs_ambiente;
					$pdo = $connection->prepare("
							UPDATE ambientes_orcamentos_rodape SET 
									ambiente = ?,
									qtd_paredes = ?,
									comprimento_1 = ?,
									comprimento_real_1 = ?,
									comprimento_2 = ?,
									comprimento_real_2 = ?,
									comprimento_3 = ?,
									comprimento_real_3 = ?,
									comprimento_4 = ?,
									comprimento_real_4 = ?,
									comprimento_5 = ?,
									comprimento_real_5 = ?,
									comprimento_6 = ?,
									comprimento_real_6 = ?,
									comprimento_7 = ?,
									comprimento_real_7 = ?,
									comprimento_8 = ?,
									comprimento_real_8 = ?,
									comprimento_9 = ?,
									comprimento_real_9 = ?,
									comprimento_10 = ?,
									comprimento_real_10 = ?,
									comprimento_11 = ?,
									comprimento_real_11 = ?,
									comprimento_12 = ?,
									comprimento_real_12 = ?,
									comprimento_13 = ?,
									comprimento_real_13 = ?,
									comprimento_14 = ?,
									comprimento_real_14 = ?,
									comprimento_15 = ?,
									comprimento_real_15 = ?,
									comprimento_16 = ?,
									comprimento_real_16 = ?,
									comprimento_17 = ?,
									comprimento_real_17 = ?,
									comprimento_18 = ?,
									comprimento_real_18 = ?,
									comprimento_19 = ?,
									comprimento_real_19 = ?,
									comprimento_20 = ?,
									comprimento_real_20 = ?,
									comprimento_21 = ?,
									comprimento_real_21 = ?,
									observacoes = ?,
									area = ?,
									alterado_por = ?, 
									data_alteracao = ? 
							WHERE id_ambientes_orcamentos_rodape = ?
					");
					$params = array(
						$nome_ambiente,
						$item['qtd_paredes'],
						$item['comprimento_1'],
						$item['comprimento_real_1'],
						$item['comprimento_2'],
						$item['comprimento_real_2'],
						$item['comprimento_3'],
						$item['comprimento_real_3'],
						$item['comprimento_4'],
						$item['comprimento_real_4'],
						$item['comprimento_5'],
						$item['comprimento_real_5'],
						$item['comprimento_6'],
						$item['comprimento_real_6'],
						$item['comprimento_7'],
						$item['comprimento_real_7'],
						$item['comprimento_8'],
						$item['comprimento_real_8'],
						$item['comprimento_9'],
						$item['comprimento_real_9'],
						$item['comprimento_10'],
						$item['comprimento_real_10'],
						$item['comprimento_11'],
						$item['comprimento_real_11'],
						$item['comprimento_12'],
						$item['comprimento_real_12'],
						$item['comprimento_13'],
						$item['comprimento_real_13'],
						$item['comprimento_14'],
						$item['comprimento_real_14'],
						$item['comprimento_15'],
						$item['comprimento_real_15'],
						$item['comprimento_16'],
						$item['comprimento_real_16'],
						$item['comprimento_17'],
						$item['comprimento_real_17'],
						$item['comprimento_18'],
						$item['comprimento_real_18'],
						$item['comprimento_19'],
						$item['comprimento_real_19'],
						$item['comprimento_20'],
						$item['comprimento_real_20'],
						$item['comprimento_21'],
						$item['comprimento_real_21'],
						$nova_observacao,
						$item['area'],
						$user_id,
						date('Y-m-d H:i:s'),
						$item['id_ambientes_orcamentos_rodape']
					);
					$pdo->execute($params);
					//atualiza a observaçao
					//se a obs antiga for em branco e a nova tem algum valor.... insere
					if (empty($obs_antigo) AND !empty($nova_observacao)){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$nova_observacao;
						$sqlD = "INSERT INTO orcamentos_observacoes 
							(
								orcamento_id, 
								ambiente_id, 
								nro_item, 
								descricao, 
								tipo, 
								cadastrado_por, 
								alterado_por, 
								data_cadastro, 
								data_alteracao
							) 
						VALUES 
							(
								'{$_POST['id_orcamento']}', 
								'{$item['id_ambientes_orcamentos_rodape']}', 
								'{$_POST['item_nro']}', 
								'{$novo_texto_obs}', 
								'G', 
								'{$user_id}', 
								'{$user_id}', 
								'{$data_atual}', 
								'{$data_atual}'
							)";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}

					//se a antiga tiver algum valor e for diferente da nova... update
					if (!empty($obs_antigo) AND ($obs_antigo != $nova_observacao)){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$nova_observacao;
						$sqlD = "UPDATE orcamentos_observacoes SET descricao='{$novo_texto_obs}', alterado_por='{$user_id}', data_alteracao='{$data_atual}' WHERE orcamento_id='{$_POST['id_orcamento']}' AND ambiente_id={$item['id_ambientes_orcamentos_rodape']}";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}

					//se a antiga tiver algum valor e a nova for em branco.... apaga
					if (!empty($obs_antigo) AND empty($nova_observacao)){
						$novo_texto_obs = "<b>AMBIENTE ".$nome_ambiente."</b>: ".$nova_observacao;
						$sqlD = "DELETE FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND ambiente_id={$item['id_ambientes_orcamentos_rodape']}";
						$pdoD = $connection->prepare($sqlD);
						$pdoD->execute();
					}
				}
			}
		}

		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}