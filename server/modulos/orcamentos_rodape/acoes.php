<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'orcamentos_rodape';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if($_POST['action'] == 'COPIA_ITEM_ORCAMENTO'){
			$orcamento_nro = $_POST['orcamento_nro'];

			//copia o orcamento
			$novo_item_nro = $_POST['nro_itens']+1;
			$pdo = $connection->prepare("INSERT INTO produtos_orcamentos_rodape SELECT NULL, 
					id_orcamento, 
					id_produto, 
					id_cor, 
					quantidade, 
					valor_unitario, 
					valor_total, 
					valor_bruto, 
					tipo_produto, 
					qtd_barras, 
					qtd_metros_porcento, 
					total_area, 
					possui_ambiente, 
					'$novo_item_nro' 
				FROM produtos_orcamentos_rodape WHERE id_produtos_orcamentos_rodape = ?
			");
			$pdo->execute(array(
				$_POST['id_produtos_orcamentos_rodape']
			));

			//pega o novo ID do ítem
			$novo_id_item_rodape = $connection->lastInsertId();


			//se possui ambientes
			if ($_POST['possui_ambiente'] == "S"){
				$novo_id_ambiente_rodape = 0;
				$pdoB2 = $connection->prepare("SELECT 
						ambiente, 
						qtd_paredes, 
						comprimento_1, 
						comprimento_real_1, 
						comprimento_2, 
						comprimento_real_2, 
						comprimento_3, 
						comprimento_real_3, 
						comprimento_4, 
						comprimento_real_4, 
						comprimento_5, 
						comprimento_real_5, 
						comprimento_6, 
						comprimento_real_6, 
						comprimento_7, 
						comprimento_real_7, 
						comprimento_8, 
						comprimento_real_8, 
						comprimento_9, 
						comprimento_real_9, 
						comprimento_10, 
						comprimento_real_10, 
						comprimento_11, 
						comprimento_real_11, 
						comprimento_12, 
						comprimento_real_12, 
						comprimento_13, 
						comprimento_real_13, 
						comprimento_14, 
						comprimento_real_14, 
						comprimento_15, 
						comprimento_real_15, 
						comprimento_16, 
						comprimento_real_16, 
						comprimento_17, 
						comprimento_real_17, 
						comprimento_18, 
						comprimento_real_18, 
						comprimento_19, 
						comprimento_real_19, 
						comprimento_20, 
						comprimento_real_20, 
						comprimento_21, 
						comprimento_real_21, 
						observacoes, 
						area 
					FROM ambientes_orcamentos_rodape WHERE id_produtos_orcamentos_rodape = ?
				");
				$pdoB2->execute(array(
					$_POST['id_produtos_orcamentos_rodape']
				));

				while ($row = $pdoB2->fetch(PDO::FETCH_ASSOC)) {
					$valorB22 = $row['ambiente'];
					$valorB23 = $row['qtd_paredes'];
					$valorB24 = $row['comprimento_1'];
					$valorB25 = $row['comprimento_real_1'];
					$valorB26 = $row['comprimento_2'];
					$valorB27 = $row['comprimento_real_2'];
					$valorB28 = $row['comprimento_3'];
					$valorB29 = $row['comprimento_real_3'];
					$valorB210 = $row['comprimento_4'];
					$valorB211 = $row['comprimento_real_4'];
					$valorB212 = $row['comprimento_5'];
					$valorB213 = $row['comprimento_real_5'];
					$valorB214 = $row['comprimento_6'];
					$valorB215 = $row['comprimento_real_6'];
					$valorB216 = $row['comprimento_7'];
					$valorB217 = $row['comprimento_real_7'];
					$valorB218 = $row['comprimento_8'];
					$valorB219 = $row['comprimento_real_8'];
					$valorB220 = $row['comprimento_9'];
					$valorB221 = $row['comprimento_real_9'];
					$valorB222 = $row['comprimento_10'];
					$valorB223 = $row['comprimento_real_10'];
					$valorB224 = $row['comprimento_11'];
					$valorB225 = $row['comprimento_real_11'];
					$valorB226 = $row['comprimento_12'];
					$valorB227 = $row['comprimento_real_12'];
					$valorB228 = $row['comprimento_13'];
					$valorB229 = $row['comprimento_real_13'];
					$valorB230 = $row['comprimento_14'];
					$valorB231 = $row['comprimento_real_14'];
					$valorB232 = $row['comprimento_15'];
					$valorB233 = $row['comprimento_real_15'];
					$valorB234 = $row['comprimento_16'];
					$valorB235 = $row['comprimento_real_16'];
					$valorB236 = $row['comprimento_17'];
					$valorB237 = $row['comprimento_real_17'];
					$valorB238 = $row['comprimento_18'];
					$valorB239 = $row['comprimento_real_18'];
					$valorB240 = $row['comprimento_19'];
					$valorB241 = $row['comprimento_real_19'];
					$valorB242 = $row['comprimento_20'];
					$valorB243 = $row['comprimento_real_20'];
					$valorB244 = $row['comprimento_21'];
					$valorB245 = $row['comprimento_real_21'];
					$valorB246 = $row['observacoes'];
					$valorB247 = $row['area'];

					$pdoB3 = $connection->prepare("INSERT INTO ambientes_orcamentos_rodape (
							id_produtos_orcamentos_rodape, 
							ambiente, 
							qtd_paredes, 
							comprimento_1, 
							comprimento_real_1, 
							comprimento_2, 
							comprimento_real_2, 
							comprimento_3, 
							comprimento_real_3, 
							comprimento_4, 
							comprimento_real_4, 
							comprimento_5, 
							comprimento_real_5, 
							comprimento_6, 
							comprimento_real_6, 
							comprimento_7, 
							comprimento_real_7, 
							comprimento_8, 
							comprimento_real_8, 
							comprimento_9, 
							comprimento_real_9, 
							comprimento_10, 
							comprimento_real_10, 
							comprimento_11, 
							comprimento_real_11, 
							comprimento_12, 
							comprimento_real_12, 
							comprimento_13, 
							comprimento_real_13, 
							comprimento_14, 
							comprimento_real_14, 
							comprimento_15, 
							comprimento_real_15, 
							comprimento_16, 
							comprimento_real_16, 
							comprimento_17, 
							comprimento_real_17, 
							comprimento_18, 
							comprimento_real_18, 
							comprimento_19, 
							comprimento_real_19, 
							comprimento_20, 
							comprimento_real_20, 
							comprimento_21, 
							comprimento_real_21, 
							observacoes, 
							area, 
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao
						) VALUES (
							'{$novo_id_item_rodape}', 
							'{$valorB22}', 
							'{$valorB23}', 
							'{$valorB24}', 
							'{$valorB25}', 
							'{$valorB26}', 
							'{$valorB27}', 
							'{$valorB28}', 
							'{$valorB29}', 
							'{$valorB210}', 
							'{$valorB211}', 
							'{$valorB212}', 
							'{$valorB213}', 
							'{$valorB214}', 
							'{$valorB215}', 
							'{$valorB216}', 
							'{$valorB217}', 
							'{$valorB218}', 
							'{$valorB219}', 
							'{$valorB220}', 
							'{$valorB221}', 
							'{$valorB222}', 
							'{$valorB223}', 
							'{$valorB224}', 
							'{$valorB225}', 
							'{$valorB226}', 
							'{$valorB227}', 
							'{$valorB228}', 
							'{$valorB229}', 
							'{$valorB230}', 
							'{$valorB231}', 
							'{$valorB232}', 
							'{$valorB233}', 
							'{$valorB234}', 
							'{$valorB235}', 
							'{$valorB236}', 
							'{$valorB237}', 
							'{$valorB238}', 
							'{$valorB239}', 
							'{$valorB240}', 
							'{$valorB241}', 
							'{$valorB242}', 
							'{$valorB243}', 
							'{$valorB244}', 
							'{$valorB245}', 
							'{$valorB246}', 
							'{$valorB247}', 
							'{$data_hora_atual}', 
							'{$user_id}', 
							'{$user_id}', 
							'{$data_hora_atual}'
						)");
					$pdoB3->execute();
					$novo_id_ambiente_rodape = $connection->lastInsertId();

					//se possui observacoes....
					if (!empty($valorB246)){
						$monta_obs = "<b>".$valorB22.": ".$valorB246."</b>";
						$pdoX4 = $connection->prepare("INSERT INTO orcamentos_observacoes (
								orcamento_id, 
								ambiente_id, 
								nro_item, 
								descricao, 
								tipo, 
								cadastrado_por, 
								alterado_por, 
								data_cadastro, 
								data_alteracao
							) VALUES (
								'{$orcamento_nro}', 
								'{$novo_id_ambiente_rodape}', 
								'{$novo_item_nro}', 
								'{$monta_obs}', 
								'G', 
								'{$user_id}', 
								'{$user_id}', 
								'{$data_hora_atual}', 
								'{$data_hora_atual}'
							)");
						$pdoX4->execute();
					}
				}
			}

			//gera os custos
			$pdo = $connection->prepare("INSERT INTO vendas_custos SELECT NULL, 
					cod_venda, 
					'{$novo_id_item_rodape}',
					cod_produto, 
					cod_cor, 
					quantidade, 
					valor_unitario, 
					acrescimo_mo, 
					acrescimo_produto, 
					valor_total,
					cod_instalador, 
					data_cadastro, 
					cadastrado_por, 
					alterado_por, 
					data_alteracao, 
					observacoes 
				FROM vendas_custos WHERE cod_item_venda = ?
			");
			$pdo->execute(array(
				$_POST['id_produtos_orcamentos_rodape']
			));
			echo json_encode(array('success'=>true, 'msg'=>$novo_id_item_rodape.' Ítem Copiado com Sucesso'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>'Erro...', 'erro'=>$e->getMessage()));
	}
}