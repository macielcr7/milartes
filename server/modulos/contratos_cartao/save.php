<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'contratos_cartao';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		for($i=1; $i < 25; $i++) {
			${"taxa{$i}"} = $_POST['taxa'.$i];
			//${"taxa{$i}"} = str_replace(' %','',${"taxa{$i}"});
			${"taxa{$i}"} = str_replace('.','',${"taxa{$i}"});
			${"taxa{$i}"} = str_replace(',','.',${"taxa{$i}"});
			if (${"taxa{$i}"} == "") { ${"taxa{$i}"} = 0; }
		}

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE contratos_cartao SET 
				conta_bancaria = ?, 
				descricao = ?, 
				tipo = ?, 
				nro_max_parcelas = ?, 
				taxa1 = ?, 
				taxa2 = ?, 
				taxa3 = ?, 
				taxa4 = ?, 
				taxa5 = ?, 
				taxa6 = ?, 
				taxa7 = ?, 
				taxa8 = ?, 
				taxa9 = ?, 
				taxa10 = ?, 
				taxa11 = ?, 
				taxa12 = ?, 
				taxa13 = ?, 
				taxa14 = ?, 
				taxa15 = ?, 
				taxa16 = ?, 
				taxa17 = ?, 
				taxa18 = ?, 
				taxa19 = ?, 
				taxa20 = ?, 
				taxa21 = ?, 
				taxa22 = ?, 
				taxa23 = ?, 
				taxa24 = ?, 
				dias_credito_parc_vista = ?, 
				dias_credito_parc_estab = ?, 
				tipo_calculo = ?, 
				ativo = ?, 
				alterado_por = ? 
				WHERE id = ?
			");
			$params = array(
				$_POST['conta_bancaria'],
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$_POST['tipo'],
				$_POST['nro_max_parcelas'],
				$taxa1,
				$taxa2,
				$taxa3,
				$taxa4,
				$taxa5,
				$taxa6,
				$taxa7,
				$taxa8,
				$taxa9,
				$taxa10,
				$taxa11,
				$taxa12,
				$taxa13,
				$taxa14,
				$taxa15,
				$taxa16,
				$taxa17,
				$taxa18,
				$taxa19,
				$taxa20,
				$taxa21,
				$taxa22,
				$taxa23,
				$taxa24,
				$_POST['dias_credito_parc_vista'],
				$_POST['dias_credito_parc_estab'],
				$_POST['tipo_calculo'],
				$_POST['ativo'],
				$user_id,
				$_POST['id']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO contratos_cartao 
					(
						conta_bancaria,
						descricao,
						tipo,
						nro_max_parcelas,
						taxa1,
						taxa2,
						taxa3,
						taxa4,
						taxa5,
						taxa6,
						taxa7,
						taxa8,
						taxa9,
						taxa10,
						taxa11,
						taxa12,
						taxa13,
						taxa14,
						taxa15,
						taxa16,
						taxa17,
						taxa18,
						taxa19,
						taxa20,
						taxa21,
						taxa22,
						taxa23,
						taxa24,
						dias_credito_parc_vista,
						dias_credito_parc_estab,
						tipo_calculo,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						ativo
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['conta_bancaria'],
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$_POST['tipo'],
				$_POST['nro_max_parcelas'],
				$taxa1,
				$taxa2,
				$taxa3,
				$taxa4,
				$taxa5,
				$taxa6,
				$taxa7,
				$taxa8,
				$taxa9,
				$taxa10,
				$taxa11,
				$taxa12,
				$taxa13,
				$taxa14,
				$taxa15,
				$taxa16,
				$taxa17,
				$taxa18,
				$taxa19,
				$taxa20,
				$taxa21,
				$taxa22,
				$taxa23,
				$taxa24,
				$_POST['dias_credito_parc_vista'],
				$_POST['dias_credito_parc_estab'],
				$_POST['tipo_calculo'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				$_POST['ativo']
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}