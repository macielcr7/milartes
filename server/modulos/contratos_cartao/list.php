<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'contratos_cartao';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT * FROM contratos_cartao WHERE id=:id"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";
			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE CC.descricao like '%$_POST[query]%'";
				$where2 = " AND CC.descricao like '%$_POST[query]%'";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_cartao'])) {
					$where = "WHERE CC.ativo='S'".$where2;
				}
				elseif ($_POST['status_cartao'] == 'X') {
					$where = "".$where1;
				}
				else{
					$where = "WHERE CC.ativo='{$_POST['status_cartao']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM contratos_cartao AS CC $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT CC.*, 
			DATE_FORMAT(CC.data_cadastro, '%d/%m/%Y %H:%i:%s') AS data_cadastro, 
				CONCAT(UPPER(E.razao_social), ' / ', UPPER(E.nome_fantasia)) AS nome_empresa,
				CONCAT(UPPER(CB.nome_banco), ' / ', CB.agencia, '-', CB.agencia_digito, ' / ', CB.conta_corrente, '-', CB.conta_corrente_digito) AS banco_agencia_conta,
				UPPER(U.nome) AS cadastrado_por_nome
				FROM contratos_cartao AS CC INNER JOIN usuarios AS U ON (U.id=CC.cadastrado_por) 
				INNER JOIN contas_bancarias AS CB ON (CB.id=CC.conta_bancaria)
				INNER JOIN empresas_sistema AS E ON (E.cod_empresa=CB.cod_empresa) 
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}	
}
?>