<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'produtos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT * FROM produtos WHERE id=:id");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_ASSOC);

			//$sql = "SELECT AP.* FROM atributos_produto AS AP INNER JOIN atributos_categoria AS AC WHERE AP.id_produto = {$_POST['id']} AND AC.id_categoria = {$linhas['categoria']}";

			$sql = "SELECT * FROM atributos_produto WHERE id_produto = {$_POST['id']}";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas['attrs'] = $pdo->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode(array('success'=>true, 'dados'=>$linhas));
		}
		else if( isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO' ){
			$query = 'WHERE 1 = 1';
			if( (isset($_POST['cod_categoria']) and !empty($_POST['cod_categoria'])) or
				(isset($_POST['in_cod_categoria']) and !empty($_POST['in_cod_categoria'])) ){
				
				if(isset($_POST['cod_categoria'])){
					$sql = "
						SELECT *, cod_categoria AS id, UPPER(descricao) AS descricao 
						FROM produtos_categorias 
						WHERE cod_categoria = {$_POST['cod_categoria']}
					";
				}
				else{
					$sql = "
						SELECT *, cod_categoria AS id, UPPER(descricao) AS descricao 
						FROM produtos_categorias 
						WHERE cod_categoria IN (".stripcslashes($_POST["in_cod_categoria"]).") ";	
				}

				$pdo = $connection->prepare($sql);
				$pdo->execute();

				$linhas = $pdo->fetchAll(PDO::FETCH_ASSOC);

				$data = array();
				foreach ($linhas as $key => $value) {
					$value['childs'] = childsCategorias($connection, $value);
					$data[] = $value;
				}

				$categorias = processCombobox($data);
				$idsCategorias = array();

				foreach ($categorias as $key => $value) {
					$idsCategorias[] = $value['id'];
				}

				$inCategorias = implode(',', $idsCategorias);
				$query .= " AND categoria IN ({$inCategorias})";
			}

			if(isset($_POST['not_tipo_produto'])){
				$query .= ' AND P.tipo_produto <> "'.$_POST['not_tipo_produto'].'" ';
			}
			else if(isset($_POST['tipo_produto'])){
				$query .= ' AND P.tipo_produto = "'.$_POST['tipo_produto'].'" ';
			}
			else if(isset($_POST['in_tipo_produto'])){
				$query .= ' AND P.tipo_produto IN ('.stripcslashes($_POST["in_tipo_produto"]).') ';
			}

			if (isset($_POST['query']) AND !empty($_POST['query'])) {
				//$query .= " AND P.descricao_completa like '%$_POST[query]%'";
				$cada_palavra = addslashes(htmlspecialchars($_POST['query']));
				$palavras = explode(' ',$cada_palavra);
				$total_palavras = count($palavras);

				$i = 0;
				while ($i != $total_palavras)
				{
					$query .= " AND P.descricao_completa LIKE '%$palavras[$i]%'";
					$i++;
				}
			}


			$sql1 = "SELECT P.id, P.tipo_produto, P.dobra_quantidade, CONCAT(UPPER(P.descricao_completa), ' - ', UPPER(UM.sigla)) AS descricao 
				FROM produtos AS P 
				INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
				{$query} AND P.ativo='S' ORDER BY descricao ASC
			";
			$pdo1 = $connection->prepare($sql1);
			$pdo1->execute();
			
			$linhas = $pdo1->fetchAll(PDO::FETCH_ASSOC);
			//$cod_produtos = array(14, 20, 21, 26, 28, 29, 30, 166, 167, 268, 271, 272, 273, 345, 346, 347, 348, 349, 350);

			foreach ($linhas as $key => $value) {
				$sql2 = "SELECT * FROM produto_precos WHERE id_produto = {$value['id']}";
				$pdo2 = $connection->prepare($sql2);
				$pdo2->execute();

				$linhas[$key]['produto_precos'] = $pdo2->fetchAll(PDO::FETCH_ASSOC);

				/*
				foreach ($linhas[$key]['produto_precos'] as $key2 => $value2) {
					if (in_array($linhas[$key]['produto_precos'][$key2]['id_produto'], $cod_produtos) AND $_COOKIE['id_empresa_atual'] == 2){
						$linhas[$key]['produto_precos'][$key2]['valor'] = $linhas[$key]['produto_precos'][$key2]['valor'] + 1.30;
					}
				}
				*/

				$sql3 = "
					SELECT atributos_produto.valor, atributos.sigla FROM atributos_produto 
					INNER JOIN produtos ON (produtos.id=atributos_produto.id_produto) 
					INNER JOIN atributos ON (atributos.id_atributos=atributos_produto.id_atributo_categoria) 
					WHERE id_produto = {$value['id']}
				";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();
				$linhas[$key]['atributos_produto'] = $pdo3->fetchAll(PDO::FETCH_ASSOC);

				//cores do produto
				$sql4 = "
					SELECT PC.id_cor, C.descricao, C.codigo FROM produtos_cores AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_produto) 
					INNER JOIN cores AS C ON (C.id_cores=PC.id_cor) 
					WHERE PC.id_produto = {$value['id']}
				";
				$pdo4 = $connection->prepare($sql4);
				$pdo4->execute();
				$linhas[$key]['produto_cores'] = $pdo4->fetchAll(PDO::FETCH_ASSOC);

			}
			//echo json_encode(array('dados'=>$linhas));

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			$sql1 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql1);
			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			$sql3 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql3);
			$sql4 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql4);
			echo json_encode(array('dados'=>$linhas, 'sql'=>$sql, 'sql1'=>$sql1, 'sql2'=>$sql2, 'sql3'=>$sql3, 'sql4'=>$sql4));
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			
			if(isset($_POST['cod_categoria']) and !empty($_POST['cod_categoria'])) {
				$sql = "
					SELECT *, cod_categoria AS id, UPPER(descricao) AS descricao 
					FROM produtos_categorias 
					WHERE cod_categoria = {$_POST['cod_categoria']}
				";
				$pdo = $connection->prepare($sql);
				$pdo->execute();

				$linhas = $pdo->fetchAll(PDO::FETCH_ASSOC);

				$data = array();
				foreach ($linhas as $key => $value) {
					$value['childs'] = childsCategorias($connection, $value);
					$data[] = $value;
				}

				$categorias = processCombobox($data);
				$idsCategorias = array();

				foreach ($categorias as $key => $value) {
					$idsCategorias[] = $value['id'];
				}

				$inCategorias = implode(',', $idsCategorias);
				$categoria_sql = " AND P.categoria IN ({$inCategorias})";
			}
			else {
				$categoria_sql = "";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('descricao_completa', 'P.descricao_completa'), $_POST['descricao_completa'], 'like');
				$buscar->setBusca(array('descricao_abreviada', 'P.descricao_abreviada'), $_POST['descricao_abreviada'], 'like');
				$buscar->setBusca(array('departamento', 'P.departamento'), $_POST['departamento']);
				$buscar->setBusca(array('categoria', 'P.categoria'), $_POST['categoria']);
				$buscar->setBusca(array('fornecedor', 'P.fornecedor'), $_POST['fornecedor']);
				$buscar->setBusca(array('setor_estoque', 'P.setor_estoque'), $_POST['setor_estoque']);
				$buscar->setBusca(array('fabricante_marca', 'P.fabricante_marca'), $_POST['fabricante_marca']);
				$buscar->setBusca(array('unidade_medida', 'P.unidade_medida'), $_POST['unidade_medida']);
				$buscar->setBusca(array('ncm', 'P.ncm'), $_POST['ncm'], 'like');
				$buscar->setBusca(array('ativo', 'P.ativo'), $_POST['ativo'], 'like');
				$buscar->setBusca(array('data_cadastro', 'P.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('cadastrado_por', 'P.cadastrado_por'), $_POST['cadastrado_por']);
				$buscar->setBusca(array('alterado_por', 'P.alterado_por'), $_POST['alterado_por']);
				$filtro = $buscar->getSql();
				$filtro2 = $filtro." ".$categoria_sql;
			}
			elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_produto'])) {
					$buscar->setBusca(array('ativo', 'P.ativo'), 'S');
					$status_produto = " AND P.ativo='S'";
				}
				elseif ($_POST['status_produto'] == 'X') {
					$buscar->setBusca(array('1'), '1');
					$status_produto = "";
				}
				else{
					$buscar->setBusca(array('ativo', 'P.ativo'), $_POST['status_produto']);
					$status_produto = " AND P.ativo='{$_POST['status_produto']}'";
				}

				if(isset($_POST['query']) and !empty($_POST['query'])){
					$buscar->setBusca(array('descricao_completa', 'P.descricao_completa'), $_POST['query'], 'like');
					$buscar->setBusca(array('nome_completo', 'F.nome_completo'), $_POST['query'], 'like');
					$buscar->setBusca(array('razao_social', 'F.razao_social'), $_POST['query'], 'like');
					$buscar->setBusca(array('nome_fantasia', 'F.nome_fantasia'), $_POST['query'], 'like');

					//$filtro = "P.descricao_completa LIKE '%{$_POST['query']}%' OR P.descricao_completa LIKE '%{$_POST['query']}%' OR F.nome_completo LIKE '%{$_POST['query']}%' OR F.razao_social LIKE '%{$_POST['query']}%' OR F.nome_fantasia LIKE '%{$_POST['query']}%'";
					$filtro = "P.descricao_completa LIKE '%{$_POST['query']}%' OR PM.descricao LIKE '%{$_POST['query']}%'";
					if(!empty($_POST['cod_categoria'])){
						$filtro2 = "WHERE P.categoria IN ({$inCategorias}) AND ({$filtro})";
					}
					else{
						$filtro2 = "WHERE ({$filtro})";
					}
					//$filtro = $buscar->getSql();
				}
				else{
					$filtro2 = $categoria_sql;
				}
			}

			//$filtro = $buscar->getSql();
			$inCategorias = implode(',', $idsCategorias);

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$sqlTotal = "SELECT count(*) AS total FROM produtos AS P 
					INNER JOIN produtos_categorias AS PC ON (P.categoria=PC.cod_categoria) 
					INNER JOIN fornecedores AS F ON (P.fornecedor=F.cod_fornecedor) 
					INNER JOIN setor_estoque AS SE ON (P.setor_estoque=SE.id) 
					INNER JOIN produtos_marcas AS PM ON (P.fabricante_marca=PM.cod_marca) 
					INNER JOIN unidades_medidas AS UM ON (P.unidade_medida=UM.id) {$filtro2} {$status_produto}";
			$pdo = $connection->prepare($sqlTotal);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT P.*, 
					UPPER(P.descricao_completa) AS descricao_completa, 
					UPPER(PC.descricao) AS descricao_categoria, 
					F.tipo_fornecedor AS tipo_fornecedor, 
					F.razao_social AS razao_social, 
					F.nome_fantasia AS nome_fantasia, 
					F.nome_completo AS nome_completo, 
					UPPER(SE.descricao) AS descricao_setor_estoque, 
					UPPER(PM.descricao) AS descricao_marca, 
					UPPER(U.nome) AS cadastrado_por_nome, 
					UPPER(UM.unidade_medida) AS unidade_medida_descricao FROM produtos AS P 
					INNER JOIN produtos_categorias AS PC ON	(P.categoria=PC.cod_categoria) 
					INNER JOIN fornecedores AS F ON (P.fornecedor=F.cod_fornecedor) 
					INNER JOIN setor_estoque AS SE ON (P.setor_estoque=SE.id) 
					INNER JOIN produtos_marcas AS PM ON (P.fabricante_marca=PM.cod_marca) 
					INNER JOIN unidades_medidas AS UM ON (P.unidade_medida=UM.id) 
					INNER JOIN usuarios AS U ON (U.id=P.cadastrado_por) {$filtro2} {$status_produto} ORDER BY {$sort} {$order} LIMIT {$start}, {$limit}
			";

			$data = $buscar->getArrayExecute();

			$pdo = $connection->prepare($sql);
			$pdo->execute( $data );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$query[$key] = $value;
				if($query[$key]->tipo_fornecedor == "F") {
					$query[$key]->monta_nome_fornecedor = $query[$key]->nome_completo;
				}
				else{
					if($query[$key]->nome_fantasia != ""){
						$query[$key]->monta_nome_fornecedor = $query[$key]->razao_social." (".$query[$key]->nome_fantasia.")";	
					}
					else{
						$query[$key]->monta_nome_fornecedor = $query[$key]->razao_social;
					}
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'total'=>$countRow, 'sql'=>$sql, 'data'=> $data) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'sqlTotal'=>$sqlTotal, 'sql'=>$sql, 'erro'=>$e->getMessage()));
	}
}
?>