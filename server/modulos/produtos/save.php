<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'produtos';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		if (empty($_POST['valor_comissao']) OR $_POST['valor_comissao'] == "0,00") {$_POST['valor_comissao'] = 0; }
		if (empty($_POST['preco_custo']) OR $_POST['preco_custo'] == "0,00") { $_POST['preco_custo'] = 0; }

		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE produtos SET 
						descricao_completa = ?,
						descricao_abreviada = ?,
						descricao_impressao = ?,
						categoria = ?,
						fornecedor = ?,
						setor_estoque = ?,
						fabricante_marca = ?,
						unidade_medida = ?,
						ncm = ?,
						peso_bruto = ?,
						peso_liquido = ?,
						prazo_garantia = ?,
						desconto_maximo = ?,
						tipo_produto = ?,
						comissionar_vendedor = ?,
						comissionar_tipo = ?,
						valor_comissao = ?,
						controlar_estoque = ?,
						quantidade_minima = ?,
						quantidade_disponivel = ?,
						possui_composicao = ?,
						preco_custo = ?,
						alterado_por = ?,
						data_alteracao = ?,
						ativo = ?,
						preco_custo_servico = ?,
						dobra_quantidade = ? 
					WHERE id = ?
			");
			$params = array(
				trim(rtrim($_POST['descricao_completa'])),
				trim(rtrim($_POST['descricao_abreviada'])),
				trim(rtrim($_POST['descricao_impressao'])),
				$_POST['categoria'],
				$_POST['fornecedor'],
				$_POST['setor_estoque'],
				$_POST['fabricante_marca'],
				$_POST['unidade_medida'],
				$_POST['ncm'],
				$_POST['peso_bruto'],
				$_POST['peso_liquido'],
				$_POST['prazo_garantia'],
				$_POST['desconto_maximo'],
				$_POST['tipo_produto'],
				$_POST['comissionar_vendedor'],
				$_POST['comissionar_tipo'],
				$_POST['valor_comissao'],
				$_POST['controlar_estoque'],
				$_POST['quantidade_minima'],
				$_POST['quantidade_disponivel'],
				$_POST['possui_composicao'],
				$_POST['preco_custo'],
				$user_id,
				$data_hora_atual,
				$_POST['ativo'],
				(isset($_POST['preco_custo_servico']) ? $_POST['preco_custo_servico'] : 0),
				$_POST['dobra_quantidade'],
				$_POST['id']
			);
			$pdo->execute($params);

			foreach ($_POST['attrs'] as $key => $value) {
				$id_attr = explode(':', $key);
				$id_attr = $id_attr[0];

				if(empty($value)){
					$value = 0;
				}
				else{
					$value = implode('.', explode(',', $value));
				}

				$pdo = $connection->prepare("
					SELECT COUNT(*) as total, id_atributos_produto FROM atributos_produto
					WHERE id_produto = {$_POST['id']} AND id_atributo_categoria = {$id_attr}
				");
				$pdo->execute();
				$count = $pdo->fetch(PDO::FETCH_OBJ);

				if($count->total == 0 AND $value != 0){
					$pdo = $connection->prepare("
						INSERT INTO atributos_produto 
							(
								id_atributo_categoria,
								id_produto,
								valor,
								data_cadastro,
								cadastrado_por,
								alterado_por,
								data_alteracao
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?
							)
					");
					$params = array(
						$id_attr,
						$_POST['id'],
						$value,
						$data_hora_atual,
						$user_id,
						$user_id,
						$data_hora_atual
					);
					$pdo->execute($params);
				}
				else{
					$pdo = $connection->prepare("
						UPDATE atributos_produto SET 
							id_atributo_categoria = ?,
							id_produto = ?,
							valor = ?,
							alterado_por = ?,
							data_alteracao = ?
							WHERE id_atributos_produto = ? 
					");
					$params = array(
						$id_attr,
						$_POST['id'],
						$value,
						$user_id,
						$data_hora_atual,
						$count->id_atributos_produto
					);
					$pdo->execute($params);
				}
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO produtos 
					(
						descricao_completa,
						descricao_abreviada,
						descricao_impressao,
						categoria,
						fornecedor,
						setor_estoque,
						fabricante_marca,
						unidade_medida,
						ncm,
						peso_bruto,
						peso_liquido,
						prazo_garantia,
						desconto_maximo,
						tipo_produto,
						comissionar_vendedor,
						comissionar_tipo,
						valor_comissao,
						controlar_estoque,
						quantidade_minima,
						quantidade_disponivel,
						possui_composicao,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						data_alteracao,
						ativo,
						preco_custo,
						preco_custo_servico,
						dobra_quantidade
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	
						?,	?,	?,	?,	?,	
						?,	?,	?,	?,	?,	
						?,	?,	?,	?,	?,	
						?,	NOW(),	?,	?,	?,	
						?,	?,	?,	?
					)
			");
			$params = array(
				trim(rtrim($_POST['descricao_completa'])),
				trim(rtrim($_POST['descricao_abreviada'])),
				trim(rtrim($_POST['descricao_impressao'])),
				$_POST['categoria'],
				$_POST['fornecedor'],
				$_POST['setor_estoque'],
				$_POST['fabricante_marca'],
				$_POST['unidade_medida'],
				$_POST['ncm'],
				$_POST['peso_bruto'],
				$_POST['peso_liquido'],
				$_POST['prazo_garantia'],
				$_POST['desconto_maximo'],
				$_POST['tipo_produto'],
				$_POST['comissionar_vendedor'],
				$_POST['comissionar_tipo'],	
				$_POST['valor_comissao'],
				$_POST['controlar_estoque'],
				$_POST['quantidade_minima'],
				$_POST['quantidade_disponivel'],
				$_POST['possui_composicao'],
				$user_id,
				$user_id,
				$data_hora_atual,
				$_POST['ativo'],
				$_POST['preco_custo'],
				(isset($_POST['preco_custo_servico']) ? $_POST['preco_custo_servico'] : 0),
				$_POST['dobra_quantidade']
			);
			$pdo->execute($params);
			$_POST['id'] = $connection->lastInsertId();

			foreach ($_POST['attrs'] as $key => $value) {
				$id_attr = explode(':', $key);
				$id_attr = $id_attr[0];

				if(empty($value)){
					$value = 0;
				}
				else{
					$value = implode('.', explode(',', $value));
				}

				$pdo = $connection->prepare("
					INSERT INTO atributos_produto 
						(
							id_atributo_categoria,
							id_produto,
							valor,
							data_cadastro,
							cadastrado_por,
							alterado_por,
							data_alteracao
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?
						)
				");
				$params = array(
					$id_attr,
					$_POST['id'],
					$value,
					$data_hora_atual,
					$user_id,
					$user_id,
					$data_hora_atual
				);
				$pdo->execute($params);
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

		if(in_array($_POST['action'], array('INSERIR', 'EDITAR'))){
			$json_composicoes = json_decode('['.$_POST['json_composicoes'].']', true);
			foreach ($json_composicoes as $key => $item) {
				if($item['id_produto_composicoes']=='0'){
					$pdo = $connection->prepare("
						INSERT INTO produto_composicoes 
							(
								id_produto, 
								id_composicao, 
								quantidade, 
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?
							)
					");
					$params = array(
						$_POST['id'],
						$item['id_composicao'],
						$item['quantidade'],
						$data_hora_atual,
						$user_id,
						$user_id,
						$data_hora_atual
					);
					$pdo->execute($params);
				}
				else{
					$pdo = $connection->prepare("
							UPDATE produto_composicoes SET 
									id_composicao = ?,
									quantidade = ?,
									alterado_por = ?,
									data_alteracao = ? 
		 					WHERE id_produto_composicoes = ?
					");
					$params = array(
						$item['id_composicao'],
						$item['quantidade'],
						$user_id,
						$data_hora_atual,
						$item['id_produto_composicoes']
					);
					$pdo->execute($params);
				}
			}

			/*$json_precos = json_decode(stripcslashes('['.$_POST['json_precos'].']'), true);
			foreach ($json_precos as $key => $item) {
				if($item['id_produto_precos']=='0'){
					$pdo = $connection->prepare("
						INSERT INTO produto_precos 
							(
								id_produto, id_categoria_precos, valor
							) 
						VALUES 
							(
								?,	?,	?
							)
					");
					$params = array(
						$_POST['id'],
						$item['id_categoria_precos'],
						$item['valor']
					);
					$pdo->execute($params);
				}
				else{
					$pdo = $connection->prepare("
							UPDATE produto_precos SET 
									id_categoria_precos = ?,
									valor = ?,
									id_produto = ?
		 					WHERE id_produto_precos = ?
					");
					$params = array(
						$item['id_categoria_precos'],
						$item['valor'],
						$item['id_produto'],
						$item['id_produto_precos']
					);
					$pdo->execute($params);
				}
			}*/

			/*$json_cores = json_decode(stripcslashes('['.$_POST['json_cores'].']'), true);
			
			foreach ($json_cores as $key => $item) {
				if($item['id_produtos_cores']=='0'){
					$pdo = $connection->prepare("
						INSERT INTO produtos_cores
							(
								id_produto, id_cor, data_cadastro, cadastrado_por, alterado_por, data_alteracao
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?
							)
					");
					$params = array(
						$_POST['id'],
						$item['id_cor'],
						date('Y-m-d H:i:s'), 
						$user_id,
						$user_id,
						date('Y-m-d H:i:s')
					);
					$pdo->execute($params);
				}
				else{
					$pdo = $connection->prepare("
							UPDATE produtos_cores SET 
									id_cor = ?,
									id_produto = ?
		 					WHERE id_produtos_cores = ?
					");
					$params = array(
						$item['id_cor'],
						$_POST['id'],
						$item['id_produtos_cores']
					);
					$pdo->execute($params);
				}
			}*/
		}

		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS, 'id_produto'=> $_POST['id']));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}
