<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'produtos';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if($_POST['action'] == 'COPIA_PRODUTO'){
			$id_produto = $_POST['id_produto'];

			//copia o produto
			$sql = "SELECT descricao_completa,
					descricao_abreviada,
					descricao_impressao,
					categoria,
					fornecedor,
					setor_estoque,
					fabricante_marca,
					unidade_medida,
					ncm,
					peso_bruto,
					peso_liquido,
					prazo_garantia,
					desconto_maximo,
					tipo_produto,
					comissionar_vendedor,
					comissionar_tipo,
					valor_comissao,
					controlar_estoque,
					quantidade_minima,
					quantidade_disponivel,
					possui_composicao,
					ativo,
					preco_custo,
					preco_custo_servico 
				FROM produtos WHERE id = '{$id_produto}'";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			foreach ($query as $key => $value) {
				$rows[$key] = $value;
				$descricao_completa =  $rows[$key]->descricao_completa." - Cópia";

				$pdoA = $connection->prepare("INSERT INTO produtos 
					(
						descricao_completa,
						descricao_abreviada,
						descricao_impressao,
						categoria,
						fornecedor,
						setor_estoque,
						fabricante_marca,
						unidade_medida,
						ncm,
						peso_bruto,
						peso_liquido,
						prazo_garantia,
						desconto_maximo,
						tipo_produto,
						comissionar_vendedor,
						comissionar_tipo,
						valor_comissao,
						controlar_estoque,
						quantidade_minima,
						quantidade_disponivel,
						possui_composicao,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						ativo,
						preco_custo,
						preco_custo_servico
					) 
						VALUES 
					(
						'{$descricao_completa}',
						'{$rows[$key]->descricao_abreviada}',
						'{$rows[$key]->descricao_impressao}',
						'{$rows[$key]->categoria}',
						'{$rows[$key]->fornecedor}',
						'{$rows[$key]->setor_estoque}',
						'{$rows[$key]->fabricante_marca}',
						'{$rows[$key]->unidade_medida}',
						'{$rows[$key]->ncm}',
						'{$rows[$key]->peso_bruto}',
						'{$rows[$key]->peso_liquido}',
						'{$rows[$key]->prazo_garantia}',
						'{$rows[$key]->desconto_maximo}',
						'{$rows[$key]->tipo_produto}',
						'{$rows[$key]->comissionar_vendedor}',
						'{$rows[$key]->comissionar_tipo}',
						'{$rows[$key]->valor_comissao}',
						'{$rows[$key]->controlar_estoque}',
						'{$rows[$key]->quantidade_minima}',
						'{$rows[$key]->quantidade_disponivel}',
						'{$rows[$key]->possui_composicao}',
						NOW(),
						$user_id,
						$user_id,
						'{$rows[$key]->ativo}',
						'{$rows[$key]->preco_custo}',
						'{$rows[$key]->preco_custo_servico}'
					)
				");
				$pdoA->execute();
			}

			//pega o novo ID do produto
			$novo_id = $connection->lastInsertId();

			//atributos do produto
			$pdoB = $connection->prepare("SELECT id_atributo_categoria, valor FROM atributos_produto WHERE id_produto = '{$id_produto}' ORDER BY id_atributos_produto");
			$pdoB->execute();

			while ($rowB = $pdoB->fetch(PDO::FETCH_ASSOC)) {
				$pdoB1 = $connection->prepare("INSERT INTO atributos_produto 
					(
						id_atributo_categoria,
						id_produto,
						valor, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
						VALUES 
					(
						'{$rowB['id_atributo_categoria']}',
						'$novo_id',
						'{$rowB['valor']}',
						NOW(),
						'$user_id',
						'$user_id',
						NOW()
					)
				");
				$pdoB1->execute();
			}


			//composiçõe do produto
			$pdoC = $connection->prepare("SELECT id_composicao, quantidade FROM produto_composicoes WHERE id_produto = '{$id_produto}'");
			$pdoC->execute();

			while ($rowC = $pdoC->fetch(PDO::FETCH_ASSOC)) {
				$pdoC1 = $connection->prepare("INSERT INTO produto_composicoes 
					(
						id_produto, 
						id_composicao, 
						quantidade, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
						VALUES 
					(
						'$novo_id', 
						'{$rowC['id_composicao']}', 
						'{$rowC['quantidade']}', 
						NOW(),
						'$user_id',
						'$user_id',
						NOW()
					)
				");
				$pdoC1->execute();
			}


			//preços do produto
			$pdoD = $connection->prepare("SELECT id_categoria_precos, valor FROM produto_precos WHERE id_produto = '{$id_produto}'");
			$pdoD->execute();

			while ($rowD = $pdoD->fetch(PDO::FETCH_ASSOC)) {
				$pdoD1 = $connection->prepare("INSERT INTO produto_precos 
					(
						id_produto, 
						id_categoria_precos, 
						valor, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
						VALUES 
					(
						'$novo_id', 
						'{$rowD['id_categoria_precos']}', 
						'{$rowD['valor']}', 
						NOW(),
						'$user_id',
						'$user_id',
						NOW()
					)
				");
				$pdoD1->execute();
			}


			//cores do produto
			$pdoE = $connection->prepare("SELECT id_cor FROM produtos_cores WHERE id_produto = '{$id_produto}'");
			$pdoE->execute();

			while ($rowE = $pdoE->fetch(PDO::FETCH_ASSOC)) {
				$pdoE1 = $connection->prepare("INSERT INTO produtos_cores
					(
						id_produto, 
						id_cor, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
						VALUES 
					(
						'$novo_id', 
						'{$rowE['id_cor']}', 
						NOW(), 
						'$user_id', 
						'$user_id', 
						NOW()
					)
				");
				$pdoE1->execute();
			}

			/*
			//imagens do produto
			$pdoF = $connection->prepare("SELECT src FROM produto_imagens WHERE id_produto = '{$id_produto}'");
			$pdoF->execute();

			while ($rowF = $pdoF->fetch(PDO::FETCH_ASSOC)) {
				$pdoF1 = $connection->prepare("INSERT INTO produto_imagens
					(
						id_produto, 
						src
					) 
						VALUES 
					(
						'$novo_id', 
						'{$rowF['src']}'
					)
				");
				$pdoF1->execute();
			}
			*/

			echo json_encode(array('success'=>true, 'msg'=>'Produto Copiado com Sucesso'));
		}

		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>'Erro...', 'erro'=>$e->getMessage()));
	}
}
?>