<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'produtos';
	try {
		if($_POST['action'] == 'DELETAR'){
			$user->getAcao($tabela, 'deletar');
			$pdo = $connection->prepare("UPDATE produtos SET ativo='N' WHERE id = ?");
			$pdo->execute(array(
				$_POST['id']
			));

			echo json_encode(array('success'=>true, 'msg'=>'PRODUTO DESATIVADO COM SUCESSO'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>'ERRO AO DESATIVAR O PRODUTO', 'erro'=>$e->getMessage()));
	}
}