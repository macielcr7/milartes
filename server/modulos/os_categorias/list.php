<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'os_categorias';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ) {
			$sql = "SELECT * FROM os_categorias WHERE id=:id";
			$pdo = $connection->prepare($sql);
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$sql = "SELECT id, UPPER(descricao) AS descricao FROM os_categorias";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_OS'){
			$sql = "SELECT id, UPPER(descricao) AS descricao FROM os_categorias WHERE status='ATIVO'";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('descricao', 'os_categorias.descricao'), $_POST['descricao'], 'like');
				$buscar->setBusca(array('status', 'os_categorias.status'), $_POST['status'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total FROM os_categorias {$filtro}");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);

			$countRow = $query->total;

			$sql = "SELECT id, UPPER(descricao) AS descricao, UPPER(status) AS status, 
					CONCAT(DATE_FORMAT(data_cadastro, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(data_cadastro, '%H:%i:%s')) AS data_cadastro2 
				FROM os_categorias 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>