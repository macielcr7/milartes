<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'os_categorias';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		
		if($_POST['action'] == 'EDITAR'){
		
			$user->getAcao($tabela, 'editar');
		
			$pdo = $connection->prepare("
					UPDATE os_categorias SET 
							descricao = ?, 
							status = ?, 
							alterado_por = ? 
 					WHERE id = ?
			");
			$params = array(
				$_POST['descricao'],
				$_POST['status'],
				$user_id, 
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
		
			$user->getAcao($tabela, 'adicionar');
		
			$pdo = $connection->prepare("
				INSERT INTO os_categorias 
					(
						descricao, 
						status, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por 
					) 
				VALUES 
					(
						?,	?,	NOW(),	?,	?
					)
			");
			$params = array(
				$_POST['descricao'], 
				$_POST['status'], 
				$user_id, 
				$user_id
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}