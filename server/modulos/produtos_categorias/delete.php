<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'produtos_categorias';
	try {
		$connection->beginTransaction();

		$pdo = $connection->prepare("UPDATE produtos_categorias SET ativo='N' WHERE cod_categoria = ?");
		$pdo->execute(array($_POST['id']));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Desativado com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar a Categoria!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}