<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'produtos_categorias';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="
				SELECT * FROM produtos_categorias AS PC 
				WHERE PC.cod_categoria=:id
			"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$item = $pdo->fetch(PDO::FETCH_ASSOC);

			$pdo = $connection->prepare("
				SELECT id_atributo FROM atributos_categoria 
				WHERE id_categoria = {$item['cod_categoria']}
			");
			$pdo->execute();
			$query = $pdo->fetchAll(PDO::FETCH_ASSOC);
			$attrs = array();
			foreach ($query as $key => $value) {
				$attrs[] = $value['id_atributo'];
			}

			$item['id_atributos[]'] = $attrs;

			echo json_encode( array('success'=>true, 'dados'=>$item) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_CATEGORIAS'){
			$sql = "
				SELECT PC.*, PC.cod_categoria AS id, UPPER(PC.descricao) AS descricao 
				FROM produtos_categorias AS PC 
				WHERE PC.ativo = 'S' AND id_parent is null 
				ORDER BY PC.descricao ASC
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_ASSOC);

			$data = array();
			foreach ($linhas as $key => $value) {
				$value['attrs'] = attrsCategoria($connection, $value);
				$value['childs'] = childsCategorias($connection, $value);
				$data[] = $value;
			}

			echo json_encode( array('dados'=> processCombobox($data) ) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_DEPTO_OS'){
			$sql = "
				SELECT PC.*, PC.cod_categoria AS id, UPPER(PC.descricao) AS descricao 
				FROM produtos_categorias AS PC 
				WHERE PC.ativo = 'S' AND id_parent is null 
				ORDER BY PC.descricao ASC
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			//echo json_encode( array('dados'=>$rows) );
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_TREE'){
			$sql = "
				SELECT PC.*, UPPER(PC.descricao) AS descricao 
				FROM produtos_categorias AS PC 
				WHERE PC.ativo = 'S' AND id_parent is null 
				ORDER BY PC.descricao ASC
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_ASSOC);

			$data = array();
			foreach ($linhas as $key => $value) {
				$value['childs'] = childsCategorias($connection, $value);
				$data[] = $value;
			}

			echo json_encode( processTree($data) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";
			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE PC.descricao like '%$_POST[query]%'";
				$where2 = " AND PC.descricao like '%$_POST[query]%'";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_categoria'])) {
					$where = "WHERE PC.ativo='S'".$where2;
				}
				elseif ($_POST['status_categoria'] == 'X') {
					$where = "".$where1;
				}
				else{
					$where = "WHERE PC.ativo='{$_POST['status_categoria']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM produtos_categorias AS PC $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT PC.*, DATE_FORMAT(PC.data_cadastro, '%d/%m/%Y ás %H:%i:%s') AS data_cadastro, 
				UPPER(U.nome) AS cadastrado_por_nome 
				FROM produtos_categorias AS PC INNER JOIN usuarios AS U ON (U.id=PC.cadastrado_por) 
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_ASSOC);

			$data = array();
			foreach ($query as $key => $value) {
				$value['parents'] = parentsCategorias($connection, $value);
				$value['parent'] = processNomeCategoriaParent($value['parents']);
				$data[] = $value;
			}

			$result["total"] = $countRow;
			$result["dados"] = $data;

			//echo json_encode($result);
			
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$data, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}	
}
?>