<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	require_once('../../lib/Upload.class.php');

	$tabela = 'produtos_categorias';
	$data_hora_atual = date('Y-m-d H:i:s');
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$up = upload('../../../uploads/produtos_categorias', $_FILES['imagem']);

		if($_POST['action'] == 'EDITAR'){
			if($up==0){
				$sql = ", imagem = '{$up['msg']}'";	
			}
			$pdo = $connection->prepare("
				UPDATE produtos_categorias SET 
				descricao = ?,
				id_parent = ?,
				aliquota_comissao = ?,
				desconto_maximo = ?,
				meta_tag_keywords = ?,
				meta_tag_description = ?,
				cod_depto = ?,
				ativo = ?,
				alterado_por = ? {$sql}
				WHERE cod_categoria = ?
			");
			$params = array(
				$_POST['descricao'],
				$_POST['id_parent'],
				$_POST['aliquota_comissao'],
				$_POST['desconto_maximo'],
				$_POST['meta_tag_keywords'],
				$_POST['meta_tag_description'],
				$_POST['cod_depto'],
				$_POST['ativo'],
				$user_id,
				$_POST['cod_categoria']
			);

			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			if($up==0){
				$_POST['imagem'] = $up['msg'];	
			}
			else{
				$_POST['imagem'] = null;	
			}
			$pdo = $connection->prepare("
				INSERT INTO produtos_categorias 
					(
						descricao,
						id_parent,
						aliquota_comissao,
						desconto_maximo,
						meta_tag_keywords,
						meta_tag_description,
						cod_depto,
						ativo,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						imagem
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['descricao'],
				$_POST['id_parent'],
				$_POST['aliquota_comissao'],
				$_POST['desconto_maximo'],
				$_POST['meta_tag_keywords'],
				$_POST['meta_tag_description'],
				$_POST['cod_depto'],
				$_POST['ativo'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				$_POST['imagem']
			);

			$pdo->execute($params);

			$_POST['cod_categoria'] = $connection->lastInsertId();
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

		if(in_array($_POST['action'], array('INSERIR', 'EDITAR'))){
			if(isset($_POST['id_atributos']) and !empty($_POST['id_atributos'])){
				$pdo = $connection->prepare("
					SELECT * FROM atributos_categoria 
					WHERE id_categoria = {$_POST['cod_categoria']}
				");
				$pdo->execute();
				$query = $pdo->fetchAll(PDO::FETCH_ASSOC);

				$method = array(
					'continue' => array()
				);

				foreach ($query as $key => $attr) {
					if(in_array($attr['id_atributo'], $_POST['id_atributos'])){
						$method['continue'][] = $attr['id_atributo'];
					}
					else{
						$pdo = $connection->prepare("
							DELETE FROM atributos_categoria 
							WHERE id_categoria = {$_POST['cod_categoria']} AND id_atributo = {$attr['id_atributo']}
						");
						$pdo->execute();

						$pdo = $connection->prepare("
							DELETE FROM atributos_produto 
							WHERE id_atributo_categoria = {$attr['id_atributo']}
						");
						$pdo->execute();
					}
				}

				foreach ($_POST['id_atributos'] as $index => $id_atributo) {
					if(in_array($id_atributo, $method['continue'])){
						continue;
					}
					else{
						$pdo = $connection->prepare("
							INSERT INTO atributos_categoria (id_categoria, id_atributo, data_cadastro, cadastrado_por, alterado_por, data_alteracao)
							VALUES ({$_POST['cod_categoria']}, {$id_atributo}, '$data_hora_atual', $user_id, $user_id, '$data_hora_atual')
						");
						$pdo->execute();
					}
				}
			}
			else{
				$pdo = $connection->prepare("
					DELETE FROM atributos_categoria WHERE id_categoria = {$_POST['cod_categoria']}
				");
				$pdo->execute();

				$sql2 = "SELECT id FROM produtos WHERE categoria = {$_POST['cod_categoria']}";
				$pdo2 = $connection->prepare($sql2);
				$pdo2->execute();

				while ($row = $pdo2->fetch(PDO::FETCH_ASSOC)) {
					$pdo = $connection->prepare("
						DELETE FROM atributos_produto 
						WHERE id_produto = {$row['id']}
					");
					$pdo->execute();
				}
			}
		}
		
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}