﻿<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'contas_bancarias';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT * FROM contas_bancarias WHERE id=:id"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'COMBO_CONTRATOS_COBRANCA' ){
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$sql ="SELECT id AS id, 
				CONCAT(UPPER(nome_banco), ' / ', agencia, '-', agencia_digito, ' / ', conta_corrente, '-', conta_corrente_digito) AS descricao
				FROM contas_bancarias WHERE ativo='S'
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";

			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE (CB.nome_banco like '%$_POST[query]%' OR CB.agencia like '%$_POST[query]%')";
				$where2 = " AND (CB.nome_banco like '%$_POST[query]%' OR CB.agencia like '%$_POST[query]%')";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_conta'])) {
					$where = "WHERE CB.ativo='S'".$where2;
				}
				elseif ($_POST['status_conta'] == 'X') {
					$where = "".$where1;
				}
				else{
					$where = "WHERE CB.ativo='{$_POST['status_conta']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM contas_bancarias AS CB $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT CB.*, 
				DATE_FORMAT(CB.data_cadastro, '%d/%m/%Y ?s %H:%i:%s') as data_cadastro, 
				UPPER(U.nome) AS cadastrado_por_nome,
				CONCAT(CB.agencia, '-', CB.agencia_digito, ' / ', CB.conta_corrente, '-', CB.conta_corrente_digito) AS agencia_conta
				FROM contas_bancarias AS CB INNER JOIN usuarios AS U ON (U.id=CB.cadastrado_por)
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}	
}
?>