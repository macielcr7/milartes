<?php
/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'contas_bancarias';
	try {
		$connection->beginTransaction();

		$pdo = $connection->prepare("UPDATE contas_bancarias SET ativo='N' WHERE id = ?");
		$pdo->execute(array($_POST['id']));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Desativado com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar o Instalador!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}