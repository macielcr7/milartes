<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'contas_bancarias';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE contas_bancarias SET 
				cod_empresa = ?,
				cod_banco = ?,
				nome_banco = ?,
				gerente_banco = ?,
				agencia = ?,
				agencia_digito = ?,
				conta_corrente = ?,
				conta_corrente_digito = ?,
				alterado_por = ?,
				ativo = ? 
				WHERE id = ?
			");
			$params = array(
				$_POST['cod_empresa'],
				$_POST['cod_banco'],
				mb_strtoupper($_POST['nome_banco'], 'UTF-8'),
				mb_strtoupper($_POST['gerente_banco'], 'UTF-8'),
				$_POST['agencia'],
				$_POST['agencia_digito'],
				$_POST['conta_corrente'],
				$_POST['conta_corrente_digito'],
				$user_id,
				$_POST['ativo'],
				$_POST['id']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO contas_bancarias 
					(
						cod_empresa,
						cod_banco,
						nome_banco,
						gerente_banco,
						agencia,
						agencia_digito,
						conta_corrente,
						conta_corrente_digito,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						ativo
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_empresa'],
				$_POST['cod_banco'],
				mb_strtoupper($_POST['nome_banco'], 'UTF-8'),
				mb_strtoupper($_POST['gerente_banco'], 'UTF-8'),
				$_POST['agencia'],
				$_POST['agencia_digito'],
				$_POST['conta_corrente'],
				$_POST['conta_corrente_digito'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				$_POST['ativo']
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}