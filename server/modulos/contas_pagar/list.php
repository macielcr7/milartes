<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'contas_pagar';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT CP.*, 
					DATE_FORMAT(CP.vencimento, '%d/%m/%Y') AS vencimento, 
					CONCAT(UPPER(U.nome), ' EM: ', DATE_FORMAT(CP.data_cadastro, '%d/%m/%Y'), ' ', DATE_FORMAT(CP.data_cadastro, '%H:%i:%s')) AS cadastrado_por, 
					CONCAT(UPPER(U2.nome), ' EM: ', DATE_FORMAT(CP.data_alteracao, '%d/%m/%Y'), ' ', DATE_FORMAT(CP.data_alteracao, '%H:%i:%s')) AS alterado_por_nome 
				FROM contas_pagar AS CP 
				LEFT JOIN usuarios AS U ON (U.id=CP.cadastrado_por) 
				LEFT JOIN usuarios AS U2 ON (U2.id=CP.alterado_por) 
				WHERE CP.id={$_POST['id']}";

			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'TIPOS_CONTA' ){
			$sql ="SELECT CP.tipo_conta AS id, CP.tipo_conta AS descricao 
				FROM contas_pagar AS CP 
				WHERE CP.tipo_conta != '' AND CP.tipo_conta like '%$_POST[query]%' 
				GROUP BY CP.tipo_conta 
				ORDER BY CP.tipo_conta ASC
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";
			if (isset($_POST['query']) AND !empty($_POST['query'])) {
				$where .= "WHERE CP.quitado='N' AND (CP.descricao like '%$_POST[query]%'";
				$where .= " OR CP.observacoes like '%$_POST[query]%'";
				$where .= " OR CP.valor_parcela like '%$_POST[query]%'";
				$where .= " OR CP.valor_nf like '%$_POST[query]%'";
				$where .= " OR F.nome_fantasia like '%$_POST[query]%'";
				$where .= " OR F.nome_completo like '%$_POST[query]%'";
				$where .= " OR F.razao_social like '%$_POST[query]%')";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST' AND empty($_POST['query'])) {
				$where .= "WHERE CP.quitado='N'";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				if ($_POST['action2'] == 'CONTAS_CADASTRADAS') {
					$data1 = implode('-', array_reverse(explode('/', $_POST['data_cadastro1'])))." 00:00:00";
					$data2 = implode('-', array_reverse(explode('/', $_POST['data_cadastro2'])))." 23:59:59";
					$where .= "WHERE (CP.data_cadastro BETWEEN '$data1' AND '$data2')";

					if (!empty($_POST['cod_empresa'])) {
						$where .= " AND CP.cod_empresa = '{$_POST['cod_empresa']}'";
					}

					if (!empty($_POST['fornecedor'])) {
						$where .= " AND CP.cod_fornecedor = '{$_POST['fornecedor']}'";
					}

					if (!empty($_POST['descricao'])) {
						$where .= " AND CP.descricao like '%{$_POST['descricao']}%'";
					}

					if (!empty($_POST['status_conta'])) {
						if ($_POST['status_conta'] == 'Q') {
							$where .= " AND CP.quitado = 'S'";
						}
						elseif ($_POST['status_conta'] == 'A') {
							$where .= " AND CP.quitado = 'N'";
						}
					}
				}
				elseif ($_POST['action2'] == 'PAGAMENTOS_EFETUADOS') {
					$data1 = implode('-', array_reverse(explode('/', $_POST['data_pagto1'])));
					$data2 = implode('-', array_reverse(explode('/', $_POST['data_pagto2'])));
					//$where .= "WHERE (contas_pagar_pagtos.data_pagto BETWEEN '$data1' AND '$data2')";


					$where2 = "WHERE (CPP.data_pagto BETWEEN '$data1' AND '$data2')";

					if (!empty($_POST['cod_empresa'])) {
						$where2 .= " AND E.cod_empresa = '{$_POST['cod_empresa']}'";
					}

					if (!empty($_POST['fornecedor'])) {
						$where2 .= " AND F.cod_fornecedor = '{$_POST['fornecedor']}'";
					}

					if (!empty($_POST['descricao'])) {
						$where2 .= " AND CP.descricao like '%{$_POST['descricao']}%'";
					}

					if (!empty($_POST['query'])) {
						$where2 .= " AND CP.descricao like '%{$_POST['query']}%'";
					}

					$where = $where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			if ($sort == 'nome_empresa') {
				$sort = 'E.nome_fantasia';
			} elseif ($sort == 'nome_fornecedor') {
				$sort = 'F.nome_completo '.$order.', F.razao_social '.$order.', F.nome_fantasia '.$order.', CP.vencimento';
			} elseif ($sort == 'descricao') {
				$sort = 'CP.descricao';
			} elseif ($sort == 'vencimento') {
				$sort = 'CP.vencimento';
			} elseif ($sort == 'valor_parcela') {
				$sort = 'CP.valor_parcela';
			} elseif ($sort == 'observacoes') {
				$sort = 'CP.observacoes';
			}
/*
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				if ($_POST['action2'] == 'PAGAMENTOS_EFETUADOS') {
					$sort = 'CPP.data_pagto';
				}
				else {
					$sort = 'CP.data_cadastro';
				}
			}
*/
			$filtro = $buscar->getSql();

			$sql = "SELECT COUNT(DISTINCT CP.id) AS total FROM contas_pagar AS CP INNER JOIN fornecedores AS F ON (F.cod_fornecedor=CP.cod_fornecedor) LEFT JOIN contas_pagar_pagtos AS CPP ON (CPP.cod_conta=CP.id) {$where} LIMIT {$start}, {$limit}";
			//$sql = "SELECT count(DISTINCT CP.id) AS total FROM contas_pagar AS CP {$where} LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			if ($_POST['action2'] == 'PAGAMENTOS_EFETUADOS') {
				$sql1 = "SELECT CP.id, 
					CP.descricao, 
					CP.tipo_conta, 
					CP.documento, 
					CP.observacoes, 
					CP.data_cadastro, 
					CP.vencimento, 
					CP.valor_nf, 
					CP.valor_parcela, 
					CP.parcela_nro, 
					CP.nf_nro, 
					CP.quitado, 
					UPPER(E.nome_fantasia) AS nome_empresa, 
					F.tipo_fornecedor, 
					F.nome_completo, 
					F.razao_social, 
					F.nome_fantasia, 
					(CP.valor_parcela - (SELECT COALESCE(SUM(valor_pago), 0) FROM contas_pagar_pagtos WHERE cod_conta=CP.id)) AS saldo_a_pagar, 
					UPPER(U.nome) AS cadastrado_por_nome 
					FROM contas_pagar_pagtos AS CPP 
					INNER JOIN contas_pagar AS CP ON (CP.id=CPP.cod_conta) 
					INNER JOIN usuarios AS U ON (U.id=CP.cadastrado_por) 
					INNER JOIN empresas_sistema AS E ON (E.cod_empresa=CP.cod_empresa) 
					INNER JOIN fornecedores AS F ON (F.cod_fornecedor=CP.cod_fornecedor) 
					$where2 ORDER BY {$sort} {$order} 
					LIMIT {$start}, {$limit}
				";
			}
			else {
				$sql1 = "SELECT CP.id, 
					CP.descricao, 
					CP.codigo_barras, 
					CP.tipo_conta, 
					CP.documento, 
					CP.observacoes, 
					CP.data_cadastro, 
					CP.vencimento, 
					CP.valor_nf, 
					CP.valor_parcela, 
					CP.parcela_nro, 
					CP.nf_nro, 
					CP.quitado, 
					UPPER(E.nome_fantasia) AS nome_empresa, 
					F.tipo_fornecedor, 
					F.nome_completo, 
					F.razao_social, 
					F.nome_fantasia, 
					(CP.valor_parcela - (SELECT COALESCE(SUM(valor_pago), 0) FROM contas_pagar_pagtos WHERE cod_conta=CP.id)) AS saldo_a_pagar, 
					UPPER(U.nome) AS cadastrado_por_nome 
					FROM contas_pagar AS CP 
					INNER JOIN usuarios AS U ON (U.id=CP.cadastrado_por) 
					INNER JOIN empresas_sistema AS E ON (E.cod_empresa=CP.cod_empresa) 
					INNER JOIN fornecedores AS F ON (F.cod_fornecedor=CP.cod_fornecedor) 
					$where ORDER BY {$sort} {$order} 
					LIMIT {$start}, {$limit}
				";
			}

			$pdo = $connection->prepare($sql1);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_ASSOC);
			
			$data = array();
			foreach ($query as $key => $value) {
				$sql2 = "
					SELECT CPP.id, CPP.cod_conta, 
						UPPER(CPP.tipo_pagto) AS tipo_pagto,
						CPP.valor_pago, 
						DATE_FORMAT(CPP.data_pagto, '%d/%m/%Y') as data_pagto, 
						CPP.data_cadastro, 
						UPPER(CPP.observacao) AS observacao 
					FROM contas_pagar_pagtos AS CPP 
					WHERE CPP.cod_conta = {$value['id']}
				";

				$pdo2 = $connection->prepare($sql2);
				$pdo2->execute();
				$query2 = $pdo2->fetchAll(PDO::FETCH_OBJ);
				$body = "";
				foreach ($query2 as $key => $value2) {
					if($value2->tipo_pagto=='1'){
						$value2->tipo_pagto = 'DINHEIRO';
					}
					elseif($value2->tipo_pagto=='2'){
						$value2->tipo_pagto = 'CHEQUE(S)';
					}
					elseif($value2->tipo_pagto=='3'){
						$value2->tipo_pagto = 'DEPÓSITO/TRANSFERÊNCIA BANCÁRIA';
					}
					elseif($value2->tipo_pagto=='4'){
						$value2->tipo_pagto = 'BOLETO/DUPLICATA';
					}
					elseif($value2->tipo_pagto=='5'){
						$value2->tipo_pagto = 'PERMUTA';
					}
					elseif($value2->tipo_pagto=='6'){
						$value2->tipo_pagto = 'CONTA CORRENTE';
					}

					$value2->valor_pago = number_format($value2->valor_pago, 2);

					$body .= "
						<tr>
							<td width='25%'>{$value2->tipo_pagto}</td>
							<td width='25%'>{$value2->data_pagto}</td>
							<td width='25%'>{$value2->valor_pago}</td>
							<td width='25%'>{$value2->observacao}</td>
						</tr>
					";
				}

				$query[$key] = $value;
				if ($value['parcela_nro'] != "PARCELA 1 DE 1"){
					$value['descricao'] = $value['descricao']." (".$value['parcela_nro'].")";
				}

				if ($value['tipo_fornecedor'] == "F") {
					$value['nome_fornecedor'] = $value['nome_completo'];
				} 
				else {
					if ($value['nome_fantasia'] != "") {
						$value['nome_fornecedor'] = $value['razao_social']." (".$value['nome_fantasia'].")";
					} else {
						$value['nome_fornecedor'] = $value['razao_social'];
					}
				}

				if($body==""){
					$body = "<tr rows='4'>
								<td>Nenhum pagamento efetuado</td>
							</tr>";
				}

				$value['body'] = $body;
				$value['rows'] = $query2;

				$data[] = $value;
			}

			$result["total"] = $countRow;
			$result["dados"] = $data;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			$sql1 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql1);
			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			//echo json_encode($result);
			echo json_encode( array('total'=>$countRow, 'dados'=>$data, 'sql'=>$sql, 'sql1'=>$sql1, 'sql2'=>$sql2) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql, 'sql1'=>$sql1, 'sql2'=>$sql2));
	}	
}
?>