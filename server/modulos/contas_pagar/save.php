<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'contas_pagar';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		//$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			if ($_POST['vencimento'] == "__/__/____" OR $_POST['vencimento'] == "") {
				$vencimento = NULL;
			} else {
				$vencimento = implode('-', array_reverse(explode('/', $_POST['vencimento'])));
			}

			$sql1 ="SELECT COALESCE(SUM(valor_pago), 0) AS total_pago FROM contas_pagar_pagtos WHERE cod_conta='{$_POST['id']}'"; 
			$pdo1 = $connection->prepare($sql1);
			$pdo1->execute();
			$query = $pdo1->fetch(PDO::FETCH_OBJ);

			if($query->total_pago > $_POST['valor_parcela']){
				throw new PDOException("Não é possivel alterar o valor para: R$ ".number_format($_POST["valor_parcela"], 2, ',', '.').", pois já foi recebido: R$ ".number_format($query->total_pago, 2, ',', '.'));
			}

			$pdo2 = $connection->prepare("
				UPDATE contas_pagar SET 
				cod_empresa = ?, 
				cod_fornecedor = ?, 
				descricao = ?, 
				tipo_conta = ?, 
				documento = ?, 
				vencimento = ?, 
				valor_parcela = ?, 
				parcela_nro = ?, 
				codigo_barras = ?, 
				observacoes = ?, 
				data_alteracao = ?, 
				alterado_por = ? 
				WHERE id = ?
			");
			$params2 = array(
				$_POST['cod_empresa'], 
				$_POST['cod_fornecedor'], 
				mb_strtoupper($_POST['descricao'], 'UTF-8'), 
				mb_strtoupper($_POST['tipo_conta'], 'UTF-8'), 
				mb_strtoupper($_POST['documento'], 'UTF-8'), 
				$vencimento, 
				$_POST['valor_parcela'], 
				$_POST['parcela_nro'], 
				$_POST['codigo_barras'], 
				mb_strtoupper($_POST['observacoes'], 'UTF-8'), 
				date('Y-m-d H:i:s'), 
				$user_id, 
				$_POST['id']
			);
			$pdo2->execute($params2);
			//$connection->commit();

			if($query->total_pago == $_POST['valor_parcela']){
				$sql3 ="UPDATE contas_pagar SET quitado='S' WHERE id='$_POST[id]'"; 
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();
				//$connection->commit();
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			foreach ($_POST['data_parcela'] AS $key => $item) {
				$pdo4 = $connection->prepare("
				INSERT INTO contas_pagar 
					(
						cod_empresa, 
						cod_fornecedor, 
						descricao, 
						tipo_conta, 
						documento, 
						vencimento, 
						nf_nro, 
						valor_nf, 
						valor_parcela, 
						parcela_nro, 
						observacoes, 
						quitado, 
						codigo_barras, 
						data_cadastro, 
						data_alteracao, 
						cadastrado_por, 
						alterado_por
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
				");

				$j = count($_POST['data_parcela']);
				$i = $key + 1;
				$params4 = array(
					$_POST['cod_empresa'], 
					$_POST['cod_fornecedor'], 
					mb_strtoupper($_POST['descricao'], 'UTF-8'), 
					mb_strtoupper($_POST['tipo_conta'], 'UTF-8'), 
					mb_strtoupper($_POST['documento'][$key], 'UTF-8'), 
					implode('-', array_reverse(explode('/', $_POST['data_parcela'][$key]))), 
					$_POST['nf_nro'], 
					$_POST['valor_nf'], 
					$_POST['valor_parcela'][$key], 
					"PARCELA ".$i." DE ".$j, 
					mb_strtoupper($_POST['observacoes'], 'UTF-8'), 
					'N', 
					$_POST['codigo_barras'][$key], 
					date('Y-m-d H:i:s'), 
					date('Y-m-d H:i:s'), 
					$user_id, 
					$user_id
				);
				$pdo4->execute($params4);
				//$connection->commit();
			}
		}

		echo json_encode(array('success'=>true, 'msg'=>'Conta(s) À Pagar Salva com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>$e->getMessage()));
	}
}
?>