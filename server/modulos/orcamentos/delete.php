<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'orcamentos';
	$data_hora_atual = date('Y-m-d H:i:s');

	try {
		if($_POST['action'] == 'VERIFICAR'){
			$sql = "SELECT count(*) as total FROM orcamentos WHERE id_orcamento=:id AND usuario_salvou = 2 ";
			$pdo = $connection->prepare($sql);

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			if($query->total>0){
				$pdo = $connection->prepare("DELETE FROM orcamentos WHERE id_orcamento = ?");
				$pdo->execute(array(
					$_POST['id']
				));

				$pdo3 = $connection->prepare("DELETE FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id']}'");
				$pdo3->execute();

				echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
			}
			else{
				echo json_encode(array('success'=>true, 'msg'=>'Orçamento Salvo'));
			}
		}
		else if($_POST['action'] == 'DELETAR_VENDA'){
			$user->getAcao($tabela, 'deletar');

			$pdo = $connection->prepare("
				UPDATE orcamentos SET 
					cancelado='S', 
					cancelado_por='{$user_id}', 
					data_cancelado='{$data_hora_atual}', 
					motivo_cancelamento = ? 
				WHERE id_orcamento = ?");
			$pdo->execute(array(
				mb_strtoupper($_POST['motivo_cancelamento'], 'UTF-8'),
				$_POST['id_orcamento']
			));

			$pdo2 = $connection->prepare("UPDATE orcamentos SET convertido_venda='N' WHERE id_orcamento = ?");
			$pdo2->execute(array(
				$_POST['venda_cod_orcamento']
			));

			echo json_encode(array('success'=>true, 'msg'=>'CENCELADO COM SUCESSO'));
		}
		else if($_POST['action'] == 'DELETAR_ORCAMENTO'){
			$user->getAcao($tabela, 'deletar');

			$pdo = $connection->prepare("UPDATE orcamentos SET cancelado='S', cancelado_por='{$user_id}', data_cancelado='{$data_hora_atual}', motivo_cancelamento='É ORCAMENTO'  WHERE id_orcamento = ?");
			$pdo->execute(array(
				$_POST['id']
			));

			$pdo6 = $connection->prepare("DELETE FROM vendas_custos WHERE cod_venda='{$_POST['id']}'");
			$pdo6->execute();

			echo json_encode(array('success'=>true, 'msg'=>'CENCELADO COM SUCESSO'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}