<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
if($_GET){
	try {
		require('../../autoLoad.php');
		$userMain = $user->getUser();
		$user_id = null;
		if($userMain){
			$user_id = $userMain['id'];
		}
		@session_start();
		header("Content-Type: text/html; charset=utf-8");


		//pega dados da empresa
		$pdo = $connection->prepare("SELECT nome_fantasia, endereco, num_end, bairro, cidade, fone1, fone2, fone3, fone4 FROM empresas_sistema WHERE cod_empresa = '{$_COOKIE['id_empresa_atual']}'");
		$pdo->execute();
		$empresa = $pdo->fetch(PDO::FETCH_OBJ);
		$nome_fantasia = mb_strtoupper($empresa->nome_fantasia, 'UTF-8');
		$endereco_empresa = mb_strtoupper($empresa->endereco.', '.$empresa->num_end, 'UTF-8');
		$bairro_cidade_empresa = mb_strtoupper($empresa->bairro.' - '.$empresa->cidade, 'UTF-8');
		$fones_empresa = "";
		if (!empty($empresa->fone1)){
			$fones_empresa .= '(' . substr($empresa->fone1, 0, 2) . ') ' . substr($empresa->fone1, 2, 4) . '-' . substr($empresa->fone1, 6);
		}
		if (!empty($empresa->fone2)){
			$fones_empresa .= ' / (' . substr($empresa->fone2, 0, 2) . ') ' . substr($empresa->fone2, 2, 4) . '-' . substr($empresa->fone2, 6);
		}
		if (!empty($empresa->fone3)){
			$fones_empresa .= ' / (' . substr($empresa->fone3, 0, 2) . ') ' . substr($empresa->fone3, 2, 4) . '-' . substr($empresa->fone3, 6);
		}
		if (!empty($empresa->fone4)){
			$fones_empresa .= ' / (' . substr($empresa->fone4, 0, 2) . ') ' . substr($empresa->fone4, 2, 4) . '-' . substr($empresa->fone4, 6);
		}
		if (!empty($empresa->fone5)){
			$fones_empresa .= ' / (' . substr($empresa->fone5, 0, 2) . ') ' . substr($empresa->fone5, 2, 4) . '-' . substr($empresa->fone5, 6);
		}

		//pega dados do orçamento
		$sql1 = "SELECT O.id_orcamento, O.id_cliente, DATE_FORMAT(O.validade_orcamento, '%d/%m/%Y') AS validade_orcamento, 
					O.cliente_descricao, O.valor_bruto, O.valor_total, O.desconto_real, O.acrescimo_real, 
					DATE_FORMAT(O.data_cadastro, '%d/%m/%Y') AS data_cadastro, O.aos_cuidados_de, O.tipo_orcamento, 
					O.fone1, O.fone2, O.fone3, O.fone4, O.fone5, O.fone1_obs, O.fone2_obs, O.fone3_obs, O.fone4_obs, 
					O.fone5_obs, C.tipo_cliente, C.nome_completo, C.razao_social, C.nome_fantasia, 
					CL.loc_nome_abreviado, O.loteamento, O.eh_venda, O.cancelado, 
					CB.bairro_nome, CE.tipo, CE.nome, O.nro_end, O.estado, O.complemento, O.ponto_ref, U.nome AS nome_orcamentista, 
					(SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento) AS total_pago, 
					(SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) AS total_adiantado, 
					(O.valor_total - (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) - (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)) AS saldo_a_receber 
				FROM orcamentos AS O 
				LEFT JOIN clientes AS C ON (O.id_cliente=C.cod_cliente) 
				LEFT JOIN correios_localidades AS CL ON (O.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (O.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (O.endereco=CE.id) 
				INNER JOIN usuarios AS U ON (U.id=O.id_orcamentista)
				WHERE O.id_orcamento = '{$_GET['id_orcamento']}'";
		$pdo1 = $connection->prepare($sql1);
		$pdo1->execute();
		$orcamento = $pdo1->fetch(PDO::FETCH_OBJ);

		$nro_orcamento = $orcamento->id_orcamento;
		$validade_orcamento = $orcamento->validade_orcamento;
		$data_orcamento = $orcamento->data_cadastro;
		$valor_bruto_orcamento = number_format($orcamento->valor_bruto, 2, ',', '.');
		$valor_total_orcamento = number_format($orcamento->valor_total, 2, ',', '.');
		$desconto_real_orcamento = number_format($orcamento->desconto_real, 2, ',', '.');
		$acrescimo_real_orcamento = number_format($orcamento->acrescimo_real, 2, ',', '.');
		$nome_orcamentista = $orcamento->nome_orcamentista;

		$monta_nome = "";

		$endereco_refencia = $orcamento->ponto_ref;
		if ($orcamento->loteamento != ""){
			$endereco_refencia .= " (".$orcamento->loteamento.")";
		}

		$monta_endereco = $orcamento->tipo." ".$orcamento->nome.", ".$orcamento->nro_end;

		$monta_endereco2 = $orcamento->tipo." ".$orcamento->nome.", ".$orcamento->nro_end." - ".$orcamento->bairro_nome.", - ".$orcamento->loc_nome_abreviado;
		$monta_endereco2 = titleCase($monta_endereco2);
		$monta_endereco2 = $monta_endereco2." - ".$orcamento->estado;
		$monta_endereco2 = str_replace(' ', '+', $monta_endereco2);


		if ($orcamento->complemento != ""){
			$monta_endereco .= " (".$orcamento->complemento.")";
		}

		if ($orcamento->tipo_cliente == "F"){
			$monta_nome = $orcamento->nome_completo;
		}
		elseif ($orcamento->tipo_cliente == "J"){
			$monta_nome = $orcamento->razao_social;
			if (!empty($orcamento->nome_fantasia)){
				$monta_nome .= " (".$orcamento->nome_fantasia.")";
			}
		}
		else{
			$monta_nome = $orcamento->cliente_descricao;
		}

		if ($orcamento->aos_cuidados_de != "") {
			$monta_nome .= " (AOS CUIDADOS DE: ".$orcamento->aos_cuidados_de.")";
		}

		$monta_bairro_cidade = $orcamento->bairro_nome." / ".$orcamento->loc_nome_abreviado;


		$telefones_cliente = "";
		if (!empty($orcamento->fone1)){
			if (strlen($orcamento->fone1) == 10){
				$telefones_cliente .= '<a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone1.'" target="_new">('.substr($orcamento->fone1, 0, 2).') '.substr($orcamento->fone1, 2, 4).'-'.substr($orcamento->fone1, 6).'</a>';
			}
			else {
				$telefones_cliente .= '<a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone1.'" target="_new">('.substr($orcamento->fone1, 0, 2).') '.substr($orcamento->fone1, 2, 5).'-'.substr($orcamento->fone1, 7).'</a>';
			}

			if (!empty($orcamento->fone1_obs)){
				$telefones_cliente .= ' - ' .$orcamento->fone1_obs;
			}
		}

		if (!empty($orcamento->fone2)){
			if (strlen($orcamento->fone2) == 10){
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone2.'" target="_new">('.substr($orcamento->fone2, 0, 2).') '.substr($orcamento->fone2, 2, 4).'-'.substr($orcamento->fone2, 6).'</a>';
			}
			else {
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone2.'" target="_new">('.substr($orcamento->fone2, 0, 2).') '.substr($orcamento->fone2, 2, 5).'-'.substr($orcamento->fone2, 7).'</a>';
			}

			if (!empty($orcamento->fone2_obs)){
				$telefones_cliente .= ' - ' .$orcamento->fone2_obs;
			}
		}

		if (!empty($orcamento->fone3)){
			if (strlen($orcamento->fone3) == 10){
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone3.'" target="_new">('.substr($orcamento->fone3, 0, 2).') '.substr($orcamento->fone3, 2, 4).'-'.substr($orcamento->fone3, 6).'</a>';
			}
			else {
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone3.'" target="_new">('.substr($orcamento->fone3, 0, 2).') '.substr($orcamento->fone3, 2, 5).'-'.substr($orcamento->fone3, 7).'</a>';
			}

			if (!empty($orcamento->fone3_obs)){
				$telefones_cliente .= ' - ' .$orcamento->fone3_obs;
			}
		}

		if (!empty($orcamento->fone4)){
			if (strlen($orcamento->fone4) == 10){
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone4.'" target="_new">('.substr($orcamento->fone4, 0, 2).') '.substr($orcamento->fone4, 2, 4).'-'.substr($orcamento->fone4, 6).'</a>';
			}
			else {
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone4.'" target="_new">('.substr($orcamento->fone4, 0, 2).') '.substr($orcamento->fone4, 2, 5).'-'.substr($orcamento->fone4, 7).'</a>';
			}

			if (!empty($orcamento->fone4_obs)){
				$telefones_cliente .= ' - ' .$orcamento->fone4_obs;
			}
		}

		if (!empty($orcamento->fone5)){
			if (strlen($orcamento->fone5) == 10){
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone5.'" target="_new">('.substr($orcamento->fone5, 0, 2).') '.substr($orcamento->fone5, 2, 4).'-'.substr($orcamento->fone5, 6).'</a>';
			}
			else {
				$telefones_cliente .= ' | <a href="https://api.whatsapp.com/send?phone=55'.$orcamento->fone5.'" target="_new">('.substr($orcamento->fone5, 0, 2).') '.substr($orcamento->fone5, 2, 5).'-'.substr($orcamento->fone5, 7).'</a>';
			}

			if (!empty($orcamento->fone5_obs)){
				$telefones_cliente .= ' - ' .$orcamento->fone5_obs;
			}
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <head>
<style>
.redondo1 {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	background: #ffffff;
	border:solid black 2px;
}
.redondo2 {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	background: #ffffff;
	border:solid black 2px;
}
.redondo2 tr:nth-child(odd){
	background-color:#fff
}
.redondo2 tr:nth-child(even){
	background-color:#f1f1f1
}
a{
font-family:  Tahoma, Verdana, Arial;
font-size: 11px;
color: black;
text-decoration: none;
}
a:hover{
color: black;
text-decoration: underline;
}
a:visited {
  color: black;
}
a:active {
  color: black;
}

@page {
	size: A4;
	margin: 0;
}

@media print {
	.page {
		margin: 0;
		border: initial;
		border-radius: initial;
		width: initial;
		min-height: initial;
		box-shadow: initial;
		background: initial;
		page-break-after: always;
	}
}
</style>
</head>
<body>

<table width="770px" cellpadding="5" cellspacing="15">
	<tr>
		<td valign="top" colspan="3">
			<div style="font-family: Tahoma, Verdana, Arial; font-size: 14px;">
				<b><?php if ($orcamento->eh_venda == "S") { echo "PEDIDO DE VENDA";} else { echo "ORÇAMENTO"; } ?>: <?php echo $nro_orcamento; if ($orcamento->cancelado == 'S') { echo " - CANCELADO"; } ?></b>
			</div>
			<div style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">
				CLIENTE: <?php echo mb_strtoupper($monta_nome, 'UTF-8'); ?>&nbsp;<?php echo $telefones_cliente; ?>&nbsp;<br>
				ENDEREÇO: <a href="https://www.google.com/maps/place/<?php echo $monta_endereco2;?>" target="_new"><?php echo mb_strtoupper($monta_endereco, 'UTF-8'); ?>&nbsp;<?php echo mb_strtoupper($endereco_refencia, 'UTF-8'); ?>&nbsp;<?php echo mb_strtoupper($monta_bairro_cidade, 'UTF-8'); ?></a>&nbsp;
			</div>
		</td>
	</tr>
	<tr>
		<td valign="top" colspan="3">
			<table width="100%" cellpadding="2" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td bgcolor="#cccccc" width="40" align="center" valign="top" style="border-bottom:1px solid #111;border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>QTDE</b></div>
					</td>
					<td bgcolor="#cccccc" width="25" align="center" valign="top" style="border-bottom: 1px solid #111;">
						<div style="font-size: 11px;"><b>UN</b></div>
					</td>
					<td bgcolor="#cccccc" valign="top" align="left" style="border-bottom: 1px solid #111;border-left-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>DESCRIÇÃO</b></div>
					</td>
					<?php if ($_GET['tipo'] == "cliente" or $_GET['tipo'] == "detalhado") { ?>
					<td bgcolor="#cccccc" width="45" align="center" valign="top" style="border-bottom: 1px solid #111; border-left-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>VLR UN</b></div>
					</td>
					<td bgcolor="#cccccc" width="55" align="center" valign="top" style="border-bottom: 1px solid #111;border-left-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>TOTAL</b></div>
					</td>
					<?php } ?>
				</tr>
<?php
$item['observacoes'] = "";
$item['possui_ambiente'] = "";

//Tipo de Orçamento: 0=geral/tradicional, 1=forro, 2=persianas, 3=pisos, 4=rodapés
if ($orcamento->tipo_orcamento == 0) {
//geral --> Pega os ítens do orçamento
	$item['observacoes'] = "";
	$sql3 = "SELECT IO.item_nro, IO.quantidade, IO.valor_unitario, IO.valor_total, IO.observacoes, 
				IO.id_cor, P.descricao_impressao AS descricao_item, P.descricao_completa AS descricao_item2, 
				UM.sigla, C.descricao AS nome_cor 
			FROM itens_orcamento AS IO 
			INNER JOIN produtos AS P ON (IO.id_produto=P.id) 
			INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
			LEFT JOIN produtos_cores AS PC ON (IO.id_cor=PC.id_produtos_cores) 
			LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
			WHERE IO.id_orcamento = '{$_GET['id_orcamento']}' ORDER BY IO.item_nro ASC";
}
elseif ($orcamento->tipo_orcamento == 1) {
//forro --> Pega os ítens do orçamento
	$sql3 = "SELECT POF.id_produtos_orcamentos_forro, POF.id_produtos_orcamentos_forro_pai AS forro_pai, 
				POF.quantidade, POF.valor_unitario, POF.id_cor, P.descricao_impressao AS descricao_item, 
				POF.valor_total, POF.valor_bruto, POF.tipo_produto, POF.possui_ambiente, POF.item_nro, 
				P.descricao_completa AS descricao_item2, UM.sigla, C.descricao AS nome_cor 
			FROM produtos_orcamentos_forro AS POF 
			INNER JOIN produtos AS P ON (POF.id_produto=P.id) 
			INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
			LEFT JOIN produtos_cores AS PC ON (POF.id_cor=PC.id_produtos_cores) 
			LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
			WHERE POF.id_orcamento = '{$_GET['id_orcamento']}' ORDER BY POF.item_nro ASC";
}
elseif ($orcamento->tipo_orcamento == 3) {
//pisos --> Pega os ítens do orçamento
	$sql3 = "SELECT POP.id_produtos_orcamentos_piso, 
				POP.quantidade, POP.valor_unitario, POP.id_cor, 
				POP.valor_total, POP.valor_bruto, POP.tipo_produto, POP.item_nro, 
				POP.qtd_caixas, POP.total_area, POP.possui_ambiente, P.descricao_impressao AS descricao_item, 
				P.descricao_completa AS descricao_item2, UM.sigla, C.descricao AS nome_cor 
			FROM produtos_orcamentos_piso AS POP 
			INNER JOIN produtos AS P ON (POP.id_produto=P.id) 
			INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
			LEFT JOIN produtos_cores AS PC ON (POP.id_cor=PC.id_produtos_cores) 
			LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
			WHERE POP.id_orcamento = '{$_GET['id_orcamento']}' ORDER BY POP.item_nro ASC";
}
elseif ($orcamento->tipo_orcamento == 4) {
//rodape --> Pega os ítens do orçamento
	$sql3 = "SELECT POR.id_produtos_orcamentos_rodape, 
				POR.quantidade, POR.valor_unitario, POR.id_cor, 
				POR.valor_total, POR.valor_bruto, POR.tipo_produto, POR.item_nro, 
				POR.qtd_barras, POR.total_area, POR.possui_ambiente, P.descricao_impressao AS descricao_item, 
				P.descricao_completa AS descricao_item2, UM.sigla, C.descricao AS nome_cor 
			FROM produtos_orcamentos_rodape AS POR 
			INNER JOIN produtos AS P ON (POR.id_produto=P.id) 
			INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
			LEFT JOIN produtos_cores AS PC ON (POR.id_cor=PC.id_produtos_cores) 
			LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
			WHERE POR.id_orcamento = '{$_GET['id_orcamento']}' ORDER BY POR.item_nro ASC";
}
			$pdo3 = $connection->prepare($sql3);
			$pdo3->execute();
			//$query = $pdo2->fetchAll(PDO::FETCH_OBJ);

			while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
			{
				$descricao_item = mb_strtoupper($item['descricao_item'], 'UTF-8');
				if ($item['id_cor'] != "0") {
					$descricao_item .= ' <b>'.mb_strtoupper($item['nome_cor'], 'UTF-8').'</b>';
				}

				if ($item['observacoes'] != "") {
					$descricao_item .= '<br><font style="font-size: 10px;">'.mb_strtoupper($item['observacoes'], 'UTF-8').'</font>';
				}

				if ($item['sigla'] == "M²" OR $item['sigla'] == "M" OR $item['sigla'] == "KM" OR $item['sigla'] == "L") {
					$quantidade_item = number_format($item['quantidade'], 2, ',', '.');
				}
				elseif ($item['sigla'] == "BR" OR $item['sigla'] == "UN" OR $item['sigla'] == "PÇ" OR $item['sigla'] == "PC" OR $item['sigla'] == "PCTE") {
					$parteInteira = floor($item['quantidade']);
					$parteFracionada = $item['quantidade']-$parteInteira;
					//echo "Aqui: " .$item['quantidade']. " / Inteiro: " .$parteInteira. " / Fração: " .$parteFracionada. "<br>";
					if ($parteFracionada > 0) {
						$casasDecimais = (int)strlen($parteFracionada)-2;
						if ($casasDecimais == 1) {
							$casasDecimais = 2;
						}
						$quantidade_item = number_format($item['quantidade'], $casasDecimais, ',', '.');
					}
					else {
						$quantidade_item = number_format($item['quantidade'], 0);
					}
				}
				else {
					$quantidade_item = $item['quantidade'];
				}
?>
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td valign="top" align="center" style="font-size: 10px;border-bottom: 1px solid #ccc; border-right-style:dotted; border-width: 1px;"><?php echo $quantidade_item; ?></td>
					<td valign="top" align="center" style="font-size: 10px;border-bottom: 1px solid #ccc"><?php echo $item['sigla']; ?></td>		
					<td valign="top" align="left" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-width: 1px;"><?php echo $descricao_item; ?></td>
					<?php if ($_GET['tipo'] == "cliente" or $_GET['tipo'] == "detalhado") { ?>
					<td valign="top" align="right" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-width: 1px;"><?php echo number_format($item['valor_unitario'], 2, ',', '.'); ?></td>
					<td valign="top" align="right" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-width: 1px;"><?php echo number_format($item['valor_total'], 2, ',', '.'); ?></td>
					<?php } ?>
				</tr>
<?php
			}
?>
			</table>
		</td>
	</tr>
<?php
//Pega os ambientes
//forro
if ($orcamento->tipo_orcamento == 1) {
	$sql5 = "SELECT count(*) AS total FROM produtos_orcamentos_forro WHERE id_orcamento = '{$_GET['id_orcamento']}' AND possui_ambiente = 'S' ORDER BY item_nro ASC";
	$pdo5 = $connection->prepare($sql5);
	$pdo5->execute();
	$query = $pdo5->fetch(PDO::FETCH_OBJ);
	$countRow = $query->total;

	if ($countRow > 0) {
?>
	<tr>
		<td valign="top" colspan="3">
			<table width="100%" cellpadding="2" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td bgcolor="#cccccc" valign="top" align="left" style="border-bottom: 1px solid #111; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>AMBIENTE</b></div>
					</td>
					<td bgcolor="#cccccc" width="100" align="right" valign="top" style="border-bottom: 1px solid #111; border-width: 1px;">
						<div style="font-size: 11px;"><b>QTDE FOLHAS</b></div>
					</td>
					<?php if ($_GET['tipo'] == "cliente" or $_GET['tipo'] == "detalhado") { ?>
					<td bgcolor="#cccccc" width="60" align="center" valign="top" style="border-bottom: 1px solid #111;border-left-style:dotted; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>COR</b></div>
					</td>
					<td bgcolor="#cccccc" width="90" align="center" valign="top" style="border-bottom: 1px solid #111; border-width: 1px;">
						<div style="font-size: 11px;"><b>ACESSÓRIO</b></div>
					</td>
					<?php } ?>
					<td bgcolor="#cccccc" width="350" align="left" valign="top" style="border-bottom: 1px solid #111;border-left-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>OBSERVAÇÕES</b></div>
					</td>
				</tr>
<?php
		$sql5 = "SELECT id_produtos_orcamentos_forro, item_nro, id_cor FROM produtos_orcamentos_forro 
			WHERE id_orcamento = '{$_GET['id_orcamento']}' AND possui_ambiente = 'S' ORDER BY item_nro ASC";
		$pdo5 = $connection->prepare($sql5);
		$pdo5->execute();

		$nro_id_produto_orcamento_forro_agrupado = "";
		while ($query5 = $pdo5->fetch(PDO::FETCH_ASSOC))
		{
			$item_nro = $query5['item_nro'];
			$nro_id_produto_orcamento_forro_agrupado .= $query5['id_produtos_orcamentos_forro']."|";

			$sql6 = "SELECT AOF.id_ambientes_orcamentos_forro, AOF.ambiente AS descricao, AOF.descricao_pecas, AOF.comprimento, 
					AOF.comprimento_real, AOF.largura, AOF.area, AOF.id_ambiente_pai, AOF.sn_l, AOF.tipo_acessorio, AOF.observacoes, C.descricao AS nome_cor
				FROM ambientes_orcamentos_forro AS AOF 
				LEFT JOIN produtos_cores AS PC ON (PC.id_produtos_cores='{$query5['id_cor']}') 
				LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
				WHERE AOF.id_produtos_orcamentos_forro = '{$query5['id_produtos_orcamentos_forro']}' 
				ORDER BY AOF.id_ambientes_orcamentos_forro ASC";
			$pdo6 = $connection->prepare($sql6);
			$pdo6->execute();

			while ($ambientes = $pdo6->fetch(PDO::FETCH_ASSOC))
			{
				$nome_ambiente = mb_strtoupper($ambientes['descricao'], 'UTF-8');
				$nome_ambiente .= " <font style=\"font-family: Tahoma, Verdana, Arial; font-size: 8px;\">(".$ambientes['comprimento']." X ".$ambientes['largura'].")</font>";
				$descricao_pecas = str_replace(".",",", $ambientes['descricao_pecas']);


				if ($ambientes['tipo_acessorio'] == "MC"){
					$nome_acessorio = "MEIA CANA";
				}
				elseif ($ambientes['tipo_acessorio'] == "RF"){
					$nome_acessorio = "RODA-FORRO";
				}
				elseif ($ambientes['tipo_acessorio'] == "EH"){
					$nome_acessorio = "EMENDA H";
				}
				elseif ($ambientes['tipo_acessorio'] == "EHF"){
					$nome_acessorio = "EMENDA H FLEXÍVEL";
				}
				elseif ($ambientes['tipo_acessorio'] == "SA"){
					$nome_acessorio = "SEM ACESSÓRIO";
				}
				else {
					$nome_acessorio = "...";
				}
?>
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td valign="top" align="left" style="font-size: 10px;border-bottom: 1px solid #ccc; border-right-style:dotted; border-width: 1px;"><?php echo $nome_ambiente; ?></td>
					<td valign="top" align="right" style="font-size: 10px;border-bottom: 1px solid #ccc"><?php echo $descricao_pecas; ?> <?php if ($_GET['tipo'] == "deposito" AND $ambientes['comprimento_real'] != $ambientes['comprimento']) { echo "<font style=\"font-family: Tahoma, Verdana, Arial; font-size: 9px;\">(".$ambientes['comprimento_real'].")</font>";}?> </td>
					<?php if ($_GET['tipo'] == "cliente" or $_GET['tipo'] == "detalhado") { ?>
						<td valign="top" nowrap align="center" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><?php echo mb_strtoupper($ambientes['nome_cor'], 'UTF-8'); ?></td>
						<td valign="top" align="center" style="font-size: 10px;border-bottom: 1px solid #ccc"><?php echo $nome_acessorio; ?></td>
					<?php } ?>
					<td valign="top" align="left" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-width: 1px;"><?php echo mb_strtoupper($ambientes['observacoes'], 'UTF-8'); ?></td>
				</tr>
<?php
			}
		}
?>
			</table>
		</td>
	</tr>
<?php
	}
}

//pisos
if ($orcamento->tipo_orcamento == 3) {
	$sql5 = "SELECT count(*) AS total FROM produtos_orcamentos_piso WHERE id_orcamento = '{$_GET['id_orcamento']}' AND possui_ambiente = 'S' ORDER BY item_nro ASC";
	$pdo5 = $connection->prepare($sql5);
	$pdo5->execute();
	$query = $pdo5->fetch(PDO::FETCH_OBJ);
	$countRow = $query->total;

	if ($countRow > 0) {
?>
	<tr>
		<td valign="top" colspan="3">
			<table width="100%" class="redondo2" cellpadding="5" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td bgcolor="#999999" width="30" align="center" valign="top" style="border-bottom: 1px solid #111; border-width: 1px;">
						<div style="font-size: 11px;"><b>ITEM</b></div>
					</td>
					<td bgcolor="#999999" valign="top" align="center" style="border-bottom: 1px solid #111; border-left-style:dotted; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>AMBIENTE</b></div>
					</td>
					<td bgcolor="#999999" width="60" align="center" valign="top" style="border-bottom: 1px solid #111;">
						<div style="font-size: 11px;"><b>COR</b></div>
					</td>
					<td bgcolor="#999999" width="90" align="center" valign="top" style="border-bottom: 1px solid #111; border-left-style:dotted; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>COMPRIMENTO</b></div>
					</td>
					<td bgcolor="#999999" width="60" align="center" valign="top" style="border-bottom: 1px solid #111; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>LARGURA</b></div>
					</td>
					<td bgcolor="#999999" width="60" align="center" valign="top" style="border-bottom: 1px solid #111;">
						<div style="font-size: 11px;"><b>ÁREA</b></div>
					</td>
					<td bgcolor="#999999" width="200" align="center" valign="top" style="border-bottom: 1px solid #111; border-left-style:dotted; border-width: 1px;">
						<div style="font-size: 11px;"><b>OBSERVAÇÕES</b></div>
					</td>
				</tr>

<?php
		$sql5 = "SELECT id_produtos_orcamentos_piso, item_nro, id_cor FROM produtos_orcamentos_piso 
			WHERE id_orcamento = '{$_GET['id_orcamento']}' AND possui_ambiente = 'S' ORDER BY item_nro ASC";
		$pdo5 = $connection->prepare($sql5);
		$pdo5->execute();

		while ($query5 = $pdo5->fetch(PDO::FETCH_ASSOC))
		{
			$item_nro = $query5['item_nro'];

			$sql6 = "SELECT AOP.id_ambientes_orcamentos_piso, AOP.ambiente AS descricao, AOP.comprimento, 
					AOP.largura, AOP.area, AOP.observacoes, C.descricao AS nome_cor
				FROM ambientes_orcamentos_piso AS AOP 
				LEFT JOIN produtos_cores AS PC ON (PC.id_produtos_cores='{$query5['id_cor']}') 
				LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
				WHERE AOP.id_produtos_orcamentos_piso = '{$query5['id_produtos_orcamentos_piso']}' 
				ORDER BY AOP.id_ambientes_orcamentos_piso ASC";
			$pdo6 = $connection->prepare($sql6);
			$pdo6->execute();

			while ($ambientes = $pdo6->fetch(PDO::FETCH_ASSOC))
			{
				$nome_ambiente = mb_strtoupper($ambientes['descricao'], 'UTF-8');
?>
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td valign="top" align="center" style="font-size: 11px;border-bottom: 1px solid #ccc"><?php echo $item_nro; ?></td>
					<td valign="top" align="left" style="font-size: 11px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><?php echo $nome_ambiente; ?></td>
					<td valign="top" nowrap align="center" style="font-size: 11px;border-bottom: 1px solid #ccc;"><?php echo mb_strtoupper($ambientes['nome_cor'], 'UTF-8'); ?></td>
					<td valign="top" align="center" style="font-size: 11px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><?php echo number_format($ambientes['comprimento'], 2, ',', '.'); ?></td>
					<td valign="top" align="center" style="font-size: 11px;border-bottom: 1px solid #ccc; border-right-style:dotted; border-width: 1px;"><?php echo number_format($ambientes['largura'], 2, ',', '.'); ?></td>
					<td valign="top" align="center" style="font-size: 11px;border-bottom: 1px solid #ccc;"><?php echo number_format($ambientes['area'], 2, ',', '.'); ?></td>
					<td valign="top" align="left" style="font-size: 11px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-width: 1px;"><?php echo mb_strtoupper($ambientes['observacoes'], 'UTF-8'); ?></td>
				</tr>
<?php


			}
		}
?>
			</table>
		</td>
	</tr>
<?php
	}
}

//rodape
if ($orcamento->tipo_orcamento == 4) {
	$sql5 = "SELECT count(*) AS total FROM produtos_orcamentos_rodape WHERE id_orcamento = '{$_GET['id_orcamento']}' AND possui_ambiente = 'S' ORDER BY item_nro ASC";
	$pdo5 = $connection->prepare($sql5);
	$pdo5->execute();
	$query = $pdo5->fetch(PDO::FETCH_OBJ);
	$countRow = $query->total;

	if ($countRow > 0) {
?>
	<tr>
		<td valign="top" colspan="3">
			<table width="100%" cellpadding="5" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td bgcolor="#cccccc" width="25" align="center" valign="top" style="border-bottom: 1px solid #111; border-width: 1px;">
						<div style="font-size: 10px;"><b>ITEM</b></div>
					</td>
					<td bgcolor="#cccccc" valign="top" align="center" style="border-bottom: 1px solid #111; border-left-style:dotted; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 10px;"><b>AMBIENTE</b></div>
					</td>
					<td bgcolor="#cccccc" width="50" align="center" valign="top" style="border-bottom: 1px solid #111; border-width: 1px;">
						<div style="font-size: 10px;"><b>PAREDES</b></div>
					</td>
					<td bgcolor="#cccccc" width="60" align="center" valign="top" style="border-bottom: 1px solid #111; border-left-style:dotted; border-right-style:dotted; border-width: 1px;">
						<div style="font-size: 10px;"><b>COR</b></div>
					</td>

					<td bgcolor="#cccccc" width="180" align="center" valign="top" style="border-bottom: 1px solid #111; border-width: 1px;">
						<div style="font-size: 10px;"><b>OBSERVAÇÕES</b></div>
					</td>
				</tr>

<?php
		$sql5 = "SELECT id_produtos_orcamentos_rodape, item_nro, id_cor FROM produtos_orcamentos_rodape 
			WHERE id_orcamento = '{$_GET['id_orcamento']}' AND possui_ambiente = 'S' ORDER BY item_nro ASC";
		$pdo5 = $connection->prepare($sql5);
		$pdo5->execute();

		while ($query5 = $pdo5->fetch(PDO::FETCH_ASSOC))
		{
			$item_nro = $query5['item_nro'];

			$sql6 = "SELECT AOR.id_ambientes_orcamentos_rodape, AOR.ambiente AS descricao, AOR.qtd_paredes, 
					AOR.comprimento_1, AOR.comprimento_2, AOR.comprimento_3, AOR.comprimento_4, AOR.comprimento_5, 
					AOR.comprimento_6, AOR.comprimento_7, AOR.comprimento_8, AOR.comprimento_9, AOR.comprimento_10, 
					AOR.comprimento_11, AOR.comprimento_12, AOR.comprimento_13, AOR.comprimento_14, AOR.comprimento_15, 
					AOR.comprimento_16, AOR.comprimento_17, AOR.comprimento_18, AOR.comprimento_19, AOR.comprimento_20, 
					AOR.comprimento_21, 
					AOR.area, AOR.observacoes, C.descricao AS nome_cor
				FROM ambientes_orcamentos_rodape AS AOR 
				LEFT JOIN produtos_cores AS PC ON (PC.id_produtos_cores='{$query5['id_cor']}') 
				LEFT JOIN cores AS C ON (PC.id_cor=C.id_cores) 
				WHERE AOR.id_produtos_orcamentos_rodape = '{$query5['id_produtos_orcamentos_rodape']}' 
				ORDER BY AOR.id_ambientes_orcamentos_rodape ASC";
			$pdo6 = $connection->prepare($sql6);
			$pdo6->execute();
			

			while ($ambientes = $pdo6->fetch(PDO::FETCH_ASSOC))
			{
				$nome_ambiente = mb_strtoupper($ambientes['descricao'], 'UTF-8');
				$medidas = str_replace(".",",", $ambientes['comprimento_1']);

				if ($ambientes['comprimento_2'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_2']);
				}
				if ($ambientes['comprimento_3'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_3']);
				}
				if ($ambientes['comprimento_4'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_4']);
				}
				if ($ambientes['comprimento_5'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_5']);
				}
				if ($ambientes['comprimento_6'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_6']);
				}
				if ($ambientes['comprimento_7'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_7']);
				}
				if ($ambientes['comprimento_8'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_8']);
				}
				if ($ambientes['comprimento_9'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_9']);
				}
				if ($ambientes['comprimento_10'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_10']);
				}
				if ($ambientes['comprimento_11'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_11']);
				}
				if ($ambientes['comprimento_12'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_12']);
				}
				if ($ambientes['comprimento_13'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_13']);
				}
				if ($ambientes['comprimento_14'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_14']);
				}
				if ($ambientes['comprimento_15'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_15']);
				}
				if ($ambientes['comprimento_16'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_16']);
				}
				if ($ambientes['comprimento_17'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_17']);
				}
				if ($ambientes['comprimento_18'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_18']);
				}
				if ($ambientes['comprimento_19'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_19']);
				}
				if ($ambientes['comprimento_20'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_20']);
				}
				if ($ambientes['comprimento_20'] != "0.00"){
					$medidas .= " + ".str_replace(".",",", $ambientes['comprimento_21']);
				}
?>
				<tr style="font-family: Tahoma, Verdana, Arial;">
					<td valign="top" align="center" style="font-size: 10px;border-bottom: 1px solid #ccc"><?php echo $item_nro; ?></td>
					<?php if ($_GET['tipo'] == "deposito") { ?>
						<td valign="top" align="left" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><div><font style="font-weight:bold;font-size: 14px;"><?php echo $nome_ambiente; ?></font></div><font style="font-size: 11px;"><?php echo $medidas; ?></font></td>
						<td valign="top" align="center" style="font-size: 10px;border-bottom: 1px solid #ccc"><font style="font-weight:bold;font-size: 14px;"><?php echo $ambientes['qtd_paredes']; ?></font></td>
						<td valign="top" nowrap align="center" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><font style="font-weight:bold;font-size: 14px;"><?php echo mb_strtoupper($ambientes['nome_cor'], 'UTF-8'); ?></font></td>
					<?php } else { ?>
						<td valign="top" align="left" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><div><?php echo $nome_ambiente; ?></div><font style="font-size: 9px;"><?php echo $medidas; ?></font></td>
						<td valign="top" align="center" style="font-size: 10px;border-bottom: 1px solid #ccc"><?php echo $ambientes['qtd_paredes']; ?></td>
						<td valign="top" nowrap align="center" style="font-size: 10px;border-bottom: 1px solid #ccc; border-left-style:dotted; border-right-style:dotted; border-width: 1px;"><?php echo mb_strtoupper($ambientes['nome_cor'], 'UTF-8'); ?></td>
					<?php }?>
					<td valign="top" align="left" style="font-size: 10px;border-bottom: 1px solid #ccc; border-width: 1px;"><?php echo mb_strtoupper($ambientes['observacoes'], 'UTF-8'); ?></td>
				</tr>
<?php


			}
		}
?>
			</table>
		</td>
	</tr>
<?php
	}
}
?>


	<tr>
		<td valign="top">
			<table width="100%" cellpadding="0" cellspacing="0">
			<?php if ($_GET['tipo'] == "deposito" or $_GET['tipo'] == "detalhado") { ?>
				<tr>
					<td valign="top" colspan="3">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">
								<td valign="top" width="100%"><b>OBSERVAÇÕES PARA DEPÓSITO:</b><br>
								<?php
								$sql4 = "SELECT count(*) AS total FROM orcamentos_observacoes WHERE orcamento_id = '{$_GET['id_orcamento']}' AND (tipo = 'D' OR tipo = 'A')";
								$pdo4 = $connection->prepare($sql4);
								$pdo4->execute();
								$query = $pdo4->fetch(PDO::FETCH_OBJ);
								$countRow = $query->total;

								$sql4 = "SELECT nro_item, descricao	FROM orcamentos_observacoes WHERE orcamento_id = '{$_GET['id_orcamento']}' AND (tipo = 'D' OR tipo = 'A') ORDER BY nro_item ASC";
								$pdo4 = $connection->prepare($sql4);
								$pdo4->execute();

								$i = 1;
								$observacoes_deposito = "";
								while ($obs = $pdo4->fetch(PDO::FETCH_ASSOC))
								{
									$obs['descricao'] = stripslashes(str_replace("\n", "<br />", $obs['descricao']));
									//$obs['descricao'] = str_replace("  "," &nbsp;", $obs['descricao']);
									$obs['descricao'] = str_replace("<b>","", $obs['descricao']);
									$obs['descricao'] = str_replace("</b>","", $obs['descricao']);


									if ($obs['nro_item'] == 0) {
										//$observacoes_deposito .= mb_strtoupper($obs['descricao'], 'UTF-8');
										$observacoes_deposito .= $obs['descricao'];
									}
									else {
										//$observacoes_deposito .= "ITEM ".$obs['nro_item'].": ".mb_strtoupper($obs['descricao'], 'UTF-8');
										$observacoes_deposito .= "ITEM ".$obs['nro_item'].": ".$obs['descricao'];
									}


									if ($i != $countRow){
										$observacoes_deposito .= "<br>";
									}
									$i++;
								}

								echo $observacoes_deposito; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>

<?php
	if ($orcamento->tipo_orcamento == 1) {
?>
				<tr>
					<td valign="top" colspan="3"><br>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">
								<td valign="top" width="100%"><b>MEDIDAS PARA CORTE:</b><br>

									<?php
									$monta_where_pecas_agrupadas = "(";
									$total_itens_orcamento = explode('|', $nro_id_produto_orcamento_forro_agrupado);
									$total_de_itens_forro = count($total_itens_orcamento);
									$total_de_itens_forro = $total_de_itens_forro-1;
									$i = 0;

									foreach($total_itens_orcamento as $key) {
										if ($i == 0) {
											$monta_where_pecas_agrupadas .= "id_produtos_orcamentos_forro = '".$key."'";
										}
										else {
											if ($i != $total_de_itens_forro) {
												$monta_where_pecas_agrupadas .= " OR id_produtos_orcamentos_forro = '".$key."'";
											}
										}
										$i++;
									}

									$monta_where_pecas_agrupadas .= ")";

									$sql44 = "SELECT SUM(qtd_pecas) AS total_folhas, comprimento FROM ambientes_orcamentos_forro 
										WHERE {$monta_where_pecas_agrupadas} 
										GROUP BY comprimento ORDER BY comprimento DESC";
									$pdo44 = $connection->prepare($sql44);
									$pdo44->execute();

									$i2 = 1;
									$i3 = 1;
									$texto_qtde_folhas = "
										<table width=\"100%\" cellpadding=\"3\" border=\"0\" cellspacing=\"0\">
											<tr style=\"font-family: Tahoma, Verdana, Arial; font-size: 11px;\">
									";

									while ($query44 = $pdo44->fetch(PDO::FETCH_ASSOC))
									{
										$texto_qtde_folhas .= "<td valign=\"top\" align=\"lef\">".$query44['total_folhas']." - ".number_format($query44['comprimento'], 2, ',', '.')."</td>";

										if ($i2 == $pdo44->rowCount()) {
											$texto_qtde_folhas .= "</tr>";
										}
										else {
											if ($i3 == 5) {
												$texto_qtde_folhas .= "</tr>";
												if ($i2 != $pdo44->rowCount()) {
													$texto_qtde_folhas .= "<tr style=\"font-family: Tahoma, Verdana, Arial; font-size: 11px;\">";
												}
												$i3 = 0;
											}
										}
										$i2++;
										$i3++;
									}

									$texto_qtde_folhas .= "</table>";

									echo $texto_qtde_folhas; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
<?php
	}
}
?>
			</table>
		</td>
		<td style="width:1px"></td>
		<td width="230" valign="top">
			<?php if ($orcamento->eh_venda == "N") { ?>
			<table width="100%" cellpadding="5" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">
					<td valign="top" width="100%" align="left">
					<?php //if ($_GET['tipo'] == "cliente") { ?>
						<div style="height: 17px;">TOTAL DOS ÍTENS:</div>
						<div style="height: 17px;">ACRÉSCIMOS:</div>
						<div style="height: 17px;">DESCONTOS:</div>
					<?php //} ?>
					
						<div style="height: 17px;"><b><?php if ($orcamento->eh_venda == "S") { echo "TOTAL DA VENDA"; } else { echo "TOTAL ORÇAMENTO"; } ?>:</b></div>
					
					</td>
					<td valign="top" width="100%" align="right">
					<?php //if ($_GET['tipo'] == "cliente") { ?>
						<div style="height: 17px;"><?php echo $valor_bruto_orcamento; ?></div>
						<div style="height: 17px;"><?php echo $acrescimo_real_orcamento; ?></div>
						<div style="height: 17px;"><?php echo $desconto_real_orcamento; ?></div>
					<?php //} ?>
						<div style="height: 17px;"><b><?php echo $valor_total_orcamento; ?></b></div>
					</td>
				</tr>
			</table>
			<?php } ?>
			<?php if ($orcamento->eh_venda == "S") { ?>
			<table width="100%" cellpadding="5" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">
					<td valign="top" width="100%" align="left">
						<?php //if ($_GET['tipo'] == "cliente") { ?>
							<div style="height: 17px;">TOTAL PEDIDO:</div>
							<div style="height: 17px;">TOTAL ADIANTADO:</div>
							<div style="height: 17px;">TOTAL PAGO:</div>
						<?php //} ?>
						<div style="height: 17px;"><b>SALDO À RECEBER:</b></div>
					</td>
					<td valign="top" width="100%" align="right">
						<?php //if ($_GET['tipo'] == "cliente") { ?>
							<div style="height: 17px;"><?php echo $valor_total_orcamento; ?></div>
							<div style="height: 17px;"><?php echo number_format($orcamento->total_adiantado, 2, ',', '.'); ?></div>
							<div style="height: 17px;"><?php echo number_format($orcamento->total_pago, 2, ',', '.'); ?></div>
						<?php //} ?>
						<div style="height: 17px;"><b><?php echo number_format($orcamento->saldo_a_receber, 2, ',', '.'); ?></b></div>
					</td>
				</tr>
			</table>
			<?php } ?>
		</td>
	</tr>
	<?php if ($orcamento->eh_venda == "N") { ?>
	<tr>
		<td valign="top">
			<div style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">ORÇAMENTISTA: <?php echo strtoupper($nome_orcamentista); ?></div> 
		</td>
	</tr>
	<?php } 
	if ($_GET['tipo'] == "detalhado") { ?>
	<tr>
		<td valign="top" colspan="3"><br><br>

			<table width="100%" class="redondo1" cellpadding="5" cellspacing="0">
				<tr style="font-family: Tahoma, Verdana, Arial; font-size: 11px;">
					<td valign="top" width="90"><b>PAGAMENTOS/ADIANTAMENTOS REALIZADOS:</b><br>
						<table width="100%" cellpadding="5" cellspacing="0" border="0">
<?php
					$sql6 = "SELECT DATE_FORMAT(VP.data_cadastro, '%d/%m/%Y %H:%i:%s') AS data_cadastro, VP.valor_pago, FP.descricao FROM vendas_pagamentos AS VP INNER JOIN formas_pagto AS FP ON (FP.id=VP.forma_pagto) WHERE VP.cod_venda='{$_GET['id_orcamento']}' AND VP.estornado='N' UNION ALL SELECT DATE_FORMAT(AU.data_vinculo, '%d/%m/%Y'), AU.valor_vinculado, FP2.descricao FROM adiantamentos_uso AS AU INNER JOIN adiantamentos AS A ON (A.id=AU.cod_adiantamento) INNER JOIN formas_pagto AS FP2 ON (FP2.id=A.forma_pagto) WHERE AU.cod_venda='{$_GET['id_orcamento']}' AND AU.cancelado='N' ORDER BY data_cadastro ASC";
					$pdo6 = $connection->prepare($sql6);
					$pdo6->execute();

					while ($pgto = $pdo6->fetch(PDO::FETCH_ASSOC))
					{
?>
							<tr>
								<td align="left"><?php echo $pgto['data_cadastro'] ?></td>
								<td width="70" align="left"><?php echo number_format($pgto['valor_pago'], 2, ',', '.'); ?></td>
								<td width="500" align="left"><?php echo $pgto['descricao'] ?></td>
							</tr>
<?php
					}
?>
						</table>
					</td>
				</tr>
			</table>

		</td>
	</tr>
	<?php } ?>
</table>


</body>
</html>
<?php
	}
	catch (PDOException $e) {
		echo $e->getMessage();
	}	
}
?>