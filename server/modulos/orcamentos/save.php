<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'orcamentos';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
		$user_nome = $userMain['login'];
		$texto = "Orçamento de: ".$user_nome." em ".date('d/m/Y')." ás ".date('H:i:s');
	}
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if(empty($_POST['id_os'])) {
			$id_os = NULL;
		}
		else {
			$id_os = $_POST['id_os'];
		}

		if(empty($_POST['desconto_porcento'])) {
			$desconto_porcento = 0;
		} else {
			$desconto_porcento = implode('.', explode(',', $_POST['desconto_porcento']));
		}

		if(empty($_POST['acrescimo_porcento'])) {
			$acrescimo_porcento = 0;
		} else {
			$acrescimo_porcento = implode('.', explode(',', $_POST['acrescimo_porcento']));
		}

		if ($_POST['cep'] == "__.___-___") {
			$cep = NULL;
		} else {
			$cep = $_POST['cep'];
		}

		if (empty($_POST['estado'])) {
			$estado = NULL;
		} else {
			$estado = $_POST['estado'];
		}

		if (empty($_POST['bairro'])) {
			$bairro = NULL;
		} else {
			$bairro = $_POST['bairro'];
		}

		if (empty($_POST['cliente_descricao']) AND $_POST['aos_cuidados_de'] != ""){
			$_POST['cliente_descricao'] = $_POST['aos_cuidados_de'];
		}

		if ($_POST['fone1'] == "(__) _____-____") {
			$fone1 = NULL;
		} else {
			$fone1 = $_POST['fone1'];
		}

		if ($_POST['fone2'] == "(__) _____-____") {
			$fone2 = NULL;
		} else {
			$fone2 = $_POST['fone2'];
		}

		if ($_POST['fone3'] == "(__) _____-____") {
			$fone3 = NULL;
		} else {
			$fone3 = $_POST['fone3'];
		}

		if ($_POST['fone4'] == "(__) _____-____") {
			$fone4 = NULL;
		} else {
			$fone4 = $_POST['fone4'];
		}

		if ($_POST['fone5'] == "(__) _____-____") {
			$fone5 = NULL;
		} else {
			$fone5 = $_POST['fone5'];
		}

		if (empty($_POST['fone1_obs'])) {
			$fone1_obs = NULL;
		}
		else {
			$fone1_obs = mb_strtoupper($_POST['fone1_obs'], 'UTF-8');
		}

		if (empty($_POST['fone2_obs'])) {
			$fone2_obs = NULL;
		}
		else {
			$fone2_obs = mb_strtoupper($_POST['fone2_obs'], 'UTF-8');
		}

		if (empty($_POST['fone3_obs'])) {
			$fone3_obs = NULL;
		}
		else {
			$fone3_obs = mb_strtoupper($_POST['fone3_obs'], 'UTF-8');
		}

		if (empty($_POST['fone4_obs'])) {
			$fone4_obs = NULL;
		}
		else {
			$fone4_obs = mb_strtoupper($_POST['fone4_obs'], 'UTF-8');
		}

		if (empty($_POST['fone5_obs'])) {
			$fone5_obs = NULL;
		}
		else {
			$fone5_obs = mb_strtoupper($_POST['fone5_obs'], 'UTF-8');
		}

		if (empty($_POST['data_venda'])) {
			$data_venda = NULL;
		}

		if ($_POST['eh_venda'] == "S") {
			if (empty($_POST['data_venda'])) {
				$data_venda = $data_hora_atual;
			} else {
				$data_venda = $_POST['data_venda'];
			}
		}

		$observacoes = trim(mb_strtoupper($_POST['observacoes'], 'UTF-8'));

		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE orcamentos SET 
							usuario_salvou = '1', 
							tipo_orcamento = '0', 
							validade_orcamento = ?, 
							id_cliente = ?, 
							id_os = ?, 
							id_orcamentista = ?,
							cliente_descricao = ?, 
							desconto_porcento = ?, 
							desconto_real = ?, 
							acrescimo_porcento = ?, 
							acrescimo_real = ?, 
							valor_bruto = ?, 
							valor_total = ?, 
							aos_cuidados_de = ?, 
							observacoes = ?, 
							alterado_por = ?, 
							data_alteracao = ?, 
							estado = ?, 
							cidade = ?, 
							bairro = ?, 
							endereco = ?, 
							nro_end = ?, 
							loteamento = ?, 
							complemento = ?, 
							ponto_ref = ?, 
							cep = ?, 
							data_venda = ?, 
							eh_venda = ?, 
							fone1 = ?, 
							fone2 = ?, 
							fone3 = ?, 
							fone4 = ?, 
							fone5 = ?, 
							fone1_obs = ?, 
							fone2_obs = ?, 
							fone3_obs = ?, 
							fone4_obs = ?, 
							fone5_obs = ? 
 					WHERE id_orcamento = ?
			");
			$params = array(
				implode('-', array_reverse(explode('/', $_POST['validade_orcamento']))),
				$_POST['id_cliente'],
				$id_os,
				$_POST['id_orcamentista'],
				mb_strtoupper($_POST['cliente_descricao'], 'UTF-8'),
				$desconto_porcento,
				implode('.', explode(',', $_POST['desconto_real'])),
				$acrescimo_porcento,
				implode('.', explode(',', $_POST['acrescimo_real'])),
				implode('.', explode(',', $_POST['valor_bruto'])),
				implode('.', explode(',', $_POST['valor_total'])),
				mb_strtoupper($_POST['aos_cuidados_de'], 'UTF-8'),
				$observacoes,
				$user_id,
				$data_hora_atual,
				$estado,
				$_POST['cidade'],
				$bairro,
				$_POST['endereco'],
				mb_strtoupper($_POST['nro_end'], 'UTF-8'),
				mb_strtoupper($_POST['loteamento'], 'UTF-8'),
				mb_strtoupper($_POST['complemento'], 'UTF-8'),
				mb_strtoupper($_POST['ponto_ref'], 'UTF-8'),
				$cep,
				$data_venda,
				$_POST['eh_venda'],
				$fone1,
				$fone2,
				$fone3,
				$fone4,
				$fone5,
				$fone1_obs,
				$fone2_obs,
				$fone3_obs,
				$fone4_obs,
				$fone5_obs,
				$_POST['id_orcamento']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			
			$pdo = $connection->prepare("
				INSERT INTO orcamentos 
					(
						data_cadastro, 
						tipo_orcamento, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao, 
						id_orcamentista, 
						usuario_salvou, 
						cliente_descricao, 
						estado, 
						cidade, 
						bairro, 
						endereco, 
						nro_end, 
						loteamento, 
						complemento, 
						ponto_ref, 
						cep, 
						data_venda, 
						eh_venda, 
						id_os
					) 
				VALUES 
					(
						?, '0',	?,	?,	?,	?,	2,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				$data_hora_atual,
				$_POST['id_orcamentista'],
				$texto,
				$estado,
				$_POST['cidade'],
				$bairro,
				$_POST['endereco'],
				mb_strtoupper($_POST['nro_end'], 'UTF-8'),
				mb_strtoupper($_POST['loteamento'], 'UTF-8'),
				mb_strtoupper($_POST['complemento'], 'UTF-8'),
				mb_strtoupper($_POST['ponto_ref'], 'UTF-8'),
				$cep,
				$data_venda,
				$_POST['eh_venda'],
				$id_os
			);
			$pdo->execute($params);
			$id_orcamento = $connection->lastInsertId();

			die(json_encode(array('id_orcamento'=> $id_orcamento, 'success'=>true, 'msg'=>SAVED_SUCCESS)));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}

		//se for orçamento, apaga os custos
		/*
		if($_POST['eh_venda'] == 'N'){
			$sqlE = "DELETE FROM vendas_custos WHERE cod_venda='{$_POST['id_orcamento']}'";
			$pdoE = $connection->prepare($sqlE);
			$pdoE->execute();
		}
		*/

		if(in_array($_POST['action'], array('INSERIR', 'EDITAR'))){
			$items = json_decode('['.$_POST['items'].']', true);
			foreach ($items as $key => $item) {
				if(empty($item['id_cor'])) {
					$item['id_cor'] = 0;
				}
				if($item['id_itens_orcamento']=='0'){
					$pdo = $connection->prepare("
						INSERT INTO itens_orcamento 
							(
								id_orcamento, 
								id_produto, 
								id_cor, 
								quantidade, 
								valor_unitario, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params = array(
						$_POST['id_orcamento'],
						$item['id_produto'],
						$item['id_cor'],
						$item['quantidade'],
						$item['valor_unitario'],
						($item['quantidade'] * $item['valor_unitario']),
						$data_hora_atual,
						$user_id,
						$user_id,
						$data_hora_atual,
						$observacoes
					);
					$pdo->execute($params);
				}	
				else{
					$pdo = $connection->prepare("
							UPDATE itens_orcamento SET 
									id_produto = ?, 
									id_cor = ?, 
									quantidade = ?, 
									valor_unitario = ?, 
									valor_total = ?, 
									alterado_por = ?, 
									data_alteracao = ?, 
									observacoes = ? 
		 					WHERE id_itens_orcamento = ?
					");
					$params = array(
						$item['id_produto'],
						$item['id_cor'],
						$item['quantidade'],
						$item['valor_unitario'],
						($item['quantidade'] * $item['valor_unitario']),
						$user_id,
						$data_hora_atual,
						$observacoes,
						$item['id_itens_orcamento']
					);
					$pdo->execute($params);
				}
			}
		}

		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}