<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$buscar2 = new Buscar();
		$tabela = 'orcamentos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql = "SELECT * FROM orcamentos WHERE id_orcamento=:id";
			$pdo = $connection->prepare($sql);

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
/*
		elseif(isset($_POST['action']) AND $_POST['action'] == 'LISTA_VENDAS_EM_ABERTOXXXXXXXXXXXX'){
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();

			if(isset($_POST['cliente'])){
				//usando alias
				$buscar->setBusca(array('id_cliente', 'id_cliente'), $_POST['cliente']);
				$buscar2->setBusca(array('id_cliente', 'id_cliente'), $_POST['cliente']);
				$buscar2->setBusca(array('saldo_a_receber', 'saldo_a_receber'), '0.00', 'maior');
			}

			if ($_POST['orcamento_venda'] == 'S') {
				//É venda
				//usuando alias
				$buscar->setBusca(array('eh_venda', 'eh_venda'), 'S');
				$buscar2->setBusca(array('eh_venda', 'eh_venda'), 'S');
			}
			else {
				//É orcamento
				$buscar->setBusca(array('eh_venda', 'eh_venda'), 'N');
				$buscar2->setBusca(array('eh_venda', 'eh_venda'), 'N');
			}

			$buscar->setBusca(array('usuario_salvou', 'usuario_salvou'), '1');
			$buscar2->setBusca(array('usuario_salvou', 'usuario_salvou'), '1');

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql2 = "SELECT O.id_cliente AS id_cliente, O.convertido_venda, O.eh_venda AS eh_venda, O.usuario_salvou AS usuario_salvou, (O.valor_total - (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) - (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)) AS saldo_a_receber, COUNT(*) AS total FROM orcamentos AS O {$filtro}";
			$pdo = $connection->prepare($sql2);
			$pdo->execute($buscar->getArrayExecute());

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$filtro2 = $buscar2->getSql2();

			$sql3 = "SELECT O.id_orcamento, 
				O.id_cliente AS id_cliente, 
				O.convertido_venda, 
				O.eh_venda AS eh_venda, 
				O.venda_finalizada, 
				O.usuario_salvou AS usuario_salvou, 
				O.validade_orcamento, 
				O.tipo_orcamento, 
				O.id_orcamentista, 
				UPPER(O.cliente_descricao) AS cliente_descricao, 
				O.data_cadastro, 
				O.data_venda, 
				CASE WHEN 
					C.tipo_cliente = 'J' 
					THEN 
						CASE WHEN C.nome_fantasia != '' 
							THEN 
								CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')') 
							ELSE 
								UPPER(C.razao_social) 
						END 
					ELSE 
						UPPER(C.nome_completo) 
				END AS cliente,
				O.desconto_porcento, 
				O.desconto_real, 
				O.acrescimo_porcento, 
				O.acrescimo_real, 
				O.valor_total, 
				O.valor_bruto, 
				(SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento) AS total_pago, 
				(SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) AS total_adiantado, 
				(O.valor_total 
				- (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) 
				- (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)
				) AS saldo_a_receber, 
				O.venda_finalizada, 
				CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', O.nro_end) AS nome_rua, 
				UPPER(O.complemento) AS endereco_complemento, 
				UPPER(CB.bairro_nome) AS nome_bairro, 
				UPPER(O.loteamento) AS endereco_loteamento, 
				UPPER(CL.loc_nome_abreviado) AS loc_nome_abreviado, 
				UPPER(O.aos_cuidados_de) AS aos_cuidados_de, 
				UPPER(O.observacoes) AS observacoes 
				FROM orcamentos AS O LEFT JOIN clientes AS C ON (O.id_cliente=C.cod_cliente) 
				LEFT JOIN correios_localidades AS CL ON (O.cidade=CL.id) 
				LEFT JOIN correios_enderecos AS CE ON (CE.id=O.endereco) 
				LEFT JOIN correios_bairros AS CB ON (O.bairro=CB.id) 
				{$filtro2} ORDER BY {$sort} {$order} LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql3);
			$pdo->execute($buscar2->getArrayExecute());

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$endereco_completo = "";
			foreach ($query as $key => $value) {
				$rows[$key] = $value;

				if($query[$key]->nome_rua != "") {
					$endereco_completo .= $rows[$key]->nome_rua;
				}

				if($query[$key]->endereco_complemento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_complemento.")";
				}

				if($query[$key]->nome_bairro != "") {
					$endereco_completo .= " - ".$rows[$key]->nome_bairro;
				}

				if($query[$key]->endereco_loteamento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_loteamento.")";
				}

				if ($endereco_completo != "") {
					if($query[$key]->loc_nome_abreviado != "") {
						$endereco_completo .= " / ".$rows[$key]->loc_nome_abreviado;
					}
				}

				if ($query[$key]->cliente != "") {
					$query[$key]->cliente = "<b>".$query[$key]->cliente."</b><br><i>".$endereco_completo."</i>";
				}
				else {
					$query[$key]->cliente = "<i>".$endereco_completo."</i>";
				}

				
				$endereco_completo = "";
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			$sql3 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql3);
			echo json_encode($result);
			//echo json_encode( array('dados'=>$query, 'sql2'=>$sql2, 'sql3'=>$sql3) );
		}
*/
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();

			if(isset($_POST['cliente'])){
				$buscar->setBusca(array('id_cliente', 'O.id_cliente'), $_POST['cliente']);
			}

			if ($_POST['orcamento_venda'] == 'S') {
				//É venda
				$buscar->setBusca(array('eh_venda', 'O.eh_venda'), 'S');
			}
			else {
				//É orcamento
				$buscar->setBusca(array('eh_venda', 'O.eh_venda'), 'N');
			}

			$buscar->setBusca(array('usuario_salvou', 'O.usuario_salvou'), '1');
			$buscar->setBusca(array('cancelado', 'O.cancelado'), 'N');

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('data_cadastro', 'O.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro']))), 'like');
				$buscar->setBusca(array('data_venda', 'O.data_venda'), implode('-', array_reverse(explode('/', $_POST['data_venda']))), 'like');
				$buscar->setBusca(array('id_vendedor', 'O.id_vendedor'), $_POST['id_vendedor']);
				$buscar->setBusca(array('cliente_descricao', 'O.cliente_descricao'), $_POST['cliente_descricao'], 'like');
				$buscar->setBusca(array('desconto', 'O.desconto'), $_POST['desconto']);
				$buscar->setBusca(array('acrescimo', 'O.acrescimo'), $_POST['acrescimo'], 'like');
				$buscar->setBusca(array('valor_total', 'O.valor_total'), $_POST['valor_total'], 'like');
				$buscar->setBusca(array('aos_cuidados_de', 'O.aos_cuidados_de'), $_POST['aos_cuidados_de'], 'like');
				$buscar->setBusca(array('observacoes', 'O.observacoes'), $_POST['observacoes'], 'like');
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();


			if ($_POST['orcamento_venda'] == 'S') {
				$sql2 = "SELECT O.id_cliente AS id_cliente, O.convertido_venda, O.eh_venda AS eh_venda, O.venda_cod_orcamento,O.usuario_salvou AS usuario_salvou, (O.valor_total - (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) - (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)) AS saldo_a_receber, COUNT(*) AS total FROM orcamentos AS O {$filtro}";
			}
			else {
				$sql2 = "SELECT COUNT(*) AS total FROM orcamentos AS O {$filtro}";
			}
			$pdo = $connection->prepare($sql2);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

/* cod. antigo
			$sql3 = "SELECT O.id_orcamento, 
				O.id_cliente AS id_cliente, 
				O.convertido_venda, 
				O.eh_venda AS eh_venda, 
				O.usuario_salvou AS usuario_salvou, 
				O.validade_orcamento, 
				O.tipo_orcamento, 
				O.id_orcamentista, 
				UPPER(O.cliente_descricao) AS cliente_descricao, 
				O.data_cadastro, 
				O.data_venda, 
				CASE WHEN 
					C.tipo_cliente = 'J' 
					THEN 
						CASE WHEN C.nome_fantasia != '' 
							THEN 
								CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')') 
							ELSE 
								UPPER(C.razao_social) 
						END 
					ELSE 
						UPPER(C.nome_completo) 
				END AS cliente,
				O.desconto_porcento, 
				O.desconto_real, 
				O.acrescimo_porcento, 
				O.acrescimo_real, 
				O.valor_total, 
				O.valor_bruto, 
				(SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento) AS total_pago, 
				(SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) AS total_adiantado, 
				(O.valor_total - (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) - (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)) AS saldo_a_receber, 
				O.venda_finalizada, 
				CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', O.nro_end) AS nome_rua, 
				UPPER(O.complemento) AS endereco_complemento, 
				UPPER(CB.bairro_nome) AS nome_bairro, 
				UPPER(O.loteamento) AS endereco_loteamento, 
				UPPER(CL.loc_nome_abreviado) AS loc_nome_abreviado, 
				UPPER(O.aos_cuidados_de) AS aos_cuidados_de, 
				UPPER(O.observacoes) AS observacoes 
				FROM orcamentos AS O LEFT JOIN clientes AS C ON (O.id_cliente=C.cod_cliente) 
				LEFT JOIN correios_localidades AS CL ON (O.cidade=CL.id) 
				LEFT JOIN correios_enderecos AS CE ON (CE.id=O.endereco) 
				LEFT JOIN correios_bairros AS CB ON (O.bairro=CB.id) 
				{$filtro} ORDER BY {$sort} {$order} LIMIT {$start}, {$limit}";
*/
			$sql3 = "SELECT 
				O.id_orcamento, 
				O.id_cliente AS id_cliente, 
				O.convertido_venda, 
				O.eh_venda AS eh_venda, 
				O.usuario_salvou AS usuario_salvou, 
				O.validade_orcamento, 
				O.tipo_orcamento, 
				O.id_orcamentista, 
				UPPER(O.cliente_descricao) AS cliente_descricao, 
				O.data_cadastro, 
				O.data_venda, 
				CASE WHEN 
					C.tipo_cliente = 'J' 
					THEN 
						CASE WHEN C.nome_fantasia != '' 
							THEN 
								CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')') 
							ELSE 
								UPPER(C.razao_social) 
						END 
					ELSE 
						UPPER(C.nome_completo) 
				END AS cliente,
				O.desconto_porcento, 
				O.desconto_real, 
				O.acrescimo_porcento, 
				O.acrescimo_real, 
				O.valor_total, 
				O.valor_bruto, 
				O.venda_finalizada, 
				O.venda_cod_orcamento, 
				CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', O.nro_end) AS nome_rua, 
				UPPER(O.complemento) AS endereco_complemento, 
				UPPER(CB.bairro_nome) AS nome_bairro, 
				UPPER(O.loteamento) AS endereco_loteamento, 
				UPPER(CL.loc_nome_abreviado) AS loc_nome_abreviado, 
				UPPER(O.aos_cuidados_de) AS aos_cuidados_de, 
				UPPER(O.observacoes) AS observacoes 
				FROM orcamentos AS O 
				LEFT JOIN clientes AS C ON (C.cod_cliente=O.id_cliente) 
				LEFT JOIN correios_localidades AS CL ON (CL.id=O.cidade) 
				LEFT JOIN correios_enderecos AS CE ON (CE.id=O.endereco) 
				LEFT JOIN correios_bairros AS CB ON (CB.id=O.bairro) 
				{$filtro} ORDER BY {$sort} {$order} LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql3);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$endereco_completo = "";
			foreach ($query as $key => $value) {
				$rows[$key] = $value;

				if($query[$key]->nome_rua != "") {
					$endereco_completo .= $rows[$key]->nome_rua;
				}

				if($query[$key]->endereco_complemento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_complemento.")";
				}

				if($query[$key]->nome_bairro != "") {
					$endereco_completo .= " - ".$rows[$key]->nome_bairro;
				}

				if($query[$key]->endereco_loteamento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_loteamento.")";
				}

				if ($endereco_completo != "") {
					if($query[$key]->loc_nome_abreviado != "") {
						$endereco_completo .= " / ".$rows[$key]->loc_nome_abreviado;
					}
				}

				if ($query[$key]->cliente != "") {
					$query[$key]->cliente = "<b>".$query[$key]->cliente."</b><br><i>".$endereco_completo."</i>";
				}
				else {
					$query[$key]->cliente = "<i>".$endereco_completo."</i>";
				}

				
				$endereco_completo = "";
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			$sql3 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql3);
			//echo json_encode(array('dados'=>$query, 'total'=>$countRow, 'sql2'=>$sql2, 'sql3'=>$sql3));
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>