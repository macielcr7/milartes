<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


if($_POST){
	require('../../autoLoad.php');
	require('../../orm.php');
	$tabela = 'orcamentos';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if($_POST['action'] == 'COPIA_ORCAMENTO'){
			$row = Orcamentos::find($_POST['id_orcamento'])->toArray();
			unset($row['id_orcamento']);
			$row['data_cadastro'] = $data_hora_atual;
			$row['data_alteracao'] = $data_hora_atual;
			$row['cadastrado_por'] = $user_id;
			$row['alterado_por'] = $user_id;

			$novo_id_orcamento = Orcamentos::create($row)->id_orcamento;

			if ($_POST['tipo_orcamento'] == 0){
				//se o tipo for geral pega os itens
				$pdoA = $connection->prepare("SELECT 
						id_produto, 
						item_nro, 
						quantidade, 
						valor_unitario, 
						valor_total, 
						observacoes, 
						id_cor 
					FROM itens_orcamento WHERE id_orcamento = ?
				");
				$pdoA->execute(array(
					$_POST['id_orcamento']
				));
				while ($row = $pdoA->fetch(PDO::FETCH_ASSOC)) {
					$valorA1 = $row['id_produto'];
					$valorA2 = $row['item_nro'];
					$valorA3 = $row['quantidade'];
					$valorA4 = $row['valor_unitario'];
					$valorA5 = $row['valor_total'];
					$valorA6 = $row['observacoes'];
					$valorA7 = $row['id_cor'];

					$pdoA1 = $connection->prepare("INSERT INTO itens_orcamento (
							id_produto, 
							item_nro, 
							id_cor, 
							quantidade, 
							valor_unitario, 
							id_orcamento, 
							valor_total, 
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) VALUES (
							'{$valorA1}', 
							'{$valorA2}', 
							'{$valorA7}', 
							'{$valorA3}', 
							'{$valorA4}', 
							'{$novo_id_orcamento}', 
							'{$valorA5}', 
							'{$data_hora_atual}', 
							'{$user_id}', 
							'{$user_id}', 
							'{$data_hora_atual}', 
							'{$valorA6}'
						)");
					$pdoA1->execute();
					$novo_id_itens_orcamento = $connection->lastInsertId();
				}
			}
			elseif($_POST['tipo_orcamento'] == 1){
				//se o tipo for forro
				$pdoB = $connection->prepare("SELECT 
						id_produtos_orcamentos_forro, 
						id_produtos_orcamentos_forro_pai, 
						id_produto, 
						id_cor, 
						quantidade, 
						valor_unitario, 
						valor_total, 
						valor_bruto, 
						tipo_produto, 
						possui_ambiente, 
						item_nro 
					FROM produtos_orcamentos_forro WHERE id_orcamento = ?
				");
				$pdoB->execute(array(
					$_POST['id_orcamento']
				));

				while ($row = $pdoB->fetch(PDO::FETCH_ASSOC)) {
					$valorB1 = $row['id_produtos_orcamentos_forro_pai'];
					$valorB2 = $row['id_produto'];
					$valorB3 = $row['id_cor'];
					$valorB4 = $row['quantidade'];
					$valorB5 = $row['valor_unitario'];
					$valorB6 = $row['valor_total'];
					$valorB7 = $row['valor_bruto'];
					$valorB9 = $row['tipo_produto'];
					$valorB10 = $row['possui_ambiente'];
					$valorB11 = $row['item_nro'];
					$valorB12 = $row['id_produtos_orcamentos_forro'];

					$pdoB1 = $connection->prepare("INSERT INTO produtos_orcamentos_forro (
							id_orcamento, 
							id_produtos_orcamentos_forro_pai,
							id_produto, 
							id_cor, 
							quantidade, 
							valor_unitario, 
							valor_total, 
							valor_bruto, 
							tipo_produto,
							possui_ambiente, 
							item_nro
						) VALUES (
							'{$novo_id_orcamento}', 
							'{$valorB1}', 
							'{$valorB2}', 
							'{$valorB3}', 
							'{$valorB4}', 
							'{$valorB5}', 
							'{$valorB6}', 
							'{$valorB7}', 
							'{$valorB9}', 
							'{$valorB10}', 
							'{$valorB11}'
						)");
					$pdoB1->execute();
					$novo_id_item_forro = $connection->lastInsertId();

					//se possui ambientes
					if ($valorB10 == "S"){
						$novo_id_ambiente_forro = 0;
						$pdoB2 = $connection->prepare("SELECT 
								ambiente, 
								qtd_pecas, 
								qtd_metros, 
								comprimento, 
								comprimento_real, 
								largura, 
								largura_real, 
								area, 
								descricao_pecas, 
								sn_l, 
								id_ambiente_pai, 
								tipo_acessorio, 
								observacoes 
							FROM ambientes_orcamentos_forro WHERE id_produtos_orcamentos_forro = ?
						");
						$pdoB2->execute(array(
							$valorB12
						));

						while ($row = $pdoB2->fetch(PDO::FETCH_ASSOC)) {
							$valorB22 = $row['ambiente'];
							$valorB23 = $row['qtd_pecas'];
							$valorB24 = $row['qtd_metros'];
							$valorB25 = $row['comprimento'];
							$valorB26 = $row['comprimento_real'];
							$valorB27 = $row['largura'];
							$valorB28 = $row['largura_real'];
							$valorB29 = $row['area'];
							$valorB210 = $row['descricao_pecas'];
							$valorB211 = $row['sn_l'];
							if ($row['id_ambiente_pai'] != 0){
								$row['id_ambiente_pai'] = $novo_id_ambiente_forro;
							}
							$valorB212 = $row['id_ambiente_pai'];
							$valorB213 = $row['tipo_acessorio'];
							$valorB214 = $row['observacoes'];

							$pdoB3 = $connection->prepare("INSERT INTO ambientes_orcamentos_forro (
									id_produtos_orcamentos_forro, 
									ambiente, 
									qtd_pecas, 
									qtd_metros, 
									comprimento, 
									comprimento_real, 
									largura, 
									largura_real, 
									area, 
									descricao_pecas, 
									sn_l, 
									id_ambiente_pai, 
									tipo_acessorio, 
									data_cadastro, 
									cadastrado_por, 
									alterado_por, 
									data_alteracao, 
									observacoes
								) VALUES (
									'{$novo_id_item_forro}', 
									'{$valorB22}', 
									'{$valorB23}', 
									'{$valorB24}', 
									'{$valorB25}', 
									'{$valorB26}', 
									'{$valorB27}', 
									'{$valorB28}', 
									'{$valorB29}', 
									'{$valorB210}', 
									'{$valorB211}', 
									'{$valorB212}', 
									'{$valorB213}', 
									'{$data_hora_atual}', 
									'{$user_id}', 
									'{$user_id}', 
									'{$data_hora_atual}', 
									'{$valorB214}'
								)");
							$pdoB3->execute();
							$novo_id_ambiente_forro = $connection->lastInsertId();

							//se possui observacoes....
							if (!empty($valorB214)){
								$monta_obs = "<b>".$valorB22.": ".$valorB214."</b>";
								$pdoX4 = $connection->prepare("INSERT INTO orcamentos_observacoes (
										orcamento_id, 
										ambiente_id, 
										nro_item, 
										descricao, 
										tipo, 
										cadastrado_por, 
										alterado_por, 
										data_cadastro, 
										data_alteracao
									) VALUES (
										'{$novo_id_orcamento}', 
										'{$novo_id_ambiente_forro}', 
										'{$valorB11}', 
										'{$monta_obs}', 
										'G', 
										'{$user_id}', 
										'{$user_id}', 
										'{$data_hora_atual}', 
										'{$data_hora_atual}'
									)");
								$pdoX4->execute();
							}
						}
					}
				}
			}
			elseif($_POST['tipo_orcamento'] == 3){
				//se o tipo for piso
				$pdoB = $connection->prepare("SELECT 
						id_produtos_orcamentos_piso, 
						id_produtos_orcamentos_piso_pai, 
						id_produto, 
						id_cor, 
						quantidade, 
						valor_unitario, 
						valor_total, 
						valor_bruto, 
						tipo_produto, 
						possui_ambiente, 
						qtd_caixas, 
						qtd_metros_porcento, 
						total_area, 
						item_nro 
					FROM produtos_orcamentos_piso WHERE id_orcamento = ?
				");
				$pdoB->execute(array(
					$_POST['id_orcamento']
				));

				while ($row = $pdoB->fetch(PDO::FETCH_ASSOC)) {
					$valorB1 = $row['id_produtos_orcamentos_piso_pai'];
					$valorB2 = $row['id_produto'];
					$valorB3 = $row['id_cor'];
					$valorB4 = $row['quantidade'];
					$valorB5 = $row['valor_unitario'];
					$valorB6 = $row['valor_total'];
					$valorB7 = $row['valor_bruto'];
					$valorB9 = $row['tipo_produto'];
					$valorB10 = $row['possui_ambiente'];
					$valorB11 = $row['item_nro'];
					$valorB12 = $row['id_produtos_orcamentos_piso'];
					$valorB13 = $row['qtd_caixas'];
					$valorB14 = $row['qtd_metros_porcento'];
					$valorB15 = $row['total_area'];

					$pdoB1 = $connection->prepare("INSERT INTO produtos_orcamentos_piso (
							id_orcamento, 
							id_produtos_orcamentos_piso_pai,
							id_produto, 
							id_cor, 
							quantidade, 
							valor_unitario, 
							valor_total, 
							valor_bruto, 
							tipo_produto,
							possui_ambiente, 
							item_nro, 
							qtd_caixas, 
							qtd_metros_porcento, 
							total_area
						) VALUES (
							'{$novo_id_orcamento}', 
							'{$valorB1}', 
							'{$valorB2}', 
							'{$valorB3}', 
							'{$valorB4}', 
							'{$valorB5}', 
							'{$valorB6}', 
							'{$valorB7}', 
							'{$valorB9}', 
							'{$valorB10}', 
							'{$valorB11}', 
							'{$valorB13}', 
							'{$valorB14}', 
							'{$valorB15}'
						)");
					$pdoB1->execute();
					$novo_id_item_piso = $connection->lastInsertId();

					//se possui ambientes
					if ($valorB10 == "S"){
						$novo_id_ambiente_piso = 0;
						$pdoB2 = $connection->prepare("SELECT 
								ambiente, 
								comprimento, 
								comprimento_real, 
								largura, 
								largura_real, 
								area, 
								sn_l, 
								id_ambiente_pai, 
								observacoes 
							FROM ambientes_orcamentos_piso WHERE id_produtos_orcamentos_piso = ?
						");
						$pdoB2->execute(array(
							$valorB12
						));

						while ($row = $pdoB2->fetch(PDO::FETCH_ASSOC)) {
							$valorB22 = $row['ambiente'];
							$valorB25 = $row['comprimento'];
							$valorB26 = $row['comprimento_real'];
							$valorB27 = $row['largura'];
							$valorB28 = $row['largura_real'];
							$valorB29 = $row['area'];
							$valorB211 = $row['sn_l'];
							if ($row['id_ambiente_pai'] != 0){
								$row['id_ambiente_pai'] = $novo_id_ambiente_piso;
							}
							$valorB212 = $row['id_ambiente_pai'];
							$valorB214 = $row['observacoes'];


							$pdoB3 = $connection->prepare("INSERT INTO ambientes_orcamentos_piso (
									id_produtos_orcamentos_piso, 
									ambiente, 
									comprimento, 
									comprimento_real, 
									largura, 
									largura_real, 
									area, 
									sn_l, 
									id_ambiente_pai, 
									data_cadastro, 
									cadastrado_por, 
									alterado_por, 
									data_alteracao, 
									observacoes
								) VALUES (
									'{$novo_id_item_piso}', 
									'{$valorB22}', 
									'{$valorB25}', 
									'{$valorB26}', 
									'{$valorB27}', 
									'{$valorB28}', 
									'{$valorB29}', 
									'{$valorB211}', 
									'{$valorB212}', 
									'{$data_hora_atual}', 
									'{$user_id}', 
									'{$user_id}', 
									'{$data_hora_atual}', 
									'{$valorB214}'
								)");
							$pdoB3->execute();
							$novo_id_ambiente_piso = $connection->lastInsertId();

							//se possui observacoes....
							if (!empty($valorB214)){
								$monta_obs = "<b>".$valorB22.": ".$valorB214."</b>";
								$pdoX4 = $connection->prepare("INSERT INTO orcamentos_observacoes (
										orcamento_id, 
										ambiente_id, 
										nro_item, 
										descricao, 
										tipo, 
										cadastrado_por, 
										alterado_por, 
										data_cadastro, 
										data_alteracao
									) VALUES (
										'{$novo_id_orcamento}', 
										'{$novo_id_ambiente_piso}', 
										'{$valorB11}', 
										'{$monta_obs}', 
										'G', 
										'{$user_id}', 
										'{$user_id}', 
										'{$data_hora_atual}', 
										'{$data_hora_atual}'
									)");
								$pdoX4->execute();
							}
						}
					}
				}
			}
			elseif($_POST['tipo_orcamento'] == 4){
				//se o tipo for rodapé
				$pdoB = $connection->prepare("SELECT 
						id_produtos_orcamentos_rodape, 
						id_produto, 
						id_cor, 
						quantidade, 
						valor_unitario, 
						valor_total, 
						valor_bruto, 
						tipo_produto, 
						qtd_barras, 
						qtd_metros_porcento, 
						total_area, 
						possui_ambiente, 
						item_nro 
					FROM produtos_orcamentos_rodape WHERE id_orcamento = ?
				");
				$pdoB->execute(array(
					$_POST['id_orcamento']
				));

				while ($row = $pdoB->fetch(PDO::FETCH_ASSOC)) {
					$valorB2 = $row['id_produto'];
					$valorB3 = $row['id_cor'];
					$valorB4 = $row['quantidade'];
					$valorB5 = $row['valor_unitario'];
					$valorB6 = $row['valor_total'];
					$valorB7 = $row['valor_bruto'];
					$valorB9 = $row['tipo_produto'];
					$valorB10 = $row['possui_ambiente'];
					$valorB11 = $row['item_nro'];
					$valorB12 = $row['id_produtos_orcamentos_rodape'];
					$valorB13 = $row['qtd_barras'];
					$valorB14 = $row['qtd_metros_porcento'];
					$valorB15 = $row['total_area'];

					$pdoB1 = $connection->prepare("INSERT INTO produtos_orcamentos_rodape (
							id_orcamento, 
							id_produto, 
							id_cor, 
							quantidade, 
							valor_unitario, 
							valor_total, 
							valor_bruto, 
							tipo_produto,
							possui_ambiente, 
							item_nro, 
							qtd_barras, 
							qtd_metros_porcento, 
							total_area
						) VALUES (
							'{$novo_id_orcamento}', 
							'{$valorB2}', 
							'{$valorB3}', 
							'{$valorB4}', 
							'{$valorB5}', 
							'{$valorB6}', 
							'{$valorB7}', 
							'{$valorB9}', 
							'{$valorB10}', 
							'{$valorB11}', 
							'{$valorB13}', 
							'{$valorB14}', 
							'{$valorB15}'
						)");
					$pdoB1->execute();
					$novo_id_item_rodape = $connection->lastInsertId();

					//se possui ambientes
					if ($valorB10 == "S"){
						$novo_id_ambiente_rodape = 0;
						$pdoB2 = $connection->prepare("SELECT 
								ambiente, 
								qtd_paredes, 
								comprimento_1, 
								comprimento_real_1, 
								comprimento_2, 
								comprimento_real_2, 
								comprimento_3, 
								comprimento_real_3, 
								comprimento_4, 
								comprimento_real_4, 
								comprimento_5, 
								comprimento_real_5, 
								comprimento_6, 
								comprimento_real_6, 
								comprimento_7, 
								comprimento_real_7, 
								comprimento_8, 
								comprimento_real_8, 
								comprimento_9, 
								comprimento_real_9, 
								comprimento_10, 
								comprimento_real_10, 
								comprimento_11, 
								comprimento_real_11, 
								comprimento_12, 
								comprimento_real_12, 
								comprimento_13, 
								comprimento_real_13, 
								comprimento_14, 
								comprimento_real_14, 
								comprimento_15, 
								comprimento_real_15, 
								comprimento_16, 
								comprimento_real_16, 
								comprimento_17, 
								comprimento_real_17, 
								comprimento_18, 
								comprimento_real_18, 
								comprimento_19, 
								comprimento_real_19, 
								comprimento_20, 
								comprimento_real_20, 
								comprimento_21, 
								comprimento_real_21, 
								observacoes, 
								area 
							FROM ambientes_orcamentos_rodape WHERE id_produtos_orcamentos_rodape = ?
						");
						$pdoB2->execute(array(
							$valorB12
						));

						while ($row = $pdoB2->fetch(PDO::FETCH_ASSOC)) {
							$valorB22 = $row['ambiente'];
							$valorB23 = $row['qtd_paredes'];
							$valorB24 = $row['comprimento_1'];
							$valorB25 = $row['comprimento_real_1'];
							$valorB26 = $row['comprimento_2'];
							$valorB27 = $row['comprimento_real_2'];
							$valorB28 = $row['comprimento_3'];
							$valorB29 = $row['comprimento_real_3'];
							$valorB210 = $row['comprimento_4'];
							$valorB211 = $row['comprimento_real_4'];
							$valorB212 = $row['comprimento_5'];
							$valorB213 = $row['comprimento_real_5'];
							$valorB214 = $row['comprimento_6'];
							$valorB215 = $row['comprimento_real_6'];
							$valorB216 = $row['comprimento_7'];
							$valorB217 = $row['comprimento_real_7'];
							$valorB218 = $row['comprimento_8'];
							$valorB219 = $row['comprimento_real_8'];
							$valorB220 = $row['comprimento_9'];
							$valorB221 = $row['comprimento_real_9'];
							$valorB222 = $row['comprimento_10'];
							$valorB223 = $row['comprimento_real_10'];
							$valorB224 = $row['comprimento_11'];
							$valorB225 = $row['comprimento_real_11'];
							$valorB226 = $row['comprimento_12'];
							$valorB227 = $row['comprimento_real_12'];
							$valorB228 = $row['comprimento_13'];
							$valorB229 = $row['comprimento_real_13'];
							$valorB230 = $row['comprimento_14'];
							$valorB231 = $row['comprimento_real_14'];
							$valorB232 = $row['comprimento_15'];
							$valorB233 = $row['comprimento_real_15'];
							$valorB234 = $row['comprimento_16'];
							$valorB235 = $row['comprimento_real_16'];
							$valorB236 = $row['comprimento_17'];
							$valorB237 = $row['comprimento_real_17'];
							$valorB238 = $row['comprimento_18'];
							$valorB239 = $row['comprimento_real_18'];
							$valorB240 = $row['comprimento_19'];
							$valorB241 = $row['comprimento_real_19'];
							$valorB242 = $row['comprimento_20'];
							$valorB243 = $row['comprimento_real_20'];
							$valorB244 = $row['comprimento_21'];
							$valorB245 = $row['comprimento_real_21'];
							$valorB246 = $row['observacoes'];
							$valorB247 = $row['area'];

							$pdoB3 = $connection->prepare("INSERT INTO ambientes_orcamentos_rodape (
									id_produtos_orcamentos_rodape, 
									ambiente, 
									qtd_paredes, 
									comprimento_1, 
									comprimento_real_1, 
									comprimento_2, 
									comprimento_real_2, 
									comprimento_3, 
									comprimento_real_3, 
									comprimento_4, 
									comprimento_real_4, 
									comprimento_5, 
									comprimento_real_5, 
									comprimento_6, 
									comprimento_real_6, 
									comprimento_7, 
									comprimento_real_7, 
									comprimento_8, 
									comprimento_real_8, 
									comprimento_9, 
									comprimento_real_9, 
									comprimento_10, 
									comprimento_real_10, 
									comprimento_11, 
									comprimento_real_11, 
									comprimento_12, 
									comprimento_real_12, 
									comprimento_13, 
									comprimento_real_13, 
									comprimento_14, 
									comprimento_real_14, 
									comprimento_15, 
									comprimento_real_15, 
									comprimento_16, 
									comprimento_real_16, 
									comprimento_17, 
									comprimento_real_17, 
									comprimento_18, 
									comprimento_real_18, 
									comprimento_19, 
									comprimento_real_19, 
									comprimento_20, 
									comprimento_real_20, 
									comprimento_21, 
									comprimento_real_21, 
									observacoes, 
									area, 
									data_cadastro, 
									cadastrado_por, 
									alterado_por, 
									data_alteracao
								) VALUES (
									'{$novo_id_item_rodape}', 
									'{$valorB22}', 
									'{$valorB23}', 
									'{$valorB24}', 
									'{$valorB25}', 
									'{$valorB26}', 
									'{$valorB27}', 
									'{$valorB28}', 
									'{$valorB29}', 
									'{$valorB210}', 
									'{$valorB211}', 
									'{$valorB212}', 
									'{$valorB213}', 
									'{$valorB214}', 
									'{$valorB215}', 
									'{$valorB216}', 
									'{$valorB217}', 
									'{$valorB218}', 
									'{$valorB219}', 
									'{$valorB220}', 
									'{$valorB221}', 
									'{$valorB222}', 
									'{$valorB223}', 
									'{$valorB224}', 
									'{$valorB225}', 
									'{$valorB226}', 
									'{$valorB227}', 
									'{$valorB228}', 
									'{$valorB229}', 
									'{$valorB230}', 
									'{$valorB231}', 
									'{$valorB232}', 
									'{$valorB233}', 
									'{$valorB234}', 
									'{$valorB235}', 
									'{$valorB236}', 
									'{$valorB237}', 
									'{$valorB238}', 
									'{$valorB239}', 
									'{$valorB240}', 
									'{$valorB241}', 
									'{$valorB242}', 
									'{$valorB243}', 
									'{$valorB244}', 
									'{$valorB245}', 
									'{$valorB246}', 
									'{$valorB247}', 
									'{$data_hora_atual}', 
									'{$user_id}', 
									'{$user_id}', 
									'{$data_hora_atual}'
								)");
							$pdoB3->execute();
							$novo_id_ambiente_rodape = $connection->lastInsertId();

							//se possui observacoes....
							if (!empty($valorB246)){
								$monta_obs = "<b>".$valorB22.": ".$valorB246."</b>";
								$pdoX4 = $connection->prepare("INSERT INTO orcamentos_observacoes (
										orcamento_id, 
										ambiente_id, 
										nro_item, 
										descricao, 
										tipo, 
										cadastrado_por, 
										alterado_por, 
										data_cadastro, 
										data_alteracao
									) VALUES (
										'{$novo_id_orcamento}', 
										'{$novo_id_ambiente_rodape}', 
										'{$valorB11}', 
										'{$monta_obs}', 
										'G', 
										'{$user_id}', 
										'{$user_id}', 
										'{$data_hora_atual}', 
										'{$data_hora_atual}'
									)");
								$pdoX4->execute();
							}
						}
					}
				}
			}


			//pega as observacoes
			$pdoX = $connection->prepare("SELECT 
					nro_item, 
					descricao, 
					tipo 
				FROM orcamentos_observacoes WHERE ambiente_id='0' AND orcamento_id = ?
			");
			$pdoX->execute(array(
				$_POST['id_orcamento']
			));

			while ($row = $pdoX->fetch(PDO::FETCH_ASSOC)) {
				$valorX2 = $row['nro_item'];
				$valorX3 = $row['descricao'];
				$valorX4 = $row['tipo'];
				/*
				if (empty($valorX1) OR $valorX1 = ''){
					$valorX1 = 0;
				}
				else {
					$valorX1 = 99;	
				}
				*/
				$pdoX3 = $connection->prepare("INSERT INTO orcamentos_observacoes (
						orcamento_id, 
						ambiente_id, 
						nro_item, 
						descricao, 
						tipo, 
						cadastrado_por, 
						alterado_por, 
						data_cadastro, 
						data_alteracao
					) VALUES (
						'{$novo_id_orcamento}', 
						'0', 
						'{$valorX2}', 
						'{$valorX3}', 
						'{$valorX4}', 
						'{$user_id}', 
						'{$user_id}', 
						'{$data_hora_atual}', 
						'{$data_hora_atual}'
					)");
				$pdoX3->execute();
			}

			if($_POST['convertido_venda'] == 'S'){
				//grava que o orçamento também foi gerado uma venda...
				$pdoX5 = $connection->prepare("
						UPDATE orcamentos SET 
								convertido_venda = ? 
	 					WHERE id_orcamento = ?
				");
				$paramsX5 = array(
					'S',
					$_POST['id_orcamento']
				);
				$pdoX5->execute($paramsX5);

				//registra que a nova cópia é uma venda
				$pdoX6 = $connection->prepare("
						UPDATE orcamentos SET 
								data_venda = ?, 
								eh_venda = ?, 
								venda_finalizada = ?, 
								venda_finalizada_em = ?, 
								venda_finalizada_por = ?, 
								venda_cod_orcamento = ? 
	 					WHERE id_orcamento = ?
				");
				$paramsX6 = array(
					date('Y-m-d H:i:s'),
					'S',
					'N',
					NULL,
					NULL,
					$_POST['id_orcamento'],
					$novo_id_orcamento
				);
				$pdoX6->execute($paramsX6);
			}
			else {
				$pdoX7 = $connection->prepare("
						UPDATE orcamentos SET 
								data_venda = ?, 
								venda_finalizada = ?, 
								venda_finalizada_em = ?, 
								venda_finalizada_por = ?, 
								convertido_venda = ?,
								eh_venda = ? 
	 					WHERE id_orcamento = ?
				");
				$paramsX7 = array(
					NULL,
					'N',
					NULL,
					NULL,
					'N',
					'N',
					$novo_id_orcamento
				);
				$pdoX7->execute($paramsX7);
			}









			//Gera os custos do produto
			if ($_POST['tipo_orcamento'] == 0) {
				//se o tipo for geral pega os itens
				$sql = "SELECT 
						id_itens_orcamento AS id_do_item, 
						id_produto AS id_do_produto, 
						quantidade, 
						observacoes, 
						id_cor 
					FROM itens_orcamento WHERE id_orcamento = '{$novo_id_orcamento}'";
			}
			elseif ($_POST['tipo_orcamento'] == 1){
				//se o tipo for forro
				$sql = "SELECT 
						id_produtos_orcamentos_forro AS id_do_item, 
						id_produtos_orcamentos_forro_pai, 
						id_produto AS id_do_produto, 
						id_cor, 
						quantidade, 
						tipo_produto 
					FROM produtos_orcamentos_forro WHERE id_orcamento = '{$novo_id_orcamento}'";
			}
			elseif ($_POST['tipo_orcamento'] == 3){
				//se o tipo for piso
				$sql = "SELECT 
						id_produtos_orcamentos_piso AS id_do_item, 
						id_produtos_orcamentos_piso_pai, 
						id_produto AS id_do_produto, 
						id_cor, 
						quantidade, 
						tipo_produto 
					FROM produtos_orcamentos_piso WHERE id_orcamento = '{$novo_id_orcamento}'";
			}
			elseif ($_POST['tipo_orcamento'] == 4){
				//se o tipo for rodapé
				$sql = "SELECT 
						id_produtos_orcamentos_rodape AS id_do_item, 
						id_produto AS id_do_produto, 
						id_cor, 
						quantidade, 
						tipo_produto 
					FROM produtos_orcamentos_rodape WHERE id_orcamento = '{$novo_id_orcamento}'";
			}

			$pdoC = $connection->prepare($sql);
			$pdoC->execute();

			while ($rowC = $pdoC->fetch(PDO::FETCH_ASSOC)) {
				$valorC0 = $rowC['id_do_item'];
				$valorC1 = $rowC['id_do_produto'];
				$valorC2 = $rowC['quantidade'];
				$valorC3 = $rowC['observacoes'];
				$valorC4 = $rowC['id_cor'];

				//pega os valores dos custos do produto
				$pdoD = $connection->prepare("SELECT possui_composicao, preco_custo AS preco_custo_produto FROM produtos WHERE id = '{$valorC1}'");
				$pdoD->execute();
				$custos = $pdoD->fetch(PDO::FETCH_OBJ);
				$precoCustoProduto = $custos->preco_custo_produto;

				if ($custos->possui_composicao == 'S') {
					$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo 
						FROM produto_composicoes AS PC 
						INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
						INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
						WHERE PC.id_produto = '{$valorC1}'";
					$pdo3 = $connection->prepare($sql3);
					$pdo3->execute();

					while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
					{
						//calcula a quantidade
						if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
							$nova_qtde = ceil($valorC2 * $item['quantidade']);
						} else {
							$nova_qtde = ($valorC2 * $item['quantidade']);
						}

						$pdo2 = $connection->prepare("
							INSERT INTO vendas_custos 
								(
									cod_venda, 
									cod_item_venda, 
									cod_produto, 
									cod_cor, 
									quantidade, 
									valor_unitario, 
									acrescimo_mo, 
									acrescimo_produto, 
									valor_total,
									data_cadastro, 
									cadastrado_por, 
									alterado_por, 
									data_alteracao, 
									observacoes
								) 
							VALUES 
								(
									?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
								)
						");
						$params2 = array(
							$novo_id_orcamento,
							$valorC0,
							$item['id_composicao'],
							$valorC4,
							$nova_qtde,
							$item['preco_custo'],
							0,
							0,
							($nova_qtde * $item['preco_custo']),
							date('Y-m-d H:i:s'),
							$user_id,
							$user_id,
							date('Y-m-d H:i:s'),
							$valorC3
						);
						$pdo2->execute($params2);
					}
				} else {
					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$novo_id_orcamento,
						$valorC0,
						$valorC1,
						$valorC4,
						$valorC2,
						$precoCustoProduto,
						0,
						0,
						($valorC2 * $precoCustoProduto),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			}

			echo json_encode(array('success'=>true, 'msg'=>'Orçamento Copiado com Sucesso'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		print_r($e);
		echo json_encode(array('success'=>false, 'msg'=>utf8_decode($e->getMessage()), 'erro'=>$e->getMessage()));
	}
}