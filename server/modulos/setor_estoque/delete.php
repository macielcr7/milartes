<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'setor_estoque';
	try {
		$connection->beginTransaction();

		$pdo = $connection->prepare("UPDATE setor_estoque SET ativo='N' WHERE id = ?");
		$pdo->execute(array($_POST['id']));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Desativado com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar o Setor de Estoque!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}