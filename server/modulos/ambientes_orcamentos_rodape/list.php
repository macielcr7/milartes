<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'ambientes_orcamentos_rodape';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("
				SELECT * 
				FROM ambientes_orcamentos_rodape
				WHERE id_orcamento=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}	
		else{			
			$result = array();			
			$buscar->setBusca(array('id_produtos_orcamentos_rodape', 'ambientes_orcamentos_rodape.id_produtos_orcamentos_rodape'), $_POST['id_produtos_orcamentos_rodape']);
			
			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("
				SELECT count(*) as total 
				FROM ambientes_orcamentos_rodape 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$pdo = $connection->prepare("
				SELECT ambientes_orcamentos_rodape.*, 
				ambientes_orcamentos_rodape.id_ambientes_orcamentos_rodape AS ordem
				FROM ambientes_orcamentos_rodape 
				{$filtro} ORDER BY id_ambientes_orcamentos_rodape ASC
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_ASSOC);

			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>