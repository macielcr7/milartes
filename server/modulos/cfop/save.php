<?php
/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'cfop';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE cfop SET 
				cod_fiscal = ?, 
				tipo = ?, 
				descricao = ?,
				detalhamento = ?,
				operacao = ?, 
				alterado_por = ?
				WHERE cod_fiscal = ?
			");
			$params = array(
				$_POST['cod_fiscal'], 
				$_POST['tipo'], 
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				mb_strtoupper($_POST['detalhamento'], 'UTF-8'),
				$_POST['operacao'],
				$user_id,
				$_POST['cod_fiscal']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO cfop 
					(
						cod_fiscal,
						tipo,
						descricao,
						detalhamento,
						operacao,
						data_cadastro,
						cadastrado_por,
						alterado_por
					)
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_fiscal'],
				$_POST['tipo'],
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				mb_strtoupper($_POST['detalhamento'], 'UTF-8'),
				$_POST['operacao'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}