<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$buscar2 = new Buscar();
		$tabela = 'veiculos_combustivel';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_abastecimento, '%d/%m/%Y') as data_abastecimento, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(data_alteracao, '%Y-%m-%d') as data_alteracao_date 
				FROM veiculos_combustivel
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
	
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('cod_veiculo', 't1.cod_veiculo'), $_POST['cod_veiculo']);
				$buscar2->setBusca(array('cod_veiculo', 't2.cod_veiculo'), $_POST['cod_veiculo']);
				$buscar->setBusca(array('hodometro', 't1.hodometro'), $_POST['hodometro']);
				$buscar->setBusca(array('tipo_combustivel', 't1.tipo_combustivel'), $_POST['tipo_combustivel'], 'like');
				$buscar->setBusca(array('quantidade', 't1.quantidade'), $_POST['quantidade'], 'like');
				$buscar->setBusca(array('valor_abastecimento', 't1.valor_abastecimento'), $_POST['valor_abastecimento'], 'like');
				$buscar->setBusca(array('data_abastecimento', 't1.data_abastecimento'), implode('-', array_reverse(explode('/', $_POST['data_abastecimento']))), 'like');
				$buscar->setBusca(array('data_cadastro', 't1.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('cadastrado_por', 't1.cadastrado_por'), $_POST['cadastrado_por']);
				$buscar->setBusca(array('alterado_por', 't1.alterado_por'), $_POST['alterado_por']);
				$buscar->setBusca(array('data_alteracao', 't1.data_alteracao'), implode('-', array_reverse(explode('/', $_POST['data_alteracao_date'])))." ".$_POST['data_alteracao_time'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = "t1.".trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			if (isset($_POST['cod_veiculo'])) {
				$buscar->setBusca(array('cod_veiculo', 't1.cod_veiculo'), $_POST['cod_veiculo']);
				$buscar2->setBusca(array('cod_veiculo', 't2.cod_veiculo'), $_POST['cod_veiculo']);
			}

			$filtro = $buscar->getSql();
			$filtro2 = $buscar2->getSql();

			$pdo = $connection->prepare("
				SELECT count(*) AS total 
				FROM veiculos_combustivel AS t1
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT t1.*, 
					U.nome AS quem_abasteceu, 
					(SELECT (t1.hodometro-t2.hodometro) FROM veiculos_combustivel AS t2 {$filtro2} AND t2.hodometro < t1.hodometro ORDER BY t2.hodometro DESC LIMIT 0,1) AS distancia_percorrida,
					DATE_FORMAT(t1.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(t1.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(t1.data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(t1.data_alteracao, '%Y-%m-%d') as data_alteracao_date 
				FROM veiculos_combustivel AS t1 
				INNER JOIN usuarios AS U ON (U.id=t1.quem_abasteceu)
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$rows[$key] = $value;
				//$rows[$key]->hodometro = number_format($rows[$key]->hodometro, 0, ',', '.');

				if (empty($rows[$key]->consumo_medio)) {
					$rows[$key]->consumo_medio = 0;
				}

				$rows[$key]->consumo_medio = ($rows[$key]->distancia_percorrida * 1000)/$rows[$key]->quantidade;
				$rows[$key]->consumo_medio = number_format($rows[$key]->consumo_medio, 2, ',', '.').' KM/L';

				if (empty($rows[$key]->distancia_percorrida)){
					$rows[$key]->distancia_percorrida = 0;
				}

				$rows[$key]->distancia_percorrida = ($rows[$key]->distancia_percorrida * 1000)." KM";

				if ($rows[$key]->tipo_combustivel == 'GNV'){
					$rows[$key]->quantidade = number_format($rows[$key]->quantidade, 2, ',', '.').' KG';
				}
				else{
					$rows[$key]->quantidade = number_format($rows[$key]->quantidade, 2, ',', '.').' L';
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			//echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$rows, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>