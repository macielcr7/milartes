<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'veiculos_combustivel';
	$data_atual = date('Y-m-d');
	$data_hora_atual = date('Y-m-d H:i:s');

//	$hodometro = str_replace(".", "", $_POST['hodometro']);
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE veiculos_combustivel SET 
						cod_veiculo = ?,
						hodometro = ?,
						tipo_combustivel = ?,
						quantidade = ?,
						valor_abastecimento = ?,
						data_abastecimento = ?,
						alterado_por = ?,
						data_alteracao = ?,
						quem_abasteceu = ? 
 					WHERE id = ?
			");
			$params = array(
				$_POST['cod_veiculo'],
				$_POST['hodometro'],
				$_POST['tipo_combustivel'],
				$_POST['quantidade'],
				$_POST['valor_abastecimento'],
				implode('-', array_reverse(explode('/', $_POST['data_abastecimento']))),
				$user_id,
				$data_hora_atual,
				$_POST['quem_abasteceu'],
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO veiculos_combustivel 
					(
						cod_veiculo,
						hodometro,
						tipo_combustivel,
						quantidade,
						valor_abastecimento,
						data_abastecimento,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						data_alteracao,
						quem_abasteceu
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_veiculo'],
				$_POST['hodometro'],
				$_POST['tipo_combustivel'],
				$_POST['quantidade'],
				$_POST['valor_abastecimento'],
				implode('-', array_reverse(explode('/', $_POST['data_abastecimento']))),
				$data_hora_atual,
				$user_id,
				$user_id,
				$data_hora_atual,
				$_POST['quem_abasteceu']
			);

			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}