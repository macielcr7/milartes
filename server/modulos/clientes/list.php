<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'clientes';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql = "SELECT C.cod_cliente, 
					C.tipo_cliente,
					C.nome_completo,
					C.razao_social, 
					C.nome_fantasia,
					C.pessoa_contato,
					DATE_FORMAT(C.data_nascimento, '%d/%m/%Y') as data_nascimento,
					C.sexo,
					C.cpf,
					C.cnpj,
					C.ie,
					C.im,
					C.identidade,
					C.profissao,
					C.estado,
					C.cidade,
					C.bairro,
					C.endereco,
					C.nro_end,
					C.loteamento,
					C.complemento,
					C.ponto_ref,
					C.cx_postal,
					C.cep,
					C.situacao_cadastral,
					C.email_nfe,
					C.id_categoria_precos, 
					CONCAT(UPPER(U.nome), ' EM: ', DATE_FORMAT(C.data_cadastro, '%d/%m/%Y'), ' ', DATE_FORMAT(C.data_cadastro, '%H:%i:%s')) as cadastrado_por,
					CONCAT(UPPER(U2.nome), ' EM: ', DATE_FORMAT(C.data_alteracao, '%d/%m/%Y'), ' ', DATE_FORMAT(C.data_alteracao, '%H:%i:%s')) as alterado_por 
				FROM clientes AS C
				LEFT JOIN usuarios AS U ON (U.id=C.cadastrado_por)
				LEFT JOIN usuarios AS U2 ON (U2.id=C.alterado_por)
				WHERE C.cod_cliente=:id
			";
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$dados = $linhas;
			if($dados->data_nascimento == '00/00/0000') {
				$dados->data_nascimento = "";
			}
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES_ORCAMENTOS' ){
			$sql = "SELECT C.cod_cliente, 
					C.estado,
					C.cidade,
					C.bairro,
					C.endereco,
					C.nro_end,
					C.loteamento,
					C.complemento,
					C.ponto_ref,
					C.cx_postal,
					C.cep, 
					(SELECT GROUP_CONCAT(CONCAT(CC.descricao, ' ', UPPER(CC.observacao)) SEPARATOR '|') AS fone FROM clientes_contato AS CC WHERE CC.cod_cliente=C.cod_cliente AND CC.tipo_contato='TELEFONE' ORDER BY CC.controle ASC) AS fones 
				FROM clientes AS C 
				WHERE C.cod_cliente=:id
			";
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$query[$key] = $value;
				if($query[$key]->fones != "") {
					$palavras = explode('|' , $query[$key]->fones);
					$total_palavras = count($palavras);

					$fones = "";
					$j = 1;
					foreach($palavras as $i => $key2) {
						$nro = explode(' ' , $key2);
						$caracteres = strlen($nro[0]);

						if ($caracteres == 10){
							$sub1 = 10;
							$sub2 = 11;
						}
						elseif ($caracteres == 11){
							$sub1 = 11;
							$sub2 = 12;
						}
						else{
							$sub1 = 11;
							$sub2 = 12;
						}
						
						if ($j == 1 ) {
							$query[$key]->fone1 =  substr($key2, 0, $sub1);
							$query[$key]->fone1_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone1_obs)) { $query[$key]->fone1_obs = ""; }
						} elseif ($j == 2 ) {
							$query[$key]->fone2 =  substr($key2, 0, $sub1);
							$query[$key]->fone2_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone2_obs)) { $query[$key]->fone2_obs = ""; }
						} elseif ($j == 3 ) {
							$query[$key]->fone3 =  substr($key2, 0, $sub1);
							$query[$key]->fone3_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone3_obs)) { $query[$key]->fone3_obs = ""; }
						} elseif ($j == 4 ) {
							$query[$key]->fone4 =  substr($key2, 0, $sub1);
							$query[$key]->fone4_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone4_obs)) { $query[$key]->fone4_obs = ""; }
						} elseif ($j == 5 ) {
							$query[$key]->fone5 =  substr($key2, 0, $sub1);
							$query[$key]->fone5_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone5_obs)) { $query[$key]->fone5_obs = ""; }
						}

						$j++;
					}
					$query[$key]->fones = $fones;
				}
			}
			echo json_encode( array('success'=>true, 'dados'=>$query) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'VALID_UNIQUE' ){
			$where = "WHERE {$_POST['param']} = :valor AND situacao_cadastral != 'E'";

			if(isset($_POST['cod_cliente']) and !empty($_POST['cod_cliente'])){
				$where .= " AND cod_cliente != ".$_POST['cod_cliente'];
			}

			$pdo = $connection->prepare("SELECT COUNT(*) AS total FROM clientes AS C {$where}");
			$pdo->bindParam(':valor', $_POST['valor']);
			$pdo->execute();

			if (empty($_POST['valor'])) {
				$total = 0;
			}
			elseif ($_POST['valor'] == "__.___.___/____-__") {
				$total = 0;
			}
			elseif ($_POST['valor'] == "___.___.___-__") {
				$total = 0;
			}
			else {
				$total = $pdo->fetch(PDO::FETCH_OBJ)->total;
			}
			
			if($total > 0){
				$success = false;
			} else{ $success = true; }

			echo json_encode( array('success'=>$success) );
		}
		elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_PROFISSAO'){
			$buscar->setBusca(array('profissao', 'C.profissao'), $_POST['query'], 'like');
			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT C.profissao AS id, C.profissao AS descricao 
				FROM clientes AS C
				{$filtro} 
				GROUP BY C.profissao
				ORDER BY C.profissao ASC
			");
			$pdo->execute($buscar->getArrayExecute());

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$pdo = $connection->prepare("
				SELECT cod_cliente as id, UPPER(nome_completo) AS descricao, id_categoria_precos 
				FROM clientes 
				WHERE tipo_cliente = 'F'
				ORDER BY nome_completo ASC
			");
			$pdo->execute();
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_ORCAMENTOS'){
			$filtro = "(C.tipo_cliente = 'F' OR C.tipo_cliente = 'J')";

			if(isset($_POST['query'])){
					$filtro .= " AND (C.nome_completo LIKE '%{$_POST['query']}%' OR C.razao_social LIKE '%{$_POST['query']}%' OR C.nome_fantasia LIKE '%{$_POST['query']}%' OR CE.nome LIKE '%{$_POST['query']}%') AND C.situacao_cadastral='N'";
			}

			if(isset($_POST['id_cliente']) and !empty($_POST['id_cliente'])){
				$filtro .= "  AND C.cod_cliente = {$_POST['id_cliente']} ";
				$offset = 'LIMIT 1';
			}

			$sql = "SELECT C.cod_cliente as id, 
					C.id_categoria_precos, 
					CONCAT(UPPER(CL.loc_nome), ' / ', C.estado) AS loc_nome, 

					CASE WHEN 
						C.tipo_cliente = 'J' 
						THEN 
							CASE WHEN C.nome_fantasia != '' 
								THEN 
									CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')') 
								ELSE 
									UPPER(C.razao_social) 
							END 
						ELSE 
							UPPER(C.nome_completo) 
					END AS descricao, 

					CASE WHEN 
						C.complemento != '' 
						THEN 
							CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', C.nro_end, ' (', UPPER(C.complemento), ')') 
						ELSE 
							CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', C.nro_end) 
					END AS nome, 
					CASE WHEN 
						C.loteamento != '' 
						THEN 
							CONCAT(UPPER(CB.bairro_nome), ' (', UPPER(C.loteamento), ')') 
						ELSE 
							UPPER(CB.bairro_nome) 
					END AS bairro_nome 
				FROM clientes AS C 
				LEFT JOIN correios_localidades AS CL ON (C.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (C.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (C.endereco=CE.id) 
				WHERE {$filtro} 
				ORDER BY C.cod_cliente DESC, descricao ASC, CE.nome ASC {$offset}
				";

			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$endereco_completo = "";
				$query[$key] = $value;
				//$query[$key]->descricao = str_pad($query[$key]->id, 5, "0", STR_PAD_LEFT)." - ".$query[$key]->descricao;

				if($query[$key]->tipo_cliente == "F") {
					$descricao = $query[$key]->nome;
				}

				if($query[$key]->nome != "") {
					$endereco_completo .= $query[$key]->nome;
				}

				if($query[$key]->bairro_nome != "") {
					$endereco_completo .= " - ". $query[$key]->bairro_nome;
				}

				if($query[$key]->loc_nome != "") {
					$endereco_completo .= " - ".$query[$key]->loc_nome;
				}

				$query[$key]->endereco_completo = $endereco_completo;
			}

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
		elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_ORCAMENTOSxxxxx'){
			$sql = "SELECT C.cod_cliente as id, 
					UPPER(C.nome_completo) AS descricao, 
					C.id_categoria_precos, 
					CONCAT(UPPER(CL.loc_nome), ' / ', C.estado) AS loc_nome, 

					CASE WHEN
						C.complemento != '' THEN CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end, ' (', UPPER(C.complemento), ')')
					ELSE
						CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end)
					END AS nome,

					CASE WHEN
						C.loteamento != '' THEN CONCAT(UPPER(CB.bairro_nome), ' (', UPPER(C.loteamento), ')') 
					ELSE
						UPPER(CB.bairro_nome)
					END AS bairro_nome  
				FROM clientes AS C 
				LEFT JOIN correios_localidades AS CL ON (C.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (C.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (C.endereco=CE.id) 
				WHERE tipo_cliente = 'F'
				ORDER BY nome_completo ASC
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$endereco_completo = "";
				$query[$key] = $value;
				//$query[$key]->descricao = str_pad($query[$key]->id, 5, "0", STR_PAD_LEFT)." - ".$query[$key]->descricao;

				if($query[$key]->nome != "") {
					$endereco_completo .= $query[$key]->nome;
				}

				if($query[$key]->bairro_nome != "") {
					$endereco_completo .= " - ". $query[$key]->bairro_nome;
				}

				if($query[$key]->loc_nome != "") {
					$endereco_completo .= " - ".$query[$key]->loc_nome;
				}

				$query[$key]->endereco_completo = $endereco_completo;
			}


			echo json_encode( array('dados'=>$query) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['query']) and !empty($_POST['query'])){
				$buscar->setBusca(array('cod_cliente', 'C.cod_cliente'), $_POST['query'], 'like');
				$buscar->setBusca(array('nome_completo', 'C.nome_completo'), $_POST['query'], 'like');
				$buscar->setBusca(array('razao_social', 'C.razao_social'), $_POST['query'], 'like');
				$buscar->setBusca(array('nome_fantasia', 'C.nome_fantasia'), $_POST['query'], 'like');
				$buscar->setBusca(array('cpf', 'C.cpf'), $_POST['query'], 'like');
				$buscar->setBusca(array('cnpj', 'C.cnpj'), $_POST['query'], 'like');
				$buscar->setBusca(array('estado', 'C.estado'), $_POST['query'], 'like');
				$buscar->setBusca(array('cidade', 'CL.loc_nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('bairro', 'CB.bairro_nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('endereco', 'CE.nome'), $_POST['query'], 'like');
				$filtro = $buscar->filtro.' ('.implode($buscar->sql, ' OR ').') ';
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('nome_completo', 'C.nome_completo'), $_POST['nome'], 'like');
				$buscar->setBusca(array('razao_social', 'C.razao_social'), $_POST['nome'], 'like');
				$buscar->setBusca(array('nome_fantasia', 'C.nome_fantasia'), $_POST['nome'], 'like');
				$buscar->setBusca(array('estado', 'C.estado'), $_POST['estado']);
				$buscar->setBusca(array('cidade', 'C.cidade'), $_POST['cidade']);
				$buscar->setBusca(array('endereco', 'C.endereco'), $_POST['endereco']);
				$filtro = $buscar->filtro.' ('.implode($buscar->sql, ' AND ').') ';
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$sql = "SELECT count(*) AS total 
				FROM clientes AS C 
				LEFT JOIN correios_localidades AS CL ON (C.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (C.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (C.endereco=CE.id) 
				{$filtro}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT 
					C.*,
					C.cod_cliente, 
					C.tipo_cliente, 
					UPPER(C.nome_completo) AS nome_completo, 
					UPPER(C.razao_social) AS razao_social, 
					UPPER(C.nome_fantasia) AS nome_fantasia, 
					C.cpf, 
					C.cnpj, 
					C.nro_end, 
					C.loteamento, 
					C.situacao_cadastral,
					C.id_categoria_precos, 
					DATE_FORMAT(C.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(C.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(C.data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(C.data_alteracao, '%Y-%m-%d') as data_alteracao_date, 
					CONCAT(UPPER(CL.loc_nome), ' / ', C.estado) AS loc_nome, 

					CASE WHEN
						C.complemento != '' THEN CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end, ' (', UPPER(C.complemento), ')')
					ELSE
						CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end)
					END AS nome,

					CASE WHEN
						C.loteamento != '' THEN CONCAT(UPPER(CB.bairro_nome), ' (', UPPER(C.loteamento), ')') 
					ELSE
						UPPER(CB.bairro_nome)
					END AS bairro_nome, 
					(SELECT GROUP_CONCAT(CONCAT(CC.descricao, ' ', UPPER(CC.observacao)) SEPARATOR '|') AS fone FROM clientes_contato AS CC WHERE CC.cod_cliente=C.cod_cliente AND CC.tipo_contato='TELEFONE' ORDER BY CC.controle ASC) AS fones 
				FROM clientes AS C 
				LEFT JOIN correios_localidades AS CL ON (C.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (C.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (C.endereco=CE.id) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
				";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$query[$key] = $value;
				if($query[$key]->fones != "") {
					$palavras = explode('|' , $query[$key]->fones);
					$total_palavras = count($palavras);

					$fones = "";
					$j = 1;
					foreach($palavras as $i => $key2) {
						$nro = explode(' ' , $key2);
						$caracteres = strlen($nro[0]);

						if ($caracteres == 10){
							$fones .= '(' . substr($key2, 0, 2) . ') ' . substr($key2, 2, 4) . '-' . substr($key2, 6);
							$sub1 = 10;
							$sub2 = 11;
						}
						elseif ($caracteres == 11){
							$fones .= '(' . substr($key2, 0, 2) . ') ' . substr($key2, 2, 5) . '-' . substr($key2, 7);
							$sub1 = 11;
							$sub2 = 12;
						}
						else{
							$fones .= substr($key2, 0, 4) . ' ' . substr($key2, 4, 3) . '-' . substr($key2, 7);
							$sub1 = 11;
							$sub2 = 12;
						}

						if ($j == 1 ) {
							$query[$key]->fone1 =  substr($key2, 0, $sub1);
							$query[$key]->fone1_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone1_obs)) { $query[$key]->fone1_obs = ""; }
						} elseif ($j == 2 ) {
							$query[$key]->fone2 =  substr($key2, 0, $sub1);
							$query[$key]->fone2_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone2_obs)) { $query[$key]->fone2_obs = ""; }
						} elseif ($j == 3 ) {
							$query[$key]->fone3 =  substr($key2, 0, $sub1);
							$query[$key]->fone3_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone3_obs)) { $query[$key]->fone3_obs = ""; }
						} elseif ($j == 4 ) {
							$query[$key]->fone4 =  substr($key2, 0, $sub1);
							$query[$key]->fone4_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone4_obs)) { $query[$key]->fone4_obs = ""; }
						} elseif ($j == 5 ) {
							$query[$key]->fone5 =  substr($key2, 0, $sub1);
							$query[$key]->fone5_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone5_obs)) { $query[$key]->fone5_obs = ""; }
						}

						if ($i != $total_palavras-1) {
							$fones .= "</br>";
						}
						$j++;
					}
					$query[$key]->fones = $fones;
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>