<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

/*cod_fornecedor:1
tipo_contato:email
descricao:maciel
observacao:macielcr7@gmail.com
controle:0
action:INSERIR*/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'fornecedores_contato';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	
	try {
		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
					UPDATE fornecedores_contato SET 
							cod_fornecedor = ?,							
							tipo_contato = ?,							
							descricao = ?,							
							observacao = ?,					
							alterado_por = ?							
 					WHERE controle = ?
			");
			$params = array(
				$_POST['cod_fornecedor'],
				$_POST['tipo_contato'],
				$_POST['descricao'],
				$_POST['observacao'],
				$user_id,
				$_POST['controle']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO fornecedores_contato 
					(
						cod_fornecedor,						
						tipo_contato,						
						descricao,						
						observacao,						
						cadastrado_por,						
						data_cadastro						
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_fornecedor'],		
				$_POST['tipo_contato'],		
				$_POST['descricao'],		
				$_POST['observacao'],		
				$user_id,		
				date('Y-m-d H:i:s')		
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}