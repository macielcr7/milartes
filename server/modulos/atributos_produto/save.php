<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'atributos_produto';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		
		if($_POST['action'] == 'EDITAR'){
		
			$user->getAcao($tabela, 'editar');
		
			$pdo = $connection->prepare("
					UPDATE atributos_produto SET 
							id_atributo_categoria = ?,
							valor = ?,
							alterado_por = ?, 
							data_alteracao = ? 
 					WHERE id_atributos_produto = ?
			");
			$params = array(
				$_POST['id_atributo_categoria'],
				$_POST['valor'],
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['id_atributos_produto']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
		
			$user->getAcao($tabela, 'adicionar');
		
			$pdo = $connection->prepare("
				INSERT INTO atributos_produto 
					(
						id_atributo_categoria,
						valor,
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['id_atributo_categoria'],
				$_POST['valor'],
				date('Y-m-d H:i:s'), 
				$user_id,
				$user_id,
				date('Y-m-d H:i:s')
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}