<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'atributos_produto';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("
				SELECT * 
				FROM atributos_produto
				WHERE id_atributos_produto=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
	
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();

//
			$buscar->setBusca(array('id_produto', 'atributos_produto.id_produto'), $_POST['id_produto']);

			if ($_POST['exibe_zerados'] == "N") {
				$buscar->setBusca(array('valor', 'atributos_produto.valor'), '0.000', 'diff');
			}
//
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('id_atributo_categoria', 'atributos_produto.id_atributo_categoria'), $_POST['id_atributo_categoria']);
				$buscar->setBusca(array('valor', 'atributos_produto.valor'), $_POST['valor'], 'like');
			}
			
			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$sql = "SELECT count(*) as total FROM atributos_produto INNER JOIN atributos ON (atributos_produto.id_atributo_categoria=atributos.id_atributos) 
				{$filtro}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			

			$sql2 = "SELECT atributos_produto.*, atributos.atributo AS descricao_atributo 
				FROM atributos_produto INNER JOIN atributos ON (atributos_produto.id_atributo_categoria=atributos.id_atributos) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql2);
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql, 'sql2'=>$sql2) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>