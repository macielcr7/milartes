<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'formas_pagto';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT * FROM formas_pagto AS FP WHERE FP.id=:id"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$pdo = $connection->prepare("
				SELECT formas_pagto.*, contratos_cartao.nro_max_parcelas as max_cartao 
				FROM formas_pagto 
					LEFT JOIN contratos_cartao ON formas_pagto.categoria = 3 AND formas_pagto.cod_convenio = contratos_cartao.id 
					LEFT JOIN contratos_cobranca ON formas_pagto.categoria = 2 AND formas_pagto.cod_convenio = contratos_cobranca.id
				WHERE formas_pagto.ativo='S'
			");
			$pdo->execute();
			
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";
			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE FP.descricao like '%$_POST[query]%'";
				$where2 = " AND FP.descricao like '%$_POST[query]%'";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_forma'])) {
					$where = "WHERE FP.ativo='S'".$where2;
				}
				elseif ($_POST['status_forma'] == 'X') {
					$where = $where1;
				}
				else{
					$where = "WHERE FP.ativo='{$_POST['status_forma']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM formas_pagto AS FP $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT FP.*, 
				DATE_FORMAT(FP.data_cadastro, '%d/%m/%Y ás %H:%i:%s') AS data_cadastro, 
				UPPER(U.nome) AS cadastrado_por_nome
				FROM formas_pagto AS FP INNER JOIN usuarios AS U ON (U.id=FP.cadastrado_por)
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}	
}
?>