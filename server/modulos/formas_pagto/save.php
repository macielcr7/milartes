<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'formas_pagto';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE formas_pagto SET 
				descricao = ?,
				tipo = ?,
				categoria = ?,
				cod_convenio = ?,
				ativo = ?,
				alterado_por = ? 
				WHERE id = ?
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$_POST['tipo'],
				$_POST['categoria'],
				$_POST['cod_convenio'],
				$_POST['ativo'],
				$user_id,
				$_POST['id']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO formas_pagto 
					(
						descricao,
						tipo,
						categoria,
						cod_convenio,
						ativo,
						data_cadastro,
						cadastrado_por,
						alterado_por
					)
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$_POST['tipo'],
				$_POST['categoria'],
				$_POST['cod_convenio'],
				$_POST['ativo'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id
			);
		}

		$pdo->execute($params);

		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (Exception $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}