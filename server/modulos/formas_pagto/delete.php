<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'formas_pagto';
	try {
		$connection->beginTransaction();

		$pdo = $connection->prepare("UPDATE formas_pagto SET ativo='N' WHERE id = ?");
		$pdo->execute(array($_POST['id']));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Desativado com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar a Forma de Pagamento!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}