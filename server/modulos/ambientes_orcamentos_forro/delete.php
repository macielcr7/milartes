<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	
	try {
		if($_POST['action'] == 'DELETAR'){
			$pdo = $connection->prepare("DELETE FROM ambientes_orcamentos_forro WHERE id_ambientes_orcamentos_forro = ?");
			$pdo->execute(array(
				$_POST['id']
			));

			$sqlD = "DELETE FROM orcamentos_observacoes WHERE orcamento_id={$_POST['id_orcamento']} AND ambiente_id={$_POST['id']}";
			$pdoD = $connection->prepare($sqlD);
			$pdoD->execute();
			
			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}