<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'produtos_cores';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("
				SELECT * 
				FROM produtos_cores
				WHERE id_produtos_cores=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$buscar->setBusca(array('id_produto', 'produtos_cores.id_produto'), $_POST['id_produto']);
			$filtro = $buscar->getSql();

			$sql = "
				SELECT produtos_cores.id_produtos_cores as id, cores.descricao 
				FROM produtos_cores 
				INNER JOIN cores ON (produtos_cores.id_cor=cores.id_cores) 
				{$filtro}
				ORDER BY cores.descricao
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode( array('dados'=> $linhas ) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO2'){
			$buscar->setBusca(array('id_produto', 'produtos_cores.id_produto'), $_POST['id_produto']);
			$filtro = $buscar->getSql();

			$sql = "
				SELECT cores.id_cores as id, cores.descricao 
				FROM produtos_cores 
				INNER JOIN cores ON (produtos_cores.id_cor=cores.id_cores) 
				{$filtro}
				ORDER BY cores.descricao
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode( array('dados'=> $linhas ) );
		}
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			$buscar->setBusca(array('id_produto', 'produtos_cores.id_produto'), $_POST['id_produto']);
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('id_cor', 'produtos_cores.id_cor'), $_POST['id_cor']);
			}
			
			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("
				SELECT count(*) as total 
				FROM produtos_cores INNER JOIN cores ON
					(produtos_cores.id_cor=cores.id_cores) 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$pdo = $connection->prepare("
				SELECT produtos_cores.*, cores.descricao 
				FROM produtos_cores 
				INNER JOIN cores ON (produtos_cores.id_cor=cores.id_cores) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>