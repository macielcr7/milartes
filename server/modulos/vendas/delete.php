<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'vendas';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		//verfifica se tem caixa aberto
		$sql = "SELECT caixa_atual FROM usuarios WHERE id = '{$user_id}'";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetch(PDO::FETCH_OBJ);
		$caixa_atual = $query->caixa_atual;

		if ($caixa_atual == 0) {
			echo json_encode(array('success'=>false, 'msg'=>'Você deve Primeiro Abrir o Caixa para poder Estornar valores', 'erro'=>'Precia Abrir o Caixa Primeiro'));
		}
		else{
			if($_POST['action'] == 'ESTORNARXXXXXXXXXXX'){
				$user->getAcao($tabela, 'estornar');

				$pdo = $connection->prepare("UPDATE vendas SET estornado='S', data_estorno='{$data_hora_atual}', estornado_por = '$user_id' WHERE  id = ?");
				$pdo->execute(array(
					$_POST['id']
				));

				//verifica o nro do caixa em que o pagamento foi efetuado
				$sql2 = "SELECT CI.caixa_nro, CI.valor, CI.forma_pagto, CI.tipo_venda, CI.cod_cliente,
				CASE WHEN
					C.tipo_cliente = 'J'
					THEN
						CASE WHEN C.nome_fantasia != ''
							THEN 
								CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')')
							ELSE
								UPPER(C.razao_social)
						END
					ELSE
						UPPER(C.nome_completo)
				END AS nome_cliente FROM caixa_itens AS CI LEFT JOIN clientes AS C ON (CI.cod_cliente=C.cod_cliente) WHERE CI.tipo_lancamento = '2' AND CI.id_tipo = ?";
				$pdo2 = $connection->prepare($sql2);
				$pdo2->execute(array(
					$_POST['id']
				));
				$query2 = $pdo2->fetch(PDO::FETCH_OBJ);
				$cx_pagamento = $query2->caixa_nro;

				if ($caixa_atual == $cx_pagamento) {
					$pdo3 = $connection->prepare("UPDATE caixa_itens SET cancelado='S', data_cancelamento='{$data_hora_atual}', cancelado_por='{$user_id}' WHERE  tipo_lancamento = '2' AND id_tipo = ?");
					$pdo3->execute(array(
						$_POST['id']
					));
				}
				else {
					//lanca como saída/devolução do caixa atual
					$texto = 'ESTORNO DE PAGAMENTO DO CAIXA: '.$cx_pagamento.' / CLIENTE: '.$query2->cod_cliente.' - '.$query2->nome_cliente;
					$sql4 = "INSERT INTO caixa_itens 
					(
						caixa_nro,
						data_hora,
						tipo_lancamento,
						cadastrado_por,
						id_tipo,
						valor,
						cancelado,
						forma_pagto,
						tipo_venda,
						cod_cliente,
						observacoes
					) 
				VALUES 
					(
						'{$caixa_atual}', '{$data_hora_atual}', 4, '{$user_id}', 0, '-{$query2->valor}', 'N', '{$query2->forma_pagto}', '{$query2->tipo_venda}','{$query2->cod_cliente}', '{$texto}'
					)";
					$pdo4 = $connection->prepare($sql4);
					$pdo4->execute();
				}

				echo json_encode(array('success'=>true, 'msg'=>'Pagmento Estornado com Sucesso!'));
			}
			else{
				throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
			}
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>