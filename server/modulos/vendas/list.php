<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$buscar2 = new Buscar();
		$tabela = 'vendas';
		$filtro2 = "";

		if(isset($_POST['action']) AND $_POST['action'] == 'LISTA_VENDAS_EM_ABERTO'){
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();

			if(isset($_POST['cliente'])){
				//usando alias
				$buscar->setBusca(array('id_cliente', 'id_cliente'), $_POST['cliente']);
				$buscar2->setBusca(array('id_cliente', 'id_cliente'), $_POST['cliente']);
				$buscar2->setBusca(array('saldo_a_receber', 'saldo_a_receber'), '0.00', 'maior');
			}

			if ($_POST['orcamento_venda'] == 'S') {
				//É venda
				//usuando alias
				$buscar->setBusca(array('eh_venda', 'eh_venda'), 'S');
				$buscar2->setBusca(array('eh_venda', 'eh_venda'), 'S');
			}
			else {
				//É orcamento
				$buscar->setBusca(array('eh_venda', 'eh_venda'), 'N');
				$buscar2->setBusca(array('eh_venda', 'eh_venda'), 'N');
			}

			$buscar->setBusca(array('usuario_salvou', 'usuario_salvou'), '1');
			$buscar2->setBusca(array('usuario_salvou', 'usuario_salvou'), '1');

			$buscar->setBusca(array('venda_finalizada', 'venda_finalizada'), 'N');
			$buscar2->setBusca(array('venda_finalizada', 'venda_finalizada'), 'N');

			$buscar->setBusca(array('cancelado', 'cancelado'), 'N');
			$buscar2->setBusca(array('cancelado', 'cancelado'), 'N');

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();
			$sql2 = "SELECT O.id_cliente AS id_cliente, O.convertido_venda, O.eh_venda AS eh_venda, O.usuario_salvou AS usuario_salvou, (O.valor_total - (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) - (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)) AS saldo_a_receber, COUNT(*) AS total FROM orcamentos AS O {$filtro}";
			$pdo = $connection->prepare($sql2);
			$pdo->execute($buscar->getArrayExecute());

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$filtro2 = $buscar2->getSql2();

			$sql3 = "SELECT O.id_orcamento, 
				O.id_cliente AS id_cliente, 
				O.convertido_venda, 
				O.eh_venda AS eh_venda, 
				O.usuario_salvou AS usuario_salvou, 
				O.validade_orcamento, 
				O.tipo_orcamento, 
				O.id_orcamentista, 
				UPPER(O.cliente_descricao) AS cliente_descricao, 
				O.data_cadastro, 
				O.data_venda, 
				CASE WHEN 
					C.tipo_cliente = 'J' 
					THEN 
						CASE WHEN C.nome_fantasia != '' 
							THEN 
								CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')') 
							ELSE 
								UPPER(C.razao_social) 
						END 
					ELSE 
						UPPER(C.nome_completo) 
				END AS cliente,
				O.desconto_porcento, 
				O.desconto_real, 
				O.acrescimo_porcento, 
				O.acrescimo_real, 
				O.valor_total, 
				O.valor_bruto, 
				(SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento) AS total_pago, 
				(SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) AS total_adiantado, 
				(O.valor_total 
				- (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) 
				- (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)
				) AS saldo_a_receber, 
				O.venda_finalizada, 
				O.venda_cod_orcamento, 
				O.cancelado, 
				CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', O.nro_end) AS nome_rua, 
				UPPER(O.complemento) AS endereco_complemento, 
				UPPER(CB.bairro_nome) AS nome_bairro, 
				UPPER(O.loteamento) AS endereco_loteamento, 
				UPPER(CL.loc_nome_abreviado) AS loc_nome_abreviado, 
				UPPER(O.aos_cuidados_de) AS aos_cuidados_de, 
				UPPER(O.observacoes) AS observacoes 
				FROM orcamentos AS O LEFT JOIN clientes AS C ON (O.id_cliente=C.cod_cliente) 
				LEFT JOIN correios_localidades AS CL ON (O.cidade=CL.id) 
				LEFT JOIN correios_enderecos AS CE ON (CE.id=O.endereco) 
				LEFT JOIN correios_bairros AS CB ON (O.bairro=CB.id) 
				{$filtro2} ORDER BY {$sort} {$order} LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql3);
			$pdo->execute($buscar2->getArrayExecute());

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$endereco_completo = "";
			foreach ($query as $key => $value) {
				$rows[$key] = $value;

				if($query[$key]->nome_rua != "") {
					$endereco_completo .= $rows[$key]->nome_rua;
				}

				if($query[$key]->endereco_complemento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_complemento.")";
				}

				if($query[$key]->nome_bairro != "") {
					$endereco_completo .= " - ".$rows[$key]->nome_bairro;
				}

				if($query[$key]->endereco_loteamento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_loteamento.")";
				}

				if ($endereco_completo != "") {
					if($query[$key]->loc_nome_abreviado != "") {
						$endereco_completo .= " / ".$rows[$key]->loc_nome_abreviado;
					}
				}

				if ($query[$key]->cliente != "") {
					$query[$key]->cliente = "<b>".$query[$key]->cliente."</b><br><i>".$endereco_completo."</i>";
				}
				else {
					$query[$key]->cliente = "<i>".$endereco_completo."</i>";
				}

				
				$endereco_completo = "";
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			$sql3 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql3);
			echo json_encode($result);
			//echo json_encode( array('dados'=>$query, 'sql2'=>$sql2, 'sql3'=>$sql3) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();

			if(isset($_POST['cliente'])){
				$buscar->setBusca(array('id_cliente', 'O.id_cliente'), $_POST['cliente']);
			}

			$buscar->setBusca(array('eh_venda', 'O.eh_venda'), 'S');
			$buscar->setBusca(array('usuario_salvou', 'O.usuario_salvou'), '1');
			$buscar->setBusca(array('venda_finalizada', 'venda_finalizada'), 'N');
			$buscar->setBusca(array('cancelado', 'cancelado'), 'N');

			if(empty($_POST['cadastrado_por'])) {
				$buscar->setBusca(array('cadastrado_por', 'O.cadastrado_por'), $user_id);
			}
			elseif ($_POST['cadastrado_por'] != 9999) {
				$buscar->setBusca(array('cadastrado_por', 'O.cadastrado_por'), $_POST['cadastrado_por']);
			}
			else {
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				//$buscar->setBusca(array('data_venda', 'O.data_venda'), implode('-', array_reverse(explode('/', $_POST['data_venda_date'])))." ".$_POST['data_venda_time'], 'like');
				if ($_POST['cadastrado_por'] != 9999) {
					$buscar->setBusca(array('cadastrado_por', 'O.cadastrado_por'), $_POST['cadastrado_por']);
				}
				$buscar->setBusca(array('id_vendedor', 'O.id_vendedor'), $_POST['id_vendedor']);
				$buscar->setBusca(array('cliente_descricao', 'O.cliente_descricao'), $_POST['cliente_descricao'], 'like');
				$buscar->setBusca(array('estado', 'O.estado'), $_POST['estado']);
				$buscar->setBusca(array('cidade', 'O.cidade'), $_POST['cidade']);
				$buscar->setBusca(array('endereco', 'O.endereco'), $_POST['endereco']);
				$filtro = $buscar->filtro.' ('.implode($buscar->sql, ' AND ').') ';

				if (!empty($_POST['data_venda_date_inicio']) OR !empty($_POST['data_venda_date_fim'])) {
					$data1 = implode('-', array_reverse(explode('/', $_POST['data_venda_date_inicio'])))." ".$_POST['data_venda_time_inicio']; 

					$data2 = implode('-', array_reverse(explode('/', $_POST['data_venda_date_fim'])))." ".$_POST['data_venda_time_fim']; 

					$filtro2 = "AND (O.data_venda BETWEEN '$data1' AND '$data2')";
				}

			}
			

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql2 = "SELECT O.id_cliente AS id_cliente, O.convertido_venda, O.eh_venda AS eh_venda, O.usuario_salvou AS usuario_salvou, (O.valor_total - (SELECT COALESCE(SUM(valor_vinculado), 0) FROM adiantamentos_uso WHERE cancelado='N' AND cod_venda=O.id_orcamento) - (SELECT COALESCE(SUM(valor_pago), 0) FROM vendas_pagamentos WHERE estornado='N' AND cod_venda=O.id_orcamento)) AS saldo_a_receber, COUNT(*) AS total FROM orcamentos AS O {$filtro} {$filtro2}";
			$pdo = $connection->prepare($sql2);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql3 = "SELECT 
				O.id_orcamento, 
				O.id_cliente AS id_cliente, 
				O.convertido_venda, 
				O.eh_venda AS eh_venda, 
				O.usuario_salvou AS usuario_salvou, 
				O.validade_orcamento, 
				O.tipo_orcamento, 
				O.id_orcamentista, 
				UPPER(O.cliente_descricao) AS cliente_descricao, 
				O.data_cadastro, 
				O.data_venda, 
				CASE WHEN 
					C.tipo_cliente = 'J' 
					THEN 
						CASE WHEN C.nome_fantasia != '' 
							THEN 
								CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')') 
							ELSE 
								UPPER(C.razao_social) 
						END 
					ELSE 
						UPPER(C.nome_completo) 
				END AS cliente,
				O.desconto_porcento, 
				O.desconto_real, 
				O.acrescimo_porcento, 
				O.acrescimo_real, 
				O.valor_total, 
				O.valor_bruto, 
				O.venda_finalizada, 
				O.venda_cod_orcamento, 
				COALESCE(vendas_pagamentos.total_pago,0) AS total_pago, 
				COALESCE(adiantamentos.total_adiantado,0) AS total_adiantado, 
				(COALESCE(O.valor_total,0) - COALESCE(adiantamentos.total_adiantado,0)  - COALESCE(vendas_pagamentos.total_pago,0)) AS saldo_a_receber, 
				IF (COALESCE(O.valor_total,0) - COALESCE(adiantamentos.total_adiantado,0) - COALESCE(vendas_pagamentos.total_pago,0) > 0,1,0) AS ordenacao, 
				CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', O.nro_end) AS nome_rua, 
				UPPER(O.complemento) AS endereco_complemento, 
				UPPER(CB.bairro_nome) AS nome_bairro, 
				UPPER(O.loteamento) AS endereco_loteamento, 
				UPPER(CL.loc_nome_abreviado) AS loc_nome_abreviado, 
				UPPER(O.aos_cuidados_de) AS aos_cuidados_de, 
				UPPER(O.observacoes) AS observacoes 
				FROM orcamentos AS O 
				LEFT JOIN clientes AS C ON (C.cod_cliente=O.id_cliente) 
				LEFT JOIN correios_localidades AS CL ON (CL.id=O.cidade) 
				LEFT JOIN correios_enderecos AS CE ON (CE.id=O.endereco) 
				LEFT JOIN correios_bairros AS CB ON (CB.id=O.bairro) 
				LEFT JOIN (SELECT COALESCE(SUM(valor_pago), 0) AS total_pago, cod_venda FROM vendas_pagamentos WHERE estornado='N' GROUP BY cod_venda) AS vendas_pagamentos ON vendas_pagamentos.cod_venda = O.id_orcamento 
				LEFT JOIN (SELECT COALESCE(SUM(valor_vinculado), 0) AS total_adiantado, cod_venda  FROM adiantamentos_uso WHERE cancelado='N' GROUP BY cod_venda) adiantamentos ON adiantamentos.cod_venda = O.id_orcamento 
				{$filtro} {$filtro2} ORDER BY ordenacao DESC, O.data_venda DESC LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql3);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$endereco_completo = "";
			foreach ($query as $key => $value) {
				$rows[$key] = $value;

				if($query[$key]->nome_rua != "") {
					$endereco_completo .= $rows[$key]->nome_rua;
				}

				if($query[$key]->endereco_complemento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_complemento.")";
				}

				if($query[$key]->nome_bairro != "") {
					$endereco_completo .= " - ".$rows[$key]->nome_bairro;
				}

				if($query[$key]->endereco_loteamento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_loteamento.")";
				}

				if ($endereco_completo != "") {
					if($query[$key]->loc_nome_abreviado != "") {
						$endereco_completo .= " / ".$rows[$key]->loc_nome_abreviado;
					}
				}

				if ($query[$key]->cliente != "") {
					$query[$key]->cliente = "<b>".$query[$key]->cliente."</b><br><i>".$endereco_completo."</i>";
				}
				else {
					$query[$key]->cliente = "<i>".$endereco_completo."</i>";
				}
				
				$endereco_completo = "";
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			$sql3 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql3);
			echo json_encode(array('dados'=>$query, 'total'=>$countRow, 'sql2'=>$sql2, 'sql3'=>$sql3));
			//echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>