<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'vendas';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		//verfifica se tem caixa aberto
		$sql = "SELECT caixa_atual FROM usuarios WHERE id = '{$user_id}'";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetch(PDO::FETCH_OBJ);
		$caixa_atual = $query->caixa_atual;

		if ($caixa_atual == 0){
			echo json_encode(array('success'=>false, 'msg'=>'Você deve Primeiro Abrir o Caixa para poder receber valores', 'erro'=>'Precia Abrir o Caixa Primeiro'));
		}
		else{
			//verifica se a forma de pagto é a vista ou a prazo
			$sql = "SELECT tipo FROM formas_pagto WHERE id = '{$_POST['forma_pagto']}'";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$vista_prazo = $query->tipo;

			if($_POST['action'] == 'EDITARXXXXXXXXXXXXXX'){
				//$user->getAcao($tabela, 'editar');
				$pdo = $connection->prepare("
						UPDATE vendas SET 
								forma_pagto = ?, 
								valor_pago = ?, 
								observacao = ?, 
								alterado_por = ?, 
								data_alteracao = ? 
	 					WHERE id = ?
				");
				$params = array(
					$_POST['forma_pagto'],
					$_POST['valor_pago'],
					mb_strtoupper($_POST['observacao'], 'UTF-8'),
					$user_id,
					date('Y-m-d H:i:s'),
					$_POST['id']
				);
				$pdo->execute($params);
			}
			elseif ($_POST['action'] == 'INSERIRXXXXXXXXXXXXX'){
				//$user->getAcao($tabela, 'adicionar');
				$pdo = $connection->prepare("
				INSERT INTO vendas 
					(
						cod_venda, 
						forma_pagto, 
						valor_pago, 
						observacao, 
						cadastrado_por, 
						alterado_por, 
						data_cadastro, 
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?
					)
				");
				$params = array(
					$_POST['cod_venda'],
					$_POST['forma_pagto'],
					$_POST['valor_pago'],
					mb_strtoupper($_POST['observacao'], 'UTF-8'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'), 
					date('Y-m-d H:i:s')
				);
				$pdo->execute($params);
				$ultimo_id = $connection->lastInsertId();

				//registra o adiantamento no caixa
				$pdo = $connection->prepare("
				INSERT INTO caixa_itens 
					(
						caixa_nro,
						data_hora,
						tipo_lancamento,
						cadastrado_por,
						id_tipo,
						valor,
						cancelado,
						forma_pagto,
						tipo_venda,
						cod_cliente,
						observacoes
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
				");
				$params = array(
					$caixa_atual,
					$data_hora_atual,
					2,
					$user_id,
					$ultimo_id,
					$_POST['valor_pago'],
					'N',
					$_POST['forma_pagto'],
					$vista_prazo,
					$_POST['cliente'],
					mb_strtoupper($_POST['observacao'], 'UTF-8')
				);
				$pdo->execute($params);
			}
			else{
				throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
			}
			echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>