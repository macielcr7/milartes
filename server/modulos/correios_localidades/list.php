﻿<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'correios_localidades';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_localidades
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if (isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO') {
			$buscar->setBusca(array('uf_sigla', 'correios_localidades.uf_sigla'), $_POST['uf_sigla']);
			$filtro = $buscar->filtro.' correios_localidades.loc_tipo = "M" AND correios_localidades.ativo = "S" ';
			$filtro = $buscar->getSql();

			$sql = "SELECT id as id, loc_nome as descricao FROM correios_localidades {$filtro}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = mb_strtoupper($rows[$key]->descricao, UTF8);
			}
			echo json_encode( array('dados'=>$rows) );
		}
		else if (isset($_POST['action']) and isset($_POST['query']) and !empty($_POST['query'])) {
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$pdo = $connection->prepare("SELECT  
					correios_localidades.id,
					correios_localidades.uf_sigla, 
					CASE WHEN
						correios_localidades.loc_nome_abreviado != correios_localidades.loc_nome THEN CONCAT(UPPER(correios_localidades.loc_nome_abreviado), ' ', UPPER(correios_localidades.loc_nome))
					ELSE
						UPPER(correios_localidades.loc_nome)
					END AS loc_nome,
					correios_localidades.cep,
					correios_localidades.cod_ibge,
					correios_localidades.ativo,
					correios_localidades.data_cadastro,
					UPPER(usuarios.nome) AS cadastrado_por,
					DATE_FORMAT(correios_localidades.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(correios_localidades.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_localidades INNER JOIN usuarios ON (usuarios.id=correios_localidades.cadastrado_por)
				WHERE correios_localidades.uf_sigla = '$_POST[uf_sigla]' AND correios_localidades.loc_tipo = 'M' AND (correios_localidades.loc_nome_abreviado like '%$_POST[query]%' OR correios_localidades.loc_nome like '%$_POST[query]%') 
				ORDER BY {$sort} {$order} 
				LIMIT $_POST[start], $_POST[limit]
			");
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = $rows[$key]->descricao;
			}
			echo json_encode( array('dados'=>$rows) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			$buscar->setBusca(array('uf_sigla', 'correios_localidades.uf_sigla'), $_POST['uf_sigla']);

			if (isset($_POST['action']) AND $_POST['action'] == 'FILTER') {
				$buscar->setBusca(array('loc_nome_abreviado', 'correios_localidades.loc_nome_abreviado'), $_POST['loc_nome_abreviado'], 'like');
				$buscar->setBusca(array('loc_nome', 'correios_localidades.loc_nome'), $_POST['loc_nome'], 'like');
				$buscar->setBusca(array('cep', 'correios_localidades.cep'), $_POST['cep'], 'like');
				$buscar->setBusca(array('situacao_localidade', 'correios_localidades.situacao_localidade'), $_POST['situacao_localidade']);
				$buscar->setBusca(array('loc_tipo', 'correios_localidades.loc_tipo'), $_POST['loc_tipo'], 'like');
				$buscar->setBusca(array('subordinacao_id', 'correios_localidades.subordinacao_id'), $_POST['subordinacao_id']);
				$buscar->setBusca(array('cod_ibge', 'correios_localidades.cod_ibge'), $_POST['cod_ibge'], 'like');
				$buscar->setBusca(array('ativo', 'correios_localidades.ativo'), $_POST['ativo'], 'like');
				$buscar->setBusca(array('cadastrado_por', 'correios_localidades.cadastrado_por'), $_POST['cadastrado_por']);
				$buscar->setBusca(array('data_cadastro', 'correios_localidades.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('alterado_por', 'correios_localidades.alterado_por'), $_POST['alterado_por']);
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total 
				FROM correios_localidades INNER JOIN correios_estados ON
					(correios_localidades.uf_sigla=correios_estados.uf) 
				{$filtro} AND correios_localidades.loc_tipo = 'M'
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SELECT  
					correios_localidades.id,
					correios_localidades.uf_sigla, 
					CASE WHEN
						correios_localidades.loc_nome_abreviado != correios_localidades.loc_nome THEN CONCAT(UPPER(correios_localidades.loc_nome_abreviado), ' ', UPPER(correios_localidades.loc_nome))
					ELSE
						UPPER(correios_localidades.loc_nome)
					END AS loc_nome,
					correios_localidades.cep,
					correios_localidades.cod_ibge,
					correios_localidades.ativo,
					correios_localidades.data_cadastro,
					UPPER(usuarios.nome) AS cadastrado_por,
					DATE_FORMAT(correios_localidades.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(correios_localidades.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_localidades INNER JOIN usuarios ON (usuarios.id=correios_localidades.cadastrado_por)
				{$filtro} AND correios_localidades.loc_tipo = 'M' 
				ORDER BY {$sort} {$order}
				LIMIT {$start}, {$limit}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>