<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'correios_enderecos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT  
					id,
					UPPER(uf_sigla) AS uf_sigla,
					localidade_id,
					UPPER(tipo) AS tipo,
					UPPER(nome) AS nome,
					bairro_id_inicial,
					cep,
					UPPER(complemento) AS complemento,
					UPPER(loteamento) AS loteamento,
					UPPER(ponto_referencia) AS ponto_referencia,
					ativo,
					cadastrado_por,
					data_cadastro, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_enderecos
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_TIPO'){
			$buscar->setBusca(array('tipo', 'correios_enderecos.tipo'), $_POST['query'], 'like');
			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT tipo AS id, tipo AS descricao 
				FROM correios_enderecos
				{$filtro} 
				GROUP BY tipo
				ORDER BY descricao ASC
			");
			$pdo->execute($buscar->getArrayExecute());

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = $rows[$key]->descricao;
			}
			echo json_encode( array('dados'=>$rows) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			if(isset($_POST['query'])){
					$filtro = " CONCAT(correios_enderecos.tipo, correios_enderecos.nome) LIKE '%{$_POST['query']}%' ";
			}
			else{
				if(isset($_POST['id']) and !empty($_POST['id'])){
					$filtro = "  correios_enderecos.id = {$_POST['id']} ";
					$offset = 'LIMIT 1';
				}
				else{
					if(isset($_POST['bairro_btwn']) and !empty($_POST['bairro_btwn'])){
						$filtro = " (correios_enderecos.bairro_id_inicial = {$_POST['bairro_btwn']} OR correios_enderecos.bairro_id_final = {$_POST['bairro_btwn']})";
					}			
					else{
						$filtro = ' 1 ';
					}
				}
			}
			
			if(isset($_POST['localidade_id'])){
				$filtro .= " AND correios_enderecos.localidade_id = {$_POST['localidade_id']} ";
			}

			$filtro .= " AND correios_enderecos.ativo = 'S' ";
			$sql = "SELECT correios_enderecos.id, correios_enderecos.nome, correios_enderecos.localidade_id, correios_enderecos.bairro_id_inicial, correios_enderecos.tipo, correios_enderecos.complemento, correios_enderecos.ponto_referencia, correios_enderecos.nome as descricao, correios_bairros.bairro_nome, correios_enderecos.loteamento, correios_enderecos.cep 
				FROM correios_enderecos INNER JOIN correios_bairros ON (correios_enderecos.bairro_id_inicial=correios_bairros.id)
				WHERE {$filtro} 
				ORDER BY correios_enderecos.tipo, correios_enderecos.nome 
				LIMIT 50";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = mb_strtoupper($rows[$key]->tipo, UTF8) . ' ' . mb_strtoupper($rows[$key]->descricao, UTF8) . " - " . mb_strtoupper($rows[$key]->bairro_nome, UTF8);
				if ($rows[$key]->loteamento != "") {
					$rows[$key]->descricao .= " (". mb_strtoupper($rows[$key]->loteamento, UTF8) .")";
				}
				if ($rows[$key]->complemento != "") {
					$rows[$key]->descricao .= " (". mb_strtoupper($rows[$key]->complemento, UTF8) .")";
				}
			}
			echo json_encode( array('dados'=>$rows, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_CEP'){
			$filtro = "correios_enderecos.localidade_id='{$_POST['localidade_id']}' AND correios_enderecos.bairro_id_inicial='{$_POST['bairro_id']}' AND correios_enderecos.nome like '{$_POST['endereco_nome']}' ";

			$sql = "
				SELECT correios_enderecos.cep as id, correios_enderecos.cep as descricao, correios_enderecos.complemento
				FROM correios_enderecos 
				WHERE {$filtro}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);

			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = mascara('##.###-###', $rows[$key]->descricao);
			}

			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['query']) AND !empty($_POST['query'])){
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$pdo = $connection->prepare("SELECT count(*) AS total 
				FROM correios_enderecos 
				WHERE correios_enderecos.uf_sigla='$_POST[uf_sigla]' AND correios_enderecos.localidade_id='$_POST[localidade_id]' AND correios_enderecos.bairro_id_inicial='$_POST[bairro_id_inicial]' AND correios_enderecos.nome like '%$_POST[query]%';
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql2 = "SELECT  
					correios_enderecos.id, 
					correios_enderecos.uf_sigla, 
					correios_enderecos.localidade_id, 
					UPPER(correios_enderecos.tipo) AS tipo, 
					UPPER(correios_enderecos.nome) AS nome, 
					correios_enderecos.bairro_id_inicial, 
					UPPER(correios_enderecos.complemento) AS complemento, 
					correios_enderecos.cep, 
					UPPER(correios_enderecos.loteamento) AS loteamento, 
					UPPER(correios_enderecos.ponto_referencia) AS ponto_referencia, 
					correios_enderecos.ativo, 
					correios_enderecos.data_cadastro, 
					UPPER(usuarios.nome) AS cadastrado_por,
					DATE_FORMAT(correios_enderecos.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(correios_enderecos.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_enderecos INNER JOIN usuarios ON (usuarios.id=correios_enderecos.cadastrado_por) 
				WHERE correios_enderecos.uf_sigla='$_POST[uf_sigla]' AND correios_enderecos.localidade_id='$_POST[localidade_id]' AND correios_enderecos.bairro_id_inicial='$_POST[bairro_id_inicial]' AND correios_enderecos.nome like '%$_POST[query]%' 
				ORDER BY {$sort} {$order}  
				LIMIT $_POST[start], $_POST[limit]";

			$pdo = $connection->prepare($sql2);
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = $rows[$key]->descricao;
			}
			
			$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			echo json_encode(array('dados'=>$rows, 'total'=>$countRow, 'sql2'=>$sql2));
			//echo json_encode( array('dados'=>$rows) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('localidade_id', 'correios_enderecos.localidade_id'), $_POST['localidade_id']);
				$buscar->setBusca(array('bairro_id_inicial', 'correios_enderecos.bairro_id_inicial'), $_POST['bairro_id_inicial']);
				$buscar->setBusca(array('tipo', 'correios_enderecos.tipo'), $_POST['tipo'], 'like');
				$buscar->setBusca(array('nome', 'correios_enderecos.nome'), $_POST['nome'], 'like');
				$buscar->setBusca(array('bairro_id_final', 'correios_enderecos.bairro_id_final'), $_POST['bairro_id_final']);
				$buscar->setBusca(array('cep', 'correios_enderecos.cep'), $_POST['cep'], 'like');
				$buscar->setBusca(array('complemento', 'correios_enderecos.complemento'), $_POST['complemento'], 'like');
				$buscar->setBusca(array('loteamento', 'correios_enderecos.loteamento'), $_POST['loteamento'], 'like');
				$buscar->setBusca(array('ponto_referencia', 'correios_enderecos.ponto_referencia'), $_POST['ponto_referencia'], 'like');
				$buscar->setBusca(array('ativo', 'correios_enderecos.ativo'), $_POST['ativo'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total 
				FROM correios_enderecos 
				WHERE uf_sigla='$_POST[uf_sigla]' AND localidade_id='$_POST[localidade_id]' AND bairro_id_inicial='$_POST[bairro_id_inicial]';
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql3 = "SELECT  
					correios_enderecos.id, 
					correios_enderecos.uf_sigla, 
					correios_enderecos.localidade_id, 
					UPPER(correios_enderecos.tipo) AS tipo, 
					UPPER(correios_enderecos.nome) AS nome, 
					correios_enderecos.bairro_id_inicial, 
					UPPER(correios_enderecos.complemento) AS complemento, 
					correios_enderecos.cep, 
					UPPER(correios_enderecos.loteamento) AS loteamento, 
					UPPER(correios_enderecos.ponto_referencia) AS ponto_referencia, 
					correios_enderecos.ativo, 
					correios_enderecos.data_cadastro, 
					UPPER(usuarios.nome) AS cadastrado_por,
					DATE_FORMAT(correios_enderecos.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(correios_enderecos.data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_enderecos INNER JOIN usuarios ON (usuarios.id=correios_enderecos.cadastrado_por) WHERE correios_enderecos.uf_sigla='$_POST[uf_sigla]' AND correios_enderecos.localidade_id='$_POST[localidade_id]' AND correios_enderecos.bairro_id_inicial='$_POST[bairro_id_inicial]'
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql3);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql3 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql3);
			$result["sql3"] = $sql3;
			//echo json_encode(array('dados'=>$query, 'total'=>$countRow, 'sql3'=>$sql3));
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>