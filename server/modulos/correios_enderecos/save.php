<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'correios_enderecos';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		
		if($_POST['action'] == 'EDITAR'){

			if ($_POST['cep'] == "__.___-___") {
				$cep = "";
			} else {
				$cep = $_POST['cep'];
			}

			$pdo = $connection->prepare("
					UPDATE correios_enderecos SET 
							uf_sigla = ?,							
							localidade_id = ?,							
							tipo = ?,							
							nome = ?,							
							bairro_id_inicial = ?,							
							bairro_id_final = ?,							
							cep = ?,							
							complemento = ?,							
							loteamento = ?,							
							ponto_referencia = ?,
							ativo = ?,							
							alterado_por = ?							
 					WHERE id = ?
			");
			$params = array(
				$_POST['uf_sigla'],
				$_POST['localidade_id'],
				$_POST['tipo'],
				$_POST['nome'],
				$_POST['bairro_id_inicial'],
				NULL,
				$cep,
				$_POST['complemento'],
				$_POST['loteamento'],
				$_POST['ponto_referencia'],
				$_POST['ativo'],
				$user_id,
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){

			if ($_POST['cep'] == "__.___-___") {
				$cep = "";
			} else {
				$cep = $_POST['cep'];
			}

			$pdo = $connection->prepare("
				INSERT INTO correios_enderecos 
					(
						uf_sigla,						
						localidade_id,						
						tipo,						
						nome,						
						bairro_id_inicial,						
						bairro_id_final,						
						cep,						
						complemento,						
						loteamento,						
						ponto_referencia,
						ativo,						
						cadastrado_por,						
						data_cadastro,						
						alterado_por						
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?			
					)
			");
			$params = array(
				$_POST['uf_sigla'],
				$_POST['localidade_id'],
				$_POST['tipo'],
				$_POST['nome'],
				$_POST['bairro_id_inicial'],
				NULL,
				$cep,
				$_POST['complemento'],
				$_POST['loteamento'],
				$_POST['ponto_referencia'],
				'S',
				$user_id,
				date('Y-m-d H:i:s'),
				$user_id
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA."<br><br>".$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}