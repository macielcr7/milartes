<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'caixas';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_abertura, '%H:%i:%s') as data_abertura_time, 
					DATE_FORMAT(data_abertura, '%Y-%m-%d') as data_abertura_date, 
					DATE_FORMAT(data_fechamento, '%H:%i:%s') as data_fechamento_time, 
					DATE_FORMAT(data_fechamento, '%Y-%m-%d') as data_fechamento_date 
				FROM caixas
				WHERE cx_nro=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
	
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('data_abertura', 'caixas.data_abertura'), implode('-', array_reverse(explode('/', $_POST['data_abertura_date'])))." ".$_POST['data_abertura_time'], 'like');
				$buscar->setBusca(array('usuario_abertura', 'caixas.usuario_abertura'), $_POST['usuario_abertura']);
				$buscar->setBusca(array('saldo_inicial', 'caixas.saldo_inicial'), $_POST['saldo_inicial'], 'like');
				$buscar->setBusca(array('obs_saldo_inicial', 'caixas.obs_saldo_inicial'), $_POST['obs_saldo_inicial'], 'like');
				$buscar->setBusca(array('data_fechamento', 'caixas.data_fechamento'), implode('-', array_reverse(explode('/', $_POST['data_fechamento_date'])))." ".$_POST['data_fechamento_time'], 'like');
				$buscar->setBusca(array('usuario_fechamento', 'caixas.usuario_fechamento'), $_POST['usuario_fechamento']);
				$buscar->setBusca(array('conferencia_saldo_final', 'caixas.conferencia_saldo_final'), $_POST['conferencia_saldo_final'], 'like');
				$buscar->setBusca(array('obs_saldo_final', 'caixas.obs_saldo_final'), $_POST['obs_saldo_final'], 'like');
				$buscar->setBusca(array('situacao', 'caixas.situacao'), $_POST['situacao'], 'like');
			}

			if (empty($_POST['filtrar_status'])) {
				$buscar->setBusca(array('situacao', 'caixas.situacao'), 'A');
				$filtrar_status = " AND caixas.situacao='A'";
			}
			elseif ($_POST['filtrar_status'] == 'X') {
				$buscar->setBusca(array('1'), '1');
				$filtrar_status = "";
			}
			else{
				$buscar->setBusca(array('situacao', 'caixas.situacao'), $_POST['filtrar_status']);
				$filtrar_status = " AND caixas.situacao='{$_POST['filtrar_status']}'";
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("
				SELECT count(*) AS total 
				FROM caixas INNER JOIN usuarios ON
					(caixas.usuario_abertura=usuarios.id) 
				{$filtro} {$filtrar_status}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT caixas.*, 
					DATE_FORMAT(caixas.data_abertura, '%H:%i:%s') as data_abertura_time, 
					DATE_FORMAT(caixas.data_abertura, '%Y-%m-%d') as data_abertura_date, 
					DATE_FORMAT(caixas.data_fechamento, '%H:%i:%s') as data_fechamento_time, 
					DATE_FORMAT(caixas.data_fechamento, '%Y-%m-%d') as data_fechamento_date, usuarios.nome as nome_user_abertura, U2.nome as nome_user_fechamento 
				FROM caixas INNER JOIN usuarios ON
					(caixas.usuario_abertura=usuarios.id) 
					INNER JOIN usuarios AS U2 ON
					(caixas.usuario_abertura=U2.id) 
				{$filtro} {$filtrar_status}
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>