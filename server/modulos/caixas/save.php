<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'caixas';
	$tabela2 = 'caixa_itens';

	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	if ($_POST['conferencia_saldo_final'] == "0,00"){
		$_POST['conferencia_saldo_final'] = null;
	}
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE caixas SET 
							saldo_inicial = ?,
							obs_saldo_inicial = ?,
							conferencia_saldo_final = ?,
							obs_saldo_final = ?,
							situacao = ?
					WHERE cx_nro = ?
			");
			$params = array(
				$_POST['saldo_inicial'],
				$_POST['obs_saldo_inicial'],
				$_POST['conferencia_saldo_final'],
				$_POST['obs_saldo_final'],
				$_POST['situacao'],
				$_POST['cx_nro']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'ABRIR'){
			$user->getAcao($tabela2, 'abrir_caixa');
			$pdo = $connection->prepare("
				INSERT INTO caixas 
					(
						data_abertura, 
						usuario_abertura, 
						saldo_inicial, 
						obs_saldo_inicial, 
						situacao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?
					)
			");
			$params = array(
				date('Y-m-d H:i:s'), 
				$user_id, 
				$_POST['saldo_inicial'], 
				mb_strtoupper($_POST['obs_saldo_inicial'], 'UTF-8'), 
				'A'
			);
			$pdo->execute($params);

			$novo_caixa = $connection->lastInsertId();
			$pdo = $connection->prepare("UPDATE usuarios SET caixa_atual='$novo_caixa' WHERE id='$user_id'");
			$pdo->execute();
		}
		else if($_POST['action'] == 'FECHAR'){
			$user->getAcao($tabela2, 'fechar_caixa');
			$pdo = $connection->prepare("
					UPDATE caixas SET 
							data_fechamento = ?, 
							usuario_fechamento = ?, 
							conferencia_saldo_final = ?, 
							obs_saldo_final = ?, 
							situacao = ? 
					WHERE cx_nro = ?
			");
			$params = array(
				date('Y-m-d H:i:s'), 
				$user_id, 
				$_POST['conferencia_saldo_final'],
				mb_strtoupper($_POST['obs_saldo_final'], 'UTF-8'), 
				'F', 
				$_POST['cx_nro']
			);
			$pdo->execute($params);

			$pdo = $connection->prepare("UPDATE usuarios SET caixa_atual='0' WHERE id='$user_id'");
			$pdo->execute();
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>