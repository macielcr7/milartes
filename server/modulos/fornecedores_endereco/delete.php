<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'fornecedores_endereco';
	try {
		if($_POST['action'] == 'DELETAR'){

			$data_hora_atual = date('Y-m-d H:i:s');
			$pdo = $connection->prepare("SELECT CE.controle, CE.estado, CE.num_end, CE.complemento, CE.loteamento, CE.cep, CE.cx_postal, CE2.tipo, CE2.nome, CB.bairro_nome, CL.loc_nome FROM fornecedores_endereco AS CE INNER JOIN correios_localidades AS CL ON (CL.id=CE.cidade) INNER JOIN correios_bairros AS CB ON (CB.id=CE.bairro) INNER JOIN correios_enderecos AS CE2 ON (CE2.id=CE.logradouro) WHERE CE.controle='$_POST[id]'");
			$pdo->execute();
	
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$descricao = "ID:".$linhas->controle."<br>Estado: ".$linhas->estado."<br>Cidade: ".$linhas->loc_nome."<br>Bairro: ".$linhas->bairro_nome."<br>Endereço: ".$linhas->tipo.", ".$linhas->nome.", ".$linhas->num_end."<br>Complemento: ".$linhas->complemento."<br>Loteamento: ".$linhas->loteamento."<br>CEP: ".$linhas->cep."<br>Cx. Postal: ".$linhas->cx_postal;
			$nome_tabela = utf8_encode('Clientes - Endere�o');
			$nome_campo = utf8_encode('Endere�o');
			$pdo = $connection->prepare("
					INSERT INTO logs_sistema (controle, chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
						VALUES 
					(NULL, md5('$data_hora_atual'), '$nome_tabela', '$_POST[id]', 'Excluiu', '$nome_campo', '$descricao', '', '$user_id', '$data_hora_atual')
			");
			$pdo->execute();


			$pdo = $connection->prepare("DELETE FROM fornecedores_endereco WHERE controle = ?");
			$pdo->execute(array(
				$_POST['id']
			));
			
			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}