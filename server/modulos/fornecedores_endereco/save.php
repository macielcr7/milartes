<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'fornecedores_endereco';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	
	try {
		
		if($_POST['action'] == 'EDITAR'){
		
			$pdo = $connection->prepare("
					UPDATE fornecedores_endereco SET 
							cod_fornecedor = ?,
							tipo_endereco = ?,
							estado = ?,
							cidade = ?,
							bairro = ?,
							logradouro = ?,
							num_end = ?,
							complemento = ?,
							loteamento = ?,
							ponto_referencia = ?,
							cep = ?,
							cx_postal = ?,
							alterado_por = ?
 					WHERE controle = ?
			");
			$params = array(
				$_POST['cod_fornecedor'],
				$_POST['tipo_endereco'],
				$_POST['estado'],
				$_POST['cidade'],
				$_POST['bairro'],
				$_POST['logradouro'],
				$_POST['num_end'],
				$_POST['complemento'],
				$_POST['loteamento'],
				$_POST['ponto_referencia'],
				$_POST['cep'],
				$_POST['cx_postal'],
				$user_id,
				$_POST['controle']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
		
			$pdo = $connection->prepare("
				INSERT INTO fornecedores_endereco 
					(
						cod_fornecedor,
						tipo_endereco,
						estado,
						cidade,
						bairro,
						logradouro,
						num_end,
						complemento,
						loteamento,
						ponto_referencia,
						cep,
						cx_postal,
						cadastrado_por,
						data_cadastro
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_fornecedor'],
				$_POST['tipo_endereco'],
				$_POST['estado'],
				$_POST['cidade'],
				$_POST['bairro'],
				$_POST['logradouro'],
				$_POST['num_end'],
				$_POST['complemento'],
				$_POST['loteamento'],
				$_POST['ponto_referencia'],
				$_POST['cep'],
				$_POST['cx_postal'],
				$user_id,
				date('Y-m-d H:i:s')
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}