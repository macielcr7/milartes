<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'fornecedores_endereco';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("
				SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM fornecedores_endereco
				WHERE controle=:id
			");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			$buscar->setBusca(array('cod_fornecedor', 'fornecedores_endereco.cod_fornecedor'), $_POST['cod_fornecedor']);
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('estado', 'fornecedores_endereco.estado'), $_POST['estado']);
				$buscar->setBusca(array('cidade', 'fornecedores_endereco.cidade'), $_POST['cidade']);
				$buscar->setBusca(array('bairro', 'fornecedores_endereco.bairro'), $_POST['bairro']);
				$buscar->setBusca(array('logradouro', 'fornecedores_endereco.logradouro'), $_POST['logradouro']);
				$buscar->setBusca(array('num_end', 'fornecedores_endereco.num_end'), $_POST['num_end'], 'like');
				$buscar->setBusca(array('complemento', 'fornecedores_endereco.complemento'), $_POST['complemento'], 'like');
				$buscar->setBusca(array('loteamento', 'fornecedores_endereco.loteamento'), $_POST['loteamento'], 'like');
				$buscar->setBusca(array('cep', 'fornecedores_endereco.cep'), $_POST['cep'], 'like');
				$buscar->setBusca(array('cx_postal', 'fornecedores_endereco.cx_postal'), $_POST['cx_postal'], 'like');
			}
			
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("
				SELECT count(*) as total 
				FROM fornecedores_endereco LEFT JOIN correios_estados ON
					(fornecedores_endereco.estado=correios_estados.uf) 
					LEFT JOIN correios_localidades ON
					(fornecedores_endereco.cidade=correios_localidades.id) LEFT JOIN correios_bairros ON
					(fornecedores_endereco.bairro=correios_bairros.id) LEFT JOIN correios_enderecos ON
					(fornecedores_endereco.logradouro=correios_enderecos.id) 
				{$filtro};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$pdo = $connection->prepare("
				SELECT fornecedores_endereco.controle, fornecedores_endereco.cep, fornecedores_endereco.cx_postal,
					UPPER(fornecedores_endereco.estado) AS estado, 
					UPPER(fornecedores_endereco.tipo_endereco) AS tipo_endereco, 
					UPPER(fornecedores_endereco.ponto_referencia) AS ponto_referencia, 
					fornecedores_endereco.cidade, fornecedores_endereco.bairro, fornecedores_endereco.logradouro, fornecedores_endereco.num_end, fornecedores_endereco.data_cadastro, 
						CASE WHEN
							fornecedores_endereco.complemento != '' THEN CONCAT(UPPER(correios_enderecos.tipo), ' ', UPPER(correios_enderecos.nome), ', ', num_end, ' (', UPPER(fornecedores_endereco.complemento), ')') 
						ELSE
							CONCAT(UPPER(correios_enderecos.tipo), ' ', UPPER(correios_enderecos.nome), ', ', num_end)
						END AS nome,
					CONCAT(UPPER(correios_localidades.loc_nome), ' / ', correios_estados.uf) AS loc_nome, 
						CASE WHEN
							fornecedores_endereco.loteamento != '' THEN CONCAT(UPPER(correios_bairros.bairro_nome), ' (', UPPER(fornecedores_endereco.loteamento), ')') 
						ELSE
							UPPER(correios_bairros.bairro_nome)
						END AS bairro_nome
				FROM fornecedores_endereco 
				LEFT JOIN correios_estados ON (fornecedores_endereco.estado=correios_estados.uf) 
				LEFT JOIN correios_localidades ON (fornecedores_endereco.cidade=correios_localidades.id) 
				LEFT JOIN correios_bairros ON (fornecedores_endereco.bairro=correios_bairros.id) 
				LEFT JOIN correios_enderecos ON (fornecedores_endereco.logradouro=correios_enderecos.id) 

				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			
			$result["total"] = $countRow;
			$result["dados"] = $query;

			
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>