<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

	@session_start();
	header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Impressão de Recibo</title>
	<link rel="stylesheet" type="text/css" href="../../../resources/css/sandri.css"/>

	<style type="text/css">
	@font-face {
		font-family: FuturaBT;
		src: url('../../../resources/css/FUTURABI.ttf');
	}
	</style>
</head>
<body bgcolor="#FFFFFF">

<?php
if($_GET){
	require('../../autoLoad.php');
	$controle = $_GET['id'];
	$pdoX = $connection->prepare("SET lc_time_names = 'pt_BR'");
	$pdoX->execute();
	$sql = "SELECT R.id, R.recebido_de_nome, R.recebido_de_docto, R.data_recibo, R.valor_recibo, R.referente, E.razao_social, E.cnpj, E.cidade, E.estado FROM recibos AS R INNER JOIN empresas_sistema AS E ON (E.cod_empresa=R.cod_empresa) WHERE R.id='$controle'";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetch(PDO::FETCH_OBJ);

	$recebido_de_nome = mb_strtoupper($query->recebido_de_nome, 'UTF-8');
	$monta_emitente = mb_strtoupper($query->razao_social, 'UTF-8');
	$monta_valor_por_extenso = valorPorExtenso($query->valor_recibo);
	$monta_referente = mb_strtoupper($query->referente, 'UTF-8');
	$valor_linha1 = "";
	$valor_linha2 = "";
	$referente_linha1 = "";
	$referente_linha2 = "";

	if (strlen($recebido_de_nome) > 41){
		$recebido_de_nome2 = substr($recebido_de_nome, 0, 41) . '...';
	}
	else{
		$recebido_de_nome2 = $recebido_de_nome;
	}

	if (!empty($query->recebido_de_docto)){
		$monta_recebido = $recebido_de_nome2. " (". $query->recebido_de_docto .")";
	}
	else{
		$monta_recebido = $recebido_de_nome2;
	}

	if (strlen($monta_valor_por_extenso) > 56){
		$valor_linha1 = substr($monta_valor_por_extenso, 0, 56) . '';
		$valor_linha2 = substr($monta_valor_por_extenso, 56, 67) . '';
	}
	else{
		$valor_linha1 = $monta_valor_por_extenso;
	}

	if (strlen($monta_referente) > 56)
	{
		$referente_linha1 = substr($monta_referente, 0, 56) . '';
		$referente_linha2 = substr($monta_referente, 56, 67) . '';
	}
	else{
		$referente_linha1 = $monta_referente;
	}

	if (!empty($query->cnpj)){
		$monta_emitente = $query->razao_social. " (". formatar($query->cnpj, 'cnpj') .")";
	}

	if (strlen($query->razao_social) > 40){
		$emitente_nome2 = substr($query->razao_social, 0, 40) . '...';
		$monta_emitente = $query->emitente_nome2 ." (". formatar($query->cnpj, 'cnpj') .")";
	}


	$copias = $_GET['copias'];
	for ($i = 1; $i <= $copias; $i++)
	{
		if ($i == 1) { $altura = 8; } else { $altura = 515; }

		if ($_COOKIE['id_empresa_atual'] == "1") { echo "<img border=\"0\" src=\"../../../resources/images/recibo_itajai.jpg\" width=\"719\" height=\"377\">"; }
		elseif ($_COOKIE['id_empresa_atual'] == "2") { echo "<img border=\"0\" src=\"../../../resources/images/recibo_camboriu.jpg\" width=\"719\" height=\"377\">"; }
		elseif ($_COOKIE['id_empresa_atual'] == "3") { echo "<img border=\"0\" src=\"../../../resources/images/recibo_anderson.gif\" width=\"719\" height=\"377\">"; }
		elseif ($_COOKIE['id_empresa_atual'] == "4") { echo "<img border=\"0\" src=\"../../../resources/images/recibo_ilhota.gif\" width=\"719\" height=\"377\">"; }
?>
<div id="recibo1" style="position: absolute; z-index: 1; top:<?php echo $altura; ?>px; width:719px; height:377px">
	<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000" id="table1">
		<tr>
			<td height="40" colspan="13">
				<table border="0" width="97%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="right"><font face="Verdana" size="4"><b><?php if ($i==1) { echo "Via Cliente";} else { echo "Via Loja";} ?></b></font></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="40" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="right">&nbsp;</td>
						<td align="left" width="285"><b><font face="Verdana" size="5">RECIBO</font><font face="Verdana" size="4"> R$ <?php echo number_format($query->valor_recibo, 2, ',', '.'); ?></font></b></td>
						<td align="center" width="110"><font face="Verdana" size="4"><b><?php echo str_pad($query->id, 5, "0", STR_PAD_LEFT); ?></b></font></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="70" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="right">&nbsp;</td>
						<td align="left" width="600"><b><font face="Arial"><?php echo strtoupper($monta_recebido); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="44" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="right">&nbsp;</td>
						<td align="left" width="600"><b><font face="Arial"><?php echo strtoupper($valor_linha1); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="35" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font face="Arial"><?php echo strtoupper($valor_linha2); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="28" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="right">&nbsp;</td>
						<td align="left" width="600"><b><font face="Arial"><?php echo strtoupper($referente_linha1); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td height="28" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font face="Arial"><?php echo strtoupper($referente_linha2); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="28" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font face="Arial">Emitente: <?php echo strtoupper($monta_emitente); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="42" colspan="13" valign="bottom">
				<table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" bordercolor="#000000">
					<tr>
						<td align="right"><font face="Arial"><b><?php echo mb_strtoupper($empresa_cidade, 'UTF-8'); ?></b></font></td>
						<td width="50" align="center">&nbsp;&nbsp;&nbsp;<b><font face="Arial"><?php echo strtoupper(data_extenso_dia($query->data_recibo)); ?></font></b></td>
						<td width="26">&nbsp;</td>
						<td width="150" align="center"><b><font face="Arial"><?php echo strtoupper(data_extenso_mes($query->data_recibo)); ?></font></b></td>
						<td width="40">&nbsp;</td>
						<td width="70" align="left"><b><font face="Arial"><?php echo strtoupper(data_extenso_ano($query->data_recibo)); ?></font></b></td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
</div>
<br><br><br><br><br><br><br><br>
<?php
	}
}
?>
</body>
</html>