<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'recibos';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if ($_POST['data_recibo'] == "__/__/____" OR $_POST['data_recibo'] == "") {
			$data_recibo = "0000-00-00";
		} else {
			$data_recibo = implode('-', array_reverse(explode('/', $_POST['data_recibo'])));
		}

		$valor_recibo = $_POST['valor_recibo'];
		$valor_recibo = str_replace('.','',$valor_recibo);
		$valor_recibo = str_replace(',','.',$valor_recibo);
		if ($valor_recibo == "") { $valor_recibo = 0.00; }

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE recibos SET 
				cod_empresa = ?, 
				recebido_de_nome = ?, 
				recebido_de_docto = ?, 
				valor_recibo = ?, 
				referente = ?, 
				data_recibo = ?, 
				alterado_por = ? 
				WHERE id = ?
			");
			$params = array(
				$_COOKIE['id_empresa_atual'], 
				$_POST['recebido_de_nome'], 
				$_POST['recebido_de_docto'], 
				$valor_recibo, 
				$_POST['referente'], 
				$data_recibo, 
				$user_id, 
				$_POST['id']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO recibos 
					(
						cod_empresa, 
						recebido_de_nome, 
						recebido_de_docto, 
						valor_recibo, 
						referente, 
						data_recibo, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_COOKIE['id_empresa_atual'], 
				$_POST['recebido_de_nome'], 
				$_POST['recebido_de_docto'], 
				$valor_recibo, 
				$_POST['referente'], 
				$data_recibo, 
				date('Y-m-d H:i:s'), 
				$user_id, 
				$user_id
			);
		}

		$pdo->execute($params);

		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Recibo Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}