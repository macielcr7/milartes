<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
if($_GET){
	try {
		require('../../autoLoad.php');
		require('../../lib/pdf/pdf.php');
		$tabela = 'recibos';
		
		$sort = 'id';
		$order = 'ASC';
		
		if (isset($_GET['sort'])){
			$sortJson = json_decode( $_GET['sort'] );
			$sort = trim(rtrim(addslashes($sortJson[0]->property )));
			$order = trim(rtrim(addslashes( $sortJson[0]->direction )));
		}
		
		
		$pdo = $connection->prepare("
			SELECT R.id, 
				R.recebido_de_nome, 
				R.referente, 
				R.emitente_nome, 
				DATE_FORMAT(R.data_recibo, '%d/%m/%Y') AS data_recibo, 
				R.valor_recibo, 
				UPPER(U.nome) AS cadastrado_por_nome 
				FROM recibos AS R INNER JOIN usuarios AS U ON (U.id=R.cadastrado_por) 
				WHERE R.id = {$_GET['id']}
		");
		$pdo->execute();
		
		$query = $pdo->fetch(PDO::FETCH_OBJ);
		
		class pdf extends FPDF_EXTENDED {
			public $width = 31.666666666667;
			public $total = 190;
			public $orientation = 'P';
			public $totalLine = 2;
			public $currentLine = 0;
			public $fillCollor = '255,255,255';
			public $textCollor = '0,0,0';
			public $columns = array();
			
			function pdf($orientation){
				parent::__construct($orientation);
				$this->orientation = $orientation;
				if($orientation=='L'){
					$this->totalLine = 3;
				}
			}
			
			function Via($dados){
				$this->Ln(10);
				$this->Image("../../../resources/images/logo.png",10);
				$this->SetFont('Arial','B',12);
				
				$this->Cell(190,6,utf8_decode("{$dados['numero_via']}º Via"),0,1,'R');
				$this->Cell(90,6,"CNPJ: {$dados['cnpj']}",0,0,'L');
				$this->Cell(50,6,"R$: {$dados['valor']}",0,0,'L');
				$this->Cell(50,6,utf8_decode("Nº: {$dados['codigo']}"),0,1,'L');

				$this->Cell($this->total,6,utf8_decode("Endereço: {$dados['endereco']}"),0,1,'L');
				$this->Cell($this->total,6,utf8_decode("Bairro: {$dados['bairro']}"),0,1,'L');
				$this->Cell($this->total,6,"Fones: {$dados['fones']}",0,1,'L');
				$this->Ln(7);
				$this->SetFont('Arial','B',9);

				$this->Cell($this->total,6,"Recebi(mos) de: {$dados['recebido_de_nome']}",'B',1,'L');
				$this->Ln(4);
				$this->MultiCell($this->total,6,utf8_decode("A importância de: {$dados['valor_nome']}"), 'B');
				$this->Ln(4);
				$this->MultiCell($this->total,6,utf8_decode("Correspondente a: {$dados['referente']}"), 'B');
				$this->Ln(10);

				$this->Cell(90,6,$dados['emitente_nome'],0,1,'L');
				$this->Cell(90,6,"Assinatura",'T',0,'C');

				$this->SetFont('Arial','B',12);
				$this->Cell(100,6,$dados['data_recibo'],0,0,'R');

				$this->Ln(10);				
			}
			
			function getFilter(){
				if(isset($_GET['action']) AND $_GET['action'] == 'FILTER'){
					$this->Cell($this->total,6, "Filtros Ultilizados:",'B',1,'C');
					$this->Ln(4);
					$this->getCampo($_GET['cod_cliente'], utf8_decode("Cod Cliente"));
					$this->getCampo($_GET['anotacao'], utf8_decode("AnotaÃ§Ã£o"));
					$this->getCampo($_GET['cadastrado_por'], utf8_decode("Cadastrado Por"));
					$this->getCampo($_GET['data_cadastro_date']." ".$_GET['data_cadastro_time'], "Data Cadastro");
					$this->getCampo($_GET['alterado_por'], utf8_decode("Alterado Por"));
					$this->getCampo($_GET['ativo'], utf8_decode("Ativo"));
				}
			}
			
			function getCampo($valor, $descricao){
				if(!empty($valor)){
					if($this->currentLine==$this->totalLine){
						$this->Ln(6);
						$this->currentLine = 0;
					}
					else{
						$this->currentLine++;
					}
					$this->Cell(($this->total/3),6, "$descricao: $valor",0,0,'L');
				}
			}
			
			function Footer(){
				$this->SetY(-15);
				$this->SetFont('Arial','I',8);
				$this->Cell(10,10,"","T"); $this->Cell(30,10,date("d/m/Y - H:i:s"),"T",0,'L');
				$this->Cell(0,10,'Página '.$this->PageNo(),"T",0,'R');
			}
		}
	
		$pdf = new pdf('P');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->Via(array(
			'numero_via'		=> 1,
			'endereco'			=> 'Novo Maranguape',
			'bairro'			=> 'Maranguape',
			'fones'				=> '(85) 8516-6042',
			'cnpj'				=> '99.999.999/9999-99',
			'valor'				=> @number_format($query->valor_recibo,2,',','.'),
			'codigo'			=> $query->id,
			'recebido_de_nome'	=> $query->recebido_de_nome,
			'valor_nome'		=> valorPorExtenso($query->valor_recibo),
			'referente'			=> $query->referente,
			'emitente_nome'		=> $query->emitente_nome,
			'data_recibo'		=> $query->data_recibo
		));

		$pdf->Cell($pdf->total,6,"",'B',1,'L');
		$pdf->Ln(10);

		$pdf->Via(array(
			'numero_via'		=> 2,
			'endereco'			=> 'Novo Maranguape',
			'bairro'			=> 'Maranguape',
			'fones'				=> '(85) 8516-6042',
			'cnpj'				=> '99.999.999/9999-99',
			'valor'				=> @number_format($query->valor_recibo,2,',','.'),
			'codigo'			=> $query->id,
			'recebido_de_nome'	=> $query->recebido_de_nome,
			'valor_nome'		=> valorPorExtenso($query->valor_recibo),
			'referente'			=> $query->referente,
			'emitente_nome'		=> $query->emitente_nome,
			'data_recibo'		=> $query->data_recibo
		));
		
		$pdf->Output();
	} 
	catch (PDOException $e) {
		echo $e->getMessage();
	}	
}

?>