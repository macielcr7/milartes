<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'recibos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT R.*, DATE_FORMAT(R.data_recibo, '%d/%m/%Y') AS data_recibo FROM recibos AS R WHERE R.id=:id"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'FAVORECIDO' ){
			$sql ="SELECT DISTINCT(recebido_de_docto) AS docto, UPPER(recebido_de_nome) AS id, UPPER(recebido_de_nome) AS descricao FROM recibos WHERE recebido_de_nome like '%$_POST[query]%' ORDER BY descricao ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";
			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where .= "WHERE R.recebido_de_nome like '%$_POST[query]%' OR R.referente like '%$_POST[query]%'";
			}
			/*elseif( isset($_POST['action']) AND $_POST['action'] == 'LIST' ){
				$where .= "WHERE R.cod_empresa='{$_COOKIE['id_empresa_atual']}'";
			}*/

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM recibos AS R $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);

			$countRow = $query->total;

			$sql = "SELECT R.id, 
				UPPER(R.recebido_de_nome) AS recebido_de_nome, 
				UPPER(R.referente) AS referente, 
				DATE_FORMAT(R.data_recibo, '%d/%m/%Y') AS data_recibo, 
				REPLACE(REPLACE(REPLACE(FORMAT(R.valor_recibo, 2), '.', '@'), ',', '.'), '@', ',') AS valor_recibo, 
				UPPER(U.nome) AS cadastrado_por_nome, 
				UPPER(ES.nome_fantasia) AS empresa_nome 
				FROM recibos AS R INNER JOIN usuarios AS U ON (U.id=R.cadastrado_por) INNER JOIN empresas_sistema AS ES ON (ES.cod_empresa=R.cod_empresa) 
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
			//echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}
}
?>