<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'atributos';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		
		if($_POST['action'] == 'EDITAR'){
		
			$user->getAcao($tabela, 'editar');
		
			$pdo = $connection->prepare("
					UPDATE atributos SET 
							atributo = ?, 
							sigla = ?, 
							ativo = ?, 
							alterado_por = ?, 
							data_alteracao = ? 
 					WHERE id_atributos = ?
			");
			$params = array(
				$_POST['atributo'],
				$_POST['sigla'],
				$_POST['ativo'],
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['id_atributos']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
		
			$user->getAcao($tabela, 'adicionar');
		
			$pdo = $connection->prepare("
				INSERT INTO atributos 
					(
						atributo,
						sigla,
						ativo,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['atributo'],
				$_POST['sigla'],
				$_POST['ativo'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				date('Y-m-d H:i:s')
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}