<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'itens_orcamento';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if($_POST['action'] == 'COPIA_ITEM_ORCAMENTO'){
			$orcamento_nro = $_POST['orcamento_nro'];
			//copia o orcamento
			$novo_item_nro = $_POST['nro_itens']+1;
			//$nro_item_selecionado = $_POST['nro_item_selecionado'];

			$pdo = $connection->prepare("INSERT INTO itens_orcamento SELECT NULL, 
					id_produto, 
					'{$novo_item_nro}', 
					id_cor, 
					quantidade, 
					valor_unitario, 
					id_orcamento, 
					valor_total, 
					data_cadastro, 
					cadastrado_por, 
					alterado_por, 
					data_alteracao, 
					observacoes 
				FROM itens_orcamento WHERE id_itens_orcamento = ?
			");
			$pdo->execute(array(
				$_POST['id_itens_orcamento']
			));

			//pega o novo ID do ítem
			$novo_id_item = $connection->lastInsertId();

			$pdoX3 = $connection->prepare("INSERT INTO orcamentos_observacoes SELECT NULL, 
					orcamento_id, 
					ambiente_id, 
					'{$novo_item_nro}', 
					descricao, 
					tipo, 
					'{$user_id}', 
					'{$user_id}', 
					'{$data_hora_atual}', 
					'{$data_hora_atual}'
				FROM orcamentos_observacoes WHERE orcamento_id = ? AND nro_item = ?
				");
			$pdoX3->execute(array(
				$_POST['orcamento_nro'],
				$_POST['nro_item_selecionado']
			));

			//gera os custos
			$pdo = $connection->prepare("INSERT INTO vendas_custos SELECT NULL, 
					cod_venda, 
					'{$novo_id_item}',
					cod_produto, 
					cod_cor, 
					quantidade, 
					valor_unitario, 
					acrescimo_mo, 
					acrescimo_produto, 
					valor_total,
					cod_instalador, 
					data_cadastro, 
					cadastrado_por, 
					alterado_por, 
					data_alteracao, 
					observacoes 
				FROM vendas_custos WHERE cod_item_venda = ?
			");
			$pdo->execute(array(
				$_POST['id_itens_orcamento']
			));

			echo json_encode(array('success'=>true, 'msg'=>$novo_id_item.' Ítem Copiado com Sucesso'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>'Erro...', 'erro'=>$e->getMessage()));
	}
}