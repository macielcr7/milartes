<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'itens_orcamento';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_atual = date('Y-m-d H:i:s');
	try {
		if(empty($_POST['quantidade'])) {
			$quantidade = 0;
		} else {
			$quantidade = implode('.', explode(',', $_POST['quantidade']));
		}

		if(empty($_POST['id_cor'])) {
			$_POST['id_cor'] = 0;
		}

		//pega os valores dos custos do produto
		$pdoX = $connection->prepare("SELECT tipo_produto, possui_composicao, preco_custo AS preco_custo_produto, preco_custo_servico, dobra_quantidade FROM produtos WHERE id = '{$_POST['id_produto']}'");
		$pdoX->execute();
		$custos = $pdoX->fetch(PDO::FETCH_OBJ);
		$preco_custo_produto = $custos->preco_custo_produto;
		//$preco_custo_servico = $custos->preco_custo_servico;

		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');

			//pega a observacao antiga
			$sqlE = "SELECT descricao FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND nro_item='{$_POST['item_nro']}'";
			$pdoE = $connection->prepare($sqlE);
			$pdoE->execute();
			$obs_antigo = $pdoE->fetchColumn();

			$nova_observacao = mb_strtoupper($_POST['observacoes'], 'UTF-8');
			$nova_observacao = ReformatString($nova_observacao);

			$pdo = $connection->prepare("
					UPDATE itens_orcamento SET 
							id_produto = ?, 
							id_cor = ?, 
							id_orcamento = ?, 
							quantidade = ?, 
							valor_unitario = ?, 
							valor_total = ?, 
							alterado_por = ?, 
							data_alteracao = ?, 
							observacoes = ?, 
							item_nro = ? 
 					WHERE id_itens_orcamento = ?
			");
			$params = array(
				$_POST['id_produto'],
				$_POST['id_cor'],
				$_POST['id_orcamento'],
				$quantidade,
				implode('.', explode(',', $_POST['valor_unitario'])),
				implode('.', explode(',', $_POST['valor_total'])),
				$user_id,
				date('Y-m-d H:i:s'),
				$nova_observacao,
				$_POST['item_nro'],
				$_POST['id_itens_orcamento']
			);
			$pdo->execute($params);

			//atualiza os custos
			$sql4 = "DELETE FROM vendas_custos WHERE cod_item_venda = '{$_POST['id_itens_orcamento']}'";
				$pdo4 = $connection->prepare($sql4);
				$pdo4->execute();

			if ($custos->possui_composicao == 'S') {
				$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo, (SELECT COUNT(*) FROM produtos_cores WHERE produtos_cores.id_produto = PC.id_composicao) AS qtd_cores 
					FROM produto_composicoes AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
					INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida) 
					WHERE PC.id_produto = '{$_POST['id_produto']}'";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();

				while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
				{
					//calcula a quantidade
					if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
						$nova_qtde = ceil($quantidade * $item['quantidade']);
					} else {
						$nova_qtde = ($quantidade * $item['quantidade']);
					}

					if ($item['qtd_cores'] > 0) {
						$cod_cor = $_POST['id_cor'];
					} else {
						$cod_cor = 0;
					}

					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$_POST['id_orcamento'],
						$_POST['id_itens_orcamento'],
						$item['id_composicao'],
						$cod_cor,
						$nova_qtde,
						$item['preco_custo'],
						0,
						0,
						($nova_qtde * $item['preco_custo']),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			} else {
				$pdo2 = $connection->prepare("
					INSERT INTO vendas_custos 
						(
							cod_venda, 
							cod_item_venda, 
							cod_produto, 
							cod_cor, 
							quantidade, 
							valor_unitario, 
							acrescimo_mo, 
							acrescimo_produto, 
							valor_total,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
				");
				$params2 = array(
					$_POST['id_orcamento'],
					$_POST['id_itens_orcamento'],
					$_POST['id_produto'],
					$_POST['id_cor'],
					$quantidade,
					$preco_custo_produto,
					0,
					0,
					($quantidade * $preco_custo_produto),
					date('Y-m-d H:i:s'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'),
					$observacoes
				);
				$pdo2->execute($params2);
			}


			//atualiza a observaçao
			//se a obs antiga for em branco e a nova tem algum valor.... insere
			if (empty($obs_antigo) AND !empty($nova_observacao)){
				$novo_texto_obs = $nova_observacao;
				$sqlD = "INSERT INTO orcamentos_observacoes 
					(
						orcamento_id, 
						ambiente_id, 
						nro_item, 
						descricao, 
						tipo, 
						cadastrado_por, 
						alterado_por, 
						data_cadastro, 
						data_alteracao
					) 
				VALUES 
					(
						'{$_POST['id_orcamento']}', 
						'0', 
						'{$_POST['item_nro']}', 
						'{$novo_texto_obs}', 
						'G', 
						'{$user_id}', 
						'{$user_id}', 
						'{$data_atual}', 
						'{$data_atual}'
					)";
				$pdoD = $connection->prepare($sqlD);
				$pdoD->execute();
			}

			//se a antiga tiver algum valor e for diferente da nova... update
			if (!empty($obs_antigo) AND ($obs_antigo != $nova_observacao)){
				$novo_texto_obs = $nova_observacao;
				$sqlD = "UPDATE orcamentos_observacoes SET descricao='{$novo_texto_obs}', alterado_por='{$user_id}', data_alteracao='{$data_atual}' WHERE orcamento_id='{$_POST['id_orcamento']}' AND nro_item='{$_POST['item_nro']}'";
				$pdoD = $connection->prepare($sqlD);
				$pdoD->execute();
			}

			//se a antiga tiver algum valor e a nova for em branco.... apaga
			if (!empty($obs_antigo) AND empty($nova_observacao)){
				$novo_texto_obs = $nova_observacao;
				$sqlD = "DELETE FROM orcamentos_observacoes WHERE orcamento_id='{$_POST['id_orcamento']}' AND nro_item='{$_POST['item_nro']}'";
				$pdoD = $connection->prepare($sqlD);
				$pdoD->execute();
			}
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');

			$observacoes = mb_strtoupper($_POST['observacoes'], 'UTF-8');
			$observacoes = ReformatString($observacoes);

			$sqlC = "SELECT count(*) FROM itens_orcamento WHERE id_orcamento='{$_POST['id_orcamento']}'";
			$pdoC = $connection->prepare($sqlC);
			$pdoC->execute();
			$num_rowsC = $pdoC->fetchColumn();

			if($num_rowsC == 0){
				$nro_item = 1;
			} else {
				$nro_item = $num_rowsC + 1;
			}

			$pdo = $connection->prepare("
				INSERT INTO itens_orcamento 
					(
						id_produto, 
						id_cor, 
						id_orcamento, 
						quantidade, 
						valor_unitario, 
						valor_total, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao, 
						observacoes, 
						item_nro
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['id_produto'],
				$_POST['id_cor'],
				$_POST['id_orcamento'],
				$quantidade,
				implode('.', explode(',', $_POST['valor_unitario'])),
				implode('.', explode(',', $_POST['valor_total'])),
				date('Y-m-d H:i:s'), 
				$user_id,
				$user_id,
				date('Y-m-d H:i:s'),
				$observacoes,
				$nro_item
			);
			$pdo->execute($params);
			$ultimo_id = $connection->lastInsertId();

			//gera os custos
			if ($custos->possui_composicao == 'S') {
				$sql3 = "SELECT PC.id_composicao, P.descricao_completa, PC.quantidade, UM.id AS unidade_medida, UM.sigla, P.preco_custo, (SELECT COUNT(*) FROM produtos_cores WHERE produtos_cores.id_produto = PC.id_composicao) AS qtd_cores 
					FROM produto_composicoes AS PC 
					INNER JOIN produtos AS P ON (P.id=PC.id_composicao) 
					INNER JOIN unidades_medidas AS UM ON (UM.id=P.unidade_medida)
					WHERE PC.id_produto = '{$_POST['id_produto']}'";
				$pdo3 = $connection->prepare($sql3);
				$pdo3->execute();

				while ($item = $pdo3->fetch(PDO::FETCH_ASSOC))
				{
					//calcula a quantidade
					if ($item['unidade_medida'] == 5 OR $item['unidade_medida'] == 8) {
						$nova_qtde = ceil($quantidade * $item['quantidade']);
					} else {
						$nova_qtde = ($quantidade * $item['quantidade']);
					}

					if ($item['qtd_cores'] > 0) {
						$cod_cor = $_POST['id_cor'];
					} else {
						$cod_cor = 0;
					}

					$pdo2 = $connection->prepare("
						INSERT INTO vendas_custos 
							(
								cod_venda, 
								cod_item_venda, 
								cod_produto, 
								cod_cor, 
								quantidade, 
								valor_unitario, 
								acrescimo_mo, 
								acrescimo_produto, 
								valor_total,
								data_cadastro, 
								cadastrado_por, 
								alterado_por, 
								data_alteracao, 
								observacoes
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
							)
					");
					$params2 = array(
						$_POST['id_orcamento'],
						$ultimo_id,
						$item['id_composicao'],
						$cod_cor,
						$nova_qtde,
						$item['preco_custo'],
						0,
						0,
						($nova_qtde * $item['preco_custo']),
						date('Y-m-d H:i:s'),
						$user_id,
						$user_id,
						date('Y-m-d H:i:s'),
						$observacoes
					);
					$pdo2->execute($params2);
				}
			} else {
				$pdo2 = $connection->prepare("
					INSERT INTO vendas_custos 
						(
							cod_venda, 
							cod_item_venda, 
							cod_produto, 
							cod_cor, 
							quantidade, 
							valor_unitario, 
							acrescimo_mo, 
							acrescimo_produto, 
							valor_total,
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
				");
				$params2 = array(
					$_POST['id_orcamento'],
					$ultimo_id,
					$_POST['id_produto'],
					$_POST['id_cor'],
					$quantidade,
					$preco_custo_produto,
					0,
					0,
					($quantidade * $preco_custo_produto),
					date('Y-m-d H:i:s'),
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'),
					$observacoes
				);
				$pdo2->execute($params2);
			}

			if (!empty($_POST['observacoes'])){
				$novo_texto_obs = $observacoes;
				$sqlD = "INSERT INTO orcamentos_observacoes 
					(
						orcamento_id, 
						ambiente_id, 
						nro_item, 
						descricao, 
						tipo, 
						cadastrado_por, 
						alterado_por, 
						data_cadastro, 
						data_alteracao
					) 
				VALUES 
					(
						'{$_POST['id_orcamento']}', 
						'0', 
						'{$nro_item}', 
						'{$novo_texto_obs}', 
						'G', 
						'{$user_id}', 
						'{$user_id}', 
						'{$data_atual}', 
						'{$data_atual}'
					)";
				$pdoD = $connection->prepare($sqlD);
				$pdoD->execute();
			}
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}