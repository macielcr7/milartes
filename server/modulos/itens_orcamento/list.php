<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'itens_orcamento';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT * 
				FROM itens_orcamento
				WHERE id_itens_orcamento=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			$buscar->setBusca(array('id_orcamento', 'itens_orcamento.id_orcamento'), $_POST['id_orcamento']);

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('quantidade', 'itens_orcamento.quantidade'), $_POST['quantidade']);
				$buscar->setBusca(array('valor_unitario', 'itens_orcamento.valor_unitario'), $_POST['valor_unitario'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) as total 
				FROM itens_orcamento {$filtro}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql2 = "SELECT itens_orcamento.*, UPPER(produtos.descricao_completa) as produto, 
				UPPER(unidades_medidas.sigla) AS sigla_um, 
				CONCAT(REPLACE(REPLACE(REPLACE(FORMAT(itens_orcamento.quantidade, 2), '.', '|'), ',', '.'), '|', ','), ' ', UPPER(unidades_medidas.sigla)) AS quantidade2, 
				cores.descricao as cor, 
				cores.codigo as css_cor 
				FROM itens_orcamento 
				INNER JOIN produtos ON (produtos.id=itens_orcamento.id_produto) 
				INNER JOIN unidades_medidas ON (unidades_medidas.id=produtos.unidade_medida) 
				LEFT  JOIN produtos_cores ON (itens_orcamento.id_cor=produtos_cores.id_produtos_cores) 
				LEFT  JOIN cores ON (produtos_cores.id_cor=cores.id_cores) 
				{$filtro} 
				ORDER BY itens_orcamento.id_itens_orcamento ASC 
				LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql2);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			//$sql2 = str_replace(array("\r", "\n", "\t", "\v"), '', $sql2);
			//echo json_encode( array('dados'=>$query, 'sql2'=>$sql2) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>