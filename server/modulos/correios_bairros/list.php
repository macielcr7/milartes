﻿<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'correios_bairros';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT *, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date 
				FROM correios_bairros
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$buscar->setBusca(array('localidade_id', 'correios_bairros.localidade_id'), $_POST['localidade_id']);
			$filtro = $buscar->getSql();

			$sql = "SELECT correios_bairros.id AS id, UPPER(correios_bairros.bairro_nome) AS descricao FROM correios_bairros {$filtro} AND correios_bairros.ativo='S' ORDER BY correios_bairros.bairro_nome ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = $rows[$key]->descricao;
			}
			//echo json_encode( array('dados'=>$rows) );

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$rows, 'sql'=>$sql) );
		}
		else if (isset($_POST['action']) and isset($_POST['query']) and !empty($_POST['query'])) {
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$pdo = $connection->prepare("SELECT  
					correios_bairros.id, 
					correios_bairros.uf_sigla, 
					correios_bairros.localidade_id, 
					UPPER(correios_bairros.bairro_nome) AS bairro_nome, 
					UPPER(correios_bairros.bairro_nome_abreviado) AS bairro_nome_abreviado, 
					correios_bairros.ativo, 
					correios_bairros.data_cadastro, 
					UPPER(usuarios.nome) AS cadastrado_por, 
					DATE_FORMAT(correios_bairros.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(correios_bairros.data_cadastro, '%Y-%m-%d') as data_cadastro_date
				FROM correios_bairros 
				INNER JOIN usuarios ON (usuarios.id=correios_bairros.cadastrado_por)
				WHERE correios_bairros.localidade_id = '$_POST[localidade_id]' AND correios_bairros.bairro_nome like '%$_POST[query]%'
				ORDER BY {$sort} {$order}
				LIMIT $_POST[start], $_POST[limit]
			");
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			$rows = array();
			foreach ($linhas as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = $rows[$key]->descricao;
			}
			echo json_encode( array('dados'=>$rows) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			$buscar->setBusca(array('uf_sigla', 'correios_bairros.uf_sigla'), $_POST['uf_sigla']);
			$buscar->setBusca(array('localidade_id', 'correios_bairros.localidade_id'), $_POST['localidade_id']);

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total 
				FROM correios_bairros INNER JOIN correios_localidades ON (correios_bairros.localidade_id=correios_localidades.id) 
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SELECT  
					correios_bairros.id, 
					correios_bairros.uf_sigla, 
					correios_bairros.localidade_id, 
					UPPER(correios_bairros.bairro_nome) AS bairro_nome, 
					UPPER(correios_bairros.bairro_nome_abreviado) AS bairro_nome_abreviado, 
					correios_bairros.ativo, 
					correios_bairros.data_cadastro, 
					UPPER(usuarios.nome) AS cadastrado_por, 
					DATE_FORMAT(correios_bairros.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(correios_bairros.data_cadastro, '%Y-%m-%d') as data_cadastro_date
				FROM correios_bairros 
				INNER JOIN usuarios ON (usuarios.id=correios_bairros.cadastrado_por)
				{$filtro} 
				ORDER BY {$sort} {$order}
				LIMIT {$start}, {$limit}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>