<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'correios_bairros';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		
		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
					UPDATE correios_bairros SET 
							uf_sigla = ?,							
							localidade_id = ?,							
							bairro_nome = ?,							
							bairro_nome_abreviado = ?,							
							ativo = ?,							
							alterado_por = ?							
 					WHERE id = ?
			");
			$params = array(
				$_POST['uf_sigla'],
				$_POST['localidade_id'],
				$_POST['bairro_nome'],
				$_POST['bairro_nome_abreviado'],
				$_POST['ativo'],
				$user_id,
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO correios_bairros 
					(
						uf_sigla,						
						localidade_id,						
						bairro_nome,						
						bairro_nome_abreviado,						
						ativo,						
						cadastrado_por,						
						data_cadastro,						
						alterado_por						
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?			
					)
			");
			$params = array(
				$_POST['uf_sigla'],		
				$_POST['localidade_id'],		
				$_POST['bairro_nome'],		
				$_POST['bairro_nome_abreviado'],		
				'S',		
				$user_id,		
				date('Y-m-d H:i:s'),
				$user_id		
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}