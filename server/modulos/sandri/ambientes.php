<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	try {
		if($_POST['action'] == 'LIST_COMBO_AMBIENTES'){
			$pdo = $connection->prepare("SELECT descricao AS id, descricao FROM ambientes_nomes WHERE descricao LIKE '%{$_POST['query']}%'ORDER BY descricao ASC");
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}

	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}	
}
?>