<?php
/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'produtos_marcas';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE produtos_marcas SET 
				descricao = ?,
				alterado_por = ?,
				ativo = ? 
				WHERE cod_marca = ?
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$user_id,
				$_POST['ativo'],
				$_POST['cod_marca']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO produtos_marcas 
					(
						descricao,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						ativo
					)
				VALUES 
					(
						?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				$_POST['ativo']
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}