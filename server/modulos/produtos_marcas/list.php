<?php
/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'produtos_marcas';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT * FROM produtos_marcas AS PM WHERE PM.cod_marca=:id"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_MARCAS'){
			$sql = "SELECT 
				PM.cod_marca AS id, UPPER(PM.descricao) AS descricao 
				FROM produtos_marcas AS PM WHERE PM.ativo = 'S' ORDER BY PM.descricao ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			//echo json_encode( array('dados'=>$rows) );
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";

			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE PM.descricao like '%$_POST[query]%'";
				$where2 = " AND PM.descricao like '%$_POST[query]%'";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_marca'])) {
					$where = "WHERE PM.ativo='S'".$where2;
				}
				elseif ($_POST['status_marca'] == 'X') {
					$where = "".$where1;
				}
				else{
					$where = "WHERE PM.ativo='{$_POST['status_marca']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM produtos_marcas AS PM	$where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT PM.*, DATE_FORMAT(PM.data_cadastro, '%d/%m/%Y ás %H:%i:%s') as data_cadastro, 
				UPPER(U.nome) AS cadastrado_por_nome 
				FROM produtos_marcas AS PM INNER JOIN usuarios AS U ON (U.id=PM.cadastrado_por) 
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}	
}
?>