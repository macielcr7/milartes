<?php
/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'produtos_marcas';
	try {
		$connection->beginTransaction();

		$pdo = $connection->prepare("UPDATE produtos_marcas SET ativo='N' WHERE cod_marca = ?");
		$pdo->execute(array($_POST['id']));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Desativada com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar a Marca!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}