<?php
/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'feriados';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if ($_POST['data_feriado'] == "__/__/____" OR $_POST['data_feriado'] == "") {
			$data_feriado = "0000-00-00";
		} else {
			$data_feriado = implode('-', array_reverse(explode('/', $_POST['data_feriado'])));
		}

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE feriados SET 
				descricao = ?,
				data_feriado = ?,
				alterado_por = ?,
				WHERE cod_tabela = ?
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$data_feriado,
				$user_id,
				$_POST['cod_tabela']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO feriados 
					(
						descricao,
						data_feriado,
						data_cadastro,
						cadastrado_por,
						alterado_por
					)
				VALUES 
					(
						?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['descricao'], 'UTF-8'),
				$data_feriado,
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}