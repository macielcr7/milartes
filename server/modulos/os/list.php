<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'os';

		$filtro = '';
		$userMain = $user->getUser();
		$user_id = null;
		if($userMain){
			$user_id = $userMain['id'];
		}

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT os.*, 
				DATE_FORMAT(os.data_agendada, '%d/%m/%Y') as data_agendada_date, 
				DATE_FORMAT(os.data_agendada, '%H:%i:%s') AS data_agendada_time, 
				DATE_FORMAT(os.data_abertura, '%d/%m/%Y') as data_abertura_date, 
				DATE_FORMAT(os.data_abertura, '%H:%i:%s') AS data_abertura_time, 
				DATE_FORMAT(os.dt_ultimo_comentario, '%d/%m/%Y') as dt_ultimo_comentario_date,  
				DATE_FORMAT(os.dt_ultimo_comentario, '%H:%i:%s') AS dt_ultimo_comentario_time, 
				CONCAT(UPPER(U.nome), ' EM: ', DATE_FORMAT(os.data_abertura, '%d/%m/%Y'), ' ', DATE_FORMAT(os.data_abertura, '%H:%i:%s')) AS cadastrado_por, 
				CONCAT(UPPER(U2.nome), ' EM: ', DATE_FORMAT(os.dt_ultimo_comentario, '%d/%m/%Y'), ' ', DATE_FORMAT(os.dt_ultimo_comentario, '%H:%i:%s')) AS alterado_por 
				FROM os 
				LEFT JOIN usuarios AS U ON (U.id=os.cadastrado_por) 
				LEFT JOIN usuarios AS U2 ON (U2.id=os.alterado_por) 
				WHERE os.id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES_ORCAMENTOS' ){
			$sql = "SELECT O.titulo,
					O.cliente, 
					O.nome_cliente, 
					O.estado,
					O.cidade,
					O.endereco_bairro AS bairro,
					O.endereco,
					O.endereco_num AS nro_end,
					O.endereco_loteamento AS loteamento,
					O.endereco_complemento AS complemento,
					O.endereco_referencia AS ponto_ref,
					O.fone1,
					O.fone2,
					O.fone3,
					O.fone4,
					O.fone5,
					O.fone1_obs,
					O.fone2_obs,
					O.fone3_obs,
					O.fone4_obs,
					O.fone5_obs 
				FROM os AS O 
				WHERE O.id=:id_os
			";
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id_os', $_POST['id_os']);
			$pdo->execute();

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$query[$key] = $value;
				if($query[$key]->fones != "") {
					$palavras = explode('|' , $query[$key]->fones);
					$total_palavras = count($palavras);

					$fones = "";
					$j = 1;
					foreach($palavras as $i => $key2) {
						$nro = explode(' ' , $key2);
						$caracteres = strlen($nro[0]);

						if ($caracteres == 10){
							$sub1 = 10;
							$sub2 = 11;
						}
						elseif ($caracteres == 11){
							$sub1 = 11;
							$sub2 = 12;
						}
						else{
							$sub1 = 11;
							$sub2 = 12;
						}

						if ($j == 1 ) {
							$query[$key]->fone1 =  substr($key2, 0, $sub1);
							$query[$key]->fone1_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone1_obs)) { $query[$key]->fone1_obs = ""; }
						} elseif ($j == 2 ) {
							$query[$key]->fone2 =  substr($key2, 0, $sub1);
							$query[$key]->fone2_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone2_obs)) { $query[$key]->fone2_obs = ""; }
						} elseif ($j == 3 ) {
							$query[$key]->fone3 =  substr($key2, 0, $sub1);
							$query[$key]->fone3_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone3_obs)) { $query[$key]->fone3_obs = ""; }
						} elseif ($j == 4 ) {
							$query[$key]->fone4 =  substr($key2, 0, $sub1);
							$query[$key]->fone4_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone4_obs)) { $query[$key]->fone4_obs = ""; }
						} elseif ($j == 5 ) {
							$query[$key]->fone5 =  substr($key2, 0, $sub1);
							$query[$key]->fone5_obs =  substr($key2, $sub2, 50);
							if (empty($query[$key]->fone5_obs)) { $query[$key]->fone5_obs = ""; }
						}

						$j++;
					}
					$query[$key]->fones = $fones;
				}
			}
			echo json_encode( array('success'=>true, 'dados'=>$query) );
		}
		elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$pdo = $connection->prepare("
				SELECT os.id AS id, os.titulo AS descricao 
				FROM os
			");
			$pdo->execute();

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			if(isset($_POST['cliente']) AND !empty($_POST['cliente'])){
				$buscar->setBusca(array('cliente', 'os.cliente'), $_POST['cliente']);
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$filtro = "WHERE os.titulo like '%{$_POST['titulo']}%'";

				if (!empty($_POST['categoria'])) {
					$filtro  = " AND os.categoria = '{$_POST['categoria']}'";
				}

				if (!empty($_POST['departamento'])) {
					$filtro .= " AND os.departamento = '{$_POST['departamento']}'";
				}

				if (!empty($_POST['endereco'])) {
					$filtro .= " AND os.endereco = '{$_POST['endereco']}'";
				}

				if (!empty($_POST['data_abertura1'])) {
					$data1 = implode('-', array_reverse(explode('/', $_POST['data_abertura1'])))." 00:00:00";
					$data2 = implode('-', array_reverse(explode('/', $_POST['data_abertura2'])))." 23:59:59";
					$filtro .= " AND (os.data_abertura BETWEEN '$data1' AND '$data2')";
				}

				if (!empty($_POST['status_diferente'])) {
					if ($_POST['status_diferente'] == 1) {
						$filtro .= " AND os.status != 'CANCELADO' AND os.status != 'FINALIZADO'";	
					}
					else {
						$filtro .= " AND os.status != '{$_POST['status_diferente']}'";
					}
				}

				if (!empty($_POST['status_igual'])) {
					$filtro .= " AND os.status = '{$_POST['status_igual']}'";
				}

				if (!empty($_POST['usuario_abertura'])) {
					$filtro .= " AND os.usuario_abertura = '{$_POST['usuario_abertura']}'";
				}

				if (!empty($_POST['usuario_responsavel'])) {
					$filtro .= " AND os.usuario_responsavel = '{$_POST['usuario_responsavel']}'";
				}

				if (!empty($_POST['estado'])) {
					$filtro .= " AND os.estado = '{$_POST['estado']}'";
				}

				if (!empty($_POST['cidade'])) {
					$filtro .= " AND os.cidade = '{$_POST['cidade']}'";
				}

				if (!empty($_POST['telefone'])) {
					$filtro .= " AND (os.fone1 = '{$_POST['telefone']}' OR os.fone2 = '{$_POST['telefone']}' OR os.fone3 = '{$_POST['telefone']}' OR os.fone4 = '{$_POST['telefone']}' OR os.fone5 = '{$_POST['telefone']}')";
				}
			} elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST') {
				$buscar->setBusca(array('status', 'os.status'), 'FINALIZADO', 'diff');
				$buscar->setBusca(array('status2', 'os.status'), 'CANCELADO', 'diff');
				$buscar->setBusca(array('usuario_responsavel', 'os.usuario_responsavel'), $_POST['usuario_responsavel']);
				//$buscar->setBusca(array('empresa', 'os.empresa'), $_COOKIE['id_empresa_atual']);
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			if (empty($filtro)) {
				$filtro = $buscar->getSql();
			}

			if ($sort == 'data_agendada' OR $sort == 'data_agendada2') {
				$sort = 'os.data_agendada '.$order.', os.id DESC';
			} elseif ($sort == 'id' OR $sort == 'id2' ) {
				$sort = 'os.id '.$order;
			} elseif ($sort == 'titulo') {
				$sort = 'os.titulo '.$order;
			} elseif ($sort == 'endereco_completo') {
				$sort = 'CP.vencimento '.$order;
			} elseif ($sort == 'depto_categoria') {
				$sort = 'PD.descricao '.$order.', OSC.descricao '.$order;
			} elseif ($sort == 'usuario_abertura') {
				$sort = 'os.usuario_abertura '.$order;
			} elseif ($sort == 'urgencia') {
				$sort = 'os.urgencia '.$order;
			} elseif ($sort == 'status') {
				$sort = 'os.status '.$order;
			}

			$sql = "SELECT count(*) AS total 
				FROM os 
				LEFT JOIN usuarios AS U ON (os.usuario_abertura=U.id) 
				LEFT JOIN usuarios AS U2 ON (os.usuario_responsavel=U2.id) 
				LEFT JOIN produtos_categorias AS PC ON (os.departamento=PC.cod_categoria) 
				LEFT JOIN os_categorias AS OSC ON (os.categoria=OSC.id) 
				LEFT JOIN empresas_sistema AS ES ON (os.empresa=ES.cod_empresa) 
				LEFT JOIN correios_estados AS CE ON (os.estado=CE.uf) 
				LEFT JOIN correios_localidades AS CL ON (os.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (os.endereco_bairro=CB.id) 
				{$filtro}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SET lc_time_names = 'pt_BR'");
			$pdo->execute();

			$sql = "SELECT os.id, os.data_abertura, os.data_fechamento, 
					UPPER(os.titulo) AS titulo, 
					os.status, os.urgencia, 
					CONCAT(UPPER(PC.descricao), ' / ', UPPER(OSC.descricao)) AS depto_categoria, 
					CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', os.endereco_num) AS nome_rua, 
					UPPER(os.endereco_complemento) AS endereco_complemento, 
					UPPER(CB.bairro_nome) AS nome_bairro, 
					UPPER(os.endereco_loteamento) AS endereco_loteamento, 
					CONCAT(DATE_FORMAT(os.data_abertura, '%d/%m/%Y'), ' ÁS ', DATE_FORMAT(os.data_abertura, '%H:%i:%s')) AS data_abertura2, 
					DATE_FORMAT(os.data_agendada, '%H:%i') AS hora_agendada, 
					UPPER(DATE_FORMAT(os.data_agendada, '%a - %d/%m/%Y')) AS data_agendada2, 
					UPPER(U.nome) AS nome_usuario_abertura, 
					UPPER(U2.nome) AS nome_usuario_responsavel, 
					UPPER(U3.nome) AS nome_usuario_fechamento, 
					UPPER(CL.loc_nome_abreviado) AS loc_nome_abreviado 
					FROM os 
					LEFT JOIN usuarios AS U ON (os.usuario_abertura=U.id) 
					LEFT JOIN usuarios AS U2 ON (os.usuario_responsavel=U2.id) 
					LEFT JOIN usuarios AS U3 ON (os.usuario_fechamento=U3.id) 
					LEFT JOIN produtos_categorias AS PC ON (os.departamento=PC.cod_categoria) 
					LEFT JOIN os_categorias AS OSC ON (os.categoria=OSC.id) 
					LEFT JOIN correios_localidades AS CL ON (os.cidade=CL.id) 
					LEFT JOIN correios_enderecos AS CE ON (CE.id=os.endereco) 
					LEFT JOIN correios_bairros AS CB ON (os.endereco_bairro=CB.id) 
					{$filtro} 
					ORDER BY {$sort} 
					LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$endereco_completo = "";
			foreach ($query as $key => $value) {
				$rows[$key] = $value;
				$rows[$key]->descricao = $rows[$key]->descricao;
				$rows[$key]->id2 = str_pad($rows[$key]->id, 5, "0", STR_PAD_LEFT);

				if($query[$key]->nome_rua != "") {
					$endereco_completo .= $rows[$key]->nome_rua;
				}

				if($query[$key]->endereco_complemento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_complemento.")";
				}

				if($query[$key]->nome_bairro != "") {
					$endereco_completo .= " - ".$rows[$key]->nome_bairro;
				}

				if($query[$key]->endereco_loteamento != "") {
					$endereco_completo .= " (".$rows[$key]->endereco_loteamento.")";
				}

				if($query[$key]->loc_nome_abreviado != "") {
					$endereco_completo .= " / ".$rows[$key]->loc_nome_abreviado;
				}

				if($query[$key]->hora_agendada != "" AND $query[$key]->hora_agendada != "00:00") {
					$query[$key]->data_agendada2  = $query[$key]->data_agendada2." ÁS ".$query[$key]->hora_agendada;
				}

				$query[$key]->titulo = "<b>".$query[$key]->titulo."</b><br><i>".$endereco_completo."</i>";
				$endereco_completo = "";
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$rows, 'sql'=>$sql) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage() , 'sql'=>$sql ));
	}	
}
?>