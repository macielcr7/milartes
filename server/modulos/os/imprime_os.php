<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
	@session_start();
	header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Impressão OS</title>
	<link rel="stylesheet" type="text/css" href="../../../resources/css/sandri.css"/>
</head>
<body bgcolor="#FFFFFF">

<?php
	if($_GET){
	require('../../autoLoad.php');
	$contador = 1;

	try {
		$ids = explode(',', $_GET['ids']);
		$total_os = count($ids)-1;
		foreach ($ids as $index => $id) {
			$pdoX = $connection->prepare("SET lc_time_names = 'pt_BR'");
			$pdoX->execute();

			$sql = "SELECT os.id, 
					UPPER(os.titulo) AS titulo, 
					UPPER(os.conteudo) AS conteudo, 
					os.urgencia, 
					UPPER(os.status) AS status, 
					UPPER(os.endereco_referencia) AS endereco_referencia, 
					os.fone1, 
					os.fone2, 
					os.fone3, 
					os.fone4, 
					os.fone5, 
					UPPER(os.fone1_obs) AS fone1_obs, 
					UPPER(os.fone2_obs) AS fone2_obs, 
					UPPER(os.fone3_obs) AS fone3_obs, 
					UPPER(os.fone4_obs) AS fone4_obs, 
					UPPER(os.fone5_obs) AS fone5_obs, 
					UPPER(DATE_FORMAT(os.data_agendada,'%W - %d/%m/%Y')) AS data_agendada, 
					DATE_FORMAT(os.data_agendada,'%H:%i') AS hora_agendada, 
					DATE_FORMAT(os.data_abertura, '%d/%m/%Y') as data_abertura_date, 
					DATE_FORMAT(os.data_abertura, '%H:%i') AS data_abertura_time, 
					DATE_FORMAT(os.dt_ultimo_comentario, '%d/%m/%Y') as dt_ultimo_comentario_date,  
					DATE_FORMAT(os.dt_ultimo_comentario, '%H:%i') AS dt_ultimo_comentario_time, 
					UPPER(CL.loc_nome) AS nome_cidade, 
					UPPER(U.login) AS nome_usuario_abertura, 
					UPPER(U.nome) AS nome_usuario_abertura2,
					UPPER(PC.descricao) AS departamento, 
					UPPER(OSC.descricao) AS categoria, 
					UPPER(CE.tipo) AS tipo_end, 
					UPPER(CE.nome) AS nome_end, 
					UPPER(CB.bairro_nome) AS bairro_end, 
					os.endereco_num, 
					os.estado, 
					CASE WHEN
						os.endereco_complemento != '' THEN CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', os.endereco_num, ' - ', UPPER(os.endereco_complemento), ' ')
					ELSE
						CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', os.endereco_num)
					END AS endereco_completo,

					CASE WHEN
						os.endereco_loteamento != '' THEN CONCAT(UPPER(CB.bairro_nome), ' (', UPPER(os.endereco_loteamento), ')') 
					ELSE
						UPPER(CB.bairro_nome)
					END AS bairro_completo
				FROM os 
				INNER JOIN usuarios AS U ON (os.usuario_abertura=U.id) 
				INNER JOIN produtos_categorias AS PC ON (PC.cod_categoria=os.departamento) 
				INNER JOIN os_categorias AS OSC ON (OSC.id=os.categoria) 
				LEFT JOIN correios_localidades AS CL ON (os.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (os.endereco_bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (os.endereco=CE.id)
				WHERE os.id = '{$id}' ";

			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$os = $pdo->fetch(PDO::FETCH_OBJ);

			$os->conteudo = stripslashes(str_replace("\n", "<br />", $os->conteudo));
			$os->conteudo = str_replace("  ", "&nbsp;&nbsp;", $os->conteudo);

			$monta_endereco2 = $os->tipo_end." ".$os->nome_end.", ".$os->endereco_num." - ".$os->bairro_end.", - ".$os->nome_cidade;
			$monta_endereco2 = titleCase($monta_endereco2);
			$monta_endereco2 = $monta_endereco2." - ".$os->estado;
			$monta_endereco2 = str_replace(' ', '+', $monta_endereco2);

			$monta_data_agendada = $os->data_agendada;
			if ($os->hora_agendada != "" AND $os->hora_agendada != "00:00") {
				$monta_data_agendada .= " ÁS ".$os->hora_agendada;
			}
			$monta_telefones = "";
			if ($os->fone1 != "") { 
				if (strlen($os->fone1) == 10) {
					$fone1 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone1.'" target="_new">(' . substr($os->fone1, 0, 2) . ') ' . substr($os->fone1, 2, 4) . '-' . substr($os->fone1, 6).'</a>';
				}
				else{
					$fone1 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone1.'" target="_new">(' . substr($os->fone1, 0, 2) . ') ' . substr($os->fone1, 2, 5) . '-' . substr($os->fone1, 7).'</a>';
				}

				if ($os->fone1_obs != "") {
					$monta_telefones .= $fone1." - ".$os->fone1_obs;
				} else {
					$monta_telefones .= $fone1;
				}
			}
			if ($os->fone2 != "") {
				if (strlen($os->fone2) == 10) {
					$fone2 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone2.'" target="_new">(' . substr($os->fone2, 0, 2) . ') ' . substr($os->fone2, 2, 4) . '-' . substr($os->fone2, 6).'</a>';
				}
				else{
					$fone2 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone2.'" target="_new">(' . substr($os->fone2, 0, 2) . ') ' . substr($os->fone2, 2, 5) . '-' . substr($os->fone2, 7).'</a>';
				}
				if ($os->fone2_obs != "") {
					$monta_telefones .= " / ".$fone2." - ".$os->fone2_obs;
				} else {
					$monta_telefones .= " / ".$fone2;
				}
			}
			if ($os->fone3 != "") {
				if (strlen($os->fone3) == 10) {
					$fone3 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone3.'" target="_new">(' . substr($os->fone3, 0, 2) . ') ' . substr($os->fone3, 2, 4) . '-' . substr($os->fone3, 6).'</a>';
				}
				else{
					$fone3 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone3.'" target="_new">(' . substr($os->fone3, 0, 2) . ') ' . substr($os->fone3, 2, 5) . '-' . substr($os->fone3, 7).'</a>';
				}
				if ($os->fone3_obs != "") {
					$monta_telefones .= " / ".$fone3." - ".$os->fone3_obs;
				} else {
					$monta_telefones .= " / ".$fone3;
				}
			}
			if ($os->fone4 != "") {
				if (strlen($os->fone4) == 10) {
					$fone4 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone4.'" target="_new">(' . substr($os->fone4, 0, 2) . ') ' . substr($os->fone4, 2, 4) . '-' . substr($os->fone4, 6).'</a>';
				}
				else{
					$fone4 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone4.'" target="_new">(' . substr($os->fone4, 0, 2) . ') ' . substr($os->fone4, 2, 5) . '-' . substr($os->fone4, 7).'</a>';
				}
				if ($os->fone4_obs != "") {
					$monta_telefones .= " / ".$fone4." - ".$os->fone4_obs;
				} else {
					$monta_telefones .= " / ".$fone4;
				}
			}
			if ($os->fone5 != "") {
				if (strlen($os->fone5) == 10) {
					$fone5 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone5.'" target="_new">(' . substr($os->fone5, 0, 2) . ') ' . substr($os->fone5, 2, 4) . '-' . substr($os->fone5, 6).'</a>';
				}
				else{
					$fone5 = '<a href="https://api.whatsapp.com/send?phone=55'.$os->fone5.'" target="_new">(' . substr($os->fone5, 0, 2) . ') ' . substr($os->fone5, 2, 5) . '-' . substr($os->fone5, 7).'</a>';
				}
				if ($os->fone5_obs != "") {
					$monta_telefones .= " / ".$fone5." - ".$os->fone5_obs;
				} else {
					$monta_telefones .= " / ".$fone5;
				}
			}
		?>
			<table width="100%" border="1" style="border-collapse: collapse" bordercolor="#000000" cellpadding="6" cellspacing="0" class="texto2">
				<tr>
					<td width="185" align="left" valign="top">
						<b>OS Nº:</b> <?php echo str_pad($os->id, 5, "0", STR_PAD_LEFT); ?><br>
						<b>OS Aberta por:</b> <?php echo strtoupper($os->nome_usuario_abertura); ?><br>
						<b>Data Cadastro:</b> <?php echo $os->data_abertura_date ." ". $os->data_abertura_time; ?><br>
						<b>Prioridade:</b>
			<?php
			if ($os->urgencia == 1) {
				echo strtoupper("Normal");
			} elseif ($os->urgencia == 2) {
				echo strtoupper("Média");
			} elseif ($os->urgencia == 3) {
				echo strtoupper("<b>Alta</b>");
			} elseif ($os->urgencia == 4) {
				echo strtoupper("<b>Urgente</b>");
			} else {
				echo "<b>....</b>";
			}
			?>
						<br>
						<b>Depto:</b> <?php echo strtoupper($os->departamento); ?>
						<br>
						<b>Categoria:</b> <?php echo strtoupper($os->categoria); ?>
						<br>

						<b>Status:</b> <?php echo strtoupper($os->status); ?>
					</td>
					<td align="left" valign="top">
						<b>Título:</b> <?php echo $os->titulo; ?><br>
						<b>Endereço:</b> <a href="https://www.google.com/maps/place/<?php echo $monta_endereco2;?>" target="_new"><?php echo $os->endereco_completo; ?> <?php if ($os->endereco_referencia != "") { echo " (" .$os->endereco_referencia. ") "; } ?></a> <br>
						<b>Bairro:</b> <?php echo $os->bairro_completo; ?> / <?php echo $os->nome_cidade; ?><br>
						<b>Telefone(s):</b> <?php echo $monta_telefones; ?><br>
						<b>Agendado Para:</b> <?php echo $monta_data_agendada; ?><br>
						<br><b>Primeiro Comentário:</b>
						<br><?php echo $os->conteudo; ?>
			<?php
			$sql1 = "SELECT count(*) AS total 
				FROM 
					os_respostas AS OSR 
					INNER JOIN usuarios AS U ON (U.id=OSR.usuario) 
				WHERE 
					OSR.os_nro='$os->id'";
			$pdo1 = $connection->prepare($sql1);
			$pdo1->execute();
			$query = $pdo1->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql2 = "SELECT UPPER(OSR.conteudo) AS ultima_resp, 
					DATE_FORMAT(OSR.data_cadastro,'%d/%m/%Y') AS data_resposta, 
					DATE_FORMAT(OSR.data_cadastro,'%T') AS hora_resposta, 
					U.login AS quem 
				FROM 
					os_respostas AS OSR 
					INNER JOIN usuarios AS U ON (U.id=OSR.usuario) 
				WHERE 
					OSR.os_nro='$os->id' 
				ORDER BY 
					OSR.id DESC 
				LIMIT 1";

			$pdo2 = $connection->prepare($sql2);
			$pdo2->execute();
			$valor = $pdo2->fetch(PDO::FETCH_OBJ);

			if ($countRow > 0){
				$valor->ultima_resp = stripslashes(str_replace("\n", "<br />", $valor->ultima_resp));
				$valor->ultima_resp = str_replace("  "," &nbsp;",$valor->ultima_resp);
			?>
						<br><br><b>Último Comentário em:</b> <?php echo $valor->data_resposta; ?> às <?php echo $valor->hora_resposta; ?> <b>Por:</b> <?php echo ucfirst($valor->quem); ?>
						<br><?php echo $valor->ultima_resp; ?>
			<?php
			}
			?>
					</td>
				</tr>
				<tr> 
					<td valign="top" bgcolor="#FFFFFF" colspan="2" height="40" align="left"><b>Nova Observação:</b></td>
				</tr>
			<table><br>
				<?php

				if ($contador == 5 AND $index != $total_os){
					$contador = 1;
					echo "<div style='page-break-before: always'><img src=\"../../../resources/images/transparente.gif\" width=\"1\" height=\"1\"></div>";
				}
				else{
					$contador++;
				}
		}
	}
	catch (PDOException $e) {
		echo $e->getMessage();
	}
}
?>
</body>
</html>