<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'os';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	if (empty($_POST['cliente'])) {
		$_POST['cliente'] = NULL;
	}

	if ($_POST['data_agendada_time'] == "__:__" OR $_POST['data_agendada_time'] == "") {
		$data_agendada_time = NULL;
	} else {
		$data_agendada_time = $_POST['data_agendada_time'];
	}

	if ($_POST['data_agendada_date'] == "__/__/____" OR $_POST['data_agendada_date'] == "") {
		$data_agendada_date = NULL;
	} else {
		$data_agendada_date = implode('-', array_reverse(explode('/', $_POST['data_agendada_date'])))." ".$data_agendada_time;
	}

	if ($_POST['fone1'] == "(__) _____-____") {
		$fone1 = NULL;
	} else {
		$fone1 = $_POST['fone1'];
	}

	if ($_POST['fone2'] == "(__) _____-____") {
		$fone2 = NULL;
	} else {
		$fone2 = $_POST['fone2'];
	}

	if ($_POST['fone3'] == "(__) _____-____") {
		$fone3 = NULL;
	} else {
		$fone3 = $_POST['fone3'];
	}

	if ($_POST['fone4'] == "(__) _____-____") {
		$fone4 = NULL;
	} else {
		$fone4 = $_POST['fone4'];
	}

	if ($_POST['fone5'] == "(__) _____-____") {
		$fone5 = NULL;
	} else {
		$fone5 = $_POST['fone5'];
	}

	if (empty($_POST['estado'])) {
		$estado = NULL;
	} else {
		$estado = $_POST['estado'];
	}

	if (empty($_POST['cidade'])) {
		$cidade = NULL;
	} else {
		$cidade = $_POST['cidade'];
	}

	if (empty($_POST['endereco'])) {
		$endereco = NULL;
	} else {
		$endereco = $_POST['endereco'];
	}

	if (empty($_POST['endereco_bairro'])) {
		$endereco_bairro = NULL;
	} else {
		$endereco_bairro = $_POST['endereco_bairro'];
	}

	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE os SET 
							titulo = ?,
							conteudo = ?,
							usuario_responsavel = ?,
							urgencia = ?,
							departamento = ?,
							categoria = ?,
							status = ?,
							cidade = ?,
							estado = ?,
							endereco = ?,
							endereco_num = ?,
							endereco_complemento = ?,
							endereco_referencia = ?,
							endereco_loteamento = ?,
							endereco_bairro = ?,
							fone1 = ?,
							fone2 = ?,
							fone3 = ?,
							fone4 = ?,
							fone5 = ?,
							empresa = ?,
							cliente = ?,
							nome_cliente = ?,
							data_agendada = ?,
							fone1_obs = ?,
							fone2_obs = ?,
							fone3_obs = ?,
							fone4_obs = ?,
							fone5_obs = ?,
							alterado_por = ?,
							dt_ultimo_comentario = ?
 					WHERE id = ?
			");
			$params = array(
				trim(rtrim($_POST['titulo'])),
				$_POST['conteudo'],
				$_POST['usuario_responsavel'],
				$_POST['urgencia'],
				$_POST['departamento'],
				$_POST['categoria'],
				$_POST['status'],
				$cidade,
				$estado,
				$endereco,
				$_POST['endereco_num'],
				$_POST['endereco_complemento'],
				$_POST['endereco_referencia'],
				$_POST['endereco_loteamento'],
				$endereco_bairro,
				$fone1,
				$fone2,
				$fone3,
				$fone4,
				$fone5,
				$_POST['empresa'],
				$_POST['cliente'],
				$_POST['nome_cliente'],
				$data_agendada_date,
				$_POST['fone1_obs'],
				$_POST['fone2_obs'],
				$_POST['fone3_obs'],
				$_POST['fone4_obs'],
				$_POST['fone5_obs'],
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['id']
			);
			$pdo->execute($params);
		}
		elseif ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');

			$pdo = $connection->prepare("SELECT id_usuario FROM os_departamentos_responsavel WHERE id_depto='{$_POST['departamento']}' AND id_empresa='{$_POST['empresa']}'");
			$pdo->execute();
			$item = $pdo->fetch(PDO::FETCH_OBJ);
			if (empty($item->id_usuario)) {
				$usuario_responsavel = 2;
			} else {
				$usuario_responsavel = $item->id_usuario;
			}
			
			$pdo = $connection->prepare("
				INSERT INTO os 
					(
						titulo,
						conteudo,
						usuario_abertura,
						usuario_responsavel,
						urgencia,
						departamento,
						categoria,
						data_abertura,
						status,
						dt_ultimo_comentario,
						cidade,
						estado,
						endereco,
						endereco_num,
						endereco_complemento,
						endereco_referencia,
						endereco_bairro,
						endereco_loteamento,
						fone1,
						fone2,
						fone3,
						fone4,
						fone5,
						fone1_obs,
						fone2_obs,
						fone3_obs,
						fone4_obs,
						fone5_obs,
						empresa,
						cliente,
						nome_cliente,
						verificado_fatura,
						data_agendada,
						cadastrado_por,
						alterado_por
					)
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['titulo'],
				$_POST['conteudo'],
				$user_id,
				$usuario_responsavel,
				$_POST['urgencia'],
				$_POST['departamento'],
				$_POST['categoria'],
				date('Y-m-d H:i:s'),
				'NOVO',
				date('Y-m-d H:i:s'),
				$cidade,
				$estado,
				$endereco,
				$_POST['endereco_num'],
				$_POST['endereco_complemento'],
				$_POST['endereco_referencia'],
				$endereco_bairro,
				$_POST['endereco_loteamento'],
				$fone1,
				$fone2,
				$fone3,
				$fone4,
				$fone5,
				$_POST['fone1_obs'],
				$_POST['fone2_obs'],
				$_POST['fone3_obs'],
				$_POST['fone4_obs'],
				$_POST['fone5_obs'],
				$_POST['empresa'],
				$_POST['cliente'],
				$_POST['nome_cliente'],
				'N',
				$data_agendada_date,
				$user_id,
				$user_id
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}