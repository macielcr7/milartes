<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'os';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		if($_POST['action'] == 'DELETAR'){
			$data_hora_atual = date('Y-m-d H:i:s');
			$user->getAcao($tabela, 'deletar');
			$pdo = $connection->prepare("INSERT INTO os_respostas (usuario, os_nro, conteudo, data_cadastro, cadastrado_por, alterado_por) VALUES ('$user_id', '{$_POST['cod_os']}', '{$_POST['motivo_cancelamento']}', '$data_hora_atual', '$user_id', '$user_id')");

			$pdo->execute();
			$pdo2 = $connection->prepare("UPDATE os SET status='CANCELADO', dt_ultimo_comentario='$data_hora_atual',  usuario_fechamento='$user_id',  data_fechamento='$data_hora_atual' WHERE id = ?");
			$pdo2->execute(array(
				$_POST['cod_os']
			));

			$descricao = $_POST['motivo_cancelamento'];
			$pdo3 = $connection->prepare("INSERT INTO logs_sistema 
				(chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
			VALUES 
				(md5(NOW()), 'os', '{$_POST['cod_os']}', 'Cancelou a OS', 'Descrição', '', '$descricao', '$user_id', '$data_hora_atual')");
			$pdo3->execute();

			echo json_encode(array('success'=>true, 'msg'=>'OS Cancelada com Sucesso'));
		}
		elseif($_POST['action'] == 'FINALIZAR'){
			$data_hora_atual = date('Y-m-d H:i:s');
			$pdo = $connection->prepare("INSERT INTO os_respostas (usuario, os_nro, conteudo, data_cadastro, cadastrado_por, alterado_por) VALUES ('$user_id', '{$_POST['cod_os']}', '{$_POST['observacao_finalizacao']}', '$data_hora_atual', '$user_id', '$user_id')");
			$pdo->execute();

			$pdo2 = $connection->prepare("UPDATE os SET status='FINALIZADO', dt_ultimo_comentario='$data_hora_atual',  usuario_fechamento='$user_id',  data_fechamento='$data_hora_atual' WHERE id = ?");
			$pdo2->execute(array(
				$_POST['cod_os']
			));

			$descricao = $_POST['observacao_finalizacao'];
			$pdo3 = $connection->prepare("INSERT INTO logs_sistema 
				(chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
			VALUES 
				(md5(NOW()), 'os', '{$_POST['cod_os']}', 'Finalizou a OS', 'Descrição', '', '$descricao', '$user_id', '$data_hora_atual')");
			$pdo3->execute();

			echo json_encode(array('success'=>true, 'msg'=>'OS Finalizada com Sucesso'));
		}
		elseif($_POST['action'] == 'REABRIR'){
			$data_hora_atual = date('Y-m-d H:i:s');
			$pdo = $connection->prepare("INSERT INTO os_respostas (usuario, os_nro, conteudo, data_cadastro, cadastrado_por, alterado_por) VALUES ('$user_id', '{$_POST['id']}', 'OS REABERTA', '$data_hora_atual', '$user_id', '$user_id')");
			$pdo->execute();

			$pdo2 = $connection->prepare("UPDATE os SET status='REABERTA', dt_ultimo_comentario='$data_hora_atual',  usuario_fechamento=NULL,  data_fechamento=NULL WHERE id = ?");
			$pdo2->execute(array(
				$_POST['id']
			));

			$pdo3 = $connection->prepare("INSERT INTO logs_sistema 
				(chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
			VALUES 
				(md5(NOW()), 'os', '{$_POST['id']}', 'Reabriu a OS', 'Descrição', '', 'OS REABERTA', '$user_id', '$data_hora_atual')");
			$pdo3->execute();

			echo json_encode(array('success'=>true, 'msg'=>'OS Reaberta com Sucesso'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}