<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'vendas_pagamentos';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		if (isset($_POST['data_venda_cartao'])) {
			$data_venda_cartao = implode('-', array_reverse(explode('/', $_POST['data_venda_cartao'])));
		}

		$observacoes = $_POST['cod_venda']." / ".mb_strtoupper($_POST['observacao'], 'UTF-8');

		//verfifica se tem caixa aberto
		$sql = "SELECT caixa_atual FROM usuarios WHERE id = '{$user_id}'";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetch(PDO::FETCH_OBJ);
		$caixa_atual = $query->caixa_atual;

		if ($caixa_atual == 0){
			echo json_encode(array('success'=>false, 'msg'=>'Você deve Primeiro Abrir o Caixa para poder receber valores', 'erro'=>'Precia Abrir o Caixa Primeiro'));
		}
		else{
			//verifica se a forma de pagto é a vista ou a prazo
			$sql = "SELECT tipo, categoria FROM formas_pagto WHERE id = '{$_POST['forma_pagto']}'";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$vista_prazo = $query->tipo;
			$categoria = $query->categoria;

			if ($_POST['action'] == 'INSERIR'){
				//$user->getAcao($tabela, 'adicionar');
				$pdo = $connection->prepare("
				INSERT INTO vendas_pagamentos 
					(
						cod_venda, 
						forma_pagto, 
						valor_pago, 
						nro_parcelas_cartao, 
						observacao, 
						cadastrado_por, 
						alterado_por, 
						data_cadastro, 
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?
					)
				");
				$params = array(
					$_POST['cod_venda'],
					$_POST['forma_pagto'],
					$_POST['valor_pago'],
					$_POST['nro_parcelas'],
					$observacoes,
					$user_id,
					$user_id,
					date('Y-m-d H:i:s'), 
					date('Y-m-d H:i:s')
				);
				$pdo->execute($params);
				$ultimo_id = $connection->lastInsertId();

				//registra o adiantamento no caixa
				$pdo = $connection->prepare("
				INSERT INTO caixa_itens 
					(
						caixa_nro,
						data_hora,
						tipo_lancamento,
						cadastrado_por,
						id_tipo,
						valor,
						nro_parcelas_cartao,
						cancelado,
						forma_pagto,
						tipo_venda,
						cod_cliente,
						observacoes
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
				");
				$params = array(
					$caixa_atual,
					$data_hora_atual,
					2,
					$user_id,
					$ultimo_id,
					$_POST['valor_pago'],
					$_POST['nro_parcelas'],
					'N',
					$_POST['forma_pagto'],
					$vista_prazo,
					$_POST['cliente'],
					$observacoes
				);
				$pdo->execute($params);






				//Categoria 2=Boleto, 3=Cartão
				if ($categoria == 3) {
					//pega calcula o valor que vai ficar a parcela liquida
					$sql3 = "SELECT * FROM contratos_cartao INNER JOIN formas_pagto ON (formas_pagto.cod_convenio=contratos_cartao.id) WHERE formas_pagto.id='{$_POST['forma_pagto']}'";
					$pdo3 = $connection->prepare($sql3);
					$pdo3->execute();
					$CC = $pdo3->fetch(PDO::FETCH_OBJ);

					$taxa = 'taxa'.$_POST['nro_parcelas'];

					$valor_da_parcela = $_POST['valor_pago'] / $_POST['nro_parcelas'];
					$valor_da_parcela = round($valor_da_parcela, 2);

					$valor_venda_liquida = $_POST['valor_pago'] - ($CC->$taxa * $_POST['valor_pago'] / 100);
					$valor_parcela_liquida = $valor_da_parcela - ($CC->$taxa * $valor_da_parcela / 100);

					$pdo = $connection->prepare("
					INSERT INTO cartoes_vendas 
						(
							tipo_lancamento,
							id_tipo_lancamento,
							valor_venda_bruta,
							numero_parcelas,
							valor_venda_liquida,
							cod_cartao,
							observacoes,
							data_venda,
							estornada,
							data_estorno,
							motivo_estorno,
							tipo_taxa,
							cod_empresa,
							data_cadastro,
							cadastrado_por,
							alterado_por,
							data_alteracao
						) 
					VALUES 
						(
							?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
						)
					");
					$params = array(
						1,
						$ultimo_id,
						$_POST['valor_pago'],
						$_POST['nro_parcelas'],
						$valor_venda_liquida,
						$_POST['forma_pagto'],
						$observacoes,
						$data_venda_cartao,
						'N',
						NULL,
						NULL,
						'N',
						$_COOKIE['id_empresa_atual'],
						$data_hora_atual,
						$user_id,
						$user_id,
						$data_hora_atual
					);
					$pdo->execute($params);
					$ultimo = $connection->lastInsertId();

					$dias_previsto = 0;
					$partes = explode("-", $data_venda_cartao);
					for ($i = 1; $i <= $_POST['nro_parcelas']; $i++) {
						$texto_descricao = "Parcela ".$i." de ".$_POST['nro_parcelas'];

						if ($_POST['nro_parcelas'] == 1) {
							$dias_previsto = $CC->dias_credito_parc_vista;
							$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
							$data_prevista_recebto = feriado($dt_prevista);
						}
						else {
							$dias_previsto = $CC->dias_credito_parc_estab;

							if ($CC->tipo_calculo == "MASTERCARD") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									//pega o dia do mes que recebeu a primeira parcela
									$partes3 = explode("-",$dt_prevista);
									$data_prevista_recebto = feriado($dt_prevista);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes3[1] + ($i - 1), $partes3[2], $partes3[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
							elseif ($CC->tipo_calculo == "VISA") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
							elseif ($CC->tipo_calculo == "HIPERCARD") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									//pega o dia do mes que recebeu a primeira parcela
									$partes3 = explode("-",$dt_prevista);
									$data_prevista_recebto = feriado($dt_prevista);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes3[1] + ($i - 1), $partes3[2], $partes3[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
							elseif ($CC->tipo_calculo == "AMERICAN") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
							elseif ($CC->tipo_calculo == "BANRICOMPRAS") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
							elseif ($CC->tipo_calculo == "NILSON") {
								$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
								$data_prevista_recebto = feriado($dt_prevista);
							}
							elseif ($CC->tipo_calculo == "CONSTRUCARD") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
							elseif ($CC->tipo_calculo == "SANTANDER") {
								if ($i == 1) {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + $dias_previsto, $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
					
									//pega o dia do mes que recebeu a primeira parcela
									$partes3 = explode("-",$data_prevista_recebto);
								}
								else {
									$dt_prevista = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + ($i * $dias_previsto), $partes[0]));
									$data_prevista_recebto = feriado($dt_prevista);
								}
							}
						}


						$pdo = $connection->prepare("
						INSERT INTO cartoes_vendas_itens 
							(
								cod_venda_cartao,
								descricao,
								parcela,
								valor_parcela_bruta,
								valor_parcela_liquida,
								data_prevista_recebto
							) 
						VALUES 
							(
								?,	?,	?,	?,	?,	?
							)
						");
						$params = array(
							$ultimo,
							$texto_descricao,
							$i,
							$valor_da_parcela,
							$valor_parcela_liquida,
							$data_prevista_recebto
						);
						$pdo->execute($params);
					}
				}
			}
			else{
				throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
			}
			echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS.$valor_venda_liquida));
			//echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>