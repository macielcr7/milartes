<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'vendas_pagamentos';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT * FROM vendas_pagamentos WHERE id=:id");
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			$pdo = $connection->prepare("SELECT count(*) AS total FROM vendas_pagamentos WHERE cod_venda='{$_POST['cod_venda']}'");
			$pdo->execute();

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;
			
			$sql = 	"SELECT VP.id, VP.cod_venda, VP.forma_pagto, VP.valor_pago, VP.data_cadastro, VP.estornado, VP.data_estorno, VP.observacao, 
						UPPER(FP.descricao) AS forma_pagamento_descricao, FP.categoria AS categoria_pagamento, 
						UPPER(U.nome) AS cadastrado_por_nome, UPPER(U2.nome) AS estornado_por_nome 
					FROM vendas_pagamentos AS VP 
					INNER JOIN formas_pagto AS FP ON (FP.id=VP.forma_pagto) 
					INNER JOIN usuarios AS U ON (VP.cadastrado_por=U.id) 
					LEFT JOIN usuarios AS U2 ON (VP.estornado_por=U2.id) 
					WHERE VP.cod_venda='{$_POST['cod_venda']}' ORDER BY VP.data_cadastro DESC";
			$pdo = $connection->prepare($sql);

			$pdo->execute();
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;
			
			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('total'=>$countRow, 'dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>