<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'vendas_pagamentos';
	$data_hora_atual = date('Y-m-d H:i:s');

	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$continua = 'N';

	try {
		$motivo_cancelamento = trim(mb_strtoupper($_POST['motivo_cancelamento'], 'UTF-8'));

		//verfifica se tem caixa aberto
		$sql = "SELECT caixa_atual FROM usuarios WHERE id = '{$user_id}'";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetch(PDO::FETCH_OBJ);
		$caixa_atual = $query->caixa_atual;

		if ($caixa_atual == 0) {
			echo json_encode(array('success'=>false, 'msg'=>'Você deve Primeiro Abrir o Caixa para poder Estornar valores', 'erro'=>'Precia Abrir o Caixa Primeiro'));
		}
		else{
			if($_POST['action'] == 'ESTORNAR'){
				$user->getAcao($tabela, 'estornar');

				//verifica se eh cartao
				if($_POST['categoria'] == 3) {
					//eh cartão, pega o controle do lancamento
					$pdo6 = $connection->prepare("SELECT id FROM cartoes_vendas WHERE id_tipo_lancamento={$_POST['id']}");
					$pdo6->execute();
					$query6 = $pdo6->fetch(PDO::FETCH_OBJ);
					$id_cartao = $query6->id;

					//verifica se o cartao já tem alguma parcela baixada
					$pdo7 = $connection->prepare("SELECT count(*) AS total FROM cartoes_vendas_itens WHERE cod_venda_cartao={$id_cartao} AND recebido_em is not NULL");
					$pdo7->execute();
					$query7 = $pdo7->fetch(PDO::FETCH_OBJ);
					$total_parcelas_baixadas = $query7->total;

					if ($total_parcelas_baixadas == 0) {
						//pode apagar registro
						$pdo8 = $connection->prepare("UPDATE cartoes_vendas SET estornada='S', data_estorno='{$data_hora_atual}', motivo_estorno='{$motivo_cancelamento}' WHERE id = ?");
						$pdo8->execute(array(
							$id_cartao
						));

						$pdo9 = $connection->prepare("DELETE FROM cartoes_vendas_itens WHERE cod_venda_cartao = ?");
						$pdo9->execute(array(
							$id_cartao
						));

						$continua = 'S';
					}
					else {
						$continua = 'N';
					}
				}
				else {
					$continua = 'S';
				}

				if ($continua == "S") {
					$pdo = $connection->prepare("UPDATE vendas_pagamentos SET estornado='S', data_estorno='{$data_hora_atual}', estornado_por = '$user_id' WHERE  id = ?");
					$pdo->execute(array(
						$_POST['id']
					));

					//verifica o nro do caixa em que o pagamento foi efetuado
					$sql2 = "SELECT CI.caixa_nro, CI.valor, CI.forma_pagto, CI.tipo_venda, CI.cod_cliente,
					CASE WHEN
						C.tipo_cliente = 'J'
						THEN
							CASE WHEN C.nome_fantasia != ''
								THEN 
									CONCAT(UPPER(C.razao_social), ' (', UPPER(C.nome_fantasia), ')')
								ELSE
									UPPER(C.razao_social)
							END
						ELSE
							UPPER(C.nome_completo)
					END AS nome_cliente FROM caixa_itens AS CI LEFT JOIN clientes AS C ON (CI.cod_cliente=C.cod_cliente) WHERE CI.tipo_lancamento = '2' AND CI.id_tipo = ?";
					$pdo2 = $connection->prepare($sql2);
					$pdo2->execute(array(
						$_POST['id']
					));
					$query2 = $pdo2->fetch(PDO::FETCH_OBJ);
					$cx_pagamento = $query2->caixa_nro;

					if ($caixa_atual == $cx_pagamento) {
						$pdo3 = $connection->prepare("UPDATE caixa_itens SET cancelado='S', data_cancelamento='{$data_hora_atual}', cancelado_por='{$user_id}' WHERE  tipo_lancamento = '2' AND id_tipo = ?");
						$pdo3->execute(array(
							$_POST['id']
						));
					}
					else {
						//lanca como saída/devolução do caixa atual
						$texto = 'ESTORNO DE PAGAMENTO DO CAIXA: '.$cx_pagamento.' / CLIENTE: '.$query2->cod_cliente.' - '.$query2->nome_cliente;
						$sql4 = "INSERT INTO caixa_itens 
						(
							caixa_nro,
							data_hora,
							tipo_lancamento,
							cadastrado_por,
							id_tipo,
							valor,
							cancelado,
							forma_pagto,
							tipo_venda,
							cod_cliente,
							observacoes
						) 
					VALUES 
						(
							'{$caixa_atual}', '{$data_hora_atual}', 4, '{$user_id}', 0, '-{$query2->valor}', 'N', '{$query2->forma_pagto}', '{$query2->tipo_venda}','{$query2->cod_cliente}', '{$texto}'
						)";
						$pdo4 = $connection->prepare($sql4);
						$pdo4->execute();
					}

					echo json_encode(array('success'=>true, 'msg'=>'PAGMENTO ESTORNADO COM SUCESSO!'));
				}
				else {
					//tem que ver para baixar manualmente do 
					echo json_encode(array('success'=>false, 'msg'=>'JÁ EXISTE PARCELA(S) DESTE PAGAMENTO BAIXADO COMO CONFERIDO PELO ADMINISTRADOR, AVISAR ELE SE REALMENTE PRECISAR ESTORNAR ESTE PAGAMENTO PARA QUE SEJA PROCESSADA NOVAMENTE A BAIXA DA(S) PARCELA(S), MAS PRIMEIRO TEM QUE SER BAIXADO MANUALMENTE NO BANCO DE DADOS'));

					$continua = 'N';
				}
			}
			else{
				throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
			}
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA, 'erro'=>$e->getMessage()));
	}
}
?>