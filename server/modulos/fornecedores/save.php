<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'fornecedores';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');

			if ($_POST['data_nascimento'] == "__/__/____" OR $_POST['data_nascimento'] == "") {
				$data_nascimento = "0000-00-00";
			} else {
				$data_nascimento = implode('-', array_reverse(explode('/', $_POST['data_nascimento'])));
			}

			if ($_POST['cpf'] == "___.___.___-__") {
				$cpf = "";
			} else {
				$cpf = $_POST['cpf'];
			}

			if ($_POST['cnpj'] == "__.___.___/____-__") {
				$cnpj = "";
			} else {
				$cnpj = $_POST['cnpj'];
			}

			if ($_POST['ie'] == "___.___.___") {
				$ie = "";
			} else {
				$ie = $_POST['ie'];
			}

			if ($_POST['im'] == "___.___.___") {
				$im = "";
			} else {
				$im = $_POST['im'];
			}

			if ($_POST['cep'] == "__.___-___") {
				$cep = "";
			} else {
				$cep = $_POST['cep'];
			}

			if (empty($_POST['estado'])) {
				$estado = NULL;
			} else {
				$estado = $_POST['estado'];
			}

			if (empty($_POST['bairro'])) {
				$bairro = NULL;
			} else {
				$bairro = $_POST['bairro'];
			}

			$pdo = $connection->prepare("
					UPDATE fornecedores SET 
							tipo_fornecedor = ?,							
							nome_completo = ?,							
							razao_social = ?,							
							nome_fantasia = ?,							
							pessoa_contato = ?,							
							data_nascimento = ?,							
							sexo = ?,							
							cpf = ?,							
							cnpj = ?,							
							ie = ?,							
							im = ?,							
							identidade = ?,							
							profissao = ?,							
							estado = ?,							
							cidade = ?,							
							bairro = ?,							
							endereco = ?,							
							nro_end = ?,							
							loteamento = ?,							
							complemento = ?,							
							ponto_ref = ?,							
							cx_postal = ?,
							cep = ?,								
							alterado_por = ?,							
							data_alteracao = ?,							
							situacao_cadastral = ?							
 					WHERE cod_fornecedor = ?
			");
			$params = array(
				mb_strtoupper($_POST['tipo_fornecedor'], 'UTF-8'),
				mb_strtoupper($_POST['nome_completo'], 'UTF-8'),
				mb_strtoupper($_POST['razao_social'], 'UTF-8'),
				mb_strtoupper($_POST['nome_fantasia'], 'UTF-8'),
				mb_strtoupper($_POST['pessoa_contato'], 'UTF-8'),
				$data_nascimento,
				mb_strtoupper($_POST['sexo'], 'UTF-8'),
				$cpf,
				$cnpj,
				$ie,
				$im,
				mb_strtoupper($_POST['identidade'], 'UTF-8'),
				mb_strtoupper($_POST['profissao'], 'UTF-8'),
				$estado,
				$_POST['cidade'],
				$bairro,
				$_POST['endereco'],
				mb_strtoupper($_POST['nro_end'], 'UTF-8'),
				mb_strtoupper($_POST['loteamento'], 'UTF-8'),
				mb_strtoupper($_POST['complemento'], 'UTF-8'),
				mb_strtoupper($_POST['ponto_ref'], 'UTF-8'),
				mb_strtoupper($_POST['cx_postal'], 'UTF-8'),
				$cep,
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['situacao_cadastral'],
				$_POST['cod_fornecedor']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');

			if ($_POST['data_nascimento'] == "__/__/____" OR $_POST['data_nascimento'] == "") {
				$data_nascimento = "0000-00-00";
			} else {
				$data_nascimento = implode('-', array_reverse(explode('/', $_POST['data_nascimento'])));
			}

			if ($_POST['cpf'] == "___.___.___-__") {
				$cpf = "";
			} else {
				$cpf = $_POST['cpf'];
			}

			if ($_POST['cnpj'] == "__.___.___/____-__") {
				$cnpj = "";
			} else {
				$cnpj = $_POST['cnpj'];
			}

			if ($_POST['ie'] == "___.___.___") {
				$ie = "";
			} else {
				$ie = $_POST['ie'];
			}

			if ($_POST['im'] == "___.___.___") {
				$im = "";
			} else {
				$im = $_POST['im'];
			}

			if ($_POST['cep'] == "__.___-___") {
				$cep = "";
			} else {
				$cep = $_POST['cep'];
			}

			if (empty($_POST['estado'])) {
				$estado = NULL;
			} else {
				$estado = $_POST['estado'];
			}

			if (empty($_POST['bairro'])) {
				$bairro = NULL;
			} else {
				$bairro = $_POST['bairro'];
			}

			$pdo = $connection->prepare("
				INSERT INTO fornecedores 
					(
						tipo_fornecedor, 
						nome_completo, 
						razao_social, 
						nome_fantasia, 
						pessoa_contato, 
						data_nascimento, 
						sexo, 
						cpf, 
						cnpj, 
						ie, 
						im, 
						identidade, 
						profissao, 
						estado, 
						cidade, 
						bairro, 
						endereco, 
						nro_end, 
						loteamento, 
						complemento, 
						ponto_ref, 
						cx_postal, 
						cep, 
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao,
						situacao_cadastral
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?, ?
					)
			");
			$params = array(
				mb_strtoupper($_POST['tipo_fornecedor'], 'UTF-8'),
				mb_strtoupper($_POST['nome_completo'], 'UTF-8'),
				mb_strtoupper($_POST['razao_social'], 'UTF-8'),
				mb_strtoupper($_POST['nome_fantasia'], 'UTF-8'),
				mb_strtoupper($_POST['pessoa_contato'], 'UTF-8'),
				$data_nascimento,
				mb_strtoupper($_POST['sexo'], 'UTF-8'),
				$cpf, 
				$cnpj, 
				$ie,
				$im,
				mb_strtoupper($_POST['identidade'], 'UTF-8'),
				mb_strtoupper($_POST['profissao'], 'UTF-8'),
				$estado,
				$_POST['cidade'], 
				$bairro, 
				$_POST['endereco'], 
				mb_strtoupper($_POST['nro_end'], 'UTF-8'),
				mb_strtoupper($_POST['loteamento'], 'UTF-8'),
				mb_strtoupper($_POST['complemento'], 'UTF-8'),
				mb_strtoupper($_POST['ponto_ref'], 'UTF-8'),
				mb_strtoupper($_POST['cx_postal'], 'UTF-8'),
				$cep, 
				date('Y-m-d H:i:s'), 
				$user_id,
				$user_id,
				date('Y-m-d H:i:s'), 
				$_POST['situacao_cadastral']
			);
			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS, 'cod_fornecedor'=> $connection->lastInsertId()));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA."<br><br>".$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}