<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'fornecedores';
	try {
		if($_POST['action'] == 'DELETAR'){
			$user->getAcao($tabela, 'deletar');
		
			$pdo = $connection->prepare("UPDATE fornecedores SET  situacao_cadastral = 'E' WHERE cod_fornecedor = ?");
			$pdo->execute(array(
				$_POST['id']
			));
			
			echo json_encode(array('success'=>true, 'msg'=>REMOVED_SUCCESS));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERRO_DELETE_DATA.'<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}