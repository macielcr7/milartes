<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'fornecedores';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT F.cod_fornecedor, 
					F.tipo_fornecedor,
					F.nome_completo,
					F.razao_social, 
					F.nome_fantasia,
					F.pessoa_contato,
					DATE_FORMAT(F.data_nascimento, '%d/%m/%Y') AS data_nascimento,
					F.sexo,
					F.cpf,
					F.cnpj,
					F.ie,
					F.im,
					F.identidade,
					F.profissao,
					F.estado,
					F.cidade,
					F.bairro,
					F.endereco,
					F.nro_end,
					F.loteamento,
					F.complemento,
					F.ponto_ref,
					F.cx_postal,
					F.cep,
					F.situacao_cadastral,
					CONCAT(UPPER(U.nome), ' EM: ', DATE_FORMAT(F.data_cadastro, '%d/%m/%Y'), ' ', DATE_FORMAT(F.data_cadastro, '%H:%i:%s')) AS cadastrado_por,
					CONCAT(UPPER(U2.nome), ' EM: ', DATE_FORMAT(F.data_alteracao, '%d/%m/%Y'), ' ', DATE_FORMAT(F.data_alteracao, '%H:%i:%s')) AS alterado_por 
				FROM fornecedores AS F
				LEFT JOIN usuarios AS U ON (U.id=F.cadastrado_por)
				LEFT JOIN usuarios AS U2 ON (U2.id=F.alterado_por)
				WHERE F.cod_fornecedor=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$dados = $linhas;
			if($dados->data_nascimento == '00/00/0000') {
				$dados->data_nascimento = "";
			}
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if( isset($_POST['action']) AND $_POST['action'] == 'VALID_UNIQUE' ){
			$where = "WHERE {$_POST['param']} = :valor AND situacao_cadastral != 'E'";

			if(isset($_POST['cod_fornecedor']) and !empty($_POST['cod_fornecedor'])){
				$where .= " AND cod_fornecedor != ".$_POST['cod_fornecedor'];
			}

			$pdo = $connection->prepare("SELECT COUNT(*) AS total FROM fornecedores AS F {$where}");
			$pdo->bindParam(':valor', $_POST['valor']);
			$pdo->execute();

			if (empty($_POST['valor'])) {
				$total = 0;
			}
			elseif ($_POST['valor'] == "__.___.___/____-__") {
				$total = 0;
			}
			elseif ($_POST['valor'] == "___.___.___-__") {
				$total = 0;
			}
			else {
				$total = $pdo->fetch(PDO::FETCH_OBJ)->total;
			}

			if($total > 0){
				$success = false;
			} else{ $success = true; }

			echo json_encode( array('success'=>$success) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'COMBO_CONTAS_PAGAR'){
			$buscar->setBusca(array('nome_completo', 'F.nome_completo'), $_POST['query'], 'like');
			$buscar->setBusca(array('nome_fantasia', 'F.nome_fantasia'), $_POST['query'], 'like');
			$buscar->setBusca(array('razao_social', 'F.razao_social'), $_POST['query'], 'like');
			$filtro = $buscar->filtro.' ('.implode($buscar->sql, ' OR ').') ';

			$filtro = $buscar->getSql();
			$sql = "SELECT F.cod_fornecedor AS id, 
				CONCAT(
					CASE WHEN
						F.tipo_fornecedor = 'F' THEN F.nome_completo
					ELSE
						CASE WHEN 
							F.nome_fantasia IS NULL or F.nome_fantasia = '' THEN F.razao_social
						ELSE
							CONCAT(F.razao_social, ' (', F.nome_fantasia, ')')
						END
					END
				, ' - ', 
					CASE WHEN
						F.tipo_fornecedor = 'F' THEN F.cpf
					ELSE
						F.cnpj
					END 
				) AS descricao 
				FROM fornecedores AS F 
				WHERE F.nome_completo like '%$_POST[query]%' OR F.nome_fantasia like '%$_POST[query]%' OR F.razao_social like '%$_POST[query]%' 
				ORDER BY F.razao_social ASC, F.nome_fantasia ASC, F.nome_completo ASC
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute($buscar->getArrayExecute());

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_PROFISSAO'){
			$buscar->setBusca(array('profissao', 'F.profissao'), $_POST['query'], 'like');
			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT F.profissao AS id, F.profissao AS descricao 
				FROM fornecedores AS F {$filtro} GROUP BY F.profissao ORDER BY F.descricao ASC");
			$pdo->execute($buscar->getArrayExecute());
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_PRODUTOS'){
			$sql = "SELECT F.cod_fornecedor AS id, 
				CONCAT(
					CASE WHEN
						F.tipo_fornecedor = 'F' THEN F.nome_completo
					ELSE
						CONCAT(F.razao_social, ' (', F.nome_fantasia, ')')
					END
				, ' - ', 
					CASE WHEN
						F.tipo_fornecedor = 'F' THEN F.cpf
					ELSE
						F.cnpj
					END 
				) AS descricao 
				FROM fornecedores AS F WHERE F.situacao_cadastral = 'N' ORDER BY F.razao_social ASC, F.nome_completo ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			//echo json_encode( array('dados'=>$rows) );
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['query']) and !empty($_POST['query'])){
				$buscar->setBusca(array('nome_completo', 'F.nome_completo'), $_POST['query'], 'like');
				$buscar->setBusca(array('razao_social', 'F.razao_social'), $_POST['query'], 'like');
				$buscar->setBusca(array('nome_fantasia', 'F.nome_fantasia'), $_POST['query'], 'like');
				$buscar->setBusca(array('cpf', 'F.cpf'), $_POST['query'], 'like');
				$buscar->setBusca(array('cnpj', 'F.cnpj'), $_POST['query'], 'like');
				$buscar->setBusca(array('estado', 'F.estado'), $_POST['query'], 'like');
				$buscar->setBusca(array('cidade', 'CL.loc_nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('bairro', 'CB.bairro_nome'), $_POST['query'], 'like');
				$buscar->setBusca(array('endereco', 'CE.nome'), $_POST['query'], 'like');				
				$filtro = $buscar->filtro.' ('.implode($buscar->sql, ' OR ').') ';
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$pdo = $connection->prepare("SELECT count(*) AS total FROM fornecedores AS F 
				LEFT JOIN correios_localidades AS CL ON (F.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (F.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (F.endereco=CE.id) 
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$pdo = $connection->prepare("SELECT F.cod_fornecedor, 
					F.tipo_fornecedor, 
					UPPER(F.nome_completo) AS nome_completo, 
					UPPER(F.razao_social) AS razao_social, 
					UPPER(F.nome_fantasia) AS nome_fantasia, 
					F.cpf, 
					F.cnpj, 
					F.nro_end, 
					F.loteamento, 
					F.situacao_cadastral, 
					DATE_FORMAT(F.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(F.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(F.data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(F.data_alteracao, '%Y-%m-%d') as data_alteracao_date, 
					CONCAT(UPPER(CL.loc_nome), ' / ', F.estado) AS loc_nome, 

					CASE WHEN
						F.complemento != '' THEN CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end, ' (', UPPER(F.complemento), ')')
					ELSE
						CONCAT(UPPER(CE.tipo), ' ', UPPER(CE.nome), ', ', nro_end)
					END AS nome, 

					CASE WHEN
						F.loteamento != '' THEN CONCAT(UPPER(CB.bairro_nome), ' (', UPPER(F.loteamento), ')') 
					ELSE
						UPPER(CB.bairro_nome)
					END AS bairro_nome, 
					(SELECT GROUP_CONCAT(CONCAT(FC.descricao, ' ', UPPER(FC.observacao)) SEPARATOR '|') AS fone FROM fornecedores_contato AS FC WHERE FC.cod_fornecedor=F.cod_fornecedor AND (FC.tipo_contato='TELEFONE' OR FC.tipo_contato='DDG') ORDER BY FC.controle ASC) AS fones 
				FROM fornecedores AS F 
				LEFT JOIN correios_localidades AS CL ON (F.cidade=CL.id) 
				LEFT JOIN correios_bairros AS CB ON (F.bairro=CB.id) 
				LEFT JOIN correios_enderecos AS CE ON (F.endereco=CE.id) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$query[$key] = $value;
				if($query[$key]->fones != "") {
					$palavras = explode('|' , $query[$key]->fones);
					$total_palavras = count($palavras);

					$fones = "";
					foreach($palavras as $i => $key2) {
						$nro = explode(' ' , $key2);
						$caracteres = strlen($nro[0]);

						if ($caracteres == 10){
							$fones .= '(' . substr($key2, 0, 2) . ') ' . substr($key2, 2, 4) . '-' . substr($key2, 6);
						}
						else{
							$fones .= substr($key2, 0, 4) . ' ' . substr($key2, 4, 3) . '-' . substr($key2, 7);
						}

						if ($i != $total_palavras-1) {
							$fones .= "</br>";
							$caracteres = 0;
						}
					}
					$query[$key]->fones = $fones;
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}
}
?>