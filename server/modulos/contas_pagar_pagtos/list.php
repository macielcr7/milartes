<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'contas_pagar_pagtos';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$pdo = $connection->prepare("SELECT * FROM contas_pagar_pagtos WHERE id=:id");
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
	
		else{
			$pag = new Paginar($_POST);
			
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			
			$result = array();
			
			$buscar->setBusca(array('cod_conta', 'CPP.cod_conta'), $_POST['cod_conta']);
			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('tipo_contato', 'CPP.tipo_contato'), $_POST['tipo_contato'], 'like');
				$buscar->setBusca(array('descricao', 'CPP.descricao'), $_POST['descricao'], 'like');
				$buscar->setBusca(array('observacao', 'CPP.observacao'), $_POST['observacao'], 'like');
			}
			
			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}
			
			$filtro = $buscar->getSql();
			
			$pdo = $connection->prepare("SELECT count(*) AS total FROM contas_pagar_pagtos AS CPP {$filtro}");
			$pdo->execute( $buscar->getArrayExecute() );
			
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			
			$countRow = $query->total;
			
			$sql = "
				SELECT CPP.id, CPP.cod_conta, UPPER(CPP.tipo_pagto) AS tipo_pagto,
					CPP.valor_pago, 
					CPP.data_pagto, 
					CPP.data_cadastro, 
					UPPER(CPP.observacao) AS observacao, 
					UPPER(U.nome) AS cadastrado_por_nome 
				FROM contas_pagar_pagtos AS CPP 
				INNER JOIN usuarios AS U ON (U.id=CPP.cadastrado_por) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit};
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			//echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>