<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'contas_pagar_pagtos';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	if ($_POST['data_pagto'] == "__/__/____" OR $_POST['data_pagto'] == "") {
		$data_pagto = "0000-00-00";
	} else {
		$data_pagto = implode('-', array_reverse(explode('/', $_POST['data_pagto'])));
	}

	try {

		$sql = "SELECT (valor_parcela-(SELECT COALESCE(SUM(valor_pago), 0) FROM contas_pagar_pagtos WHERE cod_conta='$_POST[cod_conta]')) AS saldo_a_pagar FROM contas_pagar WHERE id='$_POST[cod_conta]'";
		$pdo = $connection->prepare($sql);
		$pdo->execute();
		$query = $pdo->fetch(PDO::FETCH_OBJ);

		if($_POST['valor_pago'] > $query->saldo_a_pagar){
			throw new PDOException("Não é possível receber mais do que o saldo à pagar: R$ ".number_format($query->saldo_a_pagar, 2, ',', '.'));
		}


		if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO contas_pagar_pagtos 
					(
						cod_conta,	
						tipo_pagto,	
						valor_pago,	
						data_pagto,
						observacao,	
						data_cadastro,
						cadastrado_por
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_conta'],
				$_POST['tipo_pagto'],
				$_POST['valor_pago'],
				$data_pagto,
				$_POST['observacao'],
				date('Y-m-d H:i:s'),
				$user_id
			);
			$pdo->execute($params);

			$sql ="SELECT (valor_parcela-(SELECT COALESCE(SUM(valor_pago), 0) FROM contas_pagar_pagtos WHERE cod_conta='$_POST[cod_conta]')) AS saldo_a_pagar FROM contas_pagar WHERE id='$_POST[cod_conta]'";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$query = $pdo->fetch(PDO::FETCH_OBJ);

			if($query->saldo_a_pagar == 0){
				$sql2 ="UPDATE contas_pagar SET quitado='S' WHERE id='$_POST[cod_conta]'"; 
				$pdo = $connection->prepare($sql2);
				$pdo->execute();
			}
		}
		else{
			throw new PDOException(ACTION_NOT_FOUND);
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}