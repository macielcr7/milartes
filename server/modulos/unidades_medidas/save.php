<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'unidades_medidas';
	$userMain = $user->getUser();
	$user_id = null;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
				UPDATE unidades_medidas SET 
				sigla = ?,
				unidade_medida = ?,
				ativo = ?,
				alterado_por = ?,
				data_alteracao = ? 
				WHERE id = ?
			");
			$params = array(
				mb_strtoupper($_POST['sigla'], 'UTF-8'),
				mb_strtoupper($_POST['unidade_medida'], 'UTF-8'),
				$_POST['ativo'],
				$user_id,
				date('Y-m-d H:i:s'),
				$_POST['id']
			);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO unidades_medidas 
					(
						sigla,
						unidade_medida,
						ativo,
						data_cadastro,
						cadastrado_por,
						alterado_por,
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				mb_strtoupper($_POST['sigla'], 'UTF-8'),
				mb_strtoupper($_POST['unidade_medida'], 'UTF-8'),
				$_POST['ativo'],
				date('Y-m-d H:i:s'),
				$user_id,
				$user_id,
				date('Y-m-d H:i:s')
			);
		}
		
		$pdo->execute($params);
				
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Registro Salvo com Sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao salvar dados!', 'erro'=>$e->getMessage()));
	}
}