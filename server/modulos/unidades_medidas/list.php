<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'unidades_medidas';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$sql ="SELECT * FROM unidades_medidas AS UM WHERE UM.id=:id"; 
			$pdo = $connection->prepare($sql);
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();

			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_PRODUTOS'){
			$sql = "SELECT UM.id AS id, 
			CONCAT(UPPER(UM.sigla), ' - ', UPPER(UM.unidade_medida)) AS descricao 
				FROM unidades_medidas AS UM WHERE UM.ativo = 'S' ORDER BY UM.sigla ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			//echo json_encode( array('dados'=>$rows) );
			echo json_encode( array('dados'=>$linhas, 'sql'=>$sql) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();
			$where = "";
			if(isset($_POST['query']) and !empty($_POST['query'])){
				$where1 = "WHERE (UM.sigla like '%$_POST[query]%' OR UM.unidade_medida like '%$_POST[query]%')";
				$where2 = " AND (UM.sigla like '%$_POST[query]%' OR UM.unidade_medida like '%$_POST[query]%')";
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_unidade'])) {
					$where = "WHERE UM.ativo='S'".$where2;
				}
				elseif ($_POST['status_unidade'] == 'X') {
					$where = "".$where1;
				}
				else{
					$where = "WHERE UM.ativo='{$_POST['status_unidade']}'".$where2;
				}
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql = "SELECT count(*) AS total FROM unidades_medidas AS UM $where";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT UM.*, DATE_FORMAT(UM.data_cadastro, '%d/%m/%Y ás %H:%i:%s') as data_cadastro, 
				UPPER(U.nome) AS cadastrado_por_nome 
				FROM unidades_medidas AS UM INNER JOIN usuarios AS U ON (U.id=UM.cadastrado_por) 
				$where ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
			echo json_encode($result);
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage(), 'sql'=>$sql));
	}
}
?>