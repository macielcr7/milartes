<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'unidades_medidas';
	try {
		$connection->beginTransaction();

		$pdo = $connection->prepare("UPDATE unidades_medidas SET ativo='N' WHERE id = ?");
		$pdo->execute(array($_POST['id']));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Desativado com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar o Departamento!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}