<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'orcamentos_forro';
	$userMain = $user->getUser();
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}
	$data_hora_atual = date('Y-m-d H:i:s');
	try {
		if($_POST['action'] == 'COPIA_ITEM_ORCAMENTO'){
			$orcamento_nro = $_POST['orcamento_nro'];
			//copia o orcamento
			//INSERT INTO produtos_orcamentos_forro SELECT NULL, id_orcamento, id_produtos_orcamentos_forro_pai, id_produto, id_cor, quantidade, valor_unitario, valor_total, valor_bruto, $novo_item_nro
			$novo_item_nro = $_POST['nro_itens']+1;
			$pdo = $connection->prepare("INSERT INTO produtos_orcamentos_forro SELECT NULL, 
					id_orcamento, 
					id_produtos_orcamentos_forro_pai, 
					id_produto, 
					id_cor, 
					quantidade, 
					valor_unitario, 
					valor_total, 
					valor_bruto, 
					tipo_produto, 
					possui_ambiente, 
					'$novo_item_nro' 
				FROM produtos_orcamentos_forro WHERE id_produtos_orcamentos_forro = ?
			");
			$pdo->execute(array(
				$_POST['id_produtos_orcamentos_forro']
			));

			//pega o novo ID do ítem
			$novo_id_item = $connection->lastInsertId();


			//se possui ambientes
			if ($_POST['possui_ambiente'] == "S"){
				//INSERT INTO ambientes_orcamentos_forro SELECT NULL, id_produtos_orcamentos_forro, ambiente, qtd_pecas, qtd_metros, comprimento, comprimento_real, largura, largura_real, area, descricao_pecas, sn_l, id_ambiente_pai, tipo_acessorio, data_cadastro, cadastrado_por, alterado_por, data_alteracao, observacoes
				$novo_id_ambiente_forro = 0;
				$pdoB2 = $connection->prepare("SELECT 
						ambiente, 
						qtd_pecas, 
						qtd_metros, 
						comprimento, 
						comprimento_real, 
						largura, 
						largura_real, 
						area, 
						descricao_pecas, 
						sn_l, 
						id_ambiente_pai, 
						tipo_acessorio, 
						observacoes 
					FROM ambientes_orcamentos_forro WHERE id_produtos_orcamentos_forro = ?
				");
				$pdoB2->execute(array(
					$_POST['id_produtos_orcamentos_forro']
				));

				while ($row = $pdoB2->fetch(PDO::FETCH_ASSOC)) {
					$valorB22 = $row['ambiente'];
					$valorB23 = $row['qtd_pecas'];
					$valorB24 = $row['qtd_metros'];
					$valorB25 = $row['comprimento'];
					$valorB26 = $row['comprimento_real'];
					$valorB27 = $row['largura'];
					$valorB28 = $row['largura_real'];
					$valorB29 = $row['area'];
					$valorB210 = $row['descricao_pecas'];
					$valorB211 = $row['sn_l'];
					if ($row['id_ambiente_pai'] != 0){
						$row['id_ambiente_pai'] = $novo_id_ambiente_forro;
					}
					$valorB212 = $row['id_ambiente_pai'];
					$valorB213 = $row['tipo_acessorio'];
					$valorB214 = $row['observacoes'];

					$pdoB3 = $connection->prepare("INSERT INTO ambientes_orcamentos_forro (
							id_produtos_orcamentos_forro, 
							ambiente, 
							qtd_pecas, 
							qtd_metros, 
							comprimento, 
							comprimento_real, 
							largura, 
							largura_real, 
							area, 
							descricao_pecas, 
							sn_l, 
							id_ambiente_pai, 
							tipo_acessorio, 
							data_cadastro, 
							cadastrado_por, 
							alterado_por, 
							data_alteracao, 
							observacoes
						) VALUES (
							'{$novo_id_item}', 
							'{$valorB22}', 
							'{$valorB23}', 
							'{$valorB24}', 
							'{$valorB25}', 
							'{$valorB26}', 
							'{$valorB27}', 
							'{$valorB28}', 
							'{$valorB29}', 
							'{$valorB210}', 
							'{$valorB211}', 
							'{$valorB212}', 
							'{$valorB213}', 
							'{$data_hora_atual}', 
							'{$user_id}', 
							'{$user_id}', 
							'{$data_hora_atual}', 
							'{$valorB214}'
						)");
					$pdoB3->execute();
					$novo_id_ambiente_forro = $connection->lastInsertId();

					//se possui observacoes....
					if (!empty($valorB214)){
						$monta_obs = "<b>".$valorB22.": ".$valorB214."</b>";
						$pdoX4 = $connection->prepare("INSERT INTO orcamentos_observacoes (
								orcamento_id, 
								ambiente_id, 
								nro_item, 
								descricao, 
								tipo, 
								cadastrado_por, 
								alterado_por, 
								data_cadastro, 
								data_alteracao
							) VALUES (
								'{$orcamento_nro}', 
								'{$novo_id_ambiente_forro}', 
								'{$novo_item_nro}', 
								'{$monta_obs}', 
								'G', 
								'{$user_id}', 
								'{$user_id}', 
								'{$data_hora_atual}', 
								'{$data_hora_atual}'
							)");
						$pdoX4->execute();
					}
				}
			}

			//gera os custos
			$pdo = $connection->prepare("INSERT INTO vendas_custos SELECT NULL, 
					cod_venda, 
					'{$novo_id_item}',
					cod_produto, 
					cod_cor, 
					quantidade, 
					valor_unitario, 
					acrescimo_mo, 
					acrescimo_produto, 
					valor_total,
					cod_instalador, 
					data_cadastro, 
					cadastrado_por, 
					alterado_por, 
					data_alteracao, 
					observacoes 
				FROM vendas_custos WHERE cod_item_venda = ?
			");
			$pdo->execute(array(
				$_POST['id_produtos_orcamentos_forro']
			));

			echo json_encode(array('success'=>true, 'msg'=>$novo_id_item.' Ítem Copiado com Sucesso'));
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>'Erro...', 'erro'=>$e->getMessage()));
	}
}
