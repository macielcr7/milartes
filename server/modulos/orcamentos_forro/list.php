<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'orcamentos';
		
		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
		
			$sql = "SELECT * FROM orcamentos WHERE id_orcamento=:id";
			$pdo = $connection->prepare($sql);
			
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_AMBIENTES'){
			$buscar->setBusca(array('ambiente', 'ambientes_orcamentos_forro.ambiente'), $_POST['query'], 'like');
			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT ambiente AS id, ambiente AS descricao 
				FROM ambientes_orcamentos_forro
				{$filtro} 
				GROUP BY ambiente
				ORDER BY ambiente ASC
			");
			$pdo->execute($buscar->getArrayExecute());

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['cliente'])){
				$buscar->setBusca(array('id_cliente', 'orcamentos.id_cliente'), $_POST['cliente']);				
			}

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('data_cadastro', 'orcamentos.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_saida']))), 'like');
				$buscar->setBusca(array('id_vendedor', 'orcamentos.id_vendedor'), $_POST['id_vendedor']);
				$buscar->setBusca(array('cliente_descricao', 'orcamentos.cliente_descricao'), $_POST['cliente_descricao'], 'like');
				$buscar->setBusca(array('desconto', 'orcamentos.desconto'), $_POST['desconto']);
				$buscar->setBusca(array('acrescimo', 'orcamentos.acrescimo'), $_POST['acrescimo'], 'like');
				$buscar->setBusca(array('valor_total', 'orcamentos.valor_total'), $_POST['valor_total'], 'like');
				$buscar->setBusca(array('aos_cuidados_de', 'orcamentos.aos_cuidados_de'), $_POST['aos_cuidados_de'], 'like');
				$buscar->setBusca(array('observacoes', 'orcamentos.observacoes'), $_POST['observacoes'], 'like');
			}

			if (isset($_POST['sort'])){
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$sql2 = "SELECT count(*) as total FROM orcamentos {$filtro}";
			$pdo = $connection->prepare($sql2);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql3 = "SELECT orcamentos.id_orcamento, 
				orcamentos.id_cliente, 
				orcamentos.validade_orcamento, 
				orcamentos.tipo_orcamento, 
				orcamentos.id_orcamentista, 
				UPPER(orcamentos.cliente_descricao) as cliente_descricao, 
				orcamentos.data_cadastro, 
				orcamentos.data_venda, 
				UPPER(clientes.nome_completo) as cliente, 
				orcamentos.desconto_porcento, 
				orcamentos.desconto_real, 
				orcamentos.acrescimo_porcento, 
				orcamentos.acrescimo_real, 
				orcamentos.valor_total, 
				orcamentos.valor_bruto, 
				UPPER(orcamentos.aos_cuidados_de) AS aos_cuidados_de, 
				UPPER(orcamentos.observacoes) AS observacoes
			FROM orcamentos LEFT JOIN clientes ON (orcamentos.id_cliente=clientes.cod_cliente) {$filtro} ORDER BY {$sort} {$order} LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql3);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);
			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			//echo json_encode( array('dados'=>$query, 'sql'=>$sql, 'sql2'=>$sql2, 'sql3'=>$sql3) );
		}
	} 
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}	
}
?>