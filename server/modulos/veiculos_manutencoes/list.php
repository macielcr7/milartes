<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'veiculos_manutencoes';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("
				SELECT *, 
				DATE_FORMAT(data_manutencao, '%d/%m/%Y') as data_manutencao, 
					DATE_FORMAT(data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(data_alteracao, '%Y-%m-%d') as data_alteracao_date 
				FROM veiculos_manutencoes
				WHERE id=:id
			");

			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		elseif( isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_TIPO_MANUTENCAO' ){
			$buscar->setBusca(array('tipo_manutencao', 'vm.tipo_manutencao'), $_POST['query'], 'like');
			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT UPPER(vm.tipo_manutencao) AS id, UPPER(vm.tipo_manutencao) AS descricao 
				FROM veiculos_manutencoes AS vm 
				{$filtro} 
				GROUP BY vm.tipo_manutencao 
				ORDER BY vm.tipo_manutencao ASC
			");
			$pdo->execute($buscar->getArrayExecute());

			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();
			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('cod_veiculo', 'vm.cod_veiculo'), $_POST['cod_veiculo']);
				$buscar->setBusca(array('hodometro', 'vm.hodometro'), $_POST['hodometro']);
				$buscar->setBusca(array('tipo_manutencao', 'vm.tipo_manutencao'), $_POST['tipo_manutencao'], 'like');
				$buscar->setBusca(array('valor_manutencao', 'vm.valor_manutencao'), $_POST['valor_manutencao'], 'like');
				$buscar->setBusca(array('data_manutencao', 'vm.data_manutencao'), implode('-', array_reverse(explode('/', $_POST['data_manutencao']))), 'like');
				$buscar->setBusca(array('data_cadastro', 'vm.data_cadastro'), implode('-', array_reverse(explode('/', $_POST['data_cadastro_date'])))." ".$_POST['data_cadastro_time'], 'like');
				$buscar->setBusca(array('cadastrado_por', 'vm.cadastrado_por'), $_POST['cadastrado_por']);
				$buscar->setBusca(array('alterado_por', 'vm.alterado_por'), $_POST['alterado_por']);
				$buscar->setBusca(array('data_alteracao', 'vm.data_alteracao'), implode('-', array_reverse(explode('/', $_POST['data_alteracao_date'])))." ".$_POST['data_alteracao_time'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			if ($sort == 'oficina_nome') {
				$sort = 'F.nome_completo '.$order.', F.razao_social '.$order.', F.nome_fantasia ';
			}
			else{
				$sort = "vm.".trim($sortJson[0]['property']);
			}
			if (isset($_POST['cod_veiculo'])) {
				$buscar->setBusca(array('cod_veiculo', 'vm.cod_veiculo'), $_POST['cod_veiculo']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("
				SELECT count(*) AS total 
				FROM veiculos_manutencoes AS vm
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			$sql = "SELECT vm.*, 
					UPPER(vm.tipo_manutencao) AS tipo_manutencao, 
					F.tipo_fornecedor AS tipo_fornecedor, 
					UPPER(F.nome_completo) AS nome_completo, 
					UPPER(F.razao_social) AS razao_social, 
					UPPER(F.nome_fantasia) AS nome_fantasia,  
					DATE_FORMAT(vm.data_cadastro, '%H:%i:%s') as data_cadastro_time, 
					DATE_FORMAT(vm.data_cadastro, '%Y-%m-%d') as data_cadastro_date, 
					DATE_FORMAT(vm.data_alteracao, '%H:%i:%s') as data_alteracao_time, 
					DATE_FORMAT(vm.data_alteracao, '%Y-%m-%d') as data_alteracao_date 
				FROM veiculos_manutencoes AS vm 
				INNER JOIN fornecedores AS F ON (F.cod_fornecedor=vm.oficina) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}
			";

			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			foreach ($query as $key => $value) {
				$rows[$key] = $value;
				//$rows[$key]->tipo_manutencao = strtoupper($rows[$key]->tipo_manutencao);
				//$rows[$key]->hodometro = number_format($rows[$key]->hodometro, 0, ',', '.');

				if ($rows[$key]->tipo_fornecedor == "F"){
					$rows[$key]->oficina_nome = $rows[$key]->nome_completo;
				}
				else{
					if ($rows[$key]->nome_fantasia != ""){
						$rows[$key]->oficina_nome = $rows[$key]->razao_social."(".$rows[$key]->nome_fantasia.")";
					}
					else{
						$rows[$key]->oficina_nome = $rows[$key]->razao_social;
					}
				}
			}

			$result["total"] = $countRow;
			$result["dados"] = $query;

			echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			//echo json_encode( array('dados'=>$rows, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>