<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'veiculos_manutencoes';
	$data_atual = date('Y-m-d');
	$data_hora_atual = date('Y-m-d H:i:s');

	//$hodometro = str_replace(".", "", $_POST['hodometro']);
	try {
		if($_POST['action'] == 'EDITAR'){
			$user->getAcao($tabela, 'editar');
			$pdo = $connection->prepare("
					UPDATE veiculos_manutencoes SET 
						cod_veiculo = ?, 
						hodometro = ?, 
						tipo_manutencao = ?, 
						valor_manutencao = ?, 
						data_manutencao = ?, 
						detalhamento = ?, 
						oficina = ?, 
						alterado_por = ?, 
						data_alteracao = ? 
 					WHERE id = ?
			");
			$params = array(
				$_POST['cod_veiculo'],
				$_POST['hodometro'],
				strtoupper($_POST['tipo_manutencao']),
				$_POST['valor_manutencao'],
				implode('-', array_reverse(explode('/', $_POST['data_manutencao']))),
				strtoupper($_POST['detalhamento']),
				$_POST['oficina'],
				$user_id,
				$data_hora_atual,
				$_POST['id']
			);
			$pdo->execute($params);
		}
		else if ($_POST['action'] == 'INSERIR'){
			$user->getAcao($tabela, 'adicionar');
			$pdo = $connection->prepare("
				INSERT INTO veiculos_manutencoes 
					(
						cod_veiculo, 
						hodometro, 
						tipo_manutencao, 
						valor_manutencao, 
						data_manutencao,
						detalhamento, 
						oficina,  
						data_cadastro, 
						cadastrado_por, 
						alterado_por, 
						data_alteracao
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['cod_veiculo'], 
				$_POST['hodometro'], 
				strtoupper($_POST['tipo_manutencao']), 
				$_POST['valor_manutencao'], 
				implode('-', array_reverse(explode('/', $_POST['data_manutencao']))),
				strtoupper($_POST['detalhamento']), 
				$_POST['oficina'], 
				$data_hora_atual,
				$user_id,
				$user_id,
				$data_hora_atual
			);

			$pdo->execute($params);
		}
		else{
			throw new PDOException(utf8_encode(ACTION_NOT_FOUND));
		}
		echo json_encode(array('success'=>true, 'msg'=>SAVED_SUCCESS));
	}
	catch (PDOException $e) {
		echo json_encode(array('success'=>false, 'msg'=>ERROR_SAVE_DATA, 'erro'=>$e->getMessage()));
	}
}