<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	try {
		require('../../autoLoad.php');
		$buscar = new Buscar();
		$tabela = 'usuarios';

		if( isset($_POST['action']) AND $_POST['action'] == 'GET_VALUES' ){
			$pdo = $connection->prepare("SELECT * FROM usuarios WHERE id=:id");
			$pdo->bindParam(':id', $_POST['id']);
			$pdo->execute();
		
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			echo json_encode( array('success'=>true, 'dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO'){
			$pdo = $connection->prepare("SELECT id, nome AS descricao FROM usuarios");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO2'){
			$pdo = $connection->prepare("SELECT id, UPPER(nome) AS descricao FROM usuarios WHERE status='1' ORDER BY nome ASC");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_USUARIO_RESPONSAVEL_OS'){
			$pdo = $connection->prepare("SELECT U.id AS id, UPPER(U.nome) AS descricao FROM usuarios AS U WHERE U.status='1' ORDER BY U.nome ASC");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);

			$records = array();
			$records = array(array('id' => '', 'descricao' => 'TODOS OS USUÁRIOS'));
			foreach ($linhas as $key) {
				$records[] = array('id' => $key->id, 'descricao' => $key->descricao);
			}
			echo json_encode( array('dados'=>$records) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'RESPONSAVEL_OS'){
			$sql = "SELECT U.id AS id, UPPER(U.nome) AS descricao FROM usuarios AS U WHERE U.status='1' ORDER BY U.nome ASC";
			$pdo = $connection->prepare($sql);
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);
			echo json_encode( array('dados'=>$linhas) );
		}
		else if(isset($_POST['action']) AND $_POST['action'] == 'LIST_COMBO_VENDAS'){
			$pdo = $connection->prepare("
				SELECT distinct(O.cadastrado_por) AS id, UPPER(U.nome) AS descricao FROM orcamentos AS O INNER JOIN usuarios AS U ON (U.id=O.cadastrado_por) WHERE O.eh_venda='S' AND O.venda_finalizada='N' ORDER BY U.nome ASC
			");
			$pdo->execute();
			$linhas = $pdo->fetchAll(PDO::FETCH_OBJ);

			$records = array();
			$records = array(array('id' => '9999', 'descricao' => 'TODOS OS USUÁRIOS'));
			foreach ($linhas as $key) {
				$records[] = array('id' => $key->id, 'descricao' => $key->descricao);
			}
			echo json_encode( array('dados'=>$records) );
		}
		else{
			$pag = new Paginar($_POST);
			$page 	= $pag->getPage();
			$start	= $pag->getStart();
			$limit	= $pag->getLimit();
			$sort 	= $pag->getSort();
			$order 	= $pag->getOrder();

			$result = array();

			if(isset($_POST['action']) AND $_POST['action'] == 'FILTER'){
				$buscar->setBusca(array('nome', 'usuarios.nome'), $_POST['nome'], 'like');
				$buscar->setBusca(array('perfil_id', 'usuarios.perfil_id'), $_POST['perfil_id']);
				$buscar->setBusca(array('email', 'usuarios.email'), $_POST['email'], 'like');
				$buscar->setBusca(array('login', 'usuarios.login'), $_POST['login'], 'like');
				$buscar->setBusca(array('administrador', 'usuarios.administrador'), $_POST['administrador'], 'like');
				$buscar->setBusca(array('status', 'usuarios.status'), $_POST['status'], 'like');
			}

			if (isset($_POST['sort'])) {
				$sortJson = json_decode( stripslashes($_POST['sort']), true);
				$sort = trim($sortJson[0]['property']);
				$order = trim($sortJson[0]['direction']);
			}

			$filtro = $buscar->getSql();

			$pdo = $connection->prepare("SELECT count(*) AS total 
				FROM usuarios INNER JOIN perfil ON
					(usuarios.perfil_id=perfil.id) 
				{$filtro}
			");
			$pdo->execute( $buscar->getArrayExecute() );
			$query = $pdo->fetch(PDO::FETCH_OBJ);
			$countRow = $query->total;

			if(isset($_POST['action']) AND $_POST['action'] == 'LIST'){
				if (empty($_POST['status_usuario'])) {
					$filtro = "WHERE usuarios.status='1'";
				}
				elseif ($_POST['status_usuario'] == 'X') {
					$filtro = "";
				}
				else{
					$filtro = "WHERE usuarios.status='{$_POST['status_usuario']}'";
				}
			}

			$sql = "SELECT usuarios.*, perfil.perfil 
				FROM usuarios INNER JOIN perfil ON
					(usuarios.perfil_id=perfil.id) 
				{$filtro} 
				ORDER BY {$sort} {$order} 
				LIMIT {$start}, {$limit}";
			$pdo = $connection->prepare($sql);
			$pdo->execute( $buscar->getArrayExecute() );

			$query = $pdo->fetchAll(PDO::FETCH_OBJ);

			$result["total"] = $countRow;
			$result["dados"] = $query;

			//echo json_encode($result);
			$sql = str_replace(array("\r", "\n", "\t", "\v"), '', $sql);
			echo json_encode( array('dados'=>$query, 'sql'=>$sql) );
		}
	}
	catch (PDOException $e) {
		echo json_encode(array('dados'=>array(),'total'=>0, 'erro'=>$e->getMessage()));
	}
}
?>