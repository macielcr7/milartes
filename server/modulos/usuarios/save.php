<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'usuarios';
	$user_id = NULL;
	if($userMain){
		$user_id = $userMain['id'];
	}

	try {
		$connection->beginTransaction();

		if($_POST['action'] == 'EDITAR'){
			$pdo = $connection->prepare("
					UPDATE usuarios SET 
							nome = ?, 
							perfil_id = ?, 
							email = ?, 
							login = ?, 
							administrador = ?, 
							status = ?, 
							alterado_por = ?, 
							webmail_email = ?, 
							webmail_senha = ? 
					WHERE id = ?
			");
			$params = array(
				$_POST['nome'],
				$_POST['perfil_id'],
				$_POST['email'],
				$_POST['login'],
				$_POST['administrador'],
				$_POST['status'],
				$user_id,
				$_POST['webmail_email'],
				$_POST['webmail_senha'],
				$_POST['id']
			);
			$mensagem = "USUÁRIO ALTERADO COM SUCESSO";
		}
		else if ($_POST['action'] == 'INSERIR'){
			$pdo = $connection->prepare("
				INSERT INTO usuarios 
					(
						nome, 
						perfil_id, 
						email, 
						login, 
						senha, 
						administrador, 
						status, 
						cadastrado_por, 
						alterado_por, 
						webmail_email, 
						webmail_senha
					) 
				VALUES 
					(
						?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?
					)
			");
			$params = array(
				$_POST['nome'],
				$_POST['perfil_id'],
				$_POST['email'],
				$_POST['login'],
				md5($_POST['senha']),
				$_POST['administrador'],
				$_POST['status'],
				$user_id,
				$user_id,
				$_POST['webmail_email'],
				$_POST['webmail_senha']
			);
			$mensagem = "USUÁRIO CADASTRADO COM SUCESSO";
		}
		elseif($_POST['action'] == 'ALTERAR_SENHA'){
			$pdo = $connection->prepare("
					UPDATE usuarios SET 
							senha = ? 
					WHERE id = ?
			");
			$params = array(
				md5($_POST['senha']),
				$_POST['id']
			);
			$mensagem = "SENHA DO USUÁRIO ALTERADA COM SUCESSO";
		}

		$pdo->execute($params);
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>$mensagem));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'ERRO AO SALVAR DADOS!', 'erro'=>$e->getMessage()));
	}
}