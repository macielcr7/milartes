<?php
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if($_POST){
	require('../../autoLoad.php');
	$tabela = 'usuarios';
	try {
		$connection->beginTransaction();
/*
Não apagar este comentário
			$data_hora_atual = date('Y-m-d H:i:s');
			$pdo = $connection->prepare("SELECT id, nome FROM usuarios WHERE id='$_POST[id]'");
			$pdo->execute();
	
			$linhas = $pdo->fetch(PDO::FETCH_OBJ);
			$descricao = $linhas->id." - ".$linhas->nome;
			$nome_tabela = utf8_encode('Usuários');
			$nome_campo = utf8_encode('Usuário');
			$pdo = $connection->prepare("
					INSERT INTO logs_sistema (controle, chave, tabela, tabela_cod, acao, campo, alterado_de, alterado_para, usuario, data) 
						VALUES 
					(NULL, md5('$data_hora_atual'), '$nome_tabela', '$_POST[id]', 'Excluiu', '$nome_campo', '$descricao', '', '$user_id', '$data_hora_atual')
			");
			$pdo->execute();
*/
		$pdo = $connection->prepare("UPDATE usuarios SET status='2' WHERE id = ?");
		$pdo->execute(array(
			$_POST['id']
		));
		
		$connection->commit();
		echo json_encode(array('success'=>true, 'msg'=>'Usuário Desativado com sucesso'));
	}
	catch (PDOException $e) {
		$connection->rollBack();
		echo json_encode(array('success'=>false, 'msg'=>'Erro ao Desativar o Usuário!<br><br>'.$e->getMessage(), 'erro'=>$e->getMessage()));
	}
}

?>