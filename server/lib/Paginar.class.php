<?php
class Paginar{
	private $page;
	private $start;
	private $limit;
	private $sort = 'id';
	private $order = 'ASC';

	static function maxLimit($valor, $max=100){
		if($valor > $max){ return $max; } else{ return $valor; }
	}

	function __construct($post){
		$this->page = (int) $post['page'];
		$this->start = (int) $post['start'];
		//$this->limit = (int) self::maxLimit($post['limit']);
		$this->limit = (int) $post['limit'];

		if (isset($_POST['sort'])) {
			$sortJson = json_decode( stripslashes($_POST['sort']), true);
			$sort = trim($sortJson[0]['property']);
			$order = trim($sortJson[0]['direction']);
		}
	}

	function getPage(){
		return $this->page;
	}

	function getStart(){
		return $this->start;
	}

	function getLimit(){
		return $this->limit;
	}

	function getSort(){
		return $this->sort;
	}

	function getOrder(){
		return $this->order;
	}
}