<?php
	function upload($destino, $file) {
		if(isset($file) AND !empty($file['name'])) {
			$file_name = $file['name'];
			$file_type = $file['type'];
			$file_size = $file['size'];
			$file_tmp_name = $file['tmp_name'];
			$error = $file['error'];

			$nome = time().".".$file_name;

			switch ($error){
				case 0:
					$error_msg = false;
					break;
				case 1:
					$error_msg = 'O tamanho do arquivo é maior que o definido nas configurações do PHP!';
					break;
				case 2:
					$error_msg = 'O tamanho do arquivo é maior do que o permitido!';
					break;
				case 3:
					$error_msg = 'O upload não foi concluído!';
					break;
				case 4:
					$error_msg = 'O upload não foi feito!';
					break;
			}

			if($error == 0){
				if(!is_uploaded_file($file_tmp_name)) {
					$error = 1;
					$error_msg = 'Erro ao processar arquivo!';
				}
				else {
					if(!move_uploaded_file($file_tmp_name, $destino."/".$nome)) {
						$error = 1;
						$erro_msg = 'Não foi possível salvar o arquivo!';
					}
					else {
						$error_msg = $nome;
					}
				}
			}
		}
		else {
			$error = 5;
			$error_msg = "Arquivo ausente";
		}

		return array('erro'=> $error, 'msg'=> $error_msg);
	}
?>