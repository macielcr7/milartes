<?php
class Buscar{
	public $sql = array();
	public $arrayExecute = array();
	public $filtro = 'WHERE';
	public $filtro2 = 'HAVING';

	function __construct($boolean=true){
		if($boolean == false){
			$this->filtro = 'AND';
			$this->filtro2 = 'AND';
		}
	}

	function setBusca($campoArray, $valor, $metodo=null, $valor2=null){
		$paramCampo = $campoArray[0];
		$campoBanco = $campoArray[0];

		if(count($campoArray) == 2){
			$campoBanco = $campoArray[1];
		}
		$valor = trim($valor);
		$valor2 = trim($valor2);
		if(!empty($valor) OR $valor == '0'){
			if($metodo == 'like'){
				$this->sql[] = "{$campoBanco} LIKE :{$paramCampo}";
				$this->arrayExecute[':'.$paramCampo] = '%'.$valor.'%';
			}
			else if($metodo == 'ilike'){
				$this->sql[] = "{$campoBanco} ILIKE :{$paramCampo}";
				$this->arrayExecute[':'.$paramCampo] = '%'.$valor.'%';
			}
			else if($metodo == 'in'){
				$this->sql[] = "{$campoBanco} IN (:{$paramCampo})";
				$this->arrayExecute[':'.$paramCampo] = $valor;
			}
			else if($metodo == 'not_in'){
				$this->sql[] = "{$campoBanco} NOT IN (:{$paramCampo})";
				$this->arrayExecute[':'.$paramCampo] = $valor;
			}
			else if($metodo == 'date'){
				$this->sql[] = "TO_CHAR({$campoBanco}, 'YYYY-MM-DD')::date = :{$paramCampo}";
				$this->arrayExecute[':'.$paramCampo] = $valor;
			}
			else if($metodo == 'isnull'){
				$this->sql[] = "{$paramCampo} IS NULL";
				//$this->arrayExecute[':'.$paramCampo] = $valor;
			}
			else if($metodo == 'diff'){
				$this->sql[] = "{$campoBanco} != :{$paramCampo}";
				$this->arrayExecute[':'.$paramCampo] = $valor;
			}
			else if($metodo == 'maior'){
				$this->sql[] = "({$campoBanco} > :{$paramCampo})";
				$this->arrayExecute[':'.$paramCampo] = $valor;
			}
			else if($metodo == 'AndOr'){
				$this->sql[] = "({$campoBanco} = {$valor} OR {$campoBanco} = {$valor2})";
				//$this->arrayExecute[':'.$paramCampo] = $valor;
				//$this->arrayExecute[':'.$paramCampo] = $valor2;
			}
			else{
				$this->sql[] = "{$campoBanco} = :{$paramCampo}";
				$this->arrayExecute[':'.$paramCampo] = $valor;
			}
		}
	}

	function setBetween($campoArray, $valor1, $valor2){
		$paramCampo = $campoArray[0];
		$campoBanco = $campoArray[0];
			
		if(count($campoArray) == 2){
			$campoBanco = $campoArray[1];
		}
		$this->sql[] = "{$campoBanco} BETWEEN :{paramCampo}_inicial AND :{paramCampo}_final";
		$this->arrayExecute[":{paramCampo}_inicial"] = $valor1;
		$this->arrayExecute[":{paramCampo}_final"] = $valor2;
	}
	
	function setSql($sql){
		$this->sql[] = $sql;
	}
	function setArrayExecute($paramCampo, $valor){
		$this->arrayExecute[':'.$paramCampo] = $valor;
	}

	function getSql(){
		if(count($this->sql) > 0){
			return $this->filtro.' '.implode($this->sql, ' AND ');
		} else
			return null;
	}

	function getSql2(){
		if(count($this->sql) > 0){
			return $this->filtro2.' '.implode($this->sql, ' AND ');
		} else
			return null;
	}

	function getArrayExecute(){
		if(count($this->arrayExecute) > 0){
			return $this->arrayExecute;
		} else
			return null;
	}
}