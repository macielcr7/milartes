<?php 

define('REMOVED_SUCCESS', 'Removido com Sucesso');
define('DESATIVADO_SUCESSO', 'Desativado com Sucesso');
define('SAVED_SUCCESS', 'Salvo com Sucesso');
define('ERRO_DELETE_DATA', 'Erro ao Deletar Dados');
define('ERROR_SAVE_DATA', 'Erro ao Salvar Dados');
define('ACTION_NOT_FOUND', 'Ação Não Encontrada');

function formatObservacao($observacao, $usuario){
	if( !empty($observacao) ){
		$autenticacao = date("d/m/Y H:i:s").' - '.$usuario->getNome();
		$observacao = $observacao."\n".$autenticacao."\n\n";
	} else{
		$observacao = null;
	}
	return $observacao;
}

function formatData($data){
	return implode('-', array_reverse(explode('/',$data)));
}

function mascara($mascara, $string){
	$string = str_replace(" ","",$string);
	for($i=0;$i<strlen($string);$i++){
		$mascara[strpos($mascara,"#")] = $string[$i];
	}
	return $mascara;
}

function getMsgError($error){
	if($error[1]==1451){
		return utf8_encode("Este Registro está relacionado e não pode ser deletado");
	}
	else if($error[1]==1062){
		preg_match("/'(.*)' for key/", $msg, $matches);
		$entry = $matches[1];
		return utf8_encode("A Entrada '$entry' Já Existe!");
	}
	else{
		return ERRO_DELETE_DATA;
	}
}

function valorPorExtenso($valor=0, $complemento=true) {
	$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
	$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
 
	$c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
	$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
	$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
	$u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
 
	$z=0;
 
	$valor = number_format($valor, 2, ".", ".");
	$inteiro = explode(".", $valor);
	for($i=0;$i<count($inteiro);$i++)
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
			$inteiro[$i] = "0".$inteiro[$i];
 
	// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;) 
	$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
	for ($i=0;$i<count($inteiro);$i++) {
		$valor = $inteiro[$i];
		$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
		$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
		$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
	
		$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
		$t = count($inteiro)-1-$i;
		if ($complemento == true) {
			$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
			if ($valor == "000")$z++; elseif ($z > 0) $z--;
			if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t]; 
		}
		if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
	}
 
 	$rt = mb_strtoupper($rt, 'UTF-8');
	return($rt ? $rt : "zero");
}

function data_extenso($Data){
	$arrDaysOfWeek = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
	$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	$intDayOfWeek = date('w',strtotime($Data));
	$intDayOfMonth = date('d',strtotime($Data));
	$intMonthOfYear = date('n',strtotime($Data));
	$intYear = date('Y',strtotime($Data));

	// Formato a ser retornado
	return $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;
}

function data_extenso_dia($Data){
	$arrDaysOfWeek = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
	$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	$intDayOfWeek = date('w',strtotime($Data));
	$intDayOfMonth = date('d',strtotime($Data));
	$intMonthOfYear = date('n',strtotime($Data));
	$intYear = date('Y',strtotime($Data));

	// Formato a ser retornado
	return $intDayOfMonth;
}

function data_extenso_mes($Data){
	$arrDaysOfWeek = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
	$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	$intDayOfWeek = date('w',strtotime($Data));
	$intDayOfMonth = date('d',strtotime($Data));
	$intMonthOfYear = date('n',strtotime($Data));
	$intYear = date('Y',strtotime($Data));

	// Formato a ser retornado
	return $arrMonthsOfYear[$intMonthOfYear];
}

function data_extenso_ano($Data){
	$arrDaysOfWeek = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
	$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	$intDayOfWeek = date('w',strtotime($Data));
	$intDayOfMonth = date('d',strtotime($Data));
	$intMonthOfYear = date('n',strtotime($Data));
	$intYear = date('Y',strtotime($Data));

	// Formato a ser retornado
	return $intYear;
}

function formatar ($string, $tipo = ""){
	$string = preg_replace("[^0-9]", "", $string);
	if (!$tipo)
	{
		switch (strlen($string))
		{
			case 10:    $tipo = 'fone';     break;
			case 8:     $tipo = 'cep';      break;
			case 11:    $tipo = 'cpf';      break;
			case 14:    $tipo = 'cnpj';     break;
		}
	}
	switch ($tipo)
	{
		case 'fone':
			$string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) . 
				'-' . substr($string, 6);
		break;
		case 'cep':
			$string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
		break;
		case 'cpf':
			$string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . 
				'.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
		break;
		case 'cnpj':
			$string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . 
				'.' . substr($string, 5, 3) . '/' . 
				substr($string, 8, 4) . '-' . substr($string, 12, 2);
		break;
		case 'rg':
			$string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . 
				'.' . substr($string, 5, 3);
		break;
	}
	return $string;
}

function parentsCategorias($connection, $item, $dataRecursive = array()){
	if($item['id_parent']>0){
		$pdo = $connection->prepare("
			SELECT * FROM produtos_categorias WHERE cod_categoria = {$item['id_parent']}
		");
		$pdo->execute();
		$query = $pdo->fetchAll(PDO::FETCH_ASSOC);
		if(count($query)>0){
			$dataRecursive[] = $query[0];

			if($query[0]['id_parent']>0){
				$dataRecursive = parentsCategorias($connection, $query[0], $dataRecursive);
			}			
		}
	}

	return $dataRecursive;
}

function processNomeCategoriaParent($dataRecursive=array()){
	$nome = '';
	$dataRecursive = array_reverse($dataRecursive);
	foreach ($dataRecursive as $k => $val) {
		if($k>0){
			$nome .= ' > ';
		}

		$nome .= $val['descricao'];
	}

	return $nome;
}

function childsCategorias($connection, $item){
	$pdo = $connection->prepare("
		SELECT *, cod_categoria AS id, UPPER(descricao) AS descricao FROM produtos_categorias WHERE id_parent = {$item['cod_categoria']}
	");
	$pdo->execute();
	$query = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	$data = array();
	if(count($query)>0){
		foreach ($query as $i => $categoria) {
			$categoria['attrs'] = attrsCategoria($connection, $categoria);
			$categoria['childs'] = childsCategorias($connection, $categoria);
			$data[] = $categoria;
		}
	}

	return $data;
}

function attrsCategoria($connection, $item){
	$pdo = $connection->prepare("
		SELECT ac.id_atributo, a.atributo FROM atributos_categoria as ac
		INNER JOIN atributos as a ON(ac.id_atributo=a.id_atributos)
		WHERE ac.id_categoria = {$item['cod_categoria']};
	");
	$pdo->execute();
	return $pdo->fetchAll(PDO::FETCH_ASSOC);
}

function getChilds($item, $espace='', $dataCombo=array()){
	foreach ($item['childs'] as $k => $val) {
		$espace = ' └ ';
		$dataCombo[] = array(
			'id'			=> $val['cod_categoria'],
			//'descricao'		=> $val['descricao'],
			'descricao'=> $espace.$val['descricao'],
			'descricao_list'=> $espace.$val['descricao'],
			'attrs'			=> $val['attrs']
		);

		if(count($val['childs'])>0){
			//$espace .= $val['descricao'];
			$dataCombo = getChilds($val, $espace, $dataCombo);
		}
	}

	return $dataCombo;
}

function processCombobox($data){
	$dataCombo = array();
	foreach ($data as $key => $value) {
		$espace = $value['descricao'];
		$dataCombo[] = array(
			'id'			=> $value['id'],
			'descricao'		=> $value['descricao'],
			'descricao_list'=> $value['descricao'],
			'attrs'			=> $value['attrs']
		);
		
		$dataCombo = getChilds($value, $espace, $dataCombo);
	}

	return $dataCombo;
}

function getChildsTree($value){
	$childs = array();
	foreach ($value['childs'] as $k => $val) {
		if(count($val['childs'])>0){
			$leaf = false;
			$ch = getChildsTree($val);
		}
		else{
			$leaf = true;
			$ch = array();
		}
		
		$childs[] = array(
			'cod_categoria'	=> $val['cod_categoria'],
			'text'			=> $val['descricao'],
			'leaf'			=> $leaf,
			'children'		=> $ch
		);
	}

	return $childs;
}

function processTree($data){
	$dataTree = array();
	foreach ($data as $key => $value) {
		$leaf = count($value['childs'])>0 ? false : true;

		$dataTree[] = array(
			'cod_categoria'	=> $value['cod_categoria'],
			'text'			=> $value['descricao'],
			'leaf'			=> $leaf,
			'children'		=> getChildsTree($value)
		);
	}

	return $dataTree;
}

function ReformatString($strInput) { 
	// Replace accented characters 
	$strInput = str_replace("U00C0", "À", $strInput);
	$strInput = str_replace("U00C1", "Á", $strInput);
	$strInput = str_replace("U00C2", "Â", $strInput);
	$strInput = str_replace("U00C3", "Ã", $strInput);
	$strInput = str_replace("U00C4", "Ä", $strInput);
	$strInput = str_replace("U00C5", "Å", $strInput);
	$strInput = str_replace("U00C6", "Æ", $strInput);
	$strInput = str_replace("U00C7", "Ç", $strInput);
	$strInput = str_replace("U00C8", "È", $strInput);
	$strInput = str_replace("U00C9", "É", $strInput);
	$strInput = str_replace("U00CA", "Ê", $strInput);
	$strInput = str_replace("U00CB", "Ë", $strInput);
	$strInput = str_replace("U00CC", "Ì", $strInput);
	$strInput = str_replace("U00CD", "Í", $strInput);
	$strInput = str_replace("U00CE", "Î", $strInput);
	$strInput = str_replace("U00CF", "Ï", $strInput);
	$strInput = str_replace("U00D1", "Ñ", $strInput);
	$strInput = str_replace("U00D2", "Ò", $strInput);
	$strInput = str_replace("U00D3", "Ó", $strInput);
	$strInput = str_replace("U00D4", "Ô", $strInput);
	$strInput = str_replace("U00D5", "Õ", $strInput);
	$strInput = str_replace("U00D6", "Ö", $strInput);
	$strInput = str_replace("U00D8", "Ø", $strInput);
	$strInput = str_replace("U00D9", "Ù", $strInput);
	$strInput = str_replace("U00DA", "Ú", $strInput);
	$strInput = str_replace("U00DB", "Û", $strInput);
	$strInput = str_replace("U00DC", "Ü", $strInput);
	$strInput = str_replace("U00DD", "Ý", $strInput);

	//Now lower case accents 		
	$strInput = str_replace("U00DF", "ß", $strInput);
	$strInput = str_replace("U00E0", "à", $strInput);
	$strInput = str_replace("U00E1", "á", $strInput);
	$strInput = str_replace("U00E2", "â", $strInput);
	$strInput = str_replace("U00E3", "ã", $strInput);
	$strInput = str_replace("U00E4", "ä", $strInput);
	$strInput = str_replace("U00E5", "å", $strInput);
	$strInput = str_replace("U00E6", "æ", $strInput);
	$strInput = str_replace("U00E7", "ç", $strInput);
	$strInput = str_replace("U00E8", "è", $strInput);
	$strInput = str_replace("U00E9", "é", $strInput);
	$strInput = str_replace("U00EA", "ê", $strInput);
	$strInput = str_replace("U00EB", "ë", $strInput);
	$strInput = str_replace("U00EC", "ì", $strInput);
	$strInput = str_replace("U00ED", "í", $strInput);
	$strInput = str_replace("U00EE", "î", $strInput);
	$strInput = str_replace("U00EF", "ï", $strInput);
	$strInput = str_replace("U00F0", "ð", $strInput);
	$strInput = str_replace("U00F1", "ñ", $strInput);
	$strInput = str_replace("U00F2", "ò", $strInput);
	$strInput = str_replace("U00F3", "ó", $strInput);
	$strInput = str_replace("U00F4", "ô", $strInput);
	$strInput = str_replace("U00F5", "õ", $strInput);
	$strInput = str_replace("U00F6", "ö", $strInput);
	$strInput = str_replace("U00F8", "ø", $strInput);
	$strInput = str_replace("U00F9", "ù", $strInput);
	$strInput = str_replace("U00FA", "ú", $strInput);
	$strInput = str_replace("U00FB", "û", $strInput);
	$strInput = str_replace("U00FC", "ü", $strInput);
	$strInput = str_replace("U00FD", "ý", $strInput);
	$strInput = str_replace("U00FF", "ÿ", $strInput);

	return $strInput;
}

function feriado($data_prevista) {
	$data_prevista_recebto = $data_prevista;
	$partes = explode("-", $data_prevista);

	global $connection;

	$sql = "SELECT data_feriado FROM feriados WHERE data_feriado='{$data_prevista}'";
	$pdo = $connection->prepare($sql);
	$pdo->execute();
	$query = $pdo->fetch(PDO::FETCH_OBJ);

	$total_registros = $pdo->fetchColumn();
	if ($total_registros > 0) {
		$data_prevista_recebto = date("Y-m-d",mktime(0,0,0,$partes[1], $partes[2] + 1, $partes[0]));
		$data_prevista_recebto = feriado($data_prevista_recebto);
	}

	$partes2 = explode("-",$data_prevista_recebto);
	$dia_da_semana = date("w",mktime(0,0,0,$partes2[1], $partes2[2], $partes2[0]));

	//sábado
	if ($dia_da_semana == 6) {
		$data_prevista_recebto2 = date("Y-m-d",strtotime(date("Y-m-d", strtotime($data_prevista_recebto)) . " +2 day"));
		$data_prevista_recebto = feriado($data_prevista_recebto2);
	}

	//domingo
	if ($dia_da_semana == 0) {
		$data_prevista_recebto2 = date("Y-m-d",strtotime(date("Y-m-d", strtotime($data_prevista_recebto)) . " +1 day"));
		$data_prevista_recebto = feriado($data_prevista_recebto2);
	}

	return $data_prevista_recebto;
}

function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("de", "da", "dos", "das", "do", "I", "II", "III", "IV", "V", "VI"))
{
	$string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
	foreach ($delimiters as $dlnr => $delimiter) {
		$words = explode($delimiter, $string);
		$newwords = array();
		foreach ($words as $wordnr => $word) {
			if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
				// check exceptions list for any words that should be in upper case
				$word = mb_strtoupper($word, "UTF-8");
			} elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
				// check exceptions list for any words that should be in upper case
				$word = mb_strtolower($word, "UTF-8");
			} elseif (!in_array($word, $exceptions)) {
				// convert to uppercase (non-utf8 only)
				$word = ucfirst($word);
			}
			array_push($newwords, $word);
		}
		$string = join($delimiter, $newwords);
	}//foreach
	return $string;
}
?>