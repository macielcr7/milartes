<?php
require_once __DIR__."/vendor/autoload.php";
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Database\Eloquent\Model;

class Database {
    function __construct() {
        $capsule = new Capsule;
        $capsule->addConnection([
            "driver" => "mysql",
            "host" => "143.198.188.210",
            "database" => "milartes",
            "username" => "root",
            "password" => "Fsj@2144",
            "charset" => "utf8",
            "collation" => "utf8_unicode_ci",
            "prefix" => "",
        ]);
        $capsule->bootEloquent();
    }
}
new Database();

class Orcamentos extends Model {
    protected $primaryKey = "id_orcamento";
    protected $table = "orcamentos";
    public $timestamps = false;

    protected $fillable = array(
        "validade_orcamento",
        "id_cliente",
        "id_os",
        "id_orcamentista",
        "cliente_descricao",
        "desconto_porcento",
        "desconto_real",
        "acrescimo_porcento",
        "acrescimo_real",
        "valor_bruto",
        
        "valor_total",
        "aos_cuidados_de",
        "observacoes",
        "tipo_orcamento",
        "estado",
        "cidade",
        "bairro",
        "endereco",
        "nro_end",
        "loteamento",
        
        "complemento",
        "ponto_ref",
        "cep",
        "data_cadastro",
        "cadastrado_por",
        "alterado_por",
        "data_alteracao",
        "usuario_salvou",
        "convertido_venda",
        "data_venda",
        
        "eh_venda",
        "fone1",
        "fone2",
        "fone3",
        "fone4",
        "fone5",
        "fone1_obs",
        "fone2_obs",
        "fone3_obs",
        "fone4_obs",
        
        "fone5_obs",
        "venda_finalizada",
        "venda_finalizada_em",
        "venda_finalizada_por",
        "cancelado",
        "cancelado_por",
        "data_cancelado",
        "motivo_cancelamento",
        "venda_cod_orcamento",
        "venda_cod_instalador",
        
        "venda_data_instalacao_inicio",
        "venda_data_instalacao_fim"
    );
}