/**
 * Ux for alerts
 *
 * Fabio Jr. Policeno <fabiojpoli@hotmail.com> 
 * 26/11/2011
 */

Ext.define('ShSolutions.plugins.Alert', {
	msgAlert: null,
	title: null,
	type: null,

	/**
     * Show message of alert in top/center of app
     * @param {String} title Title of message
     * @param {String} msg Message
     * @param {String} type The type of alert (notification, information, success, warning e error)
     */
	alert: function(title, format, type) {
		var me = this,
			content,
			tpl,
			msgBox;
			
		if(!me.msgAlert)
		{
			me.msgAlert = Ext.core.DomHelper.insertFirst(Ext.getBody(), {cls: 'msg-box'}, true);
		}

		content = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
		tpl = '<div class="msg-' + type + '"><h3>' + title + '</h3><p>' + content + '</p></div>';
		me.title = title;
		me.type = type;
		me.msgBox = Ext.core.DomHelper.append(me.msgAlert, tpl, true);
		me.close();
		return me.msgBox;
	},
	
	close: function() {
		var me = this;
		if(me.msgBox) {
			me.msgBox.hide();
			me.msgBox.slideIn('t').ghost('t', {delay: 4000, remove: true});
		}
	}
}, 

function() {
	/**
	 * @class Ext.ux.Alert
	 * Singleton instance of {@link Abstracts.MessageBox}.
	 */
	Ext.ns('ShSolutions.plugins');
	ShSolutions.plugins.Alert = new this();
});

function info(title, msg, type) {
	if(type==null || type==undefined){
		type = 'notification';
	}
	var msg2 = 'Erro Inesperado';
	return ShSolutions.plugins.Alert.alert(title || 'Aviso!', msg || msg2, type);

	//Ext.ux.Alert.alert('Notification', 'Message Notification!', 'notification');
	//Ext.ux.Alert.alert('Information', 'Message Information!', 'information');
	//Ext.ux.Alert.alert('Success', 'Message Success!', 'success');
	//Ext.ux.Alert.alert('Warning', 'Message Warning!', 'warning');
    //Ext.ux.Alert.alert('Error', 'Message Error!', 'error');
};