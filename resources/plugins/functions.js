function verifica_cpf_cnpj ( valor ) {
 
	// Garante que o valor é uma string
	valor = valor.toString();
	
	// Remove caracteres inválidos do valor
	valor = valor.replace(/[^0-9]/g, '');
 
	// Verifica CPF
	if ( valor.length === 11 ) {
		return 'CPF';
	} 
	
	// Verifica CNPJ
	else if ( valor.length === 14 ) {
		return 'CNPJ';
	} 
	
	// Não retorna nada
	else {
		return false;
	}
	
} // verifica_cpf_cnpj
 

function formata_cpf_cnpj( valor ) {
 
	// O valor formatado
	var formatado = false;
	
	// Verifica se é CPF ou CNPJ
	var valida = verifica_cpf_cnpj( valor );
 
	// Garante que o valor é uma string
	valor = valor.toString();
	
	// Remove caracteres inválidos do valor
	valor = valor.replace(/[^0-9]/g, '');
 
 
	// Valida CPF
	if ( valida === 'CPF' ) {
			// Formata o CPF ###.###.###-##
			formatado  = valor.substr( 0, 3 ) + '.';
			formatado += valor.substr( 3, 3 ) + '.';
			formatado += valor.substr( 6, 3 ) + '-';
			formatado += valor.substr( 9, 2 ) + '';
		
	}
	
	// Valida CNPJ
	else if ( valida === 'CNPJ' ) {
			// Formata o CNPJ ##.###.###/####-##
			formatado  = valor.substr( 0,  2 ) + '.';
			formatado += valor.substr( 2,  3 ) + '.';
			formatado += valor.substr( 5,  3 ) + '/';
			formatado += valor.substr( 8,  4 ) + '-';
			formatado += valor.substr( 12, 14 ) + '';
		
	} 
 
	// Retorna o valor 
	return formatado;
	
} // formata_cpf_cnpj

String.prototype.mask = function(m) {
	var m, l = (m = m.split("")).length, s = this.split(""), j = 0, h = "";
	for(var i = -1; ++i < l;)
		if(m[i] != "_"){
			if(m[i] == "\\" && (h += m[++i])) continue;
			h += m[i];
			i + 1 == l && (s[j - 1] += h, h = "");
		}
		else{
			if(!s[j] && !(h = "")) break;
			(s[j] = h + s[j++]) && (h = "");
		}
	return s.join("") + h;
};

function getParams(a){
	var b = document.getElementsByTagName("script");
	for (var i = 0; i < b.length; i++){
		if (b[i].src.indexOf("/" + a) > -1){
			var c = b[i].src.split("?").pop().split("&");
			var p = {};
			for (var j = 0; j < c.length; j++){
				var d = c[j].split("=");
				p[d[0]] = d[1]
			}
			return p
		}
	}
	return {}
	
}

function isValidURL(url){
	var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

	if(RegExp.test(url)){
		return true;
	}else{
		return false;
	}
}

function isEmail(email){
	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!filter.test(email)){
		return false;
	}
	else return true;
}

function validCPF(cpf){
	while(cpf.length < 11) cpf = "0"+ cpf;
	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++){
		a[i] = cpf.charAt(i);
		if (i < 9) b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
	b = 0;
	c = 11;
	for (y=0; y<10; y++) b += (a[y] * c--);
	if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return false;
	if (cpf == 0) return false;
	return true;
}

function validCNPJ(cnpj){
	var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
	digitos_iguais = 1;

	if (cnpj.length < 14 && cnpj.length < 15){
		return false;
	}
	for (i = 0; i < cnpj.length - 1; i++){
		if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
			digitos_iguais = 0;
			break;
		}
	}

	if (!digitos_iguais){
		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0,tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;

		for (i = tamanho; i >= 1; i--){
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2){
				pos = 9;
			}
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0)){
			return false;
		}
		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--){
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2){
				pos = 9;
			}
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1)){
			return false;
		}
		return true;
	}else{
		return false;
	}
}

function validDate(date){
	if(date.length!=10) return false;
	// verificando data
	var data= date;
	var dia= data.substr(0,2);
	var barra1= data.substr(2,1);
	var mes= data.substr(3,2);
	var barra2= data.substr(5,1);
	var ano= data.substr(6,4);
	if(data.length!=10||barra1!="/"||barra2!="/"||isNaN(dia)||isNaN(mes)||isNaN(ano)||dia>31||mes>12)return false;
	if((mes==4||mes==6||mes==9||mes==11) && dia==31)return false;
	if(mes==2&&(dia>29||(dia==29 && ano%4!=0)))return false;
	if(ano < 1900)return false;
	return true;
}


/**
 * Modificações no Button
 * */

Ext.override(Ext.button.Button, {
	listeners: {
		click: function(button){
			if(typeof button.up != 'undefined'){
				var tool = button.up('toolbar');
				if(tool!=null && tool!=undefined){
					button.setDisabled(true);

					setTimeout(function(){
						if(button.getEl()){
							button.setDisabled(false);
							//sandri
						}
					}, 50);
				}				
			}
		}
	},

	initComponent: function () {
		this.callParent();
	}
});


/**
 * Modificações no Combo
 * */

Ext.override(Ext.form.field.ComboBox, {
	forceSelection: true,
	editable: true,
	displayField: 'descricao',
	queryCaching: false,
	queryMode: 'local',
	valueField: 'id',
	typeAhead: true,
	triggerAction: 'all',
	lazyRender: true,
	listeners: {
		beforeselect: function(combo){
			combo.store.clearFilter(false);
		}
	},

	initComponent: function () {
		this.callParent();
	}
});

/**
 * Modificações no Window
 * */

Ext.override(Ext.window.Window, {
	modal: false,
	autoHeight: true,
	width: 260,
	stateful: false,
	isWindow: true,
	constrainHeader: true,
	border: false,
	closeEnable: true,

	listeners: {
		beforeclose: function(panel){
			if(this.closeEnable==false){
				//return false;
			}
		}
	},

	initComponent: function () {
		this.callParent();
	}
});

/**
 * Modificações no FormPanel
 * */
Ext.override(Ext.form.Panel, {
	border: false,

	initComponent: function () {
		this.callParent();
	}
});

/**
 * Modificações no Store
 * */

Ext.override(Ext.data.Store, {
	listeners: {
		beforeload: function(store){
			store.clearFilter();
		}
	},
	initComponent: function () {
		this.callParent();
	}
});

Ext.override(Ext.form.RadioGroup, {
	labelAlign: 'top',
	labelStyle: 'font-weight: bold;font-size: 11px;',
	columns: 2,

	initComponent: function () {
		this.callParent();
	}
});

Ext.override(Ext.form.field.Number, {
	minValue: 1,
	allowBlank: false,
	decimalPrecision: 12,

	initComponent: function () {
		this.callParent();
	}
});

/**
 * Modificações no Field
 * */

Ext.override(Ext.form.field.Text, {
	labelAlign: 'top',
	allowBlank: false,
	labelStyle: 'font-weight: bold;font-size: 11px;',

	initComponent: function () {
		this.callParent();
	}
});

Ext.override(Ext.form.Labelable, {
	labelAlign: 'top',
	labelStyle: 'font-weight: bold;font-size: 11px;',

	initComponent: function () {
		this.callParent();
	}
});

Ext.override(Ext.grid.Panel, {
	enableColumnHide: true,

initComponent: function () {
	this.callParent();
}
});


Ext.form.field.Base.override({
	setLabel: function (text) {
		if (this.rendered) {
			Ext.get(this.labelEl.id).update(text);
		}
		this.fieldLabel = text;
	}
});


/**
 * Modificações no MessageBox
 * */

Ext.override(Ext.MessageBox, {
	draggable: true,

	initComponent: function () {
		this.callParent();
	}
});


Ext.override(Ext.data.proxy.Proxy, {
	reader: {
		type: 'json',
		messageProperty: 'logout',
		root: 'dados'
	},
	writer: {
		type: 'json',
		messageProperty: 'message',
		writeAllFields: false
	},
	listeners: {
		exception: function(proxy, response, operation){
			var dados = Ext.decode(response.responseText, true);
			if(dados==null){
				info('Erro!', response.responseText);
			}
			else if(dados.logout==true){
				window.location = 'login.php';
			}
			else if(dados.msg){
				info('Aviso!', dados.msg);
			}
		}
	},

	initComponent: function () {
		this.callParent();
	}
});




/**
 * Modificações no Button - Referente a field labelAlign-Top
 * */

Ext.define('ShSolutions.view.ButtonAdd', {
	extend: 'Ext.button.Button',
	alias: 'widget.buttonadd',

	maxHeight: 22,
	iconCls: 'bt_add',
	margins: '13 0 0 5',

	initComponent: function() {
		var me = this;
		me.callParent(arguments);
	}

});

/**
 * Modificações no Window - Medium
 * */

Ext.define('ShSolutions.view.WindowMedium', {
	extend: 'Ext.window.Window',
	alias: 'widget.windowmedium',
	width: 520,

	initComponent: function() {
		var me = this;
		me.callParent(arguments);
	}
});


/**
 * Modificações no Window - Big
 * */

Ext.define('ShSolutions.view.WindowBig', {
	extend: 'Ext.window.Window',
	alias: 'widget.windowbig',

	width: 780,

	initComponent: function() {
		var me = this;
		me.callParent(arguments);
	}
});


Ext.define('ShSolutions.view.ContainerWin', {
	extend: 'Ext.window.Window',
	alias: 'widget.containerwin',
	height: 400,
	stateful: true,
	resizable: true,
	maximizable: true,
	width: 750,
	layout: {
		type: 'fit'
	},

	initComponent: function() {
		var me = this;
		me.callParent(arguments);
	}
});


/*	@brief Converte uma string em formato moeda para float
	@param valor(string) - o valor em moeda
	@return valor(float) - o valor em float
*/
function converteMoedaFloat(valor){
	if(valor === ""){
		valor =  0;
	}else{
		valor = valor.replace(".","");
		valor = valor.replace(",",".");
		valor = parseFloat(valor);
	}
	return valor;
}


Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
/*	@brief Converte um valor em formato float para uma string em formato moeda
	@param valor(float) - o valor float
	@return valor(string) - o valor em moeda
*/
function converteFloatMoeda(valor){
	return (valor).formatMoney(2, ',', '.');
}

function converteFloatMoeda3(valor){
	return (valor).formatMoney(3, ',', '.');
}

function valores_negativo_grid_caixa(val) {
	if (val < 0) {
		return '<span style="color:red;">' + converteFloatMoeda(val) + '</span>';
	}
	else {
		return converteFloatMoeda(val);
	}
	return val;
}