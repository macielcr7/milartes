<html>
<head>
	<title><?php echo $TituloPagina; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="../resources/css/sandri.css" rel="stylesheet" type="text/css">
</head>

<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<script src=globalize.js></script> 
<script src=globalize.cultures.js></script> 
<script type="text/javascript">
var defaultEmptyOK = false;
var whitespace = " \t\n\r";
var decimalPointDelimiter = "."

function isEmpty(s)
{
	return ((s == null) || (s.length == 0))
}

function isWhitespace (s)
{
   var i;

    // Is s empty?
    if (isEmpty(s)) return true;

    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}

function isDigit (c)
{
   return ((c >= "0") && (c <= "9"))
}

function isFloat (s)
{
		var i;
    var seenDecimalPoint = false;

    if (isEmpty(s)) 
       if (isFloat.arguments.length == 1) return defaultEmptyOK;
       else return (isFloat.arguments[1] == true);

    if (s == decimalPointDelimiter) return false;

    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);

        if ((c == decimalPointDelimiter) && !seenDecimalPoint) seenDecimalPoint = true;
        else if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}



function checkInput(i)
{
	if (i.value == "" || isWhitespace(i.value))
	{
		return false
	}
	else
	{
		return true
	}
}


  function Calcular(opcao)
  {
	  var s;

	  if (!checkInput(document.form1.periodo))
		{
			if (checkInput(document.form1.juros) || checkInput(document.form1.parcela) || checkInput(document.form1.montante))
			{
				alert("Preencha 3 valores para calcular");
				return;
			}
			opcao = 1;
	  }
		else
		{
			opcao = 3;
		}

		s = document.form1.periodo.value
		s = s.replace(",", ".")
		if(!isFloat(s))
		{
			alert("N� de vezes deve ser um valor inteiro.")
			document.form1.periodo.focus();
			return
		}
		per_int = parseFloat(s);



		s = document.form1.juros.value
		s = s.replace(",", ".")

		if(!isFloat(s))
		{
			alert("Taxa de Juros deve ser um valor n�merico, tendo a v�rgula(,) como delimitador da parte fracion�ria.");
			document.form1.juros.focus();
			return
		}
		juros_float = parseFloat(s)/100


		s = document.form1.montante.value
		s = s.replace(".", "")
		s = s.replace(".", "")
		s = s.replace(".", "")
		s = s.replace(",", ".")
		if(!isFloat(s))
		{
			alert("Valor do Financiamento deve ser um valor n�merico maior que zero, tendo a v�rgula(,) como delimitador dos centavos.");
			document.form1.montante.focus();
			return
		}
		montante_float = parseFloat(s)



	if (opcao==3)
	{
		var conteudo = "";
		var ix = '';
		var ix2 = '';
		var tot_compra = 0;
		var fator = 0;

		conteudo += '<html>';
		conteudo += '<head>';
		conteudo += '<title>C�lculo de Juros</title>';
		conteudo += '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf8\">';
		conteudo += '<link href=\"../resources/css/sandri.css\" rel=\"stylesheet\" type=\"text/css\">';
		conteudo += '</head>';

		conteudo += '<body topmargin=\"0\" bottommargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" bgcolor=\"#FFFFFF\">';

		conteudo += '<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" align=\"center\" bgcolor=\"#FFFFFF\" class=\"texto\">';
		conteudo += '<tr>';
		conteudo += '<td valign=\"top\">';
		conteudo += '<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\" class=\"texto\" style=\"border: 5px solid #E5E5E5\">';
		conteudo += '<tr>';
		conteudo += '<td valign=\"top\" style=\"border: 1px solid #000000\">';
		conteudo += '<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" class=\"texto\">';
		conteudo += '<tr>';
		conteudo += '<td width=\"100%\" class=\"titulo_pagina\"> C�LUCLO DE JUROS - VALOR DIGITADO: R$ '+document.form1.montante.value+'</td>';
		conteudo += '</tr>';
		conteudo += '<tr>';
		conteudo += '<td align=\"left\">';
		conteudo += '<table width=\"100%\" border=\"0\" cellpadding=\"6\" cellspacing=\"1\" class=\"texto2\" bgcolor=\"#000000\">';
		conteudo += '<tr bgcolor=\"#F4F4F4\">';
		conteudo += '<td width=\"150\" class=\"titulo_grid4\" align=\"center\">Qtde de Parcelas</td>';
		conteudo += '<td class=\"titulo_grid4\" align=\"center\">Valor da Parcela</td>';
		conteudo += '<td width=\"170\" class=\"titulo_grid4\" align=\"center\">Total da Compra</td>';
		conteudo += '<td width=\"170\" class=\"titulo_grid4\" align=\"center\">Fator</td>';
		conteudo += '</tr>';


		for (ix=1;ix<=per_int;ix++)
		{
			if (ix < 4) {
				parcela_float = montante_float;
				tot_compra = parcela_float;
			}
			else {
				
				ix2 = ix - 3;
				console.log(ix);
				parcela_float = (montante_float*juros_float)/(1 - Math.pow(1/(1+juros_float),ix2));
				parcela_float = Math.round(parcela_float*100)/100;

				tot_compra = ix2*parcela_float;

			}

			parcela_float2 = (1000*juros_float)/(1 - Math.pow(1/(1+juros_float),ix));
			parcela_float2 = Math.round(parcela_float2*100)/100;


			var s = String(parcela_float);
			i = s.indexOf(".");
			if (i != -1)
			{
				s = s.substring(0,i) + "," + s.substring(i+1,s.length);
			}

			tot_compra = tot_compra.toFixed(2);
			tot_compra = tot_compra.toLocaleString();

			fator = parcela_float2/1000;
			fator = fator.toFixed(5);

			var sss = Number(parcela_float);
			sss = Globalize.format(sss, 'n2', 'pt-BR');

			var tot_compra2 = Number(tot_compra);
//			tot_compra2 = tot_compra2.toLocaleString();
			tot_compra2 = Globalize.format(tot_compra2, 'n2', 'pt-BR');


			if (ix % 2 == 0)
			{
				conteudo += '<tr onMouseMove=\"this.style.background=\'#B8CDDC\'\" onMouseOut=\"this.style.background=\'#EEEEEE\'\" bgcolor=\"#EEEEEE\">';
			}
			else
			{
				conteudo += '<tr onMouseMove=\"this.style.background=\'#B8CDDC\'\" onMouseOut=\"this.style.background=\'#FFFFFF\'\" bgcolor=\"#FFFFFF\">';
			}
			conteudo += '<td align="center">'+ix+'</td>';
			conteudo += '<td align="center">'+sss+'</td>';
			conteudo += '<td align="center">'+tot_compra2+'</td>';
			conteudo += '<td align="center">'+fator+'</td>';
			conteudo += '</tr>';
		}
			conteudo += '</table>';

			conteudo += '</td>';
			conteudo += '</tr>';
			conteudo += '</table>';
			conteudo += '<br>';
			conteudo += '<center><a href="calculo_juros.php" style="cursor:hand"><b>VOLTAR</b></a></center>';
			conteudo += '<br>';

			conteudo += '</td>';
			conteudo += '</tr>';
			conteudo += '</table>';
			conteudo += '</td>';
			conteudo += '</tr>';
			conteudo += '</table>';
			conteudo += '</body>';
			conteudo += '</html>';
		window.document.write(conteudo);
		return
	}
}
</script>
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.meio.mask.min.js" type="text/javascript"></script>
<script type="text/javascript"><!--
	(function($){
		$(
			function(){
				$('input:text').setMask();
			}
		);
	})(jQuery);
// -->
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="10" align="center" bgcolor="#FFFFFF" class="texto">
  <tr>
    <td valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" class="texto" style="border: 5px solid #E5E5E5">
        <tr>
          <td valign="top" style="border: 1px solid #000000">

						<table width="100%" border="0" cellspacing="0" cellpadding="6" class="texto">
							<tr>
								<td width="100%" class="titulo_pagina"> C�LUCLO DE JUROS</td>
							</tr>
							<tr>
								<td align="left">

								<table width="100%" border="0" cellspacing="1" cellpadding="2" class="texto">
								<form name="form1" method="post" onSubmit="Calcular();" autocomplete="off">
									<tr>
										<td width="150" bgcolor="#E7E7E7">&nbsp;Valor do Financiamento:</td>
										<td><input name="montante" alt="decimal" size="16" title="Digite o valor financiado" id="montante" value="0,00" onClick="this.select();" class="texto" type="text">&nbsp;&nbsp;&nbsp;<input value="Calcular" onclick="Calcular()" tabindex="5" type="button">

<input name="periodo" id="meses" title="Digite o n�mero de meses" value="12" type="hidden">
<input name="juros" id="juros" title="Digite taxa de juros mensal" value="2,99" type="hidden">
<input name="parcela" id="prestacao" title="Digite valor da presta��o" value="" type="hidden">
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
										</td>
									</tr>
								</form>
								</table>

<script type="text/javascript">
window.onload = function()
{
	document.form1.montante.select();
}
</script>

								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


</body>
</html>