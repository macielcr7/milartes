<html>
<head>
	<title>Tabela de Pre�o</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="../resources/css/sandri.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#FFFFFF">
<div class="texto">


	<b><a href="#rodapes">RODAP�S/FILETES</a></b><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#rodapes_staluzia">SANTA LUZIA</a><br><br><br>


	<b><a href="#pisos_laminados">PISOS LAMINADOS</a></b><br>
	&nbsp;&nbsp;&nbsp;<a href="#laminados_nacionais">NACIONAIS</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#laminados_eucafloor">EUCAFLOOR</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#laminados_durafloor">DURAFLOOR</a><br><br><br>


	<b><a href="#persianas">PERSIANAS</a></b><br>
	&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux">SANTLUX</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux_vertical">VERTICAL</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux_horizontal">HORIZONTAL</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux_rolo">ROL�</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux_romana">ROMANA</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux_cascata">ROMANA CASCATA</a><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#persianas_santlux_vertical">PAINEL</a><br><BR>


</div>
<br><br><br><br><br><br><br><br>



<div class="texto" align="center"><a name="rodapes"></a><b>RODAP�S/FILETES</b></div><br>
<table width="100%" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="80" class="titulo_grid2" align="left"><b>Fornecedor</b></td>
		<td width="80" class="titulo_grid2" align="center"><b>C�digo Fornecedor</b></td>
		<td class="titulo_grid2" align="left"><b>Descri��o</b></td>
		<td width="50" class="titulo_grid2" align="center"><b>Altura</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Espessura</b></td>
		<td width="70" class="titulo_grid2" align="center"><b>Tamanho Barra</b></td>
		<td width="50" class="titulo_grid2" align="center"><b>Qtde M�nima</b></td>
		<td width="80" class="titulo_grid2" align="center"><b>Valor de Venda</b></td>
		<td width="90" class="titulo_grid2" align="center"><b>Valor de Venda Instalado</b></td>
	</tr>
<?php
$fundo1 = "#FFFFFF";
$fundo2 = "#EEEEEE";
$fundo = $fundo1;
$frete_rodape = 1.05;//1.05 = 5%
$margem_rodape_rodaflex = 1.70;//1.40 = 40%
$margem_rodape_rodaflex2 = 1.70;//1.40 = 40%
$margem_rodape_berger = 1.70;//1.40 = 40%
$margem_rodape_berger2 = 1.70;//1.40 = 40%
$margem_rodape_staluzia = 2.00;
$margem_rodape_staluzia2 = 2.00;
$margem_mao_obra_rodape = 1.30;
$mao_obra_rodape_poliestireno = 5.50;
$mao_obra_rodape_poliestireno2 = 7.00;
$mao_obra_rodape_eva = 3.00;
$mao_obra_rodape_eva2 = 4.00;
$cola_rodape_eva = 0.50;

?>

	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>

	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left"><a name="rodapes_poliestireno"></a><a name="rodapes_staluzia"></a>SANTA LUZIA</td>
		<td align="center">01 RP</td>
		<td align="left">MODELO 01 - BRANCO</td>
		<td align="center">12,3 CM</td>
		<td align="center">2,0 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((54.20/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((54.20/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">441 RP</td>
		<td align="left">MODELO 441 - BRANCO</td>
		<td align="center">5 CM</td>
		<td align="center">2,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>

	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">442 RP</td>
		<td align="left">MODELO 442 - BRANCO</td>
		<td align="center">6,7 CM</td>
		<td align="center">1,5 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((22.50/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((22.50/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">443 RP</td>
		<td align="left">MODELO 443 - BRANCO</td>
		<td align="center">8,9 CM</td>
		<td align="center">1,7 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((32.60/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((32.60/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">444 RP</td>
		<td align="left">MODELO 444 - BRANCO</td>
		<td align="center">9,6 CM</td>
		<td align="center">2 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((43.60/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((43.60/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">445 RP</td>
		<td align="left">MODELO 445 - BRANCO</td>
		<td align="center">11,6 CM</td>
		<td align="center">2,1 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((62.40/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((62.40/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">446 RP</td>
		<td align="left">MODELO 446 - BRANCO</td>
		<td align="center">7 CM</td>
		<td align="center">1,5 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((22.60/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((22.60/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">446 RP</td>
		<td align="left">MODELO 446 - COLORIDO</td>
		<td align="center">7 CM</td>
		<td align="center">1,5 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((32.20/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((32.20/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">447 RP</td>
		<td align="left">MODELO 447 - BRANCO</td>
		<td align="center">6,5 CM</td>
		<td align="center">1,3 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((22.00/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((22.00/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">450 RP</td>
		<td align="left">MODELO 450 - BRANCO</td>
		<td align="center">8 CM</td>
		<td align="center">1,4 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((28.90/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((28.90/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">451 RP</td>
		<td align="left">MODELO 451 - BRANCO</td>
		<td align="center">7 CM</td>
		<td align="center">1,3 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((24.20/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((24.20/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">454 RP</td>
		<td align="left">MODELO 454 - BRANCO</td>
		<td align="center">10 CM</td>
		<td align="center">1,3 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((39.30/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((39.30/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">455 RP</td>
		<td align="left">MODELO 455 - BRANCO</td>
		<td align="center">5 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((21.40/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((21.40/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">456 RP</td>
		<td align="left">MODELO 456 - BRANCO</td>
		<td align="center">7 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((26.00/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((26.00/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">457 RP</td>
		<td align="left">MODELO 457 - BRANCO</td>
		<td align="center">10 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((38.80/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((38.80/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">459 RP</td>
		<td align="left">MODELO 459 - BRANCO</td>
		<td align="center">7 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((28.30/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((28.30/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">460 RP</td>
		<td align="left">MODELO 460 - BRANCO</td>
		<td align="center">10 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((41.10/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((41.10/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">461 RP</td>
		<td align="left">MODELO 461 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((53.50/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((53.50/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">464 RP</td>
		<td align="left">MODELO 464 - BRANCO</td>
		<td align="center">10 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((44.40/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((44.40/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">466 RP</td>
		<td align="left">MODELO 466 - BRANCO</td>
		<td align="center">3 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((16.60/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((16.60/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">473 RP</td>
		<td align="left">MODELO 473 - BRANCO</td>
		<td align="center">5 CM</td>
		<td align="center">1,3 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((20.00/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((20.00/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">478 RP</td>
		<td align="left">MODELO 478 - BRANCO</td>
		<td align="center">5 CM</td>
		<td align="center">1,5 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((15.60/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((15.60/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">480 RP</td>
		<td align="left">MODELO 480 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((60.00/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((60.00/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">493 RP</td>
		<td align="left">MODELO 493 - BRANCO</td>
		<td align="center">12 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((46.10/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((46.10/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">496 RP</td>
		<td align="left">MODELO 496 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,7 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((65.20/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((65.20/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">502 RP</td>
		<td align="left">MODELO 502 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((55.20/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((55.20/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">503 RP</td>
		<td align="left">MODELO 503 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((56.40/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((56.40/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">505 RP</td>
		<td align="left">MODELO 505 - BRANCO</td>
		<td align="center">20 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((99.30/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((99.30/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">506 RP</td>
		<td align="left">MODELO 506 - BRANCO</td>
		<td align="center">20 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((99.30/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((99.30/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">511 RP</td>
		<td align="left">MODELO 511 - BRANCO</td>
		<td align="center">7 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((40.10/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((40.10/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">512 RP</td>
		<td align="left">MODELO 512 - BRANCO</td>
		<td align="center">10 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((53.80/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((53.80/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">513 RP</td>
		<td align="left">MODELO 513 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((81.40/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((81.40/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">514 RP</td>
		<td align="left">MODELO 514 - BRANCO</td>
		<td align="center">10 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((53.80/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((53.80/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">515 RP</td>
		<td align="left">MODELO 515 - BRANCO</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((81.40/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((81.40/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">3454 RP</td>
		<td align="left">MODELO 3454 - COLORIDO</td>
		<td align="center">10 CM</td>
		<td align="center">1,3 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((66.10/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((66.10/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">3444 RP</td>
		<td align="left">MODELO 3444 - BLACK</td>
		<td align="center">9,6 CM</td>
		<td align="center">2 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((57.50/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((57.50/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">3457 RP</td>
		<td align="left">MODELO 3457 - BLACK</td>
		<td align="center">10 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((59.60/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((59.60/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">3461 RP</td>
		<td align="left">MODELO 3461 - BLACK</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((83.20/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((83.20/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTA LUZIA</td>
		<td align="center">3480 RP</td>
		<td align="left">MODELO 3480 - BLACK</td>
		<td align="center">15 CM</td>
		<td align="center">1,6 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,40 M</td>
		<td align="center">R$ <?php echo number_format((82.10/2.4)*$margem_rodape_staluzia2, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format(((82.10/2.4)*$margem_rodape_staluzia)+($mao_obra_rodape_poliestireno2*$margem_mao_obra_rodape), 2, ',', '.'); ?> M</td>
	</tr>



</table>
<div class="texto">
	<font color="red">
		<b>** N�O VENDER OS FILETES COMO RODAP�, ELES S�O PARA ACABAMENTOS EM FRENTE DE M�VEIS</b><br>
	</font>
</div>
<br><br>








<div class="texto" align="center"><a name="persianas"></a><b>PERSIANAS VERTICAIS</b></div><br>
<table width="100%" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="80" class="titulo_grid2" align="left"><b>Fornecedor</b></td>
		<td width="100" class="titulo_grid2" align="center"><b>Linha</b></td>
		<td class="titulo_grid2" align="left"><b>Cole��o</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�x</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�x</b></td>
		<td width="80" class="titulo_grid2" align="center"><b>�rea M�x m�</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - Altura M�nima</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - �rea M�nima</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - L�mina Avulsa</b></td>
		<td width="90" class="titulo_grid2" align="center"><b>Valor Sem Instala��o</b></td>
	</tr>
<?php
$fundo1 = "#FFFFFF";
$fundo2 = "#EEEEEE";
$fundo = $fundo1;
$frete_persianas = 1.00;//1.05 = 5%
$suporte_persianas = 5.20;
$margem_persianas_santlux = 1.80;//1.40 = 40%
$margem_persianas_lamina_avulsa = 2.00;//3.00 = 300%
$margem_persianas_acessorios = 1.50;//1.40 = 40%
?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left"><a name="persianas_santlux"></a><a name="persianas_santlux_vertical"></a>SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">AMAZON</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((33.55+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">AREZZO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((36.10+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BELINDA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((33.55+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 1144</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 1145</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 1146</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 1430</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 1431</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 1438</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK 2607</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK AMALTHEA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((57.5+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK AREZZO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.48+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK CEDRO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((58.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK EURO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK GALATHEA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((57.50+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK JUTA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.48+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK LINHO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.70*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((50.80+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK MADRI</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(7.11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((81.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK MARAJO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((58.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK MILENIUM</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK NEW AREZZO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((58.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK NUANCE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK PINPOINT</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(7.11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((81.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK SALTA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.70*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((50.80+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK SOLARIS</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((58.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK SP 103</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK SP 34</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.13+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK TOKYO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.70*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((50.80+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK TOQUIO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.70*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((50.80+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK TRESSE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((58.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK TROPICAL</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.08*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((58.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">BK UDINE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.70*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((50.80+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">CEDRO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((39.81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">CONTRAST</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(1.28*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((25.14+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">GRECIA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((36.10+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">LINHO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.68*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((37.44+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">MARAJO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((39.81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">MARROCOS</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.50+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">NATURA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.68*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((37.44+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">NEW AREZZO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.56*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((48.78+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">NUANCE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(1.28*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((25.14+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">NOBLESSE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">2,60 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format((363.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">ORION</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((41.01+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">PANDORA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.78+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">PARIS</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((29.41+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">PE (Somente cor 07 e 08) * (Sem Garantia)</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((33.43+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">PE * (Sem Garantia)</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.78+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">RAMI * (Sem Garantia)</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((33.43+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">REIA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.50+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">SALTA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((32.76+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">SCREEN</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(7.11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((81.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">SOLARIS * (ASPEN CINZA Sem Garantia)</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((33.55+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">SP 103</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.78+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">SP 34</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.78+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">SP 367</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(3.18*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.78+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">TRESSE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((39.81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL TECIDO</td>
		<td align="left">TROPICAL</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((41.01+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">ARIZONA * (B�sico Fosco)</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((44.74+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">ARIZONA * (Perolado Brilhoso)</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((57.32+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">ASTECA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">AURORA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">BAMBOO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">CONTRACT</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(2.81*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((38.23+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">CRISAL</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((57.75+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">DREAM</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">DUAL</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">ELITE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">FROST</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((46.20+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">JUTA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">LUXURY</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">MADEIRA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">MAIA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">METALLIC</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((57.75+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">NATURALE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">NOVOBRIGHT</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((49.95+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">OFFICE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((46.20+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">OLYMPIC</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">PATINA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">RATTAN</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">REGATTA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((54.32+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">RUSTIC</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((46.20+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">STUDIO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((46.20+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">THAY</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">TORTUGA</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">VINTAGE</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((47.61+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL PVC</td>
		<td align="left">WOOLAND</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">50 CM</td>
		<td align="center">3,50 M</td>
		<td align="center">12,25 M�</td>
		<td align="center">1,50 M</td>
		<td align="center">1,50 M�</td>
		<td align="center">R$ <?php echo number_format(4.01*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((52.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">VERTICAL - BAND�</td>
		<td align="left">BAND� EM ALUM�NIO</td>
		<td align="center">50 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(17.40*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>

</table>
<br>
<div class="texto" align="center"><b>ABERTURAS</b></div>
<br>
<table width="100%" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="25%" class="titulo_grid2" align="left"><b>SANTLUX = LATERAL (A)<br>COLUMBIA = LATERAL</b><br><i>as l�minas recolhem-se para uma das laterais da persiana (para o mesmo lado do comando)</i></td>
		<td width="25%" class="titulo_grid2" align="left"><b>SANTLUX = INVERTIDA (H)<br>COLUMBIA = INVERTIDA</b><br><i>as l�minas se recolhem para o lado oposto ao cord�o/comando</i></td>
		<td width="25%" class="titulo_grid2" align="left"><b>SANTLUX = CENTRAL (C)<br>COLUMBIA = BILATERAL</b><br><i>as l�minas recolhem-se igualmente para as duas laterais da persiana</i></td>
		<td class="titulo_grid2" align="left"><b>SANTLUX = BILATERAL (D)<br>COLUMBIA = CENTRAL</b><br><i>as l�minas recolhem-se para o centro da persiana</i></td>
	</tr>
		<tr  bgcolor="#FFFFFF">
			<td align="center"><img border="0" width="100" height="84" src="imagens/persiana_lateral.gif"></td>
			<td align="center"><img border="0" width="100" height="84" src="imagens/persiana_invertida.gif"></td>
			<td align="center"><img border="0" width="100" height="84" src="imagens/persiana_bilateral.gif"></td>
			<td align="center"><img border="0" width="100" height="84" src="imagens/persiana_central.gif"></td>
	</tr>
</table>

<div class="texto">
	<font color="red">
		<b>** OBSERVA��ES **</b><br>
		<b>01 � �REA M�NIMA A SER COBRADA 1,50 M� E ALTURA M�NIMA FATURADA 1,50 M</b><br>
		<b>02 � O PADR�O DA LATERAL DO BANDO DE ALUM�NIO � DE 11 CM (PARA SUPORTE 4X10), INFORMAR NO PEDIDO QUANDO MAIOR/MENOR</b><br>
		<b>03 � PERSIANA COM MELHOR TRANSPASSE (LAMELA 6,5 CM) ACRESCER 15 %</b><br>
		<b>04 � ACR�SCIMO DE 70% PARA RECORTE DE AR CONDICIONADO</b><br>
		<b>05 � PRAZO DE ENTREGA: AT� 6 DIAS �TEIS, PODENDO SER PRORROGADO CONFORME DISPONIBILIDADE DO ITEM. PVC TEXTURIZADO, GERALMENTE, DE 10 A 15 DIAS �TEIS (+ PRAZO PARA INSTALA��O: GERALMENTE DENTRO DE 7 DIAS)</b><br>
		<b>06 � PERSIANA DE TECIDO E PVC PADR�O COM CORRENTE DE BASE EM PVC NA COR BRANCA</b><br>
		<b>07 � GARANTIA 1 ANO, EXCETO TECIDOS R�STICOS (VER LISTA ABAIXO). O SERVI�O DE GARANTIA � REALIZADO NA SANTLUX, FORA DA EMPRESA N�O � REALIZADO SERVI�O DE GARANTIA</b><br>
		<b>08 � PERSIANA EM �NGULO ACRESCER 70 %</b><br>
		<b>09 � TECIDOS PODEM SOFRER PEQUENA ALTERA��O DE TONALIDADE</b><br>
		<b>10 � PARA COBRAN�A DE PERFIS � COBRAN�A M�NIMA DE 1,00 METRO LINEAR</b><br>
		<b>11 � SUGEST�O: VENDER BK E PVC COM LAMELA 6,5 PARA MELHOR TRANSPASSE ENTRE L�MINAS</b><br>
		<b>12 - TECIDOS FORA DE GARANTIA: PE, RAMI, SOLARIS ASPEN CINZA, BK ASPEN CINZA, BK JUTA, BK LINHO, BK NEW AREZZO CINZA MESCLA e RAMI, BK TROPICAL, NATURA (LINHO) 34 H e 35 H, NEW AREZZO, ORION, SOLARIS TROPICAL (TROPICAL MESCLA)</b><br>
		<b>13 - ESPA�O M�NIMO PARA INSTALAR PERSIANAS DENTRO DE V�O: 8 CM</b>
	</font>
</div>
<br><br><br><br><br><br><br>



<div class="texto" align="center"><b>PERSIANAS HORIZONTAIS - 25MM</b></div><br>
<table width="100%" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="80" class="titulo_grid2" align="left"><b>Fornecedor</b></td>
		<td width="100" class="titulo_grid2" align="center"><b>Linha</b></td>
		<td class="titulo_grid2" align="left"><b>Cole��o</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�x</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�x</b></td>
		<td width="80" class="titulo_grid2" align="center"><b>�rea M�x m�</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - Altura M�nima</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - �rea M�nima</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - Cada L�mina</b></td>
		<td width="90" class="titulo_grid2" align="center"><b>Valor Sem Instala��o</b></td>
	</tr>
<?php
$fundo1 = "#FFFFFF";
$fundo2 = "#EEEEEE";
$fundo = $fundo1;
$frete_persianas = 1.00;//1.05 = 5%
$suporte_persianas = 5.20;
$margem_persianas_santlux = 1.72;//1.40 = 40%
$margem_persianas_columbia = 1.70;//1.40 = 40%
$margem_persianas_acessorios = 1.50;//1.40 = 40%
?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left"><a name="persianas_santlux_horizontal"></a>SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - LISA E METALIZADA (PH 0.18)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.20*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((40.11+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - LISA (PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.30*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((53.50+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - BK LISA (FUROS N�O APARENTES PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.50*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((56.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - TEXTURIZADA (PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.80*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((66.85+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - BK TEXTURIZADA (FUROS N�O APARENTES PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.92*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((73.54+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - PERFURADA (PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.60*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((66.85+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - IMITA��O MADEIRA (PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(1.92*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((90.92+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 25MM - BK IMITA��O MADEIRA (FUROS N�O APARENTES PH 0.21)</td>
		<td align="center">25 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">3,00 M�</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(2.05*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((108.30+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
</table>
<div class="texto">
	<font color="red">
		<b>** CORES DISPON�VEIS **</b><br>
		<b>CORES DISPON�VEIS 25MM L�MINA 0.18 LISA: 005, 026 - 031, 034, 057, 064, 100, 104, 107, 110, 112, 131, 202, 233, 408, 544, 724, 746, 778, 928, 937, 958</b><br>
		<b>CORES DISPON�VEIS 25MM L�MINA 0.21 LISA: 005, 009, 010, 020, 026, 027, 031, 032, 034, 039, 057, 060, 061, 064, 090, 095, 100, 103, 104, 106, 107, 110, 112, 131, 140, 144, 163, 167, 202, 212, 215, 233, 355, 408, 544, 708, 724, 746, 771, 778, 928, 937, 958,  1553, 1603, 4215</b><br>
		<b>CORES DISPON�VEIS 25MM L�MINA 0.21 TEXTURIZADAS: 601, 602, 603, 604, 605, 606, 607</b><br>
		<b>CORES DISPON�VEIS 25MM L�MINA 0.21 PERFURADAS: 10101, 10164, 10167, 10538</b><br>
		<b>CORES DISPON�VEIS 25MM L�MINA 0.21 IMITA��O MADEIRA: 301, 302, 303</b><br>
		<b>PERFIL SUPERIOR (CABE�OTE) E BASE EM GALVALUME  PARA EVITAR OXIDA��O, NAS SEGUINTES CORES: 005, 009, 020, 026, 031, 034, 039, 057, 060, 090, 095, 100, 103, 104, 107, 112, 131, 140, 144, 163, 167, 202, 233, 724, 771, 778, 928, 958, 1553</b><br>

		<BR><b>** OBSERVA��ES 25MM **</b><br>
		<b>01 � �REA M�NIMA A SER COBRADA 1,00 M�</b><br>
		<b>02 � N�O REALIZAMOS MONTAGEM DE PERSIANA HORIZONTAL 25MM BK NA L�MINA DE 0.18 DEVIDO A FRAGILIDADE DO MATERIAL</b><br>
		<b>03 � GARANTIA DA PH (0.21) = 2 ANOS</b><br>
		<b>04 � GARANTIA DA PH (0.18) = 1 ANO, EXCETO OXIDA��O</b><br>
		<b>05 � SOBRE A GARANTIA: N�O SER� CONCEDIDA GARANTIA NO DESGASTE DE CORDA, MAL USO, PE�AS INSTALADAS EM LOCAIS COM HUMIDADE (BANHEIRO, ETC.). O SERVI�O DE GARANTIA � REALIZADO NA SANTLUX, FORA DA EMPRESA N�O � REALIZADO SERVI�O DE GARANTIA</b><br>
		<b>06 � PRAZO DE ENTREGA: AT� 6 DIAS �TEIS, PODENDO SOFRER ALTERA��O</b><br>
		<b>07 � PERSIANAS COM 2 CORES: ACRESCER 10% DO VALOR. PARA MAIS CORES, CONSULTAR PRE�O</b><br>
		<b>08 � MONOCOMANDO HASTE(COMANDO �NICO):  ACRESCER R$ 26,50 POR PE�A, LIMITES: LARG MIN = 45CM, LARG MAX = 2M, ALT MAX = 2M, �REA MMAX = 2,50M�</b><br>
		<b>09 � PARA COBRAN�A DE PERFIS � COBRAN�A M�NIMA DE 1,00 METRO LINEAR</b><br>
		<b>10 � PRODU��O DE 2 OU MAIS PE�AS NO MESMO TRILHO / CABE�OTE, INFORMAR NO PEDIDO. SEM CUSTO</b><br>
		<b>11 � A ALTURA DA PERSIANA RECOLHIDA EQUIPARA-SE A 5% DA ALTURA TOTAL DA PERSIANA</b><br>
		<b>12 - CABO DE A�O: ACRESCER 30%</b><br>
		<b>13 - ESPA�O M�NIMO PARA INSTALAR PERSIANAS 25MM DENTRO DE V�O: 4 CM</b><br>
	</font>
</div>
<br>

<table width="50%" align="center" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="50%" class="titulo_grid2" align="left"><b>Altura da Haste</b></td>
		<td width="50%" class="titulo_grid2" align="center"><b>Altura da Persiana</b></td>
	</tr>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="center">50 CM</td>
		<td align="center">AT� 1M</td>
	</tr>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="center">70 CM</td>
		<td align="center">DE 1,01 AT� 1,50 M</td>
	</tr>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="center">1,00 M</td>
		<td align="center">DE 1,51 AT� 2,00 M</td>
	</tr>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="center">1,20 M</td>
		<td align="center">DE 2,01 AT� 2,50 M</td>
	</tr>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="center">1,50 M</td>
		<td align="center">DE 2,51 AT� 3,00 M</td>
	</tr>
</table>

<br><br><br><br><br><br><br>



<div class="texto" align="center"><b>PERSIANAS HORIZONTAIS - 50MM</b></div><br>
<table width="100%" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="80" class="titulo_grid2" align="left"><b>Fornecedor</b></td>
		<td width="100" class="titulo_grid2" align="center"><b>Linha</b></td>
		<td class="titulo_grid2" align="left"><b>Cole��o</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�x</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�x</b></td>
		<td width="80" class="titulo_grid2" align="center"><b>�rea M�x m�</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - �rea M�nima</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - Cada L�mina</b></td>
		<td width="90" class="titulo_grid2" align="center"><b>Valor Sem Instala��o</b></td>
	</tr>
<?php
$fundo1 = "#FFFFFF";
$fundo2 = "#EEEEEE";
$fundo = $fundo1;
$frete_persianas = 1.00;//1.05 = 5%
$suporte_persianas = 5.20;
$margem_persianas_santlux = 1.65;//1.40 = 40%
$margem_persianas_columbia = 1.70;//1.40 = 40%
$margem_persianas_acessorios = 1.50;//1.40 = 40%
?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - LISA (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,60 M</td>
		<td align="center">2,50 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((95.27+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - LISA (CADAR�O E MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">4,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(2.55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((95.27+75.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - PERFURADA (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,60 M</td>
		<td align="center">2,50 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(2.95*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((119.20+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - PERFURADA (CADAR�O E MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">4,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(2.95*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((119.20+75.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - IMITA��O MADEIRA (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,60 M</td>
		<td align="center">2,50 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.80*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((137.72+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - IMITA��O MADEIRA (CADAR�O E MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">4,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.80*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((137.72+75.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - FRIZZ (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,60 M</td>
		<td align="center">2,50 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.80*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((137.72+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">ALUM�NIO 50MM - FRIZZ (CADAR�O E MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">4,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.80*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((137.72+75.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">MADEIRA NATURAL (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(6.40*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((220.80+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">MADEIRA NATURAL (CADAR�O E MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(6.40*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((220.80+75.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">MADEIRA BAMBOO (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(6.40*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((242.55+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">MADEIRA BAMBOO (CADAR�O E MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(6.40*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((242.55+75.00+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">MADEIRA PAINO (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(6.40*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((242.55+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">MADEIRA SINT�TICA (CADAR�O)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(5.10*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((180.02+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC CURVE (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((88.20+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC CRISTAL (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((117.45+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC NOVO BRIGHT (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((118+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC ARIZONA (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC STREET (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((118+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC RUSTIC (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC CANELADO (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((89+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC VINTAGE (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC KOBE (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC WOODLAND (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC MADEIRA (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">PVC BAMBOO (CADAR�O * MUITO PESADA, PREFER�NCIA COM MONOCOMANDO)</td>
		<td align="center">60 CM</td>
		<td align="center">1,80 M</td>
		<td align="center">40 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">2,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format(3.00*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
		<td align="center">R$ <?php echo number_format((105+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE ALUM�NIO - LISA (L�MINA DUPLA)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(6*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE ALUM�NIO - PERFURADA (L�MINA DUPLA)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(7*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE ALUM�NIO - IMITA��O MADEIRA (L�MINA DUPLA)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE ALUM�NIO - FRIZZ (L�MINA DUPLA)</td>
		<td align="center">60 CM</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE MADEIRA NATURAL</td>
		<td align="center">60 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(26*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE MADEIRA BAMBOO</td>
		<td align="center">60 CM</td>
		<td align="center">1,90 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(26*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE MADEIRA PIANO</td>
		<td align="center">60 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(55*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE MADEIRA SINT�TICA</td>
		<td align="center">60 CM</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(23*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - CURVE</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(7*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - CANELADO</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(7*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - RUSTIC</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - ARIZONA</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - KOBE</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - BAMBOO</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - MADEIRA</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - WOODLAND</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - VINTAGE</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(9*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - STREET</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - NOVOBRIGHT</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">HORIZONTAL</td>
		<td align="left">BAND� DE PVC - CRISTAL</td>
		<td align="center">60 CM</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(11*$margem_persianas_lamina_avulsa, 2, ',', '.'); ?> M</td>
	</tr>

</table>
<div class="texto">
	<font color="red">
		<BR><b>** OBSERVA��ES 50MM **</b><br>
		<b>01 � �REA M�NIMA A SER COBRADA 1,00 M�</b><br>
		<b>02 � GIRO E ACIONAMENTO PADR�O COM CORDAS</b><br>
		<b>03 � MONOCOMANDO: R$ 112,00 POR PERSIANA, COR PADR�O � BRANCA. ACIONAMENTO CORRENTE CONT�NUA (VER TAMANHOS E CORES),  ITEM NA QUAL DEIXA A PE�A MAIS LEVE POR�M MAIS LENTA PARA SUBIR E DESCER. LARG MAX = 60CM, LARG MAX = 1,80M, ALT MIN = 40CM, ALT MAX = 2,40M, �REA MAX = 3,00M�</b><br>
		<b>04 � MOTORIZA��O: CONSULTE!!!</b><br>
		<b>05 � GARANTIA: 1 ANO. A GARANTIA � REALIZADA NA SANTLUX, FORA DA EMPRESA N�O � REALIZADA GARANTIA</b><br>
		<b>06 � PERSIANAS INSTALADAS LADO A LADO: PODE SOFRER LEVE DESNIVELAMENTO DE L�MINAS ENTRE AS PE�AS</b><br>
		<b>07 � AS L�MINAS DE MADEIRA PODEM APRESENTAR UMA PEQUENA DIFEREN�A DE TONALIDADE POR SE TRATAREM DE PRODUTOS NATURAIS. N�O MOLHAR AS L�MINAS DE MADEIRA, POIS A MESMAS PODEM MANCHAR. PODE HAVER DESBOTAMENTO COM A INCID�NCIA DO SOL</b><br>
		<b>08 � PRAZO DE ENTREGA: AT� 10 DIAS �TEIS, PODENDO SOFRER ALTERA��O</b><br>
		<b>09 � PERSIANAS COM 2 CORES: ACRESCER 10% DO VALOR. PARA MAIS CORES, CONSULTE</b><br>
		<b>10 � PARA COBRAN�A DE PERFIS � COBRAN�A M�NIMA DE 1,00 METRO LINEAR</b><br>
		<b>11 � PRODU��O DE 2 OU MAIS PE�AS NO MESMO TRILHO / CABE�OTE, INFORMAR NO PEDIDO. SEM CUSTO</b><br>
		<b>12 - CABO DE A�O: ACRESCER 30%</b><br>
		<b>13 - VENDER OS PRODUTOS DE PVC SEMPRE COM BAND�, DEVIDO A COLORA��O DOS CABE�OTES !!!</b><br>
		<b>14 - ESPA�O M�NIMO PARA INSTALAR PERSIANAS 50MM DENTRO DE V�O: 6 CM</b><br>
		<b>15 - TAMANHOS DAS CORRENTES CONT�NUAS: 0,50 - 0,90 � 1,20 � 1,50 � 1,80 � 2,10 � 2,40</b><br>
		<b>16 - CORES DAS CORRENTES CONT�NUAS: BRANCA � BEGE � MARRON � PRETA - CINZA</b><br>
		<b>17 - PADR�O DE COR DO MONOCOMANDO E CORRENTE CONT�NUA � BRANCA. PARA OUTRAS CORES, INFORMAR NO PEDIDO</b><br>
		<b>18 - FITA 25MM ACRESCENTAR 15%</b><br>
		<b>19 - FITA 35MM ACRESCENTAR 20%</b><br>
		<b>20 - CABO DE A�O ACRESCENTAR 30%</b><br>
	</font>
</div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>



<div class="texto" align="center"><b>PERSIANAS (CORTINAS) ROL�</b></div><br>
<table width="100%" border="0" cellpadding="6" cellspacing="1" class="texto2" bgcolor="#CCCCCC">
	<tr bgcolor="#F4F4F4"> 
		<td width="80" class="titulo_grid2" align="left"><b>Fornecedor</b></td>
		<td width="100" class="titulo_grid2" align="center"><b>Linha</b></td>
		<td class="titulo_grid2" align="left"><b>Cole��o</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Larg M�x</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�n</b></td>
		<td width="60" class="titulo_grid2" align="center"><b>Alt M�x</b></td>
		<td width="80" class="titulo_grid2" align="center"><b>�rea M�x m�</b></td>
		<td width="85" class="titulo_grid2" align="center"><b>Venda - �rea M�nima</b></td>
		<td width="90" class="titulo_grid2" align="center"><b>Valor Sem Instala��o</b></td>
	</tr>
<?php
$fundo1 = "#FFFFFF";
$fundo2 = "#EEEEEE";
$fundo = $fundo1;
$frete_persianas = 1.00;//1.05 = 5%
$suporte_persianas = 20;
$margem_persianas_santlux = 1.65;//1.40 = 40%
$margem_persianas_columbia = 1.70;//1.40 = 40%
$margem_persianas_acessorios = 1.50;//1.40 = 40%
?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left"><a name="persianas_santlux_rolo"></a>SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ACIONAMENTO CORRENTE - LIMITES</td>
		<td align="center">40 CM</td>
		<td align="center">LARGURA DA BOBINA</td>
		<td align="center">20 CM</td>
		<td align="center">2,70 M</td>
		<td align="center">4,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">-</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ACIONAMENTO CORRENTE E MOLA DE AL�VIO (VERIFICAR VALORES) - LIMITES</td>
		<td align="center">75 CM</td>
		<td align="center">LARGURA DA BOBINA</td>
		<td align="center">20 CM</td>
		<td align="center">4,00 M</td>
		<td align="center">6,00 M�</td>
		<td align="center">1,00 M�</td>
		<td align="center">-</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BOX 70 - LIMITES</td>
		<td align="center">30 CM</td>
		<td align="center">1,70 M</td>
		<td align="center">20 CM</td>
		<td align="center">2,20 M</td>
		<td align="center">3,74 M�</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(54*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BOX 70 - LIMITES (SHEER ELEGANCE E SONATURA)</td>
		<td align="center">30 CM</td>
		<td align="center">1,50 M</td>
		<td align="center">20 CM</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(54*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BOX 70 - LIMITES (BK PERSTIGE)</td>
		<td align="center">30 CM</td>
		<td align="center">1,70 M</td>
		<td align="center">20 CM</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(54*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BOX 90 - LIMITES<br>
			<font size="1">HAVER� VARIA��O DE ACORDO COM A ALTURA, LARGURA E O TIPO DE TECIDO... CONSULTAR COM A MONTADORA</font>
		</td>
		<td align="center">30 CM</td>
		<td align="center">2,80 M</td>
		<td align="center">20 CM</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(79*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BAND� INTEGRADO - LIMITES<br>
			<font size="1">HAVER� VARIA��O DE ACORDO COM A ALTURA, LARGURA E O TIPO DE TECIDO... CONSULTAR COM A MONTADORA</font><br>
			<font size="1">LARGURA PARA UM �NICO M�DULO INTEIRO PODER� HAVER VARIA��O... CONSULTAR COM A MONTADORA</font>
		</td>
		<td align="center">30 CM</td>
		<td align="center">2,30 M</td>
		<td align="center">20 CM</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(37*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SUPORTE INTERMEDI�RIO<br>
			<font size="1">MELHOR ACABAMENTO</font>
		</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(13*$margem_persianas_acessorios, 2, ',', '.'); ?> UN</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">GUIA LATERAL<br>
			<font size="1">ALTURA X 2</font>
		</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M</td>
		<td align="center">R$ <?php echo number_format(27*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">GUIA INFERIOR</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M</td>
		<td align="center">R$ <?php echo number_format(27*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">MOLA DE AL�VIO<br>
			<font size="1">ACIMA DE 4 M�</font>
		</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(58*$margem_persianas_acessorios, 2, ',', '.'); ?></td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">TUBO 41/47/50MM<br>
			<font size="1">ACIMA DE 2M LINEARES</font>
		</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(26*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BASE CHATA</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M</td>
		<td align="center">R$ <?php echo number_format(20*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BASE REVESTIDA</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M</td>
		<td align="center">R$ <?php echo number_format(20*$margem_persianas_acessorios, 2, ',', '.'); ?> M</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">EMENDA DE TECIDO<br>
			<font size="1">SOLDA ELETR�NICA</font>
		</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">R$ <?php echo number_format(18*$margem_persianas_acessorios, 2, ',', '.'); ?></td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 2832</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((103+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 2958</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((82+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 2861</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((76+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3007</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((74+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3070</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3218</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((74+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3220</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((85+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3222</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((94+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3529</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((103+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3533</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((94+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3547</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3618</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((132+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3666</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((85+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 3908</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((79+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">AM 4036</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((76+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ANGKOR</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((145+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ARUBA</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((165+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BALI</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((137+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BALI 230</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((121+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BANGCOC</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((119+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BELIZE</td>
		<td align="center">-</td>
		<td align="center">2,10 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((136+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ANGKOR</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((145+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK AM 3533</td>
		<td align="center">-</td>
		<td align="center">1,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((111+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK AM 4108</td>
		<td align="center">-</td>
		<td align="center">1,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((91+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK AUREA</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((110+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK BELLA (APENAS COM GUIAS LATERAIS)</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((110+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK CAMILA</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK LIBIA</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK MADRI</td>
		<td align="center">-</td>
		<td align="center">2,10 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((121+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK MIAMI</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((81+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK MONTEVIDEO (APENAS COM GUIAS LATERAIS)</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((150+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK NAPOLES</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((109+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK PINPOINT (CONSULTAR LARGURAS DISPON�VEIS)</td>
		<td align="center">-</td>
		<td align="center">CONSULTE</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((100+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK PRESTIGE</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((126+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK SHERR ELEGANCE</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((187+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK SHERR ELEGANCE SONATURA</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((187+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK TJ 2606</td>
		<td align="center">-</td>
		<td align="center">1,5 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((111+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">BK VIENA</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((118+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">CETIM DUBAI</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((122+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">CINGAPURA</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((145+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">DAKOTA SEMI-BK</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((107+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">FINE SCREEN 1% (COR BEGE FORA DE LINHA)</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((138+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">HAWAI</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((131+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">HOUSTON</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((114+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ILLUSION</td>
		<td align="center">-</td>
		<td align="center">2,10 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((87+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">JACARTA</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((140+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">LIGHT SCREEN 8%</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((134+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">MANILA</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((123+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">MIL�O</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((164+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">NAPOLES</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((91+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">NEPAL</td>
		<td align="center">-</td>
		<td align="center">2,13 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((106+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">NILO</td>
		<td align="center">-</td>
		<td align="center">2,30 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((77+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">NOBLETE</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((170+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">NOBLETE LUXURY</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((169+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">OLIMPO</td>
		<td align="center">-</td>
		<td align="center">1,95 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((133+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">OPEN SCREEN 15%</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((149+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">PANAMA</td>
		<td align="center">-</td>
		<td align="center">2,10 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((106+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">RAMI B</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((96+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ROLLER</td>
		<td align="center">-</td>
		<td align="center">2,10 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((87+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">ROMAN</td>
		<td align="center">-</td>
		<td align="center">2,10 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((87+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SCREEN 3%</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((119+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SCREEN 5%</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((119+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SCREEN 5% MESCLA</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SCREEN STAR 3%</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((132+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SCREEN 3%</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((119+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SENEGAL</td>
		<td align="center">-</td>
		<td align="center">2,13 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((114+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SHEER ELEGANCE BASIC</td>
		<td align="center">-</td>
		<td align="center">2,25 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((165+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SHEER ELEGANCE LINEN</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((169+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SHEER ELEGANCE SONATURA</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((187+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SHEER ELEGANCE NOBLETE</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((197+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SHEER ELEGANCE BASIC</td>
		<td align="center">-</td>
		<td align="center">2,25 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((165+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SLAT (ACOMPANHA BAND� INTEGRADO)</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((267+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SOLTEX - BRANCO/BEGE</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((206+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SOLTEX</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((203+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SUNSCREEN 3%</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((133+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SUNSCREEN 3% RR</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((133+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SUNSCREEN 3% W</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((133+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">SUNSCREEN 5% RUSTIC</td>
		<td align="center">-</td>
		<td align="center">3,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((134+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">TELA PROJETORA</td>
		<td align="center">-</td>
		<td align="center">CONSULTE</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((107+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">TJ 2604</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((89+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">TJ 2606</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((89+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">TJ 2617</td>
		<td align="center">-</td>
		<td align="center">2,00 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((88+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">VAL�NCIA</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((171+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">VENEZA</td>
		<td align="center">-</td>
		<td align="center">1,83 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((151+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">VISION SHEER</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((267+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">VISION SHEER PREMIUM (ACOMPANHA BAND� INTEGRADO)</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((267+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL�</td>
		<td align="left">VISION SPARK</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((135+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">76.002</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((114+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">162.001</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((87+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">162.008</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((87+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">163.002</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((104+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">184.047</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((128+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">194.056</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((116+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">194.067</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((116+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">194.087</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((116+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">251.008</td>
		<td align="center">-</td>
		<td align="center">2,80 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((129+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.001-1</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((166+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.002-1</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((166+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.002-2</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((166+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.002-6</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.003-1</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((181+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.004-1</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((166+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.004-2</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((181+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.006-2</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.006-4</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((185+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.006-41</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.007-1</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((148+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.008-1</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.008-2</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.008-3</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((148+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.008-4</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.008-5</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((148+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.012-2</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.015</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.017</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.018-5</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((185+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.020-5</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.023</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.024-1</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.024-2</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.025</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.025-2</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.025-6</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.027-1</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.027-1Q</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.028-1</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">400.028-11</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.003-2</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.003-3</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.004-2</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.005-1</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.005-2</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((181+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.005-3</td>
		<td align="center">-</td>
		<td align="center">2,60 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((181+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.006-1</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.010-2</td>
		<td align="center">-</td>
		<td align="center">2,50 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((186+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">401.017</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((130+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.002</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((148+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.004</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((111+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.005-1</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.010</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((111+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.011</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((111+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.012</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((148+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.013</td>
		<td align="center">-</td>
		<td align="center">2,40 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((148+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>
	<tr onMouseMove="this.style.background='#B8CDDC'" onMouseOut="this.style.background='<?php echo $fundo; ?>'" bgcolor="<?php echo $fundo; ?>">
		<td align="left">SANTLUX</td>
		<td align="center">ROL� EMOTIONS</td>
		<td align="left">402.015</td>
		<td align="center">-</td>
		<td align="center">2,20 M</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">-</td>
		<td align="center">1,00 M�</td>
		<td align="center">R$ <?php echo number_format((143+$suporte_persianas)*$margem_persianas_santlux, 2, ',', '.'); ?> M�</td>
	</tr>
	<?php if ($fundo == $fundo1) { $fundo = "#EEEEEE"; } elseif ($fundo == $fundo2) { $fundo = "#FFFFFF"; }?>


	





	







</table>
<div class="texto">
	<font color="red">
		<BR><b>** OBSERVA��ES ROL� **</b><br>
		<b>01 � �REA M�NIMA A SER COBRADA 1,00 M�</b><br>
		<b>02 - BAND� INTEGRADO * LARGURA M�XIMA POR M�DULO 2,20M PODENDO HAVAR VARIA��O, CONSULTE ANTES</b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
		<b>03 - </b><br>
	</font>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</body>
</html>

