/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.Loader.setConfig({
	enabled: true,
	paths: {
		'ShSolutions': 'app',
		'ShSolutions.plugins': 'resources/plugins',
		"Extensible": "resources/plugins/ext",
        "Extensible.example": "resources/example"
	}
});

Ext.require([
	'ShSolutions.controller.Permissoes'
]);

Ext.application({
	name: 'ShSolutions',
	controllers: [
		'Desktop',
		'Clientes',
		'Clientes_Anotacoes',
		'Clientes_Contato',
		'Clientes_Endereco',
		'Adiantamentos',
		'Agenda_Telefonica_Contato',
		'Fornecedores_Anotacoes',
		'Fornecedores_Contato',
		'Fornecedores_Endereco',
		'Logs_Sistema',
		'Contas_Pagar_Pagtos',
		'Os_Respostas',
		'Produto_Composicoes',
		'Orcamentos',
		'Os',
		'Itens_Orcamento',
		'Orcamentos_Observacoes',
		'Vendas_Pagamentos',
		'Caixas',
		'Caixa_Itens',
		'Vendas'
	],
	autoCreateViewport: false,
	winRegistered: new Ext.util.MixedCollection(),
	launch: function() {
		var me = this;
		Ext.create('ShSolutions.view.Desktop');
		
		me.dados = key;
	}
});