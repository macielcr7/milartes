/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboFormasPagtoCategoria', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
					{
						id: '1',
						descricao: 'PADRÃO'
					},				
					{
						id: '2',
						descricao: 'BOLETO BANCÁRIO'
					},
					{
						id: '3',
						descricao: 'CARTÃO DE CRÉDITO'
					}
			]
		}, cfg)]);
	}
});
