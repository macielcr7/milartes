/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipoCorreiosEnderecos2', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'ALAMEDA',
					descricao: 'ALAMEDA'
				},
				{
					id: 'AVENIDA',
					descricao: 'AVENIDA'
				},
				{
					id: 'BECO',
					descricao: 'BECO'
				},
				{
					id: 'ESTRADA',
					descricao: 'ESTRADA'
				},
				{
					id: 'FAZENDA',
					descricao: 'FAZENDA'
				},
				{
					id: 'PARQUE',
					descricao: 'PARQUE'
				},
				{
					id: 'RODOVIA',
					descricao: 'RODOVIA'
				},
				{
					id: 'RUA',
					descricao: 'RUA'
				},
				{
					id: 'RUA PARTICULAR',
					descricao: 'RUA PARTICULAR'
				},
				{
					id: 'SERVID&Atilde;O',
					descricao: 'SERVID&Atilde;O'
				},
				{
					id: 'SITIO',
					descricao: 'SITIO'
				},
				{
					id: 'TRAVESSA',
					descricao: 'TRAVESSA'
				},
				{
					id: 'VIA',
					descricao: 'VIA'
				},
				{
					id: 'VILA',
					descricao: 'VILA'
				}
			]
		}, cfg)]);
	}
});