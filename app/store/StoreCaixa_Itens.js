/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCaixa_Itens', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCaixa_Itens'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCaixa_Itens',
			pageSize: 200,
			model: 'ShSolutions.model.ModelCaixa_Itens',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/caixa_itens/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridCaixa_Itens')){
				Ext.getCmp('GridCaixa_Itens').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridCaixa_Itens')){
				Ext.getCmp('GridCaixa_Itens').getEl().unmask();
			}
		});
	}
});