/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProdutos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelProdutos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreProdutos',
			pageSize: 100,
			model: 'ShSolutions.model.ModelProdutos',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'descricao_completa'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/produtos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridProdutos')){
				Ext.getCmp('GridProdutos').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridProdutos')){
				Ext.getCmp('GridProdutos').getEl().unmask();
			}
		});
	}
});