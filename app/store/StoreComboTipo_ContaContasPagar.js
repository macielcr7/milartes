/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipo_ContaContasPagar', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: '1',
					descricao: 'DINHEIRO'
				},
				{
					id: '2',
					descricao: 'CHEQUE(S)'
				},
				{
					id: '3',
					descricao: 'DEPÓSITO/TRANSFERÊNCIA BANCÁRIA'
				},
				{
					id: '4',
					descricao: 'BOLETO/DUPLICATA'
				},
				{
					id: '5',
					descricao: 'PERMUTA'
				},
				{
					id: '6',
					descricao: 'CONTA CORRENTE'
				}
			]
		}, cfg)]);
	}
});