/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboStatusOsDiferente', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreComboStatusOsDiferente',
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: '1',
					descricao: 'CANCELADO E FINALIZADO'
				},
				{
					id: '2',
					descricao: 'NOVO'
				},
				{
					id: '3',
					descricao: 'PENDENTE'
				},
				{
					id: '4',
					descricao: 'REABERTA'
				}
			]
		}, cfg)]);
	}
});