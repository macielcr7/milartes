/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreUnidades_Medidas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelUnidades_Medidas'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreUnidades_Medidas',
			pageSize: 50,
			model: 'ShSolutions.model.ModelUnidades_Medidas',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'sigla'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/unidades_medidas/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridUnidades_Medidas')){
				Ext.getCmp('GridUnidades_Medidas').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridUnidades_Medidas')){
				Ext.getCmp('GridUnidades_Medidas').getEl().unmask();
				Ext.getCmp('query_unidades_medidas').focus(false, 1000);
			}	
		});
	}
});