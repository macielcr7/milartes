/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreSetor_Estoque', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelSetor_Estoque'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreSetor_Estoque',
			model: 'ShSolutions.model.ModelSetor_Estoque',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/setor_estoque/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridSetor_Estoque')){
				Ext.getCmp('GridSetor_Estoque').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridSetor_Estoque')){
				Ext.getCmp('GridSetor_Estoque').getEl().unmask();
				Ext.getCmp('query_setor_estoque').focus(false, 1000);
			}	
		});
	}
});