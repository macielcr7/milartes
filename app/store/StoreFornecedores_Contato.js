/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreFornecedores_Contato', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelFornecedores_Contato'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreFornecedores_Contato',
			model: 'ShSolutions.model.ModelFornecedores_Contato',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'controle'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/fornecedores_contato/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if((Ext.getCmp('GridFornecedores_Contato')) && (Ext.getCmp('GridFornecedores_Contato').getEl())){
				Ext.getCmp('GridFornecedores_Contato').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if((Ext.getCmp('GridFornecedores_Contato')) && (Ext.getCmp('GridFornecedores_Contato').getEl())){
				Ext.getCmp('GridFornecedores_Contato').getEl().unmask();
			}	
		});
	}
});