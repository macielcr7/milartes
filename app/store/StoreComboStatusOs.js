/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboStatusOs', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreComboStatusOs',
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'NOVO',
					descricao: 'NOVO'
				},
				{
					id: 'CANCELADO',
					descricao: 'CANCELADO'
				},
				{
					id: 'FINALIZADO',
					descricao: 'FINALIZADO'
				},
				{
					id: 'PENDENTE',
					descricao: 'PENDENTE'
				},
				{
					id: 'REABERTA',
					descricao: 'REABERTA'
				}
			]
		}, cfg)]);
	}
});