/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreContas_Pagar', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelContas_Pagar'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreContas_Pagar',
			pageSize: 100,
			model: 'ShSolutions.model.ModelContas_Pagar',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'vencimento'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/contas_pagar/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridContas_Pagar')){
				Ext.getCmp('GridContas_Pagar').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridContas_Pagar')){
				Ext.getCmp('GridContas_Pagar').getEl().unmask();
				Ext.getCmp('query_contas_pagar').focus(false, 200);
			}
		});
	}
});