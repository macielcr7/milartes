/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreVendas_Custos', {
	extend: 'Ext.data.Store',
	requires: [
			'ShSolutions.model.ModelVendas_Custos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreVendas_Custos',
			model: 'ShSolutions.model.ModelVendas_Custos',
			remoteSort: true,
			sorters: [
			{
				direction: 'ASC',
				property: 'id'
			}],
			proxy: {
				type: 'ajax',
			extraParams: {
				action: 'LIST'
			},
			actionMethods: {
				create : 'POST',
				read   : 'POST',
				update : 'POST',
				destroy: 'POST'
			},	
				url : 'server/modulos/vendas_custos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridVendas_Custos')){
				Ext.getCmp('GridVendas_Custos').getEl().mask('Aguarde Carregando Dados...');

				if (userData.perfil_id == 1) {
					Ext.getCmp('GridVendas_Custos').columns[3].setVisible(true);
					Ext.getCmp('GridVendas_Custos').columns[4].setVisible(true);
				}
			}
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridVendas_Custos')){
				Ext.getCmp('GridVendas_Custos').getEl().unmask();
			}
		});
	}
});