/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.store.StoreContas_Bancarias', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelContas_Bancarias'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreContas_Bancarias',
			model: 'ShSolutions.model.ModelContas_Bancarias',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/contas_bancarias/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridContas_Bancarias')){
				Ext.getCmp('GridContas_Bancarias').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridContas_Bancarias')){
				Ext.getCmp('GridContas_Bancarias').getEl().unmask();
				Ext.getCmp('query_contas_bancarias').focus(false, 1000);
			}	
		});
	}
});