/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboSituacaoLocalidadeCorreiosLocalidades', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: '0',
					descricao: 'não codificada em nível de Logradouro'
				},
				{
					id: '1',
					descricao: 'Município codificado em nível de Logradouro'
				},
				{
					id: '2',
					descricao: 'Distrito ou Povoado inserido na codificação em nível de Logradouro'
				}
			]
		}, cfg)]);
	}
});