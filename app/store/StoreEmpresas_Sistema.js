/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreEmpresas_Sistema', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelEmpresas_Sistema'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreEmpresas_Sistema',
			model: 'ShSolutions.model.ModelEmpresas_Sistema',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'cod_empresa'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/empresas_sistema/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridEmpresas_Sistema')){
				Ext.getCmp('GridEmpresas_Sistema').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridEmpresas_Sistema')){
				Ext.getCmp('GridEmpresas_Sistema').getEl().unmask();
			}	
		});
	}
});
