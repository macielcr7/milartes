/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCaixaItensRetiradas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCaixaItensRetiradas',
			pageSize: 200,
			model: 'ShSolutions.model.ModelCaixa_Itens',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
				action: 'GRID_RETIRADAS'
			},
			actionMethods: {
				create : 'POST',
				read   : 'POST',
				update : 'POST',
				destroy: 'POST'
			},	
			url : 'server/modulos/caixa_itens/list.php',
			reader: {
				type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridCaixaItensRetiradas')) && (Ext.getCmp('GridCaixaItensRetiradas').getEl())){
				Ext.getCmp('GridCaixaItensRetiradas').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridCaixaItensRetiradas')) && (Ext.getCmp('GridCaixaItensRetiradas').getEl())){
				Ext.getCmp('GridCaixaItensRetiradas').getEl().unmask();
			}
		});
	}
});