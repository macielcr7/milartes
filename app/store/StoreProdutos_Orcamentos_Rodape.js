
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProdutos_Orcamentos_Rodape', {
	extend: 'Ext.data.Store',
	requires: [
			'ShSolutions.model.ModelProdutos_Orcamentos_Rodape'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreProdutos_Orcamentos_Rodape',
			model: 'ShSolutions.model.ModelProdutos_Orcamentos_Rodape',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'item_nro'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/produtos_orcamentos_rodape/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridProdutos_Orcamentos_Rodape')){
				Ext.getCmp('GridProdutos_Orcamentos_Rodape').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridProdutos_Orcamentos_Rodape')){
				Ext.getCmp('GridProdutos_Orcamentos_Rodape').getEl().unmask();
			}
		});
	}
});
