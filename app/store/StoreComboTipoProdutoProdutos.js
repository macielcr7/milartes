/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipoProdutoProdutos', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelComboLocal'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'ShSolutions.model.ModelComboLocal',
   	        data: [
				{
                    id: 'A',
                    descricao: 'ACESSÓRIO'
                },
                {
					id: 'S',
					descricao: 'SERVIÇO'
				},
                {
                    id: 'P',
                    descricao: 'PRODUTO'
                },
				{
					id: 'I',
					descricao: 'PRODUTO/INSTALADO'
				}
   	        	
   	        ]
        }, cfg)]);
        
    }
});
