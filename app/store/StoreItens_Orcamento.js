/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreItens_Orcamento', {
	extend: 'Ext.data.Store',
	requires: [
			'ShSolutions.model.ModelItens_Orcamento'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreItens_Orcamento',
			model: 'ShSolutions.model.ModelItens_Orcamento',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id_itens_orcamento'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/itens_orcamento/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridItens_Orcamento')){
				Ext.getCmp('GridItens_Orcamento').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridItens_Orcamento')){
				Ext.getCmp('GridItens_Orcamento').getEl().unmask();
			}
		});
	}
});