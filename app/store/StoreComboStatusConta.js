/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboStatusConta', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
					{
						id: 'T',
						descricao: 'TODAS'
					},
					{
						id: 'Q',
						descricao: 'QUITADAS'
					},
					{
						id: 'A',
						descricao: 'EM ABERTO'
					}
			]
		}, cfg)]);
	}
});