/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboProdutos_Categorias', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboCategorias'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboCategorias',
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST_COMBO_CATEGORIAS'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/produtos_categorias/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
	}
});