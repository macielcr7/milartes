/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAtributos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelAtributos'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreAtributos',
			model: 'ShSolutions.model.ModelAtributos',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id_atributos'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/atributos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridAtributos')){
				Ext.getCmp('GridAtributos').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridAtributos')){
				Ext.getCmp('GridAtributos').getEl().unmask();
			}	
		});
	}
});