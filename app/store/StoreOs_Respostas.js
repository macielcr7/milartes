/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreOs_Respostas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs_Respostas'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreOs_Respostas',
			pageSize: 50,
			model: 'ShSolutions.model.ModelOs_Respostas',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'data_cadastro'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},  
				url : 'server/modulos/os_respostas/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridOs_Respostas')){
				Ext.getCmp('GridOs_Respostas').getEl().mask('Aguarde Carregando Dados...');
			}   
		});

		me.on('load', function(){
			if(Ext.getCmp('GridOs_Respostas')){
				Ext.getCmp('GridOs_Respostas').getEl().unmask();
			}
		});
	}
});