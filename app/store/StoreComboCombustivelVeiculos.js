/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboCombustivelVeiculos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'DIESEL',
					descricao: 'DIESEL'
				},
				{
					id: 'ETANOL',
					descricao: 'ETANOL'
				},
				{
					id: 'GASOLINA',
					descricao: 'GASOLINA'
				},
				{
					id: 'GNV',
					descricao: 'GNV'
				}
			]
		}, cfg)]);
	}
});