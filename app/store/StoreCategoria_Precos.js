/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCategoria_Precos', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelCategoria_Precos'
    ],
	
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StoreCategoria_Precos',
            model: 'ShSolutions.model.ModelCategoria_Precos',
            remoteSort: true,
            sorters: [
            	{
            		direction: 'ASC',
            		property: 'id_categoria_precos'
            	}
            ],
   	        proxy: {
            	type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
		    	actionMethods: {
			        create : 'POST',
			        read   : 'POST',
			        update : 'POST',
			        destroy: 'POST'
			    },	
	            url : 'server/modulos/categoria_precos/list.php',
	            reader: {
	            	type: 'json',
	                root: 'dados'
	            }
            }
        }, cfg)]);
        
        
        me.on('beforeload', function(){
        	if(Ext.getCmp('GridCategoria_Precos')){
				Ext.getCmp('GridCategoria_Precos').getEl().mask('Aguarde Carregando Dados...');
			}	
  		});
  		
  		me.on('load', function(){
  			if(Ext.getCmp('GridCategoria_Precos')){
				Ext.getCmp('GridCategoria_Precos').getEl().unmask();
			}	
  		});
    }
});
