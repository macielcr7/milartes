/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProduto_Precos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelProduto_Precos'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreProduto_Precos',
			model: 'ShSolutions.model.ModelProduto_Precos',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id_produto_precos'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/produto_precos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridProduto_Precos') && Ext.getCmp('GridProduto_Precos').getEl()){
				Ext.getCmp('GridProduto_Precos').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridProduto_Precos') && Ext.getCmp('GridProduto_Precos').getEl()){
				Ext.getCmp('GridProduto_Precos').getEl().unmask();
			}	
		});
	}
});
