/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreContas_Pagar_Pagtos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelContas_Pagar_Pagtos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreContas_Pagar_Pagtos',
			model: 'ShSolutions.model.ModelContas_Pagar_Pagtos',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'data_pagto'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/contas_pagar_pagtos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridContas_Pagar_Pagtos')) && (Ext.getCmp('GridContas_Pagar_Pagtos').getEl())){
				Ext.getCmp('GridContas_Pagar_Pagtos').getEl().mask('Aguarde Carregando Dados...');
			}	
		});

		me.on('load', function(){
			if((Ext.getCmp('GridContas_Pagar_Pagtos')) && (Ext.getCmp('GridContas_Pagar_Pagtos').getEl())){
				Ext.getCmp('GridContas_Pagar_Pagtos').getEl().unmask();
			}	
		});
	}
});