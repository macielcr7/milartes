/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboContratosCartaoTipoCalculo', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
					{
						id: 'VISA',
						descricao: 'VISA'
					},
					{
						id: 'MASTERCARD',
						descricao: 'MASTERCARD'
					},
					{
						id: 'HIPERCARD',
						descricao: 'HIPERCARD'
					},
					{
						id: 'GETNET',
						descricao: 'GET NET'
					}
			]
		}, cfg)]);
		
	}
});
