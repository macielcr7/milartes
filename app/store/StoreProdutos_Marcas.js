/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.store.StoreProdutos_Marcas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelProdutos_Marcas'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreProdutos_Marcas',
			pageSize: 50,
			model: 'ShSolutions.model.ModelProdutos_Marcas',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'descricao'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/produtos_marcas/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridProdutos_Marcas')){
				Ext.getCmp('GridProdutos_Marcas').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridProdutos_Marcas')){
				Ext.getCmp('GridProdutos_Marcas').getEl().unmask();
				Ext.getCmp('query_produtos_marcas').focus(false, 1000);
			}	
		});
	}
});