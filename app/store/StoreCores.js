/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCores', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelCores'
    ],
	
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StoreCores',
            model: 'ShSolutions.model.ModelCores',
            remoteSort: true,
            sorters: [
            	{
            		direction: 'ASC',
            		property: 'id_cores'
            	}
            ],
   	        proxy: {
            	type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
		    	actionMethods: {
			        create : 'POST',
			        read   : 'POST',
			        update : 'POST',
			        destroy: 'POST'
			    },	
	            url : 'server/modulos/cores/list.php',
	            reader: {
	            	type: 'json',
	                root: 'dados'
	            }
            }
        }, cfg)]);
        
        
        me.on('beforeload', function(){
        	if(Ext.getCmp('GridCores')){
				Ext.getCmp('GridCores').getEl().mask('Aguarde Carregando Dados...');
			}	
  		});
  		
  		me.on('load', function(){
  			if(Ext.getCmp('GridCores')){
				Ext.getCmp('GridCores').getEl().unmask();
			}	
  		});
    }
});
