/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreClientes_Logs_Sistema', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelLogs_Sistema'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreClientes_Logs_Sistema',
			pageSize: 50,
			model: 'ShSolutions.model.ModelLogs_Sistema',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'controle'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/logs_sistema/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridClientes_Logs_Sistema')) && (Ext.getCmp('GridClientes_Logs_Sistema').getEl())){
					Ext.getCmp('GridClientes_Logs_Sistema').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridClientes_Logs_Sistema')) && (Ext.getCmp('GridClientes_Logs_Sistema').getEl())){
				Ext.getCmp('GridClientes_Logs_Sistema').getEl().unmask();
			}
		});
	}
});