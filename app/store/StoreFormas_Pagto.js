/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreFormas_Pagto', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelFormas_Pagto'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreFormas_Pagto',
			model: 'ShSolutions.model.ModelFormas_Pagto',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'descricao'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/formas_pagto/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridFormas_Pagto')){
				Ext.getCmp('GridFormas_Pagto').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridFormas_Pagto')){
				Ext.getCmp('GridFormas_Pagto').getEl().unmask();
				Ext.getCmp('query_formas_pagto').focus(false, 1000);
			}	
		});
	}
});