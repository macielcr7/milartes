/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreClientesOs', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreClientesOs',
			pageSize: 50,
			model: 'ShSolutions.model.ModelOs',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
				action: 'GRID_CLIENTES_OS'
			},
			actionMethods: {
				create : 'POST',
				read   : 'POST',
				update : 'POST',
				destroy: 'POST'
			},	
			url : 'server/modulos/os/list.php',
			reader: {
				type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridClientesOs')) && (Ext.getCmp('GridClientesOs').getEl())){
				Ext.getCmp('GridClientesOs').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridClientesOs')) && (Ext.getCmp('GridClientesOs').getEl())){
				Ext.getCmp('GridClientesOs').getEl().unmask();
			}
		});
	}
});