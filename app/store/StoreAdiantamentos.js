/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAdiantamentos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelAdiantamentos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreAdiantamentos',
			model: 'ShSolutions.model.ModelAdiantamentos',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/adiantamentos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridAdiantamentos')){
				Ext.getCmp('GridAdiantamentos').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridAdiantamentos')){
				Ext.getCmp('GridAdiantamentos').getEl().unmask();
			}
		});
	}
});