/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreFornecedores_Anotacoes', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelFornecedores_Anotacoes'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreFornecedores_Anotacoes',
			model: 'ShSolutions.model.ModelFornecedores_Anotacoes',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'controle'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/fornecedores_anotacoes/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if((Ext.getCmp('GridFornecedores_Anotacoes')) && (Ext.getCmp('GridFornecedores_Anotacoes').getEl())){
					Ext.getCmp('GridFornecedores_Anotacoes').getEl().mask('Aguarde Carregando Dados...');
				 }	
		});
		
		me.on('load', function(){
			if((Ext.getCmp('GridFornecedores_Anotacoes')) && (Ext.getCmp('GridFornecedores_Anotacoes').getEl())){
				Ext.getCmp('GridFornecedores_Anotacoes').getEl().unmask();
			}	
		});
	}
});