/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboOrcamentosObservacoesTipo', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'D',
					descricao: 'Depósito'
				},
				{
					id: 'G',
					descricao: 'Geral'
				},
				{
					id: 'A',
					descricao: 'Ambos (Depósito e Geral)'
				}
			]
		}, cfg)]);
	}
});