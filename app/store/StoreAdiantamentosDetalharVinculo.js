/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAdiantamentosDetalharVinculo', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOrcamentos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreAdiantamentosDetalharVinculo',
			pageSize: 200,
			model: 'ShSolutions.model.ModelAdiantamentosVinculo',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'data_vinculo'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/adiantamentos_uso/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos')){
				Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos')){
				Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos').getEl().unmask();
			}
		});
	}
});