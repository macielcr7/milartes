/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCorreios_Localidades', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCorreios_Localidades'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCorreios_Localidades',
			pageSize: 50,
			model: 'ShSolutions.model.ModelCorreios_Localidades',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'loc_nome'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/correios_localidades/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridCorreios_Localidades')){
				Ext.getCmp('GridCorreios_Localidades').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridCorreios_Localidades')){
				Ext.getCmp('GridCorreios_Localidades').getEl().unmask();
				Ext.getCmp('query_correios_localidades').focus(false, 800);
			}	
		});
	}
});
