/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboSituacaoCadastralClientes', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'R',
					descricao: 'RECUSADO'
				},
				{
					id: 'E',
					descricao: 'EXCLUÍDO'
				},
				{
					id: 'N',
					descricao: 'NORMAL'
				},
				{
					id: 'P',
					descricao: 'PENDÊNCIA FINANCEIRA'
				}
			]
		}, cfg)]);
	}
});