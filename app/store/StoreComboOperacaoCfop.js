/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboOperacaoCfop', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
					{
						id: 'DE',
						descricao: 'DENTRO DO ESTADO'
					},				
					{
						id: 'FE',
						descricao: 'FORA DO ESTADO'
					}
			]
		}, cfg)]);
	}
});