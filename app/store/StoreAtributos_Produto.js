/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAtributos_Produto', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelAtributos_Produto'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreAtributos_Produto',
			model: 'ShSolutions.model.ModelAtributos_Produto',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id_atributos_produto'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/atributos_produto/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridAtributos_Produto') && Ext.getCmp('GridAtributos_Produto')){
				Ext.getCmp('GridAtributos_Produto').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridAtributos_Produto') && Ext.getCmp('GridAtributos_Produto')){
				Ext.getCmp('GridAtributos_Produto').getEl().unmask();
			}	
		});
	}
});