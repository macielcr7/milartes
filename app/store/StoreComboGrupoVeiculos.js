/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboGrupoVeiculos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'AUTOMÓVEL',
					descricao: 'AUTOMÓVEL'
				},
				{
					id: 'CAÇAMBA',
					descricao: 'CAÇAMBA'
				},
				{
					id: 'CAMINHÃO',
					descricao: 'CAMINHÃO'
				},
				{
					id: 'CARRETA',
					descricao: 'CARRETA'
				},
				{
					id: 'CAMIONETE',
					descricao: 'CAMIONETE'
				},
				{
					id: 'MOTOCICLETA',
					descricao: 'MOTOCICLETA'
				},
				{
					id: 'UTILITÁRIO',
					descricao: 'UTILITÁRIO'
				}
			]
		}, cfg)]);
	}
});