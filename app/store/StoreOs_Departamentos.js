/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreOs_Departamentos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs_Departamentos'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreOs_Departamentos',
			pageSize: 50,
			model: 'ShSolutions.model.ModelOs_Departamentos',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'nome'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},  
				url : 'server/modulos/os_departamentos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridOs_Departamentos')){
				Ext.getCmp('GridOs_Departamentos').getEl().mask('Aguarde Carregando Dados...');
			}   
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridOs_Departamentos')){
				Ext.getCmp('GridOs_Departamentos').getEl().unmask();
			}   
		});
	}
});