/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreFornecedores', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelFornecedores'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreFornecedores',
			pageSize: 50,
			model: 'ShSolutions.model.ModelFornecedores',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'cod_fornecedor'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/fornecedores/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridFornecedores')){
				Ext.getCmp('GridFornecedores').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridFornecedores')){
				Ext.getCmp('GridFornecedores').getEl().unmask();
				Ext.getCmp('query_fornecedores').focus(false, 200);
			}
		});
	}
});