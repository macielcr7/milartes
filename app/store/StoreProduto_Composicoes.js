/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProduto_Composicoes', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelProduto_Composicoes'
    ],
	
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StoreProduto_Composicoes',
            model: 'ShSolutions.model.ModelProduto_Composicoes',
            remoteSort: true,
            sorters: [
            	{
            		direction: 'ASC',
            		property: 'id_produto_composicoes'
            	}
            ],
   	        proxy: {
            	type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
		    	actionMethods: {
			        create : 'POST',
			        read   : 'POST',
			        update : 'POST',
			        destroy: 'POST'
			    },	
	            url : 'server/modulos/produto_composicoes/list.php',
	            reader: {
	            	type: 'json',
	                root: 'dados'
	            }
            }
        }, cfg)]);
        
        
        me.on('beforeload', function(){
        	if(Ext.getCmp('GridProduto_Composicoes') && Ext.getCmp('GridProduto_Composicoes').getEl()){
				Ext.getCmp('GridProduto_Composicoes').getEl().mask('Aguarde Carregando Dados...');
			}	
  		});
  		
  		me.on('load', function(){
  			if(Ext.getCmp('GridProduto_Composicoes') && Ext.getCmp('GridProduto_Composicoes').getEl()){
				Ext.getCmp('GridProduto_Composicoes').getEl().unmask();
			}	
  		});
    }
});
