/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboCorreios_Enderecos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCorreios_Enderecos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelCorreios_Enderecos',
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST_COMBO'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/correios_enderecos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
	}
});