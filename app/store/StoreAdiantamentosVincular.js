/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAdiantamentosVincular', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOrcamentos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreAdiantamentosVincular',
			pageSize: 200,
			model: 'ShSolutions.model.ModelOrcamentos',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id_orcamento'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/vendas/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridAdiantamentos_Vincular')){
				Ext.getCmp('GridAdiantamentos_Vincular').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridAdiantamentos_Vincular')){
				Ext.getCmp('GridAdiantamentos_Vincular').getEl().unmask();
			}
		});
	}
});