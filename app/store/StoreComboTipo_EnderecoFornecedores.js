/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipo_EnderecoFornecedores', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
				data: [
					{
						id: 'RESIDENCIAL',
						descricao: 'RESIDENCIAL'
					},
					{
						id: 'COMERCIAL',
						descricao: 'COMERCIAL'
					},
					{
						id: 'COBRANÇA',
						descricao: 'COBRANÇA'
					},
					{
						id: 'ENTREGA',
						descricao: 'ENTREGA'
					},
					{
						id: 'OUTRO',
						descricao: 'OUTRO'
					}
				]
		}, cfg)]);
	}
});