/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreVendas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelVendas'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreVendas',
			pageSize: 100,
			model: 'ShSolutions.model.ModelVendas',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'data_venda'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/vendas/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridVendas') && Ext.getCmp('GridVendas').getEl()){
				Ext.getCmp('GridVendas').getEl().mask('Aguarde Carregando Dados...');
			}
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridVendas') && Ext.getCmp('GridVendas').getEl()){
				Ext.getCmp('GridVendas').getEl().unmask();
			}
		});
	}
});