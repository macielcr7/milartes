/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreOrcamentos_Observacoes', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOrcamentos_Observacoes'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreOrcamentos_Observacoes',
			model: 'ShSolutions.model.ModelOrcamentos_Observacoes',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/orcamentos_observacoes/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridOrcamentos_Observacoes') && Ext.getCmp('GridOrcamentos_Observacoes').getEl()){
				Ext.getCmp('GridOrcamentos_Observacoes').getEl().mask('Aguarde Carregando Dados...');
			}
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridOrcamentos_Observacoes') && Ext.getCmp('GridOrcamentos_Observacoes').getEl()){
				Ext.getCmp('GridOrcamentos_Observacoes').getEl().unmask();
			}
		});
	}
});