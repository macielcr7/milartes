
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAmbientes_Orcamentos_Piso', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelAmbientes_Orcamentos_Piso'
    ],
	
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StoreAmbientes_Orcamentos_Piso',
            model: 'ShSolutions.model.ModelAmbientes_Orcamentos_Piso',
            remoteSort: false,
            sorters: [
            	{
            		direction: 'ASC',
            		property: 'id_ambientes_orcamentos_piso'
            	}
            ],
   	        proxy: {
            	type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
		    	actionMethods: {
			        create : 'POST',
			        read   : 'POST',
			        update : 'POST',
			        destroy: 'POST'
			    },	
	            url : 'server/modulos/ambientes_orcamentos_piso/list.php',
	            reader: {
	            	type: 'json',
	                root: 'dados'
	            }
            }
        }, cfg)]);
        
        
        me.on('beforeload', function(){
        	if(Ext.getCmp('GridAmbientes')){
				Ext.getCmp('GridAmbientes').getEl().mask('Aguarde Carregando Dados...');
			}	
  		});
  		
  		me.on('load', function(){
  			if(Ext.getCmp('GridAmbientes')){
				Ext.getCmp('GridAmbientes').getEl().unmask();
			}	
  		});
    }
});
