/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreContratos_Cobranca', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelContratos_Cobranca'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreContratos_Cobranca',
			model: 'ShSolutions.model.ModelContratos_Cobranca',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/contratos_cobranca/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridContratos_Cobranca')){
				Ext.getCmp('GridContratos_Cobranca').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridContratos_Cobranca')){
				Ext.getCmp('GridContratos_Cobranca').getEl().unmask();
				Ext.getCmp('query_contratos_cobranca').focus(false, 200);
			}	
		});
	}
});