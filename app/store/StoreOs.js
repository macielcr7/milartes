/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreOs', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreOs',
			pageSize: 100,
			model: 'ShSolutions.model.ModelOs',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'data_agendada'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/os/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridOs')) && (Ext.getCmp('GridOs').getEl())){
				Ext.getCmp('GridOs').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridOs')) && (Ext.getCmp('GridOs').getEl())){
				Ext.getCmp('GridOs').getEl().unmask();
			}
		});
	}
});