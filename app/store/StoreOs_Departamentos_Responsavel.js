/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreOs_Departamentos_Responsavel', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs_Departamentos_Responsavel'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreOs_Departamentos_Responsavel',
			pageSize: 50,
			model: 'ShSolutions.model.ModelOs_Departamentos_Responsavel',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/os_departamentos_responsavel/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridOs_Departamentos_Responsavel')){
				Ext.getCmp('GridOs_Departamentos_Responsavel').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridOs_Departamentos_Responsavel')){
				Ext.getCmp('GridOs_Departamentos_Responsavel').getEl().unmask();
			}
		});
	}
});