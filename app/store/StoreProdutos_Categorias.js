/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProdutos_Categorias', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelProdutos_Categorias'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreProdutos_Categorias',
			pageSize: 50,
			model: 'ShSolutions.model.ModelProdutos_Categorias',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id_parent'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/produtos_categorias/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridProdutos_Categorias')){
				Ext.getCmp('GridProdutos_Categorias').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridProdutos_Categorias')){
				Ext.getCmp('GridProdutos_Categorias').getEl().unmask();
				Ext.getCmp('query_produtos_categorias').focus(false, 1000);
			}	
		});
	}
});