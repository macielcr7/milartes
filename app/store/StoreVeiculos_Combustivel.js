/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreVeiculos_Combustivel', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelVeiculos_Combustivel'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreVeiculos_Combustivel',
			model: 'ShSolutions.model.ModelVeiculos_Combustivel',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'hodometro'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/veiculos_combustivel/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridVeiculos_Combustivel')) && (Ext.getCmp('GridVeiculos_Combustivel').getEl())){
					Ext.getCmp('GridVeiculos_Combustivel').getEl().mask('Aguarde Carregando Dados...');
				 }
		});

		me.on('load', function(){
			if((Ext.getCmp('GridVeiculos_Combustivel')) && (Ext.getCmp('GridVeiculos_Combustivel').getEl())){
				Ext.getCmp('GridVeiculos_Combustivel').getEl().unmask();
			}	
		});
	}
});