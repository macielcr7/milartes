/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboLocTipoCorreiosLocalidades', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'D',
					descricao: 'Distrito'
				},
				{
					id: 'M',
					descricao: 'Município'
				},
				{
					id: 'P',
					descricao: 'Povoado'
				},
				{
					id: 'R',
					descricao: 'Região Administrativa'
				}
			]
		}, cfg)]);
	}
});