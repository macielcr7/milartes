/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboProdutosForro', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCombo'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelCombo',
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST_COMBO',
					in_cod_categoria: "1, 5",
					in_tipo_produto: "'P','I'"
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/produtos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
	}
});