/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreClientesVendas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOrcamentos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreClientesVendas',
			pageSize: 200,
			model: 'ShSolutions.model.ModelOrcamentos',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id_orcamento'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/orcamentos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridVendas')){
				Ext.getCmp('GridVendas').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridVendas')){
				Ext.getCmp('GridVendas').getEl().unmask();
			}
		});
	}
});