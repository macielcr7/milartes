/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboContratosCobrancaLayout', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'CNAB240',
					descricao: 'CNAB240'
				},
				{
					id: 'CNAB400',
					descricao: 'CNAB400'
				}
			]
		}, cfg)]);
	}
});
