Ext.define('ShSolutions.store.StoreTreeCategorias', {
	extend: 'Ext.data.TreeStore',

	requires: [
		'ShSolutions.model.ModelTreeCategorias'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreTreeCategorias',
			root: {
				text: "Categorias",
				expanded: true
			},
			model: 'ShSolutions.model.ModelTreeCategorias',
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST_TREE'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url: 'server/modulos/produtos_categorias/list.php',
				reader: {
					type: 'json'
				}
			}
		}, cfg)]);
	}
});