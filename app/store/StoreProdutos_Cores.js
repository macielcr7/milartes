/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProdutos_Cores', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelProdutos_Cores'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreProdutos_Cores',
			model: 'ShSolutions.model.ModelProdutos_Cores',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id_produtos_cores'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/produtos_cores/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridProdutos_Cores') && Ext.getCmp('GridProdutos_Cores').getEl()){
				Ext.getCmp('GridProdutos_Cores').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridProdutos_Cores') && Ext.getCmp('GridProdutos_Cores').getEl()){
				Ext.getCmp('GridProdutos_Cores').getEl().unmask();
			}	
		});
	}
});