/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreRecibos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelRecibos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreRecibos',
			pageSize: 50,
			model: 'ShSolutions.model.ModelRecibos',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/recibos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridRecibos')){
				Ext.getCmp('GridRecibos').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridRecibos')){
				Ext.getCmp('GridRecibos').getEl().unmask();
				Ext.getCmp('query_recibos').focus(false, 1000);
			}
		});
	}
});