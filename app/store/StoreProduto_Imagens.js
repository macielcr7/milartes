/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreProduto_Imagens', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelProduto_Imagens'
    ],
	
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StoreProduto_Imagens',
            model: 'ShSolutions.model.ModelProduto_Imagens',
            remoteSort: true,
            sorters: [
            	{
            		direction: 'ASC',
            		property: 'id_produto_imagens'
            	}
            ],
   	        proxy: {
            	type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
		    	actionMethods: {
			        create : 'POST',
			        read   : 'POST',
			        update : 'POST',
			        destroy: 'POST'
			    },	
	            url : 'server/modulos/produto_imagens/list.php',
	            reader: {
	            	type: 'json',
	                root: 'dados'
	            }
            }
        }, cfg)]);
        
        
        me.on('beforeload', function(){
        	if(Ext.getCmp('GridProduto_Imagens') && Ext.getCmp('GridProduto_Imagens').getEl()){
				Ext.getCmp('GridProduto_Imagens').getEl().mask('Aguarde Carregando Dados...');
			}	
  		});
  		
  		me.on('load', function(){
  			if(Ext.getCmp('GridProduto_Imagens') && Ext.getCmp('GridProduto_Imagens').getEl()){
				Ext.getCmp('GridProduto_Imagens').getEl().unmask();
			}	
  		});
    }
});
