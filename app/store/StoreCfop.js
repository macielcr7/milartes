/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.store.StoreCfop', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCfop'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCfop',
			model: 'ShSolutions.model.ModelCfop',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'cod_fiscal'
				}
			],
   			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/cfop/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridCfop')){
				Ext.getCmp('GridCfop').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridCfop')){
				Ext.getCmp('GridCfop').getEl().unmask();
				Ext.getCmp('query_cfop').focus(false, 200);
			}	
		});
	}
});