/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAgenda_Telefonica_Contato', {
		extend: 'Ext.data.Store',
		requires: [
				'ShSolutions.model.ModelAgenda_Telefonica_Contato'
		],
	
		constructor: function(cfg) {
				var me = this;
				cfg = cfg || {};
				me.callParent([Ext.apply({
						storeId: 'StoreAgenda_Telefonica_Contato',
						model: 'ShSolutions.model.ModelAgenda_Telefonica_Contato',
						remoteSort: true,
						sorters: [
							{
								direction: 'ASC',
								property: 'controle'
							}
						],
						proxy: {
							type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
					actionMethods: {
							create : 'POST',
							read   : 'POST',
							update : 'POST',
							destroy: 'POST'
					},	
							url : 'server/modulos/agenda_telefonica_contato/list.php',
							reader: {
								type: 'json',
									root: 'dados'
							}
						}
				}, cfg)]);
				
				
				me.on('beforeload', function(){
					if((Ext.getCmp('GridAgenda_Telefonica_Contato')) && (Ext.getCmp('GridAgenda_Telefonica_Contato').getEl())){
				Ext.getCmp('GridAgenda_Telefonica_Contato').getEl().mask('Aguarde Carregando Dados...');
			}	
			});
			
			me.on('load', function(){
				if((Ext.getCmp('GridAgenda_Telefonica_Contato')) && (Ext.getCmp('GridAgenda_Telefonica_Contato').getEl())){
				Ext.getCmp('GridAgenda_Telefonica_Contato').getEl().unmask();
			}	
			});
		}
});