/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAgenda_Telefonica', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelAgenda_Telefonica'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreAgenda_Telefonica',
			pageSize: 50,
			model: 'ShSolutions.model.ModelAgenda_Telefonica',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'cod_contato'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/agenda_telefonica/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridAgenda_Telefonica')){
				Ext.getCmp('GridAgenda_Telefonica').getEl().mask('Aguarde Carregando Dados...');
			}	
		});

		me.on('load', function(){
			if(Ext.getCmp('GridAgenda_Telefonica')){
				Ext.getCmp('GridAgenda_Telefonica').getEl().unmask();
				Ext.getCmp('query_agenda_telefonica').focus(false, 800);
			}
		});
	}
});