/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.store.StoreFeriados', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelFeriados'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreFeriados',
			pageSize: 50,
			model: 'ShSolutions.model.ModelFeriados',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'data_feriado'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/feriados/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridFeriados')){
				Ext.getCmp('GridFeriados').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridFeriados')){
				Ext.getCmp('GridFeriados').getEl().unmask();
				Ext.getCmp('query_feriados').focus(false, 1000);
			}	
		});
	}
});