/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreLogs_Sistema', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelLogs_Sistema'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreLogs_Sistema',
			pageSize: 50,
			model: 'ShSolutions.model.ModelLogs_Sistema',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'controle'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/logs_sistema/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridLogs_Sistema')) && (Ext.getCmp('GridLogs_Sistema').getEl())){
				Ext.getCmp('GridLogs_Sistema').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridLogs_Sistema')) && (Ext.getCmp('GridLogs_Sistema').getEl())){
				Ext.getCmp('GridLogs_Sistema').getEl().unmask();
			}
		});
	}
});