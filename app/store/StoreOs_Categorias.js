/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreOs_Categorias', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs_Categorias'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreOs_Categorias',
			pageSize: 50,
			model: 'ShSolutions.model.ModelOs_Categorias',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'descricao'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/os_categorias/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridOs_Categorias')){
				Ext.getCmp('GridOs_Categorias').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridOs_Categorias')){
				Ext.getCmp('GridOs_Categorias').getEl().unmask();
			}	
		});
	}
});