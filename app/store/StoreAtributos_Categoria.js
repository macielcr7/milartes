/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreAtributos_Categoria', {
    extend: 'Ext.data.Store',
    requires: [
        'ShSolutions.model.ModelAtributos_Categoria'
    ],
	
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StoreAtributos_Categoria',
            model: 'ShSolutions.model.ModelAtributos_Categoria',
            remoteSort: true,
            sorters: [
            	{
            		direction: 'ASC',
            		property: 'id_atributos_categoria'
            	}
            ],
   	        proxy: {
            	type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
		    	actionMethods: {
			        create : 'POST',
			        read   : 'POST',
			        update : 'POST',
			        destroy: 'POST'
			    },	
	            url : 'server/modulos/atributos_categoria/list.php',
	            reader: {
	            	type: 'json',
	                root: 'dados'
	            }
            }
        }, cfg)]);
        
        
        me.on('beforeload', function(){
        	if(Ext.getCmp('GridAtributos_Categoria')){
				Ext.getCmp('GridAtributos_Categoria').getEl().mask('Aguarde Carregando Dados...');
			}	
  		});
  		
  		me.on('load', function(){
  			if(Ext.getCmp('GridAtributos_Categoria')){
				Ext.getCmp('GridAtributos_Categoria').getEl().unmask();
			}	
  		});
    }
});
