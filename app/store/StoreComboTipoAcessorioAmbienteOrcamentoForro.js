
/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipoAcessorioAmbienteOrcamentoForro', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'SA',
					descricao: 'Sem Acessório'
				},
				{
					id: 'MC',
					descricao: 'Meia Cana'
				},
				{
					id: 'RF',
					descricao: 'Roda Forro (Perfil U)'
				},
				{
					id: 'EH',
					descricao: 'Emenda H'
				},
				{
					id: 'EHF',
					descricao: 'Emenda H Flexível'
				}
			]
		}, cfg)]);
	}
});