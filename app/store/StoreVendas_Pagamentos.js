/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreVendas_Pagamentos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelVendas_Pagamentos'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreVendas_Pagamentos',
			model: 'ShSolutions.model.ModelVendas_Pagamentos',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'data_cadastro'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/vendas_pagamentos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridVendas_Pagamentos') && Ext.getCmp('GridVendas_Pagamentos').getEl()){
				Ext.getCmp('GridVendas_Pagamentos').getEl().mask('Aguarde Carregando Dados...');
			}
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridVendas_Pagamentos') && Ext.getCmp('GridVendas_Pagamentos').getEl()){
				Ext.getCmp('GridVendas_Pagamentos').getEl().unmask();
			}
		});
	}
});