/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboContratosCobrancaModalidadeTaxa', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: '1',
					descricao: 'NA REMESSA DO ARQUIVO AO BANCO'
				},
				{
					id: '2',
					descricao: 'DESCONTADO DO VALOR DO BOLETO QUANDO O CLIENTE EFETUAR O PAGAMENTO'
				}
			]
		}, cfg)]);
	}
});
