/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipo_ContatoClientesContato', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: 'TELEFONE',
					descricao: 'Telefone'
				},
				{
					id: 'DDG',
					descricao: 'Telefone DDG'
				},
				{
					id: 'EMAIL',
					descricao: 'E-mail'
				},
				{
					id: 'SITE',
					descricao: 'Site'
				}
			]
		}, cfg)]);
	}
});