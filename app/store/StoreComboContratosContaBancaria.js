/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboContratosContaBancaria', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCombo'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelCombo',
   			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'COMBO_CONTRATOS_COBRANCA'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/contas_bancarias/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
	}
});