/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreInstaladores', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelInstaladores'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreInstaladores',
			pageSize: 50,
			model: 'ShSolutions.model.ModelInstaladores',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'cod_instalador'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/instaladores/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridInstaladores')){
				Ext.getCmp('GridInstaladores').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridInstaladores')){
				Ext.getCmp('GridInstaladores').getEl().unmask();
				Ext.getCmp('query_instaladores').focus(false, 200);
			}	
		});
	}
});