/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreFornecedores_Endereco', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelFornecedores_Endereco'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreFornecedores_Endereco',
			model: 'ShSolutions.model.ModelFornecedores_Endereco',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'controle'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/fornecedores_endereco/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if((Ext.getCmp('GridFornecedores_Endereco')) && (Ext.getCmp('GridFornecedores_Endereco').getEl())){
				Ext.getCmp('GridFornecedores_Endereco').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if((Ext.getCmp('GridFornecedores_Endereco')) && (Ext.getCmp('GridFornecedores_Endereco').getEl())){
				Ext.getCmp('GridFornecedores_Endereco').getEl().unmask();
			}	
		});
	}
});
