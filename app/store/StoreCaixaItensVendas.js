/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCaixaItensVendas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCaixaItensVendas',
			pageSize: 200,
			model: 'ShSolutions.model.ModelCaixa_Itens',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'GRID_RESUMO_VENDAS'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/caixa_itens/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridCaixaItensVendas')) && (Ext.getCmp('GridCaixaItensVendas').getEl())){
				Ext.getCmp('GridCaixaItensVendas').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridCaixaItensVendas')) && (Ext.getCmp('GridCaixaItensVendas').getEl())){
				Ext.getCmp('GridCaixaItensVendas').getEl().unmask();
			}
		});
	}
});