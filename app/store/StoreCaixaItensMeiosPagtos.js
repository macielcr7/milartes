/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCaixaItensMeiosPagtos', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelOs'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCaixaItensMeiosPagtos',
			pageSize: 200,
			model: 'ShSolutions.model.ModelCaixa_Itens',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'forma_pagto'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
				action: 'GRID_MEIOS_PAGTOS'
			},
			actionMethods: {
				create : 'POST',
				read   : 'POST',
				update : 'POST',
				destroy: 'POST'
			},	
			url : 'server/modulos/caixa_itens/list.php',
			reader: {
				type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridCaixaItensMeiosPagtos')) && (Ext.getCmp('GridCaixaItensMeiosPagtos').getEl())){
				Ext.getCmp('GridCaixaItensMeiosPagtos').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridCaixaItensMeiosPagtos')) && (Ext.getCmp('GridCaixaItensMeiosPagtos').getEl())){
				Ext.getCmp('GridCaixaItensMeiosPagtos').getEl().unmask();
			}
		});
	}
});