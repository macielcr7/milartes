/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreContratos_Cartao', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelContratos_Cartao'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreContratos_Cartao',
			model: 'ShSolutions.model.ModelContratos_Cartao',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'id'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/contratos_cartao/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
		
		
		me.on('beforeload', function(){
			if(Ext.getCmp('GridContratos_Cartao')){
				Ext.getCmp('GridContratos_Cartao').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridContratos_Cartao')){
				Ext.getCmp('GridContratos_Cartao').getEl().unmask();
				Ext.getCmp('query_contratos_cartao').focus(false, 1000);
			}	
		});
	}
});