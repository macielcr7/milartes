/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreCaixas', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCaixas'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreCaixas',
			model: 'ShSolutions.model.ModelCaixas',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'cx_nro'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/caixas/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridCaixas')){
				Ext.getCmp('GridCaixas').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if(Ext.getCmp('GridCaixas')){
				Ext.getCmp('GridCaixas').getEl().unmask();
			}
		});
	}
});