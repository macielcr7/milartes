/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreUsuarios_Empresas_Sistema', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelUsuarios_Empresas_Sistema'
	],
	
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreUsuarios_Empresas_Sistema',
			model: 'ShSolutions.model.ModelUsuarios_Empresas_Sistema',
			remoteSort: true,
			sorters: [
				{
					direction: 'ASC',
					property: 'controle'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},	
				url : 'server/modulos/usuarios_empresas_sistema/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if(Ext.getCmp('GridUsuarios_Empresas_Sistema')){
				Ext.getCmp('GridUsuarios_Empresas_Sistema').getEl().mask('Aguarde Carregando Dados...');
			}	
		});
		
		me.on('load', function(){
			if(Ext.getCmp('GridUsuarios_Empresas_Sistema')){
				Ext.getCmp('GridUsuarios_Empresas_Sistema').getEl().unmask();
			}	
		});
	}
});