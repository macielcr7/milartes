/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboTipoLancamentoCaixaItens', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelComboLocal'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelComboLocal',
			data: [
				{
					id: '0',
					descricao: 'SUPRIMENTO (ADICIONAR DINHEIRO)'
				},				
				{
					id: '2',
					descricao: 'VENDA'
				},
				{
					id: '3',
					descricao: 'ADIANTAMENTO'
				}
			]
		}, cfg)]);
	}
});