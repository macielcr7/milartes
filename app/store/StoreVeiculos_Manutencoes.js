/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreVeiculos_Manutencoes', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelVeiculos_Manutencoes'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			storeId: 'StoreVeiculos_Manutencoes',
			pageSize: 50,
			model: 'ShSolutions.model.ModelVeiculos_Manutencoes',
			remoteSort: true,
			sorters: [
				{
					direction: 'DESC',
					property: 'hodometro'
				}
			],
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/veiculos_manutencoes/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);

		me.on('beforeload', function(){
			if((Ext.getCmp('GridVeiculos_Manutencoes')) && (Ext.getCmp('GridVeiculos_Manutencoes').getEl())){
				Ext.getCmp('GridVeiculos_Manutencoes').getEl().mask('Aguarde Carregando Dados...');
			}
		});

		me.on('load', function(){
			if((Ext.getCmp('GridVeiculos_Manutencoes')) && (Ext.getCmp('GridVeiculos_Manutencoes').getEl())){
				Ext.getCmp('GridVeiculos_Manutencoes').getEl().unmask();
			}
		});
	}
});