/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.store.StoreComboProdutosAcessoriosPiso', {
	extend: 'Ext.data.Store',
	requires: [
		'ShSolutions.model.ModelCombo'
	],

	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			model: 'ShSolutions.model.ModelCombo',
			proxy: {
				type: 'ajax',
				extraParams: {
					action: 'LIST_COMBO',
					cod_categoria: 14,
					tipo_produto: 'A'
				},
				actionMethods: {
					create : 'POST',
					read   : 'POST',
					update : 'POST',
					destroy: 'POST'
				},
				url : 'server/modulos/produtos/list.php',
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		}, cfg)]);
	}
});