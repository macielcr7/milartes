/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Setor_Estoque', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Setor_Estoque',

	refs: [
		{
			ref: 'list',
			selector: 'setor_estoquelist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addsetor_estoquewin form'
		},
		{
			ref: 'filterBtn',
			selector: 'setor_estoquelist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtersetor_estoquewin'
		},
		{
			ref: 'filterForm',
			selector: 'filtersetor_estoquewin form'
		},
		{
			ref: 'addWin',
			selector: 'addsetor_estoquewin'
		}
	],

	models: [
		'ModelCombo',
		'ModelSetor_Estoque'
	],
	stores: [
		'StoreSetor_Estoque',
		'StoreComboAtivo'
	],

	views: [
		'setor_estoque.List',
		'setor_estoque.Edit'
	],

	init: function(application) {
		this.control({
			'setor_estoquelist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'setor_estoquelist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'setor_estoquelist button[action=adicionar]': {
				click: this.add
			},
			'setor_estoquelist button[action=editar]': {
				click: this.btedit
			},
			'setor_estoquelist button[action=deletar]': {
				click: this.btdel
			},
			'setor_estoquelist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addsetor_estoquewin button[action=salvar]': {
				click: this.update
			},
			'addsetor_estoquewin button[action=resetar]': {
				click: this.reset
			},
			'addsetor_estoquewin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtersetor_estoquewin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtersetor_estoquewin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtersetor_estoquewin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtersetor_estoquewin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddSetor_EstoqueWin', 'Setor_Estoque', 'setor_estoque.Edit', function(){
			me.getAddWin().setTitle('Alterar Setor de Estoque');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/setor_estoque/list.php');
			Ext.getCmp('action_setor_estoque').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'setor_estoque', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desativar o Setor de Estoque: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Setor_Estoque', 'Setor_Estoque', 'setor_estoque.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('setor_estoque', 'editar')==true){
			Ext.getCmp('button_edit_setor_estoque').setDisabled(false);
		}

		if(this.existPermissao('setor_estoque', 'deletar')==true){
			Ext.getCmp('button_del_setor_estoque').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_setor_estoque').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridSetor_Estoque').store.proxy.extraParams.status_setor = item.value;
		Ext.getCmp('GridSetor_Estoque').store.load({
			start: 0,
			limit: 50
		});
	}
});