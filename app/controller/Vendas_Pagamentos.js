/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Vendas_Pagamentos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Vendas_Pagamentos',
	started: true,
	noDeleteLoad: true,

	refs: [
		{
			ref: 'list',
			selector: 'gridvendas_pagamentoslist'
		},
		{
			ref: 'form',
			selector: 'addvendas_pagamentoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridvendas_pagamentoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtervendas_pagamentoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtervendas_pagamentoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addvendas_pagamentoswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelVendas_Pagamentos'
	],
	stores: [
		'StoreComboProdutos',
		'StoreVendas_Pagamentos',
		'StoreComboOrcamentosObservacoesTipo'
	],
	views: [
		'vendas_pagamentos.Grid',
		'vendas_pagamentos.List',
		'vendas_pagamentos.Edit'
	],

	init: function(application) {
		this.control({
			'gridvendas_pagamentoslist': {
				afterrender: this.getPermissoes,
				cellclick: this.clickCelula,
				render: this.gridLoad
			},
			/*'gridvendas_pagamentoslist gridpanel': { 
			},*/
			'gridvendas_pagamentoslist button[action=adicionar]': {
				click: this.add
			},
			'gridvendas_pagamentoslist button[action=editar]': {
				click: this.btedit
			},
			'gridvendas_pagamentoslist button[action=estornar]': {
				click: this.btEstorno
			},            
			'addvendas_pagamentoswin button[action=salvar]': {
				click: this.update
			},
			'addvendas_pagamentoswin button[action=resetar]': {
				click: this.reset
			},
			'addvendas_pagamentoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addvendas_pagamentoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addvendas_pagamentoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addvendas_pagamentoswin form combobox[id=forma_pagto_vendas_pagamentos]': {
				change: this.changeQtdeParcelas
			}
		});
	},

	changeQtdeParcelas: function(combo, value){
		var me = this;
		var record = combo.store.getById(value);

		if (value!=null && !isNaN(parseInt(value)))	{
			var max_cartao = parseInt(record.raw.max_cartao);
			var comboNroParcelas = Ext.getCmp('nro_parcelas_vendas_pagamentos');
			var comboDataCartao = Ext.getCmp('data_venda_cartao_vendas_pagamentos');

			if (!isNaN(max_cartao)) {
				comboNroParcelas.setDisabled(false);
				comboNroParcelas.setVisible(true);
				comboNroParcelas.setValue(1);
				comboNroParcelas.setMaxValue(max_cartao);
				comboDataCartao.setDisabled(false);
				comboDataCartao.setVisible(true);
			}
			else {
				comboNroParcelas.reset();
				comboNroParcelas.setDisabled(true);
				comboNroParcelas.setVisible(false);

				comboDataCartao.reset();
				comboDataCartao.setDisabled(true);
				comboDataCartao.setVisible(false);
			}
		}
	},

	clickCelula: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		var me = this;
		var params = me.getList().store.proxy.extraParams;
		Ext.getCmp('button_estorna_vendas_pagamentos').setDisabled(true);

		if(record.get('estornado') == 'S') {
			Ext.getCmp('button_estorna_vendas_pagamentos').setDisabled(true);
		}
		else {
			if (params.venda_finalizada == 'S') {
				Ext.getCmp('button_estorna_vendas_pagamentos').setDisabled(true);
				Ext.getCmp('button_add_vendas_pagamentos').setDisabled(true);

				info(me.titleErro, 'A Venda Já Finalizada...');
				return true;
			}
			else {
				Ext.getCmp('button_estorna_vendas_pagamentos').setDisabled(false);
			}
		}
	},

	estornaPagamento: function(grid, record, button, motivo) {
		var me = this;
		me.deleteAjax(grid, 'vendas_pagamentos', {
			action: 'ESTORNAR',
			id: record.get('id'),
			motivo_cancelamento: motivo,
			categoria: record.get('categoria_pagamento')
		}, button, function(){
			grid.store.load({
				callback: function(){
					var grid1 = Ext.getCmp('GridAdiantamentos_Vincular');
					var grid2 = Ext.getCmp('GridVendas');
					var grid3 = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos');
					var grid4 = Ext.getCmp('GridCaixa_Itens');
					var grid5 = Ext.getCmp('GridAdiantamentos');

					setTimeout(function() {
						if(grid1 && grid1.getEl()){
							grid1.store.load();
						}

						if(grid2 && grid2.getEl()){
							grid2.store.load();
						}

						if(grid3 && grid3.getEl()){
							grid3.store.load();
						}

						if(grid4 && grid4.getEl()){
							grid4.store.load();
						}

						if(grid5 && grid5.getEl()){
							grid5.store.load();
						}
					}, 500);

					var valor_estornado = parseFloat(record.get('valor_pago'));
					var saldo_a_receber_anterior = parseFloat(grid.store.proxy.extraParams.saldo_a_receber);
					var saldo_a_receber_atual = saldo_a_receber_anterior + valor_estornado;
					grid.store.proxy.extraParams.saldo_a_receber = parseFloat(saldo_a_receber_atual).toFixed(2);
				}
			})
		});
	},

	btEstorno: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.prompt('Confirmar', 'Deseja Estornar o Pagamento de <b>R$: '+converteFloatMoeda(record.get('valor_pago'))+'?</b>', function(btnText, sInput){
				if (btnText === 'ok') {
					if (sInput == '') {
						Ext.Msg.alert("ERRO", "DIGITE UM MOTIVO ");
					}
					else {
						me.estornaPagamento(me.getList(), record, button, sInput);
					}
				}
			}, this);
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Vendas_Pagamentos', 'Vendas_Pagamentos', 'vendas_pagamentos.Edit', function(){
			var cod_venda = me.getList().store.proxy.extraParams.cod_venda;
			var id_cliente = me.getList().store.proxy.extraParams.id_cliente;
			var saldo_a_receber = me.getList().store.proxy.extraParams.saldo_a_receber;
			me.getAddWin().setTitle('Adicionando Pagamento para Venda Nro: '+cod_venda);
			Ext.getCmp('cod_venda_vendas_pagamentos').setValue(cod_venda);
			Ext.getCmp('cod_cliente_vendas_pagamentos').setValue(id_cliente);
			Ext.getCmp('saldo_a_receber_vendas_pagamentos').setValue(saldo_a_receber);
		});
	},

	update: function(button) {
		var me = this;
		var valor_pago = parseFloat(Ext.getCmp('valor_pago_vendas_pagamentos').getValue());
		var saldo_a_receber = parseFloat(Ext.getCmp('saldo_a_receber_vendas_pagamentos').getValue());
		var diferenca = saldo_a_receber - valor_pago;

		if (Ext.getCmp('forma_pagto_vendas_pagamentos').getValue() != null) {
			if (valor_pago > saldo_a_receber) {
				info(this.titleErro, 'O valor à pagar não pode ser maior que o saldo à receber');
				return true;
			}
			else {
				var gridX = Ext.getCmp('GridVendas_Pagamentos');
				if(gridX && gridX.getEl()) {
					if (valor_pago == saldo_a_receber) {
						Ext.getCmp('button_add_vendas_pagamentos').setDisabled(true);
					}
					else {
						Ext.getCmp('button_add_vendas_pagamentos').setDisabled(false);
					}
					gridX.store.proxy.extraParams.saldo_a_receber = parseFloat(saldo_a_receber - valor_pago).toFixed(2);
				}

				me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(){
					if (me.getList()!=undefined) {
						me.getList().store.load();
					}

					var grid1 = Ext.getCmp('GridVendas');
					var grid2 = Ext.getCmp('GridAdiantamentos');
					var grid3 = Ext.getCmp('GridAdiantamentos_Vincular');
					var grid4 = Ext.getCmp('GridCaixa_Itens');
					var grid5 = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos');

					setTimeout(function() {
						if(grid1 && grid1.getEl()){
							grid1.store.load();
						}

						if(grid2 && grid2.getEl()){
							grid2.store.load();
						}

						if(grid3 && grid3.getEl()){
							grid3.store.load();
						}

						if(grid4 && grid4.getEl()){
							grid4.store.load();
						}

						if(grid5 && grid5.getEl()){
							grid5.store.load();
						}
					}, 500);
				}, false);
			}
		}
		else {
			info(this.titleErro, 'Selecione uma Forma de Pagamento');
			return true;
		}
	}
});