/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Usuarios_Empresas_Sistema', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Usuarios_Empresas_Sistema',
	
	refs: [
		{
			ref: 'list',
			selector: 'usuarios_empresas_sistemalist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addusuarios_empresas_sistemawin form'
		},
		{
			ref: 'filterBtn',
			selector: 'usuarios_empresas_sistemalist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterusuarios_empresas_sistemawin'
		},
		{
			ref: 'filterForm',
			selector: 'filterusuarios_empresas_sistemawin form'
		},
		{
			ref: 'addWin',
			selector: 'addusuarios_empresas_sistemawin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelUsuarios_Empresas_Sistema'
	],
	stores: [
		'StoreComboUsuarios',
		'StoreComboEmpresas_Sistema2',
		'StoreUsuarios_Empresas_Sistema'
	],
	
	views: [
		'usuarios_empresas_sistema.List',
		'usuarios_empresas_sistema.Filtro',
		'usuarios_empresas_sistema.Edit'
	],

	init: function(application) {
		this.control({
			'usuarios_empresas_sistemalist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'usuarios_empresas_sistemalist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'usuarios_empresas_sistemalist button[action=adicionar]': {
				click: this.add
			},
			'usuarios_empresas_sistemalist button[action=editar]': {
				click: this.btedit
			},
			'usuarios_empresas_sistemalist button[action=deletar]': {
				click: this.btdel
			},
			'addusuarios_empresas_sistemawin button[action=salvar]': {
				click: this.update
			},
			'addusuarios_empresas_sistemawin button[action=resetar]': {
				click: this.reset
			},
			'addusuarios_empresas_sistemawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addusuarios_empresas_sistemawin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addusuarios_empresas_sistemawin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterusuarios_empresas_sistemawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterusuarios_empresas_sistemawin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterusuarios_empresas_sistemawin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterusuarios_empresas_sistemawin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddUsuarios_Empresas_SistemaWin', 'Usuarios_Empresas_Sistema', 'usuarios_empresas_sistema.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Usuarios_Empresas_Sistema');
			var id_usuario = me.getList().store.proxy.extraParams.id_usuario;
			Ext.getCmp('id_empresa_usuarios_empresas_sistema').store.proxy.extraParams.id_usuario = id_usuario;
			Ext.getCmp('id_empresa_usuarios_empresas_sistema').store.load();
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/usuarios_empresas_sistema/list.php');
			Ext.getCmp('action_usuarios_empresas_sistema').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'usuarios_empresas_sistema', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar a Associa&ccedil;&atilde;o: '+record.get('controle')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Usuarios_Empresas_Sistema', 'Usuarios_Empresas_Sistema', 'usuarios_empresas_sistema.Edit', function(){
			var id_usuario = me.getList().store.proxy.extraParams.id_usuario;
			Ext.getCmp('id_usuario_usuarios_empresas_sistema').setValue(id_usuario);
			Ext.getCmp('id_empresa_usuarios_empresas_sistema').store.proxy.extraParams.id_usuario = id_usuario;
			Ext.getCmp('id_empresa_usuarios_empresas_sistema').store.load();
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('usuarios_empresas_sistema', 'deletar')==true){
			Ext.getCmp('button_del_usuarios_empresas_sistema').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.usuarios_empresas_sistema.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});