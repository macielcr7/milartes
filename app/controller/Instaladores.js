/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Instaladores', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	//storePai: true,
	tabela: 'Instaladores',

	refs: [
		{
			ref: 'list',
			selector: 'instaladoreslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addinstaladoreswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'instaladoreslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterinstaladoreswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterinstaladoreswin form'
		},
		{
			ref: 'addWin',
			selector: 'addinstaladoreswin'
		}
	],

	models: [
		'ModelCombo',
		'ModelInstaladores'
	],
	stores: [
		'StoreComboAtivo',
		'StoreInstaladores'
	],

	views: [
		'instaladores.List',
		'instaladores.Edit'
	],

	init: function(application) {
		this.control({
			'instaladoreslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'instaladoreslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'instaladoreslist button[action=adicionar]': {
				click: this.add
			},
			'instaladoreslist button[action=editar]': {
				click: this.btedit
			},
			'instaladoreslist button[action=deletar]': {
				click: this.btdel
			},
			'addinstaladoreswin button[action=salvar]': {
				click: this.update
			},
			'addinstaladoreswin button[action=resetar]': {
				click: this.reset
			},
			'instaladoreslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addinstaladoreswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterinstaladoreswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterinstaladoreswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterinstaladoreswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterinstaladoreswin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddInstaladoresWin', 'Instaladores', 'instaladores.Edit', function(){
			me.getAddWin().setTitle('Altera&ccedil;&atilde;o de Instaladores');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_instalador'), 'server/modulos/instaladores/list.php');
			Ext.getCmp('action_instaladores').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'instaladores', {
			action: 'DELETAR',
			id: record.get('cod_instalador')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar o Instalador: '+record.get('nome_completo')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Instaladores', 'Instaladores', 'instaladores.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('instaladores', 'editar')==true){
			Ext.getCmp('button_edit_instaladores').setDisabled(false);
		}

		if(this.existPermissao('instaladores', 'deletar')==true){
			Ext.getCmp('button_del_instaladores').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_instaladores').getValue();
		this.getList().store.load({
			start: 0,
			limit: 25
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridInstaladores').store.proxy.extraParams.status_instalador = item.value;
		Ext.getCmp('GridInstaladores').store.load({
			start: 0,
			limit: 50
		});
	}
});