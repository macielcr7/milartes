/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Contas_Pagar', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Contas_Pagar',

	refs: [
		{
			ref: 'list',
			selector: 'contas_pagarlist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcontas_pagarwin form'
		},
		{
			ref: 'filterBtn',
			selector: 'contas_pagarlist button menuitem[action=relatorios_contas_cadastradas]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercontas_pagarwin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercontas_pagarwin form'
		},
		{
			ref: 'addWin',
			selector: 'addcontas_pagarwin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelContas_Pagar'
	],
	stores: [
		'StoreContas_Pagar',
		'StoreComboContasPagarEmpresas',
		'StoreComboContasPagarFornecedores',
		'StoreComboContasPagarTipoConta',
		'StoreContas_Pagar_Pagtos',
		'StoreComboStatusConta'
	],

	views: [
		'contas_pagar.List',
		'contas_pagar.Edit',
		'contas_pagar.Add',
		'contas_pagar.Filtro',
		'contas_pagar.Filtro2'
	],

	init: function(application) {
		this.control({
			'contas_pagarlist gridpanel': {
				afterrender: this.getPermissoes3,
				select: this.RegistroSelecionado,
				render: this.gridLoad
			},
			'contas_pagarlist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'contas_pagarlist button menuitem[action=relatorios_contas_cadastradas]': {
				click: this.Relatorio1
			},
			'contas_pagarlist button menuitem[action=relatorios_contas_pagtos_efetuados]': {
				click: this.Relatorio2
			},
			'contas_pagarlist button[action=adicionar]': {
				click: this.add
			},
			'contas_pagarlist button[action=editar]': {
				click: this.btedit
			},
			'contas_pagarlist button[action=deletar]': {
				click: this.btdel
			},
			'contas_pagarlist button[action=pagar_conta]': {
				click: this.pagarConta
			},
			'contas_pagarlist button[action=codigo_barras]': {
				click: this.codigoBarras
			},
			'addcontas_pagarwin tabpanel': {
				tabchange: this.loadActive
			},
			'addcontas_pagarwin button[id=button_salvar_contas_pagar], addcontas_pagarwin button[id=button_salvar_contas_pagar_edit]': {
				click: this.update
			},
			'addcontas_pagarwin button[id=button_resetar_contas_pagar]': {
				click: this.reset
			},
			'addcontas_pagarwin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addcontas_pagarwin form fieldcontainer combobox': {
				change: this.enableButton,
				afterrender: this.comboLoad
			},
			'addcontas_pagarwin numberfield[id=parcelas_parcelamento]': {
				change: this.setDateFields
			},
			'addcontas_pagarwin form textfield[id=valor_contas_pagar]': {
				change: this.setParcelas
			},
			'filtercontas_pagarwin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercontas_pagarwin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercontas_pagarwin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercontas_pagarwin': {
				show: this.filterSetFields
			}
		});
	},

	getPermissoes3: function(grid){
		var me = this;
		
		var data = this.application.dados[this.tabela.toLowerCase()];
		var items = grid.down('toolbar').items.items;
		
		Ext.each(items, function(p){
			Ext.each(data, function(j){
				if(p.action && p.action==j.acao){
					p.setVisible(true);
				}
			});
		});


		if(this.existPermissao('contas_pagar', 'relatorios_contas_cadastradas')==true){
			Ext.getCmp('button_relatorios_contas_cadastradas_contas_pagar').setVisible(true);
		}

		if(this.existPermissao('contas_pagar', 'relatorios_contas_pagtos_efetuados')==true){
			Ext.getCmp('button_relatorios_contas_pagtos_efetuados_contas_pagar').setVisible(true);
		}
	},

	setParcelas: function(field, v){
		var total = Ext.getCmp('valor_contas_pagar').getValue()=='' ? 0 : parseFloat(Ext.getCmp('valor_contas_pagar').getValue());
		
		var qtdParcelas = Ext.getCmp('parcelas_parcelamento').getValue();
		var parcelas = total/qtdParcelas;

		for(var i=1; i<=qtdParcelas; i++){
			Ext.getCmp('valor_parcela_'+i+'_parcelamento').setValue(parcelas);
		}
	},

	setDateFields: function(field, v){
		var total = Ext.getCmp('valor_contas_pagar').getValue()=='' ? 0 : parseFloat(Ext.getCmp('valor_contas_pagar').getValue());

		var dataPrimeiroPagto = Ext.getCmp('vencimento_contas_pagar').getValue();
		var partesData = dataPrimeiroPagto.split("/");

		var primeiroPagto = new Date(partesData[2],partesData[1]-1,partesData[0],00,00,00);
		if(v!=null && v>=0 && v<=100){
			Ext.getCmp('fieldset_parcelamento').removeAll();
			var parcelas = total/v;
			var i2 = 0;

			for(var i=1; i<=v; i++){
				var now = new Date(primeiroPagto);
				var month = now.getMonth();
				var mes = now.setMonth(month + i2);

				Ext.getCmp('fieldset_parcelamento').add({
					xtype: 'fieldcontainer',
					autoHeight: true,
					layout: {
						type: 'hbox'
					},
					items: [
						{
							xtype: 'textfield',
							name: 'data_parcela[]',
							allowBlank: false,
							margin: '0 5 0 0',
							id: 'data_parcela_'+i+'_parcelamento',
							widht: 50,
							fieldLabel: 'Parcela '+i,
							mask: '99/99/9999',
							returnWithMask: true,
							plugins: 'textmask',
							value: Ext.Date.format(now, 'd/m/Y'),
							validator: function(value){
								if(value!="" && !validDate(value)){
									return 'Data Inválida...';
								}
								else{
									return true;
								}
							}
						},
						{
							xtype: 'textfield',
							id: 'nro_documento_'+i+'_parcelamento',
							name: 'documento[]',
							widht: 50,
							allowBlank: true,
							fieldLabel: 'Nro Documento '+i
						},
						{
							xtype: 'textfield',
							id: 'codigo_barras'+i+'_parcelamento',
							name: 'codigo_barras[]',
							flex: 1,
							anchor: '100%',
							allowBlank: true,
							fieldLabel: 'Código Barras '+i
						},
						{
							xtype: 'textfield',
							id: 'valor_parcela_'+i+'_parcelamento',
							name: 'valor_parcela[]',
							widht: 50,
							//anchor: '95%',
							allowBlank: false,
							value: parcelas,
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							fieldLabel: 'Valor da Parcela '+i
						}
					]
				});
				i2++;
			}
		}
	},

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	pagarConta: function(button){
		if (this.getList().selModel.hasSelection()) {
			var me = this;
			var record = me.getList().getSelectionModel().getLastSelected();
			me.getDesktopWindow('AddContas_Pagar_PagtosWin', 'Contas_Pagar_Pagtos', 'contas_pagar_pagtos.Edit', function(controller){
				Ext.getCmp('cod_conta_contas_pagar_pagtos').setValue(record.get('id'));
				Ext.getCmp('data_pagto_contas_pagar_pagtos').setValue(Ext.Date.format(new Date(), 'd/m/Y'));
				Ext.getCmp('valor_pago_contas_pagar_pagtos').setValue(record.get('saldo_a_pagar'));
				Ext.getCmp('tipo_pagto_contas_pagar_pagtos').setValue('6');
			});
		}
		else{
			info(this.titleErro, this.detalharErroGrid);
			return true;
		}
	},

	codigoBarras: function(button){
		if (this.getList().selModel.hasSelection()) {
			var me = this;
			var record = me.getList().getSelectionModel().getLastSelected();

			var valor = record.get('codigo_barras');
			Ext.Msg.show({
				prompt: true,
				title: "Copiar Código de Barras",
				minWidth: Ext.Msg.minPromptWidth,
				width: 420,
				msg: "Pressione Ctrl+C e depois Enter para copiar",
				buttons: Ext.Msg.OK,
				value: valor
			});

			Ext.Msg.textField.selectText();
		}
		else{
			info(this.titleErro, this.detalharErroGrid);
			return true;
		}
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('EditContas_PagarWin', 'Contas_Pagar', 'contas_pagar.Edit', function(){
			me.getAddWin().setTitle('Alterando Conta à Pagar');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/contas_pagar/list.php');
			Ext.getCmp('action_contas_pagar_edit').setValue('EDITAR');
			Ext.getCmp('GridContas_Pagar_Pagtos').setDisabled(false);
			Ext.getCmp('GridContas_Pagar_Pagtos').store.proxy.extraParams.cod_conta = record.get('id');
			Ext.getCmp('GridContas_Pagar_Pagtos').store.proxy.extraParams.saldo_a_pagar = record.get('saldo_a_pagar');
			Ext.getCmp('GridContas_Pagar_Pagtos').store.proxy.extraParams.quitado = record.get('quitado');
			var data = me.application.dados[me.tabela.toLowerCase()];

			Ext.getCmp('cadastrado_por_contas_pagar_edit').setVisible(true);
			Ext.getCmp('alterado_por_contas_pagar_edit').setVisible(true);
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'contas_pagar', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desaja Remover a Conta Selecionada?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	add: function(button) {
		var me = this;
		window.App = me;
		me.getDesktopWindow('Contas_Pagar', 'Contas_Pagar', 'contas_pagar.Add', function(){
			empresa = me.getCurrentEmpresa();
			Ext.getCmp('cod_empresa_contas_pagar').setValue(empresa.cod_empresa);
		});
	},


	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(dados){

/*
			if(Ext.getCmp('action_contas_pagar').getValue()=='INSERIR'){
				Ext.getCmp('action_contas_pagar').setValue('EDITAR');
				Ext.getCmp('GridContas_Pagar_Pagtos').setDisabled(false);				
				Ext.getCmp('GridContas_Pagar_Pagtos').store.proxy.extraParams.cod_conta = dados.id;
				Ext.getCmp('GridContas_Pagar_Pagtos').store.load();
			}
*/
		}, false);
	},

	RegistroSelecionado: function(selModel,record){
		var me = this;
		me.gridPanel = selModel.view.up('gridpanel');
		var saldo_a_pagar = record.get('saldo_a_pagar');

		if(this.existPermissao('contas_pagar', 'editar')==true && saldo_a_pagar > 0){
			Ext.getCmp('button_edit_contas_pagar').setDisabled(false);
		}

		if(this.existPermissao('contas_pagar', 'deletar')==true && saldo_a_pagar > 0){
			Ext.getCmp('button_del_contas_pagar').setDisabled(false);
		}

		if(this.existPermissao('contas_pagar', 'pagar_conta')==true && saldo_a_pagar > 0){
			Ext.getCmp('button_pagar_conta_contas_pagar').setDisabled(false);
			Ext.getCmp('button_codigo_barras_contas_pagar').setDisabled(false);
			if (record.get('codigo_barras') == null ||  record.get('codigo_barras') == "") {
				Ext.getCmp('button_codigo_barras_contas_pagar').setDisabled(true);
			}
		}

		if (saldo_a_pagar == 0){
			Ext.getCmp('button_edit_contas_pagar').setDisabled(true);
			Ext.getCmp('button_del_contas_pagar').setDisabled(true);
			Ext.getCmp('button_pagar_conta_contas_pagar').setDisabled(true);
			Ext.getCmp('button_codigo_barras_contas_pagar').setDisabled(true);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_contas_pagar').getValue();
		this.getList().store.load({
			start: 0,
			limit: 100
		});
	},

	Relatorio1: function(button){
		var me = this;
		window.App = me;

		me.btnFilter = button;
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.contas_pagar.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	Relatorio2: function(button){
		var me = this;
		window.App = me;

		me.btnFilter = button;
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.contas_pagar.Filtro2', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});