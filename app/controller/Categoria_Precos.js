/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Categoria_Precos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Categoria_Precos',
	
	refs: [
		{
			ref: 'list',
			selector: 'categoria_precoslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcategoria_precoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'categoria_precoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercategoria_precoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercategoria_precoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcategoria_precoswin'
		}
	],

	models: [
		'ModelCombo',
		'ModelCategoria_Precos'
	],
	stores: [
		'StoreComboCategoria_Precos',
		'StoreCategoria_Precos'		
	],
	views: [
		'categoria_precos.List',
		'categoria_precos.Edit'
	],

	init: function(application) {
		this.control({
			'categoria_precoslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'categoria_precoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'categoria_precoslist button[action=adicionar]': {
				click: this.add
			},
			'categoria_precoslist button[action=editar]': {
				click: this.btedit
			},
			'categoria_precoslist button[action=deletar]': {
				click: this.btdel
			},            'addcategoria_precoswin button[action=salvar]': {
				click: this.update
			},
			'addcategoria_precoswin button[action=resetar]': {
				click: this.reset
			},
			'addcategoria_precoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcategoria_precoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcategoria_precoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercategoria_precoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercategoria_precoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercategoria_precoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercategoria_precoswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddCategoria_PrecosWin', 'Categoria_Precos', 'categoria_precos.Edit', function(){
			me.getAddWin().setTitle('Alterar Tabela de Preços');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_categoria_precos'), 'server/modulos/categoria_precos/list.php');
			Ext.getCmp('action_categoria_precos').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'categoria_precos', {
			action: 'DELETAR',
			id: record.get('id_categoria_precos')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar a Tabela de Preços: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Categoria_Precos', 'Categoria_Precos', 'categoria_precos.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('categoria_precos', 'editar')==true){
			Ext.getCmp('button_edit_categoria_precos').setDisabled(false);
		}

		if(this.existPermissao('categoria_precos', 'deletar')==true){
			Ext.getCmp('button_del_categoria_precos').setDisabled(false);
		}
	},
	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.categoria_precos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});