/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produtos_Cores', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Produtos_Cores',
	noDeleteLoad: false,
	
	refs: [
		{
			ref: 'list',
			selector: 'gridprodutos_coreslist'
		},
		{
			ref: 'form',
			selector: 'addprodutos_coreswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridprodutos_coreslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterprodutos_coreswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterprodutos_coreswin form'
		},
		{
			ref: 'addWin',
			selector: 'addprodutos_coreswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelProdutos_Cores'
	],
	stores: [
		'StoreComboCores',
		'StoreProdutos_Cores'
	],
	
	views: [
		'produtos_cores.Grid',
		'produtos_cores.List',
		'produtos_cores.Edit'
	],

	init: function(application) {
		this.control({
			'gridprodutos_coreslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'gridprodutos_coreslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridprodutos_coreslist button[action=adicionar]': {
				click: this.add
			},
			'gridprodutos_coreslist button[action=editar]': {
				click: this.btedit
			},
			'gridprodutos_coreslist button[action=deletar]': {
				click: this.btdel
			},
			'addprodutos_coreswin button[action=salvar]': {
				click: this.update
			},
			'addprodutos_coreswin button[action=resetar]': {
				click: this.reset
			},
			'addprodutos_coreswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addprodutos_coreswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addprodutos_coreswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterprodutos_coreswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterprodutos_coreswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterprodutos_coreswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterprodutos_coreswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddProdutos_CoresWin', 'Produtos_Cores', 'produtos_cores.Edit', function(){
			me.getAddWin().setTitle('Alteração das Cores do Produto');
			if(record.get('id_produto')>0){
				me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_produtos_cores'), 'server/modulos/produtos_cores/list.php');
			}
			else{
				me.getForm().getForm().setValues(record.data);
			}

			Ext.getCmp('action_produtos_cores').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		
		if(record.get('id_produtos_cores')>0){
			me.deleteAjax(grid, 'produtos_cores', {
				action: 'DELETAR',
				id: record.get('id_produtos_cores')
			}, button, false);
		}
		//grid.store.remove(record);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar a cor do produto?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Produtos_Cores', 'Produtos_Cores', 'produtos_cores.Edit', function(){
			var id_produto = Ext.getCmp('id_produtos').getValue();
			Ext.getCmp('id_produto_produtos_cores').setValue(me.getList().store.proxy.extraParams.id_produto);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.produtos_cores.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});
