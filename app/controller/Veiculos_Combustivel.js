/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Veiculos_Combustivel', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Veiculos_Combustivel',

	refs: [
		{
			ref: 'list',
			selector: 'gridveiculos_combustivellist'
		},
		{
			ref: 'form',
			selector: 'addveiculos_combustivelwin form'
		},
		{
			ref: 'filterBtn',
			selector: 'veiculos_combustivellist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterveiculos_combustivelwin'
		},
		{
			ref: 'filterForm',
			selector: 'filterveiculos_combustivelwin form'
		},
		{
			ref: 'addWin',
			selector: 'addveiculos_combustivelwin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelVeiculos_Combustivel'
	],
	stores: [
		'StoreVeiculos_Combustivel',
		'StoreComboOsResponsavel',
		'StoreComboCombustivelVeiculos'
	],

	views: [
		'veiculos_combustivel.Grid',
		'veiculos_combustivel.List',
		'veiculos_combustivel.Filtro',
		'veiculos_combustivel.Edit',
	],

	init: function(application) {
		this.control({
			'gridveiculos_combustivellist': { 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'gridveiculos_combustivellist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridveiculos_combustivellist button[action=adicionar]': {
				click: this.add
			},
			'gridveiculos_combustivellist button[action=editar]': {
				click: this.btedit
			},
			'gridveiculos_combustivellist button[action=deletar]': {
				click: this.btdel
			},
			'addveiculos_combustivelwin button[action=salvar]': {
				click: this.update
			},
			'addveiculos_combustivelwin button[action=resetar]': {
				click: this.reset
			},
			'addveiculos_combustivelwin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addveiculos_combustivelwin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addveiculos_combustivelwin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterveiculos_combustivelwin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterveiculos_combustivelwin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterveiculos_combustivelwin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterveiculos_combustivelwin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddVeiculos_CombustivelWin', 'Veiculos_Combustivel', 'veiculos_combustivel.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Veiculos_Combustivel');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/veiculos_combustivel/list.php');
			Ext.getCmp('action_veiculos_combustivel').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'veiculos_combustivel', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Veiculos_Combustivel', 'Veiculos_Combustivel', 'veiculos_combustivel.Edit', function(){
			Ext.getCmp('cod_veiculo_veiculos_combustivel').setValue(me.getList().store.proxy.extraParams.cod_veiculo);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('veiculos_combustivel', 'editar')==true){
			Ext.getCmp('button_edit_veiculos_combustivel').setDisabled(false);
		}

		if(this.existPermissao('veiculos_combustivel', 'deletar')==true){
			Ext.getCmp('button_del_veiculos_combustivel').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.veiculos_combustivel.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});