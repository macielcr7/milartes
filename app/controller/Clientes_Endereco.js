/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Clientes_Endereco', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Clientes_Endereco',
	started: true,
	
	refs: [
		{
			ref: 'list',
			selector: 'clientes_enderecolist'
		},
		{
			ref: 'form',
			selector: 'addclientes_enderecowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'clientes_enderecolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterclientes_enderecowin'
		},
		{
			ref: 'filterForm',
			selector: 'filterclientes_enderecowin form'
		},
		{
			ref: 'addWin',
			selector: 'addclientes_enderecowin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelClientes_Endereco'
	],
	stores: [
		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboCorreios_Bairros',
		'StoreComboCorreios_Enderecos',
		'StoreClientes_Endereco',
		'StoreComboTipo_EnderecoClientes'
	],
	
	views: [
		'clientes_endereco.List',
		'clientes_endereco.Filtro',
		'clientes_endereco.Edit'
	],

	init: function(application) {
		this.control({
			'clientes_enderecolist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'clientes_enderecolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'clientes_enderecolist button[action=adicionar]': {
				click: this.add
			},
			'clientes_enderecolist button[action=editar]': {
				click: this.btedit
			},
			'clientes_enderecolist button[action=deletar]': {
				click: this.btdel
			},			
			'clientes_enderecolist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},

			'addclientes_enderecowin button[action=salvar]': {
				click: this.update
			},
			'addclientes_enderecowin button[action=resetar]': {
				click: this.reset
			},
			'addclientes_enderecowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addclientes_enderecowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addclientes_enderecowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addclientes_enderecowin form fieldcontainer combobox[id=estado_clientes_endereco]': {
				change: this.loadCidade
			},
			'addclientes_enderecowin form fieldcontainer combobox[id=cidade_clientes_endereco]': {
				change: this.loadBairro
			},
			'addclientes_enderecowin form fieldcontainer combobox[id=logradouro_clientes_endereco]': {
				change: this.setEndereco
			},
			'filterclientes_enderecowin form fieldcontainer combobox[id=estado_filter_clientes_endereco]': {
				change: this.loadCidade
			},
			'filterclientes_enderecowin form fieldcontainer combobox[id=cidade_filter_clientes_endereco]': {
				change: this.loadBairro
			},
			'filterclientes_enderecowin form fieldcontainer combobox[id=bairro_filter_clientes_endereco]': {
				change: this.loadEndereco
			},
			'filterclientes_enderecowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterclientes_enderecowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterclientes_enderecowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterclientes_enderecowin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	endereco: false,

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			
			Ext.getCmp('bairro_nome_clientes_endereco').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_clientes_endereco').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_clientes_endereco').setValue(record.get('loteamento'));
			}
			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_clientes_enderecos').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_clientes_endereco').setValue(record.get('cep'));
			Ext.getCmp('num_end_clientes_endereco').focus(false, 100);
		}
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('cidade_clientes_endereco');
		}
		else{
			var comboCidade = Ext.getCmp('cidade_filter_clientes_endereco');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			Ext.getCmp('logradouro_clientes_endereco').store.proxy.extraParams.localidade_id = combo.getValue();
			me.bairro = true;
			if(Ext.getCmp('logradouro_clientes_endereco').isDisabled()){
				Ext.getCmp('logradouro_clientes_endereco').setDisabled(false);
			}
			return true;
		}
		else{
			var comboCidade = Ext.getCmp('bairro_filter_clientes_endereco');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('logradouro_clientes_endereco');
		}
		else{
			var comboCidade = Ext.getCmp('logradouro_filter_clientes_endereco');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},
   
	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/clientes_endereco/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('logradouro_clientes_endereco');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddClientes_EnderecoWin', 'Clientes_Endereco', 'clientes_endereco.Edit', function(dados){
			me.getAddWin().setTitle('Alteração de Endereço');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/clientes_endereco/list.php', function(dados){
				me.getForm().el.mask('Aguarde...');
				window.controlS = me;
				
				var a = setInterval(function() {
					if(dados.cidade != null && dados.cidade != "") {
						if(me.cidade==true){
							Ext.getCmp('cidade_clientes_endereco').setValue(dados.cidade);

							me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
							me.cidade = null;
						}
					}
					else {
							me.cidade = null;
					}

					if(dados.logradouro != null && dados.logradouro != ""){
						if(me.endereco==true){
							Ext.getCmp('logradouro_clientes_endereco').setValue(parseInt(dados.logradouro));
							me.endereco = null;
						}
					}
					else {
						me.endereco = null;
					}
					if(me.cidade == null && me.endereco == null) {
						if(me.getForm()) {
							me.getForm().el.unmask();
						}
						
						clearInterval(a);
						me.cidade = false;
						me.endereco = false;
					}
				}, 300);
			});
			Ext.getCmp('action_clientes_endereco').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'clientes_endereco', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar este Endereço?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	add: function(button) {
		var me = this;
		window.App = me;
		me.cidade = false;
		me.getDesktopWindow('Clientes_Endereco', 'Clientes_Endereco', 'clientes_endereco.Edit', function(){
			Ext.getCmp('cod_cliente_clientes_endereco').setValue(me.getList().store.proxy.extraParams.cod_cliente);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_clientes_endereco').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					if(me.cidade==true){
						var record = Ext.getCmp('cidade_clientes_endereco').store.findRecord('descricao', empresa.cidade);
						if(record){
							Ext.getCmp('cidade_clientes_endereco').setValue(record.get('id'));
							Ext.getCmp('logradouro_clientes_endereco').focus(false, 200);
						}
						me.cidade = null;
					}
				}else{me.cidade = null;}
				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('clientes_endereco', 'editar')==true){
			Ext.getCmp('button_edit_clientes_endereco').setDisabled(false);
		}

		if(this.existPermissao('clientes_endereco', 'deletar')==true){
			Ext.getCmp('button_del_clientes_endereco').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.clientes_endereco.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});
