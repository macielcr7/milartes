/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Contas_Bancarias', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Contas_Bancarias',

	refs: [
		{
			ref: 'list',
			selector: 'contas_bancariaslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcontas_bancariaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'contas_bancariaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercontas_bancariaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercontas_bancariaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcontas_bancariaswin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelContas_Bancarias'
	],
	stores: [
		'StoreComboAtivo',
		'StoreContas_Bancarias',
		'StoreComboContasBancariasEmpresas'
	],
	
	views: [
		'contas_bancarias.List',
		'contas_bancarias.Edit'
	],

	init: function(application) {
		this.control({
			'contas_bancariaslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'contas_bancariaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'contas_bancariaslist button[action=adicionar]': {
				click: this.add
			},
			'contas_bancariaslist button[action=editar]': {
				click: this.btedit
			},
			'contas_bancariaslist button[action=deletar]': {
				click: this.btdel
			},
			'contas_bancariaslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addcontas_bancariaswin button[action=salvar]': {
				click: this.update
			},
			'addcontas_bancariaswin button[action=resetar]': {
				click: this.reset
			},
			'addcontas_bancariaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addcontas_bancariaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddContas_BancariasWin', 'Contas_Bancarias', 'contas_bancarias.Edit', function(){
			me.getAddWin().setTitle('Alterando Dados Bancários');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/contas_bancarias/list.php');
			Ext.getCmp('action_contas_bancarias').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'contas_bancarias', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar a Conta Bancária?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Contas_Bancarias', 'Contas_Bancarias', 'contas_bancarias.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('contas_bancarias', 'editar')==true){
			Ext.getCmp('button_edit_contas_bancarias').setDisabled(false);
		}

		if(this.existPermissao('contas_bancarias', 'deletar')==true){
			Ext.getCmp('button_del_contas_bancarias').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_contas_bancarias').getValue();
		this.getList().store.load({
			start: 0,
			limit: 25
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridContas_Bancarias').store.proxy.extraParams.status_conta = item.value;
		Ext.getCmp('GridContas_Bancarias').store.load({
			start: 0,
			limit: 50
		});
	}
});