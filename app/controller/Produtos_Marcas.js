/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produtos_Marcas', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Produtos_Marcas',

	refs: [
		{
			ref: 'list',
			selector: 'produtos_marcaslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addprodutos_marcaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'produtos_marcaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterprodutos_marcaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterprodutos_marcaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addprodutos_marcaswin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelProdutos_Marcas'
	],
	stores: [
		'StoreComboAtivo',
		'StoreProdutos_Marcas'
	],
	
	views: [
		'produtos_marcas.List',
		'produtos_marcas.Edit'
	],

	init: function(application) {
		this.control({
			'produtos_marcaslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'produtos_marcaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'produtos_marcaslist button[action=adicionar]': {
				click: this.add
			},
			'produtos_marcaslist button[action=editar]': {
				click: this.btedit
			},
			'produtos_marcaslist button[action=deletar]': {
				click: this.btdel
			},
			'produtos_marcaslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addprodutos_marcaswin button[action=salvar]': {
				click: this.update
			},
			'addprodutos_marcaswin button[action=resetar]': {
				click: this.reset
			},
			'addprodutos_marcaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterprodutos_marcaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterprodutos_marcaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterprodutos_marcaswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterprodutos_marcaswin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddProdutos_MarcasWin', 'Produtos_Marcas', 'produtos_marcas.Edit', function(){
			me.getAddWin().setTitle('Alterar Marcas de Produtos');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_marca'), 'server/modulos/produtos_marcas/list.php');
			Ext.getCmp('action_produtos_marcas').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'produtos_marcas', {
			action: 'DELETAR',
			id: record.get('cod_marca')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desativar a Marca: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Produtos_Marcas', 'Produtos_Marcas', 'produtos_marcas.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('produtos_marcas', 'editar')==true){
			Ext.getCmp('button_edit_produtos_marcas').setDisabled(false);
		}

		if(this.existPermissao('produtos_marcas', 'deletar')==true){
			Ext.getCmp('button_del_produtos_marcas').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_produtos_marcas').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridProdutos_Marcas').store.proxy.extraParams.status_marca = item.value;
		Ext.getCmp('GridProdutos_Marcas').store.load({
			start: 0,
			limit: 50
		});
	}
});