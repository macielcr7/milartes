/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Contas_Pagar_Pagtos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Contas_Pagar_Pagtos',
	started: true,
	
	refs: [
		{
			ref: 'list',
			selector: 'contas_pagar_pagtoslist'
		},
		{
			ref: 'form',
			selector: 'addcontas_pagar_pagtoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'contas_pagar_pagtoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercontas_pagar_pagtoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercontas_pagar_pagtoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcontas_pagar_pagtoswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelContas_Pagar_Pagtos'
	],
	stores: [
		'StoreContas_Pagar_Pagtos',
		'StoreComboTipo_ContaContasPagar'
	],
	
	views: [
		'contas_pagar_pagtos.List',
		'contas_pagar_pagtos.Edit'
	],

	init: function(application) {
		this.control({
			'contas_pagar_pagtoslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'contas_pagar_pagtoslist button[action=adicionar]': {
				click: this.add
			},
			'contas_pagar_pagtoslist button[action=editar]': {
				click: this.btedit
			},
			'contas_pagar_pagtoslist button[action=deletar]': {
				click: this.btdel
			},			
			'contas_pagar_pagtoslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addcontas_pagar_pagtoswin button[action=salvar]': {
				click: this.update
			},
			'addcontas_pagar_pagtoswin button[action=resetar]': {
				click: this.reset
			},
			'addcontas_pagar_pagtoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcontas_pagar_pagtoswin form fieldcontainer combobox[name=tipo_contato]': {
				change: this.alterTextField
			},
			'addcontas_pagar_pagtoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcontas_pagar_pagtoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			}
		});
	},

	alterTextField: function(combo){
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		if(record.get('id')=='TELEFONE'){
			Ext.getCmp('descricao_tel_contas_pagar_pagtos').setDisabled(false);
			Ext.getCmp('descricao_tel_contas_pagar_pagtos').setVisible(true);

			Ext.getCmp('descricao_contas_pagar_pagtos').setDisabled(true);
			Ext.getCmp('descricao_contas_pagar_pagtos').setVisible(false);
		}
		else{
			Ext.getCmp('descricao_tel_contas_pagar_pagtos').setDisabled(true);
			Ext.getCmp('descricao_tel_contas_pagar_pagtos').setVisible(false);

			Ext.getCmp('descricao_contas_pagar_pagtos').setDisabled(false);
			Ext.getCmp('descricao_contas_pagar_pagtos').setVisible(true);	
		}
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/contas_pagar_pagtos/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddContas_Pagar_PagtosWin', 'Contas_Pagar_Pagtos', 'contas_pagar_pagtos.Edit', function(){
			me.getAddWin().setTitle('Alteração de Pagamento');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/contas_pagar_pagtos/list.php');
			Ext.getCmp('action_contas_pagar_pagtos').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'contas_pagar_pagtos', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Remover o Pagamento de R$: '+record.get('valor_pago')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Contas_Pagar_Pagtos', 'Contas_Pagar_Pagtos', 'contas_pagar_pagtos.Edit', function(){
			Ext.getCmp('cod_conta_contas_pagar_pagtos').setValue(me.getList().store.proxy.extraParams.cod_conta);
			Ext.getCmp('data_pagto_contas_pagar_pagtos').setValue(Ext.Date.format(new Date(), 'd/m/Y'));
			Ext.getCmp('valor_pago_contas_pagar_pagtos').setValue(me.getList().store.proxy.extraParams.saldo_a_pagar);
		});
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('contas_pagar_pagtos', 'deletar')==true){
			Ext.getCmp('button_del_contas_pagar_pagtos').setDisabled(false);
		}
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(){
			var grid = Ext.getCmp('GridContas_Pagar');
			if(grid && grid.getEl()){
				grid.store.load();
			}
		}, false);
	}
});