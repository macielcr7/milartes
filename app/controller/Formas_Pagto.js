/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Formas_Pagto', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Formas_Pagto',

	refs: [
		{
			ref: 'list',
			selector: 'formas_pagtolist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addformas_pagtowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'formas_pagtolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterformas_pagtowin'
		},
		{
			ref: 'filterForm',
			selector: 'filterformas_pagtowin form'
		},
		{
			ref: 'addWin',
			selector: 'addformas_pagtowin'
		}
	],
	models: [
		'ModelCombo',
		'ModelFormas_Pagto'
	],
	stores: [
		'StoreFormas_Pagto',
		'StoreComboFormasPagtoCategoria',
		'StoreComboFormasPagtoTipo',
		'StoreComboAtivo'
	],

	views: [
		'formas_pagto.List',
		'formas_pagto.Edit'
	],

	init: function(application) {
		this.control({
			'formas_pagtolist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'formas_pagtolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'formas_pagtolist button[action=adicionar]': {
				click: this.add
			},
			'formas_pagtolist button[action=editar]': {
				click: this.btedit
			},
			'formas_pagtolist button[action=deletar]': {
				click: this.btdel
			},
			'formas_pagtolist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addformas_pagtowin button[action=salvar]': {
				click: this.update
			},
			'addformas_pagtowin button[action=resetar]': {
				click: this.reset
			},
			'addformas_pagtowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addformas_pagtoswin form fieldcontainer combobox[id=categoria_formas_pagto]': {
				change: this.habilitar
			},
			'filterformas_pagtowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterformas_pagtowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterformas_pagtowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterformas_pagtowin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddFormas_PagtoWin', 'Formas_Pagto', 'formas_pagto.Edit', function(){
			me.getAddWin().setTitle('Alterar Forma de Pagamento');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/formas_pagto/list.php');
			Ext.getCmp('action_formas_pagto').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'formas_pagto', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desativar a Forma de Pagamento: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Formas_Pagto', 'Formas_Pagto', 'formas_pagto.Edit', function(){
		});
	},

	habilitar: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(combo.getValue()=='2'){
			Ext.getCmp('convenio_formas_pagto').setVisible(true);
		}
		else if(combo.getValue()=='3'){
			Ext.getCmp('convenio_formas_pagto').setVisible(true);
		}
		else{
			Ext.getCmp('convenio_formas_pagto').setVisible(false);
		}
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('formas_pagto', 'editar')==true){
			Ext.getCmp('button_edit_formas_pagto').setDisabled(false);
		}

		if(this.existPermissao('formas_pagto', 'deletar')==true){
			Ext.getCmp('button_del_formas_pagto').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_formas_pagto').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridFormas_Pagto').store.proxy.extraParams.status_forma = item.value;
		Ext.getCmp('GridFormas_Pagto').store.load({
			start: 0,
			limit: 50
		});
	}
});