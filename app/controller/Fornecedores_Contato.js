/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Fornecedores_Contato', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Fornecedores_Contato',
	started: true,

	refs: [
		{
			ref: 'list',
			selector: 'fornecedores_contatolist'
		},
		{
			ref: 'form',
			selector: 'addfornecedores_contatowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'fornecedores_contatolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterfornecedores_contatowin'
		},
		{
			ref: 'filterForm',
			selector: 'filterfornecedores_contatowin form'
		},
		{
			ref: 'addWin',
			selector: 'addfornecedores_contatowin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelFornecedores_Contato'
	],
	stores: [
		'StoreFornecedores_Contato',
		'StoreComboTipo_ContatoFornecedoresContato'
	],

	views: [
		'fornecedores_contato.List',
		'fornecedores_contato.Filtro',
		'fornecedores_contato.Edit'
	],

	init: function(application) {
		this.control({
			'fornecedores_contatolist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'fornecedores_contatolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'fornecedores_contatolist button[action=adicionar]': {
				click: this.add
			},
			'fornecedores_contatolist button[action=editar]': {
				click: this.btedit
			},
			'fornecedores_contatolist button[action=deletar]': {
				click: this.btdel
			},
			'fornecedores_contatolist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addfornecedores_contatowin button[action=salvar]': {
				click: this.update
			},
			'addfornecedores_contatowin button[action=resetar]': {
				click: this.reset
			},
			'addfornecedores_contatowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addfornecedores_contatowin form fieldcontainer combobox[name=tipo_contato]': {
				change: this.alterTextField
			},
			'addfornecedores_contatowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addfornecedores_contatowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterfornecedores_contatowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterfornecedores_contatowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterfornecedores_contatowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterfornecedores_contatowin': {
				show: this.filterSetFields
			}
		});
	},

	alterTextField: function(combo){
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		if(record.get('id')=='TELEFONE'){
			Ext.getCmp('descricao_tel_fornecedores_contato').setDisabled(false);
			Ext.getCmp('descricao_tel_fornecedores_contato').setVisible(true);

			Ext.getCmp('descricao_tel_ddg_fornecedores_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_ddg_fornecedores_contato').setVisible(false);
			Ext.getCmp('descricao_fornecedores_contato').setDisabled(true);
			Ext.getCmp('descricao_fornecedores_contato').setVisible(false);
		}
		else if(record.get('id')=='DDG'){
			Ext.getCmp('descricao_tel_ddg_fornecedores_contato').setDisabled(false);
			Ext.getCmp('descricao_tel_ddg_fornecedores_contato').setVisible(true);

			Ext.getCmp('descricao_fornecedores_contato').setDisabled(true);
			Ext.getCmp('descricao_fornecedores_contato').setVisible(false);
			Ext.getCmp('descricao_tel_fornecedores_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_fornecedores_contato').setVisible(false);
		}
		else{
			Ext.getCmp('descricao_tel_fornecedores_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_fornecedores_contato').setVisible(false);
			Ext.getCmp('descricao_tel_ddg_fornecedores_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_ddg_fornecedores_contato').setVisible(false);

			Ext.getCmp('descricao_fornecedores_contato').setDisabled(false);
			Ext.getCmp('descricao_fornecedores_contato').setVisible(true);	
		}
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/fornecedores_contato/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddFornecedores_ContatoWin', 'Fornecedores_Contato', 'fornecedores_contato.Edit', function(){
			me.getAddWin().setTitle('Alteração de Contato');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/fornecedores_contato/list.php');
			Ext.getCmp('action_fornecedores_contato').setValue('EDITAR');
			Ext.getCmp('tipo_contato_fornecedores_contato').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'fornecedores_contato', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar o Contato: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Fornecedores_Contato', 'Fornecedores_Contato', 'fornecedores_contato.Edit', function(){
			Ext.getCmp('cod_fornecedor_fornecedores_contato').setValue(me.getList().store.proxy.extraParams.cod_fornecedor);
			Ext.getCmp('descricao_tel_fornecedores_contato').focus(false, 1000);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('fornecedores_contato', 'editar')==true){
			Ext.getCmp('button_edit_fornecedores_contato').setDisabled(false);
		}

		if(this.existPermissao('fornecedores_contato', 'deletar')==true){
			Ext.getCmp('button_del_fornecedores_contato').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.fornecedores_contato.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});