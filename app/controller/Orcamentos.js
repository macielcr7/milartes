/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Orcamentos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Orcamentos',
	started: true,

	dependeciesControllers: [
		'Itens_Orcamento',
		'Orcamentos_Observacoes',
		'Vendas_Pagamentos'
	],
	refs: [
		{
			ref: 'list',
			selector: 'gridorcamentoslist'
		},
		{
			ref: 'form',
			selector: 'addorcamentoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridorcamentoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterorcamentoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterorcamentoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addorcamentoswin'
		}
	],
	models: [
		'ModelCombo',
		'ModelOrcamentos'
	],
	stores: [
		'StoreComboClientes',
		'StoreComboOrcamentosClientes',
		'StoreComboOrcamentista',
		'StoreComboProdutos',
		'StoreOrcamentos',
		'StoreComboOrcamentosVendas',
		'StoreComboFormas_Pagto'
	],
	views: [
		'orcamentos.Grid',
		'orcamentos.List',
		'orcamentos.Filtro',
		'orcamentos.Edit'
	],

	menuGrid: Ext.create('Ext.menu.Menu', {
		id: 'menu_grid_orcamentos',
		width: 240,
		items: [
			{
				text: 'Imprimir - Versão Cliente',
				id: 'menuItem_imprime_cliente_orcamentos',
				iconCls: 'bt_imprimir2',
				action: 'impressao_cliente'
			},
			{
				text: 'Imprimir - Versão Depósito',
				id: 'menuItem_imprime_deposito_orcamentos',
				iconCls: 'bt_imprimir2',
				action: 'impressao_deposito'
			},
			{
				text: 'Converter Orçamento em Venda',
				id: 'menuItem_converte_em_venda_orcamentos',
				iconCls: 'bt_converte',
				action: 'converte_em_venda'
			}
		]
	}),

	id_categoria_precos: 1,

	init: function(application) {
		this.control({
			'#menu_grid_orcamentos': {
				click: this.menuClick
			},
			'gridorcamentoslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado,
				itemcontextmenu: this.contextMenu
			},
			'gridorcamentoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridorcamentoslist button[action=gerar_pdf]': {
				click: this.btPDF
			},
			'gridorcamentoslist menuitem[action=impressao_cliente]': {
				click: this.btPrintCliente
			},
			'gridorcamentoslist menuitem[action=impressao_deposito]': {
				click: this.btPrintDeposito
			},
			'gridorcamentoslist menuitem[action=geral]': {
				click: this.addGeral
			},
			'gridorcamentoslist menuitem[action=forro]': {
				click: this.addForro
			},
			'gridorcamentoslist menuitem[action=piso]': {
				click: this.addPiso
			},
			'gridorcamentoslist menuitem[action=rodape]': {
				click: this.addRodape
			},
			'gridorcamentoslist button[action=editar]': {
				click: this.btedit
			},
			'gridorcamentoslist button[action=deletar]': {
				click: this.btdel
			},
			'gridorcamentoslist button[action=copiar_orcamento]': {
				click: this.btcopia
			},
			'gridorcamentoslist button[action=converte_em_venda]': {
				click: this.btConverteVenda
			},
			'addorcamentoswin button[action=salvar]': {
				click: this.update
			},
			'addorcamentoswin button[action=resetar]': {
				click: this.reset
			},
			'addorcamentoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addorcamentoswin form textfield[id=valor_total_orcamentos]': {
				blur: this.changeTotal
			},
			'addorcamentoswin form textfield[id=acrescimo_porcento_orcamentos], addorcamentoswin form textfield[id=acrescimo_real_orcamentos]': {
				keyup: this.changeAcrescimo
			},
			'addorcamentoswin form numberfield[id=desconto_porcento_orcamentos], addorcamentoswin form textfield[id=desconto_real_orcamentos]': {
				keyup: this.changeDesconto
			},
			'addorcamentoswin form fieldcontainer combobox[id=id_cliente_orcamentos]': {
				change: this.changeGridValores
			},
			'addorcamentoswin checkbox[id=cliente_cadastrado_orcamentos]': {
				change: this.enableCliente
			},
			'addorcamentoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addorcamentoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addorcamentoswin form fieldcontainer combobox[id=estado_orcamentos]': {
				change: this.loadCidade
			},
			'addorcamentoswin form fieldcontainer combobox[id=cidade_orcamentos]': {
				change: this.loadBairro
			},
			'addorcamentoswin form fieldcontainer combobox[id=endereco_orcamentos]': {
				change: this.setEndereco
			},/*
			'addorcamentoswin form textfield[id=id_os_orcamentos]': {
				blur: this.loadDadosOS
			},*/
			'filterorcamentoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterorcamentoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterorcamentoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterorcamentoswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	endereco: false,

	contextMenu: function(view, record, item, index, e){
		this.recordEdit = record;
		e.stopEvent();

		if(record.get('convertido_venda') == "N") {
			Ext.getCmp('menuItem_converte_em_venda_orcamentos').setVisible(true);
		}
		else {
			Ext.getCmp('menuItem_converte_em_venda_orcamentos').setVisible(false);
		}

		this.menuGrid.showAt(e.getXY());
	},

	menuClick: function(menu, item, e, eOpts){
		var me = this;
		var record = this.recordEdit;
		var grid = Ext.getCmp('GridOrcamentos');

		if(item.action=='converte_em_venda'){
			if (me.getList().selModel.hasSelection()) {
				Ext.Msg.confirm('Confirmar', 'Deseja Converter o Orçamento <b>'+record.get('id_orcamento')+'</b> em Venda?', function(btn){
				if(btn=='yes'){
					me.GeraCopia(me.getList(), record, item, 'S');
				}
			});
							
			}
		}
		else if(item.action=='impressao_cliente'){
			if (me.getList().selModel.hasSelection()) {
				me.btPrintCliente(grid, record, menu);
			}
		}
		else if(item.action=='impressao_deposito'){
			if (me.getList().selModel.hasSelection()) {
				me.btPrintDeposito(grid, record, menu);
			}
		}
	},

	loadDadosOS: function(field){
		var me = this;
		var v = field.getValue();
		Ext.Ajax.request({
			url : 'server/modulos/os/list.php',
			params : {
				action: 'GET_VALUES',
				id : v
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'O Número de OS digitado não foi Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos').setDisabled(false);

					if (resposta.dados.cliente==null){
						Ext.getCmp('aos_cuidados_de_orcamentos').setValue('');
						Ext.getCmp('cliente_descricao_orcamentos').setVisible(true);
						Ext.getCmp('cliente_descricao_orcamentos').setDisabled(false);
						Ext.getCmp('fieldcontainer_id_cliente_orcamentos').setVisible(false);
						Ext.getCmp('cliente_cadastrado_orcamentos').setValue(false);
						Ext.getCmp('id_cliente_orcamentos').setDisabled(true);
						Ext.getCmp('cliente_descricao_orcamentos').setValue(resposta.dados.titulo);
					}
					else{
						Ext.getCmp('cliente_descricao_orcamentos').setVisible(false);
						Ext.getCmp('cliente_descricao_orcamentos').setDisabled(true);
						Ext.getCmp('fieldcontainer_id_cliente_orcamentos').setVisible(true);
						Ext.getCmp('cliente_cadastrado_orcamentos').setValue(true);
						Ext.getCmp('id_cliente_orcamentos').setDisabled(false);
						Ext.getCmp('aos_cuidados_de_orcamentos').setValue('');

						if (resposta.dados.titulo != resposta.dados.nome_cliente){
							Ext.getCmp('aos_cuidados_de_orcamentos').setValue(resposta.dados.titulo);
						}

						Ext.getCmp('id_cliente_orcamentos').store.load({
							params: {
								id_cliente: resposta.dados.cliente.toString()
							}
						});

						var c = setInterval(function(){
							me.verificaAnotacoesCliente(resposta.dados.cliente);
							Ext.getCmp('id_cliente_orcamentos').setValue(resposta.dados.cliente.toString());
							clearInterval(c);
						}, 400);
					}

					var texto = "Deseja ATUALIZAR os dados de Endereco deste orçamento com os mesmos dados da OS?";
					Ext.Msg.confirm('Confirmar', texto, function(btn){
						if(btn=='yes'){
							me.loadEnderecoOS(v);
						}
					});
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadEnderecoOS: function(os_nro){
		var me = this;
		Ext.Ajax.request({
			url : 'server/modulos/os/list.php',
			params : {
				action: 'GET_VALUES',
				id : os_nro
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'O Número de OS digitado não foi Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
					Ext.getCmp('estado_orcamentos').setValue(resposta.dados.estado);
					Ext.getCmp('ponto_ref_orcamentos').setValue(resposta.dados.endereco_referencia);
					Ext.getCmp('complemento_orcamentos').setValue(resposta.dados.endereco_complemento);
					Ext.getCmp('nro_end_orcamentos').setValue(resposta.dados.endereco_num);
					Ext.getCmp('loteamento_orcamentos').setValue(resposta.dados.loteamento);
					Ext.getCmp('fone1_orcamentos').setValue(resposta.dados.fone1);
					Ext.getCmp('fone2_orcamentos').setValue(resposta.dados.fone2);
					Ext.getCmp('fone3_orcamentos').setValue(resposta.dados.fone3);
					Ext.getCmp('fone4_orcamentos').setValue(resposta.dados.fone4);
					Ext.getCmp('fone5_orcamentos').setValue(resposta.dados.fone5);
					Ext.getCmp('fone1_obs_orcamentos').setValue(resposta.dados.fone1_obs);
					Ext.getCmp('fone2_obs_orcamentos').setValue(resposta.dados.fone2_obs);
					Ext.getCmp('fone3_obs_orcamentos').setValue(resposta.dados.fone3_obs);
					Ext.getCmp('fone4_obs_orcamentos').setValue(resposta.dados.fone4_obs);
					Ext.getCmp('fone5_obs_orcamentos').setValue(resposta.dados.fone5_obs);

					var a = setInterval(function(){
						Ext.getCmp('cidade_orcamentos').setValue(resposta.dados.cidade);
						me.comboEndereco(resposta.dados.cidade, resposta.dados.endereco_bairro, resposta.dados.endereco);
						clearInterval(a);
					}, 300);

					var b = setInterval(function(){
						Ext.getCmp('endereco_orcamentos').setValue(parseInt(resposta.dados.endereco));
						me.cidade = null;
						clearInterval(b);
					}, 400);

					if (resposta.dados.cliente!=null){
						Ext.getCmp('aos_cuidados_de_orcamentos').setValue('');

						if (resposta.dados.titulo != resposta.dados.nome_cliente){
							Ext.getCmp('aos_cuidados_de_orcamentos').setValue(resposta.dados.titulo);
						}
					}
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadEnderecoCliente: function(id_cliente){
		var me = this;
		Ext.Ajax.request({
			url : 'server/modulos/clientes/list.php',
			params : {
				action: 'GET_VALUES_ORCAMENTOS',
				id : id_cliente
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'Cliente não Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
					Ext.getCmp('estado_orcamentos').setValue(resposta.dados[0].estado);
					Ext.getCmp('ponto_ref_orcamentos').setValue(resposta.dados[0].ponto_ref);
					Ext.getCmp('complemento_orcamentos').setValue(resposta.dados[0].complemento);
					Ext.getCmp('nro_end_orcamentos').setValue(resposta.dados[0].nro_end);
					Ext.getCmp('loteamento_orcamentos').setValue(resposta.dados[0].loteamento);
					Ext.getCmp('fone1_orcamentos').setValue(resposta.dados[0].fone1);
					Ext.getCmp('fone2_orcamentos').setValue(resposta.dados[0].fone2);
					Ext.getCmp('fone3_orcamentos').setValue(resposta.dados[0].fone3);
					Ext.getCmp('fone4_orcamentos').setValue(resposta.dados[0].fone4);
					Ext.getCmp('fone5_orcamentos').setValue(resposta.dados[0].fone5);
					Ext.getCmp('fone1_obs_orcamentos').setValue(resposta.dados[0].fone1_obs);
					Ext.getCmp('fone2_obs_orcamentos').setValue(resposta.dados[0].fone2_obs);
					Ext.getCmp('fone3_obs_orcamentos').setValue(resposta.dados[0].fone3_obs);
					Ext.getCmp('fone4_obs_orcamentos').setValue(resposta.dados[0].fone4_obs);
					Ext.getCmp('fone5_obs_orcamentos').setValue(resposta.dados[0].fone5_obs);

					var a = setInterval(function(){
						Ext.getCmp('cidade_orcamentos').setValue(resposta.dados[0].cidade);
						me.comboEndereco(resposta.dados[0].cidade, resposta.dados[0].bairro, resposta.dados[0].endereco);
						clearInterval(a);
					}, 300);

					var b = setInterval(function(){
						me.verificaAnotacoesCliente(id_cliente);
						Ext.getCmp('endereco_orcamentos').setValue(parseInt(resposta.dados[0].endereco));
						me.cidade = null;
						clearInterval(b);
					}, 400);
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}

		var comboCidade = Ext.getCmp('cidade_orcamentos');

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}

		Ext.getCmp('endereco_orcamentos').store.proxy.extraParams.localidade_id = combo.getValue();
		me.bairro = true;
		if(Ext.getCmp('endereco_orcamentos').isDisabled()){
			Ext.getCmp('endereco_orcamentos').setDisabled(false);
		}
		return true;

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_orcamentos');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_orcamentos');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_orcamentos');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			Ext.getCmp('bairro_nome_orcamentos').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_orcamentos').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_orcamentos').setValue(record.get('loteamento'));
			}
			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_orcamentos').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_orcamentos').setValue(record.get('cep'));
			Ext.getCmp('nro_end_orcamentos').focus(false, 100);
		}
	},

	allProdutos: function(){
		var me = this;
		me.getStore('StoreComboProdutos').load({
			callback: function(){
				window.StoreComboProdutosOrcamentos = me.getStore('StoreComboProdutos');
			}
		});
	},

	checkDelete: function(id_orcamento){
		var me = this;
		me.deleteAjax(Ext.getCmp('GridOrcamentos'), 'orcamentos', {
			action: 'VERIFICAR',
			id: id_orcamento
		}, false, false);
	},

	edit: function(grid, record) {
		var me = this;

		//Tipo de Orçamento: 0=geral/tradicional, 1=forro, 2=persianas, 3=pisos, 4=rodapés
		var vencido = record.get('validade_orcamento') < new Date();

		if(vencido){
			Ext.MessageBox.confirm('Atenção', 'O Orçamento está vencido deseja atualizar os valores?', function(btn){
				if(btn !== 'yes'){
					vencido = false;
				}
				me.editOrcamento(grid, record, vencido);
			});
		}
		else{
			me.editOrcamento(grid, record, false);
		}
	},

	editOrcamento: function(grid, record, vencido) {
		var me = this;
		console.log('Editando Orçamento ID: '+record.get('id_orcamento'))
		if(record.get('tipo_orcamento')==0){
			//GERAL
			me.allProdutos();
			me.delete_orcamento = false;
			me.orcamentoVencido = vencido;

			me.getDesktopWindow('AddOrcamentosWin', 'Orcamentos', 'orcamentos.Edit', function(){
				//produtos
				var grid = Ext.getCmp('GridItens_Orcamento');
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();

				var form = Ext.getCmp('FormOrcamentos');
				var win  = Ext.getCmp('AddOrcamentosWin');

				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos/list.php', function(dados){
					Ext.getCmp('action_orcamentos').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos').setValue('N');
					Ext.getCmp('validade_orcamento_orcamentos').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos').setValue(record.get('aos_cuidados_de'));
					
					
					if(record.get('convertido_venda')=="S"){
						control.getController('Orcamentos').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('convertido_venda_orcamentos').setReadOnly(true);
					}

					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos').setValue(record.get('cliente_descricao'));
						Ext.getCmp('cliente_descricao_orcamentos').focus(false, 1500);
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos').setValue(true);
						Ext.getCmp('id_cliente_orcamentos').setValue(''+record.get('id_cliente')+'');
						//Ext.getCmp('id_cliente_orcamentos').focus(false, 1500);
					}

					//enderecos inicio
					Ext.getCmp('FormOrcamentos').el.mask('Aguarde...');
					var me = control.getController('Orcamentos');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentos')){
								Ext.getCmp('FormOrcamentos').el.unmask();
							}
							
							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);
					//fim enderecos....

					var count = Ext.getCmp('GridItens_Orcamento').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos').setValue(count);

					if(vencido){
						Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos').setValue(d);
					}
				});
			});
		}
		else if(record.get('tipo_orcamento')==1){
			//Forro
			control.getController('OrcamentosForro').allProdutos();
			//verificar se a linha acima, realmente precisa dela... demora muito pra carregar quando remoto

			control.getController('OrcamentosForro').delete_orcamento = false;
			control.getController('OrcamentosForro').orcamentoVencido = vencido;
			//window.StoreComboProdutosOrcamentosForro = control.getController('OrcamentosForro').getStore('StoreComboProdutos').load();

			me.getDesktopWindow('AddOrcamentosForroWin', 'OrcamentosForro', 'orcamentos_forro.Edit', function(){
				//produtos
				var grid = me.getController('OrcamentosForro').getGridProdutos();
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos_forro').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();
				
				var form = Ext.getCmp('FormOrcamentosForro');
				var win  = Ext.getCmp('AddOrcamentosForroWin');
				
				me.getController('OrcamentosForro').areaTotalAmbiente = null;
				
				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos_forro/list.php', function(dados){
					Ext.getCmp('action_orcamentos_forro').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos_forro').setValue('N');
					Ext.getCmp('validade_orcamento_orcamentos_forro').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos_forro').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos_forro').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos_forro').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos_forro').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos_forro').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos_forro').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue(record.get('aos_cuidados_de'));
					

					if(record.get('convertido_venda')=="S"){
						control.getController('OrcamentosForro').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('convertido_venda_orcamentos_forro').setReadOnly(true);
					}
					
					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos_forro').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos_forro').setValue(record.get('cliente_descricao'));
						Ext.getCmp('cliente_descricao_orcamentos_forro').focus(false, 1500);
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos_forro').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_forro').setValue(''+record.get('id_cliente')+'');
						Ext.getCmp('id_cliente_orcamentos_forro').focus(false, 1500);
					}

					//enderecos inicio
					Ext.getCmp('FormOrcamentosForro').el.mask('Aguarde...');
					var me = control.getController('OrcamentosForro');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos_forro').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos_forro').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentosForro')){
								Ext.getCmp('FormOrcamentosForro').el.unmask();
							}
							
							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);
					//fim enderecos....

					var count = Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_forro').setValue(count);

					if(vencido){
						Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos_forro').setValue(d);
					}
				});
			});
		}
		else if(record.get('tipo_orcamento')==2){
			//PERSIANA
			console.log('falta fazer....');
		}
		else if(record.get('tipo_orcamento')==3){
			//Piso
			control.getController('OrcamentosPiso').allProdutos();

			control.getController('OrcamentosPiso').delete_orcamento = false;
			control.getController('OrcamentosPiso').orcamentoVencido = vencido;
			//window.StoreComboProdutosOrcamentosPiso = control.getController('OrcamentosPiso').getStore('StoreComboProdutos').load();

			me.getDesktopWindow('AddOrcamentosPisoWin', 'OrcamentosPiso', 'orcamentos_piso.Edit', function(){
				//produtos
				var grid = me.getController('OrcamentosPiso').getGridProdutos();
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos_piso').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();

				var form = Ext.getCmp('FormOrcamentosPiso');
				var win  = Ext.getCmp('AddOrcamentosPisoWin');
				me.getController('OrcamentosPiso').areaTotalAmbiente = null;

				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos_piso/list.php', function(dados){
					Ext.getCmp('action_orcamentos_piso').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos_piso').setValue('N');
					Ext.getCmp('validade_orcamento_orcamentos_piso').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos_piso').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos_piso').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos_piso').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos_piso').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos_piso').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos_piso').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos_piso').setValue(record.get('aos_cuidados_de'));

					if(record.get('convertido_venda')=="S"){
						control.getController('OrcamentosPiso').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('convertido_venda_orcamentos_piso').setReadOnly(true);
					}

					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos_piso').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos_piso').setValue(record.get('cliente_descricao'));
						Ext.getCmp('cliente_descricao_orcamentos_piso').focus(false, 1500);
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos_piso').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_piso').setValue(record.get('id_cliente').toString());
						Ext.getCmp('id_cliente_orcamentos_piso').focus(false, 1500);
					}

					//endereco
					Ext.getCmp('FormOrcamentosPiso').el.mask('Aguarde...');
					var me = control.getController('OrcamentosPiso');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos_piso').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos_piso').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentosPiso')){
								Ext.getCmp('FormOrcamentosPiso').el.unmask();
							}
							
							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);

					var count = Ext.getCmp('GridProdutos_Orcamentos_Piso').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_piso').setValue(count);

					if(vencido){
						Ext.getCmp('button_resetar_orcamentos_piso').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_piso').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos_piso').setValue(d);
					}
				});
			});
		}
		else if(record.get('tipo_orcamento')==4){
			//Rodape
			control.getController('OrcamentosRodape').allProdutos();
			control.getController('OrcamentosRodape').delete_orcamento = false;
			control.getController('OrcamentosRodape').orcamentoVencido = vencido;
			//window.StoreComboProdutosOrcamentosRodape = control.getController('OrcamentosRodape').getStore('StoreComboProdutos').load();

			me.getDesktopWindow('AddOrcamentosRodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.Edit', function(){
				//produtos
				var grid = me.getController('OrcamentosRodape').getGridProdutos();
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos_rodape').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();

				var form = Ext.getCmp('FormOrcamentosRodape');
				var win  = Ext.getCmp('AddOrcamentosRodapeWin');
				
				me.getController('OrcamentosRodape').areaTotalAmbiente = null;
				
				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos_rodape/list.php', function(dados){
					Ext.getCmp('action_orcamentos_rodape').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos_rodape').setValue('N');
					Ext.getCmp('validade_orcamento_orcamentos_rodape').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos_rodape').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos_rodape').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos_rodape').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos_rodape').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos_rodape').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos_rodape').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue(record.get('aos_cuidados_de'));

					if(record.get('convertido_venda')=="S"){
						control.getController('OrcamentosRodape').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('convertido_venda_orcamentos_rodape').setReadOnly(true);
					}
					
					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos_rodape').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setValue(record.get('cliente_descricao'));
						Ext.getCmp('cliente_descricao_orcamentos_rodape').focus(false, 1500);
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos_rodape').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_rodape').setValue(''+record.get('id_cliente')+'');
						Ext.getCmp('id_cliente_orcamentos_rodape').focus(false, 1500);
					}

					Ext.getCmp('FormOrcamentosRodape').el.mask('Aguarde...');
					var me = control.getController('OrcamentosRodape');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos_rodape').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos_rodape').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentosRodape')){
								Ext.getCmp('FormOrcamentosRodape').el.unmask();
							}

							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);


					var count = Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_rodape').setValue(count);
					
					if(vencido){
						Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos_rodape').setValue(d);
					}
				});
			});
		}
		else{
			console.info('Tipo não encontrado!');
		}
	},

	dateFormat: function (data, db){
		var dia = data.getDate();
		var mes = data.getMonth()+1;
		var ano = data.getFullYear();

		if (dia.toString().length == 1)
			dia = "0"+dia;

		if (mes.toString().length == 1)
			mes = "0"+mes;

		if(db==true){
			return ano+'-'+mes+'-'+dia;
		}  
		else{
			return dia+"/"+mes+"/"+ano;
		}
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'orcamentos', {
			action: 'DELETAR_ORCAMENTO',
			id: record.get('id_orcamento')
		}, button, false);
	},

	GeraCopia: function(grid, record, button, venda) {
		var me = this;
		me.acoesAjax(grid, 'orcamentos', {
			action: 'COPIA_ORCAMENTO',
			tipo_orcamento: record.get('tipo_orcamento'),
			id_orcamento: record.get('id_orcamento'),
			convertido_venda: venda
		}, button, function(){
			grid.store.load({
				callback: function(){
					var grid1 = Ext.getCmp('GridVendas');

					setTimeout(function() {
						if(grid1 && grid1.getEl()){
							grid1.store.load();
						}
					}, 500);
				}
			})
		});
	},

	btPDF: function(button) {
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/pdf.php?id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btPrintCliente: function(button) {
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/impressao.php?tipo=cliente&id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, 'Selecione um Orçamento para Imprimir');
			return true;
		}
	},

	btPrintDeposito: function(button) {
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/impressao.php?tipo=deposito&id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, 'Selecione um Orçamento para Imprimir');
			return true;
		}
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Cancelar o orçamento?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	btcopia: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Gerar uma Cópia do orçamento: '+record.get('id_orcamento')+'?', function(btn){
				if(btn=='yes'){
					me.GeraCopia(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar um Orçamento para poder Copiar');
			return true;
		}
	},

	btConverteVenda: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Converter o Orçamento <b>'+record.get('id_orcamento')+'</b> em Venda?', function(btn){
				if(btn=='yes'){
					me.GeraCopia(me.getList(), record, button, 'S');
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar uma Venda para Converter');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	addGeral: function(button) {
		var me = this;
		me.delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosWin', 'Orcamentos', 'orcamentos.Edit', function(){
			var form = Ext.getCmp('FormOrcamentos');
			Ext.getCmp('action_orcamentos').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos').setValue('N');
			Ext.getCmp('GridItens_Orcamento').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos').setValue(userID);
			Ext.getCmp('id_os_orcamentos').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);

			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'success');
						Ext.getCmp('AddOrcamentosWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridItens_Orcamento').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 0;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos').setValue(params.cliente);
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddOrcamentosWin').close();
				}
			});
		});
	},

	addForro: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentosForro = control.getController('OrcamentosForro').getStore('StoreComboProdutos').load();
		control.getController('OrcamentosForro').delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosForroWin', 'OrcamentosForro', 'orcamentos_forro.Edit', function(){
			var form = Ext.getCmp('FormOrcamentosForro');
			Ext.getCmp('action_orcamentos_forro').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos_forro').setValue('N');
			Ext.getCmp('GridProdutos_Orcamentos_Forro').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos_forro').setValue(userID);
			Ext.getCmp('id_os_orcamentos_forro').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos_forro').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos_forro').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos_forro').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
			
			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos_forro/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, response.responseText, 'success');
						Ext.getCmp('AddProdutos_Orcamentos_ForroWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos_forro').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridProdutos_Orcamentos_Forro').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 1;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_forro').setValue(params.cliente);
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddProdutos_Orcamentos_ForroWin').close();
				}
			});
		});
	},

	addRodape: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentosRodape = control.getController('OrcamentosRodape').getStore('StoreComboProdutos').load();
		control.getController('OrcamentosRodape').delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosRodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.Edit', function(){
			var form = Ext.getCmp('FormOrcamentosRodape');
			Ext.getCmp('action_orcamentos_rodape').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos_rodape').setValue('N');
			Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos_rodape').setValue(userID);
			Ext.getCmp('id_os_orcamentos_rodape').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos_rodape').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos_rodape').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos_rodape').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
			
			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos_rodape/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'success');
						Ext.getCmp('AddProdutos_Orcamentos_RodapeWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos_rodape').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 4;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_rodape').setValue(params.cliente);                            
						}
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_forro').setValue(params.cliente);                            
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddProdutos_Orcamentos_RodapeWin').close();
				}
			});
		});
	},

	addPiso: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentosPiso = control.getController('OrcamentosPiso').getStore('StoreComboProdutos').load();
		control.getController('OrcamentosPiso').delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosPisoWin', 'OrcamentosPiso', 'orcamentos_piso.Edit', function(){
			var form = Ext.getCmp('FormOrcamentosPiso');
			Ext.getCmp('action_orcamentos_piso').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos_piso').setValue('N');
			Ext.getCmp('GridProdutos_Orcamentos_Piso').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos_piso').setValue(userID);
			Ext.getCmp('id_os_orcamentos_piso').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos_piso').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos_piso').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos_piso').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);

			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos_piso/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'success');
						Ext.getCmp('AddProdutos_Orcamentos_PisoWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos_piso').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridProdutos_Orcamentos_Piso').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 3;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_piso').setValue(params.cliente);                            
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, response.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddProdutos_Orcamentos_PisoWin').close();
				}
			});
		});
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.orcamentos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	enableCliente: function(checkbox, newValue){
		var me = this;

		me.resetCombo(Ext.getCmp('button_id_cliente_orcamentos'));
		Ext.getCmp('cliente_descricao_orcamentos').reset();
		
		if(newValue==false){
			Ext.getCmp('cliente_descricao_orcamentos').setVisible(true);
			Ext.getCmp('cliente_descricao_orcamentos').setDisabled(false);
			Ext.getCmp('cliente_descricao_orcamentos').setValue('CONSUMIDOR');
			Ext.getCmp('fieldcontainer_id_cliente_orcamentos').setVisible(false);
			Ext.getCmp('id_cliente_orcamentos').setDisabled(true);
		}
		else{
			Ext.getCmp('cliente_descricao_orcamentos').setVisible(false);
			Ext.getCmp('cliente_descricao_orcamentos').setDisabled(true);
			Ext.getCmp('fieldcontainer_id_cliente_orcamentos').setVisible(true);
			Ext.getCmp('id_cliente_orcamentos').setDisabled(false);
		}
	},

	orcamentoVencido: false,
	
	changeCliente: function(id_categoria_precos, id_cliente){
		var me              = this;
		var grid            = Ext.getCmp('GridItens_Orcamento');
		var items           = grid.store.data.items;
		var produtoStore    = window.StoreComboProdutosOrcamentos;
		var total           = 0;

		if(!produtoStore){
			return true;
		}

		for(var i=0;i<items.length;i++){
			var recordProduto = produtoStore.getById(''+items[i].get('id_produto')+'');
			
			if(recordProduto){
				if(me.id_categoria_precos_old!=id_categoria_precos){
					var valor_unit = me.getValorRecord(recordProduto, id_categoria_precos);
				}
				else{
					var valor_unit = items[i].get('valor_unitario');
				}

				if(me.orcamentoVencido){
					var valor_unit = me.getValorRecord(recordProduto, id_categoria_precos);
				}

				//var valor_total = valor_unit * items[i].get('quantidade');
				var valor_total = (valor_unit * items[i].get('quantidade')).toFixed(2);

				items[i].set('valor_unitario', valor_unit);
				items[i].set('valor_total', valor_total);

				total += parseFloat(valor_total);
			}
		}

		setTimeout(function(){
			me.setValorTotal(total);
		}, 300);
	},

	getValorRecord: function(recordProduto, id_categoria_precos){
		var preco   = recordProduto.raw.preco_venda || recordProduto.raw.preco_custo;
		var precos = recordProduto.raw.produto_precos;

		if(precos!=undefined){
			for(var i=0;i<precos.length;i++){
				if(precos[i].id_categoria_precos==id_categoria_precos){
					preco = precos[i].valor;
				}
			}
		}

		if(preco==undefined || preco==null){
			info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria');
		}
		
		return preco;
	},

	setValorTotal: function(total){
		var me = this;
		var grid = Ext.getCmp('GridItens_Orcamento');
		var items = grid.store.data.items;
		var desconto = Ext.getCmp('desconto_real_orcamentos').getValue();
		var acrescimo = Ext.getCmp('acrescimo_real_orcamentos').getValue();
		if(typeof total != 'number'){
			total = null;
		}
		if(total==undefined || total==null){
			total = 0;
			
			for(var i=0;i<items.length;i++){
				total += items[i].get('valor_total');
			}
		}

		var subTotal = total;
		subTotal = Math.round((subTotal).toFixed(10) * 100)/100;
		Ext.getCmp('valor_bruto_orcamentos').setValue(subTotal);

		if(acrescimo!=""){
			total += parseFloat(acrescimo);
		}

		if(desconto!=""){
			total -= parseFloat(desconto);
		}

		total = Math.round((total).toFixed(10) * 100)/100;
		Ext.getCmp('valor_total_orcamentos').setValue(total);
	}, 

	changeGridValores: function(comboCliente, value){
		var me = this;
		me.getCategoriaPrecos(comboCliente, value);
		me.changeCliente(me.id_categoria_precos, value);
	},

	getValorUnitario: function(produto, id_produto){
		var me              = this;
		var produtoStore    = produto.store;
		var recordProduto   = produtoStore.getById(id_produto);

		if(recordProduto){
			return me.getValorRecord(recordProduto, me.id_categoria_precos);
		}
		else{
			return 0;
		}
	},

	getCategoriaPrecos: function(comboCliente, value){
		var me = this;
		
		me.id_categoria_precos_old = me.id_categoria_precos;
		if(Ext.getCmp('cliente_cadastrado_orcamentos').getValue()==true){
			var recordCliente = comboCliente.store.getById(comboCliente.getValue());

			if(recordCliente && recordCliente.raw.id_categoria_precos!=null){
				me.id_categoria_precos = recordCliente.raw.id_categoria_precos;
			}
		}
		return me.id_categoria_precos;
	},

	changeQuantidadeAndPrecoProdutos: function(){
		var me = this;
		me.changeCliente(me.id_categoria_precos);
	},

	changeTotal: function(field, value){
		var me = this;
		var valor_bruto = Ext.getCmp('valor_bruto_orcamentos').getValue();
		//var desconto = Ext.getCmp('desconto_real_orcamentos').getValue();
		//var acrescimo = Ext.getCmp('acrescimo_real_orcamentos').getValue();
		var desconto = 0;
		var acrescimo = 0;

		if(valor_bruto!=undefined && valor_bruto!=null && valor_bruto!=""){
			if(desconto==""){desconto = 0;}
			if(acrescimo==""){acrescimo = 0;}
			valor_bruto = parseFloat(valor_bruto);
			desconto 	= parseFloat(desconto);
			acrescimo	= parseFloat(acrescimo);
			
			if(field.getValue()==""){
				var valor_total = 0;
			}

			var valor_total = parseFloat(field.getValue());

			var subtotal = valor_bruto + acrescimo;
			subtotal 	-= desconto;

			if(subtotal>valor_total){
				/*var desconto2 = subtotal - valor_total;
				desconto += desconto2;
				*/
				desconto = parseFloat(subtotal - valor_total);
				desconto = Math.round((desconto).toFixed(10) * 100)/100;
				Ext.getCmp('acrescimo_real_orcamentos').setValue(0);
				Ext.getCmp('acrescimo_porcento_orcamentos').setValue(0);
				Ext.getCmp('desconto_real_orcamentos').setValue(desconto);
				me.changeDesconto(Ext.getCmp('desconto_real_orcamentos'), desconto);
			}
			else{
				/*var acrescimo2 = valor_total - subtotal;
				acrescimo += acrescimo2;
				*/

				acrescimo = parseFloat(valor_total - subtotal);
				acrescimo = Math.round((acrescimo).toFixed(10) * 100)/100;
				Ext.getCmp('desconto_real_orcamentos').setValue(0);
				Ext.getCmp('desconto_porcento_orcamentos').setValue(0);
				Ext.getCmp('acrescimo_real_orcamentos').setValue(acrescimo);
				me.changeAcrescimo(Ext.getCmp('acrescimo_real_orcamentos'), acrescimo);
			}
		}
	},

	changeAcrescimo: function(field, value){
		var me = this;
		var total = Ext.getCmp('valor_bruto_orcamentos').getValue();
		if(total!=undefined && total!=null && total!=""){
			total = parseFloat(total);
			if(total>0){
				
				if(field.name=='acrescimo_real'){
					var porcento = ((parseFloat(field.getValue()) * 100) / total);
					Ext.getCmp('acrescimo_porcento_orcamentos').setValue(porcento);
				}
				else{
					var real = parseFloat( (total * field.getValue())/100 );
					Ext.getCmp('acrescimo_real_orcamentos').setValue(real);
				} 

				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
			}
		}
	},

	changeDesconto: function(field, value){
		var me = this;
		var total = Ext.getCmp('valor_bruto_orcamentos').getValue();
		if(total!=undefined && total!=null && total!=""){
			total = parseFloat(total);
			if(total>0){
				if(field.name=='desconto_real'){
					var porcento = ((parseFloat(field.getValue()) * 100) / total);
					Ext.getCmp('desconto_porcento_orcamentos').setValue(porcento);
				}
				else{
					var real = parseFloat( (total * field.getValue())/100 );
					Ext.getCmp('desconto_real_orcamentos').setValue(real);
				} 

				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
			}
		}
	},

	update: function(button) {
		var me         = this;
		//var grid       = Ext.getCmp('GridOrcamentos');
		var grid       = Ext.getCmp('GridItens_Orcamento');
		var win        = Ext.getCmp('AddOrcamentosWin');
		var form       = Ext.getCmp('FormOrcamentos');

		var records         = grid.store.getModifiedRecords();
		var json_produtos  = [];
		for(var i in records){
			json_produtos.push(Ext.encode(records[i].data));
		}

		me.formParams = {
			items: [json_produtos]
		};
		
		me.saveForm(grid, form, win, button, function(){

		}, false);
	},

	RegistroSelecionado: function(){
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			Ext.getCmp('button_imprimir_orcamentos').setDisabled(false);
			Ext.getCmp('button_copiar_orcamentos').setDisabled(false);

			if(record.get('convertido_venda') == "S"){
				//parei aqui... verificar todos os convertido_venda
				//se pode editar/deletar... inclusive vendas
				Ext.getCmp('button_edit_orcamentos').setDisabled(true);
				Ext.getCmp('button_del_orcamentos').setDisabled(true);
				Ext.getCmp('button_converte1_orcamentos').setDisabled(true);
			}
			else{
				Ext.getCmp('button_converte1_orcamentos').setDisabled(false);
				Ext.getCmp('button_edit_orcamentos').setDisabled(false);
				Ext.getCmp('button_del_orcamentos').setDisabled(false);
			}
			
			if(record.get('saldo_a_receber') > 0) {
				Ext.getCmp('button_pagamento_orcamentos').setDisabled(false);
			}
			else {
				Ext.getCmp('button_pagamento_orcamentos').setDisabled(true);
			}
		}
	}
});