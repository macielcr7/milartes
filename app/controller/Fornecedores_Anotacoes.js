/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Fornecedores_Anotacoes', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Fornecedores_Anotacoes',
	started: true,
	
	refs: [
		{
			ref: 'list',
			selector: 'fornecedores_anotacoeslist'
		},
		{
			ref: 'form',
			selector: 'addfornecedores_anotacoeswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'fornecedores_anotacoeslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterfornecedores_anotacoeswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterfornecedores_anotacoeswin form'
		},
		{
			ref: 'addWin',
			selector: 'addfornecedores_anotacoeswin'
		}
	],
	
	models: [
		'ModelFornecedores_Anotacoes',
		'ModelComboLocal'
	],
	stores: [
		'StoreFornecedores_Anotacoes',
		'StoreComboAtivoFornecedoresAnotacoes'
	],
	
	views: [
		'fornecedores_anotacoes.List',
		'fornecedores_anotacoes.Filtro',
		'fornecedores_anotacoes.Edit'
	],

	init: function(application) {
		this.control({
			'fornecedores_anotacoeslist': {				 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'fornecedores_anotacoeslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'fornecedores_anotacoeslist button[action=adicionar]': {
				click: this.add
			},
			'fornecedores_anotacoeslist button[action=editar]': {
				click: this.btedit
			},
			'fornecedores_anotacoeslist button[action=deletar]': {
				click: this.btdel
			},			
			'fornecedores_anotacoeslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addfornecedores_anotacoeswin button[action=salvar]': {
				click: this.update
			},
			'addfornecedores_anotacoeswin button[action=resetar]': {
				click: this.reset
			},
			'addfornecedores_anotacoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addfornecedores_anotacoeswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addfornecedores_anotacoeswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterfornecedores_anotacoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterfornecedores_anotacoeswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterfornecedores_anotacoeswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterfornecedores_anotacoeswin': {
				show: this.filterSetFields
			}
		});
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/fornecedores_anotacoes/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddFornecedores_AnotacoesWin', 'Fornecedores_Anotacoes', 'fornecedores_anotacoes.Edit', function(){
			me.getAddWin().setTitle('Alteração de Anotação');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/fornecedores_anotacoes/list.php');
			Ext.getCmp('action_fornecedores_anotacoes').setValue('EDITAR');
			Ext.getCmp('anotacao_fornecedores_anotacoes').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'fornecedores_anotacoes', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar a Anotação: '+record.get('controle')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Fornecedores_Anotacoes', 'Fornecedores_Anotacoes', 'fornecedores_anotacoes.Edit', function(){
			Ext.getCmp('cod_fornecedor_fornecedores_anotacoes').setValue(me.getList().store.proxy.extraParams.cod_fornecedor);
			Ext.getCmp('anotacao_fornecedores_anotacoes').focus(false, 1000);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('fornecedores_anotacoes', 'editar')==true){
			Ext.getCmp('button_edit_fornecedores_anotacoes').setDisabled(false);
		}

		if(this.existPermissao('fornecedores_anotacoes', 'deletar')==true){
			Ext.getCmp('button_del_fornecedores_anotacoes').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.fornecedores_anotacoes.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});