/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.controller.Cfop', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Cfop',

	refs: [
		{
			ref: 'list',
			selector: 'cfoplist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcfopwin form'
		},
		{
			ref: 'filterBtn',
			selector: 'cfoplist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercfopwin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercfopwin form'
		},
		{
			ref: 'addWin',
			selector: 'addcfopwin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelCfop'
	],
	stores: [
		'StoreCfop',
		'StoreComboTipoCfop',
		'StoreComboOperacaoCfop'
	],
	
	views: [
		'cfop.List',
		'cfop.Edit'
	],

	init: function(application) {
		this.control({
			'cfoplist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'cfoplist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'cfoplist button[action=adicionar]': {
				click: this.add
			},
			'cfoplist button[action=editar]': {
				click: this.btedit
			},
			'cfoplist button[action=deletar]': {
				click: this.btdel
			},
			'addcfopwin button[action=salvar]': {
				click: this.update
			},
			'addcfopwin button[action=resetar]': {
				click: this.reset
			},
			'addcfopwin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercfopwin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercfopwin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercfopwin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercfopwin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddCfopWin', 'Cfop', 'cfop.Edit', function(){
			me.getAddWin().setTitle('Alterar CFOP');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_fiscal'), 'server/modulos/cfop/list.php');
			Ext.getCmp('action_cfop').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'cfop', {
			action: 'DELETAR',
			id: record.get('cod_fiscal')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar o CFOP: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Cfop', 'Cfop', 'cfop.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('cfop', 'editar')==true){
			Ext.getCmp('button_edit_cfop').setDisabled(false);
		}

		if(this.existPermissao('cfop', 'deletar')==true){
			Ext.getCmp('button_del_cfop').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_cfop').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	}
});