/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Correios_Localidades', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Correios_Localidades',
	
	refs: [
		{
			ref: 'list',
			selector: 'correios_localidadeslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcorreios_localidadeswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'correios_localidadeslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercorreios_localidadeswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercorreios_localidadeswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcorreios_localidadeswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelCombo',
		'ModelCorreios_Localidades'
	],
	stores: [
		'StoreComboCorreios_Estados',
		'StoreComboSituacaoLocalidadeCorreiosLocalidades',
		'StoreComboLocTipoCorreiosLocalidades',
		'StoreComboCorreios_Localidades',
		'StoreCorreios_Localidades',
		'StoreComboAtivo'
	],
	
	views: [
		'correios_localidades.List',
		'correios_localidades.Filtro',
		'correios_localidades.Edit'
	],

	init: function(application) {
		this.control({
			'correios_localidadeslist gridpanel': {				 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'correios_localidadeslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'correios_localidadeslist button[action=adicionar]': {
				click: this.add
			},
			'correios_localidadeslist button[action=list_bairros]': {
				click: this.listBairros
			},
			'correios_localidadeslist button[action=editar]': {
				click: this.btedit
			},
			'correios_localidadeslist button[action=deletar]': {
				click: this.btdel
			},
			'addcorreios_localidadeswin button[action=salvar]': {
				click: this.update
			},
			'addcorreios_localidadeswin button[action=resetar]': {
				click: this.reset
			},
			'addcorreios_localidadeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcorreios_localidadeswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcorreios_localidadeswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercorreios_localidadeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercorreios_localidadeswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercorreios_localidadeswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercorreios_localidadeswin': {
				show: this.filterSetFields
			}
		});
	},

	listBairros: function(button){
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.getDesktopWindow('List-Correios_Bairros', 'Correios_Bairros', 'correios_bairros.List', function(controller){
				controller.getList().store.proxy.extraParams.uf_sigla = record.get('uf_sigla');
				controller.getList().store.proxy.extraParams.localidade_id = record.get('id');
			});
		}
		else{
			info(this.titleErro, this.detalharErroGrid);
			return true;
		}
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddCorreios_LocalidadesWin', 'Correios_Localidades', 'correios_localidades.Edit', function(){
			me.getAddWin().setTitle('Alteração de Cidades');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/correios_localidades/list.php');
			Ext.getCmp('action_correios_localidades').setValue('EDITAR');
			Ext.getCmp('loc_nome_abreviado_correios_localidades').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'correios_localidades', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Desativar esta Cidade: '+record.get('loc_nome')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Correios_Localidades', 'Correios_Localidades', 'correios_localidades.Edit', function(){

			var frm = button.up('form');
			if(frm!=null && frm.down('combobox[id=estado_clientes]')){
				var v = Ext.getCmp('estado_clientes').getValue();
				if(v!="" && v!=null){
					Ext.getCmp('uf_sigla_correios_localidades').setValue(v);
				}
			}
			else if(frm!=null && frm.down('combobox[id=uf_sigla_correios_enderecos]')){
				var v = Ext.getCmp('uf_sigla_correios_enderecos').getValue();
				if(v!="" && v!=null){
					Ext.getCmp('uf_sigla_correios_localidades').setValue(v);
				}
			}
			else {
				if(me.getList()){
					var uf_sigla = me.getList().store.proxy.extraParams.uf_sigla;
					Ext.getCmp('uf_sigla_correios_localidades').setValue(uf_sigla);
				}
			}
		});
		Ext.getCmp('loc_nome_abreviado_correios_localidades').focus(false, 1000);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('correios_localidades', 'editar')==true){
			Ext.getCmp('button_edit_correios_localidades').setDisabled(false);
		}

		if(this.existPermissao('correios_localidades', 'deletar')==true){
			Ext.getCmp('button_del_correios_localidades').setDisabled(false);
		}

		if(this.existPermissao('correios_localidades', 'list_bairros')==true){
			Ext.getCmp('button_add_bairros_correios_localidades').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_correios_localidades').getValue();
		this.getList().store.load({
			start: 0,
			limit: 30
		});
	}
});