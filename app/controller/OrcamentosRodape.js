/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.OrcamentosRodape', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},
	tabela: 'OrcamentosRodape',
	started: false,
	dependeciesControllers: [
		'Orcamentos_Observacoes'
	],
	refs: [
		{
			ref: 'gridAmbientes',
			selector: 'addprodutos_orcamentos_rodapewin gridambientes_orcamentos_rodapelist'
		},
		{
			ref: 'gridProdutos',
			selector: 'addorcamentos_rodapewin gridprodutos_orcamentos_rodapelist'
		},
		{
			ref: 'formProdutos',
			selector: 'addprodutos_orcamentos_rodapewin form'
		},
		{
			ref: 'addProdutosWin',
			selector: 'addprodutos_orcamentos_rodapewin'
		},
		{
			ref: 'formAmbientes',
			selector: 'addambientes_orcamentos_rodapewin form'
		},
		{
			ref: 'addAmbientesWin',
			selector: 'addambientes_orcamentos_rodapewin'
		},
		{
			ref: 'form',
			selector: 'addorcamentos_rodapewin form'
		},
		{
			ref: 'addWin',
			selector: 'addorcamentos_rodapewin'
		}
	],

	models: [
		'ModelCombo',
		'ModelComboLocal',
		'ModelAmbientes_Orcamentos_Rodape',
		'ModelProdutos_Orcamentos_Rodape'
	],

	stores: [
		'StoreComboClientes',
		'StoreComboOrcamentista',
		'StoreComboProdutosRodape',
		'StoreAmbientes_Orcamentos_Rodape',
		'StoreProdutos_Orcamentos_Rodape',
		'StoreComboProdutosCores',
		'StoreAmbientesNomes',
		'StoreComboOrcamentosVendas'
	],	

	views: [
		'orcamentos_rodape.GridAmbientes',
		'orcamentos_rodape.GridProdutos',
		'orcamentos_rodape.EditAmbientes',
		'orcamentos_rodape.EditProdutos',
		'orcamentos_rodape.EditGeral',
		'orcamentos_rodape.Edit',
	],

	produto_rodape: false,
	produto_cor: false,
	noDeleteLoad: true,
	areaTotalAmbiente: null,
	somaTotalAmbiente: 0,
	id_categoria_precos: 1,
	tipo_produto: 'P',
	record_produto: null,
	delete_orcamento: false,

	init: function(application) {
		this.control({
			////////////////////--PRODUTOS--////////////////////////
			'addorcamentos_rodapewin gridprodutos_orcamentos_rodapelist button[action=adicionar]':{
				click: this.addProduto
			},
			'addorcamentos_rodapewin gridprodutos_orcamentos_rodapelist button[action=editar]':{
				click: this.editProduto
			},
			'addorcamentos_rodapewin gridprodutos_orcamentos_rodapelist button[action=add_geral]':{
				click: this.addGeral
			},
			'addorcamentos_rodapewin gridprodutos_orcamentos_rodapelist button[action=deletar]':{
				click: this.deleteProduto
			},
			'addorcamentos_rodapewin gridprodutos_orcamentos_rodapelist button[action=copiar_item_orcamento]':{
				click: this.btCopiaItemOrcamento
			},
			'addprodutos_orcamentos_rodapewin form fieldcontainer button[action=add_ambientes]': {
				click: this.setStepAmbientes
			},
			'addprodutos_orcamentos_rodapewin form fieldcontainer combobox[id=id_produto_produtos_orcamentos_rodape]': {
				change: this.changeCores
			},
			'addprodutos_orcamentos_rodapewin numberfield[id=qtd_barras_orcamentos_rodape]': {
				change: this.changeTotalAreaProduto
			},
			'addprodutos_orcamentos_rodapewin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addprodutos_orcamentos_rodapewin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addprodutos_orcamentos_rodapewin button[action=salvar]': {
				click: this.updateProduto
			},

/*			'addprodutos_orcamentos_rodapewin': {
				beforeclose: this.beforeCloseProduto
			},
*/
			////////////////////--PRODUTOS--////////////////////////

			////////////////////--PRODUTOS - GERAL--////////////////////////
			'addageral_orcamentos_rodapewin combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addageral_orcamentos_rodapewin combobox[id=id_produto_geral_orcamentos_rodape]': {
				change: this.setValorUnitarioGeral
			},
			'addageral_orcamentos_rodapewin button[action=salvar]': {
				click: this.updateProdutoGeral
			},
			////////////////////--PRODUTOS - GERAL--////////////////////////


			////////////////////--AMBIENTES--////////////////////////
			'addprodutos_orcamentos_rodapewin gridambientes_orcamentos_rodapelist button[action=adicionar]':{
				click: this.addAmbiente
			},
			'addprodutos_orcamentos_rodapewin gridambientes_orcamentos_rodapelist button[action=editar]':{
				click: this.editAmbiente
			},
			'addprodutos_orcamentos_rodapewin gridambientes_orcamentos_rodapelist button[action=deletar]':{
				click: this.deleteAmbiente
			},
			'addambientes_orcamentos_rodapewin button[action=salvar]': {
				click: this.updateAmbiente
			},
			'addambientes_orcamentos_rodapewin form combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addambientes_orcamentos_rodapewin form button[action=reset_combo]': {
				click: this.resetCombo
			},
			////////////////////--AMBIENTES--////////////////////////

			'addorcamentos_rodapewin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addorcamentos_rodapewin form textfield[id=valor_total_orcamentos_rodape]': {
				blur: this.changeTotal
			},
			'addorcamentos_rodapewin form textfield[id=acrescimo_porcento_orcamentos_rodape], addorcamentos_rodapewin form textfield[id=acrescimo_real_orcamentos_rodape]': {
				keyup: this.changeAcrescimo
			},
			'addorcamentos_rodapewin form numberfield[id=desconto_porcento_orcamentos_rodape], addorcamentos_rodapewin form textfield[id=desconto_real_orcamentos_rodape]': {
				keyup: this.changeDesconto
			},
			'addorcamentos_rodapewin form combobox[id=id_cliente_orcamentos_rodape]': {
				change: this.changeGridValores
			},
			'addorcamentos_rodapewin checkbox[id=cliente_cadastrado_orcamentos_rodape]': {
				change: this.enableCliente
			},
			'addorcamentos_rodapewin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addorcamentos_rodapewin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addorcamentos_rodapewin button[action=salvar]': {
				click: this.update
			},
			'addorcamentos_rodapewin form fieldcontainer combobox[id=estado_orcamentos_rodape]': {
				change: this.loadCidade
			},
			'addorcamentos_rodapewin form fieldcontainer combobox[id=cidade_orcamentos_rodape]': {
				change: this.loadBairro
			},
			'addorcamentos_rodapewin form fieldcontainer combobox[id=endereco_orcamentos_rodape]': {
				change: this.setEndereco
			}/*,
			'addorcamentos_rodapewin form textfield[id=id_os_orcamentos_rodape]': {
				blur: this.loadDadosOS
			}*/
		});
	},

	cidade: false,
	endereco: false,

	loadDadosOS: function(field){
		var me = this;
		var v = field.getValue();
		Ext.Ajax.request({
			url : 'server/modulos/os/list.php',
			params : {
				action: 'GET_VALUES',
				id : v
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'O Número de OS digitado não foi Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);

					if (resposta.dados.cliente==null){
						Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue('');
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setVisible(true);
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setDisabled(false);
						Ext.getCmp('fieldcontainer_id_cliente_orcamentos_rodape').setVisible(false);
						Ext.getCmp('cliente_cadastrado_orcamentos_rodape').setValue(false);
						Ext.getCmp('id_cliente_orcamentos_rodape').setDisabled(true);
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setValue(resposta.dados.titulo);
					}
					else{
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setVisible(false);
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setDisabled(true);
						Ext.getCmp('fieldcontainer_id_cliente_orcamentos_rodape').setVisible(true);
						Ext.getCmp('cliente_cadastrado_orcamentos_rodape').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_rodape').setDisabled(false);
						Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue('');

						if (resposta.dados.titulo != resposta.dados.nome_cliente){
							Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue(resposta.dados.titulo);
						}

						Ext.getCmp('id_cliente_orcamentos_rodape').store.load({
							params: {
								id_cliente: resposta.dados.cliente.toString()
							}
						});

						var c = setInterval(function(){
							me.verificaAnotacoesCliente(resposta.dados.cliente);
							Ext.getCmp('id_cliente_orcamentos_rodape').setValue(resposta.dados.cliente.toString());
							clearInterval(c);
						}, 400);
					}

					var texto = "Deseja ATUALIZAR os dados de Endereco deste orçamento com os mesmos dados da OS?";
					Ext.Msg.confirm('Confirmar', texto, function(btn){
						if(btn=='yes'){
							me.loadEnderecoOS(v);
						}
					});
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadEnderecoOS: function(os_nro){
		var me = this;
		Ext.Ajax.request({
			url : 'server/modulos/os/list.php',
			params : {
				action: 'GET_VALUES',
				id : os_nro
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'O Número de OS digitado não foi Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('estado_orcamentos_rodape').setValue(resposta.dados.estado);
					Ext.getCmp('ponto_ref_orcamentos_rodape').setValue(resposta.dados.endereco_referencia);
					Ext.getCmp('complemento_orcamentos_rodape').setValue(resposta.dados.endereco_complemento);
					Ext.getCmp('nro_end_orcamentos_rodape').setValue(resposta.dados.endereco_num);
					Ext.getCmp('loteamento_orcamentos_rodape').setValue(resposta.dados.loteamento);
					Ext.getCmp('fone1_orcamentos_rodape').setValue(resposta.dados.fone1);
					Ext.getCmp('fone2_orcamentos_rodape').setValue(resposta.dados.fone2);
					Ext.getCmp('fone3_orcamentos_rodape').setValue(resposta.dados.fone3);
					Ext.getCmp('fone4_orcamentos_rodape').setValue(resposta.dados.fone4);
					Ext.getCmp('fone5_orcamentos_rodape').setValue(resposta.dados.fone5);
					Ext.getCmp('fone1_obs_orcamentos_rodape').setValue(resposta.dados.fone1_obs);
					Ext.getCmp('fone2_obs_orcamentos_rodape').setValue(resposta.dados.fone2_obs);
					Ext.getCmp('fone3_obs_orcamentos_rodape').setValue(resposta.dados.fone3_obs);
					Ext.getCmp('fone4_obs_orcamentos_rodape').setValue(resposta.dados.fone4_obs);
					Ext.getCmp('fone5_obs_orcamentos_rodape').setValue(resposta.dados.fone5_obs);

					var a = setInterval(function(){
						Ext.getCmp('cidade_orcamentos_rodape').setValue(resposta.dados.cidade);
						me.comboEndereco(resposta.dados.cidade, resposta.dados.endereco_bairro, resposta.dados.endereco);
						clearInterval(a);
					}, 300);

					var b = setInterval(function(){
						Ext.getCmp('endereco_orcamentos_rodape').setValue(parseInt(resposta.dados.endereco));
						me.cidade = null;
						clearInterval(b);
					}, 400);

					if (resposta.dados.cliente!=null){
						Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue('');

						if (resposta.dados.titulo != resposta.dados.nome_cliente){
							Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue(resposta.dados.titulo);
						}
					}
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadEnderecoCliente: function(id_cliente){
		var me = this;
		Ext.Ajax.request({
			url : 'server/modulos/clientes/list.php',
			params : {
				action: 'GET_VALUES_ORCAMENTOS',
				id : id_cliente
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'Cliente não Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('estado_orcamentos_rodape').setValue(resposta.dados[0].estado);
					Ext.getCmp('ponto_ref_orcamentos_rodape').setValue(resposta.dados[0].ponto_ref);
					Ext.getCmp('complemento_orcamentos_rodape').setValue(resposta.dados[0].complemento);
					Ext.getCmp('nro_end_orcamentos_rodape').setValue(resposta.dados[0].nro_end);
					Ext.getCmp('loteamento_orcamentos_rodape').setValue(resposta.dados[0].loteamento);
					Ext.getCmp('fone1_orcamentos_rodape').setValue(resposta.dados[0].fone1);
					Ext.getCmp('fone2_orcamentos_rodape').setValue(resposta.dados[0].fone2);
					Ext.getCmp('fone3_orcamentos_rodape').setValue(resposta.dados[0].fone3);
					Ext.getCmp('fone4_orcamentos_rodape').setValue(resposta.dados[0].fone4);
					Ext.getCmp('fone5_orcamentos_rodape').setValue(resposta.dados[0].fone5);
					Ext.getCmp('fone1_obs_orcamentos_rodape').setValue(resposta.dados[0].fone1_obs);
					Ext.getCmp('fone2_obs_orcamentos_rodape').setValue(resposta.dados[0].fone2_obs);
					Ext.getCmp('fone3_obs_orcamentos_rodape').setValue(resposta.dados[0].fone3_obs);
					Ext.getCmp('fone4_obs_orcamentos_rodape').setValue(resposta.dados[0].fone4_obs);
					Ext.getCmp('fone5_obs_orcamentos_rodape').setValue(resposta.dados[0].fone5_obs);

					var a = setInterval(function(){
						Ext.getCmp('cidade_orcamentos_rodape').setValue(resposta.dados[0].cidade);
						me.comboEndereco(resposta.dados[0].cidade, resposta.dados[0].bairro, resposta.dados[0].endereco);
						clearInterval(a);
					}, 300);

					var b = setInterval(function(){
						me.verificaAnotacoesCliente(id_cliente);
						Ext.getCmp('endereco_orcamentos_rodape').setValue(parseInt(resposta.dados[0].endereco));
						me.cidade = null;
						clearInterval(b);
					}, 400);
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}

		var comboCidade = Ext.getCmp('cidade_orcamentos_rodape');

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}

		Ext.getCmp('endereco_orcamentos_rodape').store.proxy.extraParams.localidade_id = combo.getValue();
		me.bairro = true;
		if(Ext.getCmp('endereco_orcamentos_rodape').isDisabled()){
			Ext.getCmp('endereco_orcamentos_rodape').setDisabled(false);
		}
		return true;

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_orcamentos_rodape');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_orcamentos_rodape');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_orcamentos_rodape');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			Ext.getCmp('bairro_nome_orcamentos_rodape').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_orcamentos_rodape').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_orcamentos_rodape').setValue(record.get('loteamento'));
			}
			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_orcamentos_rodape').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_orcamentos_rodape').setValue(record.get('cep'));
			Ext.getCmp('nro_end_orcamentos_rodape').focus(false, 100);
		}
	},

	enableAll: false,

	allProdutos: function(callback){
		var me = this;
		control.getController('OrcamentosRodape').getStore('StoreComboProdutos').load({
			callback: function(){
				me.enableAll = true;
				window.StoreComboProdutosOrcamentosRodape = control.getController('OrcamentosRodape').getStore('StoreComboProdutos');
				if(typeof callback == 'function'){
					callback(me);
				}
			}
		});
	},

	beforeCloseProduto: function(win){
		var me = this;
		if(win.closeMe) {
			win.closeMe = false;
			return true;
		}
		Ext.Msg.show({
			title:'Deseja fechar o formulário?',
			msg:'se tiver feito alguma alteração e não ter salvo o formulário clique em "SIM" para salvar',
			buttons:Ext.Msg.YESNO,
			callback:function(btn) {
				if('yes' === btn) {
					me.updateProduto(Ext.getCmp('button_salvar_produtos_orcamentos_rodape'));
				}
				else{
					win.closeMe = true;
					win.close();
				}
			}
		})

		return false;
	},

	updateProdutoGeral: function(button){
		console.log('updateProdutoGeral');
		var me         = this;
		var grid       = Ext.getCmp('GridOrcamentos');
		var win        = Ext.getCmp('AddGeral_Orcamentos_RodapeWin');
		var form       = Ext.getCmp('FormGeral_Orcamentos_Rodape');

		if(!form.getForm().isValid()){
			return true;
		}

		var valor_total = parseFloat(Ext.getCmp('valor_unitario_geral_orcamentos_rodape').getValue()) * Ext.getCmp('quantidade_geral_orcamentos_rodape').getValue();
		valor_total = valor_total.toFixed(2);
		var id_orcamento = Ext.getCmp('id_orcamento_orcamentos_rodape').getValue();
		me.formParams = {
			tipo_produto: 'P',
			id_orcamento: id_orcamento,
			valor_bruto: valor_total,
			valor_total: valor_total
		};
		
		me.saveForm(grid, form, win, button, function(){
			me.getGridProdutos().store.proxy.extraParams.id_orcamento = id_orcamento;
			me.getGridProdutos().store.load({
				callback: function(){
					me.setValorTotal();

					Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
					Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();
					
					var count = Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_rodape').setValue(count);
				}
			});
		}, false);
	},

	setValorUnitarioGeral: function(combo, value){
		console.log('setValorUnitarioGeral');
		var me = this;
		var record = combo.store.getById(value);
		var comboCorGeral = Ext.getCmp('cor_geral_orcamentos_rodape');

		if (record!=null && value!=null && !isNaN(parseInt(value)))	{
			var qtde_cores = record.raw.produto_cores.length;

			if (qtde_cores > 0) {
				comboCorGeral.setDisabled(false);
				comboCorGeral.setVisible(true);

				comboCorGeral.store.proxy.extraParams.id_produto = combo.getValue();
				comboCorGeral.store.load({
					callback: function(r, options, success){
						comboCorGeral.setValue(r[0].data.id);
						if (Ext.getCmp('quantidade_geral_orcamentos_rodape').getValue() == null) {
							Ext.getCmp('quantidade_geral_orcamentos_rodape').setValue(1);
						}

						if (qtde_cores == 1) {
							Ext.getCmp('quantidade_geral_orcamentos_rodape').focus(false, 10);
						}
						else {
							Ext.getCmp('cor_geral_orcamentos_rodape').focus(false, 10);
						}
					}
				});
			}
			else {
				Ext.getCmp('cor_geral_orcamentos_rodape').reset();
				comboCorGeral.store.removeAll();
				comboCorGeral.setDisabled(true);
				comboCorGeral.setVisible(false);
			}

			if(record!=null){
				var valor_unitario  = me.getValorUnitario(combo, value);
				Ext.getCmp('valor_unitario_geral_orcamentos_rodape').setValue(valor_unitario);
			}
		}
		else {
			Ext.getCmp('cor_geral_orcamentos_rodape').reset();
			comboCorGeral.store.removeAll();
			comboCorGeral.setDisabled(true);
			comboCorGeral.setVisible(false);
		}
	},

	changeTotalAreaProduto: function(field, new_value, old_value){
		console.log('changeTotalAreaProduto');
		var me = this;
		me.changeAreaTotalAmbientes(new_value);
	},

	changeValorUnitario: function(combo, value){
		var me = this;
		var record = combo.store.getById(value);
		if(record!=null){
			var valor_unitario  = me.getValorUnitario(combo, value);
			if(Ext.getCmp('valor_unitario_acessorios_orcamentos_rodape').getValue()==""){
				Ext.getCmp('valor_unitario_acessorios_orcamentos_rodape').setValue(valor_unitario);
			}
		}
	}, 

	changeTotal: function(field, value){
		console.log('changeTotal');
		var me = this;
		var valor_bruto = Ext.getCmp('valor_bruto_orcamentos_rodape').getValue();
		//var desconto = Ext.getCmp('desconto_real_orcamentos_rodape').getValue();
		//var acrescimo = Ext.getCmp('acrescimo_real_orcamentos_rodape').getValue();
		var desconto = 0;
		var acrescimo = 0;

		if(valor_bruto!=undefined && valor_bruto!=null && valor_bruto!=""){
			if(desconto==""){desconto = 0;}
			if(acrescimo==""){acrescimo = 0;}

			valor_bruto = parseFloat(valor_bruto);
			desconto 	= parseFloat(desconto);
			acrescimo	= parseFloat(acrescimo);
			
			if(field.getValue()==""){
				var valor_total = 0;
			}

			var valor_total = parseFloat(field.getValue());

			var subtotal = valor_bruto + acrescimo;
			subtotal 	-= desconto;

			if(subtotal>valor_total){
				/*var desconto2 = subtotal - valor_total;
				desconto += desconto2;
				*/
				desconto = parseFloat(subtotal - valor_total);
				desconto = Math.round((desconto).toFixed(10) * 100)/100;
				Ext.getCmp('acrescimo_real_orcamentos_rodape').setValue(0);
				Ext.getCmp('acrescimo_porcento_orcamentos_rodape').setValue(0);
				Ext.getCmp('desconto_real_orcamentos_rodape').setValue(desconto);
				me.changeDesconto(Ext.getCmp('desconto_real_orcamentos_rodape'), desconto);
			}
			else{
				/*var acrescimo2 = valor_total - subtotal;
				acrescimo += acrescimo2;
				*/
				acrescimo = parseFloat(valor_total - subtotal);
				acrescimo = Math.round((acrescimo).toFixed(10) * 100)/100;
				Ext.getCmp('desconto_real_orcamentos_rodape').setValue(0);
				Ext.getCmp('desconto_porcento_orcamentos_rodape').setValue(0);
				Ext.getCmp('acrescimo_real_orcamentos_rodape').setValue(acrescimo);
				me.changeAcrescimo(Ext.getCmp('acrescimo_real_orcamentos_rodape'), acrescimo);
			}
		}
	},

	changeAcrescimo: function(field, value){
		console.log('changeAcrescimo');
		var me = this;
		var total = Ext.getCmp('valor_bruto_orcamentos_rodape').getValue();
		if(total!=undefined && total!=null && total!=""){
			total = parseFloat(total);
			if(total>0){
				
				if(field.name=='acrescimo_real'){
					var porcento = ((parseFloat(field.getValue()) * 100) / total);
					Ext.getCmp('acrescimo_porcento_orcamentos_rodape').setValue(porcento);
				}
				else{
					var real = parseFloat( (total * field.getValue())/100 );
					Ext.getCmp('acrescimo_real_orcamentos_rodape').setValue(real);
				} 

				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
			}
		}
	},

	changeDesconto: function(field, value){
		console.log('changeDesconto');
		var me = this;
		var total = Ext.getCmp('valor_bruto_orcamentos_rodape').getValue();
		if(total!=undefined && total!=null && total!=""){
			total = parseFloat(total);
			if(total>0){
				if(field.name=='desconto_real'){
					var porcento = ((parseFloat(field.getValue()) * 100) / total);
					Ext.getCmp('desconto_porcento_orcamentos_rodape').setValue(porcento);
				}
				else{
					var real = parseFloat( (total * field.getValue())/100 );
					Ext.getCmp('desconto_real_orcamentos_rodape').setValue(real);
				} 

				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
			}
		}
	},

	resetParams: function(){
		this.produto_rodape = false;
		this.produto_cor = false;
		this.noDeleteLoad = true;
		this.areaTotalAmbiente = null;
		this.somaTotalAmbiente = 0;

		var v = Ext.getCmp('id_cliente_orcamentos_rodape').store.getById(Ext.getCmp('id_cliente_orcamentos_rodape').getValue());
		
		if(v){
			if(v && v.raw.id_categoria_precos!=null){
				this.id_categoria_precos = v.raw.id_categoria_precos;
			}
		}
		else{
			this.id_categoria_precos = 1;
		}
		
		this.tipo_produto = 'P';
		this.record_produto = null;
	},

	////////////////////--PRODUTOS--////////////////////////
	changeCores: function(combo, value){
		console.log('changeCores');
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}

		if(!window.StoreComboProdutosOrcamentosRodape){
			window.StoreComboProdutosOrcamentosRodape = combo.store;
		}

		me.tipo_produto = record.raw.tipo_produto;
		me.record_produto = record;
		var comboCor = Ext.getCmp('id_cor_produtos_orcamentos_rodape');
		
		var valor = me.getValorUnitario(Ext.getCmp('id_produto_produtos_orcamentos_rodape'), Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue());

		if(Ext.getCmp('action_produtos_orcamentos_rodape').getValue()=="INSERIR"){
			Ext.getCmp('valor_unitario_produtos_orcamentos_rodape').setValue(valor);
		}
		else{
			var gridProdutos = me.getGridProdutos();
			if (gridProdutos.selModel.hasSelection()) {
				var record = gridProdutos.getSelectionModel().getLastSelected();
				if(record.get('id_produto')!=Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue()){
					Ext.getCmp('valor_unitario_produtos_orcamentos_rodape').setValue(valor);		
				}
			}
		}

		Ext.getCmp('step_2_produtos_orcamentos_rodape').setDisabled(true);
		Ext.getCmp('button_salvar_produtos_orcamentos_rodape').setDisabled(true);

		if(me.tipo_produto=='S'){

			Ext.getCmp('quantidade_produtos_orcamentos_rodape').setDisabled(false);
			Ext.getCmp('quantidade_produtos_orcamentos_rodape').setVisible(true);

			me.produto_cor = true;
			comboCor.setDisabled(true);
			comboCor.setVisible(false);
			Ext.getCmp('proximo_step_2_produtos_orcamentos_rodape').setDisabled(true);
			Ext.getCmp('step_2_produtos_orcamentos_rodape').setExpanded(false);
		}
		else{
			Ext.getCmp('proximo_step_2_produtos_orcamentos_rodape').setDisabled(false);
			Ext.getCmp('step_2_produtos_orcamentos_rodape').setExpanded(true);

			Ext.getCmp('quantidade_produtos_orcamentos_rodape').setDisabled(true);
			Ext.getCmp('quantidade_produtos_orcamentos_rodape').setVisible(false);
			
			comboCor.setDisabled(false);
			comboCor.setVisible(true);

			comboCor.store.proxy.extraParams.id_produto = combo.getValue();
			comboCor.store.load({
				callback: function(){
					me.produto_cor = true;
				}
			});
		}
	},

	addProduto: function(button){
		var me = this;
		me.resetParams();
		me.areaTotalAmbiente = 0;
		me.getDesktopWindow('AddProdutos_Orcamentos_RodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.EditProdutos', function(){
			Ext.getCmp('id_orcamento_produtos_orcamentos_rodape').setValue(Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.proxy.extraParams.id_orcamento);
			Ext.getCmp('id_produto_produtos_orcamentos_rodape').store.load({
				/*params: {
					in_cod_categoria: "7, 8, 9, 10",
					in_tipo_produto: "'P','I'"
				}*/
			});
			me.getGridAmbientes().store.removeAll();
		});
	},

	addGeral: function(button) {
		var me = this;
		me.getDesktopWindow('AddGeral_Orcamentos_RodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.EditGeral', false);
	},

	editProduto: function(button){
		console.log('editProduto');
		var me = this;
		var gridProdutos = me.getGridProdutos();
		me.resetParams();

		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();
			me.areaTotalAmbiente = record.get('quantidade');

			if(record.get('possui_ambiente')=='S'){
				me.getDesktopWindow('AddProdutos_Orcamentos_RodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.EditProdutos', function(){
					me.getFormProdutos().el.mask('Aguarde...');
					Ext.getCmp('action_orcamentos_rodape').setValue('EDITAR');
					Ext.getCmp('id_produtos_orcamentos_rodape_produtos_orcamentos_rodape').setValue(record.get('id_produtos_orcamentos_rodape'));
					Ext.getCmp('item_nro_produtos_orcamentos_rodape').setValue(record.get('item_nro'));
					Ext.getCmp('id_orcamento_produtos_orcamentos_rodape').setValue(record.raw.id_orcamento);
					Ext.getCmp('valor_unitario_produtos_orcamentos_rodape').setValue(record.get('valor_unitario'));
					
					Ext.getCmp('id_produto_produtos_orcamentos_rodape').store.load({
						/*params: {
							in_cod_categoria: "7, 8, 9, 10",
							in_tipo_produto: "'P','I'"
						},*/
						callback: function(){
							me.produto_rodape = true;
						}
					});

					Ext.getCmp('id_cor_produtos_orcamentos_rodape').store.load({
						params: {
							id_produto: record.get('id_produto')
						},
						callback: function(){
							me.produto_cor = true;
						}
					});

					var store = me.getGridAmbientes().store;
					store.proxy.extraParams.id_produtos_orcamentos_rodape = record.get('id_produtos_orcamentos_rodape');
					store.load();

					var a = setInterval(function(){
						if(record.get('id_produto') != null && record.get('id_produto') > 0){
							if(me.produto_rodape==true){
								Ext.getCmp('id_produto_produtos_orcamentos_rodape').setValue(''+record.get('id_produto')+'');
								me.produto_rodape = null;
								me.changeAreaTotalAmbientes(record.get('qtd_barras'));

								var barra = me.getBarraProduto(''+record.get('id_produto')+'');
								var qtdBarra = Math.ceil((me.areaTotalAmbiente/barra.barra).toFixed(5));
								Ext.getCmp('qtd_barras_orcamentos_rodape').setMinValue(qtdBarra);
							}
						} 
						else{
							me.produto_rodape = null;
						}

						if(record.get('id_cor') != null && record.get('id_cor') > 0){
							if(me.produto_cor==true){
								Ext.getCmp('id_cor_produtos_orcamentos_rodape').setValue(''+record.get('id_cor')+'');
								me.produto_cor = null;
							}
						} 
						else{
							me.produto_cor = null;
						}

						if(me.produto_cor == null && me.produto_rodape == null){
							if(me.getFormProdutos()){
								me.getFormProdutos().el.unmask();
								Ext.getCmp('action_produtos_orcamentos_rodape').setValue('EDITAR');
								Ext.getCmp('step_2_produtos_orcamentos_rodape').setDisabled(true);
								//me.setStepAmbientes(button);
							}
							
							clearInterval(a);
							me.produto_cor = false;
							me.produto_rodape = false;
						}
					});
				});
			}
			else{
				me.getDesktopWindow('AddGeral_Orcamentos_RodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.EditGeral', function(){
					me.produto_rodape = false;
					Ext.getCmp('FormGeral_Orcamentos_Rodape').el.mask('Aguarde...');
					Ext.getCmp('id_produto_geral_orcamentos_rodape').store.load({
						/*params: {
							in_cod_categoria: "7, 8, 9, 10",
							in_tipo_produto: "'P','I'"
						},*/
						callback: function(){
							me.produto_rodape = true;
						}
					});
					Ext.getCmp('action_geral_orcamentos_rodape').setValue('EDITAR');
					Ext.getCmp('quantidade_geral_orcamentos_rodape').setValue(record.get('quantidade'));
					Ext.getCmp('valor_unitario_geral_orcamentos_rodape').setValue(record.get('valor_unitario'));
					Ext.getCmp('item_nro_geral_orcamentos_rodape').setValue(record.get('item_nro'));
					Ext.getCmp('id_produto_geral_orcamentos_rodape').setValue(record.get('id_produto'));
					Ext.getCmp('id_produtos_orcamentos_rodape_geral_orcamentos_rodape').setValue(record.get('id_produtos_orcamentos_rodape'));

					var a = setInterval(function(){
						if(record.get('id_produto') != null && record.get('id_produto') > 0){
							if(me.produto_rodape==true){
								Ext.getCmp('id_produto_geral_orcamentos_rodape').setValue(''+record.get('id_produto')+'');

								setTimeout(function() {
									if(record.get('id_cor') != null && record.get('id_cor') > 0){
										Ext.getCmp('cor_geral_orcamentos_rodape').setValue(''+record.get('id_cor')+'');
									}
								}, 800);
								me.produto_rodape = null;
							}
						} 
						else{
							me.produto_rodape = null;
						}

						if(me.produto_rodape == null){
							if(Ext.getCmp('FormGeral_Orcamentos_Rodape')){
								Ext.getCmp('FormGeral_Orcamentos_Rodape').el.unmask();
							}
							
							clearInterval(a);
							me.produto_rodape = false;
						}
					});
				});
			}

		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},
	
	deleteProduto: function(button) {
		var me = this;
		me.resetParams();
		var gridProdutos = me.getGridProdutos();
		
		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar o Produto?', function(btn){
				if(btn=='yes'){
					if(record.get('id_produtos_orcamentos_rodape')>0){
						me.deleteAjax(gridProdutos, 'produtos_orcamentos_rodape', {
							action: 'DELETAR',
							id: record.get('id_produtos_orcamentos_rodape'),
							id_orcamento: record.get('id_orcamento'),
							item_nro: record.get('item_nro')
						}, button, function(){
							gridProdutos.store.load();  
							Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						});
					}

					gridProdutos.store.remove(record);
					me.setValorTotal();
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();

					var count = Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_rodape').setValue(count);

					if (Ext.getCmp('valor_total_orcamentos_rodape').getValue()=='' || Ext.getCmp('valor_total_orcamentos_rodape').getValue()==null){
						Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(false);
					}
					else {
						Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
					}
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	updateProduto: function(button){
		console.log('updateProduto');
		var me = this;
		if(!me.getFormProdutos().getForm().isValid()){
			return true;
		}
		var action          = Ext.getCmp('action_produtos_orcamentos_rodape').getValue();
		var id_produto      = Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue();
		var id_cor          = Ext.getCmp('id_cor_produtos_orcamentos_rodape').getValue();
		var item_nro          = Ext.getCmp('item_nro_produtos_orcamentos_rodape').getValue();

		//PRODUTO ou SERVICO
		//var quantidade      = me.areaTotalAmbiente;
		var quantidade      = Ext.getCmp('total_area_orcamentos_rodape').getValue();
		//var valor_unitario  = me.getValorUnitario(Ext.getCmp('id_produto_produtos_orcamentos_rodape'), Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue());
		var valor_unitario  = Ext.getCmp('valor_unitario_produtos_orcamentos_rodape').getValue();
		//var valor_total     = valor_unitario * me.areaTotalAmbiente;
		var valor_total     = valor_unitario * Ext.getCmp('total_area_orcamentos_rodape').getValue();

		//PRODUTO INSTALADO
		if(me.tipo_produto=='I'){
			var quantidade_servico  = Ext.getCmp('total_area_orcamentos_rodape').getValue();
			//var quantidade_servico  = me.areaTotalAmbiente;
			var record              = Ext.getCmp('id_produto_produtos_orcamentos_rodape').store.getById(id_produto);
			var valor_servico       = quantidade_servico * parseFloat(record.raw.preco_custo_servico);
			//var valor_total         = valor_total + valor_servico;
			var valor_total         = valor_total;
		}

		var records         = me.getGridAmbientes().store.getModifiedRecords();
		var json_ambientes  = [];
		for(var i in records){
			json_ambientes.push(Ext.encode(records[i].data));
		}

		var id_orcamento = Ext.getCmp('id_orcamento_orcamentos_rodape').getValue();

		var valor_bruto = valor_total;
		valor_total = valor_total;

		me.formParams = {
			tipo_produto: me.tipo_produto,
			id_orcamento: id_orcamento,
			id_produto: id_produto,
			id_cor: id_cor,
			item_nro: item_nro,
			quantidade: quantidade,
			
			total_area: Ext.getCmp('total_area_orcamentos_rodape').getValue(),
			qtd_barras: Ext.getCmp('qtd_barras_orcamentos_rodape').getValue(),

			valor_unitario: valor_unitario,
			valor_bruto: valor_bruto.toFixed(2),
			valor_total: valor_total.toFixed(2),

			ambientes: [json_ambientes]
		};
		
		me.saveForm(me.getGridProdutos(), me.getFormProdutos(), me.getAddProdutosWin(), button, function(){
			me.getGridProdutos().store.proxy.extraParams.id_orcamento = id_orcamento;
			me.getGridProdutos().store.load({
				callback: function(){
					me.setValorTotal();

					Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
					Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();

					var count = Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_rodape').setValue(count);
				}
			});
		}, false);
	},

	setStepAmbientes: function(button){
		console.log('setStepAmbientes');
		var me = this;
		if(button==true || (Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue()!=null && Ext.getCmp('id_cor_produtos_orcamentos_rodape').getValue()!=null)){
			//Ext.getCmp('step_1_produtos_orcamentos_rodape').setDisabled(true);
			//Ext.getCmp('proximo_step_2_produtos_orcamentos_rodape').setDisabled(true);
			Ext.getCmp('step_2_produtos_orcamentos_rodape').setDisabled(false);
			Ext.getCmp('button_salvar_produtos_orcamentos_rodape').setDisabled(false);
			Ext.getCmp('button_resetar_produtos_orcamentos_rodape').setDisabled(true);

			if(me.getGridAmbientes().store.getCount()>0){
				me.recalculateAmbiente();
			}
		}
	},
	////////////////////--PRODUTOS--////////////////////////


	////////////////////--AMBIENTES--////////////////////////
	addAmbiente: function(button){
		var me = this;
		me.getDesktopWindow('AddAmbientes_Orcamentos_RodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.EditAmbientes');
	},

	editAmbiente: function(button){
		var me = this;
		var gridAmbientes = me.getGridAmbientes();

		if (gridAmbientes.selModel.hasSelection()) {
			var record = gridAmbientes.getSelectionModel().getLastSelected();
			
			me.getDesktopWindow('AddAmbientes_Orcamentos_RodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.EditAmbientes', function(){
				me.getFormAmbientes().getForm().setValues(record.data);
				Ext.getCmp('action_ambientes_orcamentos_rodape').setValue('EDITAR');
			});
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	deleteAmbiente: function(button) {
		var me = this;
		var gridAmbientes = me.getGridAmbientes();
		if (gridAmbientes.selModel.hasSelection()) {
			var record = gridAmbientes.getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar o Ambiente?', function(btn){
				if(btn=='yes'){
					if(record.get('id_ambientes_orcamentos_rodape')>0){
						me.deleteAjax(gridAmbientes, 'ambientes_orcamentos_rodape', {
							action: 'DELETAR',
							id: record.get('id_ambientes_orcamentos_rodape'),
							id_orcamento: Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.proxy.extraParams.id_orcamento
						}, button, false);
					}

					gridAmbientes.store.remove(record);
					me.changeAreaTotalAmbientes();
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	getComprimentoProduto: function(id_produto){
		console.log('getComprimentoProduto');
		var me = this;
		var comprimento = 0;

		window.StoreComboProdutosOrcamentosRodape = Ext.getCmp('id_produto_produtos_orcamentos_rodape').store;
		if(window.StoreComboProdutosOrcamentosRodape){
			var record = StoreComboProdutosOrcamentosRodape.getById(id_produto);
			var attrs = record.raw.atributos_produto;
			
			if(attrs!=null && attrs!=undefined && attrs.length>0){
				for(var i=0; i<attrs.length;i++){
					if(attrs[i]['sigla']=='comprimento'){
					   comprimento = parseFloat(attrs[i]['valor']);
					}
				}
			}
		}

		return {comprimento: comprimento};
	},

	getBarraProduto: function(id_produto){
		console.log('getBarraProduto');
		var me = this;
		var barra = 1;

		if(window.StoreComboProdutosOrcamentosRodape){
			var record = StoreComboProdutosOrcamentosRodape.getById(id_produto);
			var attrs = record.raw.atributos_produto;
			
			if(attrs!=null && attrs!=undefined && attrs.length>0){
				for(var i=0; i<attrs.length;i++){
					if(attrs[i]['sigla']=='barra_com'){
					   barra = parseFloat(attrs[i]['valor']);
					}
				}
			}
		}

		return {barra: barra};
	},

	updateAmbiente: function(button){
		console.log('updateAmbiente');
		var me = this;
		var values = me.getFormAmbientes().getForm().getValues();
		if(!me.getFormAmbientes().getForm().isValid()){
			return true;
		}

		var id_produto = Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue();

		var divLarguraAltura = me.getComprimentoProduto(id_produto);
		var compAcadaXcm = divLarguraAltura.comprimento;
		var qtd_paredes = 21;

		if(isNaN(parseFloat(values.comprimento_real_1)) || values.comprimento_real_1==0){values.comprimento_real_1 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_2)) || values.comprimento_real_2==0){values.comprimento_real_2 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_3)) || values.comprimento_real_3==0){values.comprimento_real_3 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_4)) || values.comprimento_real_4==0){values.comprimento_real_4 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_5)) || values.comprimento_real_5==0){values.comprimento_real_5 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_6)) || values.comprimento_real_6==0){values.comprimento_real_6 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_7)) || values.comprimento_real_7==0){values.comprimento_real_7 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_8)) || values.comprimento_real_8==0){values.comprimento_real_8 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_9)) || values.comprimento_real_9==0){values.comprimento_real_9 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_10)) || values.comprimento_real_10==0){values.comprimento_real_10 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_11)) || values.comprimento_real_11==0){values.comprimento_real_11 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_12)) || values.comprimento_real_12==0){values.comprimento_real_12 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_13)) || values.comprimento_real_13==0){values.comprimento_real_13 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_14)) || values.comprimento_real_14==0){values.comprimento_real_14 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_15)) || values.comprimento_real_15==0){values.comprimento_real_15 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_16)) || values.comprimento_real_16==0){values.comprimento_real_16 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_17)) || values.comprimento_real_17==0){values.comprimento_real_17 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_18)) || values.comprimento_real_18==0){values.comprimento_real_18 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_19)) || values.comprimento_real_19==0){values.comprimento_real_19 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_20)) || values.comprimento_real_20==0){values.comprimento_real_20 = 0;qtd_paredes--;}
		if(isNaN(parseFloat(values.comprimento_real_21)) || values.comprimento_real_21==0){values.comprimento_real_21 = 0;qtd_paredes--;}

		values.comprimento_1   = Math.ceil(values.comprimento_real_1 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_2   = Math.ceil(values.comprimento_real_2 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_3   = Math.ceil(values.comprimento_real_3 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_4   = Math.ceil(values.comprimento_real_4 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_5   = Math.ceil(values.comprimento_real_5 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_6   = Math.ceil(values.comprimento_real_6 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_7   = Math.ceil(values.comprimento_real_7 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_8   = Math.ceil(values.comprimento_real_8 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_9   = Math.ceil(values.comprimento_real_9 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_10  = Math.ceil(values.comprimento_real_10 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_11  = Math.ceil(values.comprimento_real_11 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_12  = Math.ceil(values.comprimento_real_12 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_13  = Math.ceil(values.comprimento_real_13 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_14  = Math.ceil(values.comprimento_real_14 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_15  = Math.ceil(values.comprimento_real_15 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_16  = Math.ceil(values.comprimento_real_16 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_17  = Math.ceil(values.comprimento_real_17 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_18  = Math.ceil(values.comprimento_real_18 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_19  = Math.ceil(values.comprimento_real_19 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_20  = Math.ceil(values.comprimento_real_20 / compAcadaXcm) * compAcadaXcm;
		values.comprimento_21  = Math.ceil(values.comprimento_real_21 / compAcadaXcm) * compAcadaXcm;

		var area = values.comprimento_1 + values.comprimento_2 + values.comprimento_3 + values.comprimento_4 + 
					values.comprimento_5 + values.comprimento_6 + values.comprimento_7 + values.comprimento_8 + 
					values.comprimento_9 + values.comprimento_10 + values.comprimento_11 + values.comprimento_12 + 
					values.comprimento_13 + values.comprimento_14 + values.comprimento_15 + values.comprimento_16 + 
					values.comprimento_17 + values.comprimento_18 + values.comprimento_19 + values.comprimento_20 + 
					values.comprimento_21;

		if(values.action=='INSERIR'){
			me.getGridAmbientes().store.add({
				ambiente: values.ambiente,
				observacoes: values.observacoes.toUpperCase(),
				comprimento_1: parseFloat(values.comprimento_1).toFixed(2),
				comprimento_real_1: values.comprimento_real_1,
				comprimento_2: parseFloat(values.comprimento_2).toFixed(2),
				comprimento_real_2: values.comprimento_real_2,
				comprimento_3: parseFloat(values.comprimento_3).toFixed(2),
				comprimento_real_3: values.comprimento_real_3,
				comprimento_4: parseFloat(values.comprimento_4).toFixed(2),
				comprimento_real_4: values.comprimento_real_4,
				comprimento_5: parseFloat(values.comprimento_5).toFixed(2),
				comprimento_real_5: values.comprimento_real_5,
				comprimento_6: parseFloat(values.comprimento_6).toFixed(2),
				comprimento_real_6: values.comprimento_real_6,
				comprimento_7: parseFloat(values.comprimento_7).toFixed(2),
				comprimento_real_7: values.comprimento_real_7,
				comprimento_8: parseFloat(values.comprimento_8).toFixed(2),
				comprimento_real_8: values.comprimento_real_8,
				comprimento_9: parseFloat(values.comprimento_9).toFixed(2),
				comprimento_real_9: values.comprimento_real_9,
				comprimento_10: parseFloat(values.comprimento_10).toFixed(2),
				comprimento_real_10: values.comprimento_real_10,
				comprimento_11: parseFloat(values.comprimento_11).toFixed(2),
				comprimento_real_11: values.comprimento_real_11,
				comprimento_12: parseFloat(values.comprimento_12).toFixed(2),
				comprimento_real_12: values.comprimento_real_12,
				comprimento_13: parseFloat(values.comprimento_13).toFixed(2),
				comprimento_real_13: values.comprimento_real_13,
				comprimento_14: parseFloat(values.comprimento_14).toFixed(2),
				comprimento_real_14: values.comprimento_real_14,
				comprimento_15: parseFloat(values.comprimento_15).toFixed(2),
				comprimento_real_15: values.comprimento_real_15,
				comprimento_16: parseFloat(values.comprimento_16).toFixed(2),
				comprimento_real_16: values.comprimento_real_16,
				comprimento_17: parseFloat(values.comprimento_17).toFixed(2),
				comprimento_real_17: values.comprimento_real_17,
				comprimento_18: parseFloat(values.comprimento_18).toFixed(2),
				comprimento_real_18: values.comprimento_real_18,
				comprimento_19: parseFloat(values.comprimento_19).toFixed(2),
				comprimento_real_19: values.comprimento_real_19,
				comprimento_20: parseFloat(values.comprimento_20).toFixed(2),
				comprimento_real_20: values.comprimento_real_20,
				comprimento_21: parseFloat(values.comprimento_21).toFixed(2),
				comprimento_real_21: values.comprimento_real_21,

				area: area.toFixed(2),
				qtd_paredes: qtd_paredes
			});

			me.areaTotalAmbiente = area.toFixed(2);
		}
		else{
			window.rec = me.getGridAmbientes().getSelectionModel().getLastSelected();
			me.areaTotalAmbiente = area.toFixed(2);

			rec.set('ambiente', values.ambiente);
			rec.set('observacoes', values.observacoes.toUpperCase());
			rec.set('comprimento_1', parseFloat(values.comprimento_1).toFixed(2));
			rec.set('comprimento_real_1', values.comprimento_real_1);
			rec.set('comprimento_2', parseFloat(values.comprimento_2).toFixed(2));
			rec.set('comprimento_real_2', values.comprimento_real_2);
			rec.set('comprimento_3', parseFloat(values.comprimento_3).toFixed(2));
			rec.set('comprimento_real_3', values.comprimento_real_3);
			rec.set('comprimento_4', parseFloat(values.comprimento_4).toFixed(2));
			rec.set('comprimento_real_4', values.comprimento_real_4);
			rec.set('comprimento_5', parseFloat(values.comprimento_5).toFixed(2));
			rec.set('comprimento_real_5', values.comprimento_real_5);
			rec.set('comprimento_6', parseFloat(values.comprimento_6).toFixed(2));
			rec.set('comprimento_real_6', values.comprimento_real_6);
			rec.set('comprimento_7', parseFloat(values.comprimento_7).toFixed(2));
			rec.set('comprimento_real_7', values.comprimento_real_7);
			rec.set('comprimento_8', parseFloat(values.comprimento_8).toFixed(2));
			rec.set('comprimento_real_8', values.comprimento_real_8);
			rec.set('comprimento_9', parseFloat(values.comprimento_9).toFixed(2));
			rec.set('comprimento_real_9', values.comprimento_real_9);
			rec.set('comprimento_10', parseFloat(values.comprimento_10).toFixed(2));
			rec.set('comprimento_real_10', values.comprimento_real_10);
			rec.set('comprimento_11', parseFloat(values.comprimento_11).toFixed(2));
			rec.set('comprimento_real_11', values.comprimento_real_11);
			rec.set('comprimento_12', parseFloat(values.comprimento_12).toFixed(2));
			rec.set('comprimento_real_12', values.comprimento_real_12);
			rec.set('comprimento_13', parseFloat(values.comprimento_13).toFixed(2));
			rec.set('comprimento_real_13', values.comprimento_real_13);
			rec.set('comprimento_14', parseFloat(values.comprimento_14).toFixed(2));
			rec.set('comprimento_real_14', values.comprimento_real_14);
			rec.set('comprimento_15', parseFloat(values.comprimento_15).toFixed(2));
			rec.set('comprimento_real_15', values.comprimento_real_15);
			rec.set('comprimento_16', parseFloat(values.comprimento_16).toFixed(2));
			rec.set('comprimento_real_16', values.comprimento_real_16);
			rec.set('comprimento_17', parseFloat(values.comprimento_17).toFixed(2));
			rec.set('comprimento_real_17', values.comprimento_real_17);
			rec.set('comprimento_18', parseFloat(values.comprimento_18).toFixed(2));
			rec.set('comprimento_real_18', values.comprimento_real_18);
			rec.set('comprimento_19', parseFloat(values.comprimento_19).toFixed(2));
			rec.set('comprimento_real_19', values.comprimento_real_19);
			rec.set('comprimento_20', parseFloat(values.comprimento_20).toFixed(2));
			rec.set('comprimento_real_20', values.comprimento_real_20);
			rec.set('comprimento_21', parseFloat(values.comprimento_21).toFixed(2));
			rec.set('comprimento_real_21', values.comprimento_real_21);

			rec.set('area', area.toFixed(2));
			rec.set('qtd_paredes', qtd_paredes);
		}
		me.getAddAmbientesWin().close();
		me.getGridAmbientes().getSelectionModel().deselectAll();
		me.changeAreaTotalAmbientes();
	},

	recalculateAmbiente: function(){
		console.log('recalculateAmbiente');
		var me = this;
		var grid = me.getGridAmbientes();
		var produto = Ext.getCmp('id_produto_produtos_orcamentos_rodape');
		var divLarguraAltura = me.getComprimentoProduto(produto.getValue());
		var compAcadaXcm = divLarguraAltura.comprimento;
		var items = grid.store.data.items;

		for(var i=0;i<items.length;i++){
			var item = items[i];

			var qtd_paredes = 21;
			var comprimento_real_1 = item.get('comprimento_real_1');
			var comprimento_real_2 = item.get('comprimento_real_2');
			var comprimento_real_3 = item.get('comprimento_real_3');
			var comprimento_real_4 = item.get('comprimento_real_4');
			var comprimento_real_5 = item.get('comprimento_real_5');
			var comprimento_real_6 = item.get('comprimento_real_6');
			var comprimento_real_7 = item.get('comprimento_real_7');
			var comprimento_real_8 = item.get('comprimento_real_8');
			var comprimento_real_9 = item.get('comprimento_real_9');
			var comprimento_real_10 = item.get('comprimento_real_10');
			var comprimento_real_11 = item.get('comprimento_real_11');
			var comprimento_real_12 = item.get('comprimento_real_12');
			var comprimento_real_13 = item.get('comprimento_real_13');
			var comprimento_real_14 = item.get('comprimento_real_14');
			var comprimento_real_15 = item.get('comprimento_real_15');
			var comprimento_real_16 = item.get('comprimento_real_16');
			var comprimento_real_17 = item.get('comprimento_real_17');
			var comprimento_real_18 = item.get('comprimento_real_18');
			var comprimento_real_19 = item.get('comprimento_real_19');
			var comprimento_real_20 = item.get('comprimento_real_20');
			var comprimento_real_21 = item.get('comprimento_real_21');

			if(isNaN(parseFloat(comprimento_real_1)) || comprimento_real_1==0){comprimento_real_1 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_2)) || comprimento_real_2==0){comprimento_real_2 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_3)) || comprimento_real_3==0){comprimento_real_3 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_4)) || comprimento_real_4==0){comprimento_real_4 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_5)) || comprimento_real_5==0){comprimento_real_5 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_6)) || comprimento_real_6==0){comprimento_real_6 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_7)) || comprimento_real_7==0){comprimento_real_7 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_8)) || comprimento_real_8==0){comprimento_real_8 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_9)) || comprimento_real_9==0){comprimento_real_9 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_10)) || comprimento_real_10==0){comprimento_real_10 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_11)) || comprimento_real_11==0){comprimento_real_11 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_12)) || comprimento_real_12==0){comprimento_real_12 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_13)) || comprimento_real_13==0){comprimento_real_13 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_14)) || comprimento_real_14==0){comprimento_real_14 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_15)) || comprimento_real_15==0){comprimento_real_15 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_16)) || comprimento_real_16==0){comprimento_real_16 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_17)) || comprimento_real_17==0){comprimento_real_17 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_18)) || comprimento_real_18==0){comprimento_real_18 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_19)) || comprimento_real_19==0){comprimento_real_19 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_20)) || comprimento_real_20==0){comprimento_real_20 = 0;qtd_paredes--;}
			if(isNaN(parseFloat(comprimento_real_21)) || comprimento_real_21==0){comprimento_real_21 = 0;qtd_paredes--;}

			var comprimentoTemp1 = Math.ceil(item.get('comprimento_real_1') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp2 = Math.ceil(item.get('comprimento_real_2') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp3 = Math.ceil(item.get('comprimento_real_3') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp4 = Math.ceil(item.get('comprimento_real_4') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp5 = Math.ceil(item.get('comprimento_real_5') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp6 = Math.ceil(item.get('comprimento_real_6') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp7 = Math.ceil(item.get('comprimento_real_7') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp8 = Math.ceil(item.get('comprimento_real_8') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp9 = Math.ceil(item.get('comprimento_real_9') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp10 = Math.ceil(item.get('comprimento_real_10') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp11 = Math.ceil(item.get('comprimento_real_11') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp12 = Math.ceil(item.get('comprimento_real_12') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp13 = Math.ceil(item.get('comprimento_real_13') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp14 = Math.ceil(item.get('comprimento_real_14') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp15 = Math.ceil(item.get('comprimento_real_15') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp16 = Math.ceil(item.get('comprimento_real_16') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp17 = Math.ceil(item.get('comprimento_real_17') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp18 = Math.ceil(item.get('comprimento_real_18') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp19 = Math.ceil(item.get('comprimento_real_19') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp20 = Math.ceil(item.get('comprimento_real_20') / compAcadaXcm) * compAcadaXcm;
			var comprimentoTemp21 = Math.ceil(item.get('comprimento_real_21') / compAcadaXcm) * compAcadaXcm;

			var area = comprimentoTemp1 + comprimentoTemp2 + comprimentoTemp3 + comprimentoTemp4 + comprimentoTemp5 +
					comprimentoTemp6 + comprimentoTemp7 + comprimentoTemp8 + comprimentoTemp9 + comprimentoTemp10 + 
					comprimentoTemp11 + comprimentoTemp12 + comprimentoTemp13 + comprimentoTemp14 + comprimentoTemp15 + 
					comprimentoTemp16 + comprimentoTemp17 + comprimentoTemp18 + comprimentoTemp19 + comprimentoTemp20 + 
					comprimentoTemp21;

			item.set('comprimento_1', parseFloat(comprimentoTemp1).toFixed(2));
			item.set('comprimento_2', parseFloat(comprimentoTemp2).toFixed(2));
			item.set('comprimento_3', parseFloat(comprimentoTemp3).toFixed(2));
			item.set('comprimento_4', parseFloat(comprimentoTemp4).toFixed(2));
			item.set('comprimento_5', parseFloat(comprimentoTemp5).toFixed(2));
			item.set('comprimento_6', parseFloat(comprimentoTemp6).toFixed(2));
			item.set('comprimento_7', parseFloat(comprimentoTemp7).toFixed(2));
			item.set('comprimento_8', parseFloat(comprimentoTemp8).toFixed(2));
			item.set('comprimento_9', parseFloat(comprimentoTemp9).toFixed(2));
			item.set('comprimento_10', parseFloat(comprimentoTemp10).toFixed(2));
			item.set('comprimento_11', parseFloat(comprimentoTemp11).toFixed(2));
			item.set('comprimento_12', parseFloat(comprimentoTemp12).toFixed(2));
			item.set('comprimento_13', parseFloat(comprimentoTemp13).toFixed(2));
			item.set('comprimento_14', parseFloat(comprimentoTemp14).toFixed(2));
			item.set('comprimento_15', parseFloat(comprimentoTemp15).toFixed(2));
			item.set('comprimento_16', parseFloat(comprimentoTemp16).toFixed(2));
			item.set('comprimento_17', parseFloat(comprimentoTemp17).toFixed(2));
			item.set('comprimento_18', parseFloat(comprimentoTemp18).toFixed(2));
			item.set('comprimento_19', parseFloat(comprimentoTemp19).toFixed(2));
			item.set('comprimento_20', parseFloat(comprimentoTemp20).toFixed(2));
			item.set('comprimento_21', parseFloat(comprimentoTemp21).toFixed(2));

			item.set('area', area.toFixed(2));
			item.set('qtd_paredes', qtd_paredes);
		}

		me.changeAreaTotalAmbientes();
	},

	////////////////////--AMBIENTES--////////////////////////

	changeAreaTotalAmbientes: function(qtdBarra){
		console.log('changeAreaTotalAmbientes');
		var me      = this;
		me.areaTotalAmbiente = 0;
		var grid    = me.getGridAmbientes();

		grid.store.each(function(record,idx){
			me.areaTotalAmbiente += parseFloat(record.get('area'));
		});

		var id_produto = Ext.getCmp('id_produto_produtos_orcamentos_rodape').getValue();
		var barra = me.getBarraProduto(id_produto);

		if(qtdBarra==null || qtdBarra==undefined){
			var qtdBarra = Math.ceil((me.areaTotalAmbiente/barra.barra).toFixed(5));
			Ext.getCmp('qtd_barras_orcamentos_rodape').setMinValue(qtdBarra);
		}

		var total_area = Math.round((qtdBarra*barra.barra).toFixed(10) * 100)/100;
		Ext.getCmp('qtd_barras_orcamentos_rodape').setValue(qtdBarra);
		Ext.getCmp('total_area_orcamentos_rodape').setValue(total_area);

		me.changePorcentoArea();
		return me.areaTotalAmbiente;
	},

	changePorcentoArea: function(){
		console.log('changePorcentoArea');
		var me = this;
		var total1 = me.areaTotalAmbiente;
		var total2 = Ext.getCmp('total_area_orcamentos_rodape').getValue();
		var porcento = ((total2 / total1)-1)*100;
		Ext.getCmp('qtd_metros_porcento_orcamentos_rodape').setValue(porcento);
	},

	enableCliente: function(checkbox, newValue){
		var me = this;

		me.resetCombo(Ext.getCmp('button_id_cliente_orcamentos_rodape'));
		Ext.getCmp('cliente_descricao_orcamentos_rodape').reset();

		if(newValue==false){
			Ext.getCmp('cliente_descricao_orcamentos_rodape').setVisible(true);
			Ext.getCmp('cliente_descricao_orcamentos_rodape').setDisabled(false);
			Ext.getCmp('cliente_descricao_orcamentos_rodape').setValue('CONSUMIDOR');
			Ext.getCmp('fieldcontainer_id_cliente_orcamentos_rodape').setVisible(false);
			Ext.getCmp('id_cliente_orcamentos_rodape').setDisabled(true);
		}
		else{
			Ext.getCmp('cliente_descricao_orcamentos_rodape').setVisible(false);
			Ext.getCmp('cliente_descricao_orcamentos_rodape').setDisabled(true);
			Ext.getCmp('fieldcontainer_id_cliente_orcamentos_rodape').setVisible(true);
			Ext.getCmp('id_cliente_orcamentos_rodape').setDisabled(false);
		}
	},

	orcamentoVencido: false,

	changeCliente: function(id_categoria_precos, only_price, id_cliente){
		console.log('changeCliente');
		var me              = this;
		var grid            = Ext.getCmp('GridProdutos_Orcamentos_Rodape');
		var items           = grid.store.data.items;
		var produtoStore    = window.StoreComboProdutosOrcamentosRodape;
		var total           = 0;

		this.allProdutos(function(me){
			var grid            = Ext.getCmp('GridProdutos_Orcamentos_Rodape');
			var items           = grid.store.data.items;
			var produtoStore    = window.StoreComboProdutosOrcamentosRodape;
			var total           = 0;

			if(only_price!=true){
				if(!produtoStore || me.areaTotalAmbiente==null){
					return true;
				}
			}

			for(var i=0;i<items.length;i++){
				var recordProduto = produtoStore.getById(''+items[i].get('id_produto')+'');
				
				if(recordProduto){
					if(me.id_categoria_precos_old!=id_categoria_precos){
						var valor_unit = me.getValorRecord(recordProduto, id_categoria_precos);
					}
					else{
						var valor_unit = items[i].get('valor_unitario');
					}

					if(me.orcamentoVencido){
						var valor_unit = me.getValorRecord(recordProduto, id_categoria_precos);
					}

					if(only_price!=true){
						var valor_total = valor_unit * me.areaTotalAmbiente;
						items[i].set('quantidade', me.areaTotalAmbiente);
					}
					else{
						//var valor_total = valor_unit * items[i].get('quantidade');
						var valor_total = (valor_unit * items[i].get('quantidade')).toFixed(2);
					}

					//valor_total = parseFloat(valor_total);
					//valor_total = valor_total.toFixed(2);

					items[i].set('valor_unitario', valor_unit);
					items[i].set('valor_total', valor_total);

					//total += valor_total;
					total += parseFloat(valor_total);
					total = Math.round((total).toFixed(10) * 100)/100;
				}
			}

			setTimeout(function(){
				me.setValorTotal(total);
			}, 300);
		});
	},

	getValorRecord: function(recordProduto, id_categoria_precos){
		console.log('getValorRecord');
		var preco   = recordProduto.raw.preco_venda || recordProduto.raw.preco_custo;
		var precos = recordProduto.raw.produto_precos;

		if(precos!=undefined){
			for(var i=0;i<precos.length;i++){
				if(precos[i].id_categoria_precos==id_categoria_precos){
					preco = precos[i].valor;
				}
			}
		}

		if(preco==undefined || preco==null){
			info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria');
		}
		
		return preco;
	},
	
	setValorTotal: function(total){
		console.log('setValorTotal');
		var me = this;

		var grid = Ext.getCmp('GridProdutos_Orcamentos_Rodape');
		var items = grid.store.data.items;
		var desconto = Ext.getCmp('desconto_real_orcamentos_rodape').getValue();
		var acrescimo = Ext.getCmp('acrescimo_real_orcamentos_rodape').getValue();
		if(typeof total != 'number'){
			total = null;
		}
		if(total==undefined || total==null){
			total = 0;
			
			for(var i=0;i<items.length;i++){
				total += items[i].get('valor_total');
			}
		}

		var subTotal = total;
		subTotal = Math.round((subTotal).toFixed(10) * 100)/100;
		Ext.getCmp('valor_bruto_orcamentos_rodape').setValue(subTotal);

		if(acrescimo!=""){
			total += parseFloat(acrescimo);
		}

		if(desconto!=""){
			total -= parseFloat(desconto);
		}

		total = Math.round((total).toFixed(10) * 100)/100;
		Ext.getCmp('valor_total_orcamentos_rodape').setValue(total);
	},

	changeGridValores: function(comboCliente, value){
		console.log('changeGridValores');
		var me = this;
		var produtoStore    = window.StoreComboProdutosOrcamentosRodape;
		
		if(!produtoStore){
			control.getController('OrcamentosRodape').getStore('StoreComboProdutos').load({
				callback: function(){
					window.StoreComboProdutosOrcamentosRodape = control.getController('OrcamentosRodape').getStore('StoreComboProdutos');
					me.getCategoriaPrecos(comboCliente, value);
					me.changeCliente(me.id_categoria_precos, true, value);
				}
			});
		}
		else{
			me.getCategoriaPrecos(comboCliente, value);
			me.changeCliente(me.id_categoria_precos, true, value);
		}
	},

	getValorUnitario: function(produto, id_produto){
		console.log('getValorUnitario');
		var me              = this;
		var produtoStore    = produto.store;
		var recordProduto   = produtoStore.getById(id_produto);

		if(recordProduto){
			return me.getValorRecord(recordProduto, me.id_categoria_precos);
		}
		else{
			return 0;
		}
	},

	getCategoriaPrecos: function(comboCliente, value){
		console.log('getCategoriaPrecos');
		var me = this;

		me.id_categoria_precos_old = me.id_categoria_precos;
		if(Ext.getCmp('cliente_cadastrado_orcamentos_rodape').getValue()==true){
			var recordCliente = comboCliente.store.getById(comboCliente.getValue());

			if(recordCliente && recordCliente.raw.id_categoria_precos!=null){
				me.id_categoria_precos = recordCliente.raw.id_categoria_precos;
			}
		}
		return me.id_categoria_precos;
	},

	changeQuantidadeAndPrecoProdutos: function(){
		consolel.log('changeQuantidadeAndPrecoProdutos');
		var me = this;
		me.changeCliente(me.id_categoria_precos);
	},

	update: function(button) {
		console.log('update');
		var me         = this;
		var grid       = Ext.getCmp('GridOrcamentos');
		var win        = Ext.getCmp('AddOrcamentosRodapeWin');
		var form       = Ext.getCmp('FormOrcamentosRodape');

		//remover orcamento nao salvo
		me.delete_orcamento = false;
		var records         = me.getGridProdutos().store.getModifiedRecords();
		var json_produtos  = [];
		for(var i in records){
			json_produtos.push(Ext.encode(records[i].data));
		}

		me.formParams = {
			produtos: [json_produtos]
		};
			
		me.saveForm(grid, form, win, button, function(){
			me.resetParams();
			me.areaTotalAmbiente = 0;
		}, false);
	},

	btCopiaItemOrcamento: function(button) {
		var me = this;
		var gridProdutos = me.getGridProdutos();


		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Gerar uma Cópia do Ítem: '+record.get('item_nro')+'?', function(btn){
				if(btn=='yes'){
					me.GeraCopiaItem(gridProdutos, record, button);
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar um Orçamento para poder Copiar');
			return true;
		}
	},

	GeraCopiaItem: function(grid, record, button, callbackSuccess) {
		var me = this;

		me.acoesAjax(grid, 'orcamentos_rodape', {
			action: 'COPIA_ITEM_ORCAMENTO',
			orcamento_nro: record.get('id_orcamento'),
			nro_itens: Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.getCount(),
			id_produtos_orcamentos_rodape: record.get('id_produtos_orcamentos_rodape'),
			possui_ambiente: record.get('possui_ambiente')
		}, button, function(){
			var c = setInterval(function(){
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();
				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
				clearInterval(c);
			}, 1500);
		});
	}
});