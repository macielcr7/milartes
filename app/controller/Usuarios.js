/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Usuarios', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Usuarios',

	refs: [
		{
			ref: 'list',
			selector: 'usuarioslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addusuarioswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'usuarioslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterusuarioswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterusuarioswin form'
		},
		{
			ref: 'addWin',
			selector: 'addusuarioswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelUsuarios'
	],
	stores: [
		'StoreComboPerfil',
		'StoreComboAdministradorUsuarios',
		'StoreComboStatusUsuarios',
		'StoreUsuarios'		
	],
	
	views: [
		'usuarios.List',
		'usuarios.Filtro',
		'usuarios.Edit',
		'usuarios.AlterarSenha'
	],

	init: function(application) {
		this.control({
			'usuarioslist gridpanel': {
				afterrender: this.getPermissoes,
				select: this.RegistroSelecionado,
				render: this.gridLoad
			},
			'usuarioslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'usuarioslist button[action=adicionar]': {
				click: this.add
			},
			'usuarioslist button[action=editar]': {
				click: this.btedit
			},
			'usuarioslist button[action=deletar]': {
				click: this.btdel
			},			
			'usuarioslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'usuarioslist button[action=empresas_usuarios]': {
				click: this.btEmpresa
			},
			'usuarioslist button[action=modulos]': {
				click: this.setModulos
			},
			'usuarioslist button[action=alterar_senha]': {
				click: this.trocarSenha
			},
			'usuarioslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addusuarioswin button[action=salvar]': {
				click: this.update
			},
			'addusuarioswin button[action=resetar]': {
				click: this.reset
			},
			'addusuarioswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addusuarioswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addusuarioswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterusuarioswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterusuarioswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterusuarioswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterusuarioswin': {
				show: this.filterSetFields
			}
		});
	},

	setModulos: function(button){
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			
			if(record.get('administrador')==true){
				info('Aviso!', 'Adiministradores tem todas as permissões!');
				return true;
			}
			
			me.getDesktopWindow('AddPermissoesWin', 'Permissoes', 'permissoes.Edit', function(){
				Ext.getCmp('TreePermissoes').store.proxy.extraParams = {
					action: 'USUARIO',
					usuario_id: record.get('id'),
					perfil_id: record.get('perfil_id')
				};
				
				Ext.getCmp('TreePermissoes').store.load();
			});
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
		
	},
	
	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/usuarios/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddUsuariosWin', 'Usuarios', 'usuarios.Edit', function(){
			me.getAddWin().setTitle('Alterar Dados do Usuário');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/usuarios/list.php');
			Ext.getCmp('action_usuarios').setValue('EDITAR');
			Ext.getCmp('senha_usuarios').setDisabled(true);
			Ext.getCmp('nome_usuarios').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'usuarios', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	editEmpresa: function(grid, record){
		var me = this;
		me.getDesktopWindow('List-Usuarios_Empresas_Sistema', 'Usuarios_Empresas_Sistema', 'usuarios_empresas_sistema.List', function(){
			Ext.getCmp('GridUsuarios_Empresas_Sistema').store.proxy.extraParams.id_usuario = record.get('id');
			Ext.getCmp('GridUsuarios_Empresas_Sistema').store.load();
		});
	},

	btEmpresa: function(button){
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.editEmpresa(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Desativar o Usuário: '+record.get('nome')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.desativarErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('AddUsuariosWin', 'Usuarios', 'usuarios.Edit', function(){
			Ext.getCmp('nome_usuarios').focus(false, 1000);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('usuarios', 'editar')==true){
			Ext.getCmp('button_edit_usuarios').setDisabled(false);
		}

		if(this.existPermissao('usuarios', 'deletar')==true){
			Ext.getCmp('button_del_usuarios').setDisabled(false);
		}

		if(this.existPermissao('usuarios', 'modulos')==true){
			Ext.getCmp('button_modulos_usuarios').setDisabled(false);
		}

		if(this.existPermissao('usuarios', 'empresas_usuarios')==true){
			Ext.getCmp('button_empresas_usuarios').setDisabled(false);
		}

		if(this.existPermissao('usuarios', 'alterar_senha')==true){
			Ext.getCmp('button_alterar_senha_usuarios').setDisabled(false);
		}
	},

	trocarSenha: function(button) {
		var me = this;
		var record = me.getList().getSelectionModel().getLastSelected();

		me.getDesktopWindow('TrocarSenhaUsuariosWin', 'Usuarios', 'usuarios.AlterarSenha', function(){
			me.getAddWin().setTitle('Alterar Dados do Usuário: '+record.get('nome'));
			Ext.getCmp('id_usuarios').setValue(record.get('id'));
			Ext.getCmp('senha_usuarios').focus(false, 1000);
		});
	},

	btStoreLoadFielter: function(button){
		var win = Ext.getCmp('FilterUsuariosWin');
		if(!win) win = Ext.widget('filterusuarioswin');
		win.show();
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridUsuarios').store.proxy.extraParams.status_usuario = item.value;
		Ext.getCmp('GridUsuarios').store.load({
			start: 0,
			limit: 25
		});
	}
});