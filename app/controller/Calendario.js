/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Calendario', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Calendario',
	
	refs: [],
	
	models: [],
	stores: [],
	
	views: [
		'calendario.List',
	],

	init: function(application) {
		this.control({});
	}

});