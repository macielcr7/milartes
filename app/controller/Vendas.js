/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Vendas', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},
	tabela: 'Vendas',
	started: true,
	refs: [
		{
			ref: 'list',
			selector: 'gridvendaslist'
		},
		{
			ref: 'form',
			selector: 'addvendaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridvendaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtervendaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtervendaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addvendaswin'
		}
	],
	models: [
		'ModelVendas'
	],
	stores: [
		'StoreVendas',
		'StoreItens_Orcamento',
		'StoreOrcamentos',
		'StoreComboUsuariosVendas'
	],
	views: [
		'vendas.Grid',
		'vendas.List',
		'vendas.Cancelamento',
		'vendas.Filtro'
	],

	menuGrid: Ext.create('Ext.menu.Menu', {
		id: 'menu_grid_vendas',
		width: 260,
		items: [
			{
				text: 'Imprimir - Versão Cliente',
				id: 'menuItem_imprime_cliente_vendas',
				iconCls: 'bt_imprimir2',
				action: 'impressao_cliente'
			},
			{
				text: 'Imprimir - Versão Depósito',
				id: 'menuItem_imprime_deposito_vendas',
				iconCls: 'bt_imprimir2',
				action: 'impressao_deposito'
			},
			{
				text: 'Imprimir - Versão Depósito 2',
				id: 'menuItem_imprime_deposito2_vendas',
				iconCls: 'bt_imprimir2',
				action: 'impressao_deposito2'
			},
			{
				text: 'Imprimir - Versão Completa com Pagamentos',
				id: 'menuItem_imprime_cliente_vendas_pagtos',
				iconCls: 'bt_imprimir2',
				action: 'impressao_detalhado'
			},
			{
				text: 'Converter Venda em Orçamento',
				id: 'menuItem_converte_em_orcamento_vendas',
				iconCls: 'bt_converte',
				action: 'converte_em_orcamento'
			},
			{
				text: 'Adicionar Pagamento',
				id: 'menuItem_adiciona_pagamento_vendas',
				iconCls: 'bt_totalizar_periodos_caixas',
				action: 'adicina_pagamento'
			}
		]
	}),

	init: function(application) {
		this.control({
			'#menu_grid_vendas': {
				click: this.menuClick
			},
			'gridvendaslist': {
				afterrender: this.getPermissoes2,
				render: this.gridLoad,
				select: this.RegistroSelecionado,
				itemcontextmenu: this.contextMenu
			},
			'gridvendaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter2
			},
			'gridvendaslist menuitem[action=impressao_cliente]': {
				click: this.btPrintCliente
			},
			'gridvendaslist menuitem[action=impressao_deposito]': {
				click: this.btPrintDeposito
			},
			'gridvendaslist menuitem[action=impressao_deposito2]': {
				click: this.btPrintDeposito2
			},
			'gridvendaslist menuitem[action=impressao_detalhado]': {
				click: this.btPrintDetalhado
			},
			'gridvendaslist button[action=editar]': {
				click: this.btedit
			},
			'gridvendaslist combobox[action=filtrar_usuario]': {
				select: this.btStoreLoadFiltraUsuario
			},
			'gridvendaslist button[action=deletar]': {
				//click: this.btdel
				click: this.cancelarVenda
			},
			'gridvendaslist button[action=efetua_pagamento]': {
				click: this.btPagamento
			},
			'gridvendaslist button[action=ver_custos]': {
				click: this.btCustosVenda
			},
			'gridvendaslist button[action=converte_em_orcamento]': {
				click: this.btConverteOrcamento
			},
			'addvendaswin button[action=salvar_cancelamento]': {
				click: this.updateCancelamento
			},
			/*'addvendaswin button[action=resetar]': {
				click: this.reset
			},*/
			'gridvendaslist menuitem[action=geral]': {
				click: this.addGeral
			},
			'gridvendaslist menuitem[action=forro]': {
				click: this.addForro
			},
			'gridvendaslist menuitem[action=piso]': {
				click: this.addPiso
			},
			'gridvendaslist menuitem[action=rodape]': {
				click: this.addRodape
			},
			'addvendaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addvendaswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addvendaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtervendaswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'filtervendaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtervendaswin form fieldcontainer combobox[id=estado_filter_vendas]': {
				change: this.loadCidade2
			},
			'filtervendaswin form fieldcontainer combobox[id=cidade_filter_vendas]': {
				change: this.loadBairro2
			},
			'filtervendaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtervendaswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtervendaswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	endereco: false,

	loadCidade2: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		
		var comboCidade = Ext.getCmp('cidade_filter_vendas');

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro2: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		
		if (Ext.getCmp('endereco_filter_vendas').store){
			Ext.getCmp('endereco_filter_vendas').store.proxy.extraParams.localidade_id = combo.getValue();
			if (Ext.getCmp('endereco_filter_vendas').isDisabled()){
				Ext.getCmp('endereco_filter_vendas').setDisabled(false);
			}				
		}
		return true;


		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
			}
		});
	},

	contextMenu: function(view, record, item, index, e){
		this.recordEdit = record;
		e.stopEvent();
		Ext.getCmp('menuItem_converte_em_orcamento_vendas').setVisible(false);

		if(record.get('saldo_a_receber') > 0) {
			Ext.getCmp('menuItem_adiciona_pagamento_vendas').setVisible(true);
		}
		else {
			Ext.getCmp('menuItem_adiciona_pagamento_vendas').setVisible(false);
		}


		this.menuGrid.showAt(e.getXY());
	},

	menuClick: function(menu, item, e, eOpts){
		var me = this;
		var record = this.recordEdit;
		var grid = Ext.getCmp('GridVendas');

		if(item.action=='converte_em_orcamento'){
			if (me.getList().selModel.hasSelection()) {
				me.ConverterEmOrcamento(grid, record, menu);
			}
		}
		else if(item.action=='adicina_pagamento'){
			if (me.getList().selModel.hasSelection()) {
				me.efetuaPagamentoDireto(grid, record, menu);
			}
		}
		else if(item.action=='impressao_cliente'){
			if (me.getList().selModel.hasSelection()) {
				me.btPrintCliente(grid, record, menu);
			}
		}
		else if(item.action=='impressao_detalhado'){
			if (me.getList().selModel.hasSelection()) {
				me.btPrintDetalhado(grid, record, menu);
			}
		}
		else if(item.action=='impressao_deposito'){
			if (me.getList().selModel.hasSelection()) {
				me.btPrintDeposito(grid, record, menu);
			}
		}
		else if(item.action=='impressao_deposito2'){
			if (me.getList().selModel.hasSelection()) {
				me.btPrintDeposito2(grid, record, menu);
			}
		}
	},

	getPermissoes2: function(grid, eOpts){
		var me = this;
		var data = this.application.dados[this.tabela.toLowerCase()];
		var items = grid.down('toolbar').items.items;

		Ext.each(items, function(p){
			Ext.each(data, function(j){
				if(p.action && p.action==j.acao){
					p.setVisible(true);
				}
			});
		});

		/*if (userData.perfil_id == 1 || userData.id == 5 ) {
			grid.down('toolbar').add(
				{ 
					xtype: 'tbfill' 
				},
				{
					xtype: 'combobox',
					hideLabel: true,
					store: 'StoreComboUsuariosVendas',
					valueField: 'id',
					displayField: 'descricao',
					typeAhead: true,
					queryMode: 'remote',
					triggerAction: 'all',
					emptyText:'Cadastrado Por...',
					selectOnFocus: true,
					width: 250,
					allowBlank: true,
					id: 'filtra_usuario_vendas',
					action: 'filtrar_usuario'
				}
			);
		}*/
	},

	checkDelete: function(id_orcamento){
		var me = this;
		me.deleteAjax(Ext.getCmp('GridVendas'), 'orcamentos', {
			action: 'VERIFICAR',
			id: id_orcamento
		}, false, false);
	},

	editVenda: function(grid, record, vencido) {
		var me = this;
		console.log('Editando Orçamento ID: '+record.get('id_orcamento'))
		if(record.get('tipo_orcamento')==0){
			//GERAL
			control.getController('Orcamentos').allProdutos();
			control.getController('Orcamentos').delete_orcamento = false;
			control.getController('Orcamentos').orcamentoVencido = vencido;

			me.getDesktopWindow('AddOrcamentosWin', 'Orcamentos', 'orcamentos.Edit', function(){
				//produtos
				var grid = Ext.getCmp('GridItens_Orcamento');
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();

				var form = Ext.getCmp('FormOrcamentos');
				var win  = Ext.getCmp('AddOrcamentosWin');

				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos/list.php', function(dados){
					Ext.getCmp('action_orcamentos').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos').setValue('S');
					Ext.getCmp('validade_orcamento_orcamentos').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos').setValue(record.get('aos_cuidados_de'));
					//Ext.getCmp('id_os_orcamentos').focus(false, 3500);
					
					if(record.get('eh_venda')=="S"){
						control.getController('Orcamentos').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('eh_venda_orcamentos').setReadOnly(true);
					}

					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos').setValue(record.get('cliente_descricao'));
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos').setValue(true);
						Ext.getCmp('id_cliente_orcamentos').setValue(''+record.get('id_cliente')+'');
					}

					//enderecos inicio
					Ext.getCmp('FormOrcamentos').el.mask('Aguarde...');
					var me = control.getController('Orcamentos');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentos')){
								Ext.getCmp('FormOrcamentos').el.unmask();
							}
							
							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);
					//fim enderecos....

					var count = Ext.getCmp('GridItens_Orcamento').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos').setValue(count);

					if(vencido){
						Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos').setValue(d);
					}
				});
			});
		}
		else if(record.get('tipo_orcamento')==1){
			//Forro
			control.getController('OrcamentosForro').allProdutos();
			//verificar se a linha acima, realmente precisa dela... demora muito pra carregar quando remoto

			control.getController('OrcamentosForro').delete_orcamento = false;
			control.getController('OrcamentosForro').orcamentoVencido = vencido;
			//window.StoreComboProdutosOrcamentosForro = control.getController('OrcamentosForro').getStore('StoreComboProdutos').load();

			me.getDesktopWindow('AddOrcamentosForroWin', 'OrcamentosForro', 'orcamentos_forro.Edit', function(){
				//produtos
				var grid = me.getController('OrcamentosForro').getGridProdutos();
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos_forro').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();
				
				var form = Ext.getCmp('FormOrcamentosForro');
				var win  = Ext.getCmp('AddOrcamentosForroWin');
				
				me.getController('OrcamentosForro').areaTotalAmbiente = null;
				
				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos_forro/list.php', function(dados){
					Ext.getCmp('action_orcamentos_forro').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos_forro').setValue('S');
					Ext.getCmp('validade_orcamento_orcamentos_forro').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos_forro').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos_forro').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos_forro').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos_forro').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos_forro').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos_forro').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue(record.get('aos_cuidados_de'));
					//Ext.getCmp('id_os_orcamentos_forro').focus(false, 3500);

					if(record.get('eh_venda')=="S"){
						control.getController('OrcamentosForro').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('eh_venda_orcamentos_forro').setReadOnly(true);
					}
					
					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos_forro').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos_forro').setValue(record.get('cliente_descricao'));
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos_forro').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_forro').setValue(''+record.get('id_cliente')+'');
					}

					//enderecos inicio
					Ext.getCmp('FormOrcamentosForro').el.mask('Aguarde...');
					var me = control.getController('OrcamentosForro');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos_forro').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos_forro').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentosForro')){
								Ext.getCmp('FormOrcamentosForro').el.unmask();
							}
							
							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);
					//fim enderecos....

					var count = Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_forro').setValue(count);

					if(vencido){
						Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos_forro').setValue(d);
					}
				});
			});
		}
		else if(record.get('tipo_orcamento')==2){
			//PERSIANA
			console.log('falta fazer....');
		}
		else if(record.get('tipo_orcamento')==3){
			//Piso
			control.getController('OrcamentosPiso').allProdutos();

			control.getController('OrcamentosPiso').delete_orcamento = false;
			control.getController('OrcamentosPiso').orcamentoVencido = vencido;
			//window.StoreComboProdutosOrcamentosPiso = control.getController('OrcamentosPiso').getStore('StoreComboProdutos').load();

			me.getDesktopWindow('AddOrcamentosPisoWin', 'OrcamentosPiso', 'orcamentos_piso.Edit', function(){
				//produtos
				var grid = me.getController('OrcamentosPiso').getGridProdutos();
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos_piso').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();

				var form = Ext.getCmp('FormOrcamentosPiso');
				var win  = Ext.getCmp('AddOrcamentosPisoWin');
				
				me.getController('OrcamentosPiso').areaTotalAmbiente = null;
				
				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos_piso/list.php', function(dados){
					Ext.getCmp('action_orcamentos_piso').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos_piso').setValue('S');
					Ext.getCmp('validade_orcamento_orcamentos_piso').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos_piso').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos_piso').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos_piso').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos_piso').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos_piso').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos_piso').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos_piso').setValue(record.get('aos_cuidados_de'));
					//Ext.getCmp('id_os_orcamentos_piso').focus(false, 3500);
					
					if(record.get('eh_venda')=="S"){
						control.getController('OrcamentosPiso').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('eh_venda_orcamentos_piso').setReadOnly(true);
					}

					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos_piso').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos_piso').setValue(record.get('cliente_descricao'));
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos_piso').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_piso').setValue(record.get('id_cliente').toString());
					}

					//endereco
					Ext.getCmp('FormOrcamentosPiso').el.mask('Aguarde...');
					var me = control.getController('OrcamentosPiso');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos_piso').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos_piso').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentosPiso')){
								Ext.getCmp('FormOrcamentosPiso').el.unmask();
							}
							
							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);

					var count = Ext.getCmp('GridProdutos_Orcamentos_Piso').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_piso').setValue(count);

					if(vencido){
						Ext.getCmp('button_resetar_orcamentos_piso').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_piso').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos_piso').setValue(d);
					}
				});
			});
		}
		else if(record.get('tipo_orcamento')==4){
			//Rodape
			control.getController('OrcamentosRodape').allProdutos();
			control.getController('OrcamentosRodape').delete_orcamento = false;
			control.getController('OrcamentosRodape').orcamentoVencido = vencido;
			//window.StoreComboProdutosOrcamentosRodape = control.getController('OrcamentosRodape').getStore('StoreComboProdutos').load();

			me.getDesktopWindow('AddOrcamentosRodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.Edit', function(){
				//produtos
				var grid = me.getController('OrcamentosRodape').getGridProdutos();
				grid.store.removeAll();
				grid.store.proxy.extraParams.id_orcamento = record.get('id_orcamento');
				grid.store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				grid.store.load();

				Ext.getCmp('id_cliente_orcamentos_rodape').store.load({
					params: {
						id_cliente: record.get('id_cliente')
					}
				});

				Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = record.get('id_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = record.get('tipo_orcamento');
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();

				var form = Ext.getCmp('FormOrcamentosRodape');
				var win  = Ext.getCmp('AddOrcamentosRodapeWin');
				
				me.getController('OrcamentosRodape').areaTotalAmbiente = null;
				
				me.getValuesForm(form, win, record.get('id_orcamento'), 'server/modulos/orcamentos_rodape/list.php', function(dados){
					Ext.getCmp('action_orcamentos_rodape').setValue('EDITAR');
					Ext.getCmp('eh_venda_orcamentos_rodape').setValue('S');
					Ext.getCmp('validade_orcamento_orcamentos_rodape').setValue(record.get('validade_orcamento'));
					Ext.getCmp('desconto_real_orcamentos_rodape').setValue(record.get('desconto_real'));
					Ext.getCmp('acrescimo_real_orcamentos_rodape').setValue(record.get('acrescimo_real'));
					Ext.getCmp('desconto_porcento_orcamentos_rodape').setValue(record.get('desconto_porcento'));
					Ext.getCmp('acrescimo_porcento_orcamentos_rodape').setValue(record.get('acrescimo_porcento'));
					Ext.getCmp('valor_bruto_orcamentos_rodape').setValue(record.get('valor_bruto'));
					Ext.getCmp('valor_total_orcamentos_rodape').setValue(record.get('valor_total'));
					Ext.getCmp('aos_cuidados_de_orcamentos_rodape').setValue(record.get('aos_cuidados_de'));
					//Ext.getCmp('id_os_orcamentos_rodape').focus(false, 3500);

					if(record.get('eh_venda')=="S"){
						control.getController('OrcamentosRodape').getAddWin().setTitle('Editando Venda: '+record.get('id_orcamento'));
						Ext.getCmp('eh_venda_orcamentos_rodape').setReadOnly(true);
					}
					
					if(record.get('id_cliente')==null || record.get('id_cliente')==undefined || record.get('id_cliente')==0){
						Ext.getCmp('cliente_cadastrado_orcamentos_rodape').setValue(false);
						Ext.getCmp('cliente_descricao_orcamentos_rodape').setValue(record.get('cliente_descricao'));
					}
					else{
						Ext.getCmp('cliente_cadastrado_orcamentos_rodape').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_rodape').setValue(''+record.get('id_cliente')+'');
					}

					Ext.getCmp('FormOrcamentosRodape').el.mask('Aguarde...');
					var me = control.getController('OrcamentosRodape');
					window.controlS = me;

					var a = setInterval(function(){
						if(dados.cidade != null && dados.cidade != ""){
							if(me.cidade==true){
								Ext.getCmp('cidade_orcamentos_rodape').setValue(dados.cidade);
								me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
								me.cidade = null;
							}
						} else {
								me.cidade = null;
						}

						if(dados.endereco != null && dados.endereco != ""){
							if(me.endereco==true){
								Ext.getCmp('endereco_orcamentos_rodape').setValue(parseInt(dados.endereco));
								me.endereco = null;
							}
						}else{me.endereco = null;}
						if(me.cidade == null && me.endereco == null){
							if(Ext.getCmp('FormOrcamentosRodape')){
								Ext.getCmp('FormOrcamentosRodape').el.unmask();
							}

							clearInterval(a);
							me.cidade = false;
							me.endereco = false;
						}
					}, 300);

					var count = Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_rodape').setValue(count);
					
					if(vencido){
						Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_rodape').setDisabled(false);
						var d = new Date();
						d.setDate(d.getDate() + 10);
						Ext.getCmp('validade_orcamento_orcamentos_rodape').setValue(d);
					}
				});
			});
		}
		else{
			console.info('Tipo não encontrado!');
		}
	},

	GeraCopia: function(grid, record, button, venda) {
		var me = this;
		me.acoesAjax(grid, 'orcamentos', {
			action: 'COPIA_ORCAMENTO',
			tipo_orcamento: record.get('tipo_orcamento'),
			id_orcamento: record.get('id_orcamento'),
			convertido_venda: venda
		}, button, false);
	},

	btPrintCliente: function(button) {
		console.log('1');
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/impressao.php?tipo=cliente&id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, 'Selecione uma Venda para Imprimir');
			return true;
		}
	},

	btPrintDeposito: function(button) {
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/impressao.php?tipo=deposito&id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, 'Selecione uma Venda para Imprimir');
			return true;
		}
	},

	btPrintDeposito2: function(button) {
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/impressao2.php?tipo=deposito&id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, 'Selecione uma Venda para Imprimir');
			return true;
		}
	},

	btPrintDetalhado: function(button) {
		var me = this;
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/orcamentos/impressao.php?tipo=detalhado&id_orcamento='+record.get('id_orcamento'));
		}
		else{
			info(this.titleErro, 'Selecione uma Venda para Imprimir');
			return true;
		}
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			if(record.get('total_pago') == 0 && record.get('total_adiantado') == 0) {
				this.editVenda(this.getList(), record, false);
			}
			else {
				alert('Muito Cuidado.... Existem Valores Pagos/Adiantados');
				this.editVenda(this.getList(), record, false);
			}

		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btConverteOrcamento: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Criar uma Cópia da Venda <b>'+record.get('id_orcamento')+'</b> em Orçamento?', function(btn){
				if(btn=='yes'){
					me.GeraCopia(me.getList(), record, button, 'N');
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar uma Venda para Criar um Orçamento');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	addGeral: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentos = control.getController('Orcamentos').getStore('StoreComboProdutos').load();
		control.getController('Orcamentos').delete_orcamento = true;
		me.getDesktopWindow('AddOrcamentosWin', 'Orcamentos', 'orcamentos.Edit', function(){
			var form = Ext.getCmp('FormOrcamentos');
			control.getController('Orcamentos').getAddWin().setTitle('Venda - Geral');
			Ext.getCmp('action_orcamentos').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos').setValue('S');
			Ext.getCmp('GridItens_Orcamento').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos').setValue(userID);
			Ext.getCmp('id_os_orcamentos').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);

			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'success');
						Ext.getCmp('AddOrcamentosWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridItens_Orcamento').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 0;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos').setValue(params.cliente);
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddOrcamentosWin').close();
				}
			});
		});
	},

	addForro: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentosForro = control.getController('OrcamentosForro').getStore('StoreComboProdutos').load();
		control.getController('OrcamentosForro').delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosForroWin', 'OrcamentosForro', 'orcamentos_forro.Edit', function(){
			var form = Ext.getCmp('FormOrcamentosForro');
			control.getController('OrcamentosForro').getAddWin().setTitle('Venda - Forro');
			Ext.getCmp('action_orcamentos_forro').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos_forro').setValue('S');
			Ext.getCmp('GridProdutos_Orcamentos_Forro').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos_forro').setValue(userID);
			Ext.getCmp('id_os_orcamentos_forro').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos_forro').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos_forro').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos_forro').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
			
			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos_forro/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, response.responseText, 'success');
						Ext.getCmp('AddProdutos_Orcamentos_ForroWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos_forro').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridProdutos_Orcamentos_Forro').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 1;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_forro').setValue(params.cliente);
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddProdutos_Orcamentos_ForroWin').close();
				}
			});
		});
	},

	addRodape: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentosRodape = control.getController('OrcamentosRodape').getStore('StoreComboProdutos').load();
		control.getController('OrcamentosRodape').delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosRodapeWin', 'OrcamentosRodape', 'orcamentos_rodape.Edit', function(){
			var form = Ext.getCmp('FormOrcamentosRodape');
			control.getController('OrcamentosRodape').getAddWin().setTitle('Venda - Rodapé');
			Ext.getCmp('action_orcamentos_rodape').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos_rodape').setValue('S');
			Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos_rodape').setValue(userID);
			Ext.getCmp('id_os_orcamentos_rodape').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos_rodape').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos_rodape').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos_rodape').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
			
			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos_rodape/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'success');
						Ext.getCmp('AddProdutos_Orcamentos_RodapeWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos_rodape').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridProdutos_Orcamentos_Rodape').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 4;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_rodape').setValue(params.cliente);                            
						}
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_forro').setValue(params.cliente);                            
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddProdutos_Orcamentos_RodapeWin').close();
				}
			});
		});
	},

	addPiso: function(button) {
		var me = this;
		window.StoreComboProdutosOrcamentosPiso = control.getController('OrcamentosPiso').getStore('StoreComboProdutos').load();
		control.getController('OrcamentosPiso').delete_orcamento = true;

		me.getDesktopWindow('AddOrcamentosPisoWin', 'OrcamentosPiso', 'orcamentos_piso.Edit', function(){
			var form = Ext.getCmp('FormOrcamentosPiso');
			control.getController('OrcamentosPiso').getAddWin().setTitle('Venda - Piso');
			Ext.getCmp('action_orcamentos_piso').setValue('EDITAR');
			Ext.getCmp('eh_venda_orcamentos_piso').setValue('S');
			Ext.getCmp('GridProdutos_Orcamentos_Piso').store.removeAll();
			Ext.getCmp('GridOrcamentos_Observacoes').store.removeAll();
			Ext.getCmp('id_orcamentista_orcamentos_piso').setValue(userID);
			Ext.getCmp('id_os_orcamentos_piso').focus(false, 1000);

			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_orcamentos_piso').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					var record = Ext.getCmp('cidade_orcamentos_piso').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_orcamentos_piso').setValue(record.get('id'));
					}
					me.cidade = null;
				}else{
					me.cidade = null;
				}

				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);

			form.el.mask('Aguarde, preparando dados do orçamento...');
			Ext.Ajax.request({
				url: 'server/modulos/orcamentos_piso/save.php',
				params: {
					action: 'INSERIR'
				},
				success: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, o.responseText, 'success');
						Ext.getCmp('AddProdutos_Orcamentos_PisoWin').close();
					}
					else if(dados.success==true){
						Ext.getCmp('id_orcamento_orcamentos_piso').setValue(dados.id_orcamento);
						var params = me.getList().store.proxy.extraParams;
						Ext.getCmp('GridProdutos_Orcamentos_Piso').store.proxy.extraParams.id_orcamento = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.tipo_orcamento = 3;
						Ext.getCmp('GridOrcamentos_Observacoes').store.proxy.extraParams.orcamento_id = dados.id_orcamento;
						Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						if(params && params.cliente){
							Ext.getCmp('id_cliente_orcamentos_piso').setValue(params.cliente);                            
						}
					}
					else{
						info(me.avisoText, dados.msg, 'error');
						if(dados.logout){
							window.location = 'login.php';
						}
					}
					if(form){
						form.el.unmask();
					}
				},
				failure: function(o){
					var dados = Ext.decode(o.responseText, true);
					if(dados==null){
						info(me.titleErro, response.responseText, 'error');
					}
					else if(dados.logout){
						window.location = 'login.php';
					}
					Ext.getCmp('AddProdutos_Orcamentos_PisoWin').close();
				}
			});
		});
	},

	update: function(button) {
		var me         = this;
		//var grid       = Ext.getCmp('GridOrcamentos');
		var grid       = Ext.getCmp('GridItens_Orcamento');
		var win        = Ext.getCmp('AddOrcamentosWin');
		var form       = Ext.getCmp('FormOrcamentos');

		var records         = grid.store.getModifiedRecords();
		var json_produtos  = [];
		for(var i in records){
			json_produtos.push(Ext.encode(records[i].data));
		}

		me.formParams = {
			items: [json_produtos]
		};

		me.saveForm(grid, form, win, button, function(){

		}, false);
	},

	RegistroSelecionado: function(){
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			Ext.getCmp('button_imprimir_vendas').setDisabled(false);
			Ext.getCmp('button_pagamento_vendas').setDisabled(false);
			Ext.getCmp('button_converte_vendas').setDisabled(false);
			Ext.getCmp('button_custos_venda').setDisabled(false);
			//verificar se realmente vai deixar editar/deletar
			Ext.getCmp('button_edit_vendas').setDisabled(false);
			Ext.getCmp('button_del_vendas').setDisabled(false);
		}
	},

	ConverterEmOrcamento: function(grid, record, button) {
		var me = this;
		me.acoesAjax(grid, 'orcamentos', {
			action: 'CONVERTE_EM_ORCAMENTO',
			id_orcamento: record.get('id_orcamento')
		}, button, false);
	},

	btPagamento: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.efetuaPagamentoGrid(this.getList(), record);
		}
		else{
			info(this.titleErro, 'Selecione um Ítem para Efetuar o Pagamento');
			return true;
		}
	},

	efetuaPagamentoGrid: function(grid, record) {
		var me = this;
		me.getDesktopWindow('List-Vendas_Pagamentos', 'Vendas_Pagamentos', 'vendas_pagamentos.List', function(controller){
			Ext.getCmp('List-Vendas_Pagamentos').setTitle('Pagamentos Realizados para a Venda: '+record.get('id_orcamento'));
			controller.getList().store.proxy.extraParams.cod_venda = record.get('id_orcamento');
			controller.getList().store.proxy.extraParams.id_cliente = record.get('id_cliente');
			controller.getList().store.proxy.extraParams.saldo_a_receber = record.get('saldo_a_receber');
			controller.getList().store.proxy.extraParams.venda_finalizada = record.get('venda_finalizada');

			if (record.get('saldo_a_receber') == 0) {
				Ext.getCmp('button_add_vendas_pagamentos').setDisabled(true);
			}

			if (record.get('venda_finalizada') == 'S') {
				Ext.getCmp('button_estorna_vendas_pagamentos').setDisabled(true);
			}
		});
	},

	efetuaPagamentoDireto: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddVendas_PagamentosWin', 'Vendas_Pagamentos', 'vendas_pagamentos.Edit', function(controller){
			Ext.getCmp('AddVendas_PagamentosWin').setTitle('Adicionando Pagamento para Venda Nro: '+record.get('id_orcamento'));
			Ext.getCmp('cod_venda_vendas_pagamentos').setValue(record.get('id_orcamento'));
			Ext.getCmp('cod_cliente_vendas_pagamentos').setValue(record.get('id_cliente'));
			Ext.getCmp('saldo_a_receber_vendas_pagamentos').setValue(record.get('saldo_a_receber'));
		});
	},

	btCustosVenda: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.gridCustosVenda(this.getList(), record);
		}
		else{
			info(this.titleErro, 'Selecione um Ítem para Efetuar o Pagamento');
			return true;
		}
	},

	gridCustosVenda: function(grid, record) {
		var me = this;
		me.getDesktopWindow('List-Vendas_Custos', 'Vendas_Custos', 'vendas_custos.List', function(controller){
			Ext.getCmp('List-Vendas_Custos').setTitle('Custos para a Venda: '+record.get('id_orcamento'));
			controller.getList().store.proxy.extraParams.cod_venda = record.get('id_orcamento');
			controller.getList().store.proxy.extraParams.id_cliente = record.get('id_cliente');
			controller.getList().store.proxy.extraParams.saldo_a_receber = record.get('saldo_a_receber');
			controller.getList().store.proxy.extraParams.venda_finalizada = record.get('venda_finalizada');
		});
	},

	cancelarVenda: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			if(record.get('total_pago') == 0 && record.get('total_adiantado') == 0) {
				me.getDesktopWindow('Cancelamento-Vendas', 'Vendas', 'vendas.Cancelamento', function(controller){
					me.getAddWin().setTitle('Cancelamento da Venda: '+record.get('id_orcamento'));
					Ext.getCmp('id_orcamentos_vendas').setValue(record.get('id_orcamento'));
				});
			}
			else{
				Ext.Msg.alert('Aviso', 'Existe Valores Pagos para esta venda... Estorne os Valores para poder Cancelar a Venda');
			}
		}
		else{
			info(me.titleErro, me.delErroGrid);
			return true;
		}
	},

	updateCancelamento: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFiltraUsuario: function(){
		this.getList().store.proxy.extraParams.cadastrado_por = Ext.getCmp('filtra_usuario_vendas').getValue();
		this.getList().store.proxy.extraParams.start = 0;
		this.getList().store.proxy.extraParams.limit = 100;
		this.getList().store.loadPage(1);
	},

	/*
	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.vendas.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},
	*/

	btStoreLoadFielter2: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.vendas.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();

		var me = this;
		var empresa = me.getCurrentEmpresa();
		Ext.getCmp('estado_filter_vendas').setValue(empresa.estado);
		var a = setInterval(function(){
			if(empresa.cidade != null && empresa.cidade != ""){
				if(me.cidade==true){
					var record = Ext.getCmp('cidade_filter_vendas').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_filter_vendas').setValue(record.get('id'));
					}
					me.cidade = null;
				}
			} 
			else {
				me.cidade = null;
			}
			if(me.cidade == null){
				clearInterval(a);
			}
		}, 1000);
	}
});