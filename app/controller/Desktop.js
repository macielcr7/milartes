/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Desktop', {
	extend: 'Ext.app.Controller',
	mixins: {
		iconsDesk		: 'ShSolutions.plugins.IconDesktop',
		winRegisterDesk : 'ShSolutions.plugins.winRegisterDesktop',
		winTaskBar		: 'ShSolutions.plugins.TaskBar',
		notify			: 'ShSolutions.plugins.Alert',
		contextMenuDesk : 'ShSolutions.plugins.ContextMenuDesktop'
	},

	requires: [
		'ShSolutions.plugins.IconDesktop',
		'ShSolutions.plugins.ContextMenuDesktop',
		'ShSolutions.plugins.TaskBar',
		'ShSolutions.plugins.winRegisterDesktop'
	],

	views: [
		'Desktop'
	],
	body: null,
	ContainerDesktop: null,

	menuStart: [],

	init: function(application) {
		var me = this;
		me.control({
			'desktopwin': {
				afterrender: me.renderDesktop
			},
			'desktopwin button[id=view-desktop]': {
				click: me.hideAllVWindows,
				mouseover: me.delOpacityAllWin,
				mouseout: me.addOpacityAllWin,
			},
			'#view-empresa-menu': {
				click: me.alterEmpresa
			}
		});
		window.control = me;
	},

	alterEmpresa: function(btn, item){
		var id = item.id.replace('view-empresa-item-', '');
		Ext.getCmp('view-empresa-item-'+Ext.util.Cookies.get('id_empresa_atual')).setIconCls('');
		Ext.util.Cookies.set('id_empresa_atual', id);
		Ext.util.Cookies.set('descricao_empresa_atual', item.text);
		item.setIconCls('view-empresa');
	},

	ajaxDesktopIcons: function(){
		var me = this;
		
		Ext.Ajax.request({
			url: 'server/modulos/menu.php',
			method: 'GET',
			params: {
				action: 'LIST_ALL_PREFERENCES'
			},
			success: function(o){
				var d = Ext.decode(o.responseText);
				if(d.success==true){
					if(d.icon_desktop.length>0){
						for(var i=0;i<d.icon_desktop.length;i++){
							me.addIconDesktop({
								id: d.icon_desktop[i].id,
								name: d.icon_desktop[i].descricao,
								iconCls: d.icon_desktop[i].iconCls+'-shortcut',
								tooltip: d.icon_desktop[i].descricao,
								Controller: d.icon_desktop[i].controller,
								className:  d.icon_desktop[i].className
							});
						}
					}
					
					if(d.menu_start.length>0){
						for(var i=0;i<d.menu_start.length;i++){
							Ext.getCmp('startmenu-desktop').addMenuItem({
								xtype: 'menuitem',
								text: d.menu_start[i].descricao,
								iconCls: d.menu_start[i].iconCls,
								
								model_id: d.menu_start[i].id,
								model_ctl: d.menu_start[i].controller,
								model_cls: d.menu_start[i].className,
								model_me: me,
								handler: function(){
									this.model_me.getWindow(this.model_id, this.model_ctl, this.model_cls);
								}
							});
						}	
					}
					//se puder melhorar o código abaixo para carregar mais rápido.... e nao consegui fazer minimizar a janela...
					me.addIconDesktop({
						id: 'List-Calendario',
						name: 'Calendário',
						iconCls: 'calendario-shortcut',
						tooltip: 'Calend&aacute;rio',
						Controller: 'Calendario',
						className:  'calendario.List'
					});

					me.addIconDesktop({
						id: 'List-WebMail',
						name: 'Web Mail',
						iconCls: 'webmail-shortcut',
						tooltip: 'Web Mail',
						execCallbackOnly: true,
						callback: function(){
							window.open('../gestao/webmail/index.php?user_mail='+webmailEmail+'&pw_mail='+webmailSenha);
						}
					});

					me.addIconDesktop({
						id: 'List-Diversos',
						name: 'Diversos',
						iconCls: 'diversos-shortcut',
						tooltip: 'Diversos',
						execCallbackOnly: true,
						callback: function(){
							window.open('temp/diversos.php');
						}
					});
				}
			}
		});
	},
	
	initDesktopMenu: function(comp){
		var me = this;
		me.contextMenu = new Ext.menu.Menu(me.createDesktopMenu(comp));
		
		comp.el.on('contextmenu', function(event){
			//if($(event.target).parents('.x-grid-row').length==0){
				if(!me.contextMenu.rendered){
					me.contextMenu.on('beforeshow', me.onDesktopMenuBeforeShow);
				}
				event.preventDefault();
				me.contextMenu.showAt(event.getXY());
				me.contextMenu.doConstrain();				
			//}
		});
	},
	
	hideMask: function(){
		Ext.get('loading').hide();
		Ext.get('loading-mask').setOpacity(0, true);
		setTimeout(function(){
			Ext.get('loading-mask').hide();
		},800);
	},
	
	renderDesktop: function(comp, objs){
		var me = this;
		
		me.hideMask();
		
		me.body = Ext.getBody();
		me.ContainerDesktop = comp;
		me.windows = new Ext.util.MixedCollection();
		me.windowBar = Ext.getCmp('toolbar-button-win');
		me.winquickBar = Ext.getCmp('toolbar-quick-win');
		me.startMenu = Ext.getCmp('startmenu-desktop');
		me.windowMenu = new Ext.menu.Menu(me.createWindowMenu());
		me.iconMenu = new Ext.menu.Menu(me.createIconMenu());

		me.initColRow();
		me.initDesktopMenu(comp);
		me.ajaxDesktopIcons();
		me.afterLayout();

		if(window.empresa_sistemaAll!= null && window.empresa_sistemaAll.length>1){
			for(var i=0;i<window.empresa_sistemaAll.length;i++){
				if(window.empresa_sistemaAll[i].id!=Ext.util.Cookies.get('id_empresa_atual')){
					Ext.getCmp('view-empresa-menu').add({
						text: window.empresa_sistemaAll[i].descricao,
						id: 'view-empresa-item-'+ window.empresa_sistemaAll[i].id,
					});
				}
			}
		}
		
		Ext.EventManager.onWindowResize(me.updateIconDesktopSize, this, {
			delay:500
		});
	}
});