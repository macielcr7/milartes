/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Agenda_Telefonica', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Agenda_Telefonica',

	refs: [
		{
			ref: 'list',
			selector: 'agenda_telefonicalist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addagenda_telefonicawin form'
		},
		{
			ref: 'filterBtn',
			selector: 'agenda_telefonicalist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteragenda_telefonicawin'
		},
		{
			ref: 'filterForm',
			selector: 'filteragenda_telefonicawin form'
		},
		{
			ref: 'addWin',
			selector: 'addagenda_telefonicawin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelAgenda_Telefonica'
	],
	stores: [
		'StoreAgenda_Telefonica_Contato',
		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboCorreios_Bairros',
		'StoreComboCorreios_Enderecos',
		'StoreComboAtivo',
		'StoreComboUsuarios',
		'StoreComboProfissaoAgenda_Telefonica',
		'StoreComboSexo',
		'StoreAgenda_Telefonica'
	],

	views: [
		'agenda_telefonica.List',
		'agenda_telefonica.Edit'
	],

	init: function(application) {
		this.control({
			'agenda_telefonicalist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'agenda_telefonicalist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'agenda_telefonicalist button[action=adicionar]': {
				click: this.add
			},
			'agenda_telefonicalist button[action=editar]': {
				click: this.btedit
			},
			'agenda_telefonicalist button[action=deletar]': {
				click: this.btdel
			},
			'addagenda_telefonicawin tabpanel': {
				tabchange: this.loadActive
			},
			'addagenda_telefonicawin button[id=button_salvar_agenda_telefonica]': {
				click: this.update
			},
			'addagenda_telefonicawin button[id=button_resetar_agenda_telefonica]': {
				click: this.reset
			},
			'addagenda_telefonicawin form fieldcontainer combobox': {
				change: this.enableButton,
				afterrender: this.comboLoad
			},
			'addagenda_telefonicawin form fieldcontainer combobox[id=estado_agenda_telefonica]': {
				change: this.loadCidade
			},
			'addagenda_telefonicawin form fieldcontainer combobox[id=cidade_agenda_telefonica]': {
				change: this.loadBairro
			},
			'addagenda_telefonicawin form fieldcontainer combobox[id=endereco_agenda_telefonica]': {
				change: this.setEndereco
			},
			'addagenda_telefonicawin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addagenda_telefonicawin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			}
		});
	},

	cidade: false,
	endereco: false,

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			
			Ext.getCmp('bairro_nome_agenda_telefonica').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_agenda_telefonica').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_agenda_telefonica').setValue(record.get('loteamento'));
			}

			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_agenda_telefonica').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_agenda_telefonica').setValue(record.get('cep'));
			Ext.getCmp('nro_end_agenda_telefonica').focus(false, 100);
		}
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('cidade_agenda_telefonica');
		}
		else{
			var comboCidade = Ext.getCmp('cidade_filter_agenda_telefonica');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			Ext.getCmp('endereco_agenda_telefonica').store.proxy.extraParams.localidade_id = combo.getValue();
			me.bairro = true;
			if(Ext.getCmp('endereco_agenda_telefonica').isDisabled()){
				Ext.getCmp('endereco_agenda_telefonica').setDisabled(false);
			}
			return true;
		}
		else{
			var comboCidade = Ext.getCmp('bairro_filter_agenda_telefonica');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_agenda_telefonica');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_agenda_telefonica');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_agenda_telefonica');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		
		me.getDesktopWindow('AddAgenda_TelefonicaWin', 'Agenda_Telefonica', 'agenda_telefonica.Edit', function(dados){
			me.getAddWin().setTitle('Alteração do Cadastro do Contato');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_contato'), 'server/modulos/agenda_telefonica/list.php', function(dados){
				me.getForm().el.mask('Aguarde...');
				window.controlS = me;
				
				var a = setInterval(function(){
					if(dados.cidade != null && dados.cidade != ""){
						if(me.cidade==true){
							Ext.getCmp('cidade_agenda_telefonica').setValue(dados.cidade);
							me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
							me.cidade = null;
						}
					} else {
							me.cidade = null;
					}

					if(dados.endereco != null && dados.endereco != ""){
						if(me.endereco==true){
							Ext.getCmp('endereco_agenda_telefonica').setValue(parseInt(dados.endereco));
							me.endereco = null;
						}
					}else{me.endereco = null;}
					if(me.cidade == null && me.endereco == null){
						if(me.getForm()){
							me.getForm().el.unmask();
						}
						
						clearInterval(a);
						me.cidade = false;
						me.endereco = false;
					}
				}, 300);
			});
			
			Ext.getCmp('action_agenda_telefonica').setValue('EDITAR');
			me.hability(record.get('tipo_contato'));
			
			Ext.getCmp('GridAgenda_Telefonica_Contato').setDisabled(false);
			Ext.getCmp('cadastrado_por_agenda_telefonica').setVisible(true);
			Ext.getCmp('alterado_por_agenda_telefonica').setVisible(true);
			Ext.getCmp('GridAgenda_Telefonica_Contato').store.proxy.extraParams.cod_contato = record.get('cod_contato');

			
			var data = me.application.dados[me.tabela.toLowerCase()];
			
			Ext.each(data, function(j){
//				if(j.acao=='cliente_logs'){
//					Ext.getCmp('GridLogs_Sistema').setDisabled(false);
//					Ext.getCmp('GridLogs_Sistema').store.proxy.extraParams.tabela_cod = record.get('cod_cliente');
//					Ext.getCmp('GridLogs_Sistema').store.proxy.extraParams.modulo = 'Clientes';
//				}
			});
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'agenda_telefonica', {
			action: 'DELETAR',
			id: record.get('cod_contato')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			if(record.get('tipo_contato')=='F'){
				var contato = record.get('nome_completo');				
			}
			else{
				var contato = record.get('razao_social');
			}
			Ext.Msg.confirm('Confirmar', 'Deseja Apagar o Contato: '+contato+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	add: function(button) {
		var me = this;
		window.App = me;
		me.cidade = false;

		me.getDesktopWindow('Agenda_Telefonica', 'Agenda_Telefonica', 'agenda_telefonica.Edit', function(){
			me.hability(button.type);
			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_agenda_telefonica').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					if(me.cidade==true){
						var record = Ext.getCmp('cidade_agenda_telefonica').store.findRecord('descricao', empresa.cidade);
						if(record){
							Ext.getCmp('cidade_agenda_telefonica').setValue(record.get('id'));
						}
						me.cidade = null;
					}
				}else{me.cidade = null;}
				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
			
			Ext.getCmp('GridAgenda_Telefonica_Contato').setDisabled(true);
		});
	},

	hability: function(type){
		if(type=='J'){
			Ext.getCmp('fieldcontainer_jAgendaTelefone').setVisible(true);

			Ext.getCmp('razao_social_agenda_telefonica').setVisible(true);
			Ext.getCmp('nome_fantasia_agenda_telefonica').setVisible(true);

			Ext.getCmp('razao_social_agenda_telefonica').setDisabled(false);
			Ext.getCmp('nome_fantasia_agenda_telefonica').setDisabled(false);
			Ext.getCmp('pessoa_contato_agenda_telefonica').setDisabled(false);

			Ext.getCmp('fieldcontainer_fAgendaTelefone').setVisible(false);
			Ext.getCmp('nome_completo_agenda_telefonica').setVisible(false);
			Ext.getCmp('profissao_agenda_telefonica').setVisible(false);

			Ext.getCmp('nome_completo_agenda_telefonica').setDisabled(true);
			Ext.getCmp('profissao_agenda_telefonica').setDisabled(true);
			Ext.getCmp('sexo_agenda_telefonica').setDisabled(true);

			Ext.getCmp('razao_social_agenda_telefonica').focus(false, 1000);
			Ext.getCmp('tipo_contato_agenda_telefonica').setValue('J');
		}
		else{
			Ext.getCmp('fieldcontainer_jAgendaTelefone').setVisible(false);
			Ext.getCmp('razao_social_agenda_telefonica').setVisible(false);
			Ext.getCmp('nome_fantasia_agenda_telefonica').setVisible(false);

			Ext.getCmp('razao_social_agenda_telefonica').setDisabled(true);
			Ext.getCmp('nome_fantasia_agenda_telefonica').setDisabled(true);
			Ext.getCmp('pessoa_contato_agenda_telefonica').setDisabled(true);


			Ext.getCmp('fieldcontainer_fAgendaTelefone').setVisible(true);
			Ext.getCmp('nome_completo_agenda_telefonica').setVisible(true);
			Ext.getCmp('profissao_agenda_telefonica').setVisible(true);

			Ext.getCmp('nome_completo_agenda_telefonica').setDisabled(false);
			Ext.getCmp('profissao_agenda_telefonica').setDisabled(false);
			Ext.getCmp('sexo_agenda_telefonica').setDisabled(false);

			Ext.getCmp('nome_completo_agenda_telefonica').focus(false, 1000);
			Ext.getCmp('tipo_contato_agenda_telefonica').setValue('F');
		}
	},

	winAddNoClose: true,

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(dados){

			if(Ext.getCmp('action_agenda_telefonica').getValue()=='INSERIR'){
				Ext.getCmp('action_agenda_telefonica').setValue('EDITAR');

				Ext.getCmp('GridAgenda_Telefonica_Contato').setDisabled(false);
				Ext.getCmp('GridAgenda_Telefonica_Contato').store.proxy.extraParams.cod_contato = dados.cod_contato;
				Ext.getCmp('GridAgenda_Telefonica_Contato').store.load();
				Ext.getCmp('TabPanelCentralAgendaTelefonica').setActiveTab(1);
			}

		}, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('agenda_telefonica', 'editar')==true){
			Ext.getCmp('button_edit_agenda_telefonica').setDisabled(false);
		}

		if(this.existPermissao('agenda_telefonica', 'deletar')==true){
			Ext.getCmp('button_del_agenda_telefonica').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_agenda_telefonica').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	}
});