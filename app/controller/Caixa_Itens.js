/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Caixa_Itens', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Caixa_Itens',
	started: true,

	refs: [
		{
			ref: 'list',
			selector: 'caixa_itenslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcaixa_itenswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'caixa_itenslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercaixa_itenswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercaixa_itenswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcaixa_itenswin'
		},
		{
			ref: 'list2',
			selector: 'caixa_itenslist'
		}
	],

	models: [
		'ModelCaixa_Itens'
	],

	stores: [
		'StoreCaixa_Itens',
		'StoreComboTipoLancamentoCaixaItens',
		'StoreCaixaItensVendas',
		'StoreCaixaItensRetiradas',
		'StoreCaixaItensAdiantamentos',
		'StoreCaixaItensMeiosPagtos',
		'StoreCaixaItensSuprimentos'
	],

	views: [
		'caixa_itens.List',
		'caixa_itens.Filtro',
		//'caixa_itens.Edit',
		'caixa_itens.AdicionaValores',
		'caixa_itens.Totais',
		'caixa_itens.FechaCaixa',
		'caixa_itens.AbreCaixa',
		'caixa_itens.RetiraValores',
		'caixa_itens.Vendas',
		'caixa_itens.Retiradas',
		'caixa_itens.Adiantamentos',
		'caixa_itens.MeiosPagamentos',
		'caixa_itens.AlteraValoresVendas',
		'caixa_itens.Suprimentos'
	],

	init: function(application) {
		this.control({
			'caixa_itenslist gridpanel': { 
				afterrender: this.getPermissoes2,
				render: this.loadGrid,
				select: this.RegistroSelecionado/*,
				cellclick: this.clickCelula*/
			},
			'caixa_itenslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'caixa_itenslist button[action=editar]': {
				click: this.btedit
			},
			'caixa_itenslist button[action=add_din_caixa]': {
				click: this.adicionaDin
			},
			'caixa_itenslist button[action=retirar_din_caixa]': {
				click: this.retiraDin
			},
			'caixa_itenslist button[action=abrir_caixa]': {
				click: this.abrirCaixa
			},
			'caixa_itenslist button[action=fechar_caixa]': {
				click: this.fechaCaixa
			},
			'caixa_itenslist button[action=ver_totais]': {
				click: this.conferirTotais
			},
			'caixa_itenslist button[action=deletar]': {
				click: this.btdel
			},
			'addcaixa_itenswin button[action=salvar]': {
				click: this.update
			},
			'addcaixa_itenswin button[action=resetar]': {
				click: this.reset
			},
			'addcaixa_itenswin tabpanel': {
				tabchange: this.loadActive
			},
			'addcaixa_itenswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcaixa_itenswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcaixa_itenswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addcaixa_itenswin form combobox[id=forma_pagto_valores_caixa_itens], addcaixa_itenswin form combobox[id=forma_pagto_venda_caixa_itens], addcaixa_itenswin form combobox[id=forma_pagto_retirada_caixa_itens]': {
				change: this.changeQtdeParcelas
			},
			'filtercaixa_itenswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercaixa_itenswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercaixa_itenswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercaixa_itenswin': {
				show: this.filterSetFields
			}
		});
	},

	changeQtdeParcelas: function(combo, value){
		var me = this;
		var record = combo.store.getById(value);
		var record2 = me.getList().getSelectionModel().getLastSelected();
		var campo = "";
		var campo2 = "";
		//console.log(record2.get('nro_parcelas_cartao'));

		if (combo.id == "forma_pagto_valores_caixa_itens") {
			campo = "nro_parcelas_valores_caixa_itens";
			campo2 = "data_venda_cartao_valores_caixa_itens";

		}
		else if (combo.id == "forma_pagto_venda_caixa_itens") {
			campo = "nro_parcelas_venda_caixa_itens";
			campo2 = "data_venda_cartao_caixa_itens";
		}
		else if (combo.id == "forma_pagto_retirada_caixa_itens") {
			campo = "nro_parcelas_retirada_caixa_itens";
			campo2 = "data_venda_cartao_retirada_caixa_itens";
		}

		if (value!=null && !isNaN(parseInt(value)))	{
			var max_cartao = parseInt(record.raw.max_cartao);
			var comboNroParcelas = Ext.getCmp(campo);
			var comboDataCartao = Ext.getCmp(campo2);

			if (!isNaN(max_cartao)) {
				comboNroParcelas.setDisabled(false);
				comboNroParcelas.setVisible(true);
				comboNroParcelas.setValue(1);
				comboNroParcelas.setMaxValue(max_cartao);
				comboDataCartao.setDisabled(false);
				comboDataCartao.setVisible(true);
			}
			else {
				comboNroParcelas.reset();
				comboNroParcelas.setDisabled(true);
				comboNroParcelas.setVisible(false);

				comboDataCartao.reset();
				comboDataCartao.setDisabled(true);
				comboDataCartao.setVisible(false);
			}
		}
	},

	getPermissoes2: function(grid){
		var me = this;
		var data = this.application.dados[this.tabela.toLowerCase()];
		var items = grid.down('toolbar').items.items;

		Ext.each(items, function(p){
			Ext.each(data, function(j){
				if(p.action && p.action==j.acao){
					p.setVisible(true);
				}
			});
		});
	},

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.loadPage(1);
		}
	},

	VerificaCaixaAberto: function(){
		var me = this;
		console.info('verificando se o caixa está aberto...');
		Ext.Ajax.request({
			url : 'server/modulos/caixa_itens/list.php',
			params : {
				action: 'USER_CAIXA_ATUAL',
				user : userID
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'Errox', 'information');
				}
				else if(resposta.success==true){
					NroCaixaDesejado = resposta.dados[0].caixa_atual;
					NroCaixaDesejado2 = resposta.dados[0].caixa_atual;
					console.log('Caixa Atal do Usuário: '+NroCaixaDesejado);

					if (NroCaixaDesejado == 0) {
						me.getList2().setTitle('O Caixa Está Fechado...');
						console.log('O Caixa Está Fechado...');
						
						if(me.existPermissao('caixa_itens', 'abrir_caixa')==true){
							Ext.getCmp('button_abrir_caixa').setVisible(true);
							Ext.getCmp('button_abrir_caixa').setDisabled(false);
						}
						Ext.getCmp('button_fechar_caixa').setDisabled(true);
						Ext.getCmp('button_suprimento_caixa').setDisabled(true);
						Ext.getCmp('button_retirada_caixa').setDisabled(true);
						Ext.getCmp('button_edit_caixa_itens').setDisabled(true);
						Ext.getCmp('button_del_caixa_itens').setDisabled(true);
						Ext.getCmp('button_total_caixa_itens').setDisabled(true);

						Ext.getCmp('button_fechar_caixa').setVisible(false);
						Ext.getCmp('button_suprimento_caixa').setVisible(false);
						Ext.getCmp('button_retirada_caixa').setVisible(false);
						Ext.getCmp('button_edit_caixa_itens').setVisible(false);
						Ext.getCmp('button_del_caixa_itens').setVisible(false);
						Ext.getCmp('button_total_caixa_itens').setVisible(false);
					}
					else {
						console.log('O Caixa está Aberto...')
						me.getList2().setTitle('Conferindo Valores do Caixa: '+NroCaixaDesejado);
						if(me.existPermissao('caixa_itens', 'abrir_caixa')==true){
							Ext.getCmp('button_abrir_caixa').setDisabled(true);
							Ext.getCmp('button_abrir_caixa').setVisible(false);
						}
						if(me.existPermissao('caixa_itens', 'fechar_caixa')==true){
							Ext.getCmp('button_fechar_caixa').setDisabled(false);
							Ext.getCmp('button_fechar_caixa').setVisible(true);
						}
						if(me.existPermissao('caixa_itens', 'add_din_caixa')==true){
							Ext.getCmp('button_suprimento_caixa').setDisabled(false);
							Ext.getCmp('button_suprimento_caixa').setVisible(true);
						}
						if(me.existPermissao('caixa_itens', 'retirar_din_caixa')==true){
							Ext.getCmp('button_retirada_caixa').setDisabled(false);
							Ext.getCmp('button_retirada_caixa').setVisible(true);
						}
						if(me.existPermissao('caixa_itens', 'editar')==true){
							Ext.getCmp('button_edit_caixa_itens').setDisabled(false);
							Ext.getCmp('button_edit_caixa_itens').setVisible(true);
						}
						if(me.existPermissao('caixa_itens', 'deletar')==true){
							Ext.getCmp('button_del_caixa_itens').setDisabled(false);
							Ext.getCmp('button_del_caixa_itens').setVisible(true);
						}
						if(me.existPermissao('caixa_itens', 'ver_totais')==true){
							Ext.getCmp('button_total_caixa_itens').setDisabled(false);
							Ext.getCmp('button_total_caixa_itens').setVisible(true);
						}
					}
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});

		setTimeout(function(){

		}, 300);
	},

	abrirCaixa: function(button) {
		var me = this;
		me.getDesktopWindow('Caixa_Itens', 'Caixa_Itens', 'caixa_itens.AbreCaixa', function(){
		});
	},

	fechaCaixa: function(button) {
		var me = this;
		me.getDesktopWindow('Caixa_Itens', 'Caixa_Itens', 'caixa_itens.FechaCaixa', function(){
			me.getAddWin().setTitle('Fechamento do Caixa #: '+NroCaixaDesejado2);
			Ext.getCmp('cx_nro_caixa').setValue(parseInt(NroCaixaDesejado2));
			Ext.getCmp('action_caixa').setValue('FECHAR');
		});
	},

	edit: function(grid, record) {
		var me = this;
		//0=Suprimento, 1=Retirada, 2=Venda, 3=Adiantamento, 4=Devolução

		if(record.get('tipo_lancamento')==0){
			//0=Suprimentos
			if (record.get('categoria')==3){
				Ext.Msg.alert('Aviso', 'Valores de Cartão de Crédito/Débito/Boleto, devem ser Estornado/Cancelado ou falar com Anderson');
			}
			else {
				me.getDesktopWindow('AddDinCaixaWin', 'Caixa_Itens', 'caixa_itens.AdicionaValores', function(){
					me.getAddWin().setTitle('Alteração de Valores do Caixa: Suprimento');
					me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/caixa_itens/list.php');
					Ext.getCmp('action_valores_caixa_itens').setValue('EDITAR_VALORES_SUPRIMENTO');

					if (userData.perfil_id == 1) {
						Ext.getCmp('cx_nro_valores_caixa_itens').setVisible(true);
					}
				});
			}
		}
		else if(record.get('tipo_lancamento')==1){
			//1=Retiradas
			if (record.get('categoria')==3){
				Ext.Msg.alert('Aviso', 'Valores de Cartão de Crédito/Débito/Boleto, devem ser Estornado/Cancelado ou falar com Anderson');
			}
			else {
				me.getDesktopWindow('RetiraDinCaixaWin', 'Caixa_Itens', 'caixa_itens.RetiraValores', function(){
					me.getAddWin().setTitle('Alteração de Valores do Caixa: Retirada');
					me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/caixa_itens/list.php');
					Ext.getCmp('action_retirada_caixa_itens').setValue('EDITAR_VALORES_RETIRADA');
					Ext.getCmp('valor_retirada_caixa_itens').setValue(Math.abs(record.get('valor')));

					if (userData.perfil_id == 1) {
						Ext.getCmp('cx_nro_retirada_caixa_itens').setVisible(true);
					}
				});
			}
		}
		else if(record.get('tipo_lancamento')==2){
			//2=Vendas
			//Altera Valor e Forma de Pagamento
			if (record.get('categoria')==3){
				Ext.Msg.alert('Aviso', 'Valores de Cartão de Crédito/Débito/Boleto, devem ser Alterados/Cancelados pelo Menu de Vendas');
			}
			else {
				me.getDesktopWindow('AlteraValoresVendasWin', 'Caixa_Itens', 'caixa_itens.AlteraValoresVendas', function(){
					me.getAddWin().setTitle('Alteração de Valor da Venda ou Forma de Pagamento');
					me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/caixa_itens/list.php');
					Ext.getCmp('action_venda_caixa_itens').setValue('ALTERA_VALOR_VENDA');
					Ext.getCmp('valor_venda_caixa_itens').setValue(Math.abs(record.get('valor')));

					if (userData.perfil_id == 1) {
						Ext.getCmp('cx_nro_venda_caixa_itens').setVisible(true);
					}
				});
			}
		}
		else if(record.get('tipo_lancamento')==3){
			//3=Adiantamentos
			Ext.Msg.alert('Aviso', 'Valores de Adiantamentos, devem ser Alterados/Cancelados pelo Menu de Clientes / Adiantamentos. Pois pode haver algum vínculo já efetuado.');
		}
		else{
			console.info('Tipo não encontrado!');
		}
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'caixa_itens', {
			action: 'CANCELAR_ITEM',
			id_item: record.get('id')
		}, button, function(){
			Ext.getCmp('button_edit_caixa_itens').setDisabled(true);
			Ext.getCmp('button_del_caixa_itens').setDisabled(true);

			setTimeout(function(){
				me.VerificaCaixaAberto();
			},260);
		});
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			if(record.get('tipo_lancamento')==0){
				//2=Suprimento
				var valor = converteFloatMoeda(record.get('valor'));
				Ext.Msg.confirm('Confirmar', 'Deseja Cancelar o Suprimento de ID: '+record.get('id')+' no Valor de R$ '+valor+'?', function(btn){
					if(btn=='yes'){
						me.del(me.getList(), record, button);
					}
				});
			}
			else if(record.get('tipo_lancamento')==1){
				//1=Retiradas
				var valor = Math.abs(record.get('valor'));
				valor = converteFloatMoeda(valor);
				Ext.Msg.confirm('Confirmar', 'Deseja Cancelar a Retirada de ID: '+record.get('id'), function(btn){
					if(btn=='yes'){
						me.del(me.getList(), record, button);
					}
				});
			}
			else if(record.get('tipo_lancamento')==2){
				//2=Vendas
				Ext.Msg.alert('Aviso', 'Valores de Vendas, devem ser Alterados/Cancelados pelo Menu de Vendas');
			}
			else if(record.get('tipo_lancamento')==3){
				//3=Adiantamentos
				Ext.Msg.alert('Aviso', 'Valores de Adiantamentos, devem ser Alterados/Cancelados pelo Menu de Clientes / Adiantamentos');
			}
			else{
				Ext.Msg.alert('Aviso', 'Tipo de Lançamento Não Localizado');
			}
		}
		else{
			info(me.titleErro, 'Selecine um Ítem para Cancelar');
			return true;
		}
	},

	adicionaDin: function(button){
		var me = this;
		me.getDesktopWindow('Caixa_Itens', 'Caixa_Itens', 'caixa_itens.AdicionaValores', function(controller){
			var caixa = NroCaixaDesejado2;
			me.getAddWin().setTitle('Adicionando Suprimento no Caixa: '+NroCaixaDesejado2);
			Ext.getCmp('cx_nro_valores_caixa_itens').setValue(parseInt(NroCaixaDesejado2));
			Ext.getCmp('action_valores_caixa_itens').setValue('ADD_VALORES');
			Ext.getCmp('forma_pagto_valores_caixa_itens').setValue("1");

			if (userData.perfil_id == 1) {
				Ext.getCmp('cx_nro_valores_caixa_itens').setVisible(true);
			}
		});
	},

	retiraDin: function(button){
		var me = this;
		me.getDesktopWindow('Caixa_Itens', 'Caixa_Itens', 'caixa_itens.RetiraValores', function(controller){
			var caixa = NroCaixaDesejado2;
			me.getAddWin().setTitle('Retirando Valores do Caixa: '+NroCaixaDesejado2);
			Ext.getCmp('cx_nro_retirada_caixa_itens').setValue(parseInt(NroCaixaDesejado2));
			Ext.getCmp('action_retirada_caixa_itens').setValue('RETIRA_VALORES');
			Ext.getCmp('forma_pagto_retirada_caixa_itens').setValue("1");

			if (userData.perfil_id == 1) {
				Ext.getCmp('cx_nro_retirada_caixa_itens').setVisible(true);
			}
		});
	},

	conferirTotais: function(button){
		var me = this;
		me.getDesktopWindow('Totais-Caixa_Itens', 'Caixa_Itens', 'caixa_itens.Totais', function(controller){
			me.getAddWin().setTitle('Conferindo Valores do Caixa: '+NroCaixaDesejado2);
			controller.getList().store.proxy.extraParams.caixa_nro = NroCaixaDesejado2;

			Ext.getCmp('GridCaixaItensAdiantamentos').store.proxy.extraParams.caixa_nro = NroCaixaDesejado2;
			Ext.getCmp('GridCaixaItensSuprimentos').store.proxy.extraParams.caixa_nro = NroCaixaDesejado2;
			Ext.getCmp('GridCaixaItensMeiosPagtos').store.proxy.extraParams.caixa_nro = NroCaixaDesejado2;
			Ext.getCmp('GridCaixaItensRetiradas').store.proxy.extraParams.caixa_nro = NroCaixaDesejado2;
			Ext.getCmp('GridCaixaItensVendas').store.proxy.extraParams.caixa_nro = NroCaixaDesejado2;
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(){
			Ext.getCmp('button_edit_caixa_itens').setDisabled(true);
			Ext.getCmp('button_del_caixa_itens').setDisabled(true);

			setTimeout(function(){
				me.VerificaCaixaAberto();
			},260);
		}, false, 'caixa_itens');
	},

	RegistroSelecionado: function(selModel, record, index, options){
		var me = this;
		if(record.get('cancelado') == 'S' || record.get('tipo_lancamento') == 3 || record.get('tipo_lancamento') == 4) {
			Ext.getCmp('button_edit_caixa_itens').setDisabled(true);
			Ext.getCmp('button_del_caixa_itens').setDisabled(true);

			if (userData.perfil_id == 1 && record.get('cancelado') == 'N') {
				Ext.getCmp('button_edit_caixa_itens').setDisabled(false);
			}
		}
		else {
			if(me.existPermissao('caixa_itens', 'editar')==true){
				Ext.getCmp('button_edit_caixa_itens').setDisabled(false);
				Ext.getCmp('button_del_caixa_itens').setDisabled(false);
			}
		}
	},

	loadGrid: function(record, index){
		var me = this;
		//me.getList().store.removeAll();
		//me.getList().store.sync();
		//me.getList().store.currentPage = 1;

		if(!me.getList().loadDisabled){
			me.getList().store.proxy.extraParams = {
				action: 'LIST'
			};

			setTimeout(function(){
				//me.getList().store.load();
				me.getList().store.loadPage(1);
				me.VerificaCaixaAberto();
			},200);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.caixa_itens.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});