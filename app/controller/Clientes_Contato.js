/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Clientes_Contato', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Clientes_Contato',
	started: true,

	refs: [
		{
			ref: 'list',
			selector: 'clientes_contatolist'
		},
		{
			ref: 'form',
			selector: 'addclientes_contatowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'clientes_contatolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterclientes_contatowin'
		},
		{
			ref: 'filterForm',
			selector: 'filterclientes_contatowin form'
		},
		{
			ref: 'addWin',
			selector: 'addclientes_contatowin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelClientes_Contato'
	],
	stores: [
		'StoreClientes_Contato',
		'StoreComboTipo_ContatoClientesContato'
	],

	views: [
		'clientes_contato.List',
		'clientes_contato.Filtro',
		'clientes_contato.Edit'
	],

	init: function(application) {
		this.control({
			'clientes_contatolist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'clientes_contatolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'clientes_contatolist button[action=adicionar]': {
				click: this.add
			},
			'clientes_contatolist button[action=editar]': {
				click: this.btedit
			},
			'clientes_contatolist button[action=deletar]': {
				click: this.btdel
			},			
			'clientes_contatolist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addclientes_contatowin button[action=salvar]': {
				click: this.update
			},
			'addclientes_contatowin button[action=resetar]': {
				click: this.reset
			},
			'addclientes_contatowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addclientes_contatowin form fieldcontainer combobox[name=tipo_contato]': {
				change: this.alterTextField
			},
			'addclientes_contatowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addclientes_contatowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterclientes_contatowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterclientes_contatowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterclientes_contatowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterclientes_contatowin': {
				show: this.filterSetFields
			}
		});
	},

	alterTextField: function(combo){
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}

		if(record.get('id')=='TELEFONE'){
			Ext.getCmp('descricao_tel_clientes_contato').setDisabled(false);
			Ext.getCmp('descricao_tel_clientes_contato').setVisible(true);

			Ext.getCmp('descricao_tel_ddg_clientes_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_ddg_clientes_contato').setVisible(false);
			Ext.getCmp('descricao_clientes_contato').setDisabled(true);
			Ext.getCmp('descricao_clientes_contato').setVisible(false);
		}
		else if(record.get('id')=='DDG'){
			Ext.getCmp('descricao_tel_ddg_clientes_contato').setDisabled(false);
			Ext.getCmp('descricao_tel_ddg_clientes_contato').setVisible(true);

			Ext.getCmp('descricao_clientes_contato').setDisabled(true);
			Ext.getCmp('descricao_clientes_contato').setVisible(false);
			Ext.getCmp('descricao_tel_clientes_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_clientes_contato').setVisible(false);
		}
		else{
			Ext.getCmp('descricao_tel_clientes_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_clientes_contato').setVisible(false);
			Ext.getCmp('descricao_tel_ddg_clientes_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_ddg_clientes_contato').setVisible(false);

			Ext.getCmp('descricao_clientes_contato').setDisabled(false);
			Ext.getCmp('descricao_clientes_contato').setVisible(true);
		}
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/clientes_contato/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddClientes_ContatoWin', 'Clientes_Contato', 'clientes_contato.Edit', function(){
			me.getAddWin().setTitle('Alteração de Contato');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/clientes_contato/list.php');
			Ext.getCmp('action_clientes_contato').setValue('EDITAR');
			Ext.getCmp('tipo_contato_clientes_contato').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'clientes_contato', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar o Contato: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Clientes_Contato', 'Clientes_Contato', 'clientes_contato.Edit', function(){
			Ext.getCmp('cod_cliente_clientes_contato').setValue(me.getList().store.proxy.extraParams.cod_cliente);
			Ext.getCmp('descricao_tel_clientes_contato').focus(false, 1000);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('clientes_contato', 'editar')==true){
			Ext.getCmp('button_edit_clientes_contato').setDisabled(false);
		}

		if(this.existPermissao('clientes_contato', 'deletar')==true){
			Ext.getCmp('button_del_clientes_contato').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.clientes_contato.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});