/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Clientes', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	started: true,
	storePai: true,
	tabela: 'Clientes',

	dependeciesControllers: [
		'Os'
	],

	refs: [
		{
			ref: 'list',
			selector: 'clienteslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addclienteswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'clienteslist button[action=filtrar], clienteslist button[action=filtro]'
		},
		{
			ref: 'filterWin',
			selector: 'filterclienteswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterclienteswin form'
		},
		{
			ref: 'addWin',
			selector: 'addclienteswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelOs',
		'ModelClientes'
	],
	stores: [
		'StoreComboCategoria_Precos',
		'StoreComboSexoClientes',
		'StoreComboSituacaoCadastralClientes',
		'StoreComboTipo_ClienteClientes',
		'StoreComboProfissaoClientes',
		'StoreClientes_Anotacoes',
		'StoreClientes_Contato',
		'StoreClientes_Endereco',
		'StoreClientesOs',
		'StoreClientes_Logs_Sistema',
		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboCorreios_Bairros',
		'StoreComboCorreios_Enderecos',
		'StoreComboCepClientes',
		'StoreComboUsuarios',
		'StoreComboClientes',
		'StoreClientes',
		'StoreAdiantamentos',
		'StoreClientesVendas'
	],
	
	views: [
		'clientes.OsGrid',
		'clientes.List',
		'clientes.Edit',
		'clientes.Filtro'
	],
	 
	menuGrid: Ext.create('Ext.menu.Menu', {
		id: 'menu_grid_clientes',
		width: 240,
		items: [
			{
				text: 'Adicionar OS',
				id: 'menuItem_add_os',
				iconCls: 'bt_add_os',
				action: 'adicionar_os'
			},
			{
				text: 'Adicionar Recibo',
				id: 'menuItem_add_recibo',
				iconCls: 'recibos',
				action: 'adicionar_recibo'
			},
			{
				text: 'Alterar Cadastro',
				id: 'menuItem_edit_cliente',
				iconCls: 'bt_edit2',
				action: 'editar_cliente'
			}
		]
	}),

	init: function(application) {
		this.control({
			'#menu_grid_clientes': {
				click: this.menuClick
			},
			'clienteslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado,
				itemcontextmenu: this.contextMenu/*,
				celldblclick: this.btedit*/
			},
			'clienteslist button[action=filtro]': {
				click: this.btStoreLoadFielter
			},
			'clienteslist button[action=filtrar]': {
				click: this.btStoreLoadFielter2
			},
			'clienteslist button[action=adicionar_pf]': {
				click: this.add
			},
			'clienteslist button[action=adicionar_pj]': {
				click: this.add
			},
			'clienteslist button[action=editar]': {
				click: this.btedit
			},
			'clienteslist button[action=deletar]': {
				click: this.btdel
			},			
			'clienteslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addclienteswin': {
				beforeclose: this.loadClientes
			},
			'addclienteswin tabpanel': {
				tabchange: this.loadActive
			},
			'addclienteswin button[id=button_salvar_clientes]': {
				click: this.update
			},
			'addclienteswin button[id=button_resetar_clientes]': {
				click: this.reset
			},
			'addclienteswin form fieldcontainer combobox': {
				change: this.enableButton,
				afterrender: this.comboLoad
			},
			'addclienteswin form fieldcontainer combobox[id=estado_clientes]': {
				change: this.loadCidade
			},
			'addclienteswin form fieldcontainer combobox[id=cidade_clientes]': {
				change: this.loadBairro
			},
			'addclienteswin form fieldcontainer combobox[id=endereco_clientes]': {
				change: this.setEndereco
			},
			'addclienteswin form fieldcontainer textfield[id=cpf_clientes]': {
				blur: this.verifyUnique
			},
			'addclienteswin form fieldcontainer textfield[id=cnpj_clientes]': {
				blur: this.verifyUnique
			},
			'addclienteswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addclienteswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterclienteswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'filterclienteswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterclienteswin form fieldcontainer combobox[id=estado_filter_clientes]': {
				change: this.loadCidade
			},
			'filterclienteswin form fieldcontainer combobox[id=cidade_filter_clientes]': {
				change: this.loadBairro
			},
			'filterclienteswin form fieldcontainer combobox[id=bairro_filter_clientes]': {
				change: this.loadEndereco
			},
			'filterclienteswin form fieldcontainer combobox[id=tipo_cliente_filter_clientes]': {
				change: this.habilityFilter
			},
			'filterclienteswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterclienteswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterclienteswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	endereco: false,
	recordEdit: false,

	verifyOrcamentos: function(){
		var list1 = Ext.getCmp('List-Orcamentos');
		var list2 = Ext.getCmp('List-Vendas');
		var edit1 = Ext.getCmp('AddOrcamentosForroWin');
		var edit2 = Ext.getCmp('AddAcessorios_Orcamentos_ForroWin');
		var edit3 = Ext.getCmp('AddAmbientes_Orcamentos_ForroWin');
		var edit4 = Ext.getCmp('AddProdutos_Orcamentos_ForroWin');

		if(list1){list1.close();}
		if(list2){list2.close();}
		if(edit1){edit1.close();}
		if(edit2){edit2.close();}
		if(edit3){edit3.close();}
		if(edit4){edit4.close();}
	},

	loadClientes: function(painel){
		var cod_cliente = Ext.getCmp('cod_cliente_clientes').getValue();
		if(cod_cliente>0){
			if(Ext.getCmp('GridClientes')){
				this.getList().store.load();
			}
		}
	},

	contextMenu: function(view, record, item, index, e){
		this.recordEdit = record;
		e.stopEvent();
		if(this.existPermissao('clientes', 'editar')==false){
			Ext.getCmp('menuItem_edit_cliente').setVisible(false);
		}
		if(this.existPermissao('os', 'adicionar')==false){
			Ext.getCmp('menuItem_add_os').setVisible(false);
		}
		if(this.existPermissao('recibos', 'adicionar')==false){
			Ext.getCmp('menuItem_add_recibo').setVisible(false);
		}
		this.menuGrid.showAt(e.getXY());
	},

	menuClick: function( menu, item, e, eOpts ){
		var record = this.recordEdit;
		if(item.action=='editar_cliente'){
			this.edit(menu, record);
		}
		else if(item.action=='adicionar_os'){
			this.setDataClienteOs(record);
		}
		else if(item.action=='adicionar_recibo'){
			this.setDataClienteRecibo(record);
		}
	},

	setDataClienteRecibo: function(record){
		var me = this.application.getController('Recibos');
		me.getDesktopWindow('AddRecibosWin', 'Recibos', 'recibos.Edit', function(){
			if(record.get('tipo_cliente')=='F'){
				var titulo = record.get('nome_completo');
				var documento = record.get('cpf');
			}
			else{
				var titulo = record.get('razao_social');
				var documento = record.get('cnpj');
			}
			me.getForm().el.mask('Aguarde...');

			empresa = me.getCurrentEmpresa();

			me.getForm().getForm().setValues({
				data_recido: Ext.Date.format(new Date(), 'd/m/Y'),
				recebido_de_nome: titulo,
				recebido_de_docto: documento,
				emitente_nome: empresa.descricao_empresa_atual
			});

			me.getForm().el.unmask();
		});
	},

	setDataClienteOs: function(record){
		var me = this.application.getController('Os');
		me.getDesktopWindow('AddOsWin', 'Os', 'os.Edit', function(){
			if(record.get('tipo_cliente')=='F'){
				var titulo = record.get('nome_completo');
			}
			else{
				var titulo = record.get('razao_social');
			}
			me.getForm().el.mask('Aguarde...');

			empresa = me.getCurrentEmpresa();

			me.getForm().getForm().setValues({
				titulo: titulo,
				nome_cliente: titulo,
				estado: record.get('estado'),
				endereco_bairro_nome: record.get('bairro_nome'),
				endereco_num: record.get('nro_end'),
				endereco_complemento: record.get('complemento'),
				endereco_loteamento: record.get('loteamento'),
				endereco_referencia: record.get('ponto_ref'),
				cliente: record.get('cod_cliente'),
				empresa: empresa.cod_empresa,
				fone1: record.get('fone1'),
				fone1_obs: record.get('fone1_obs'),
				fone2: record.get('fone2'),
				fone2_obs: record.get('fone2_obs'),
				fone3: record.get('fone3'),
				fone3_obs: record.get('fone3_obs'),
				fone4: record.get('fone4'),
				fone4_obs: record.get('fone4_obs'),
				fone5: record.get('fone5'),
				categoria: 7,
				departamento: 1,
				fone5_obs: record.get('fone5_obs')
			});

			var a = setInterval(function(){
				me.verificaAnotacoesCliente(record.get('cod_cliente'));
				if(record.get('cidade') != null && record.get('cidade') != ""){
					if(me.cidade==true){
						
						Ext.getCmp('cidade_os').setValue(record.get('cidade'));
						me.comboEndereco(record.get('cidade'), record.get('bairro'), record.get('endereco'));
						me.cidade = null;
					}
				} 
				else {
					me.cidade = null;
				}

				if(record.get('endereco') != null && record.get('endereco') != ""){
					if(me.endereco==true){
						Ext.getCmp('endereco_os').setValue(parseInt(record.get('endereco')));
						me.endereco = null;
					}
				}else{me.endereco = null;}
				if(me.cidade == null && me.endereco == null){
					if(me.getForm()){
						me.getForm().el.unmask();
					}

					clearInterval(a);
					me.cidade = false;
					me.endereco = false;
				}
			}, 500);
		});
	},

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			
			Ext.getCmp('bairro_nome_clientes').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_clientes').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_clientes').setValue(record.get('loteamento'));
			}
			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_clientes').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_clientes').setValue(record.get('cep'));
			Ext.getCmp('nro_end_clientes').focus(false, 100);
		}
	},

	verifyUnique: function(field){
		var v = field.getValue();
		var cod_cliente = field.up('form').down('[name=cod_cliente]').getValue();
		Ext.Ajax.request({
			url: 'server/modulos/clientes/list.php',
			method: 'POST',
			params:{
				action: 'VALID_UNIQUE',
				cod_cliente: cod_cliente,
				param: field.name,
				valor: v
			},
			success: function(s, o){
				var dados = Ext.decode(s.responseText);
				if(dados.success==false){
					field.markInvalid(field.fieldLabel+' Já Existe...');
				}
			}
		});
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('cidade_clientes');
		}
		else{
			var comboCidade = Ext.getCmp('cidade_filter_clientes');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			Ext.getCmp('endereco_clientes').store.proxy.extraParams.localidade_id = combo.getValue();
			me.bairro = true;
			if(Ext.getCmp('endereco_clientes').isDisabled()){
				Ext.getCmp('endereco_clientes').setDisabled(false);
			}
			return true;
		}
		else{
			if(Ext.getCmp('endereco_filter_clientes').store){
				Ext.getCmp('endereco_filter_clientes').store.proxy.extraParams.localidade_id = combo.getValue();
				if(Ext.getCmp('endereco_filter_clientes').isDisabled()){
					Ext.getCmp('endereco_filter_clientes').setDisabled(false);
				}				
			}
			me.bairro = true;
			return true;
		}

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_clientes');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_clientes');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/clientes/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_clientes');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.verifyOrcamentos();
		me.getDesktopWindow('AddClientesWin', 'Clientes', 'clientes.Edit', function(dados){
			me.getAddWin().setTitle('Alteração do Cadastro do Cliente');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_cliente'), 'server/modulos/clientes/list.php', function(dados){
				me.getForm().el.mask('Aguarde...');
				window.controlS = me;
				
				var a = setInterval(function(){
					me.verificaAnotacoesCliente(record.get('cod_cliente'));
					if(dados.cidade != null && dados.cidade != ""){
						if(me.cidade==true){
							Ext.getCmp('cidade_clientes').setValue(dados.cidade);
							me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
							me.cidade = null;
						}
					} else {
							me.cidade = null;
					}

					if(dados.endereco != null && dados.endereco != ""){
						if(me.endereco==true){
							Ext.getCmp('endereco_clientes').setValue(parseInt(dados.endereco));
							me.endereco = null;
						}
					}else{me.endereco = null;}
					if(me.cidade == null && me.endereco == null){
						if(me.getForm()){
							me.getForm().el.unmask();
						}
						
						clearInterval(a);
						me.cidade = false;
						me.endereco = false;
					}
				}, 500);
			});
			
			Ext.getCmp('action_clientes').setValue('EDITAR');
			me.hability(record.get('tipo_cliente'));

			Ext.getCmp('GridClientes_Anotacoes').setDisabled(false);
			Ext.getCmp('GridClientes_Contato').setDisabled(false);
			Ext.getCmp('GridClientes_Endereco').setDisabled(false);
			Ext.getCmp('GridClientesOs').setDisabled(false);
			Ext.getCmp('GridOrcamentos').setDisabled(false);
			Ext.getCmp('GridAdiantamentos').setDisabled(false);
			Ext.getCmp('GridVendas').setDisabled(false);

			Ext.getCmp('cadastrado_por_clientes').setVisible(true);
			Ext.getCmp('alterado_por_clientes').setVisible(true);

			Ext.getCmp('GridClientes_Anotacoes').store.proxy.extraParams.cod_cliente = record.get('cod_cliente');
			Ext.getCmp('GridClientes_Contato').store.proxy.extraParams.cod_cliente = record.get('cod_cliente');
			Ext.getCmp('GridClientes_Endereco').store.proxy.extraParams.cod_cliente = record.get('cod_cliente');
			Ext.getCmp('GridClientesOs').store.proxy.extraParams.cliente = record.get('cod_cliente');
			Ext.getCmp('GridOrcamentos').store.proxy.extraParams.cliente = record.get('cod_cliente');
			Ext.getCmp('GridVendas').store.proxy.extraParams.cliente = record.get('cod_cliente');
			Ext.getCmp('GridVendas').store.proxy.extraParams.orcamento_venda = 'S';
			Ext.getCmp('GridVendas').store.proxy.extraParams.cadastrado_por = '9999';
			Ext.getCmp('GridAdiantamentos').store.proxy.extraParams.cod_cliente = record.get('cod_cliente');

			var data = me.application.dados[me.tabela.toLowerCase()];

			Ext.each(data, function(j){
				if(j.acao=='cliente_logs'){
					Ext.getCmp('GridClientes_Logs_Sistema').setDisabled(false);
					Ext.getCmp('GridClientes_Logs_Sistema').store.proxy.extraParams.tabela_cod = record.get('cod_cliente');
					Ext.getCmp('GridClientes_Logs_Sistema').store.proxy.extraParams.modulo = 'Clientes';
				}
			});
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'clientes', {
			action: 'DELETAR',
			id: record.get('cod_cliente')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			if(record.get('tipo_cliente')=='F'){
				var cliente = record.get('nome_completo');
			}
			else{
				var cliente = record.get('razao_social');
			}
			Ext.Msg.confirm('Confirmar', 'Deseja Apagar o Cliente: '+cliente+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	add: function(button) {
		var me = this;
		window.App = me;
		me.cidade = false;

		grid = button.up('gridpanel');
		me.gridPanel = grid;

		me.verifyOrcamentos();

		me.getDesktopWindow('Clientes', 'Clientes', 'clientes.Edit', function(){
			me.hability(button.type);
			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_clientes').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					if(me.cidade==true){
						var record = Ext.getCmp('cidade_clientes').store.findRecord('descricao', empresa.cidade);
						if(record){
							Ext.getCmp('cidade_clientes').setValue(record.get('id'));
						}
						me.cidade = null;
					}
				}else{me.cidade = null;}
				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);

			Ext.getCmp('GridClientes_Anotacoes').setDisabled(true);
			Ext.getCmp('GridClientes_Contato').setDisabled(true);
			Ext.getCmp('GridClientes_Endereco').setDisabled(true);
			Ext.getCmp('GridOrcamentos').setDisabled(true);
			Ext.getCmp('GridAdiantamentos').setDisabled(true);
			Ext.getCmp('GridVendas').setDisabled(true);
			Ext.getCmp('id_categoria_precos_clientes').setValue("1");
			//Ext.getCmp('GridClientesOs').setDisabled(true);
		});
	},

	hability: function(type){
		if(type=='J'){
			Ext.getCmp('fieldcontainer_j').setVisible(true);
			Ext.getCmp('razao_social_clientes').setVisible(true);
			Ext.getCmp('nome_fantasia_clientes').setVisible(true);
			Ext.getCmp('ie_clientes').setVisible(true);
			Ext.getCmp('razao_social_clientes').setDisabled(false);
			Ext.getCmp('nome_fantasia_clientes').setDisabled(false);
			Ext.getCmp('cnpj_clientes').setDisabled(false);
			Ext.getCmp('im_clientes').setDisabled(false);
			Ext.getCmp('ie_clientes').setDisabled(false);
			Ext.getCmp('pessoa_contato_clientes').setDisabled(false);

			Ext.getCmp('fieldcontainer_f').setVisible(false);
			Ext.getCmp('nome_completo_clientes').setVisible(false);
			Ext.getCmp('profissao_clientes').setVisible(false);
			Ext.getCmp('data_nascimento_clientes').setVisible(false);
			Ext.getCmp('nome_completo_clientes').setDisabled(true);
			Ext.getCmp('profissao_clientes').setDisabled(true);
			Ext.getCmp('data_nascimento_clientes').setDisabled(true);
			Ext.getCmp('cpf_clientes').setDisabled(true);
			Ext.getCmp('sexo_clientes').setDisabled(true);
			Ext.getCmp('identidade_clientes').setDisabled(true);
			Ext.getCmp('razao_social_clientes').focus(false, 1000);
			Ext.getCmp('tipo_cliente_clientes').setValue('J');
		}
		else{
			Ext.getCmp('fieldcontainer_j').setVisible(false);
			Ext.getCmp('razao_social_clientes').setVisible(false);
			Ext.getCmp('nome_fantasia_clientes').setVisible(false);
			Ext.getCmp('ie_clientes').setVisible(false);

			Ext.getCmp('razao_social_clientes').setDisabled(true);
			Ext.getCmp('nome_fantasia_clientes').setDisabled(true);
			Ext.getCmp('cnpj_clientes').setDisabled(true);
			Ext.getCmp('im_clientes').setDisabled(true);
			Ext.getCmp('ie_clientes').setDisabled(true);
			Ext.getCmp('pessoa_contato_clientes').setDisabled(true);


			Ext.getCmp('fieldcontainer_f').setVisible(true);
			Ext.getCmp('nome_completo_clientes').setVisible(true);
			Ext.getCmp('profissao_clientes').setVisible(true);
			Ext.getCmp('data_nascimento_clientes').setVisible(true);

			Ext.getCmp('nome_completo_clientes').setDisabled(false);
			Ext.getCmp('profissao_clientes').setDisabled(false);
			Ext.getCmp('data_nascimento_clientes').setDisabled(false);
			Ext.getCmp('cpf_clientes').setDisabled(false);
			Ext.getCmp('sexo_clientes').setDisabled(false);
			Ext.getCmp('identidade_clientes').setDisabled(false);

			Ext.getCmp('nome_completo_clientes').focus(false, 1000);
			Ext.getCmp('tipo_cliente_clientes').setValue('F');
		}
	},

	habilityFilter: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(combo.getValue()=='J'){
			Ext.getCmp('fieldcontainer_filter_j').setVisible(true);
			Ext.getCmp('razao_social_filter_clientes').setVisible(true);
			Ext.getCmp('nome_fantasia_filter_clientes').setVisible(true);
			Ext.getCmp('ie_filter_clientes').setVisible(true);
			Ext.getCmp('razao_social_filter_clientes').setDisabled(false);
			Ext.getCmp('nome_fantasia_filter_clientes').setDisabled(false);
			Ext.getCmp('cnpj_filter_clientes').setDisabled(false);
			Ext.getCmp('im_filter_clientes').setDisabled(false);
			Ext.getCmp('ie_filter_clientes').setDisabled(false);
			Ext.getCmp('pessoa_contato_filter_clientes').setDisabled(false);

			Ext.getCmp('fieldcontainer_filter_f').setVisible(false);
			Ext.getCmp('nome_completo_filter_clientes').setVisible(false);
			Ext.getCmp('profissao_filter_clientes').setVisible(false);
			Ext.getCmp('data_nascimento_filter_clientes').setVisible(false);
			Ext.getCmp('nome_completo_filter_clientes').setDisabled(true);
			Ext.getCmp('profissao_filter_clientes').setDisabled(true);
			Ext.getCmp('data_nascimento_filter_clientes').setDisabled(true);
			Ext.getCmp('cpf_filter_clientes').setDisabled(true);
			Ext.getCmp('sexo_filter_clientes').setDisabled(true);
			Ext.getCmp('identidade_filter_clientes').setDisabled(true);
		}
		else{
			Ext.getCmp('fieldcontainer_filter_j').setVisible(false);
			Ext.getCmp('razao_social_filter_clientes').setVisible(false);
			Ext.getCmp('nome_fantasia_filter_clientes').setVisible(false);
			Ext.getCmp('ie_filter_clientes').setVisible(false);
			Ext.getCmp('razao_social_filter_clientes').setDisabled(true);
			Ext.getCmp('nome_fantasia_filter_clientes').setDisabled(true);
			Ext.getCmp('cnpj_filter_clientes').setDisabled(true);
			Ext.getCmp('im_filter_clientes').setDisabled(true);
			Ext.getCmp('ie_filter_clientes').setDisabled(true);
			Ext.getCmp('pessoa_contato_filter_clientes').setDisabled(true);

			Ext.getCmp('fieldcontainer_filter_f').setVisible(true);
			Ext.getCmp('nome_completo_filter_clientes').setVisible(true);
			Ext.getCmp('profissao_filter_clientes').setVisible(true);
			Ext.getCmp('data_nascimento_filter_clientes').setVisible(true);
			Ext.getCmp('nome_completo_filter_clientes').setDisabled(false);
			Ext.getCmp('profissao_filter_clientes').setDisabled(false);
			Ext.getCmp('data_nascimento_filter_clientes').setDisabled(false);
			Ext.getCmp('cpf_filter_clientes').setDisabled(false);
			Ext.getCmp('sexo_filter_clientes').setDisabled(false);
			Ext.getCmp('identidade_filter_clientes').setDisabled(false);
		}
	},

	winAddNoClose: true,

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(dados){
			if(Ext.getCmp('action_clientes').getValue()=='INSERIR'){
				Ext.getCmp('action_clientes').setValue('EDITAR');
				Ext.getCmp('cod_cliente_clientes').setValue(dados.cod_cliente);

				Ext.getCmp('GridClientes_Anotacoes').setDisabled(false);
				Ext.getCmp('GridClientes_Contato').setDisabled(false);
				Ext.getCmp('GridClientes_Endereco').setDisabled(false);
				Ext.getCmp('GridClientes_Logs_Sistema').setDisabled(false);
				Ext.getCmp('GridClientesOs').setDisabled(false);
				Ext.getCmp('GridOrcamentos').setDisabled(false);
				Ext.getCmp('GridAdiantamentos').setDisabled(false);
				Ext.getCmp('GridVendas').setDisabled(true);

				Ext.getCmp('GridClientes_Anotacoes').store.proxy.extraParams.cod_cliente = dados.cod_cliente;
				Ext.getCmp('GridClientes_Contato').store.proxy.extraParams.cod_cliente = dados.cod_cliente;
				Ext.getCmp('GridClientes_Endereco').store.proxy.extraParams.cod_cliente = dados.cod_cliente;
				Ext.getCmp('GridClientesOs').store.proxy.extraParams.cliente = dados.cod_cliente;
				Ext.getCmp('GridOrcamentos').store.proxy.extraParams.cliente = dados.cod_cliente;
				Ext.getCmp('GridVendas').store.proxy.extraParams.cliente = dados.cod_cliente;
				Ext.getCmp('GridVendas').store.proxy.extraParams.orcamento_venda = 'S';
				Ext.getCmp('GridVendas').store.proxy.extraParams.cadastrado_por = '9999';
				Ext.getCmp('GridAdiantamentos').store.proxy.extraParams.cod_cliente = dados.cod_cliente;

				Ext.getCmp('TabPanelCentralCliente').setActiveTab(1);
			}
		}, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('clientes', 'editar')==true){
			Ext.getCmp('button_edit_clientes').setDisabled(false);
		}

		if(this.existPermissao('clientes', 'deletar')==true){
			Ext.getCmp('button_del_clientes').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_clientes').getValue();
		//this.getList().store.proxy.extraParams.start = 0;
		//this.getList().store.proxy.extraParams.limit = 50;
		this.getList().store.loadPage(1);
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.clientes.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();

		var me = this;
		var empresa = me.getCurrentEmpresa();
		Ext.getCmp('estado_filter_clientes').setValue(empresa.estado);
		var a = setInterval(function(){
			if(empresa.cidade != null && empresa.cidade != ""){
				if(me.cidade==true){
					var record = Ext.getCmp('cidade_filter_clientes').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_filter_clientes').setValue(record.get('id'));
					}
					me.cidade = null;
				}
			} 
			else {
				me.cidade = null;
			}
			if(me.cidade == null){
				clearInterval(a);
			}
		}, 1000);
	}
});