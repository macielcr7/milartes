/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produtos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	dependeciesControllers: [
		'Atributos_Produto',
		'Produto_Imagens',
		'Produtos_Cores',
		'Produto_Precos',
		'Produto_Composicoes'
	],

	tabela: 'Produtos',
	refs: [
		{
			ref: 'list',
			selector: 'produtoslist gridpanel[id=GridProdutos]'
		},
		{
			ref: 'form',
			selector: 'addprodutoswin form[id=FormProdutos]'
		},
		{
			ref: 'filterBtn',
			selector: 'produtoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterprodutoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterprodutoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addprodutoswin'
		}
	],
	models: [
		'ModelTreeCategorias',
		'ModelComboCategorias',
		'ModelComboLocal',
		'ModelProdutos'
	],
	stores: [
		'StoreTreeCategorias',
		'StoreComboTipoProdutoProdutos',
		'StoreComboComissionar_VendedorProdutos',
		'StoreComboComissionar_TipoProdutos',
		'StoreComboControlar_EstoqueProdutos',
		'StoreComboPossui_ComposicaoProdutos',
		'StoreComboProdutos_Categorias',
		'StoreComboFornecedores',
		'StoreComboSetor_Estoque',
		'StoreComboProdutos_Marcas',
		'StoreComboUnidades_Medidas',
		'StoreComboAtivoProdutos',
		'StoreProdutos',
		'StoreProdutos_Logs_Sistema',
		'StoreComboSimNao'
	],
	views: [
		'produtos.List',
		'produtos.Filtro',
		'produtos.Edit'
	],

	init: function(application) {
		this.control({
			'produtoslist gridpanel[id=GridProdutos]': { 
				afterrender: this.getPermissoes,
				render: this.loadGrid,
				select: this.RegistroSelecionado
			},
			'produtoslist treepanel':{
				select: this.loadGrid
			},
			'produtoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'produtoslist button[action=filtro_grid]': {
				click: this.btStoreLoadFielterQuery
			},
			'produtoslist button[action=adicionar]': {
				click: this.add
			},
			'produtoslist button[action=editar]': {
				click: this.btedit
			},
			'produtoslist button[action=deletar]': {
				click: this.btdel
			},
			'produtoslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'produtoslist button[action=copiar_produto]': {
				click: this.btcopia
			},
			'produtoslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addprodutoswin tabpanel': {
				tabchange: this.loadActive
			},
			'addprodutoswin button[action=salvar]': {
				click: this.update
			},
			'addprodutoswin button[action=resetar]': {
				click: this.reset
			},
			'addprodutoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addprodutoswin form fieldcontainer combobox[id=tipo_produto_produtos]': {
				change: this.tipoProduto
			},
			'addprodutoswin form fieldcontainer combobox[id=categoria_produtos]': {
				change: this.setAttrs
			},
			'addprodutoswin combobox[id=possui_composicao_produtos]': {
				change: this.toogleComposicoes
			},
			'addprodutoswin combobox[id=comissionar_tipo_produtos]': {
				change: this.toogleTipoComissao
			},
			'addprodutoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addprodutoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterprodutoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterprodutoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterprodutoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterprodutoswin': {
				show: this.filterSetFields
			}
		});
	},

	tipoProduto: function(combo, value){
		var me = this;
		if(value=='I'){
			Ext.getCmp('preco_custo_servico_produtos').setDisabled(false);
			Ext.getCmp('preco_custo_servico_produtos').setVisible(true);
		}
		else{
			Ext.getCmp('preco_custo_servico_produtos').setDisabled(true);
			Ext.getCmp('preco_custo_servico_produtos').setVisible(false);
		}
	},

	toogleTipoComissao: function(combo, value){
		var me = this;
		var fMoeda 		= Ext.getCmp('valor_comissao_produtos');
		var fPorcento 	= Ext.getCmp('valor_comissao_porcentagem_produtos');

		if(combo.getValue()=='PORCENTO'){
			fMoeda.setVisible(false);
			fMoeda.setDisabled(true);
			fPorcento.setVisible(true);
			fPorcento.setDisabled(false);
		}
		else{
			fMoeda.setVisible(true);
			fMoeda.setDisabled(false);
			fPorcento.setVisible(false);
			fPorcento.setDisabled(true);
		}
	},

	toogleComposicoes: function(combo, value){
		var me = this;
		var grid = Ext.getCmp('GridProduto_Composicoes');
		if(combo.getValue()!=null && combo.getValue()=='S'){
			grid.setDisabled(false);
		}
		else{
			grid.setDisabled(true);
		}
	},

	setAttrs: function(combo, value){
		var me = this;
		Ext.getCmp('panel_attrs').removeAll();

		if(combo.getValue()!=null){
			var record = Ext.getCmp('categoria_produtos').store.getById(value);
			if(record && record.raw){
				var attrs = record.raw.attrs;

				for(var i=0;i<attrs.length;i++){
					Ext.getCmp('panel_attrs').add({
						xtype: 'textfield',
						id: 'attr_'+attrs[i].id_atributo+'_produtos',
						name: 'attrs['+attrs[i].id_atributo+'::]',
						allowBlank: true,
						mask: '999.999.990,000',
						money: true,
						plugins: 'textmask',
						fieldLabel: attrs[i].atributo
					});
				}
			}
		}
	},

	loadGrid: function(tree, record, index, eOpts ){
		var me = this;
		var categorias = Ext.getCmp('TreeCategorias').getSelectionModel().getLastSelected();
		//me.getList().store.removeAll();
		//me.getList().store.sync();
		//me.getList().store.currentPage = 1;

		if(categorias!=null){
			me.getList().store.proxy.extraParams.cod_categoria = categorias.get('cod_categoria');
		}
		else {
			me.getList().store.proxy.extraParams = {
				action: 'LIST'
			};
		}

		setTimeout(function(){
			//me.getList().store.load();
			me.getList().store.loadPage(1);
		},100);
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/produtos/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},

	loadActive: function(comp, newcomp, oldcomp){
		//if(newcomp.store && newcomp.store.getCount()==0 && Ext.getCmp('action_produtos').getValue()=='EDITAR'){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	edit: function(grid, record) {
		var me = this;
		me.gridPanel = grid;

		me.getDesktopWindow('AddProdutosWin', 'Produtos', 'produtos.Edit', function(dados){
			me.getAddWin().setTitle('Alteração do Produto ID='+record.get('id'));

			Ext.getCmp('GridProduto_Composicoes').store.proxy.extraParams.id_produto = record.get('id');
			Ext.getCmp('GridProduto_Imagens').store.proxy.extraParams.id_produto = record.get('id');
			Ext.getCmp('GridProdutos_Cores').store.proxy.extraParams.id_produto = record.get('id');
			Ext.getCmp('GridProduto_Precos').store.proxy.extraParams.id_produto = record.get('id');

			Ext.getCmp('GridProduto_Composicoes').store.load();
			Ext.getCmp('GridProduto_Imagens').store.load();
			Ext.getCmp('GridProdutos_Cores').store.load();
			Ext.getCmp('GridProduto_Precos').store.load();

			Ext.getCmp('GridProduto_Precos').setDisabled(false);
			Ext.getCmp('GridProduto_Imagens').setDisabled(false);
			Ext.getCmp('GridProdutos_Cores').setDisabled(false);

			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/produtos/list.php', function(dados){
				Ext.getCmp('TabPanelCentralProdutos').setActiveTab(1);
				Ext.getCmp('TabPanelCentralProdutos').setActiveTab(0);
				me.getForm().el.mask('Aguarde, Carregando Dados...');
				Ext.getCmp('action_produtos').setValue('EDITAR');
				Ext.getCmp('FormDadosComplementares').getForm().setValues(dados);
				Ext.getCmp('categoria_produtos').setValue(parseInt(dados.categoria));

				setTimeout(function(){
					if(dados.attrs){
						for(var i=0;i<dados.attrs.length;i++){
							var attr = dados.attrs[i];
							Ext.getCmp('attr_'+attr.id_atributo_categoria+'_produtos').setValue(attr.valor);
						}
					}
					me.getForm().el.unmask();
				}, 500);

				if(dados.comissionar_tipo=="PORCENTO"){
					Ext.getCmp('valor_comissao_porcentagem_produtos').setValue(dados.valor_comissao);
					Ext.getCmp('valor_comissao_produtos').setValue('');
				}
				else{
					Ext.getCmp('valor_comissao_porcentagem_produtos').setValue('');
					Ext.getCmp('valor_comissao_produtos').setValue(dados.valor_comissao);
				}
			});

			var data = me.application.dados[me.tabela.toLowerCase()];

			Ext.each(data, function(j){
				/*
				if(j.acao=='custo_produto'){
					Ext.getCmp('preco_custo_produtos').setVisible(false);
				}
				*/
				if(j.acao == 'produtos_logs'){
					Ext.getCmp('GridProdutos_Logs_Sistema').setDisabled(false);
					Ext.getCmp('GridProdutos_Logs_Sistema').store.proxy.extraParams.tabela_cod = record.get('id');
					Ext.getCmp('GridProdutos_Logs_Sistema').store.proxy.extraParams.modulo = 'produtos';
				}
			});
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'produtos', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Desativar: '+record.get('descricao_completa')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	btcopia: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Gerar uma Cópia do Produto: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.GeraCopia(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar um Produto para poder Copiar');
			return true;
		}
	},

	GeraCopia: function(grid, record, button) {
		var me = this;
		me.acoesAjax(grid, 'produtos', {
			action: 'COPIA_PRODUTO',
			id_produto: record.get('id')
		}, button, false);
	},

	add: function(button) {
		var me = this;

		grid = button.up('gridpanel');
		me.gridPanel = grid;

		me.getDesktopWindow('Produtos', 'Produtos', 'produtos.Edit', function(){
			Ext.getCmp('TabPanelCentralProdutos').setActiveTab(1);
			Ext.getCmp('TabPanelCentralProdutos').setActiveTab(0);

			Ext.getCmp('GridProduto_Composicoes').store.proxy.extraParams.id_produto = -1;
			Ext.getCmp('GridProduto_Imagens').store.proxy.extraParams.id_produto = -1;
			Ext.getCmp('GridProdutos_Cores').store.proxy.extraParams.id_produto = -1;
			Ext.getCmp('GridProduto_Precos').store.proxy.extraParams.id_produto = -1;

			Ext.getCmp('GridProduto_Composicoes').store.removeAll();
			Ext.getCmp('GridProduto_Imagens').store.removeAll();
			Ext.getCmp('GridProdutos_Cores').store.removeAll();
			Ext.getCmp('GridProduto_Precos').store.removeAll();

			var data = me.application.dados[me.tabela.toLowerCase()];

			Ext.each(data, function(j){
				/*
				if(j.acao=='custo_produto'){
					Ext.getCmp('preco_custo_produtos').setVisible(false);
				}
				*/
			});
		});
	},

	winAddNoClose: true,

	update: function(button) {
		var me = this;
		var grid = Ext.getCmp('GridProduto_Composicoes');
		var records = grid.store.getModifiedRecords();
		var json_composicoes = [];
		for(var i in records){
			json_composicoes.push(Ext.encode(records[i].data));
		}

		/*var grid = Ext.getCmp('GridProduto_Precos');
		var records = grid.store.getModifiedRecords();
		var json_precos = [];
		for(var i in records){
			json_precos.push(Ext.encode(records[i].data));
		}*/

		/*var grid = Ext.getCmp('GridProdutos_Cores');
		var records = grid.store.getModifiedRecords();
		var json_cores = [];
		for(var i in records){
			json_cores.push(Ext.encode(records[i].data));
		}*/

		me.formParams = Ext.getCmp('FormDadosComplementares').getForm().getValues();
		me.formParams.json_composicoes = [json_composicoes];
		//me.formParams.json_precos = [json_precos];
		//me.formParams.json_cores = [json_cores];

		me.saveForm(me.gridPanel, me.getForm(), me.getAddWin(), button, function(dados){
			if(Ext.getCmp('action_produtos').getValue()=='INSERIR'){
				Ext.getCmp('action_produtos').setValue('EDITAR');
				Ext.getCmp('GridProduto_Precos').store.proxy.extraParams.id_produto = dados.id_produto;
				Ext.getCmp('GridProdutos_Logs_Sistema').setDisabled(false);
				Ext.getCmp('GridProduto_Precos').setDisabled(false);
				Ext.getCmp('GridProdutos_Cores').setDisabled(false);
				Ext.getCmp('GridProdutos_Cores').store.proxy.extraParams.id_produto = dados.id_produto;
				Ext.getCmp('TabPanelCentralProdutos').setActiveTab(3);
			}

			me.gridPanel.store.load();
		}, false);
	},

	RegistroSelecionado: function(grid, record){
		Ext.getCmp('ProdutoPrecosLocal').store.load({
			params: {
				id_produto: record.get('id')
			}
		});

		Ext.getCmp('ProdutoCoresLocal').store.load({
			params: {
				id_produto: record.get('id')
			}
		});

		Ext.getCmp('ProdutoAtributosLocal').store.load({
			params: {
				id_produto: record.get('id'),
				exibe_zerados: "N"
			}
		});

		if(this.existPermissao('produtos', 'editar')==true){
			Ext.getCmp('button_edit_produtos').setDisabled(false);
		}

		if(this.existPermissao('produtos', 'deletar')==true){
			Ext.getCmp('button_del_produtos').setDisabled(false);
		}

		if(this.existPermissao('produtos', 'copiar_produto')==true){
			Ext.getCmp('button_copiar_produtos').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.produtos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	btStoreLoadFielterQuery: function(button){
		Ext.getCmp('GridProdutos').store.proxy.extraParams.query = Ext.getCmp('query_produtos').getValue();
		Ext.getCmp('GridProdutos').store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridProdutos').store.proxy.extraParams.status_produto = item.value;
		Ext.getCmp('GridProdutos').store.load({
			start: 0,
			limit: 50
		});
	}
});