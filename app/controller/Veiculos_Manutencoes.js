/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Veiculos_Manutencoes', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Veiculos_Manutencoes',

	refs: [
		{
			ref: 'list',
			selector: 'gridveiculos_manutencoeslist'
		},
		{
			ref: 'form',
			selector: 'addveiculos_manutencoeswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'veiculos_manutencoeslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterveiculos_manutencoeswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterveiculos_manutencoeswin form'
		},
		{
			ref: 'addWin',
			selector: 'addveiculos_manutencoeswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelVeiculos_Manutencoes'
	],
	stores: [
		'StoreVeiculos_Manutencoes',
		'StoreComboOsResponsavel',
		'StoreComboCombustivelVeiculos',
		'StoreComboVeiculosManutencoes',
		'StoreComboFornecedores'
	],

	views: [
		'veiculos_manutencoes.Grid',
		'veiculos_manutencoes.List',
		'veiculos_manutencoes.Filtro',
		'veiculos_manutencoes.Edit',
	],

	init: function(application) {
		this.control({
			'gridveiculos_manutencoeslist': { 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'gridveiculos_manutencoeslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridveiculos_manutencoeslist button[action=adicionar]': {
				click: this.add
			},
			'gridveiculos_manutencoeslist button[action=editar]': {
				click: this.btedit
			},
			'gridveiculos_manutencoeslist button[action=deletar]': {
				click: this.btdel
			},
			'addveiculos_manutencoeswin button[action=salvar]': {
				click: this.update
			},
			'addveiculos_manutencoeswin button[action=resetar]': {
				click: this.reset
			},
			'addveiculos_manutencoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addveiculos_manutencoeswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addveiculos_manutencoeswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterveiculos_manutencoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterveiculos_manutencoeswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterveiculos_manutencoeswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterveiculos_manutencoeswin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddVeiculos_ManutencoesWin', 'Veiculos_Manutencoes', 'veiculos_manutencoes.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Veiculos_Manutencoes');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/veiculos_manutencoes/list.php');
			Ext.getCmp('action_veiculos_manutencoes').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'veiculos_manutencoes', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Veiculos_Manutencoes', 'Veiculos_Manutencoes', 'veiculos_manutencoes.Edit', function(){
			Ext.getCmp('cod_veiculo_veiculos_manutencoes').setValue(me.getList().store.proxy.extraParams.cod_veiculo);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('veiculos_manutencoes', 'editar')==true){
			Ext.getCmp('button_edit_veiculos_manutencoes').setDisabled(false);
		}

		if(this.existPermissao('veiculos_manutencoes', 'deletar')==true){
			Ext.getCmp('button_del_veiculos_manutencoes').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.veiculos_manutencoes.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});