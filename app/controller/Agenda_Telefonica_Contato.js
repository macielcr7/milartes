/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Agenda_Telefonica_Contato', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Agenda_Telefonica_Contato',
	started: true,
	
	refs: [
		{
			ref: 'list',
			selector: 'agenda_telefonica_contatolist'
		},
		{
			ref: 'form',
			selector: 'addagenda_telefonica_contatowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'agenda_telefonica_contatolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteragenda_telefonica_contatowin'
		},
		{
			ref: 'filterForm',
			selector: 'filteragenda_telefonica_contatowin form'
		},
		{
			ref: 'addWin',
			selector: 'addagenda_telefonica_contatowin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelAgenda_Telefonica_Contato'
	],
	stores: [
		'StoreAgenda_Telefonica_Contato',
		'StoreComboTipo_ContatoClientesContato'
	],
	
	views: [
		'agenda_telefonica_contato.List',
		'agenda_telefonica_contato.Edit'
	],

	init: function(application) {
		this.control({
			'agenda_telefonica_contatolist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'agenda_telefonica_contatolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'agenda_telefonica_contatolist button[action=adicionar]': {
				click: this.add
			},
			'agenda_telefonica_contatolist button[action=editar]': {
				click: this.btedit
			},
			'agenda_telefonica_contatolist button[action=deletar]': {
				click: this.btdel
			},
			'agenda_telefonica_contatolist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addagenda_telefonica_contatowin button[action=salvar]': {
				click: this.update
			},
			'addagenda_telefonica_contatowin button[action=resetar]': {
				click: this.reset
			},
			'addagenda_telefonica_contatowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addagenda_telefonica_contatowin form fieldcontainer combobox[name=tipo_contato]': {
				change: this.alterTextField
			},
			'addagenda_telefonica_contatowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addagenda_telefonica_contatowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteragenda_telefonica_contatowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteragenda_telefonica_contatowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteragenda_telefonica_contatowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteragenda_telefonica_contatowin': {
				show: this.filterSetFields
			}
		});
	},

	alterTextField: function(combo){
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}

		if(record.get('id')=='TELEFONE'){
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').setDisabled(false);
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').setVisible(true);

			Ext.getCmp('descricao_tel_ddg_agenda_telefonica_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_ddg_agenda_telefonica_contato').setVisible(false);
			Ext.getCmp('descricao_agenda_telefonica_contato').setDisabled(true);
			Ext.getCmp('descricao_agenda_telefonica_contato').setVisible(false);
		}
		else if(record.get('id')=='DDG'){
			Ext.getCmp('descricao_tel_ddg_agenda_telefonica_contato').setDisabled(false);
			Ext.getCmp('descricao_tel_ddg_agenda_telefonica_contato').setVisible(true);

			Ext.getCmp('descricao_agenda_telefonica_contato').setDisabled(true);
			Ext.getCmp('descricao_agenda_telefonica_contato').setVisible(false);
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').setVisible(false);
		}
		else{
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').setVisible(false);
			Ext.getCmp('descricao_tel_ddg_agenda_telefonica_contato').setDisabled(true);
			Ext.getCmp('descricao_tel_ddg_agenda_telefonica_contato').setVisible(false);

			Ext.getCmp('descricao_agenda_telefonica_contato').setDisabled(false);
			Ext.getCmp('descricao_agenda_telefonica_contato').setVisible(true);
		}
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/agenda_telefonica_contato/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddAgenda_Telefonica_ContatoWin', 'Agenda_Telefonica_Contato', 'agenda_telefonica_contato.Edit', function(){
			me.getAddWin().setTitle('Alteração de Contato');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/agenda_telefonica_contato/list.php');
			Ext.getCmp('action_agenda_telefonica_contato').setValue('EDITAR');
			Ext.getCmp('tipo_contato_agenda_telefonica_contato').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'agenda_telefonica_contato', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar o Contato: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Agenda_Telefonica_Contato', 'Agenda_Telefonica_Contato', 'agenda_telefonica_contato.Edit', function(){
			Ext.getCmp('cod_contato_agenda_telefonica_contato').setValue(me.getList().store.proxy.extraParams.cod_contato);
			Ext.getCmp('descricao_tel_agenda_telefonica_contato').focus(false, 1000);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('agenda_telefonica_contato', 'editar')==true){
			Ext.getCmp('button_edit_agenda_telefonica_contato').setDisabled(false);
		}

		if(this.existPermissao('agenda_telefonica_contato', 'deletar')==true){
			Ext.getCmp('button_del_agenda_telefonica_contato').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.agenda_telefonica_contato.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});