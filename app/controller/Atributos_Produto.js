/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Atributos_Produto', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Atributos_Produto',
	
	refs: [
		{
			ref: 'list',
			selector: 'gridatributos_produtolist'
		},
		{
			ref: 'form',
			selector: 'addatributos_produtowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridatributos_produtolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteratributos_produtowin'
		},
		{
			ref: 'filterForm',
			selector: 'filteratributos_produtowin form'
		},
		{
			ref: 'addWin',
			selector: 'addatributos_produtowin'
		}
	],
	
	models: [
		'ModelAtributos_Produto'
	],
	stores: [
		'StoreAtributos_Produto'
	],
	
	views: [
		'atributos_produto.Grid',
		'atributos_produto.List',
		'atributos_produto.Filtro',
		'atributos_produto.Edit'
	],

	init: function(application) {
		this.control({
			'gridatributos_produtolist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'gridatributos_produtolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridatributos_produtolist button[action=adicionar]': {
				click: this.add
			},
			'gridatributos_produtolist button[action=editar]': {
				click: this.btedit
			},
			'gridatributos_produtolist button[action=deletar]': {
				click: this.btdel
			},            'addatributos_produtowin button[action=salvar]': {
				click: this.update
			},
			'addatributos_produtowin button[action=resetar]': {
				click: this.reset
			},
			'addatributos_produtowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addatributos_produtowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addatributos_produtowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteratributos_produtowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteratributos_produtowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteratributos_produtowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteratributos_produtowin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddAtributos_ProdutoWin', 'Atributos_Produto', 'atributos_produto.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Atributos_Produto');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_atributos_produto'), 'server/modulos/atributos_produto/list.php');
			Ext.getCmp('action_atributos_produto').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'atributos_produto', {
			action: 'DELETAR',
			id: record.get('id_atributos_produto')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id_atributos_produto')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Atributos_Produto', 'Atributos_Produto', 'atributos_produto.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.atributos_produto.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});