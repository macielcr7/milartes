/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Util', {
	mixins: {
		observable: 'Ext.util.Observable'
	},

	titleErro: 'Erro!',
	avisoText: 'Aviso!',
	saveFormText: 'Aguarde...',
	delGridText: 'Deletando, Aguarde...',
	delErroGrid: 'Selecione um Registro para Deletar!',
	editErroGrid: 'Selecione um Registro para Editar!',
	detalharErroGrid: 'Selecione um Registro para Detalhar...',
	desativarErroGrid: 'Selecione um Registro para Desativar...',
	loadingGridText: 'Aguarde Carregando Dados...',
	filteredText: 'Filtrar <span class="buttonFilter">*</span>',
	filterText: 'Filtrar',
	connectFalhaText: 'Comunicação Ajax Perdida',
	server_failure: 'Falha no Servidor Codigo de erro: ',
	fieldsInvalidText: 'Campos com valores invalidos',
	requiredsFieldsText: 'Existem campos obrigatorios...',
	respostaOsErroGrid: 'Selecione uma OS para detalhar!',
	pdfErroGrid: 'Erro ao Gerar o PDF!',
	impressaoErroGrid: 'Selecione ao menos 1 ítem antes de Imprimir!',
	erroCancelarOs: 'Selecione uma OS para Cancelar!',
	erroFinalizarOs: 'Selecione uma OS para Finalizar!',

	storePai: false,
	started: false,

	clearInterval: false,
	disabledCombo: false,

	formParams: {},
	dependeciesControllers: [],

	initController: function(controller){
		var me = this;
		var control = me.startController(controller);
		return control;
	},

	startController: function(controller){
		var me = this;
		var control = me.application.getController(controller);
		if (control.started == undefined || control.started == null || control.started == false) {
			control.init(me.application);
			if(control.dependeciesControllers.length>0){
				me.getDependencesController(control);				
			}
			control.started = true;
		}

		return control;
	},

	getDependencesController: function(){
		var me = this;
		var controllers = me.dependeciesControllers;
		for(var i in controllers){
			me.startController(controllers[i]);
		}
	},

	existPermissao: function(modulo, acao){
		var data = this.application.dados[modulo.toLowerCase()];
		var data_i = false;
		for(var i in data){
			if(data[i].acao==acao){
				data_i = true;
			}
		}

		return data_i;
	},

	getPermissoes: function(grid){
		var me = this;
		var data = this.application.dados[this.tabela.toLowerCase()];
		var items = grid.down('toolbar').items.items;

		Ext.each(items, function(p){
			Ext.each(data, function(j){
				if(p.action && p.action==j.acao){
					p.setVisible(true);
				}
			});
		});
	},

	getDesktopWindow: function(id, Controller, className, callback){
		var me = this;
		this.application.getController('Desktop').getWindow(id, Controller, className, callback);
		if(!Ext.getCmp(id)){
			return true;
		}
	},

	getStoresDependes: function(){
		var me = this;
		if(me.getStore('StoreCombo'+me.id)){
			me.getStore('StoreCombo'+me.id).load();
		}
	},

	verifyCombo: function(comp, callbackParse){
		var me = this;
		var callback = Ext.emptyFn;
		if(me.getForm()){
			var form = me.getForm(); 
		}
		else if(me.getFilterForm()){
			var form = me.getFilterForm(); 
		}
		if(comp.xtype=='combobox' && !comp.loadDisabled){
			if(me.disabledCombo==false){
				setTimeout(function(){
					//if(comp.store.getCount()==0){
						form.el.mask(me.saveFormText);
					//}
				},10);
				me.disabledCombo = 0;
			}
			//if(comp.store.getCount()==0){
				me.disabledCombo++;
			//}
			callback = function(){
				me.disabledCombo--;
				if(me.disabledCombo==0){
					if(form){
						form.el.unmask();
						setTimeout(function(){
							if(form){
								form.el.unmask();
								var win = comp.up('window');
								win.closeEnable = true;
							}
						}, 300);

						if(typeof callbackParse == 'function'){
							callbackParse();
						}
						me.disabledCombo = 0;
					}
				}
			}
		}

		return callback;
	},

	processInterval: function(callback, combobox){
		//if(combobox){
			this.clearInterval = setInterval(function(){
				if(typeof callback === 'function'){
					callback();
				}
			},1000);
		//}
		//else{
		//	callback();
		//}
	},

	saveForm: function(grid, form, win, button, callbackSuccess, callbackFailure, modulo){
		var me = this;
		if(form.getForm().isValid()){
			form.el.mask(me.saveFormText);
			button.setDisabled(true);
			form.getForm().submit({
				params: me.formParams,
				success: function(f, o){
					button.setDisabled(false);
					form.el.unmask();
					if(o.result){
						if(o.result.success==true){
							info(me.avisoText, o.result.msg, 'success');
							
							if(grid){
								if(grid.loadDisabled && grid.loadDisabled==false){
									//nao faz nada
								}
								else{
									if (modulo == 'caixa_itens') {
										setTimeout(function(){
											grid.store.load();
										},250);
									}
									else {
										grid.store.load();
									}
								}
							}

							if(typeof callbackSuccess == 'function'){
								callbackSuccess(o.result);
							}

							if(!me.winAddNoClose){
								win.close();
							}

							if(me.storePai){
								me.getStoresDependes();
							}
						}
					}
				},
				failure: function(f, action){
					form.el.unmask();
					switch (action.failureType) {
						case Ext.form.action.Action.CLIENT_INVALID:
							info(me.titleErro, me.fieldsInvalidText, 'error');
							break;
						case Ext.form.action.Action.CONNECT_FAILURE:
							info(me.titleErro, me.connectFalhaText, 'error');
							break;
						case Ext.form.action.Action.SERVER_INVALID:
							info(me.titleErro, action.result.msg, 'error');
					}
					button.setDisabled(false);
					if(typeof callbackFailure == 'function'){
						callbackFailure();
					}
				}
			});
		}
		else{
			info(me.titleErro, me.requiredsFieldsText, 'warning');
		}
	},

	deleteAjax: function(grid, tabela, params, button, callbackSuccess){
		var me = this;
		if(button){
			button.setDisabled(true);			
		}
		grid.getEl().mask(me.delGridText);
		Ext.Ajax.request({
			url: 'server/modulos/'+tabela+'/delete.php',
			params: params,
			success: function(o){
				var o = Ext.decode(o.responseText);
				if(button){
					button.setDisabled(false);
				}
				if(o.success===true){
					info(me.avisoText, o.msg, 'success');
					
					if(me.noDeleteLoad==undefined || me.noDeleteLoad==false){
						grid.store.load();
					}
					else{
						grid.getEl().unmask();
					}
					
					if(typeof callbackSuccess == 'function'){
						callbackSuccess();
					}

					if(me.storePai){
						me.getStoresDependes();
					}
				}
				else{
					info(me.titleErro, o.msg, 'error');
					grid.getEl().unmask();
				}
			},
			failure: function(o){
				if(button){
					button.setDisabled(false);
				}
				info(me.titleErro, me.server_failure + o.status, 'error');
				grid.getEl().unmask();
			}
		});
	},

	acoesAjax: function(grid, tabela, params, button, callbackSuccess){
		var me = this;
		button.setDisabled(true);
		grid.getEl().mask(me.saveFormText);
		Ext.Ajax.request({
			url: 'server/modulos/'+tabela+'/acoes.php',
			params: params,
			success: function(o){
				var o = Ext.decode(o.responseText);
				button.setDisabled(false);
				if(o.success===true){
					info(me.avisoText, o.msg, 'success');
					grid.store.load();
					if(typeof callbackSuccess == 'function'){
						callbackSuccess();
					}

					if(me.storePai){
						me.getStoresDependes();
					}
				}
				else{
					info(me.titleErro, o.msg, 'error');
					grid.getEl().unmask();
				}
			},
			failure: function(o){
				button.setDisabled(false);
				info(me.titleErro, me.server_failure + o.status, 'error');
				grid.getEl().unmask();
			}
		});
	},

	getValuesForm: function(form, win, id, url, callback, combobox){
		var me = this;
		form.el.mask(me.saveFormText);
		win.closeEnable = false;

		Ext.Ajax.request({
			url: url,
			params: {
				id: id,
				action: 'GET_VALUES'
			},
			success: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'success');
				}
				else if(dados.success==true){
					me.processInterval(function(){
						if(me.disabledCombo<=0){
							form.getForm().setValues(dados.dados);
							form.el.unmask();
							if(typeof callback == 'function'){
								callback(dados.dados);
							}
							clearInterval(me.clearInterval);
							return true;
						}
						else{
							console.info(me.disabledCombo);
						}
						setTimeout(function(){
							clearInterval(me.clearInterval);
						}, 10000);
					}, combobox);
				}
				else{
					info(me.avisoText, dados.msg, 'error');
					if(dados.logout){
						window.location = 'login.php';
					}
				}
				if(form){
					form.el.unmask();
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}

				form.el.unmask();
			}
		});
	},

	gridLoad: function(comp){
		//comp.store.currentPage = 1;
		if(!comp.loadDisabled){
			this.getList().store.proxy.extraParams = {
				action: 'LIST'
			};

			setTimeout(function(){
				comp.store.loadPage(1);
				//comp.store.load();
			},100);
		}
	},

	comboLoad: function(comp){
		var me = this;
		
		if(!comp.loadDisabled){
			if(typeof comp.callback === 'function'){
				comp.callback(); 
			}

			if(comp.store.proxy.url!=undefined){
				var callback = me.verifyCombo(comp);
				comp.store.load({
					callback: callback
				});
			}
		}
	},

	reset: function(button){
		Ext.each(this.getForm().getForm().getFields().items, function(field){
			if(field.isVisible()==true){
				field.reset();
			}
		});
	},

	resetFielter: function(button){
		if(!this.btnFilter){
			this.btnFilter = this.getFilterBtn();
		}

		var reset = this.btnFilter.text.replace(' <span class="buttonFilter">*</span>', '');
		this.btnFilter.setText(reset);
		this.getList().store.proxy.extraParams = {
			action: 'LIST'
		};
		this.getList().store.load();
		this.getFilterWin().close();
	},

	filterSetFields: function(){
		var p = this.getList().store.proxy.extraParams;
		if(p.action=='FILTER'){
			this.getFilterForm().getForm().setValues(p);
		}
	},

	setFielter: function(button){
		var me = this;
		var form = me.getFilterForm().getValues();
		if(!me.btnFilter){
			me.btnFilter = me.getFilterBtn();
		}
		Ext.each(me.getFilterForm().getForm().getFields().items, function(field){
			if(field.xtype=='combobox'){
				form[field.name+'_nome'] = field.getRawValue();
			}
		});
		me.getList().store.proxy.extraParams = form;
		this.filterText = me.btnFilter.text;
		this.filteredText = me.btnFilter.text+' <span class="buttonFilter">*</span>'; 
		me.btnFilter.setText(this.filteredText);
		me.getList().store.load();
		me.getFilterWin().close();
	},

	enableButton: function(comp){
		var id = comp.button_id;
		if(id!=null){
			if(comp.getValue()!=null){
				Ext.getCmp(id).setVisible(true);
			}
			else{
				Ext.getCmp(id).setVisible(false);
			}
		}
	},

	getAddWindow: function(button){
		this.application.getController(button.tabela).add(button);
	},

	resetCombo: function(button){
		var id = button.combo_id;
		Ext.getCmp(id).reset();
		button.setVisible(false);
	},

	constructor: function (config) {
		this.mixins.observable.constructor.call(this, config);
	},

	verificaAnotacoesCliente: function(cliente){
		Ext.Ajax.request({
			url: 'server/modulos/clientes_anotacoes/list.php',
			method: 'POST',
			params:{
				action: 'VEIRIFICA_MENSAGENS',
				cod_cliente: cliente
			},
			success: function(s, o){
				var dados = Ext.decode(s.responseText);
				if(dados.mensagem.length > 0){
					Ext.Msg.alert('Anotações do Cadastro', dados.mensagem);
				}
			}
		});
	}
});