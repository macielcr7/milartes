/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Unidades_Medidas', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Unidades_Medidas',

	refs: [
		{
			ref: 'list',
			selector: 'unidades_medidaslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addunidades_medidaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'unidades_medidaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterunidades_medidaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterunidades_medidaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addunidades_medidaswin'
		}
	],

	models: [
		'ModelCombo',
		'ModelUnidades_Medidas'
	],
	stores: [
		'StoreComboAtivo',
		'StoreUnidades_Medidas'
	],

	views: [
		'unidades_medidas.List',
		'unidades_medidas.Edit'
	],

	init: function(application) {
		this.control({
			'unidades_medidaslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'unidades_medidaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'unidades_medidaslist button[action=adicionar]': {
				click: this.add
			},
			'unidades_medidaslist button[action=editar]': {
				click: this.btedit
			},
			'unidades_medidaslist button[action=deletar]': {
				click: this.btdel
			},
			'unidades_medidaslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addunidades_medidaswin button[action=salvar]': {
				click: this.update
			},
			'addunidades_medidaswin button[action=resetar]': {
				click: this.reset
			},
			'addunidades_medidaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterunidades_medidaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterunidades_medidaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterunidades_medidaswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterunidades_medidaswin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddUnidades_MedidasWin', 'Unidades_Medidas', 'unidades_medidas.Edit', function(){
			me.getAddWin().setTitle('Alterar Unidade de Medida');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/unidades_medidas/list.php');
			Ext.getCmp('action_unidades_medidas').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'unidades_medidas', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desativar a Unidade de Medida: '+record.get('unidade_medida')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Unidades_Medidas', 'Unidades_Medidas', 'unidades_medidas.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('unidades_medidas', 'editar')==true){
			Ext.getCmp('button_edit_unidades_medidas').setDisabled(false);
		}

		if(this.existPermissao('unidades_medidas', 'deletar')==true){
			Ext.getCmp('button_del_unidades_medidas').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_unidades_medidas').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridUnidades_Medidas').store.proxy.extraParams.status_unidade = item.value;
		Ext.getCmp('GridUnidades_Medidas').store.load({
			start: 0,
			limit: 50
		});
	}
});