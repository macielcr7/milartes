/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Empresas_Sistema', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Empresas_Sistema',
	
	refs: [
		{
			ref: 'list',
			selector: 'empresas_sistemalist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addempresas_sistemawin form'
		},
		{
			ref: 'filterBtn',
			selector: 'empresas_sistemalist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterempresas_sistemawin'
		},
		{
			ref: 'filterForm',
			selector: 'filterempresas_sistemawin form'
		},
		{
			ref: 'addWin',
			selector: 'addempresas_sistemawin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelEmpresas_Sistema'
	],
	stores: [
		'StoreComboEmpresas_Sistema',
		'StoreEmpresas_Sistema'		
	],
	
	views: [
		'empresas_sistema.List',
		'empresas_sistema.Filtro',
		'empresas_sistema.Edit'
	],

	init: function(application) {
		this.control({
			'empresas_sistemalist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'empresas_sistemalist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'empresas_sistemalist button[action=adicionar]': {
				click: this.add
			},
			'empresas_sistemalist button[action=editar]': {
				click: this.btedit
			},
			'empresas_sistemalist button[action=deletar]': {
				click: this.btdel
			},			'addempresas_sistemawin button[action=salvar]': {
				click: this.update
			},
			'addempresas_sistemawin button[action=resetar]': {
				click: this.reset
			},
			'addempresas_sistemawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addempresas_sistemawin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addempresas_sistemawin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterempresas_sistemawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterempresas_sistemawin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterempresas_sistemawin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterempresas_sistemawin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddEmpresas_SistemaWin', 'Empresas_Sistema', 'empresas_sistema.Edit', function(){
			me.getAddWin().setTitle('Altera&ccedil;&atilde;o de Dados da Empresas do Sistema');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_empresa'), 'server/modulos/empresas_sistema/list.php');
			Ext.getCmp('action_empresas_sistema').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'empresas_sistema', {
			action: 'DELETAR',
			id: record.get('cod_empresa')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar a Empresa: '+record.get('cod_empresa')+' - '+record.get('nome_fantasia')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Empresas_Sistema', 'Empresas_Sistema', 'empresas_sistema.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('empresas_sistema', 'editar')==true){
			Ext.getCmp('button_edit_empresas_sistema').setDisabled(false);
		}

		if(this.existPermissao('empresas_sistema', 'deletar')==true){
			Ext.getCmp('button_del_empresas_sistema').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.empresas_sistema.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});