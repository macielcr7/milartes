/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Contratos_Cartao', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Contratos_Cartao',

	refs: [
		{
			ref: 'list',
			selector: 'contratos_cartaolist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcontratos_cartaowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'contratos_cartaolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercontratos_cartaowin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercontratos_cartaowin form'
		},
		{
			ref: 'addWin',
			selector: 'addcontratos_cartaowin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelContratos_Cartao'
	],
	stores: [
		'StoreComboAtivo',
		'StoreComboContratosContaBancaria',
		'StoreComboContratosCartaoTipo',
		'StoreComboContratosCartaoTipoCalculo',
		'StoreContratos_Cartao'
	],
	
	views: [
		'contratos_cartao.List',
		'contratos_cartao.Edit'
	],

	init: function(application) {
		this.control({
			'contratos_cartaolist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'contratos_cartaolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'contratos_cartaolist button[action=adicionar]': {
				click: this.add
			},
			'contratos_cartaolist button[action=editar]': {
				click: this.btedit
			},
			'contratos_cartaolist button[action=deletar]': {
				click: this.btdel
			},
			'contratos_cartaolist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addcontratos_cartaowin button[action=salvar]': {
				click: this.update
			},
			'addcontratos_cartaowin button[action=resetar]': {
				click: this.reset
			},
			'addcontratos_cartaowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addcontratos_cartaowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercontratos_cartaowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercontratos_cartaowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercontratos_cartaowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercontratos_cartaowin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddContratos_CartaoWin', 'Contratos_Cartao', 'contratos_cartao.Edit', function(){
			me.getAddWin().setTitle('Alteração do Contrato de Cartão de Crédito');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/contratos_cartao/list.php');
			Ext.getCmp('action_contratos_cartao').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'contratos_cartao', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desativar o Contrato: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Contratos_Cartao', 'Contratos_Cartao', 'contratos_cartao.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('contratos_cartao', 'editar')==true){
			Ext.getCmp('button_edit_contratos_cartao').setDisabled(false);
		}

		if(this.existPermissao('contratos_cartao', 'deletar')==true){
			Ext.getCmp('button_del_contratos_cartao').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_contratos_cartao').getValue();
		this.getList().store.load({
			start: 0,
			limit: 25
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridContratos_Cartao').store.proxy.extraParams.status_cartao = item.value;
		Ext.getCmp('GridContratos_Cartao').store.load({
			start: 0,
			limit: 50
		});
	}
});