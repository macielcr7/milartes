/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Correios_Enderecos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Correios_Enderecos',
	
	refs: [
		{
			ref: 'list',
			selector: 'correios_enderecoslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcorreios_enderecoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'correios_enderecoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercorreios_enderecoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercorreios_enderecoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcorreios_enderecoswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelCombo',
		'ModelCorreios_Enderecos'
	],
	stores: [
		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboAtivo',
		'StoreComboTipoCorreiosEnderecos2',
		'StoreComboCorreios_Bairros',
		'StoreComboCorreios_Enderecos',
		'StoreCorreios_Enderecos'
	],
	
	views: [
		'correios_enderecos.List',
		'correios_enderecos.Filtro',
		'correios_enderecos.Edit'
	],

	init: function(application) {
		this.control({
			'correios_enderecoslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'correios_enderecoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'correios_enderecoslist button[action=adicionar]': {
				click: this.add
			},
			'correios_enderecoslist button[action=editar]': {
				click: this.btedit
			},
			'correios_enderecoslist button[action=deletar]': {
				click: this.btdel
			},			
			'addcorreios_enderecoswin button[action=salvar]': {
				click: this.update
			},
			'addcorreios_enderecoswin button[action=resetar]': {
				click: this.reset
			},
			'addcorreios_enderecoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcorreios_enderecoswin form fieldcontainer combobox[id=uf_sigla_correios_enderecos]': {
				change: this.loadCidade
			},
			'addcorreios_enderecoswin form fieldcontainer combobox[id=localidade_id_correios_enderecos]': {
				change: this.loadBairro
			},
			'addcorreios_enderecoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcorreios_enderecoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercorreios_enderecoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercorreios_enderecoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercorreios_enderecoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercorreios_enderecoswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	bairro: false,

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('localidade_id_correios_enderecos');
		}
		else{
			var comboCidade = Ext.getCmp('localidade_id_filter_correios_enderecos');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade1 = Ext.getCmp('bairro_id_inicial_correios_enderecos');
		}
		else{
			var comboCidade1 = Ext.getCmp('bairro_id_inicial_filter_correios_enderecos');
		}
		if(comboCidade1.isDisabled()){
			comboCidade1.setDisabled(false);
		}
		comboCidade1.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade1.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddCorreios_EnderecosWin', 'Correios_Enderecos', 'correios_enderecos.Edit', function(){
			me.getAddWin().setTitle('Alteração de Enderecos');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/correios_enderecos/list.php');
			Ext.getCmp('action_correios_enderecos').setValue('EDITAR');
			Ext.getCmp('nome_correios_enderecos').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'correios_enderecos', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Desativar este Endereço: '+record.get('nome')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.cidade = false;
		me.bairro = false;
		
		me.getDesktopWindow('Correios_Enderecos', 'Correios_Enderecos', 'correios_enderecos.Edit', function(){
			var frm = button.up('form');

			//form clientes
			if(frm!=null && frm.down('combobox[id=estado_clientes]')){
				var v = Ext.getCmp('estado_clientes').getValue();
				if(v!="" && v!=null){
					Ext.getCmp('uf_sigla_correios_enderecos').setValue(v);
				}
			}
			//form fornecedores
			else if(frm!=null && frm.down('combobox[id=endereco_fornecedores]')){
				var v = Ext.getCmp('endereco_fornecedores').getValue();
				if(v!="" && v!=null){
					Ext.getCmp('uf_sigla_correios_enderecos').setValue(v);
				}
			}
			//listagem de grid
			else{
				var list = Ext.getCmp('GridCorreios_Estados');
				if(list && list.selModel.hasSelection()){
					var relEst = list.getSelectionModel().getLastSelected();
					Ext.getCmp('uf_sigla_correios_enderecos').setValue(relEst.raw.uf);
				}
			}
			
			var a = setInterval(function(){
				//form clientes
				if(Ext.getCmp('cidade_clientes')){
					if(me.cidade==true){
						var v = Ext.getCmp('cidade_clientes').getValue();
						if(v!="" && v!=null){
							Ext.getCmp('localidade_id_correios_enderecos').setValue(v);
						}						
						
						me.cidade = null;
					}
				}
				//form fornecedores
				else if(Ext.getCmp('cidade_fornecedores')){
					if(me.cidade==true){
						var v = Ext.getCmp('cidade_fornecedores').getValue();
						if(v!="" && v!=null){
							Ext.getCmp('localidade_id_correios_enderecos').setValue(v);
						}						
						
						me.cidade = null;
					}
				}
				else{
					var list = Ext.getCmp('GridCorreios_Localidades');
					if(list && list.selModel.hasSelection()){
						var relEst = list.getSelectionModel().getLastSelected();
						Ext.getCmp('localidade_id_correios_enderecos').setValue(relEst.raw.id);
					}
					me.cidade = null;
				}

				//form clientes
				if(Ext.getCmp('bairro_clientes')){
					if(me.bairro==true){
						var v = Ext.getCmp('bairro_clientes').getValue();
						if(v!="" && v!=null){
							Ext.getCmp('bairro_id_inicial_correios_enderecos').setValue(v);
						}
						me.bairro = null;
					}
				}
				//form fornecedores
				else if(Ext.getCmp('bairro_fornecedores')){
					if(me.bairro==true){
						var v = Ext.getCmp('bairro_fornecedores').getValue();
						if(v!="" && v!=null){
							Ext.getCmp('bairro_id_inicial_correios_enderecos').setValue(v);
						}
						me.bairro = null;
					}
				}
				else{
					var list = Ext.getCmp('GridCorreios_Bairros');
					if(list && list.selModel.hasSelection()){
						var relEst = list.getSelectionModel().getLastSelected();
						Ext.getCmp('bairro_id_inicial_correios_enderecos').setValue(relEst.raw.id);
					}
					me.bairro = null;
				}

				if(me.cidade == null && me.bairro == null){
					clearInterval(a);
				}
			}, 1000);
			setTimeout(function(){
					clearInterval(a);
			}, 15000);
		});
		Ext.getCmp('tipo_correios_enderecos').focus(false, 1000);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('correios_enderecos', 'editar')==true){
			Ext.getCmp('button_edit_correios_enderecos').setDisabled(false);
		}

		if(this.existPermissao('correios_enderecos', 'deletar')==true){
			Ext.getCmp('button_del_correios_enderecos').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_correios_enderecos').getValue();
		this.getList().store.load({
			start: 0,
			limit: 30
		});
	}
});