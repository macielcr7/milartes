/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Veiculos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Veiculos',
	dependeciesControllers: [
		'Veiculos_Combustivel',
		'Veiculos_Manutencoes'
	],
	started: false,
	refs: [
		{
			ref: 'list',
			selector: 'gridveiculoslist'
		},
		{
			ref: 'form',
			selector: 'addveiculoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'veiculoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterveiculoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterveiculoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addveiculoswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelVeiculos'
	],
	stores: [
		'StoreComboUnidades_Medidas',
		'StoreComboAtivoVeiculos',
		'StoreVeiculos',
		'StoreComboGrupoVeiculos',
		'StoreComboCombustivelVeiculos',
		'StoreVeiculos_Logs_Sistema',
		'StoreVeiculos_Combustivel',
		'StoreVeiculos_Manutencoes'
	],

	views: [
		'veiculos.List',
		'veiculos.Grid',
		'veiculos.Filtro',
		'veiculos.Edit'
	],

	init: function(application) {
		this.control({
			'veiculoslist gridpanel': { 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'veiculoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'veiculoslist button[action=adicionar]': {
				click: this.add
			},
			'veiculoslist button[action=editar]': {
				click: this.btedit
			},
			'veiculoslist button[action=deletar]': {
				click: this.btdel
			},
			'veiculoslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'veiculoslist button[action=abastecimentos]': {
				click: this.abastecimentos
			},
			'veiculoslist button[action=manutencoes]': {
				click: this.manutencoes
			},
			'veiculoslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addveiculoswin tabpanel': {
				tabchange: this.loadActive
			},
			'addveiculoswin button[action=salvar]': {
				click: this.update
			},
			'addveiculoswin button[action=resetar]': {
				click: this.reset
			},
			'addveiculoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addveiculoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addveiculoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterveiculoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterveiculoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterveiculoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterveiculoswin': {
				show: this.filterSetFields
			}
		});
	},

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/veiculos/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},

	abastecimentos: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('List-Veiculos_Combustivel', 'Veiculos_Combustivel', 'veiculos_combustivel.List', function(controller){
				Ext.getCmp('List-Veiculos_Combustivel').setTitle('Abastecimento Para: '+record.get('descricao')+': '+record.get('placa'));
				controller.veiculos = record.get('id');
				controller.getList().store.proxy.extraParams.cod_veiculo = record.get('id');
				controller.getList().store.load();
			});
		}
		else{
			info(me.titleErro, me.respostaOsErroGrid);
			return true;
		}
	},

	manutencoes: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('List-Veiculos_Manutencoes', 'Veiculos_Manutencoes', 'veiculos_manutencoes.List', function(controller){
				Ext.getCmp('List-Veiculos_Manutencoes').setTitle('Manutenções Para: '+record.get('descricao')+': '+record.get('placa'));
				controller.veiculos = record.get('id');
				controller.getList().store.proxy.extraParams.cod_veiculo = record.get('id');
				controller.getList().store.load();
			});
		}
		else{
			info(me.titleErro, me.respostaOsErroGrid);
			return true;
		}
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddVeiculosWin', 'Veiculos', 'veiculos.Edit', function(dados){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Veiculos');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/veiculos/list.php');
			Ext.getCmp('action_veiculos').setValue('EDITAR');

			Ext.getCmp('GridVeiculos_Combustivel').setDisabled(false);
			Ext.getCmp('GridVeiculos_Combustivel').store.proxy.extraParams.cod_veiculo = record.get('id');
			Ext.getCmp('GridVeiculos_Manutencoes').setDisabled(false);
			Ext.getCmp('GridVeiculos_Manutencoes').store.proxy.extraParams.cod_veiculo = record.get('id');

			var data = me.application.dados[me.tabela.toLowerCase()];
			Ext.each(data, function(j){
				if(j.acao=='veiculos_logs'){
					Ext.getCmp('GridVeiculos_Logs_Sistema').setDisabled(false);
					Ext.getCmp('GridVeiculos_Logs_Sistema').store.proxy.extraParams.tabela_cod = record.get('id');
					Ext.getCmp('GridVeiculos_Logs_Sistema').store.proxy.extraParams.modulo = 'veiculos';
				}
			});
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'veiculos', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Desativar o Veículo: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;

		me.getDesktopWindow('Veiculos', 'Veiculos', 'veiculos.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(dados){
/*
			if(Ext.getCmp('action_veiculos').getValue()=='INSERIR'){
				Ext.getCmp('action_veiculos').setValue('EDITAR');
				Ext.getCmp('id_veiculos').setValue(dados.id);
				Ext.getCmp('GridVeiculos_Combustivel').setDisabled(false);
				Ext.getCmp('GridVeiculos_Manutencoes').setDisabled(false);
				Ext.getCmp('GridClientes_Logs_Sistema').setDisabled(false);
			}
*/
		}, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('veiculos', 'editar')==true){
			Ext.getCmp('button_edit_veiculos').setDisabled(false);
		}

		if(this.existPermissao('veiculos', 'deletar')==true){
			Ext.getCmp('button_del_veiculos').setDisabled(false);
		}

		if(this.existPermissao('veiculos', 'abastecimentos')==true){
			Ext.getCmp('button_abastecimentos_veiculos').setDisabled(false);
		}

		if(this.existPermissao('veiculos', 'manutencoes')==true){
			Ext.getCmp('button_manutecoes_veiculos').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.veiculos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridVeiculos').store.proxy.extraParams.status_veiculo = item.value;
		Ext.getCmp('GridVeiculos').store.load({
			start: 0,
			limit: 25
		});
	}
});