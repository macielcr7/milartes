/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.controller.Feriados', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Feriados',

	refs: [
		{
			ref: 'list',
			selector: 'feriadoslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addferiadoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'feriadoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterferiadoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterferiadoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addferiadoswin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelFeriados'
	],
	stores: [
		'StoreFeriados'
	],
	
	views: [
		'feriados.List',
		'feriados.Edit'
	],

	init: function(application) {
		this.control({
			'feriadoslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'feriadoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'feriadoslist button[action=adicionar]': {
				click: this.add
			},
			'feriadoslist button[action=editar]': {
				click: this.btedit
			},
			'feriadoslist button[action=deletar]': {
				click: this.btdel
			},
			'addferiadoswin button[action=salvar]': {
				click: this.update
			},
			'addferiadoswin button[action=resetar]': {
				click: this.reset
			},
			'addferiadoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterferiadoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterferiadoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterferiadoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterferiadoswin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddFeriadosWin', 'Feriados', 'feriados.Edit', function(){
			me.getAddWin().setTitle('Alterar Feriado');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_feriado'), 'server/modulos/feriados/list.php');
			Ext.getCmp('action_feriados').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'feriados', {
			action: 'DELETAR',
			id: record.get('cod_feriado')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar o Feriado: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Feriados', 'Feriados', 'feriados.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('feriados', 'editar')==true){
			Ext.getCmp('button_edit_feriados').setDisabled(false);
		}

		if(this.existPermissao('feriados', 'deletar')==true){
			Ext.getCmp('button_del_feriados').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_feriados').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	}
});