/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produto_Precos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Produto_Precos',
	noDeleteLoad: false,
	
	refs: [
		{
			ref: 'list',
			selector: 'gridproduto_precoslist'
		},
		{
			ref: 'form',
			selector: 'addproduto_precoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridproduto_precoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterproduto_precoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterproduto_precoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addproduto_precoswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelProduto_Precos'
	],
	stores: [
		'StoreComboCategoria_Precos',
		'StoreComboProdutos',
		'StoreProduto_Precos'
	],
	
	views: [
		'produto_precos.Grid',
		'produto_precos.List',
		'produto_precos.Filtro',
		'produto_precos.Edit'
	],

	init: function(application) {
		this.control({
			'gridproduto_precoslist': {
				afterrender: this.getPermissoes,
						render: this.gridLoad
			},
			'gridproduto_precoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridproduto_precoslist button[action=adicionar]': {
				click: this.add
			},
			'gridproduto_precoslist button[action=editar]': {
				click: this.btedit
			},
			'gridproduto_precoslist button[action=deletar]': {
				click: this.btdel
			},            
			'addproduto_precoswin button[action=salvar]': {
				click: this.update
			},
			'addproduto_precoswin button[action=resetar]': {
				click: this.reset
			},
			'addproduto_precoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addproduto_precoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addproduto_precoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterproduto_precoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterproduto_precoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterproduto_precoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterproduto_precoswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddProduto_PrecosWin', 'Produto_Precos', 'produto_precos.Edit', function(){
			me.getAddWin().setTitle('Alteração de Preço de Venda');

			if(record.get('id_produto')>0){
				me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_produto_precos'), 'server/modulos/produto_precos/list.php');
			}
			else{
				me.getForm().getForm().setValues(record.data);
			}

			Ext.getCmp('action_produto_precos').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;

		//grid.store.remove(record);
		if(record.get('id_produto_precos')>0){
			me.deleteAjax(grid, 'produto_precos', {
				action: 'DELETAR',
				id: record.get('id_produto_precos')
			}, button, false);            
		}
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Produto_Precos', 'Produto_Precos', 'produto_precos.Edit', function(){
			Ext.getCmp('id_produto_produto_precos').setValue(me.getList().store.proxy.extraParams.id_produto);
		});
	},

	update: function(button) {
		var me = this;
		/*var values = me.getForm().getForm().getValues();
		
		var record = me.getList().store.findRecord('id_categoria_precos', values.id_categoria_precos);
		if(record){
			record.set('valor', values.valor);
		}
		else{
			var record = Ext.getCmp('id_categoria_precos_produto_precos').store.getById(values.id_categoria_precos);
			if(!record){
				var record = Ext.getCmp('id_categoria_precos_produto_precos').store.getById(parseInt(values.id_categoria_precos));
			}
			me.getList().store.add({
				id_produto: values.id_produto,
				id_categoria_precos: values.id_categoria_precos,
				descricao: record.get('descricao'),
				valor: values.valor
			});
		}

		me.getAddWin().close();*/		
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.produto_precos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});
