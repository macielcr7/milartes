/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Os_Respostas', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Os_Respostas',
	started: true,
	refs: [
		{
			ref: 'list',
			selector: 'gridos_respostaslist'
		},
		{
			ref: 'form',
			selector: 'addos_respostaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'os_respostaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteros_respostaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filteros_respostaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addos_respostaswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelOs_Respostas'
	],
	stores: [
		'StoreComboUsuarios',
		'StoreComboOs',
		'StoreOs_Respostas'
	],

	views: [
		'os_respostas.Grid',
		'os_respostas.List',
		'os_respostas.Filtro',
		'os_respostas.Edit'
	],

	init: function(application) {
		this.control({
			'gridos_respostaslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'gridos_respostaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridos_respostaslist button[action=adicionar]': {
				click: this.add
			},
			'gridos_respostaslist button[action=editar]': {
				click: this.btedit
			},
			'gridos_respostaslist button[action=deletar]': {
				click: this.btdel
			},
			'addos_respostaswin button[action=salvar]': {
				click: this.update
			},
			'addos_respostaswin button[action=resetar]': {
				click: this.reset
			},
			'addos_respostaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addos_respostaswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addos_respostaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteros_respostaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteros_respostaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteros_respostaswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteros_respostaswin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddOs_RespostasWin', 'Os_Respostas', 'os_respostas.Edit', function(){
			me.getAddWin().setTitle('Altera��o de Resposta da OS');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/os_respostas/list.php');
			Ext.getCmp('action_os_respostas').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'os_respostas', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Remover a Resposta: '+record.get('conteudo')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Os_Respostas', 'Os_Respostas', 'os_respostas.Edit', function(){
			Ext.getCmp('os_os_respostas').setValue(me.os);
			Ext.getCmp('usuario_os_respostas').setValue(userData.id);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('os_respostas', 'editar')==true){
			Ext.getCmp('button_edit_os_respostas').setDisabled(false);
		}

		if(this.existPermissao('os_respostas', 'deletar')==true){
			Ext.getCmp('button_del_os_respostas').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.os_respostas.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});
