/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Os_Categorias', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Os_Categorias',

	refs: [
		{
			ref: 'list',
			selector: 'os_categoriaslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addos_categoriaswin form'
		},
		{
			ref: 'filterWin',
			selector: 'filteros_categoriaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filteros_categoriaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addos_categoriaswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelCombo',
		'ModelOs_Categorias'
	],
	stores: [
		'StoreComboStatusOs_Categorias',
		'StoreComboOs_Categorias',
		'StoreOs_Categorias'
	],
	
	views: [
		'os_categorias.List',
		'os_categorias.Edit'
	],

	init: function(application) {
		this.control({
			'os_categoriaslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'os_categoriaslist button[action=adicionar]': {
				click: this.add
			},
			'os_categoriaslist button[action=editar]': {
				click: this.btedit
			},
			'os_categoriaslist button[action=deletar]': {
				click: this.btdel
			},
			'addos_categoriaswin button[action=salvar]': {
				click: this.update
			},
			'addos_categoriaswin button[action=resetar]': {
				click: this.reset
			},
			'addos_categoriaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addos_categoriaswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addos_categoriaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteros_categoriaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteros_categoriaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteros_categoriaswin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddOs_CategoriasWin', 'Os_Categorias', 'os_categorias.Edit', function(){
			me.getAddWin().setTitle('Editar Categoria de OS');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/os_categorias/list.php');
			Ext.getCmp('action_os_categorias').setValue('EDITAR');
			Ext.getCmp('descricao_os_categorias').focus(false, 1000);
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'os_categorias', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Desativar: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Os_Categorias', 'Os_Categorias', 'os_categorias.Edit', false);
		Ext.getCmp('descricao_os_categorias').focus(false, 1000);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('os_categorias', 'editar')==true){
			Ext.getCmp('button_edit_os_categorias').setDisabled(false);
		}

		if(this.existPermissao('os_categorias', 'deletar')==true){
			Ext.getCmp('button_del_os_categorias').setDisabled(false);
		}
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	}
});