/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Vendas_Custos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Vendas_Custos',
	
	refs: [
		{
			ref: 'list',
			selector: 'vendas_custoslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addvendas_custoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'vendas_custoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtervendas_custoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtervendas_custoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addvendas_custoswin'
		}
	],
	
	models: [
		'ModelVendas_Custos'
	],
	stores: [
		'StoreVendas_Custos',
		'StoreComboProdutosCusto'
	],
	
	views: [
		'vendas_custos.List',
		'vendas_custos.Grid',
		'vendas_custos.Edit'
	],

	init: function(application) {
		this.control({
			'vendas_custoslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'vendas_custoslist button[action=adicionar]': {
				click: this.add
			},
			'vendas_custoslist button[action=editar]': {
				click: this.btedit
			},
			'vendas_custoslist button[action=deletar]': {
				click: this.btdel
			},            'addvendas_custoswin button[action=salvar]': {
				click: this.update
			},
			'addvendas_custoswin button[action=resetar]': {
				click: this.reset
			},
			'addvendas_custoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addvendas_custoswin form fieldcontainer combobox[id=cod_produto_vendas_custos]': {
				change: this.setValorProduto
			},
			'addvendas_custoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addvendas_custoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtervendas_custoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtervendas_custoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtervendas_custoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtervendas_custoswin': {
				show: this.filterSetFields
			}
		});
	},

	setValorProduto: function(combo, value){
		console.log('setValorProduto');
		var me = this;
		var recordProduto = combo.store.getById(value);
		var comboCor = Ext.getCmp('cor_vendas_custos');

		if (recordProduto!=null && value!=null && !isNaN(parseInt(value))) {
			var qtde_cores = recordProduto.raw.produto_cores.length;

			if (qtde_cores > 0) {
				comboCor.setDisabled(false);
				comboCor.setVisible(true);

				comboCor.store.proxy.extraParams.id_produto = combo.getValue();
				comboCor.store.load({
					callback: function(r, options, success){
						comboCor.setValue(r[0].data.id);
						if (Ext.getCmp('quantidade_vendas_custos').getValue() == null) {
							Ext.getCmp('quantidade_vendas_custos').setValue(1);
						}
						
						if (qtde_cores == 1) {
							Ext.getCmp('quantidade_vendas_custos').focus(false, 10);
						}
						else {
							Ext.getCmp('cor_vendas_custos').focus(false, 10);
						}
					}
				});
			}
			else {
				Ext.getCmp('cor_vendas_custos').reset();
				comboCor.store.removeAll();
	 			comboCor.setDisabled(true);
				comboCor.setVisible(false);
			}

			if(recordProduto){
				window.StoreProdutoItemOrcamento = combo.store;
				Ext.getCmp('quantidade_vendas_custos').setMaxValue(recordProduto.raw.quantidade_disponivel);
				Ext.getCmp('valor_unitario_vendas_custos').setValue(me.getValorRecord(recordProduto));            
			}
			else{
				//info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria!!!');
			}
		}
		else {
			Ext.getCmp('cor_vendas_custos').reset();
			comboCor.store.removeAll();
 			comboCor.setDisabled(true);
			comboCor.setVisible(false);
		}
	},

	getValorRecord: function(recordProduto){
		console.log('getValorRecord');
		console.log(recordProduto);
		var preco = recordProduto.raw.preco_custo;

		if(preco==undefined || preco==null){
			info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria');
		}

		return preco;
	},

	add: function(button) {
		console.log('add');
		var me = this;

		me.getDesktopWindow('Vendas_Custos', 'Vendas_Custos', 'vendas_custos.Edit', function(){
			var cod_venda = me.getList().store.proxy.extraParams.cod_venda;
			me.getAddWin().setTitle('Adicionando Custos para Venda Nro: '+cod_venda);
			Ext.getCmp('cod_venda_vendas_custos').setValue(cod_venda);

			if (userData.perfil_id == 1) {
				Ext.getCmp('valor_unitario_vendas_custos').setVisible(true);
			}
		});
	},

	update: function(button) {
		console.log('update');
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddVendas_CustosWin', 'Vendas_Custos', 'vendas_custos.Edit', function(){
			me.getAddWin().setTitle('Alteração dos Custos da Venda');
			var data = record.data;
			data.cod_produto = ""+data.cod_produto+"";
			data.cod_cor = ""+data.cod_cor+"";
			//console.log(data);
			me.getForm().getForm().setValues(data);
			//me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/vendas_custos/list.php');
			Ext.getCmp('action_vendas_custos').setValue('EDITAR');

			setTimeout(function() {
				if(data.cod_cor != null && data.cod_cor > 0){
					Ext.getCmp('cor_vendas_custos').setValue(''+data.cod_cor+'');
				}
			}, 1000);

			if (userData.perfil_id == 1) {
				Ext.getCmp('valor_unitario_vendas_custos').setVisible(true);
			}
		});
	},

	del: function(grid, record, button) {
		console.log('del');
		var me = this;
		me.deleteAjax(grid, 'vendas_custos', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		console.log('btedit');
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		console.log('btdel');
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('produto')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	}
});