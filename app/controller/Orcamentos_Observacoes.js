/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Orcamentos_Observacoes', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Orcamentos_Observacoes',
	started: true,
	noDeleteLoad: true,

	refs: [
		{
			ref: 'list',
			selector: 'gridorcamentos_observacoeslist'
		},
		{
			ref: 'form',
			selector: 'addorcamentos_observacoeswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridorcamentos_observacoeslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterorcamentos_observacoeswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterorcamentos_observacoeswin form'
		},
		{
			ref: 'addWin',
			selector: 'addorcamentos_observacoeswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelOrcamentos_Observacoes'
	],
	stores: [
		'StoreComboProdutos',
		'StoreOrcamentos_Observacoes',
		'StoreComboOrcamentosObservacoesTipo'
	],
	
	views: [
		'orcamentos_observacoes.Grid',
		'orcamentos_observacoes.List',
		'orcamentos_observacoes.Edit'
	],

	init: function(application) {
		this.control({
			'gridorcamentos_observacoeslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'gridorcamentos_observacoeslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridorcamentos_observacoeslist button[action=adicionar]': {
				click: this.add
			},
			'gridorcamentos_observacoeslist button[action=editar]': {
				click: this.btedit
			},
			'gridorcamentos_observacoeslist button[action=deletar]': {
				click: this.btdel
			},            
			'addorcamentos_observacoeswin button[action=salvar]': {
				click: this.update
			},
			'addorcamentos_observacoeswin button[action=resetar]': {
				click: this.reset
			},
			'addorcamentos_observacoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addorcamentos_observacoeswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addorcamentos_observacoeswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddOrcamentos_ObservacoesWin', 'Orcamentos_Observacoes', 'orcamentos_observacoes.Edit', function(){
			me.getAddWin().setTitle('Alteração da Observação');
			me.getForm().getForm().setValues(record.data);

			Ext.getCmp('id_orcamentos_observacoes').setValue(''+record.data.id+'');


			//Tipo de Orçamento: 0=geral/tradicional, 1=forro, 2=persianas, 3=pisos, 4=rodapés
			var tipo_orcamento = me.getList().store.proxy.extraParams.tipo_orcamento;

			if(tipo_orcamento==0){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos').getValue();
			}
			else if(tipo_orcamento==1){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos_forro').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos_forro').getValue();
			}
			else if(tipo_orcamento==2){
				var orcamento_id = Ext.getCmp('xxxx').getValue();
				var maximo = Ext.getCmp('xxxx').getValue();
			}
			else if(tipo_orcamento==3){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos_piso').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos_piso').getValue();
			}
			else if(tipo_orcamento==4){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos_rodape').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos_rodape').getValue();
			}

			Ext.getCmp('nro_item_orcamentos_observacoes').setMinValue(0);
			Ext.getCmp('nro_item_orcamentos_observacoes').setMaxValue(maximo);
			Ext.getCmp('orcamento_id_orcamentos_observacoes').setValue(orcamento_id);
			Ext.getCmp('action_orcamentos_observacoes').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		grid.store.remove(record);
		if(record.get('id')==0){
			return true;
		}

		me.deleteAjax(grid, 'orcamentos_observacoes', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, function(){
			Ext.getCmp('GridOrcamentos_Observacoes').store.load();
		});
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Orcamentos_Observacoes', 'Orcamentos_Observacoes', 'orcamentos_observacoes.Edit', function(){
			//Tipo de Orçamento: 0=geral/tradicional, 1=forro, 2=persianas, 3=pisos, 4=rodapés
			var tipo_orcamento = me.getList().store.proxy.extraParams.tipo_orcamento;

			if(tipo_orcamento==0){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos').getValue();
			}
			else if(tipo_orcamento==1){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos_forro').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos_forro').getValue();
			}
			else if(tipo_orcamento==2){
				var orcamento_id = Ext.getCmp('xxxx').getValue();
				var maximo = Ext.getCmp('xxxx').getValue();
			}
			else if(tipo_orcamento==3){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos_piso').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos_piso').getValue();
			}
			else if(tipo_orcamento==4){
				var orcamento_id = Ext.getCmp('id_orcamento_orcamentos_rodape').getValue();
				var maximo = Ext.getCmp('total_de_itens_orcamentos_rodape').getValue();
			}

			Ext.getCmp('nro_item_orcamentos_observacoes').setMinValue(0);
			Ext.getCmp('nro_item_orcamentos_observacoes').setMaxValue(maximo);
			Ext.getCmp('orcamento_id_orcamentos_observacoes').setValue(orcamento_id);
		});
	},



	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.orcamentos_observacoes.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});