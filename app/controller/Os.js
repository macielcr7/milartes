/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Os', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Os',
	started: true,
	refs: [
		{
			ref: 'list',
			selector: 'gridoslist'
		},
		{
			ref: 'form',
			selector: 'addoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'oslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteroswin'
		},
		{
			ref: 'filterForm',
			selector: 'filteroswin form'
		},
		{
			ref: 'addWin',
			selector: 'addoswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelCombo',
		'ModelOs'
	],
	stores: [
		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboOs_Categorias',
		'StoreComboCorreios_Enderecos',
		'StoreComboStatusOs',
		'StoreComboUrgenciaOs',
		'StoreComboOsEmpresas',
		'StoreComboOsResponsavel',
		'StoreComboOs',
		'StoreOs',
		'StoreComboProdutos_CategoriasOs',
		'StoreComboUsuarios',
		'StoreComboStatusOsDiferente',
		'StoreComboUsuarioResponsavel',
		'StoreOS_Logs_Sistema',
		'StoreOs_Respostas'
	],

	views: [
		'clientes.OsGrid',
		'os.Grid',
		'os.List',
		'os.Filtro',
		'os.Edit',
		'os.Cancelamento',
		'os.Finalizacao'
	],

	init: function(application) {
		this.control({
			'gridoslist, gridclientesoslist': {
				afterrender: this.getPermissoes2,
				select: this.RegistroSelecionado,
				render: this.gridLoad
			},
			'oslist button[action=filtrar], gridoslist button menuitem[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'oslist combobox[action=filtrar2]': {
				select: this.btStoreLoadFielter2
			},
			'oslist button[action=adicionar], gridclientesoslist button[action=adicionar]': {
				click: this.add
			},
			'oslist button[action=editar], gridclientesoslist button[action=editar]': {
				click: this.btedit
			},
			'oslist button[action=deletar], gridclientesoslist button[action=deletar]': {
				click: this.cancelarOS
			},
			'oslist button[action=finalizar], gridclientesoslist button[action=finalizar]': {
				click: this.finalizarOS
			},
			'oslist button[action=reabrir], gridclientesoslist button[action=reabrir]': {
				click: this.reabrirOS
			},
			'oslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'oslist button[action=os_respostas], gridclientesoslist button[action=os_respostas]': {
				click: this.respostas
			},
			'oslist button menuitem[action=os_departamentos]': {
				click: this.Departamentos
			},
			'oslist button menuitem[action=os_categorias]': {
				click: this.Categorias
			},
			'addoswin tabpanel': {
				tabchange: this.loadActive
			},
			'addoswin button[action=salvar]': {
				click: this.update
			},
			'addoswin button[action=resetar]': {
				click: this.reset
			},
			'addoswin form fieldcontainer combobox': {
				change: this.enableButton,
				afterrender: this.comboLoad
			},
			'addoswin form fieldcontainer combobox[name=estado]': {
				change: this.loadCidade
			},
			'addoswin form fieldcontainer combobox[name=cidade]': {
				change: this.loadBairro
			},
			'addoswin form fieldcontainer combobox[name=endereco]': {
				change: this.setEndereco
			},
			'addoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'filteroswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteroswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteroswin form fieldcontainer combobox[name=estado]': {
				change: this.loadCidade
			},
			'filteroswin form fieldcontainer combobox[name=cidade]': {
				change: this.loadBairro
			},
			'filteroswin form fieldcontainer combobox[name=endereco]': {
				change: this.setEndereco
			},
			'filteroswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteroswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteroswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	endereco: false,
	gridPanel: false,

	getPermissoes2: function(grid){
		var me = this;
		var data = this.application.dados[this.tabela.toLowerCase()];
		var items = grid.down('toolbar').items.items;
		
		Ext.each(items, function(p){
			Ext.each(data, function(j){
				if(p.action && p.action==j.acao){
					p.setVisible(true);
				}
			});
		});

		if(this.existPermissao('os', 'filtrar')==true){
			var filtro1 = Ext.ComponentQuery.query('oslist [name=filtro_1]');
			var filtro2 = Ext.ComponentQuery.query('oslist [name=filtro_2]');
			
			if(filtro1.length>0){
				filtro1[0].setVisible(false);
			}
			if(filtro2.length>0){
				filtro2[0].setVisible(true);
			}
		}
		if(this.existPermissao('os', 'os_departamentos')==true){
			var filtro1 = Ext.ComponentQuery.query('oslist [action=os_departamentos]');
			if(filtro1.length>0){
				filtro1[0].setVisible(true);
			}
		}
		if(this.existPermissao('os', 'os_categorias')==true){
			var filtro1 = Ext.ComponentQuery.query('oslist [action=os_categorias]');
			if(filtro1.length>0){
				filtro1[0].setVisible(true);
			}
		}
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			if(this.getForm()){
				Ext.getCmp('endereco_bairro_nome_os').setValue(record.get('bairro_nome'));
				Ext.getCmp('endereco_bairro_os').setValue(record.get('bairro_id_inicial'));
				if (record.get('loteamento') != '') {
					Ext.getCmp('endereco_loteamento_os').setValue(record.get('loteamento'));
				}
				if (record.get('ponto_referencia') != '') {
					Ext.getCmp('endereco_referencia_os').setValue(record.get('ponto_referencia'));
				}
				Ext.getCmp('endereco_num_os').focus(false, 100);
			}
			else{
				Ext.getCmp('endereco_bairro_filter_os').setValue(record.get('bairro_nome'));
			}
		}
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('cidade_os');
		}
		else{
			var comboCidade = Ext.getCmp('cidade_filter_os');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			Ext.getCmp('endereco_os').store.proxy.extraParams.localidade_id = combo.getValue();
			me.bairro = true;
			if(Ext.getCmp('endereco_os').isDisabled()){
				Ext.getCmp('endereco_os').setDisabled(false);
			}
			return true;
		}
		else{
			if(Ext.getCmp('endereco_filter_os').store){
				Ext.getCmp('endereco_filter_os').store.proxy.extraParams.localidade_id = combo.getValue();
				if(Ext.getCmp('endereco_filter_os').isDisabled()){
					Ext.getCmp('endereco_filter_os').setDisabled(false);
				}				
			}
			me.bairro = true;
			return true;
		}

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_os');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_os');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	Departamentos: function(){
		var me = this;
		me.getDesktopWindow('List-Produtos_Categorias', 'Produtos_Categorias', 'produtos_categorias.List', function(controller){
		});
	},

	Categorias: function(){
		var me = this;
		me.getDesktopWindow('List-Os_Categorias', 'Os_Categorias', 'os_categorias.List', function(controller){
		});
	},
	
	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_os');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	gerarPdf: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;

		if (grid.selModel.hasSelection()) {

			var items = grid.getSelectionModel().selected.items;
			var ids = '';
			for(var i in items){
				if(i>0){
					ids += ',';
				}

				ids += items[i].get('id');
			}

			window.open('server/modulos/os/imprime_os.php?ids='+ids);
		}
		else{
			info(this.avisoText, this.impressaoErroGrid, 'warning');
			return true;
		}
		
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddOsWin', 'Os', 'os.Edit', function(dados){
			me.getAddWin().setTitle('Alteração da OS: '+record.get('id2'));
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/os/list.php', function(dados){
				me.getForm().el.mask('Aguarde...');
				window.controlS = me;
				Ext.getCmp('departamento_os').setValue(parseInt(dados.departamento));
				Ext.getCmp('categoria_os').setValue(parseInt(dados.categoria));
				
				var a = setInterval(function(){
					if(dados.cidade != null && dados.cidade != ""){
						if(me.cidade==true){
							Ext.getCmp('cidade_os').setValue(dados.cidade);
							me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
							me.cidade = null;
						}
					} else {
							me.cidade = null;
					}

					if(dados.endereco != null && dados.endereco != ""){
						if(me.endereco==true){
							Ext.getCmp('endereco_os').setValue(parseInt(dados.endereco));
							me.endereco = null;
						}
					}else{me.endereco = null;}
					if(me.cidade == null && me.endereco == null){
						if(me.getForm()){
							me.getForm().el.unmask();
						}

						clearInterval(a);
						me.cidade = false;
						me.endereco = false;
					}
				}, 300);
			});

			Ext.getCmp('action_os').setValue('EDITAR');
			Ext.getCmp('status_os').setVisible(true);
			Ext.getCmp('usuario_responsavel_os').setVisible(true);
			Ext.getCmp('cadastrado_por_os').setVisible(true);
			Ext.getCmp('alterado_por_os').setVisible(true);

			var data = me.application.dados[me.tabela.toLowerCase()];
			Ext.each(data, function(j){
				if(j.acao=='os_logs'){
					Ext.getCmp('GridOSLogs_Sistema').setDisabled(false);
					Ext.getCmp('GridOSLogs_Sistema').store.proxy.extraParams.tabela_cod = record.get('id');
					Ext.getCmp('GridOSLogs_Sistema').store.proxy.extraParams.modulo = 'os';
				}

				if(j.acao=='os_respostas'){
					Ext.getCmp('GridOs_Respostas').setDisabled(false);
					Ext.getCmp('GridOs_Respostas').store.proxy.extraParams.os = record.get('id');
				}
			});
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'os', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	reabre: function(grid, record, button) {
		var me = this;
		me.acoesAjax(grid, 'os', {
			action: 'REABRIR',
			id: record.get('id')
		}, button, false);
	},

	cancelarOS: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('Cancelamento-Os', 'Os', 'os.Cancelamento', function(controller){
				me.getAddWin().setTitle('Cancelamento da OS: '+record.get('id2'));
				Ext.getCmp('id_os').setValue(record.get('id'));
			});
		}
		else{
			info(me.titleErro, me.erroCancelarOs);
			return true;
		}
	},

	respostas: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('List-Os_Respostas', 'Os_Respostas', 'os_respostas.List', function(controller){
				Ext.getCmp('List-Os_Respostas').setTitle('Respostas da OS: '+record.get('id2'));
				controller.os = record.get('id');
				controller.getList().store.proxy.extraParams.os = record.get('id');
				controller.getList().store.load();
			});
		}
		else{
			info(me.titleErro, me.respostaOsErroGrid);
			return true;
		}
	},

	finalizarOS: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('Finalizacao-Os', 'Os', 'os.Finalizacao', function(controller){
				me.getAddWin().setTitle('Finalização da OS: '+record.get('id2'));
				Ext.getCmp('id_os').setValue(record.get('id'));
			});
		}
		else{
			info(me.titleErro, me.erroFinalizarOs);
			return true;
		}
	},

	reabrirOS: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;

		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Reabrir a OS: '+record.get('id2')+'?', function(btn){
				if(btn=='yes'){
					me.reabre(grid, record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	btedit: function(button) {
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;

		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();

			var status = record.get('status');
			if(this.existPermissao('os', 'editar')==true  && (status!='CANCELADO' && status!='FINALIZADO')){
				this.edit(grid, record);
			}
			else{
				this.edit(grid, record, button);
				Ext.getCmp('button_salvar_os').setVisible(false);
			}
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;

		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.del(grid, record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	add: function(button, callback) {
		var me = this;
		me.cidade = false;
		grid = button.up('gridpanel');
		me.gridPanel = grid;

		if(grid.xtype=='gridclientesoslist'){
			var clientes = this.application.getController('Clientes');
			clientes.setDataClienteOs(clientes.getList().getSelectionModel().getLastSelected());
		}
		else{
			me.getDesktopWindow('Os', 'Os', 'os.Edit', function(){
				var empresa = me.getCurrentEmpresa();
				Ext.getCmp('estado_os').setValue(empresa.estado);
				Ext.getCmp('empresa_os').setValue(empresa.cod_empresa);
				Ext.getCmp('departamento_os').setValue(1);
				Ext.getCmp('categoria_os').setValue(7);
				var a = setInterval(function(){
					if(empresa.cidade != null){
						if(me.cidade==true){
							var record = Ext.getCmp('cidade_os').store.findRecord('descricao', empresa.cidade);
							if(record){
								Ext.getCmp('cidade_os').setValue(record.get('id'));
							}
							me.cidade = null;
						}
					}else{me.cidade = null;}
					if(me.cidade == null){
						clearInterval(a);
					}
				}, 1000);

				setTimeout(function(){
					clearInterval(a);
					if(typeof callback == 'function'){
						callback();
					}
				}, 7000);
			});			
		}
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.gridPanel, me.getForm(), me.getAddWin(), button, function(dados){

			if(Ext.getCmp('action_os').getValue()=='INSERIR'){
				Ext.getCmp('action_os').setValue('EDITAR');

				Ext.getCmp('GridOSLogs_Sistema').setDisabled(false);
				Ext.getCmp('GridOs_Respostas').setDisabled(false);
				//Ext.getCmp('GridOSLogs_Sistema').store.load();
			}
		}, false);
	},

	RegistroSelecionado: function(selModel, record){
		var me = this;
		var status = record.get('status');
		me.gridPanel = selModel.view.up('gridpanel');

		if(this.existPermissao('os', 'editar')==true  && (status!='CANCELADO' && status!='FINALIZADO')){
			me.gridPanel.down('button[action=editar]').setDisabled(false);
		}
		else{
			if (userData.perfil_id == 1) {
				me.gridPanel.down('button[action=editar]').setDisabled(false);
			}
			else {
				me.gridPanel.down('button[action=editar]').setDisabled(true);
			}
		}

		if(this.existPermissao('os', 'deletar')==true && (status!='CANCELADO' && status!='FINALIZADO')){
			me.gridPanel.down('button[action=deletar]').setDisabled(false);
		}
		else{
			me.gridPanel.down('button[action=deletar]').setDisabled(true);
		}

		if(this.existPermissao('os', 'os_respostas')==true){
			me.gridPanel.down('button[action=os_respostas]').setDisabled(false);
		}

		if(this.existPermissao('os', 'finalizar')==true && (status!='CANCELADO' && status!='FINALIZADO')){
			me.gridPanel.down('button[action=finalizar]').setDisabled(false);
		}
		else{
			me.gridPanel.down('button[action=finalizar]').setDisabled(true);
		}

		if(this.existPermissao('os', 'reabrir')==true && (status=='CANCELADO' || status=='FINALIZADO')){
			me.gridPanel.down('button[action=reabrir]').setDisabled(false);
		}
		else{
			me.gridPanel.down('button[action=reabrir]').setDisabled(true);
		}

		if(me.gridPanel.xtype=='gridclientesoslist'){
			var total_selecionado = Ext.getCmp('GridClientesOs').getSelectionModel().getCount();
		}
		else{
			var total_selecionado = Ext.getCmp('GridOs').getSelectionModel().getCount();
		}

		if (total_selecionado > 1){
			me.gridPanel.down('button[action=editar]').setDisabled(true);
			me.gridPanel.down('button[action=deletar]').setDisabled(true);
			me.gridPanel.down('button[action=os_respostas]').setDisabled(true);
			me.gridPanel.down('button[action=finalizar]').setDisabled(true);
			me.gridPanel.down('button[action=reabrir]').setDisabled(true);
		}
	},

	desabilitaBotoes: function(selModel, record){
		var me = this;
		var status = record.get('status');
		me.gridPanel = selModel.view.up('gridpanel');
		me.gridPanel.down('button[action=editar]').setDisabled(true);
		me.gridPanel.down('button[action=deletar]').setDisabled(true);
		me.gridPanel.down('button[action=os_respostas]').setDisabled(true);
		me.gridPanel.down('button[action=finalizar]').setDisabled(true);
		me.gridPanel.down('button[action=reabrir]').setDisabled(true);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.os.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();

		var me = this;
		var empresa = me.getCurrentEmpresa();
		Ext.getCmp('estado_filter_os').setValue(empresa.estado);
		var a = setInterval(function(){
			if(empresa.cidade != null && empresa.cidade != ""){
				if(me.cidade==true){
					var record = Ext.getCmp('cidade_filter_os').store.findRecord('descricao', empresa.cidade);
					if(record){
						Ext.getCmp('cidade_filter_os').setValue(record.get('id'));
					}
					me.cidade = null;
				}
			} 
			else {
				me.cidade = null;
			}
			if(me.cidade == null){
				clearInterval(a);
			}
		}, 1000);
	},

	btStoreLoadFielter2: function(){
		this.getList().store.proxy.extraParams.usuario_responsavel = Ext.getCmp('filtra_responsavel').getValue();
		this.getList().store.load({
			start: 0,
			limit: 100
		});
	}
});