/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.OrcamentosForro', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'OrcamentosForro',
	started: false,
	
	dependeciesControllers: [
		//'Itens_OrcamentoForro'
		'Orcamentos_Observacoes'
	],

	refs: [
		{
			ref: 'gridAmbientes',
			selector: 'addprodutos_orcamentos_forrowin gridambientes_orcamentos_forrolist'
		},
		{
			ref: 'gridProdutos',
			selector: 'addorcamentos_forrowin gridprodutos_orcamentos_forrolist'
		},
		{
			ref: 'formProdutos',
			selector: 'addprodutos_orcamentos_forrowin form'
		},
		{
			ref: 'addProdutosWin',
			selector: 'addprodutos_orcamentos_forrowin'
		},
		{
			ref: 'formAmbientes',
			selector: 'addambientes_orcamentos_forrowin form'
		},
		{
			ref: 'addAmbientesWin',
			selector: 'addambientes_orcamentos_forrowin'
		},
		{
			ref: 'form',
			selector: 'addorcamentos_forrowin form'
		},
		{
			ref: 'addWin',
			selector: 'addorcamentos_forrowin'
		}
	],

	models: [
		'ModelCombo',
		'ModelComboLocal',
		'ModelAmbientes_Orcamentos_Forro',
		'ModelProdutos_Orcamentos_Forro'
	],
	
	stores: [
		'StoreComboClientes',
		'StoreComboOrcamentista',
		'StoreComboProdutos',
		'StoreComboProdutosForro',
		'StoreComboProdutosServicos',
		'StoreComboProdutosAcessoriosForro',
		'StoreAmbientes_Orcamentos_Forro',
		'StoreProdutos_Orcamentos_Forro',
		'StoreComboAmbienteOrcamentoForroL',
		'StoreComboTipoAcessorioAmbienteOrcamentoForro',
		'StoreComboProdutosCores',
		'StoreAmbientesNomes',
		'StoreComboOrcamentosVendas'
	],	
	
	views: [
		'orcamentos_forro.GridAmbientes',
		'orcamentos_forro.GridProdutos',
		'orcamentos_forro.EditAmbientes',
		'orcamentos_forro.EditProdutos',
		'orcamentos_forro.EditAcessorios',
		'orcamentos_forro.EditGeral',
		'orcamentos_forro.Edit'
	],

	produto_forro: false,
	produto_cor: false,
	noDeleteLoad: true,
	areaTotalAmbiente: null,
	somaTotalAmbiente: 0,
	id_categoria_precos: 1,
	tipo_produto: 'P',
	record_produto: null,
	delete_orcamento: false,

	init: function(application) {
		this.control({
			////////////////////--PRODUTOS--////////////////////////
			'addorcamentos_forrowin gridprodutos_orcamentos_forrolist button[action=adicionar]':{
				click: this.addProduto
			},
			'addorcamentos_forrowin gridprodutos_orcamentos_forrolist button[action=add_acessorio]':{
				click: this.addAcessorio
			},
			'addorcamentos_forrowin gridprodutos_orcamentos_forrolist button[action=add_geral]':{
				click: this.addGeral
			},
			'addorcamentos_forrowin gridprodutos_orcamentos_forrolist button[action=editar]':{
				click: this.editProduto
			},
			'addorcamentos_forrowin gridprodutos_orcamentos_forrolist button[action=deletar]':{
				click: this.deleteProduto
			},
			'addorcamentos_forrowin gridprodutos_orcamentos_forrolist button[action=copiar_item_orcamento]':{
				click: this.btCopiaItemOrcamento
			},
			'addprodutos_orcamentos_forrowin form fieldcontainer button[action=add_ambientes]': {
				click: this.setStepAmbientes
			},
			'addprodutos_orcamentos_forrowin form fieldcontainer combobox[id=id_produto_produtos_orcamentos_forro]': {
				change: this.changeCores
			},
			'addprodutos_orcamentos_forrowin form fieldcontainer combobox[id=sn_l_ambientes_orcamentos_forro]': {
				change: this.changeL
			},
			'addprodutos_orcamentos_forrowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addprodutos_orcamentos_forrowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addprodutos_orcamentos_forrowin button[action=salvar]': {
				click: this.updateProduto
			},

/*			'addprodutos_orcamentos_forrowin': {
				beforeclose: this.beforeCloseProduto
			},
*/
			////////////////////--PRODUTOS--////////////////////////

			////////////////////--PRODUTOS - GERAL--////////////////////////
			'addageral_orcamentos_forrowin combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addageral_orcamentos_forrowin combobox[id=id_produto_geral_orcamentos_forro]': {
				change: this.setValorUnitarioGeral
			},
			'addageral_orcamentos_forrowin button[action=salvar]': {
				click: this.updateProdutoGeral
			},
			////////////////////--PRODUTOS - GERAL--////////////////////////


			///////////////////-- PRODUTOS - ACESSORIOS --////////////////////////
			'addacessorios_orcamentos_forrowin button[action=salvar]': {
				click: this.updateAcessorio
			},
			'addacessorios_orcamentos_forrowin form combobox[id=id_produto_acessorios_orcamentos_forro]': {
				change: this.changeValorUnitario
			},

			////////////////////--AMBIENTES--////////////////////////
			'addprodutos_orcamentos_forrowin gridambientes_orcamentos_forrolist button[action=adicionar]':{
				click: this.addAmbiente
			},
			'addprodutos_orcamentos_forrowin gridambientes_orcamentos_forrolist button[action=editar]':{
				click: this.editAmbiente
			},
			'addprodutos_orcamentos_forrowin gridambientes_orcamentos_forrolist button[action=deletar]':{
				click: this.deleteAmbiente
			},
			'addambientes_orcamentos_forrowin button[action=salvar]': {
				click: this.updateAmbiente
			},
			'addambientes_orcamentos_forrowin form combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addambientes_orcamentos_forrowin form combobox[id=ambiente_ambientes_orcamentos_forro]': {
				change: this.changeComboL
			},
			'addambientes_orcamentos_forrowin form button[action=reset_combo]': {
				click: this.resetCombo
			},
			////////////////////--AMBIENTES--////////////////////////

			'addorcamentos_forrowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addorcamentos_forrowin form textfield[id=valor_total_orcamentos_forro]': {
				blur: this.changeTotal
			},
			'addorcamentos_forrowin form textfield[id=acrescimo_porcento_orcamentos_forro], addorcamentos_forrowin form textfield[id=acrescimo_real_orcamentos_forro]': {
				keyup: this.changeAcrescimo
			},
			'addorcamentos_forrowin form numberfield[id=desconto_porcento_orcamentos_forro], addorcamentos_forrowin form textfield[id=desconto_real_orcamentos_forro]': {
				keyup: this.changeDesconto
			},
			'addorcamentos_forrowin form combobox[id=id_cliente_orcamentos_forro]': {
				change: this.changeGridValores
			},
			'addorcamentos_forrowin checkbox[id=cliente_cadastrado_orcamentos_forro]': {
				change: this.enableCliente
			},
			'addorcamentos_forrowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addorcamentos_forrowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addorcamentos_forrowin button[action=salvar]': {
				click: this.update
			},
			'addorcamentos_forrowin form fieldcontainer combobox[id=estado_orcamentos_forro]': {
				change: this.loadCidade
			},
			'addorcamentos_forrowin form fieldcontainer combobox[id=cidade_orcamentos_forro]': {
				change: this.loadBairro
			},
			'addorcamentos_forrowin form fieldcontainer combobox[id=endereco_orcamentos_forro]': {
				change: this.setEndereco
			}
		});
	},

	cidade: false,
	endereco: false,

	loadDadosOS: function(field){
		var me = this;
		var v = field.getValue();
		Ext.Ajax.request({
			url : 'server/modulos/os/list.php',
			params : {
				action: 'GET_VALUES',
				id : v
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'O Número de OS digitado não foi Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);

					if (resposta.dados.cliente==null){
						Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue('');
						Ext.getCmp('cliente_descricao_orcamentos_forro').setVisible(true);
						Ext.getCmp('cliente_descricao_orcamentos_forro').setDisabled(false);
						Ext.getCmp('fieldcontainer_id_cliente_orcamentos_forro').setVisible(false);
						Ext.getCmp('cliente_cadastrado_orcamentos_forro').setValue(false);
						Ext.getCmp('id_cliente_orcamentos_forro').setDisabled(true);
						Ext.getCmp('cliente_descricao_orcamentos_forro').setValue(resposta.dados.titulo);
					}
					else{
						Ext.getCmp('cliente_descricao_orcamentos_forro').setVisible(false);
						Ext.getCmp('cliente_descricao_orcamentos_forro').setDisabled(true);
						Ext.getCmp('fieldcontainer_id_cliente_orcamentos_forro').setVisible(true);
						Ext.getCmp('cliente_cadastrado_orcamentos_forro').setValue(true);
						Ext.getCmp('id_cliente_orcamentos_forro').setDisabled(false);
						Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue('');

						if (resposta.dados.titulo != resposta.dados.nome_cliente){
							Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue(resposta.dados.titulo);
						}

						Ext.getCmp('id_cliente_orcamentos_forro').store.load({
							params: {
								id_cliente: resposta.dados.cliente.toString()
							}
						});

						var c = setInterval(function(){
							me.verificaAnotacoesCliente(resposta.dados.cliente);
							Ext.getCmp('id_cliente_orcamentos_forro').setValue(resposta.dados.cliente.toString());
							clearInterval(c);
						}, 400);
					}

					var texto = "Deseja ATUALIZAR os dados de Endereco deste orçamento com os mesmos dados da OS?";
					Ext.Msg.confirm('Confirmar', texto, function(btn){
						if(btn=='yes'){
							me.loadEnderecoOS(v);
						}
					});
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadEnderecoOS: function(os_nro){
		var me = this;
		Ext.Ajax.request({
			url : 'server/modulos/os/list.php',
			params : {
				action: 'GET_VALUES',
				id : os_nro
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'O Número de OS digitado não foi Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('estado_orcamentos_forro').setValue(resposta.dados.estado);
					Ext.getCmp('ponto_ref_orcamentos_forro').setValue(resposta.dados.endereco_referencia);
					Ext.getCmp('complemento_orcamentos_forro').setValue(resposta.dados.endereco_complemento);
					Ext.getCmp('nro_end_orcamentos_forro').setValue(resposta.dados.endereco_num);
					Ext.getCmp('loteamento_orcamentos_forro').setValue(resposta.dados.loteamento);
					Ext.getCmp('fone1_orcamentos_forro').setValue(resposta.dados.fone1);
					Ext.getCmp('fone2_orcamentos_forro').setValue(resposta.dados.fone2);
					Ext.getCmp('fone3_orcamentos_forro').setValue(resposta.dados.fone3);
					Ext.getCmp('fone4_orcamentos_forro').setValue(resposta.dados.fone4);
					Ext.getCmp('fone5_orcamentos_forro').setValue(resposta.dados.fone5);
					Ext.getCmp('fone1_obs_orcamentos_forro').setValue(resposta.dados.fone1_obs);
					Ext.getCmp('fone2_obs_orcamentos_forro').setValue(resposta.dados.fone2_obs);
					Ext.getCmp('fone3_obs_orcamentos_forro').setValue(resposta.dados.fone3_obs);
					Ext.getCmp('fone4_obs_orcamentos_forro').setValue(resposta.dados.fone4_obs);
					Ext.getCmp('fone5_obs_orcamentos_forro').setValue(resposta.dados.fone5_obs);

					var a = setInterval(function(){
						Ext.getCmp('cidade_orcamentos_forro').setValue(resposta.dados.cidade);
						me.comboEndereco(resposta.dados.cidade, resposta.dados.endereco_bairro, resposta.dados.endereco);
						clearInterval(a);
					}, 300);

					var b = setInterval(function(){
						Ext.getCmp('endereco_orcamentos_forro').setValue(parseInt(resposta.dados.endereco));
						me.cidade = null;
						clearInterval(b);
					}, 400);

					if (resposta.dados.cliente!=null){
						Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue('');

						if (resposta.dados.titulo != resposta.dados.nome_cliente){
							Ext.getCmp('aos_cuidados_de_orcamentos_forro').setValue(resposta.dados.titulo);
						}
					}
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadEnderecoCliente: function(id_cliente){
		var me = this;
		Ext.Ajax.request({
			url : 'server/modulos/clientes/list.php',
			params : {
				action: 'GET_VALUES_ORCAMENTOS',
				id : id_cliente
			},
			success: function(o){
				var resposta = Ext.decode(o.responseText, true);
				if(resposta.dados==false){
					info(me.titleErro, 'Cliente não Localizado', 'information');
				}
				else if(resposta.success==true){
					Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('estado_orcamentos_forro').setValue(resposta.dados[0].estado);
					Ext.getCmp('ponto_ref_orcamentos_forro').setValue(resposta.dados[0].ponto_ref);
					Ext.getCmp('complemento_orcamentos_forro').setValue(resposta.dados[0].complemento);
					Ext.getCmp('nro_end_orcamentos_forro').setValue(resposta.dados[0].nro_end);
					Ext.getCmp('loteamento_orcamentos_forro').setValue(resposta.dados[0].loteamento);
					Ext.getCmp('fone1_orcamentos_forro').setValue(resposta.dados[0].fone1);
					Ext.getCmp('fone2_orcamentos_forro').setValue(resposta.dados[0].fone2);
					Ext.getCmp('fone3_orcamentos_forro').setValue(resposta.dados[0].fone3);
					Ext.getCmp('fone4_orcamentos_forro').setValue(resposta.dados[0].fone4);
					Ext.getCmp('fone5_orcamentos_forro').setValue(resposta.dados[0].fone5);
					Ext.getCmp('fone1_obs_orcamentos_forro').setValue(resposta.dados[0].fone1_obs);
					Ext.getCmp('fone2_obs_orcamentos_forro').setValue(resposta.dados[0].fone2_obs);
					Ext.getCmp('fone3_obs_orcamentos_forro').setValue(resposta.dados[0].fone3_obs);
					Ext.getCmp('fone4_obs_orcamentos_forro').setValue(resposta.dados[0].fone4_obs);
					Ext.getCmp('fone5_obs_orcamentos_forro').setValue(resposta.dados[0].fone5_obs);

					var a = setInterval(function(){
						Ext.getCmp('cidade_orcamentos_forro').setValue(resposta.dados[0].cidade);
						me.comboEndereco(resposta.dados[0].cidade, resposta.dados[0].bairro, resposta.dados[0].endereco);
						clearInterval(a);
					}, 300);

					var b = setInterval(function(){
						me.verificaAnotacoesCliente(id_cliente);
						Ext.getCmp('endereco_orcamentos_forro').setValue(parseInt(resposta.dados[0].endereco));
						me.cidade = null;
						clearInterval(b);
					}, 400);
				}
				else{
					info(me.avisoText, resposta.msg, 'error');
					if(resposta.logout){
						window.location = 'login.php';
					}
				}
			},
			failure: function(o){
				var dados = Ext.decode(o.responseText, true);
				if(dados==null){
					info(me.titleErro, response.responseText, 'error');
				}
				else if(dados.logout){
					window.location = 'login.php';
				}
			}
		});
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}

		var comboCidade = Ext.getCmp('cidade_orcamentos_forro');

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}

		Ext.getCmp('endereco_orcamentos_forro').store.proxy.extraParams.localidade_id = combo.getValue();
		me.bairro = true;
		if(Ext.getCmp('endereco_orcamentos_forro').isDisabled()){
			Ext.getCmp('endereco_orcamentos_forro').setDisabled(false);
		}
		return true;

		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_orcamentos_forro');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_orcamentos_forro');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_orcamentos_forro');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			Ext.getCmp('bairro_nome_orcamentos_forro').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_orcamentos_forro').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_orcamentos_forro').setValue(record.get('loteamento'));
			}
			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_orcamentos_forro').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_orcamentos_forro').setValue(record.get('cep'));
			Ext.getCmp('nro_end_orcamentos_forro').focus(false, 100);
		}
	},

	beforeCloseProduto: function(win){
		var me = this;
		if(win.closeMe) {
			win.closeMe = false;
			return true;
		}
		Ext.Msg.show({
			title:'Deseja fechar o formulário?',
			msg:'Se foi feito alguma alteração nos AMBIENTES clique em "SIM" para salvar agora',
			buttons:Ext.Msg.YESNO,
			callback:function(btn) {
				if('yes' === btn) {
					me.updateProduto(Ext.getCmp('button_salvar_produtos_orcamentos_forro'));
				}
				else{
					win.closeMe = false;
					win.close();
				}
			}
		})

		return false;
	},

	updateProdutoGeral: function(button){
		console.log('updateProdutoGeral');
		var me         = this;
		var grid       = Ext.getCmp('GridOrcamentos');
		var win        = Ext.getCmp('AddGeral_Orcamentos_ForroWin');
		var form       = Ext.getCmp('FormGeral_Orcamentos_Forro');

		if(!form.getForm().isValid()){
			return true;
		}

		var valor_total = parseFloat(Ext.getCmp('valor_unitario_geral_orcamentos_forro').getValue()) * Ext.getCmp('quantidade_geral_orcamentos_forro').getValue();
		var id_orcamento = Ext.getCmp('id_orcamento_orcamentos_forro').getValue();
		var valor_bruto = valor_total;

		me.formParams = {
			tipo_produto: 'P',
			id_orcamento: id_orcamento,
			valor_bruto: valor_total.toFixed(2),
			valor_total: valor_total.toFixed(2)
		};

		me.saveForm(grid, form, win, button, function(){
			me.getGridProdutos().store.proxy.extraParams.id_orcamento = id_orcamento;
			me.getGridProdutos().store.load({
				callback: function(){
					me.setValorTotal();

					Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
					Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();
					
					var count = Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_forro').setValue(count);
				}
			});
		}, false);
	},

	setValorUnitarioGeral: function(combo, value){
		console.log('setValorUnitarioGeral');
		var me = this;
		var record = combo.store.getById(value);
		//var record = combo.store.getById(combo.getValue());
		//var value = record.get('id');

		var comboCorGeral = Ext.getCmp('cor_geral_orcamentos_forro');

		if (record!=null &&  value!=null && !isNaN(parseInt(value)))	{
			var qtde_cores = record.raw.produto_cores.length;

			if (qtde_cores > 0) {
				comboCorGeral.setDisabled(false);
				comboCorGeral.setVisible(true);

				comboCorGeral.store.proxy.extraParams.id_produto = combo.getValue();
				comboCorGeral.store.load({
					callback: function(r, options, success){
						comboCorGeral.setValue(r[0].data.id);
						if (Ext.getCmp('quantidade_geral_orcamentos_forro').getValue() == null) {
							Ext.getCmp('quantidade_geral_orcamentos_forro').setValue(1);
						}

						if (qtde_cores == 1) {
							Ext.getCmp('quantidade_geral_orcamentos_forro').focus(false, 10);
						}
						else {
							Ext.getCmp('cor_geral_orcamentos_forro').focus(false, 10);
						}
					}
				});
			}
			else {
				Ext.getCmp('cor_geral_orcamentos_forro').reset();
				comboCorGeral.store.removeAll();
	 			comboCorGeral.setDisabled(true);
				comboCorGeral.setVisible(false);
			}

			if(record!=null){
				var valor_unitario  = me.getValorUnitario(combo, value);
				Ext.getCmp('valor_unitario_geral_orcamentos_forro').setValue(valor_unitario);
			}
		}
		else {
			Ext.getCmp('cor_geral_orcamentos_forro').reset();
			comboCorGeral.store.removeAll();
 			comboCorGeral.setDisabled(true);
			comboCorGeral.setVisible(false);
		}
	},

	changeValorUnitario: function(combo, value){
		console.log('setValorUnitarioGeral');
		var me = this;
		var record = combo.store.getById(value);
		var comboCorAcessorio = Ext.getCmp('cor_acessorios_orcamentos_forro');

		if (record!=null && value!=null && !isNaN(parseInt(value)))	{
			var qtde_cores = record.raw.produto_cores.length;

			if (qtde_cores > 0) {
				comboCorAcessorio.setDisabled(false);
				comboCorAcessorio.setVisible(true);

				comboCorAcessorio.store.proxy.extraParams.id_produto = combo.getValue();
				comboCorAcessorio.store.load({
					callback: function(r, options, success){
						comboCorAcessorio.setValue(r[0].data.id);
						if (Ext.getCmp('quantidade_acessorios_orcamentos_forro').getValue() == null || Ext.getCmp('quantidade_acessorios_orcamentos_forro').getValue() == 0) {
							Ext.getCmp('quantidade_acessorios_orcamentos_forro').setValue(1);
						}

						if (qtde_cores == 1) {
							Ext.getCmp('quantidade_acessorios_orcamentos_forro').focus(false, 10);
						}
						else {
							Ext.getCmp('cor_acessorios_orcamentos_forro').focus(false, 10);
						}
					}
				});
			}
			else {
				Ext.getCmp('cor_acessorios_orcamentos_forro').reset();
				comboCorAcessorio.store.removeAll();
				comboCorAcessorio.setDisabled(true);
				comboCorAcessorio.setVisible(false);
			}

			if(record!=null){
				var valor_unitario  = me.getValorUnitario(combo, value);
				//if(Ext.getCmp('valor_unitario_acessorios_orcamentos_piso').getValue()==""){
					Ext.getCmp('valor_unitario_acessorios_orcamentos_forro').setValue(valor_unitario);
				//}
			}
		}
		else {
			Ext.getCmp('cor_acessorios_orcamentos_forro').reset();
			comboCorAcessorio.store.removeAll();
			comboCorAcessorio.setDisabled(true);
			comboCorAcessorio.setVisible(false);
		}
	},

	changeComboL: function(combo, value){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(record!=null && record.get('descricao')=='L'){
			Ext.getCmp('sn_l_ambientes_orcamentos_forro').setValue('S');				
		}
		else {
			Ext.getCmp('sn_l_ambientes_orcamentos_forro').setValue('N');				
		}
	},

	changeL: function(combo, value){
		var me = this;
		var id_ambiente_pai = 0;
		var total = Ext.getCmp('GridAmbientes_Orcamentos_Forro').store.getCount();
		if(total>0 && combo.getValue()=='S'){
			var rec = me.getAmbientePai(total);
			if(rec){
				id_ambiente_pai = rec.get('id_ambientes_orcamentos_forro');
				Ext.getCmp('id_ambiente_pai_ambientes_orcamentos_forro').setValue(id_ambiente_pai);
				return true;
			}
		}

		Ext.getCmp('id_ambiente_pai_ambientes_orcamentos_forro').reset();
		Ext.getCmp('sn_l_ambientes_orcamentos_forro').reset();
	},

	getAmbientePai: function(total){
		var me = this;
		var record = false;
		var records = Ext.getCmp('GridAmbientes_Orcamentos_Forro').store.data.items.reverse();
		for(var i=0;i<total;i++){
			var rec = records[i];
			if(rec.get('sn_l')=='N'){
				record = rec;
			}
		}

		return record;
	},

	changeTotal: function(field, value){
		console.log('changeTotal');
		var me = this;
		var valor_bruto = Ext.getCmp('valor_bruto_orcamentos_forro').getValue();
		var desconto = 0;
		var acrescimo = 0;

		if(valor_bruto!=undefined && valor_bruto!=null && valor_bruto!=""){
			if(desconto==""){desconto = 0;}
			if(acrescimo==""){acrescimo = 0;}

			valor_bruto = parseFloat(valor_bruto);
			desconto 	= parseFloat(desconto);
			acrescimo	= parseFloat(acrescimo);
			
			if(field.getValue()==""){
				var valor_total = 0;
			}

			var valor_total = parseFloat(field.getValue());
			var subtotal = valor_bruto + acrescimo;
			subtotal 	-= desconto;

			if(subtotal>valor_total){
				/*var desconto2 = subtotal - valor_total;
				desconto += desconto2;
				*/
				desconto = parseFloat(subtotal - valor_total);
				desconto = Math.round((desconto).toFixed(10) * 100)/100;
				Ext.getCmp('acrescimo_real_orcamentos_forro').setValue(0);
				Ext.getCmp('acrescimo_porcento_orcamentos_forro').setValue(0);
				Ext.getCmp('desconto_real_orcamentos_forro').setValue(desconto);
				me.changeDesconto(Ext.getCmp('desconto_real_orcamentos_forro'), desconto);
			}
			else{
				/*var acrescimo2 = valor_total - subtotal;
				acrescimo += acrescimo2;
				*/
				acrescimo = parseFloat(valor_total - subtotal);
				acrescimo = Math.round((acrescimo).toFixed(10) * 100)/100;
				Ext.getCmp('desconto_real_orcamentos_forro').setValue(0);
				Ext.getCmp('desconto_porcento_orcamentos_forro').setValue(0);
				Ext.getCmp('acrescimo_real_orcamentos_forro').setValue(acrescimo);
				me.changeAcrescimo(Ext.getCmp('acrescimo_real_orcamentos_forro'), acrescimo);
			}
		}
	},

	changeAcrescimo: function(field, value){
		var me = this;
		var total = Ext.getCmp('valor_bruto_orcamentos_forro').getValue();
		if(total!=undefined && total!=null && total!=""){
			total = parseFloat(total);
			if(total>0){
				
				if(field.name=='acrescimo_real'){
					var porcento = ((parseFloat(field.getValue()) * 100) / total);
					Ext.getCmp('acrescimo_porcento_orcamentos_forro').setValue(porcento);
				}
				else{
					var real = parseFloat( (total * field.getValue())/100 );
					Ext.getCmp('acrescimo_real_orcamentos_forro').setValue(real);
				} 

				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);               
			}
		}
	},

	changeDesconto: function(field, value){
		var me = this;
		var total = Ext.getCmp('valor_bruto_orcamentos_forro').getValue();
		if(total!=undefined && total!=null && total!=""){
			total = parseFloat(total);
			if(total>0){
				if(field.name=='desconto_real'){
					var porcento = ((parseFloat(field.getValue()) * 100) / total);
					Ext.getCmp('desconto_porcento_orcamentos_forro').setValue(porcento);
				}
				else{
					var real = parseFloat( (total * field.getValue())/100 );
					Ext.getCmp('desconto_real_orcamentos_forro').setValue(real);
				} 

				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
			}
		}
	},

	resetParams: function(){
		this.produto_forro = false;
		this.produto_cor = false;
		this.noDeleteLoad = true;
		this.areaTotalAmbiente = null;
		this.somaTotalAmbiente = 0;

		var v = Ext.getCmp('id_cliente_orcamentos_forro').store.getById(Ext.getCmp('id_cliente_orcamentos_forro').getValue());
		
		if(v){
			if(v && v.raw.id_categoria_precos!=null){
				this.id_categoria_precos = v.raw.id_categoria_precos;
			}
		}
		else{
			this.id_categoria_precos = 1;
		}
		
		this.tipo_produto = 'P';
		this.record_produto = null;
	},

	////////////////////--PRODUTOS--////////////////////////
	changeCores: function(combo, value){
		console.log('changeCores');
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}

		if(!window.StoreComboProdutosOrcamentosForro){
			window.StoreComboProdutosOrcamentosForro = combo.store;
		}

		me.tipo_produto = record.raw.tipo_produto;
		me.record_produto = record;
		var comboCor = Ext.getCmp('id_cor_produtos_orcamentos_forro');
		
		var valor = me.getValorUnitario(Ext.getCmp('id_produto_produtos_orcamentos_forro'), Ext.getCmp('id_produto_produtos_orcamentos_forro').getValue());

		if(Ext.getCmp('action_produtos_orcamentos_forro').getValue()=="INSERIR"){
			Ext.getCmp('valor_unitario_produtos_orcamentos_forro').setValue(valor);
		}
		else{
			var gridProdutos = me.getGridProdutos();
			if (gridProdutos.selModel.hasSelection()) {
				var record = gridProdutos.getSelectionModel().getLastSelected();
				console.log(record);
				if(record.get('id_produto')!=Ext.getCmp('id_produto_produtos_orcamentos_forro').getValue()){
					Ext.getCmp('valor_unitario_produtos_orcamentos_forro').setValue(valor);		
				}
			}
		}

		Ext.getCmp('step_2_produtos_orcamentos_forro').setDisabled(true);
		Ext.getCmp('button_salvar_produtos_orcamentos_forro').setDisabled(true);

		if(me.tipo_produto=='S'){
			Ext.getCmp('quantidade_produtos_orcamentos_forro').setDisabled(false);
			Ext.getCmp('quantidade_produtos_orcamentos_forro').setVisible(true);

			me.produto_cor = true;
			comboCor.setDisabled(true);
			comboCor.setVisible(false);
			Ext.getCmp('proximo_step_2_produtos_orcamentos_forro').setDisabled(true);
			Ext.getCmp('step_2_produtos_orcamentos_forro').setExpanded(false);
		}
		else{
			Ext.getCmp('proximo_step_2_produtos_orcamentos_forro').setDisabled(false);
			Ext.getCmp('step_2_produtos_orcamentos_forro').setExpanded(true);

			Ext.getCmp('quantidade_produtos_orcamentos_forro').setDisabled(true);
			Ext.getCmp('quantidade_produtos_orcamentos_forro').setVisible(false);
			
			comboCor.setDisabled(false);
			comboCor.setVisible(true);

			comboCor.store.proxy.extraParams.id_produto = combo.getValue();
			//var qtde_cores = record.raw.produto_cores.length;
			console.log(record);
			Ext.getCmp('id_cor_produtos_orcamentos_forro').focus(false, 10);


			comboCor.store.load({
				callback: function(){
					me.produto_cor = true;
				}
			});
		}
	},

	addProduto: function(button){
		console.log('addProduto');
		var me = this;
		me.resetParams();
		me.areaTotalAmbiente = 0;
		me.getDesktopWindow('AddProdutos_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditProdutos', function(){
			Ext.getCmp('id_orcamento_produtos_orcamentos_forro').setValue(Ext.getCmp('GridProdutos_Orcamentos_Forro').store.proxy.extraParams.id_orcamento);
			Ext.getCmp('id_produto_produtos_orcamentos_forro').store.load({
				/*params: {
					in_cod_categoria: "1, 5",
					in_tipo_produto: "'P','I'"
				}*/
			});
			me.getGridAmbientes().store.removeAll();
		});
	},

	addAcessorio: function(button){
		console.log('addAcessorio');
		var me = this;
		var gridProdutos = me.getGridProdutos();
		if (gridProdutos.selModel.hasSelection()) {
			var record = me.record_produto = gridProdutos.getSelectionModel().getLastSelected();
			//if(record.raw.tipo_produto=='P' || record.raw.tipo_produto=='I'){
				var comboCliente = Ext.getCmp('id_cliente_orcamentos_forro');
				me.getCategoriaPrecos(comboCliente, comboCliente.getValue());

				me.getDesktopWindow('AddAcessorios_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditAcessorios', function(){
					Ext.getCmp('id_produto_acessorios_orcamentos_forro').store.load({
						/*params: {
							in_cod_categoria: "1, 5",
							in_tipo_produto: "'A'"
						}*/
					});

					var quantidade = Math.ceil((parseFloat(record.raw.soma) * 2) / 6);
					Ext.getCmp('id_produto_pai_acessorios_orcamentos_forro').setValue(record.get('id_produtos_orcamentos_forro'));
					Ext.getCmp('quantidade_acessorios_orcamentos_forro').setValue(quantidade);
				});
			//}
			//else{
			//	info(this.titleErro, "Um Acessório só pode ser associado a Produto/Produto Instalado!");                
			//}
		}
		else{
			me.record_produto = false;
			me.getDesktopWindow('AddAcessorios_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditAcessorios', function(){
				Ext.getCmp('id_produto_acessorios_orcamentos_forro').store.load({
					/*params: {
						in_cod_categoria: "1, 5",
						in_tipo_produto: "'A'"
					}*/
				});
			});
		}
	},

	addGeral: function(button) {
		var me = this;
		me.getDesktopWindow('AddGeral_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditGeral', false);
	},

	editProduto: function(button){
		console.log('editProduto');
		var me = this;
		var gridProdutos = me.getGridProdutos();
		me.resetParams();

		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();
			me.areaTotalAmbiente = record.get('quantidade');

			if(record.get('tipo_produto')=='A'){
				me.editAcessorio(button);
			}
			else{
				if(record.get('possui_ambiente')=='S'){
					me.getDesktopWindow('AddProdutos_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditProdutos', function(){
						me.getFormProdutos().el.mask('Aguarde...');
						Ext.getCmp('action_orcamentos_forro').setValue('EDITAR');
						Ext.getCmp('id_produtos_orcamentos_forro_produtos_orcamentos_forro').setValue(record.get('id_produtos_orcamentos_forro'));
						Ext.getCmp('item_nro_produtos_orcamentos_forro').setValue(record.get('item_nro'));
						Ext.getCmp('id_orcamento_produtos_orcamentos_forro').setValue(record.raw.id_orcamento);
						Ext.getCmp('valor_unitario_produtos_orcamentos_forro').setValue(record.get('valor_unitario'));
						
						Ext.getCmp('id_produto_produtos_orcamentos_forro').store.load({
							/*params: {
								in_cod_categoria: "1, 5",
								in_tipo_produto: "'P','I'"
							},*/
							callback: function(){
								me.produto_forro = true;
							}
						});

						Ext.getCmp('id_cor_produtos_orcamentos_forro').store.load({
							params: {
								id_produto: record.get('id_produto')
							},
							callback: function(){
								me.produto_cor = true;
							}
						});

						var store = me.getGridAmbientes().store;
						store.proxy.extraParams.id_produtos_orcamentos_forro = record.get('id_produtos_orcamentos_forro');
						store.load({
							callback: function(){
								var total = me.changeAreaTotalAmbientes('TOTAL');
							}
						});

						var a = setInterval(function(){
							if(record.get('id_produto') != null && record.get('id_produto') > 0){
								if(me.produto_forro==true){
									Ext.getCmp('id_produto_produtos_orcamentos_forro').setValue(''+record.get('id_produto')+'');
									me.produto_forro = null;
								}
							} 
							else{
								me.produto_forro = null;
							}

							if(record.get('id_cor') != null && record.get('id_cor') > 0){
								if(me.produto_cor==true){
									Ext.getCmp('id_cor_produtos_orcamentos_forro').setValue(''+record.get('id_cor')+'');
									me.produto_cor = null;
								}
							} 
							else{
								me.produto_cor = null;
							}

							if(me.produto_cor == null && me.produto_forro == null){
								if(me.getFormProdutos()){
									me.getFormProdutos().el.unmask();
									Ext.getCmp('action_produtos_orcamentos_forro').setValue('EDITAR');
									Ext.getCmp('step_2_produtos_orcamentos_forro').setDisabled(true);
									//me.setStepAmbientes(button);
								}
								
								clearInterval(a);
								me.produto_cor = false;
								me.produto_forro = false;
							}
						});
					});
				}
				else{
					me.getDesktopWindow('AddGeral_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditGeral', function(){
						me.produto_forro = false;
						Ext.getCmp('FormGeral_Orcamentos_Forro').el.mask('Aguarde...');
						Ext.getCmp('id_produto_geral_orcamentos_forro').store.load({
							/*params: {
								in_cod_categoria: "1, 5",
								in_tipo_produto: "'P','I'"
							},*/
							callback: function(){
								me.produto_forro = true;
							}
						});
						Ext.getCmp('action_geral_orcamentos_forro').setValue('EDITAR');
						Ext.getCmp('quantidade_geral_orcamentos_forro').setValue(record.get('quantidade'));
						Ext.getCmp('valor_unitario_geral_orcamentos_forro').setValue(record.get('valor_unitario'));
						Ext.getCmp('item_nro_geral_orcamentos_forro').setValue(record.get('item_nro'));
						Ext.getCmp('id_produto_geral_orcamentos_forro').setValue(record.get('id_produto'));
						Ext.getCmp('id_produtos_orcamentos_forro_geral_orcamentos_forro').setValue(record.get('id_produtos_orcamentos_forro'));

						var a = setInterval(function(){
							if(record.get('id_produto') != null && record.get('id_produto') > 0){
								if(me.produto_forro==true){
									Ext.getCmp('id_produto_geral_orcamentos_forro').setValue(''+record.get('id_produto')+'');

									setTimeout(function() {
										if(record.get('id_cor') != null && record.get('id_cor') > 0){
											Ext.getCmp('cor_geral_orcamentos_forro').setValue(''+record.get('id_cor')+'');
											console.log('Geral');
										}
									}, 800);

									me.produto_forro = null;
								}
							} 
							else{
								me.produto_forro = null;
							}

							if(me.produto_forro == null){
								if(Ext.getCmp('FormGeral_Orcamentos_Forro')){
									Ext.getCmp('FormGeral_Orcamentos_Forro').el.unmask();
								}
								
								clearInterval(a);
								me.produto_forro = false;
							}
						});
					});
				}
			}

		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},
	
	deleteProduto: function(button) {
		var me = this;
		me.resetParams();
		var gridProdutos = me.getGridProdutos();
		
		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar o Produto?', function(btn){
				if(btn=='yes'){
					if(record.get('id_produtos_orcamentos_forro')>0){
						me.deleteAjax(gridProdutos, 'produtos_orcamentos_forro', {
							action: 'DELETAR',
							id: record.get('id_produtos_orcamentos_forro'),
							id_orcamento: record.get('id_orcamento'),
							item_nro: record.get('item_nro')
						}, button, function(){
							gridProdutos.store.load();
							Ext.getCmp('GridOrcamentos_Observacoes').store.load();
						});
					}

					gridProdutos.store.remove(record);
					me.setValorTotal();
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();

					var count = Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_forro').setValue(count);

					if (Ext.getCmp('valor_total_orcamentos_forro').getValue()=='' || Ext.getCmp('valor_total_orcamentos_forro').getValue()==null){
						Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(false);
					}
					else {
						Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
						Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
					}
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	updateProduto: function(button){
		console.log('updateProduto');
		var me = this;
		if(!me.getFormProdutos().getForm().isValid()){
			return true;
		}
		var action          = Ext.getCmp('action_produtos_orcamentos_forro').getValue();
		var id_produto      = Ext.getCmp('id_produto_produtos_orcamentos_forro').getValue();
		var id_cor          = Ext.getCmp('id_cor_produtos_orcamentos_forro').getValue();
		var item_nro        = Ext.getCmp('item_nro_produtos_orcamentos_forro').getValue();
		
		//PRODUTO ou SERVICO
		var quantidade      = me.areaTotalAmbiente;
		//var valor_unitario  = me.getValorUnitario(Ext.getCmp('id_produto_produtos_orcamentos_forro'), Ext.getCmp('id_produto_produtos_orcamentos_forro').getValue());
		var valor_unitario  = Ext.getCmp('valor_unitario_produtos_orcamentos_forro').getValue();
		var valor_total     = valor_unitario * me.areaTotalAmbiente;
		
		//PRODUTO INSTALADO
		if(me.tipo_produto=='I'){
			var quantidade_servico  = me.areaTotalAmbiente;
			var record              = Ext.getCmp('id_produto_produtos_orcamentos_forro').store.getById(id_produto);
			var valor_servico       = quantidade_servico * parseFloat(record.raw.preco_custo_servico);
			//var valor_total         = valor_total + valor_servico;
			var valor_total         = valor_total;
		}

		var records         = me.getGridAmbientes().store.getModifiedRecords();
		var json_ambientes  = [];
		for(var i in records){
			json_ambientes.push(Ext.encode(records[i].data));
		}

		var id_orcamento = Ext.getCmp('id_orcamento_orcamentos_forro').getValue();
		valor_total = valor_total;

		me.formParams = {
			tipo_produto: me.tipo_produto,
			id_orcamento: id_orcamento,
			id_produto: id_produto,
			id_cor: id_cor,
			item_nro: item_nro,
			quantidade: quantidade,
			valor_unitario: valor_unitario,
			valor_bruto: valor_total.toFixed(2),
			valor_total: valor_total.toFixed(2),
			ambientes: [json_ambientes]
		};

		me.saveForm(me.getGridProdutos(), me.getFormProdutos(), me.getAddProdutosWin(), button, function(){
			me.getGridProdutos().store.proxy.extraParams.id_orcamento = id_orcamento;
			me.getGridProdutos().store.load({
				callback: function(){
					me.setValorTotal();
					Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
					Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();

					var count = Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_forro').setValue(count);
				}
			});
		}, false);
	},

	setStepAmbientes: function(button){
		var me = this;
		if(button==true || (Ext.getCmp('id_produto_produtos_orcamentos_forro').getValue()!=null && Ext.getCmp('id_cor_produtos_orcamentos_forro').getValue()!=null)){
			//Ext.getCmp('step_1_produtos_orcamentos_forro').setDisabled(true);
			//Ext.getCmp('proximo_step_2_produtos_orcamentos_forro').setDisabled(true);
			Ext.getCmp('step_2_produtos_orcamentos_forro').setDisabled(false);
			Ext.getCmp('button_salvar_produtos_orcamentos_forro').setDisabled(false);
			Ext.getCmp('button_resetar_produtos_orcamentos_forro').setDisabled(true);

			if(me.getGridAmbientes().store.getCount()>0){
				me.recalculateAmbiente();
			}
		}
	},
	////////////////////--PRODUTOS--////////////////////////


	////////////////////--ACESSORIOS--////////////////////////
	editAcessorio: function(button){
		console.log('editAcessorio');
		var me = this;
		var gridProdutos = me.getGridProdutos();
		//if (gridProdutos.selModel.hasSelection()) {
			var record =  gridProdutos.getSelectionModel().getLastSelected();
			if(record.get('id_produtos_orcamentos_forro_pai')!=null && record.get('id_produtos_orcamentos_forro_pai')>0){
				var id_record = record.get('id_produtos_orcamentos_forro_pai');
				me.record_produto = gridProdutos.store.getById(id_record);
			}
			else{
				me.record_produto = false;                
			}

			me.getDesktopWindow('AddAcessorios_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditAcessorios', function(){
				var form = Ext.getCmp('FormAcessorios_Orcamentos_Forro');
				form.el.mask('Aguarde...');

				Ext.getCmp('id_produto_acessorios_orcamentos_forro').store.load({
					/*params: {
						in_cod_categoria: "1, 5",
						in_tipo_produto: "'A'"
					},*/
					callback: function(){
						me.produto_forro = true;
					}
				});

				var a = setInterval(function(){
					if(record.get('id_produto') != null && record.get('id_produto') > 0){
						if(me.produto_forro==true){
							Ext.getCmp('id_produto_acessorios_orcamentos_forro').setValue(''+record.get('id_produto')+'');

							setTimeout(function() {
								if(record.get('id_cor') != null && record.get('id_cor') > 0){
									Ext.getCmp('cor_acessorios_orcamentos_forro').setValue(''+record.get('id_cor')+'');
									console.log('Acessório');
								}
							}, 300);

							me.produto_forro = null;
						}
					} 
					else{
						me.produto_forro = null;
					}

					if(me.produto_forro == null){
						if(form.el){
							form.el.unmask();
						}
						
						clearInterval(a);
						me.produto_forro = false;
					}
				}, 1000);

				Ext.getCmp('quantidade_acessorios_orcamentos_forro').setValue(record.get('quantidade'));
				Ext.getCmp('id_produto_pai_acessorios_orcamentos_forro').setValue(record.get('id_produtos_orcamentos_forro_pai'));
				Ext.getCmp('id_acessorios_orcamentos_forro').setValue(record.get('id_produtos_orcamentos_forro'));
				Ext.getCmp('item_nro_acessorios_orcamentos_forro').setValue(record.get('item_nro'));
				Ext.getCmp('valor_unitario_acessorios_orcamentos_forro').setValue(record.get('valor_unitario'));
				Ext.getCmp('action_acessorios_orcamentos_forro').setValue('EDITAR');
			});
		//}
		//else{
		//	info(this.titleErro, "Selecione um Acessório para Editar");
		//}
	},

	changeEmenda: function(combo, value){
		var me = this;
		if(combo.getValue()!=null){
			if(value=='S'){
				var quantidade = Math.ceil((parseFloat(me.record_produto.raw.soma) * 2) /6);
			}
			else{
				var quantidade = Math.ceil(me.record.get('quantidade') * 1.5);
			}

			Ext.getCmp('quantidade_acessorios_orcamentos_forro').setValue(quantidade);
		}
	},

	updateAcessorio: function(button){
		console.log('updateAcessorio');
		var me      = this;
		var win     = Ext.getCmp('AddAcessorios_Orcamentos_ForroWin');
		var form    = Ext.getCmp('FormAcessorios_Orcamentos_Forro');

		if(!form.getForm().isValid()){
			return true;
		}

		var quantidade      = Ext.getCmp('quantidade_acessorios_orcamentos_forro').getValue();
		var valor_unitario  = Ext.getCmp('valor_unitario_acessorios_orcamentos_forro').getValue();
		var valor_total     = valor_unitario * quantidade;
		var id_orcamento = Ext.getCmp('id_orcamento_orcamentos_forro').getValue();

		me.formParams = {
			tipo_produto: 'A',
			id_orcamento: id_orcamento,
			valor_unitario: valor_unitario,
			valor_bruto: valor_total.toFixed(2),
			valor_total: valor_total.toFixed(2)
		};


		me.saveForm(me.getGridProdutos(), form, win, button, function(){
			me.getGridProdutos().store.proxy.extraParams.id_orcamento = id_orcamento;
			me.getGridProdutos().store.load({
				callback: function(){
					me.setValorTotal();

					Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
					Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();

					var count = Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos_forro').setValue(count);
				}
			});
		}, false);
	},
	////////////////////--ACESSORIOS--////////////////////////



	////////////////////--AMBIENTES--////////////////////////
	addAmbiente: function(button){
		var me = this;
		me.getDesktopWindow('AddAmbientes_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditAmbientes');
	},

	editAmbiente: function(button){
		var me = this;
		var gridAmbientes = me.getGridAmbientes();

		if (gridAmbientes.selModel.hasSelection()) {
			var record = gridAmbientes.getSelectionModel().getLastSelected();
			
			me.getDesktopWindow('AddAmbientes_Orcamentos_ForroWin', 'OrcamentosForro', 'orcamentos_forro.EditAmbientes', function(){
				me.getFormAmbientes().getForm().setValues(record.data);
				Ext.getCmp('action_ambientes_orcamentos_forro').setValue('EDITAR');
			});
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	deleteAmbiente: function(button) {
		var me = this;
		var gridAmbientes = me.getGridAmbientes();
		if (gridAmbientes.selModel.hasSelection()) {
			var record = gridAmbientes.getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar o Ambiente?', function(btn){
				if(btn=='yes'){
					if(record.get('id_ambientes_orcamentos_forro')>0){
						me.deleteAjax(gridAmbientes, 'ambientes_orcamentos_forro', {
							action: 'DELETAR',
							id: record.get('id_ambientes_orcamentos_forro'),
							id_orcamento: Ext.getCmp('GridProdutos_Orcamentos_Forro').store.proxy.extraParams.id_orcamento
						}, button, false);
					}
					
					me.changeSomaAmbientes('DELETAR', null, record.get('comprimento') + record.get('largura'));
					me.changeAreaTotalAmbientes('DELETAR', null, record.get('area'));

					gridAmbientes.store.remove(record);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	getComprimentoLarguraProduto: function(id_produto){
		console.log('getComprimentoLarguraProduto');
		var me = this;
		var comprimento = 0;
		var largura = 0;

		window.StoreComboProdutosOrcamentosForro = Ext.getCmp('id_produto_produtos_orcamentos_forro').store;
		if(window.StoreComboProdutosOrcamentosForro){
			var record = StoreComboProdutosOrcamentosForro.getById(id_produto);
			var attrs = record.raw.atributos_produto;
			
			if(attrs!=null && attrs!=undefined && attrs.length>0){
				for(var i=0; i<attrs.length;i++){
					if(attrs[i]['sigla']=='comprimento'){
					   comprimento = parseFloat(attrs[i]['valor']);
					}
					else if(attrs[i]['sigla']=='largura'){
					   largura = parseFloat(attrs[i]['valor']);
					}
				}                
			}
		}

		return {comprimento: comprimento, largura: largura};
	},

	updateAmbiente: function(button){
		console.log('updateAmbiente');
		var me = this;
		var values = me.getFormAmbientes().getForm().getValues();
		if(!me.getFormAmbientes().getForm().isValid()){
			return true;
		}

		var divLarguraAltura = me.getComprimentoLarguraProduto(Ext.getCmp('id_produto_produtos_orcamentos_forro').getValue());
		
		values.largura = (Math.ceil(values.largura_real / divLarguraAltura.largura)) * divLarguraAltura.largura;
		values.comprimento = (Math.ceil(values.comprimento_real / divLarguraAltura.comprimento)) * divLarguraAltura.comprimento;
		
		var largura = parseFloat(values.largura).toFixed(2);
		var comprimento = parseFloat(values.comprimento).toFixed(2);

		var qtd_metros = (Math.ceil(comprimento / divLarguraAltura.comprimento)) * divLarguraAltura.comprimento;
		var qtd_folhas = Math.ceil(largura / divLarguraAltura.largura);
		qtd_metros = qtd_metros.toFixed(2);

		var comboProduto  = Ext.getCmp('id_produto_produtos_orcamentos_forro');
		var recordProduto = comboProduto.store.getById(comboProduto.getValue());
		if (recordProduto.raw.dobra_quantidade == "S") {
			qtd_folhas = Number(qtd_folhas*2);
		}

		if(values.action=='INSERIR'){
			var id_ambiente_pai = 0;
			if(Ext.getCmp('sn_l_ambientes_orcamentos_forro').getValue()=='S'){
				var record = me.getGridAmbientes().getSelectionModel().getLastSelected();
				if(record){
					if(record.get('id_ambientes_orcamentos_forro')>0 && record.get('sn_l')=='N'){
						id_ambiente_pai = record.get('id_ambientes_orcamentos_forro');
					}
				}
			}

			me.getGridAmbientes().store.add({
				ambiente: values.ambiente,
				observacoes: values.observacoes,
				largura: largura,
				largura_real: values.largura_real,
				comprimento: comprimento,
				comprimento_real: values.comprimento_real,
				area: (comprimento * largura).toFixed(2),
				qtd_pecas: qtd_folhas,
				qtd_metros: qtd_metros,
				sn_l: Ext.getCmp('sn_l_ambientes_orcamentos_forro').getValue(),
				tipo_acessorio: Ext.getCmp('tipo_acessorio_ambientes_orcamentos_forro').getValue(),
				id_ambiente_pai: id_ambiente_pai,
				descricao_pecas: qtd_folhas+' de '+qtd_metros+'m'
			});

			me.changeSomaAmbientes('INSERIR', comprimento + largura);
			me.changeAreaTotalAmbientes('INSERIR', (comprimento * largura).toFixed(2));
		}
		else{
			window.rec = me.getGridAmbientes().getSelectionModel().getLastSelected();

			me.changeSomaAmbientes('EDITAR', comprimento + largura, rec.get('comprimento') + rec.get('largura'));
			me.changeAreaTotalAmbientes('EDITAR', (comprimento * largura).toFixed(2), rec.get('area'));

			rec.set('ambiente', values.ambiente);
			rec.set('observacoes', values.observacoes);
			rec.set('largura', largura);
			rec.set('comprimento', comprimento);
			rec.set('largura_real', values.largura_real);
			rec.set('comprimento_real', values.comprimento_real);
			rec.set('area', (comprimento * largura).toFixed(2));
			rec.set('qtd_pecas', qtd_folhas);
			rec.set('qtd_metros', qtd_metros);
			rec.set('sn_l', Ext.getCmp('sn_l_ambientes_orcamentos_forro').getValue());
			rec.set('tipo_acessorio', Ext.getCmp('tipo_acessorio_ambientes_orcamentos_forro').getValue());
			rec.set('id_ambiente_pai', Ext.getCmp('id_ambiente_pai_ambientes_orcamentos_forro').getValue());
			rec.set('descricao_pecas', qtd_folhas+' de '+qtd_metros+'m');
		}

		me.getAddAmbientesWin().close();
		me.getGridAmbientes().getSelectionModel().deselectAll();
	},

	recalculateAmbiente: function(){
		console.log('recalculateAmbiente');
		var me = this;
		var grid = me.getGridAmbientes();
		var produto = Ext.getCmp('id_produto_produtos_orcamentos_forro');
		var divLarguraAltura = me.getComprimentoLarguraProduto(produto.getValue());
		var items = grid.store.data.items;

		me.somaTotalAmbiente = 0;
		me.areaTotalAmbiente = 0;

		for(var i=0;i<items.length;i++){
			var item = items[i];

			var larguraTemp = (Math.ceil( item.get('largura_real') / divLarguraAltura.largura )) * divLarguraAltura.largura;
			var comprimentoTemp = (Math.ceil( item.get('comprimento_real') / divLarguraAltura.comprimento )) * divLarguraAltura.comprimento;
			
			var largura = parseFloat(larguraTemp).toFixed(2);
			var comprimento = parseFloat(comprimentoTemp).toFixed(2);
			var qtd_metros = (Math.ceil( comprimento / divLarguraAltura.comprimento )) * divLarguraAltura.comprimento;
			var qtd_folhas = Math.ceil( largura / divLarguraAltura.largura );
			qtd_metros = qtd_metros.toFixed(2);

			var comboProduto  = Ext.getCmp('id_produto_produtos_orcamentos_forro');
			var recordProduto = comboProduto.store.getById(comboProduto.getValue());
			if (recordProduto.raw.dobra_quantidade == "S") {
				qtd_folhas = Number(qtd_folhas*2);
			}

			item.set('largura', largura);
			item.set('comprimento', comprimento);
			item.set('area', (comprimento * largura).toFixed(2));
			item.set('qtd_pecas', qtd_folhas);
			item.set('qtd_metros', qtd_metros);
			item.set('descricao_pecas', qtd_folhas+' de '+qtd_metros+'m');

			me.changeSomaAmbientes('INSERIR', comprimento + largura);
			me.changeAreaTotalAmbientes('INSERIR', (comprimento * largura).toFixed(2));
		}
	},

	////////////////////--AMBIENTES--////////////////////////

	changeSomaAmbientes: function(action, newValor, oldValor){
		var me      = this;
		me.somaTotalAmbiente = parseFloat(me.somaTotalAmbiente);
		if(action=='INSERIR'){
			me.somaTotalAmbiente += parseFloat(newValor);
		}
		else if(action=='EDITAR'){
			me.somaTotalAmbiente -= parseFloat(oldValor);
			me.somaTotalAmbiente += parseFloat(newValor);
		}
		else if(action=='DELETAR'){
			me.somaTotalAmbiente -= parseFloat(oldValor);
		}
		else{
			var grid    = me.getGridAmbientes();
			var items   = grid.store.data.items;
			
			me.somaTotalAmbiente = 0;

			for(var i=0;items.length;i++){
				if(items[i]!=undefined){
					me.somaTotalAmbiente += parseFloat(items[i].get('comprimento') + items[i].get('largura'));
				}
			}

			return me.somaTotalAmbiente;
		}
	},

	changeAreaTotalAmbientes: function(action, newValor, oldValor){
		var me      = this;
		me.areaTotalAmbiente = parseFloat(me.areaTotalAmbiente);
		if(action=='INSERIR'){
			me.areaTotalAmbiente += parseFloat(newValor);
		}
		else if(action=='EDITAR'){
			me.areaTotalAmbiente -= parseFloat(oldValor);
			me.areaTotalAmbiente += parseFloat(newValor);
		}
		else if(action=='DELETAR'){
			me.areaTotalAmbiente -= parseFloat(oldValor);
		}
		else{
			me.areaTotalAmbiente = 0;
			var grid    = me.getGridAmbientes();

			grid.store.each(function(record,idx){
				me.areaTotalAmbiente += parseFloat(record.get('area'));
			});

			return me.areaTotalAmbiente;
		}
		me.areaTotalAmbiente = Math.round((me.areaTotalAmbiente ).toFixed(10) * 100)/100;

		Ext.getCmp('total_m2_orcamentos_forro').setValue(me.areaTotalAmbiente);
	},

	enableCliente: function(checkbox, newValue){
		var me = this;

		me.resetCombo(Ext.getCmp('button_id_cliente_orcamentos_forro'));
		Ext.getCmp('cliente_descricao_orcamentos_forro').reset();
		
		if(newValue==false){
			Ext.getCmp('cliente_descricao_orcamentos_forro').setVisible(true);
			Ext.getCmp('cliente_descricao_orcamentos_forro').setDisabled(false);
			Ext.getCmp('cliente_descricao_orcamentos_forro').setValue('CONSUMIDOR');
			Ext.getCmp('fieldcontainer_id_cliente_orcamentos_forro').setVisible(false);
			Ext.getCmp('id_cliente_orcamentos_forro').setDisabled(true);
		}
		else{
			Ext.getCmp('cliente_descricao_orcamentos_forro').setVisible(false);
			Ext.getCmp('cliente_descricao_orcamentos_forro').setDisabled(true);
			Ext.getCmp('fieldcontainer_id_cliente_orcamentos_forro').setVisible(true);
			Ext.getCmp('id_cliente_orcamentos_forro').setDisabled(false);
		}
	},

	orcamentoVencido: false,

	getProduto: function(id_produto){
		/*'StoreComboProdutos',
		'StoreComboProdutosServicos',
		'StoreComboProdutosAcessorios',*/
	},

	changeCliente: function(id_categoria_precos, only_price, id_cliente){
		console.log('changeCliente');
		var me              = this;
		var grid            = Ext.getCmp('GridProdutos_Orcamentos_Forro');
		var items           = grid.store.data.items;
		var produtoStore    = window.StoreComboProdutosOrcamentosForro;
		var total           = 0;

		this.allProdutos(function(me){
			var grid            = Ext.getCmp('GridProdutos_Orcamentos_Forro');
			var items           = grid.store.data.items;
			var produtoStore    = window.StoreComboProdutosOrcamentosForro;
			var total           = 0;

			if(only_price!=true){
				if(!produtoStore || me.areaTotalAmbiente==null){
					return true;
				}
			}

			console.log('Calculando os Ítens X...');
			for(var i=0;i<items.length;i++){
				var i2 = i+1;
				var recordProduto = produtoStore.getById(''+items[i].get('id_produto')+'');
				console.log(items[i]);
				console.log(recordProduto);
				console.log('Item: '+i2);

				if(recordProduto){
					if(me.id_categoria_precos_old!=id_categoria_precos){
						var valor_unit = me.getValorRecord(recordProduto, id_categoria_precos);
					}
					else{
						var valor_unit = items[i].get('valor_unitario');
					}

					if(me.orcamentoVencido){
						var valor_unit = me.getValorRecord(recordProduto, id_categoria_precos);
					}

					console.log('Valor Unitario do item: '+i2+': '+valor_unit);
					if(only_price!=true){
						var valor_total = valor_unit * me.areaTotalAmbiente;
						items[i].set('quantidade', me.areaTotalAmbiente);
					}
					else{
						var valor_total = (valor_unit * items[i].get('quantidade')).toFixed(2);
						var and_sand = (items[i].get('quantidade')).toFixed(2);
						console.log('Qtde Item '+i2+' = '+and_sand);
					}
					console.log('Valor total do Item: '+i2+' (valor Unitario * qtde: '+valor_total);
					items[i].set('valor_unitario', valor_unit);
					items[i].set('valor_total', valor_total);

					//1 centavo
					total += parseFloat(valor_total);
					total = Math.round((total).toFixed(10) * 100)/100;
					console.log('Somando os Itens....: '+total)
				}
			}

			setTimeout(function(){
				me.setValorTotal(total);
			}, 300);
		});
	},

	getValorRecord: function(recordProduto, id_categoria_precos){
		var preco   = recordProduto.raw.preco_venda || recordProduto.raw.preco_custo;
		var precos = recordProduto.raw.produto_precos;

		if(precos!=undefined){
			for(var i=0;i<precos.length;i++){
				if(precos[i].id_categoria_precos==id_categoria_precos){
					preco = precos[i].valor;
				}
			}
		}

		if(preco==undefined || preco==null){
			info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria');
		}

		return preco;
	},

	setValorTotal: function(total){
		var me = this;
		var grid = Ext.getCmp('GridProdutos_Orcamentos_Forro');
		var items = grid.store.data.items;
		var desconto = Ext.getCmp('desconto_real_orcamentos_forro').getValue();
		var acrescimo = Ext.getCmp('acrescimo_real_orcamentos_forro').getValue();
		if(typeof total != 'number'){
			total = null;
		}
		if(total==undefined || total==null){
			total = 0;

			for(var i=0;i<items.length;i++){
				console.log('Item: '+i+' valor total de: '+items[i].get('valor_total'))
				total += items[i].get('valor_total');
			}
		}

		var subTotal = total;
		subTotal = Math.round((subTotal).toFixed(10) * 100)/100;
		Ext.getCmp('valor_bruto_orcamentos_forro').setValue(subTotal);

		if(acrescimo!=""){
			total += parseFloat(acrescimo);
		}

		if(desconto!=""){
			total -= parseFloat(desconto);
		}

		total = Math.round((total).toFixed(10) * 100)/100;
		Ext.getCmp('valor_total_orcamentos_forro').setValue(total);
	},  

	enableAll: false,

	allProdutos: function(callback){
		var me = this;
		control.getController('OrcamentosForro').getStore('StoreComboProdutos').load({
			callback: function(){
				me.enableAll = true;
				window.StoreComboProdutosOrcamentosForro = control.getController('OrcamentosForro').getStore('StoreComboProdutos');
				if(typeof callback == 'function'){
					callback(me);
				}
			}
		});
	},

	changeGridValores: function(comboCliente, value){
		var me = this;
		var produtoStore    = window.StoreComboProdutosOrcamentosForro;

		if(!produtoStore){
			control.getController('OrcamentosForro').getStore('StoreComboProdutos').load({
				params: {

				},
				callback: function(){
					window.StoreComboProdutosOrcamentosForro = control.getController('OrcamentosForro').getStore('StoreComboProdutos');
					me.getCategoriaPrecos(comboCliente, value);
					me.changeCliente(me.id_categoria_precos, true, value);
				}
			});
		}
		else{
			me.getCategoriaPrecos(comboCliente, value);
			me.changeCliente(me.id_categoria_precos, true, value);
		}
	},

	getValorUnitario: function(produto, id_produto){
		var me              = this;
		var produtoStore    = produto.store;
		var recordProduto   = produtoStore.getById(id_produto);

		if(recordProduto){
			return me.getValorRecord(recordProduto, me.id_categoria_precos);
		}
		else{
			return 0;
		}
	},

	getCategoriaPrecos: function(comboCliente, value){
		var me = this;
		me.id_categoria_precos_old = me.id_categoria_precos;
		if(Ext.getCmp('cliente_cadastrado_orcamentos_forro').getValue()==true){
			var recordCliente = comboCliente.store.getById(comboCliente.getValue());

			if(recordCliente && recordCliente.raw.id_categoria_precos!=null){
				me.id_categoria_precos = recordCliente.raw.id_categoria_precos;
				var texto = 'Pegando tabela de preço para: '+recordCliente.raw.id_categoria_precos+' '+comboCliente.getValue();
				console.log(texto)
			}
		}
		return me.id_categoria_precos;
	},

	changeQuantidadeAndPrecoProdutos: function(){
		var me = this;
		me.changeCliente(me.id_categoria_precos);
	},

	update: function(button) {
		var me         = this;
		var grid       = Ext.getCmp('GridOrcamentos');
		var win        = Ext.getCmp('AddOrcamentosForroWin');
		var form       = Ext.getCmp('FormOrcamentosForro');

		//remover orcamento nao salvo
		me.delete_orcamento = false;

		var records         = me.getGridProdutos().store.getModifiedRecords();
		var json_produtos  = [];
		for(var i in records){
			json_produtos.push(Ext.encode(records[i].data));
		}

		me.formParams = {
			produtos: [json_produtos]
		};
		
		me.saveForm(grid, form, win, button, function(){
			me.resetParams();
			me.areaTotalAmbiente = 0;
		}, false);
	},

	btCopiaItemOrcamento: function(button) {
		var me = this;
		var gridProdutos = me.getGridProdutos();


		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Gerar uma Cópia do Ítem: '+record.get('item_nro')+'?', function(btn){
				if(btn=='yes'){
					me.GeraCopiaItem(gridProdutos, record, button);
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar um Orçamento para poder Copiar');
			return true;
		}
	},

	GeraCopiaItem: function(grid, record, button, callbackSuccess) {
		var me = this;
		me.acoesAjax(grid, 'orcamentos_forro', {
			action: 'COPIA_ITEM_ORCAMENTO',
			orcamento_nro: record.get('id_orcamento'),
			nro_itens: Ext.getCmp('GridProdutos_Orcamentos_Forro').store.getCount(),
			id_produtos_orcamentos_forro: record.get('id_produtos_orcamentos_forro'),
			possui_ambiente: record.get('possui_ambiente')
		}, button, function(){
			var c = setInterval(function(){
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();
				me.setValorTotal();
				Ext.getCmp('button_resetar_orcamentos_forro').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos_forro').setDisabled(false);
				clearInterval(c);
			}, 1500);
		});
	}
});