/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produto_Composicoes', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Produto_Composicoes',
	started: true,
	noDeleteLoad: true,

	refs: [
		{
			ref: 'list',
			selector: 'gridproduto_composicoeslist'
		},
		{
			ref: 'form',
			selector: 'addproduto_composicoeswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridproduto_composicoeslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterproduto_composicoeswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterproduto_composicoeswin form'
		},
		{
			ref: 'addWin',
			selector: 'addproduto_composicoeswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelProduto_Composicoes'
	],
	stores: [
		'StoreComboProdutos',
		'StoreProduto_Composicoes'
	],
	
	views: [
		'produto_composicoes.Grid',
		'produto_composicoes.List',
		'produto_composicoes.Filtro',
		'produto_composicoes.Edit'
	],

	init: function(application) {
		this.control({
			'gridproduto_composicoeslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'gridproduto_composicoeslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridproduto_composicoeslist button[action=adicionar]': {
				click: this.add
			},
			'gridproduto_composicoeslist button[action=editar]': {
				click: this.btedit
			},
			'gridproduto_composicoeslist button[action=deletar]': {
				click: this.btdel
			},
			'addproduto_composicoeswin button[action=salvar]': {
				click: this.update
			},
			'addproduto_composicoeswin button[action=resetar]': {
				click: this.reset
			},
			'addproduto_composicoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addproduto_composicoeswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addproduto_composicoeswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterproduto_composicoeswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterproduto_composicoeswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterproduto_composicoeswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterproduto_composicoeswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddProduto_ComposicoesWin', 'Produto_Composicoes', 'produto_composicoes.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Composições');
			me.getForm().getForm().setValues(record.data);

			Ext.getCmp('id_composicao_produto_composicoes').setValue(''+record.data.id_composicao+'');
			//me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_produto_composicoes'), 'server/modulos/produto_composicoes/list.php');
			Ext.getCmp('action_produto_composicoes').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		grid.store.remove(record);        
		if(record.get('id_produto_composicoes')==0){
			return true;
		}

		me.deleteAjax(grid, 'produto_composicoes', {
			action: 'DELETAR',
			id: record.get('id_produto_composicoes')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id_produto_composicoes')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Produto_Composicoes', 'Produto_Composicoes', 'produto_composicoes.Edit', function(){
			var id_produto = Ext.getCmp('id_produtos').getValue();
			Ext.getCmp('id_produto_produto_composicoes').setValue(id_produto);
		});
	},

	update: function(button) {
		var me = this;
		var values = me.getForm().getForm().getValues();
		var record = Ext.getCmp('id_composicao_produto_composicoes').store.getById(values.id_composicao);
		if(!record){
			var record = Ext.getCmp('id_composicao_produto_composicoes').store.getById(parseInt(values.id_composicao));
		}

		if(values.action=='INSERIR'){
			me.getList().store.add({
				id_produto_composicoes: 0,
				id_produto: values.id_produto,
				quantidade: values.quantidade,
				id_composicao: record.get('id'),
				composicao: record.get('descricao')
			});
		}
		else{
			window.rec = this.getList().getSelectionModel().getLastSelected();
			window.record = record;
			rec.set('id_composicao', record.get('id'));
			rec.set('composicao', record.get('descricao'));
			rec.set('quantidade', values.quantidade);
			this.getList().getSelectionModel().deselectAll();
		}

		me.getAddWin().close();
		//me.saveForm(me.getList(), var, me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.produto_composicoes.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});