/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produtos_Categorias', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Produtos_Categorias',

	refs: [
		{
			ref: 'list',
			selector: 'produtos_categoriaslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addprodutos_categoriaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'produtos_categoriaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterprodutos_categoriaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterprodutos_categoriaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addprodutos_categoriaswin'
		}
	],

	models: [
		'ModelCombo',
		'ModelProdutos_Categorias'
	],
	stores: [
		'StoreComboAtivo',
		'StoreComboAtributos',
		'StoreProdutos_Categorias',
		'StoreComboProdutos_Categorias',
		'StoreComboDeptoCategorias'
	],

	views: [
		'produtos_categorias.List',
		'produtos_categorias.Edit'
	],

	init: function(application) {
		this.control({
			'produtos_categoriaslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'produtos_categoriaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'produtos_categoriaslist button[action=adicionar]': {
				click: this.add
			},
			'produtos_categoriaslist button[action=propriedades]': {
				click: this.propriedades
			},

			'produtos_categoriaslist button[action=responsavel_os]': {
				click: this.responsavel_os
			},

			'produtos_categoriaslist button[action=editar]': {
				click: this.btedit
			},
			'produtos_categoriaslist button[action=deletar]': {
				click: this.btdel
			},
			'produtos_categoriaslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addprodutos_categoriaswin button[action=salvar]': {
				click: this.update
			},
			'addprodutos_categoriaswin button[action=resetar]': {
				click: this.reset
			},
			'addprodutos_categoriaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addprodutos_categoriaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterprodutos_categoriaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterprodutos_categoriaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterprodutos_categoriaswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterprodutos_categoriaswin': {
				show: this.filterSetFields
			}
		});
	},

	responsavel_os: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('List-Os_Departamentos_Responsavel', 'Os_Departamentos_Responsavel', 'os_departamentos_responsavel.List', function(controller){
				Ext.getCmp('List-Os_Departamentos_Responsavel').setTitle('Responsável pelo Departamento: '+record.get('descricao')+' nas OS');
				controller.cod_depto = record.get('cod_depto');
				controller.getList().store.proxy.extraParams.action = 'RESPONSAVEL_EM_OS';
				//Ext.getCmp('id_depto_os_departamentos_responsavel').setValue(parseInt(record.get('cod_depto')));
				controller.getList().store.proxy.extraParams.id_depto = parseInt(record.get('cod_categoria'));
				controller.getList().store.proxy.extraParams.nome_depto = record.get('descricao');
				//controller.getList().store.load();
			});
		}
		else{
			info(me.titleErro, me.editErroGrid);
			return true;
		}
	},

	propriedades: function(button){
		var me = this;		
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddProdutos_CategoriasWin', 'Produtos_Categorias', 'produtos_categorias.Edit', function(){
			me.getAddWin().setTitle('Alterar Categorias de Produtos');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_categoria'), 'server/modulos/produtos_categorias/list.php', function(data){
				Ext.getCmp('id_parent_categoria').setValue(parseInt(data.id_parent));
			});
			Ext.getCmp('action_produtos_categorias').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'produtos_categorias', {
			action: 'DELETAR',
			id: record.get('cod_categoria')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Desativar a Categoria: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Produtos_Categorias', 'Produtos_Categorias', 'produtos_categorias.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(grid, record){
		if(this.existPermissao('produtos_categorias', 'editar')==true){
			Ext.getCmp('button_edit_produtos_categorias').setDisabled(false);
		}

		if(this.existPermissao('produtos_categorias', 'deletar')==true){
			Ext.getCmp('button_del_produtos_categorias').setDisabled(false);
		}

		if(this.existPermissao('produtos_categorias', 'responsavel_os')==true){
			if(record.get('id_parent') == null || record.get('id_parent') == 0){
				Ext.getCmp('button_responsavel_os_produtos_categorias').setDisabled(false);				
			}
			else{
				Ext.getCmp('button_responsavel_os_produtos_categorias').setDisabled(true);
			}
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_produtos_categorias').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridProdutos_Categorias').store.proxy.extraParams.status_categoria = item.value;
		Ext.getCmp('GridProdutos_Categorias').store.load({
			start: 0,
			limit: 50
		});
	}
});