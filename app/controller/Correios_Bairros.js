/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Correios_Bairros', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Correios_Bairros',
	
	refs: [
		{
			ref: 'list',
			selector: 'correios_bairroslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcorreios_bairroswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'correios_bairroslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercorreios_bairroswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercorreios_bairroswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcorreios_bairroswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelCombo',
		'ModelCorreios_Bairros'
	],
	stores: [
		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboCorreios_Bairros',
		'StoreCorreios_Bairros',
		'StoreComboAtivo'
	],
	
	views: [
		'correios_bairros.List',
		'correios_bairros.Filtro',
		'correios_bairros.Edit'
	],

	init: function(application) {
		this.control({
			'correios_bairroslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'correios_bairroslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'correios_bairroslist button[action=adicionar]': {
				click: this.add
			},
			'correios_bairroslist button[action=list_enderecos]': {
				click: this.listEnderecos
			},
			'correios_bairroslist button[action=editar]': {
				click: this.btedit
			},
			'correios_bairroslist button[action=deletar]': {
				click: this.btdel
			},			
			'addcorreios_bairroswin button[action=salvar]': {
				click: this.update
			},
			'addcorreios_bairroswin button[action=resetar]': {
				click: this.reset
			},
			'addcorreios_bairroswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcorreios_bairroswin form fieldcontainer combobox[id=uf_sigla_correios_bairros]': {
				change: this.loadCidade
			},
			'addcorreios_bairroswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcorreios_bairroswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercorreios_bairroswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercorreios_bairroswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercorreios_bairroswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercorreios_bairroswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('localidade_id_correios_bairros');
		}
		else{
			var comboCidade = Ext.getCmp('localidade_id_filter_correios_bairros');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	listEnderecos: function(button){
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.getDesktopWindow('List-Correios_Enderecos', 'Correios_Enderecos', 'correios_enderecos.List', function(controller){
				controller.getList().store.proxy.extraParams.uf_sigla = record.get('uf_sigla');
				controller.getList().store.proxy.extraParams.localidade_id = record.get('localidade_id');
				controller.getList().store.proxy.extraParams.bairro_id_inicial = record.get('id');
			});
		}
		else{
			info(this.titleErro, this.detalharErroGrid);
			return true;
		}
	},

	edit: function(grid, record) {
		var me = this;
		me.cidade = false;
		me.getDesktopWindow('AddCorreios_BairrosWin', 'Correios_Bairros', 'correios_bairros.Edit', function(){
			me.getAddWin().setTitle('Alteração de Bairros');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/correios_bairros/list.php', function(dados){
				var a = setInterval(function(){
					if(me.cidade==true){
						if(dados.localidade_id!=null){
							Ext.getCmp('localidade_id_correios_bairros').setValue(dados.localidade_id);
							me.cidade = null;
						}
						else{me.cidade = null;}
					}
					if(me.cidade == null){
						clearInterval(a);
						me.getForm().el.unmask();
					}
					
				}, 1000);
			});
			Ext.getCmp('action_correios_bairros').setValue('EDITAR');
		});
		Ext.getCmp('bairro_nome_correios_bairros').focus(false, 1000);
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'correios_bairros', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Apagar este Bairro: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.cidade = false;

		me.getDesktopWindow('Correios_Bairros', 'Correios_Bairros', 'correios_bairros.Edit', function(){
			var frm = button.up('form');
			if(frm!=null && frm.down('combobox[id=estado_clientes]')){
				var v = Ext.getCmp('estado_clientes').getValue();
				if(v!="" && v!=null){
					Ext.getCmp('uf_sigla_correios_bairros').setValue(v);
				}
			}
			else {
				var uf_sigla = me.getList().store.proxy.extraParams.uf_sigla;
				Ext.getCmp('uf_sigla_correios_bairros').setValue(uf_sigla);

				Ext.getCmp('localidade_id_correios_bairros').store.proxy.extraParams.uf_sigla = uf_sigla;
				Ext.getCmp('localidade_id_correios_bairros').store.load();

			}
			
			var a = setInterval(function(){
				me.getForm().el.mask("Aguarde...");
				if(me.cidade==true){
					if(Ext.getCmp('cidade_clientes')){
						var v = Ext.getCmp('cidade_clientes').getValue();
						if(v!="" && v!=null){
							Ext.getCmp('localidade_id_correios_bairros').setValue(v);
						}
						me.cidade = null;
					}
					else if(Ext.getCmp('localidade_id_correios_bairros')){
						var v = me.getList().store.proxy.extraParams.localidade_id;
						if(v!="" && v!=null){
							Ext.getCmp('localidade_id_correios_bairros').setValue(''+v+'');
						}
						me.cidade = null;
					}
					else{
						me.cidade = null;
					}
				}
				else{
					if(Ext.getCmp('uf_sigla_correios_bairros').getValue()==null){
						me.cidade = null;
					}
				}
				if(me.cidade == null){
					clearInterval(a);
					me.getForm().el.unmask();
				}
			}, 1000);
		});

		Ext.getCmp('bairro_nome_correios_bairros').focus(false, 1000);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('correios_bairros', 'editar')==true){
			Ext.getCmp('button_edit_correios_bairros').setDisabled(false);
		}

		if(this.existPermissao('correios_bairros', 'deletar')==true){
			Ext.getCmp('button_del_correios_bairros').setDisabled(false);
		}

		if(this.existPermissao('correios_bairros', 'list_enderecos')==true){
			Ext.getCmp('button_add_enderecos_correios_bairros').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_correios_bairros').getValue();
		this.getList().store.load({
			start: 0,
			limit: 30
		});
	}
});
