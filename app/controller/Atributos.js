/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Atributos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	storePai: true,
	tabela: 'Atributos',
	
	refs: [
		{
			ref: 'list',
			selector: 'atributoslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addatributoswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'atributoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteratributoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filteratributoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addatributoswin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelAtributos'
	],
	stores: [
		'StoreComboAtributos',
		'StoreComboAtivo',
		'StoreAtributos'
	],
	
	views: [
		'atributos.List',
		'atributos.Filtro',
		'atributos.Edit'
	],

	init: function(application) {
		this.control({
			'atributoslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'atributoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'atributoslist button[action=adicionar]': {
				click: this.add
			},
			'atributoslist button[action=editar]': {
				click: this.btedit
			},
			'atributoslist button[action=deletar]': {
				click: this.btdel
			},
			'addatributoswin button[action=salvar]': {
				click: this.update
			},
			'addatributoswin button[action=resetar]': {
				click: this.reset
			},
			'addatributoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addatributoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addatributoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteratributoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteratributoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteratributoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteratributoswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddAtributosWin', 'Atributos', 'atributos.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Atributos');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_atributos'), 'server/modulos/atributos/list.php');
			Ext.getCmp('action_atributos').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'atributos', {
			action: 'DELETAR',
			id: record.get('id_atributos')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja Desativar o Atributo: '+record.get('atributo')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Atributos', 'Atributos', 'atributos.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.atributos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});