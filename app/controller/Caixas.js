/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Caixas', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Caixas',
	started: true,

	refs: [
		{
			ref: 'list',
			selector: 'caixaslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcaixaswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'caixaslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercaixaswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercaixaswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcaixaswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelCaixas'
	],

	stores: [
		'StoreCaixas',
		'StoreComboCaixasSituacao'
	],

	views: [
		'caixas.List',
		'caixas.Filtro',
		'caixas.Edit'
	],

	init: function(application) {
		this.control({
			'caixaslist gridpanel': { 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado/*,
				celldblclick: this.btedit*/
			},
			'caixaslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'caixaslist button[action=adicionar]': {
				click: this.add
			},
			'caixaslist button[action=editar]': {
				click: this.btedit
			},
			'caixaslist button[action=deletar]': {
				click: this.btdel
			},
			'caixaslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'caixaslist button[action=ver_totais]': {
				click: this.conferirTotais
			},
			'caixaslist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addcaixaswin button[action=salvar]': {
				click: this.update
			},
			'addcaixaswin button[action=resetar]': {
				click: this.reset
			},
			'addcaixaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcaixaswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcaixaswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercaixaswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercaixaswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercaixaswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercaixaswin': {
				show: this.filterSetFields
			}
		});
	},

	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/caixas/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddCaixasWin', 'Caixas', 'caixas.Edit', function(){
			me.getAddWin().setTitle('Alteração do Caixa');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cx_nro'), 'server/modulos/caixas/list.php');
			Ext.getCmp('action_caixas').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'caixas', {
			action: 'DELETAR',
			id: record.get('cx_nro')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('cx_nro')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Caixas', 'Caixas', 'caixas.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	conferirTotais: function(button){
		var me = this;

		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			var caixa_atual_user = NroCaixaDesejado;
			var caixa_selecionado = record.get('cx_nro');
			NroCaixaDesejado2 = caixa_selecionado;

			me.getDesktopWindow('AddCaixa_ItensWin', 'Caixa_Itens', 'caixa_itens.Totais', function(controller){
				control.getController('Caixa_Itens').getAddWin().setTitle('Conferindo Valores do Caixa: '+record.get('cx_nro'));
				
				//control.getController('Caixa_Itens').getStore('StoreCaixa_Itens').proxy.extraParams.caixa_nro = record.get('cx_nro');
				//control.getController('Caixa_Itens').getStore('StoreCaixa_Itens').load();
				Ext.getCmp('GridCaixaItensAdiantamentos').store.proxy.extraParams.caixa_nro = caixa_selecionado;
				Ext.getCmp('GridCaixaItensSuprimentos').store.proxy.extraParams.caixa_nro = caixa_selecionado;
				Ext.getCmp('GridCaixaItensMeiosPagtos').store.proxy.extraParams.caixa_nro = caixa_selecionado;
				Ext.getCmp('GridCaixaItensRetiradas').store.proxy.extraParams.caixa_nro = caixa_selecionado;
				Ext.getCmp('GridCaixaItensVendas').store.proxy.extraParams.caixa_nro = caixa_selecionado;
			});

			var c = setInterval(function(){
				NroCaixaDesejado = caixa_atual_user;
				//clearInterval(c);
			}, 1000);
		}
		else{
			info(me.titleErro, 'Selecione um Caixa para detalhar...');
			return true;
		}
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('caixas', 'editar')==true){
			Ext.getCmp('button_edit_caixas').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.caixas.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridCaixas').store.proxy.extraParams.filtrar_status = item.value;
		Ext.getCmp('GridCaixas').store.load({
			start: 0,
			limit: 25
		});
	}
});