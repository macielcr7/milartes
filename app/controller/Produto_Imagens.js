/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Produto_Imagens', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Produto_Imagens',
	
	refs: [
		{
			ref: 'list',
			selector: 'gridproduto_imagenslist'
		},
		{
			ref: 'form',
			selector: 'addproduto_imagenswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'gridproduto_imagenslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterproduto_imagenswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterproduto_imagenswin form'
		},
		{
			ref: 'addWin',
			selector: 'addproduto_imagenswin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelProduto_Imagens'
	],
	stores: [
		'StoreComboProdutos',
		'StoreProduto_Imagens',
		'StoreComboProdutosCores2'
	],
	
	views: [
		'produto_imagens.Grid',
		'produto_imagens.List',
		'produto_imagens.Filtro',
		'produto_imagens.Edit'
	],

	init: function(application) {
		this.control({
			'gridproduto_imagenslist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'gridproduto_imagenslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridproduto_imagenslist button[action=adicionar]': {
				click: this.add
			},
			'gridproduto_imagenslist button[action=editar]': {
				click: this.btedit
			},
			'gridproduto_imagenslist button[action=deletar]': {
				click: this.btdel
			},
			'addproduto_imagenswin button[action=salvar]': {
				click: this.update
			},
			'addproduto_imagenswin button[action=resetar]': {
				click: this.reset
			},
			'addproduto_imagenswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addproduto_imagenswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addproduto_imagenswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterproduto_imagenswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterproduto_imagenswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterproduto_imagenswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterproduto_imagenswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddProduto_ImagensWin', 'Produto_Imagens', 'produto_imagens.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Imagens');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_produto_imagens'), 'server/modulos/produto_imagens/list.php');
			Ext.getCmp('action_produto_imagens').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'produto_imagens', {
			action: 'DELETAR',
			id: record.get('id_produto_imagens')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar a imagem?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Produto_Imagens', 'Produto_Imagens', 'produto_imagens.Edit', function(){
			var id_produto = Ext.getCmp('id_produtos').getValue();
			Ext.getCmp('id_produto_produto_imagens').setValue(id_produto);

			Ext.getCmp('id_cor_produto_imagens').store.load({
				params: {
					id_produto: id_produto
				}
			});
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.produto_imagens.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});