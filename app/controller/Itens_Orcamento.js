/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Itens_Orcamento', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Itens_Orcamento',
	started: true,
	refs: [
		{
			ref: 'list',
			selector: 'griditens_orcamentolist'
		},
		{
			ref: 'form',
			selector: 'additens_orcamentowin form'
		},
		{
			ref: 'filterBtn',
			selector: 'itens_orcamentolist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteritens_orcamentowin'
		},
		{
			ref: 'filterForm',
			selector: 'filteritens_orcamentowin form'
		},
		{
			ref: 'addWin',
			selector: 'additens_orcamentowin'
		}
	],
	models: [
		'ModelItens_Orcamento'
	],
	stores: [
		'StoreItens_Orcamento',
		'StoreComboProdutos',
		'StoreComboProdutosCores',
		'StoreComboOrcamentosVendas'
	],
	views: [
		'itens_orcamento.Grid',
		'itens_orcamento.List',
		'itens_orcamento.Filtro',
		'itens_orcamento.Edit'
	],

	init: function(application) {
		this.control({
			'griditens_orcamentolist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'griditens_orcamentolist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'griditens_orcamentolist button[action=adicionar]': {
				click: this.add
			},
			'griditens_orcamentolist button[action=editar]': {
				click: this.btedit
			},
			'griditens_orcamentolist button[action=deletar]': {
				click: this.btdel
			},
			'griditens_orcamentolist button[action=copiar_item_orcamento]':{
				click: this.btCopiaItemOrcamento
			},
			'additens_orcamentowin button[action=salvar]': {
				click: this.update
			},
			'additens_orcamentowin button[action=resetar]': {
				click: this.reset
			},
			'additens_orcamentowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'additens_orcamentowin form fieldcontainer combobox[id=id_produto_itens_orcamento]': {
				change: this.setValorProduto
			},
			'additens_orcamentowin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'additens_orcamentowin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteritens_orcamentowin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteritens_orcamentowin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteritens_orcamentowin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteritens_orcamentowin': {
				show: this.filterSetFields
			}
		});
	},

	id_categoria_precos: 1,

	setValorProduto: function(combo, value){
		var me = this;
		me.id_categoria_precos = 1;
		if(Ext.getCmp('cliente_cadastrado_orcamentos').getValue()==true){
			var comboCliente  = Ext.getCmp('id_cliente_orcamentos');
			var recordCliente = comboCliente.store.getById(comboCliente.getValue());
		
			if(recordCliente){
				me.id_categoria_precos = recordCliente.raw.id_categoria_precos;
			}
		}

		var recordProduto = combo.store.getById(value);
		var comboCor = Ext.getCmp('cor_itens_orcamento');

		if (recordProduto!=null && value!=null && !isNaN(parseInt(value))) {
			var qtde_cores = recordProduto.raw.produto_cores.length;

			if (qtde_cores > 0) {
				comboCor.setDisabled(false);
				comboCor.setVisible(true);

				comboCor.store.proxy.extraParams.id_produto = combo.getValue();
				comboCor.store.load({
					callback: function(r, options, success){
						comboCor.setValue(r[0].data.id);
						if (Ext.getCmp('quantidade_itens_orcamento').getValue() == null) {
							Ext.getCmp('quantidade_itens_orcamento').setValue(1);
						}
						
						if (qtde_cores == 1) {
							Ext.getCmp('quantidade_itens_orcamento').focus(false, 10);
						}
						else {
							Ext.getCmp('cor_itens_orcamento').focus(false, 10);
						}
					}
				});
			}
			else {
				Ext.getCmp('cor_itens_orcamento').reset();
				comboCor.store.removeAll();
	 			comboCor.setDisabled(true);
				comboCor.setVisible(false);
			}

			if(recordProduto){
				window.StoreProdutoItemOrcamento = combo.store;
				Ext.getCmp('quantidade_itens_orcamento').setMaxValue(recordProduto.raw.quantidade_disponivel);
				Ext.getCmp('valor_unitario_itens_orcamento').setValue( me.getValorRecord(recordProduto, me.id_categoria_precos));            
			}
			else{
				//info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria!!!');
			}
		}
		else {
			Ext.getCmp('cor_itens_orcamento').reset();
			comboCor.store.removeAll();
 			comboCor.setDisabled(true);
			comboCor.setVisible(false);
		}
	},

	getValorRecord: function(recordProduto, id_categoria_precos){
		var preco   = recordProduto.raw.preco_venda || recordProduto.raw.preco_custo;
		var precos = recordProduto.raw.produto_precos;

		if(precos!=undefined){
			for(var i=0;i<precos.length;i++){
				if(precos[i].id_categoria_precos==id_categoria_precos){
					preco = precos[i].valor;
				}
			}
		}

		if(preco==undefined || preco==null){
			info('Atenção!', 'O preço não foi encontrado! <br> O Produto não possui os preços referentes a categoria');
		}
		
		return preco;
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddItens_OrcamentoWin', 'Itens_Orcamento', 'itens_orcamento.Edit', function(){
			me.getAddWin().setTitle('Alteração do Ítem');
			var data = record.data;
			data.id_produto = ""+data.id_produto+"";
			//console.log(data);
			me.getForm().getForm().setValues(data);
			//me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_itens_orcamento'), 'server/modulos/itens_orcamento/list.php');
			Ext.getCmp('action_itens_orcamento').setValue('EDITAR');

			setTimeout(function() {
				if(data.id_cor != null && data.id_cor > 0){
					Ext.getCmp('cor_itens_orcamento').setValue(''+data.id_cor+'');
				}
			}, 1000);
		});
	},

	noDeleteLoad: true,

	del: function(grid, record, button) {
		var me = this;
		var id = record.get('id_itens_orcamento');
		grid.store.remove(record);
		me.getList().getSelectionModel().deselectAll();
		me.getController('Orcamentos').setValorTotal();

		if(id>0){
			me.deleteAjax(grid, 'itens_orcamento', {
				action: 'DELETAR',
				id: id,
				id_orcamento: record.get('id_orcamento'),
				item_nro: record.get('item_nro')
			}, button, function(){
				Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
				Ext.getCmp('GridItens_Orcamento').store.load();
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();
			});
		}
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('produto')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Itens_Orcamento', 'Itens_Orcamento', 'itens_orcamento.Edit', false);
	},

	update: function(button) {
		var me = this;
		var values = me.getForm().getForm().getValues();
		var record = Ext.getCmp('id_produto_itens_orcamento').store.getById(values.id_produto);
		if(!record){
			var record = Ext.getCmp('id_produto_itens_orcamento').store.getById(parseInt(values.id_produto));
		}

		window.StoreComboProdutosOrcamentos = Ext.getCmp('id_produto_itens_orcamento').store;

		values.quantidade = values.quantidade.replace(',', '.');
		me.formParams = {
			valor_total: values.valor_unitario * values.quantidade,
			id_orcamento: Ext.getCmp('id_orcamento_orcamentos').getValue()
		};
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(){
			me.getList().store.load({
				callback: function(){
					me.getController('Orcamentos').setValorTotal();
					Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
					Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
					Ext.getCmp('GridOrcamentos_Observacoes').store.load();
					var count = Ext.getCmp('GridItens_Orcamento').store.getCount();
					Ext.getCmp('total_de_itens_orcamentos').setValue(count);
				}
			})
		}, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.itens_orcamento.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	btCopiaItemOrcamento: function(button) {
		var me = this;
		var gridProdutos = Ext.getCmp('GridItens_Orcamento');

		if (gridProdutos.selModel.hasSelection()) {
			var record = gridProdutos.getSelectionModel().getLastSelected();
			Ext.Msg.confirm('Confirmar', 'Deseja Gerar uma Cópia do Ítem: '+record.get('item_nro')+'?', function(btn){
				if(btn=='yes'){
					me.GeraCopiaItem(gridProdutos, record, button);
				}
			});
		}
		else{
			info(this.titleErro, 'Você deve Selecionar um Orçamento para poder Copiar');
			return true;
		}
	},

	GeraCopiaItem: function(grid, record, button, callbackSuccess) {
		var me = this;

		me.acoesAjax(grid, 'itens_orcamento', {
			action: 'COPIA_ITEM_ORCAMENTO',
			orcamento_nro: record.get('id_orcamento'),
			nro_itens: Ext.getCmp('GridItens_Orcamento').store.getCount(),
			nro_item_selecionado: record.get('item_nro'),
			id_itens_orcamento: record.get('id_itens_orcamento'),
		}, button, function(){
			var c = setInterval(function(){
				Ext.getCmp('GridOrcamentos_Observacoes').store.load();
				me.getController('Orcamentos').setValorTotal();
				Ext.getCmp('button_resetar_orcamentos').setDisabled(true);
				Ext.getCmp('button_salvar_orcamentos').setDisabled(false);
				clearInterval(c);
			}, 1500);
		});
	}
});