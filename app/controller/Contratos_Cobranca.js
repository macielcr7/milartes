/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Contratos_Cobranca', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Contratos_Cobranca',

	refs: [
		{
			ref: 'list',
			selector: 'contratos_cobrancalist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcontratos_cobrancawin form'
		},
		{
			ref: 'filterBtn',
			selector: 'contratos_cobrancalist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercontratos_cobrancawin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercontratos_cobrancawin form'
		},
		{
			ref: 'addWin',
			selector: 'addcontratos_cobrancawin'
		}
	],

	models: [
		'ModelCombo',
		'ModelContratos_Cobranca'
	],
	stores: [
		'StoreComboAtivo',
		'StoreComboContratosCobrancaLayout',
		'StoreComboContratosCobrancaModalidadeTaxa',
		'StoreComboContratosContaBancaria',
		'StoreContratos_Cobranca'
	],

	views: [
		'contratos_cobranca.List',
		'contratos_cobranca.Edit'
	],

	init: function(application) {
		this.control({
			'contratos_cobrancalist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'contratos_cobrancalist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'contratos_cobrancalist button[action=adicionar]': {
				click: this.add
			},
			'contratos_cobrancalist button[action=editar]': {
				click: this.btedit
			},
			'contratos_cobrancalist button[action=deletar]': {
				click: this.btdel
			},
			'contratos_cobrancalist button menuitem[action=filtrar_status]': {
				click: this.btStoreLoadFielter2
			},
			'addcontratos_cobrancawin button[action=salvar]': {
				click: this.update
			},
			'addcontratos_cobrancawin button[action=resetar]': {
				click: this.reset
			},
			'addcontratos_cobrancawin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addcontratos_cobrancawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercontratos_cobrancawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercontratos_cobrancawin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercontratos_cobrancawin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercontratos_cobrancawin': {
				show: this.filterSetFields
			}
		});
	},


	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddContratos_CobrancaWin', 'Contratos_Cobranca', 'contratos_cobranca.Edit', function(){
			me.getAddWin().setTitle('Alteração do Contrato de Cobrança');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/contratos_cobranca/list.php');
			Ext.getCmp('action_contratos_cobranca').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'contratos_cobranca', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar o Contrato: '+record.get('descricao')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Contratos_Cobranca', 'Contratos_Cobranca', 'contratos_cobranca.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('contratos_cobranca', 'editar')==true){
			Ext.getCmp('button_edit_contratos_cobranca').setDisabled(false);
		}

		if(this.existPermissao('contratos_cobranca', 'deletar')==true){
			Ext.getCmp('button_del_contratos_cobranca').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_contratos_cobranca').getValue();
		this.getList().store.load({
			start: 0,
			limit: 25
		});
	},

	btStoreLoadFielter2: function(item, checked){
		Ext.getCmp('GridContratos_Cobranca').store.proxy.extraParams.status_cobranca = item.value;
		Ext.getCmp('GridContratos_Cobranca').store.load({
			start: 0,
			limit: 50
		});
	}
});