/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Atributos_Categoria', {
    extend: 'Ext.app.Controller',
	mixins: {
        controls: 'ShSolutions.controller.Util'
    },

	tabela: 'Atributos_Categoria',
	
	refs: [
        {
        	ref: 'list',
        	selector: 'atributos_categorialist gridpanel'
        },
        {
        	ref: 'form',
        	selector: 'addatributos_categoriawin form'
        },
        {
        	ref: 'filterBtn',
        	selector: 'atributos_categorialist button[action=filtrar]'
        },
        {
        	ref: 'filterWin',
        	selector: 'filteratributos_categoriawin'
        },
        {
        	ref: 'filterForm',
        	selector: 'filteratributos_categoriawin form'
        },
        {
        	ref: 'addWin',
        	selector: 'addatributos_categoriawin'
        }
    ],
	
    models: [
		'ModelComboLocal',
		'ModelAtributos_Categoria'
	],
	stores: [
		'StoreComboAtributos',
		'StoreComboProdutos_Categorias',
		'StoreAtributos_Categoria'		
	],
	
    views: [
        'atributos_categoria.List',
        'atributos_categoria.Filtro',
        'atributos_categoria.Edit'
    ],

    init: function(application) {
    	this.control({
    		'atributos_categorialist gridpanel': {                 
				afterrender: this.getPermissoes,
                render: this.gridLoad
            },
            'atributos_categorialist button[action=filtrar]': {
            	click: this.btStoreLoadFielter
            },
            'atributos_categorialist button[action=adicionar]': {
                click: this.add
            },
            'atributos_categorialist button[action=editar]': {
                click: this.btedit
            },
            'atributos_categorialist button[action=deletar]': {
                click: this.btdel
            },            'addatributos_categoriawin button[action=salvar]': {
                click: this.update
            },
            'addatributos_categoriawin button[action=resetar]': {
                click: this.reset
            },
            'addatributos_categoriawin form fieldcontainer combobox': {
                change: this.enableButton,
				render: this.comboLoad
            },
            'addatributos_categoriawin form fieldcontainer button[action=reset_combo]': {
                click: this.resetCombo
            },
			'addatributos_categoriawin form fieldcontainer button[action=add_win]': {
                click: this.getAddWindow
            },
            'filteratributos_categoriawin form fieldcontainer combobox': {
                change: this.enableButton,
				render: this.comboLoad
            },
            'filteratributos_categoriawin button[action=resetar_filtro]': {
                click: this.resetFielter
            },
            'filteratributos_categoriawin button[action=filtrar_busca]': {
                click: this.setFielter
            },
            'filteratributos_categoriawin': {
                show: this.filterSetFields
            }
        });
    },
	
    edit: function(grid, record) {
    	var me = this;
		me.getDesktopWindow('AddAtributos_CategoriaWin', 'Atributos_Categoria', 'atributos_categoria.Edit', function(){
    		me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Atributos_Categoria');
	        me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_atributos_categoria'), 'server/modulos/atributos_categoria/list.php');
	        Ext.getCmp('action_atributos_categoria').setValue('EDITAR');
    	});
    },

    del: function(grid, record, button) {
     	var me = this;
     	me.deleteAjax(grid, 'atributos_categoria', {
			action: 'DELETAR',
			id: record.get('id_atributos_categoria')
		}, button, false);

    },

    btedit: function(button) {
        if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
    },

    btdel: function(button) {
    	var me = this;
        if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id_atributos_categoria')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
    },

    add: function(button) {
		var me = this;
        me.getDesktopWindow('Atributos_Categoria', 'Atributos_Categoria', 'atributos_categoria.Edit', false);
    },

    update: function(button) {
    	var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
    },

    btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
    	if(!win) var win = Ext.create('ShSolutions.view.atributos_categoria.Filtro', {
    		animateTarget: button.getEl()
    	});
    	win.show();
    }

});
