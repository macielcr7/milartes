/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Recibos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Recibos',

	refs: [
		{
			ref: 'list',
			selector: 'reciboslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addreciboswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'reciboslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterreciboswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterreciboswin form'
		},
		{
			ref: 'addWin',
			selector: 'addreciboswin'
		}
	],
	
	models: [
		'ModelCombo',
		'ModelRecibos'
	],
	stores: [
		'StoreRecibos',
		'StoreComboRecibosDeNome'
	],
	
	views: [
		'recibos.List',
		'recibos.Edit'
	],

	init: function(application) {
		this.control({
			'reciboslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'reciboslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'reciboslist button[action=adicionar]': {
				click: this.add
			},
			'reciboslist button[action=editar]': {
				click: this.btedit
			},
			'reciboslist button[action=deletar]': {
				click: this.btdel
			},
			'addreciboswin button[action=salvar]': {
				click: this.update
			},
			'addreciboswin button[action=resetar]': {
				click: this.reset
			},
			'reciboslist button[action=imprimir_recibo]': {
				click: this.imprimirRecibo
			},
			'addreciboswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'addreciboswin form fieldcontainer combobox': {
				change: this.enableButton,
				afterrender: this.comboLoad
			},
			'addreciboswin form fieldcontainer combobox[id=recebido_de_nome_recibos]': {
				change: this.setDocto1
			},
			'addreciboswin form fieldcontainer combobox[id=emitente_nome_recibos]': {
				change: this.setDocto2
			}
		});
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	imprimirRecibo: function(button){
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/recibos/imprime_recibo.php?id='+record.get('id')+'&copias=1'
			);
		}
		else{
			info(this.titleErro, this.detalharErroGrid);
			return true;
		}
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddRecibosWin', 'Recibos', 'recibos.Edit', function(){
			me.getAddWin().setTitle('Alterar Recibo');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/recibos/list.php');
			Ext.getCmp('action_recibos').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
	 	var me = this;
	 	me.deleteAjax(grid, 'recibos', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Apagar o Recibo Emitido Para: '+record.get('emitente_nome')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Recibos', 'Recibos', 'recibos.Edit', function(){
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('recibos', 'editar')==true){
			Ext.getCmp('button_edit_recibos').setDisabled(false);
		}

		if(this.existPermissao('recibos', 'deletar')==true){
			Ext.getCmp('button_del_recibos').setDisabled(false);
		}

		if(this.existPermissao('recibos', 'imprimir_recibo')==true){
			Ext.getCmp('button_imprimir_recibos').setDisabled(false);
		}
	},

	setDocto1: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			Ext.getCmp('recebido_de_docto_recibos').setValue(record.get('docto'));
			Ext.getCmp('referente_recibos').focus(false, 100);
		}
	},

	setDocto2: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			Ext.getCmp('emitente_docto_recibos').setValue(record.get('docto'));
			Ext.getCmp('referente_recibos').focus(false, 100);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_recibos').getValue();
		this.getList().store.load({
			start: 0,
			limit: 50
		});
	}
});