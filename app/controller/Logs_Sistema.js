/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Logs_Sistema', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Logs_Sistema',
	started: true,

	refs: [
		{
			ref: 'list',
			selector: 'gridlogs_sistemalist'
		},
		{
			ref: 'form',
			selector: 'addlogs_sistemawin form'
		},
		{
			ref: 'filterBtn',
			selector: 'logs_sistemalist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterlogs_sistemawin'
		},
		{
			ref: 'filterForm',
			selector: 'filterlogs_sistemawin form'
		},
		{
			ref: 'addWin',
			selector: 'addlogs_sistemawin'
		}
	],

	models: [
		'ModelLogs_Sistema'
	],
	stores: [
		'StoreLogs_Sistema'
	],
	
	views: [
		'logs_sistema.List',
		'logs_sistema.Filtro',
		'logs_sistema.Edit',
		'logs_sistema.LogsSistemaList'
	],

	init: function(application) {
		this.control({
			'gridlogs_sistemalist': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'gridlogs_sistemalist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'gridlogs_sistemalist button[action=adicionar]': {
				click: this.add
			},
			'gridlogs_sistemalist button[action=editar]': {
				click: this.btedit
			},
			'gridlogs_sistemalist button[action=deletar]': {
				click: this.btdel
			},			
			'addlogs_sistemawin button[action=salvar]': {
				click: this.update
			},
			'addlogs_sistemawin button[action=resetar]': {
				click: this.reset
			},
			'addlogs_sistemawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addlogs_sistemawin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addlogs_sistemawin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterlogs_sistemawin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterlogs_sistemawin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterlogs_sistemawin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterlogs_sistemawin': {
				show: this.filterSetFields
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddLogs_SistemaWin', 'Logs_Sistema', 'logs_sistema.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Logs_Sistema');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('controle'), 'server/modulos/logs_sistema/list.php');
			Ext.getCmp('action_logs_sistema').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'logs_sistema', {
			action: 'DELETAR',
			id: record.get('controle')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('controle')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Logs_Sistema', 'Logs_Sistema', 'logs_sistema.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.logs_sistema.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});