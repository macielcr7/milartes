/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Cores', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},
	storePai: true,
	tabela: 'Cores',
	refs: [
		{
			ref: 'list',
			selector: 'coreslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addcoreswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'coreslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filtercoreswin'
		},
		{
			ref: 'filterForm',
			selector: 'filtercoreswin form'
		},
		{
			ref: 'addWin',
			selector: 'addcoreswin'
		}
	],
	models: [
		'ModelCombo',
		'ModelCores'
	],
	stores: [
		'StoreComboCores',
		'StoreCores'
	],
	views: [
		'cores.List',
		'cores.Edit'
	],
	init: function(application) {
		this.control({
			'coreslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad
			},
			'coreslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'coreslist button[action=adicionar]': {
				click: this.add
			},
			'coreslist button[action=editar]': {
				click: this.btedit
			},
			'coreslist button[action=deletar]': {
				click: this.btdel
			},
			'addcoreswin button[action=salvar]': {
				click: this.update
			},
			'addcoreswin button[action=resetar]': {
				click: this.reset
			},
			'addcoreswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addcoreswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addcoreswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filtercoreswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filtercoreswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filtercoreswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filtercoreswin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddCoresWin', 'Cores', 'cores.Edit', function(){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Cores');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id_cores'), 'server/modulos/cores/list.php');
			Ext.getCmp('action_cores').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'cores', {
			action: 'DELETAR',
			id: record.get('id_cores')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id_cores')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Cores', 'Cores', 'cores.Edit', false);
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	btStoreLoadFielterX: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.cores.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_cores').getValue();
		this.getList().store.load({
			start: 0,
			limit: 30
		});
	}
});