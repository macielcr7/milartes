/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Fornecedores', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Fornecedores',
	
	refs: [
		{
			ref: 'list',
			selector: 'fornecedoreslist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addfornecedoreswin form'
		},
		{
			ref: 'filterBtn',
			selector: 'fornecedoreslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filterfornecedoreswin'
		},
		{
			ref: 'filterForm',
			selector: 'filterfornecedoreswin form'
		},
		{
			ref: 'addWin',
			selector: 'addfornecedoreswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelFornecedores'
	],
	stores: [
		'StoreComboSexoFornecedores',
		'StoreComboSituacaoCadastralFornecedores',
		'StoreComboTipo_FornecedorFornecedores',
		'StoreComboProfissaoFornecedores',
		
		'StoreFornecedores_Anotacoes',
		'StoreFornecedores_Contato',
		'StoreFornecedores_Endereco',
		'StoreFornecedor_Logs_Sistema',

		'StoreComboCorreios_Estados',
		'StoreComboCorreios_Localidades',
		'StoreComboCorreios_Bairros',
		'StoreComboCorreios_Enderecos',
		'StoreComboCepFornecedores',
		'StoreComboUsuarios',
		'StoreFornecedores'
	],

	views: [
		'fornecedores.List',
		'fornecedores.Edit'
	],

	menuGrid: Ext.create('Ext.menu.Menu', {
		id: 'menu_grid_fornecedores',
		width: 240,
		items: [
			{
				text: 'Adicionar uma Ordem de Compra',
				iconCls: 'btn_add'
			}, 
			{
				text: 'Adicionar uma Conta a Pagar',
				iconCls: 'btn_add'
			}, 
			{
				text: 'Alterar Cadastro',
				iconCls: 'btn_add'
			}
		]
	}),

	init: function(application) {
		this.control({
			'fornecedoreslist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				itemcontextmenu: this.contextMenu,
				select: this.RegistroSelecionado
			},
			'fornecedoreslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'fornecedoreslist button[action=adicionar]': {
				click: this.add
			},
			'fornecedoreslist button[action=editar]': {
				click: this.btedit
			},
			'fornecedoreslist button[action=deletar]': {
				click: this.btdel
			},
			'fornecedoreslist button[action=gerar_pdf]': {
				click: this.gerarPdf
			},
			'addfornecedoreswin tabpanel': {
				tabchange: this.loadActive
			},
			'addfornecedoreswin button[id=button_salvar_fornecedores]': {
				click: this.update
			},
			'addfornecedoreswin button[id=button_resetar_fornecedores]': {
				click: this.reset
			},
			'addfornecedoreswin form fieldcontainer combobox': {
				change: this.enableButton,
				afterrender: this.comboLoad
			},
			'addfornecedoreswin form fieldcontainer combobox[id=estado_fornecedores]': {
				change: this.loadCidade
			},
			'addfornecedoreswin form fieldcontainer combobox[id=cidade_fornecedores]': {
				change: this.loadBairro
			},
			'addfornecedoreswin form fieldcontainer combobox[id=endereco_fornecedores]': {
				change: this.setEndereco
			},
			'addfornecedoreswin form fieldcontainer textfield[id=cpf_fornecedores]': {
				blur: this.verifyUnique
			},
			'addfornecedoreswin form fieldcontainer textfield[id=cnpj_fornecedores]': {
				blur: this.verifyUnique
			},
			'addfornecedoreswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addfornecedoreswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filterfornecedoreswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filterfornecedoreswin form fieldcontainer combobox[id=estado_filter_fornecedores]': {
				change: this.loadCidade
			},
			'filterfornecedoreswin form fieldcontainer combobox[id=cidade_filter_fornecedores]': {
				change: this.loadBairro
			},
			'filterfornecedoreswin form fieldcontainer combobox[id=bairro_filter_fornecedores]': {
				change: this.loadEndereco
			},
			'filterfornecedoreswin form fieldcontainer combobox[id=tipo_fornecedor_filter_fornecedores]': {
				change: this.habilityFilter
			},

			'filterfornecedoreswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filterfornecedoreswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filterfornecedoreswin': {
				show: this.filterSetFields
			}
		});
	},

	cidade: false,
	endereco: false,

	contextMenu: function(view, record, item, index,  e){
		e.stopEvent();
		this.menuGrid.showAt(e.getXY());
	},

	loadActive: function(comp, newcomp, oldcomp){
		if(newcomp.store){
			newcomp.store.load();
		}
	},

	setEndereco: function(combo){
		var me = this;
		var record = combo.store.getById(combo.getValue());
		if(!record){
			return true;
		}
		else{
			
			Ext.getCmp('bairro_nome_fornecedores').setValue(record.get('bairro_nome'));
			Ext.getCmp('bairro_fornecedores').setValue(record.get('bairro_id_inicial'));
			if (record.get('loteamento') != '') {
				Ext.getCmp('loteamento_fornecedores').setValue(record.get('loteamento'));
			}
			if (record.get('ponto_referencia') != '') {
				Ext.getCmp('ponto_ref_fornecedores').setValue(record.get('ponto_referencia'));
			}
			Ext.getCmp('cep_fornecedores').setValue(record.get('cep'));
			Ext.getCmp('nro_end_fornecedores').focus(false, 100);
		}
	},

	verifyUnique: function(field){
		var v = field.getValue();
		var cod_fornecedor = field.up('form').down('[name=cod_fornecedor]').getValue();
		Ext.Ajax.request({
			url: 'server/modulos/fornecedores/list.php',
			method: 'POST',
			params:{
				action: 'VALID_UNIQUE',
				cod_fornecedor: cod_fornecedor,
				param: field.name,
				valor: v
			},
			success: function(s, o){
				var dados = Ext.decode(s.responseText);
				if(dados.success==false){
					field.markInvalid(field.fieldLabel+' J� Existe...');
				}
			}
		});
	},

	loadCidade: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('cidade_fornecedores');
		}
		else{
			var comboCidade = Ext.getCmp('cidade_filter_fornecedores');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.uf_sigla = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.cidade = true;
			}
		});
	},

	loadBairro: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			Ext.getCmp('endereco_fornecedores').store.proxy.extraParams.localidade_id = combo.getValue();
			me.bairro = true;
			if(Ext.getCmp('endereco_fornecedores').isDisabled()){
				Ext.getCmp('endereco_fornecedores').setDisabled(false);
			}
			return true;
		}
		else{
			var comboCidade = Ext.getCmp('bairro_filter_fornecedores');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.localidade_id = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.bairro = true;
			}
		});
	},

	loadEndereco: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(this.getForm()){
			var comboCidade = Ext.getCmp('endereco_fornecedores');
		}
		else{
			var comboCidade = Ext.getCmp('endereco_filter_fornecedores');
		}
		if(comboCidade.isDisabled()){
			comboCidade.setDisabled(false);
		}
		comboCidade.store.proxy.extraParams.bairro_btwn = combo.getValue();
		comboCidade.store.load({
			callback: function(){
				me.endereco = true;
			}
		});
	},
   
	gerarPdf: function(button){
		var me = this;
		window.open('server/modulos/fornecedores/pdf.php?'+
			Ext.Object.toQueryString(me.getList().store.proxy.extraParams)
		);
	},

	comboEndereco: function(id, bairro, endereco){
		var me = this;
		var comboBairro = Ext.getCmp('endereco_fornecedores');
		if(comboBairro.isDisabled()){
			comboBairro.setDisabled(false);
		}
		comboBairro.store.proxy.extraParams.localidade_id = id;
		if(bairro!=null){
			comboBairro.store.proxy.extraParams.bairro_btwn = bairro;
		}

		if(endereco){
			var params = {id: endereco};
		}
		else{
			var params = {};
		}

		comboBairro.store.load({
			params: params,
			callback: function(){
				me.endereco = true;
			}
		});
	},

	edit: function(grid, record) {
		var me = this;
		
		me.getDesktopWindow('AddFornecedoresWin', 'Fornecedores', 'fornecedores.Edit', function(dados){
			me.getAddWin().setTitle('Edi&ccedil;&atilde;o de Fornecedores');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('cod_fornecedor'), 'server/modulos/fornecedores/list.php', function(dados){
				me.getForm().el.mask('Aguarde...');
				window.controlS = me;
				
				var a = setInterval(function(){
					if(dados.cidade != null && dados.cidade != ""){
						if(me.cidade==true){
							Ext.getCmp('cidade_fornecedores').setValue(dados.cidade);
							me.comboEndereco(dados.cidade, dados.bairro, dados.endereco);
							me.cidade = null;
						}
					} else {
							me.cidade = null;
					}

					if(dados.endereco != null && dados.endereco != ""){
						if(me.endereco==true){
							Ext.getCmp('endereco_fornecedores').setValue(parseInt(dados.endereco));
							me.endereco = null;
						}
					}else{me.endereco = null;}
					if(me.cidade == null && me.endereco == null){
						if(me.getForm()){
							me.getForm().el.unmask();
						}
						
						clearInterval(a);
						me.cidade = false;
						me.endereco = false;
					}
				}, 300);
			});
			
			Ext.getCmp('action_fornecedores').setValue('EDITAR');
			me.hability(record.get('tipo_fornecedor'));
			
			Ext.getCmp('GridFornecedores_Anotacoes').setDisabled(false);
			Ext.getCmp('GridFornecedores_Contato').setDisabled(false);
			Ext.getCmp('GridFornecedores_Endereco').setDisabled(false);

			Ext.getCmp('cadastrado_por_fornecedores').setVisible(true);
			Ext.getCmp('alterado_por_fornecedores').setVisible(true);

			Ext.getCmp('GridFornecedores_Anotacoes').store.proxy.extraParams.cod_fornecedor = record.get('cod_fornecedor');
			Ext.getCmp('GridFornecedores_Contato').store.proxy.extraParams.cod_fornecedor = record.get('cod_fornecedor');
			Ext.getCmp('GridFornecedores_Endereco').store.proxy.extraParams.cod_fornecedor = record.get('cod_fornecedor');
			
			var data = me.application.dados[me.tabela.toLowerCase()];
			
			Ext.each(data, function(j){
				if(j.acao=='fornecedor_logs'){
					Ext.getCmp('GridFornecedor_Logs_Sistema').setDisabled(false);
					Ext.getCmp('GridFornecedor_Logs_Sistema').store.proxy.extraParams.tabela_cod = record.get('cod_fornecedor');
					Ext.getCmp('GridFornecedor_Logs_Sistema').store.proxy.extraParams.modulo = 'Fornecedores';
				}
			});
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'fornecedores', {
			action: 'DELETAR',
			id: record.get('cod_fornecedor')
		}, button, false);
	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid, 'error');
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();
			if(record.get('tipo_fornecedor')=='F'){
				var fornecedor = record.get('nome_completo');				
			}
			else{
				var fornecedor = record.get('razao_social');
			}
			Ext.Msg.confirm('Confirmar', 'Deseja Apagar o Fornecedor: '+fornecedor+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid, 'error');
			return true;
		}
	},

	getCurrentEmpresa: function(){
		var current = null;
		for(var i in empresa_sistemaAll){
			if(empresa_sistemaAll[i].id==Ext.util.Cookies.get('id_empresa_atual')){
				current = empresa_sistemaAll[i];
			}
		}
		return current;
	},

	add: function(button) {
		var me = this;
		window.App = me;
		me.cidade = false;
		me.getDesktopWindow('Fornecedores', 'Fornecedores', 'fornecedores.Edit', function(){
			me.hability(button.type);
			empresa = me.getCurrentEmpresa();
			Ext.getCmp('estado_fornecedores').setValue(empresa.estado);
			var a = setInterval(function(){
				if(empresa.cidade != null){
					if(me.cidade==true){
						var record = Ext.getCmp('cidade_fornecedores').store.findRecord('descricao', empresa.cidade);
						if(record){
							Ext.getCmp('cidade_fornecedores').setValue(record.get('id'));
						}
						me.cidade = null;
					}
				}else{me.cidade = null;}
				if(me.cidade == null){
					clearInterval(a);
				}
			}, 1000);

			setTimeout(function(){
				clearInterval(a);
			}, 7000);
			
			Ext.getCmp('GridFornecedores_Anotacoes').setDisabled(true);
			Ext.getCmp('GridFornecedores_Contato').setDisabled(true);
			Ext.getCmp('GridFornecedores_Endereco').setDisabled(true);
		});
	},

	hability: function(type){
		if(type=='J'){
			Ext.getCmp('fieldcontainer_jf').setVisible(true);

			Ext.getCmp('razao_social_fornecedores').setVisible(true);
			Ext.getCmp('nome_fantasia_fornecedores').setVisible(true);
			Ext.getCmp('ie_fornecedores').setVisible(true);

			Ext.getCmp('razao_social_fornecedores').setDisabled(false);
			Ext.getCmp('nome_fantasia_fornecedores').setDisabled(false);
			Ext.getCmp('cnpj_fornecedores').setDisabled(false);
			Ext.getCmp('im_fornecedores').setDisabled(false);
			Ext.getCmp('ie_fornecedores').setDisabled(false);
			Ext.getCmp('pessoa_contato_fornecedores').setDisabled(false);


			Ext.getCmp('fieldcontainer_ff').setVisible(false);
			Ext.getCmp('nome_completo_fornecedores').setVisible(false);
			Ext.getCmp('profissao_fornecedores').setVisible(false);
			Ext.getCmp('data_nascimento_fornecedores').setVisible(false);

			Ext.getCmp('nome_completo_fornecedores').setDisabled(true);
			Ext.getCmp('profissao_fornecedores').setDisabled(true);
			Ext.getCmp('data_nascimento_fornecedores').setDisabled(true);
			Ext.getCmp('cpf_fornecedores').setDisabled(true);
			Ext.getCmp('sexo_fornecedores').setDisabled(true);
			Ext.getCmp('identidade_fornecedores').setDisabled(true);

			Ext.getCmp('razao_social_fornecedores').focus(false, 1000);
			Ext.getCmp('tipo_fornecedor_fornecedores').setValue('J');
		}
		else{
			Ext.getCmp('fieldcontainer_jf').setVisible(false);
			Ext.getCmp('razao_social_fornecedores').setVisible(false);
			Ext.getCmp('nome_fantasia_fornecedores').setVisible(false);
			Ext.getCmp('ie_fornecedores').setVisible(false);

			Ext.getCmp('razao_social_fornecedores').setDisabled(true);
			Ext.getCmp('nome_fantasia_fornecedores').setDisabled(true);
			Ext.getCmp('cnpj_fornecedores').setDisabled(true);
			Ext.getCmp('im_fornecedores').setDisabled(true);
			Ext.getCmp('ie_fornecedores').setDisabled(true);
			Ext.getCmp('pessoa_contato_fornecedores').setDisabled(true);


			Ext.getCmp('fieldcontainer_ff').setVisible(true);
			Ext.getCmp('nome_completo_fornecedores').setVisible(true);
			Ext.getCmp('profissao_fornecedores').setVisible(true);
			Ext.getCmp('data_nascimento_fornecedores').setVisible(true);

			Ext.getCmp('nome_completo_fornecedores').setDisabled(false);
			Ext.getCmp('profissao_fornecedores').setDisabled(false);
			Ext.getCmp('data_nascimento_fornecedores').setDisabled(false);
			Ext.getCmp('cpf_fornecedores').setDisabled(false);
			Ext.getCmp('sexo_fornecedores').setDisabled(false);
			Ext.getCmp('identidade_fornecedores').setDisabled(false);

			Ext.getCmp('nome_completo_fornecedores').focus(false, 1000);
			Ext.getCmp('tipo_fornecedor_fornecedores').setValue('F');
		}
	},

	habilityFilter: function(combo){
		var me = this;
		if(!combo.store.getById(combo.getValue())){
			return true;
		}
		if(combo.getValue()=='J'){
			Ext.getCmp('fieldcontainer_filter_jf').setVisible(true);

			Ext.getCmp('razao_social_filter_fornecedores').setVisible(true);
			Ext.getCmp('nome_fantasia_filter_fornecedores').setVisible(true);
			Ext.getCmp('ie_filter_fornecedores').setVisible(true);

			Ext.getCmp('razao_social_filter_fornecedores').setDisabled(false);
			Ext.getCmp('nome_fantasia_filter_fornecedores').setDisabled(false);
			Ext.getCmp('cnpj_filter_fornecedores').setDisabled(false);
			Ext.getCmp('im_filter_fornecedores').setDisabled(false);
			Ext.getCmp('ie_filter_fornecedores').setDisabled(false);
			Ext.getCmp('pessoa_contato_filter_fornecedores').setDisabled(false);


			Ext.getCmp('fieldcontainer_filter_ff').setVisible(false);
			Ext.getCmp('nome_completo_filter_fornecedores').setVisible(false);
			Ext.getCmp('profissao_filter_fornecedores').setVisible(false);
			Ext.getCmp('data_nascimento_filter_fornecedores').setVisible(false);

			Ext.getCmp('nome_completo_filter_fornecedores').setDisabled(true);
			Ext.getCmp('profissao_filter_fornecedores').setDisabled(true);
			Ext.getCmp('data_nascimento_filter_fornecedores').setDisabled(true);
			Ext.getCmp('cpf_filter_fornecedores').setDisabled(true);
			Ext.getCmp('sexo_filter_fornecedores').setDisabled(true);
			Ext.getCmp('identidade_filter_fornecedores').setDisabled(true);
		}
		else{
			Ext.getCmp('fieldcontainer_filter_jf').setVisible(false);
			Ext.getCmp('razao_social_filter_fornecedores').setVisible(false);
			Ext.getCmp('nome_fantasia_filter_fornecedores').setVisible(false);
			Ext.getCmp('ie_filter_fornecedores').setVisible(false);

			Ext.getCmp('razao_social_filter_fornecedores').setDisabled(true);
			Ext.getCmp('nome_fantasia_filter_fornecedores').setDisabled(true);
			Ext.getCmp('cnpj_filter_fornecedores').setDisabled(true);
			Ext.getCmp('im_filter_fornecedores').setDisabled(true);
			Ext.getCmp('ie_filter_fornecedores').setDisabled(true);
			Ext.getCmp('pessoa_contato_filter_fornecedores').setDisabled(true);


			Ext.getCmp('fieldcontainer_filter_ff').setVisible(true);
			Ext.getCmp('nome_completo_filter_fornecedores').setVisible(true);
			Ext.getCmp('profissao_filter_fornecedores').setVisible(true);
			Ext.getCmp('data_nascimento_filter_fornecedores').setVisible(true);

			Ext.getCmp('nome_completo_filter_fornecedores').setDisabled(false);
			Ext.getCmp('profissao_filter_fornecedores').setDisabled(false);
			Ext.getCmp('data_nascimento_filter_fornecedores').setDisabled(false);
			Ext.getCmp('cpf_filter_fornecedores').setDisabled(false);
			Ext.getCmp('sexo_filter_fornecedores').setDisabled(false);
			Ext.getCmp('identidade_filter_fornecedores').setDisabled(false);
		}
	},

	winAddNoClose: true,

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(dados){

			if(Ext.getCmp('action_fornecedores').getValue()=='INSERIR'){
				Ext.getCmp('action_fornecedores').setValue('EDITAR');

				Ext.getCmp('GridFornecedores_Anotacoes').setDisabled(false);
				Ext.getCmp('GridFornecedores_Contato').setDisabled(false);
				Ext.getCmp('GridFornecedores_Endereco').setDisabled(false);
				Ext.getCmp('GridFornecedor_Logs_Sistema').setDisabled(false);
				
				Ext.getCmp('GridFornecedores_Anotacoes').store.proxy.extraParams.cod_fornecedor = dados.cod_fornecedor;
				Ext.getCmp('GridFornecedores_Contato').store.proxy.extraParams.cod_fornecedor = dados.cod_fornecedor;
				Ext.getCmp('GridFornecedores_Endereco').store.proxy.extraParams.cod_fornecedor = dados.cod_fornecedor;

				Ext.getCmp('GridFornecedores_Anotacoes').store.load();
				Ext.getCmp('GridFornecedores_Contato').store.load();
				Ext.getCmp('GridFornecedores_Endereco').store.load();
				Ext.getCmp('GridFornecedor_Logs_Sistema').store.load();

				Ext.getCmp('TabPanelCentralFornecedores').setActiveTab(2);
			}

		}, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('fornecedores', 'editar')==true){
			Ext.getCmp('button_edit_fornecedores').setDisabled(false);
		}

		if(this.existPermissao('fornecedores', 'deletar')==true){
			Ext.getCmp('button_del_fornecedores').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		this.getList().store.proxy.extraParams.query = Ext.getCmp('query_fornecedores').getValue();
		this.getList().store.load({
			start: 0,
			limit: 25
		});
	}

});