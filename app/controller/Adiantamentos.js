/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Adiantamentos', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Adiantamentos',
	started: true,

	refs: [
		{
			ref: 'list',
			selector: 'adiantamentoslist'
		},
		{
			ref: 'form',
			selector: 'addadiantamentoswin form'
		},
		{
			ref: 'list2',
			selector: 'addadiantamentosvincularvalorwin'
		},
		{
			ref: 'form2',
			selector: 'addadiantamentosvincularvalorwin form'
		},
		{
			ref: 'addWin2',
			selector: 'addadiantamentosvincularvalorwin'
		},
		{
			ref: 'list3',
			selector: 'addadiantamentosdesvincularvalorwin'
		},
		{
			ref: 'form3',
			selector: 'addadiantamentosdesvincularvalorwin form'
		},
		{
			ref: 'addWin3',
			selector: 'addadiantamentosdesvincularvalorwin'
		},
		{
			ref: 'filterBtn',
			selector: 'adiantamentoslist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteradiantamentoswin'
		},
		{
			ref: 'filterForm',
			selector: 'filteradiantamentoswin form'
		},
		{
			ref: 'addWin',
			selector: 'addadiantamentoswin'
		}
	],

	models: [
		'ModelComboLocal',
		'ModelAdiantamentos',
		'ModelAdiantamentosVinculo'
	],

	stores: [
		'StoreComboFormas_Pagto',
		'StoreComboEstornadoAdiantamentos',
		'StoreAdiantamentos',
		'StoreAdiantamentosVincular',
		'StoreAdiantamentosDetalharVinculo'
	],

	views: [
		'adiantamentos.List',
		'adiantamentos.Filtro',
		'adiantamentos.Edit',
		'adiantamentos.Vincular',
		'adiantamentos.VincularValor',
		'adiantamentos.DetalharVinculo',
		'adiantamentos.Desvinculo'
	],

	init: function(application) {
		this.control({
			'adiantamentoslist': { 
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				cellclick: this.clickCelula
			},
			'adiantamentos_vincularlist gridpanel': { 
				cellclick: this.clickCelulaVincular
			},
			'adiantamentos_vincularlist button[action=vincular_valor]': {
				click: this.vincularValor
			},
			'adiantamentos_detalhar_vinculoslist gridpanel': { 
				cellclick: this.clickCelulaDetalharAdiantamento
			},
			'adiantamentos_detalhar_vinculoslist button[action=desvincular]': {
				click: this.desvinculaAdiantamento
			},
			'addadiantamentosdesvincularvalorwin button[action=salvar]': {
				click: this.updateDesvinculo
			},
			'adiantamentoslist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'adiantamentoslist button[action=adicionar]': {
				click: this.add
			},
			'adiantamentoslist button[action=estornar]': {
				click: this.btEstorno
			},
			'adiantamentoslist button[action=vincular]': {
				click: this.btVincular
			},
			'adiantamentoslist button[action=detalhar_vinculo]': {
				click: this.btDetalharVinculo
			},
			'adiantamentoslist button[action=imprimir]': {
				click: this.imprimirAdiantamento
			},
			'addadiantamentoswin button[action=salvar]': {
				click: this.update
			},
			'addadiantamentoswin button[action=resetar]': {
				click: this.reset
			},
			'addadiantamentosvincularvalorwin form textfield[id=valor_adiantamentos_vincular]': {
				change: this.validaVinculo
			},
			'addadiantamentosvincularvalorwin button[action=salvar]': {
				click: this.updateVinculo
			},
			'addadiantamentoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addadiantamentoswin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addadiantamentoswin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteradiantamentoswin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteradiantamentoswin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteradiantamentoswin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteradiantamentoswin': {
				show: this.filterSetFields
			},
			'addadiantamentoswin form combobox[id=forma_pagto_adiantamentos]': {
				change: this.changeQtdeParcelas
			}
		});
	},

	changeQtdeParcelas: function(combo, value){
		var me = this;
		var record = combo.store.getById(value);

		if (value!=null && !isNaN(parseInt(value)))	{
			var max_cartao = parseInt(record.raw.max_cartao);
			var comboNroParcelas = Ext.getCmp('nro_parcelas_adiantamentos');

			if (!isNaN(max_cartao)) {
				comboNroParcelas.setDisabled(false);
				comboNroParcelas.setVisible(true);
				comboNroParcelas.setValue(1);
				comboNroParcelas.setMaxValue(max_cartao);
			}
			else {
				comboNroParcelas.reset();
				comboNroParcelas.setDisabled(true);
				comboNroParcelas.setVisible(false);
			}
		}
	},

	validaVinculo: function (e, text, o) {
		text = parseFloat(text);
		var saldo_disponivel = parseFloat(Ext.getCmp('valor_disponivel_adiantamentos_vincular').getValue());
		var saldo_a_receber = parseFloat(Ext.getCmp('saldo_a_receber_adiantamentos_vincular').getValue());

		if (text == 0) {
			Ext.getCmp('button_salvar_adiantamentos_vincular').setDisabled(true);
		}
		else {
			if (text > saldo_a_receber) {
				Ext.getCmp('button_salvar_adiantamentos_vincular').setDisabled(true);
			}
			else {
				if (text > saldo_disponivel) {
					Ext.getCmp('button_salvar_adiantamentos_vincular').setDisabled(true);
				} else {
					Ext.getCmp('button_salvar_adiantamentos_vincular').setDisabled(false);
				}
			}
		}
	},

	vincularValor: function(button) {
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			var saldo_disponivel = grid.store.proxy.extraParams.saldo_adiantamento;
			var id_adiantamento = grid.store.proxy.extraParams.id_adiantamento;

			me.getDesktopWindow('List-Adiantamentos_Vincular_Valor', 'Adiantamentos', 'adiantamentos.VincularValor', function(controller){
				Ext.getCmp('valor_disponivel_adiantamentos_vincular').setValue(saldo_disponivel);
				Ext.getCmp('saldo_a_receber_adiantamentos_vincular').setValue(record.get('saldo_a_receber'));
				Ext.getCmp('id_venda_adiantamentos_vincular').setValue(record.get('id_orcamento'));
				Ext.getCmp('id_adiantamento_adiantamentos_vincular').setValue(id_adiantamento);
			});
		}
		else{
			info(me.titleErro, 'Selecione um Ítem para Vincular');
			return true;
		}
	},

	estornaAdiantamento: function(grid, record, button, motivo) {
		var me = this;
		me.deleteAjax(grid, 'adiantamentos', {
			action: 'DELETAR',
			id: record.get('id'),
			motivo_cancelamento: motivo,
			categoria: record.get('categoria_pagamento')
		}, button, function(){
			grid.store.load({
				callback: function(){
					var grid1 = Ext.getCmp('GridAdiantamentos_Vincular');
					var grid2 = Ext.getCmp('GridVendas');
					var grid3 = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos');
					var grid4 = Ext.getCmp('GridCaixa_Itens');
					var grid5 = Ext.getCmp('GridVendas_Pagamentos');

					setTimeout(function() {
						if(grid1 && grid1.getEl()){
							grid1.store.load();
						}

						if(grid2 && grid2.getEl()){
							grid2.store.load();
						}

						if(grid3 && grid3.getEl()){
							grid3.store.load();
						}

						if(grid4 && grid4.getEl()){
							grid4.store.load();
						}

						if(grid5 && grid5.getEl()){
							grid5.store.load();
						}
					}, 500);
				}
			})
		});
	},

	btEstorno: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.prompt('Confirmar', 'Deseja Estornar o Adiantamento de <b>R$: '+converteFloatMoeda(record.get('valor'))+'?</b>', function(btnText, sInput){
				if (btnText === 'ok') {
					if (sInput == '') {
						Ext.Msg.alert("ERRO", "DIGITE UM MOTIVO ");
					}
					else {
						me.estornaAdiantamento(me.getList(), record, button, sInput);
					}
				}
			}, this);
		}
		else{
			info(this.titleErro, 'Selecione um Ítem para Estornar');
			return true;
		}
	},

	btVincular: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('List-Adiantamentos_Vincular', 'Adiantamentos', 'adiantamentos.Vincular', function(controller){
				Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.action = 'LISTA_VENDAS_EM_ABERTO';
				Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.orcamento_venda = 'S';
				Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.saldo_adiantamento = record.get('saldo');
				Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.cliente = record.get('cliente');
				Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.id_adiantamento = record.get('id');
				Ext.getCmp('GridAdiantamentos_Vincular').store.load();
			});
		}
		else{
			info(me.titleErro, 'Selecione um Ítem para Vincular');
			return true;
		}
	},

	btDetalharVinculo: function(button){
		var me = this;
		grid = button.up('gridpanel');
		me.gridPanel = grid;
		if (grid.selModel.hasSelection()) {
			var record = grid.getSelectionModel().getLastSelected();
			me.getDesktopWindow('List-Adiantamentos_Detalhamento_Vinculos', 'Adiantamentos', 'adiantamentos.DetalharVinculo', function(controller){
				Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos').store.proxy.extraParams.cod_adiantamento = record.get('id');
				Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos').store.load();
			});
		}
		else{
			info(me.titleErro, 'Selecione um Ítem para Detalhar');
			return true;
		}
	},

	desvinculaAdiantamento: function(button) {
		var me = this;
		var grid = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos');
		var record = grid.getSelectionModel().getLastSelected();

		me.getDesktopWindow('Desvinculo-Adiantamento', 'Adiantamentos', 'adiantamentos.Desvinculo', function(){
			Ext.getCmp('Desvinculo-Adiantamento').setTitle('Desvinculando o Valor de R$ '+converteFloatMoeda(record.get('valor_vinculado')));
			me.getValuesForm(me.getForm3(), me.getAddWin3(), record.get('id'), 'server/modulos/adiantamentos_uso/list.php');
		});
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Adiantamentos', 'Adiantamentos', 'adiantamentos.Edit', function(){
			Ext.getCmp('cliente_adiantamentos').setValue(me.getList().store.proxy.extraParams.cod_cliente);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, function(){
			me.getList().store.load({
				callback: function(){
					var grid1 = Ext.getCmp('GridCaixa_Itens');

					setTimeout(function() {
						if(grid1 && grid1.getEl()){
							grid1.store.load();
						}
					}, 500);
				}
			})
		}, false);
	},

	updateDesvinculo: function(button) {
		var me = this;
		var grid = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos');
		Ext.Msg.confirm('Confirmar', 'Confirma o Desvínculo do Valor ao Pedido?', function(btn){
			if(btn=='yes'){
				me.saveForm(grid, me.getForm3(), me.getAddWin3(), button, function(){
					grid.store.load({
						callback: function(){
							Ext.getCmp('button_desvincular_detalhamento_adiantamento').setDisabled(true)

							var grid1 = Ext.getCmp('GridAdiantamentos_Vincular');
							var grid2 = Ext.getCmp('GridVendas');
							var grid3 = Ext.getCmp('GridAdiantamentos');
							var grid4 = Ext.getCmp('GridCaixa_Itens');
							var grid5 = Ext.getCmp('GridVendas_Pagamentos');

							setTimeout(function() {
								if(grid1 && grid1.getEl()){
									grid1.store.load();
								}

								if(grid2 && grid2.getEl()){
									grid2.store.load();
								}

								if(grid3 && grid3.getEl()){
									grid3.store.load();
								}

								if(grid4 && grid4.getEl()){
									grid4.store.load();
								}

								if(grid5 && grid5.getEl()){
									grid5.store.load();
								}
							}, 500);
						}
					})
				}, false);
			}
		});
	},

	updateVinculo: function(button) {
		var me = this;
		var grid = Ext.getCmp('GridAdiantamentos_Vincular');
		var valor_vinculado = parseFloat(Ext.getCmp('valor_adiantamentos_vincular').getValue());
		var saldo_a_receber = parseFloat(Ext.getCmp('saldo_a_receber_adiantamentos_vincular').getValue());
		var valor_disponivel = parseFloat(Ext.getCmp('valor_disponivel_adiantamentos_vincular').getValue());

		if (valor_vinculado > saldo_a_receber) {
			Ext.getCmp('button_vincular_adiantamento_adiantamentos').setDisabled(false);
			info(this.titleErro, 'O valor à vincular não pode ser maior que o saldo à receber');
			return true;
		}
		else {
			Ext.Msg.confirm('Confirmar', 'Confirma o Vínculo do Valor ao Pedido?', function(btn){
				if(btn=='yes'){
					if (valor_vinculado == saldo_a_receber) {
						Ext.getCmp('button_vincular_adiantamento_adiantamentos').setDisabled(true);
					}
					else {
						Ext.getCmp('button_vincular_adiantamento_adiantamentos').setDisabled(false);
					}

					Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.saldo_a_receber = parseFloat(saldo_a_receber - valor_vinculado).toFixed(2);
					Ext.getCmp('GridAdiantamentos_Vincular').store.proxy.extraParams.saldo_adiantamento = parseFloat(valor_disponivel - valor_vinculado).toFixed(2);
					me.saveForm(grid, me.getForm2(), me.getAddWin2(), button, function(){
						grid.store.load({
							callback: function(){
								var grid1 = Ext.getCmp('GridAdiantamentos');
								var grid2 = Ext.getCmp('GridVendas');
								var grid3 = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos');
								var grid4 = Ext.getCmp('GridCaixa_Itens');
								var grid5 = Ext.getCmp('GridVendas_Pagamentos');

								setTimeout(function() {
									if(grid1 && grid1.getEl()){
										grid1.store.load();
									}

									if(grid2 && grid2.getEl()){
										grid2.store.load();
									}

									if(grid3 && grid3.getEl()){
										grid3.store.load();
									}

									if(grid4 && grid4.getEl()){
										grid4.store.load();
									}

									if(grid5 && grid5.getEl()){
										grid5.store.load();
									}
								}, 1000);
							}
						})
					}, false);
				}
			});
		}
	},

	clickCelulaVincular: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts){
		var me = this;
		var total_selecionado = Ext.getCmp('GridAdiantamentos_Vincular').getSelectionModel().getCount();

		if (total_selecionado > 0) {
			Ext.getCmp('button_vincular_adiantamento_adiantamentos').setDisabled(false);
		}
		else {
			Ext.getCmp('button_vincular_adiantamento_adiantamentos').setDisabled(true);
		}
	},

	clickCelulaDetalharAdiantamento: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts){
		var me = this;
		var total_selecionado = Ext.getCmp('GridAdiantamentos_Detalhar_Vinculos').getSelectionModel().getCount();

		if (total_selecionado > 0) {
			if(record.get('cancelado') == 'S') {
				Ext.getCmp('button_desvincular_detalhamento_adiantamento').setDisabled(true);
			}
			else {
				if(record.get('venda_finalizada') == 'S') {
					Ext.getCmp('button_desvincular_detalhamento_adiantamento').setDisabled(true);
					info(me.titleErro, 'A Venda ao qual esse Vínculo está associada, já foi finalizada.');
					return true;
				}
				else {
					Ext.getCmp('button_desvincular_detalhamento_adiantamento').setDisabled(false);
				}
			}
		}
		else {
			Ext.getCmp('button_desvincular_detalhamento_adiantamento').setDisabled(true);
		}
	},

	clickCelula: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		var me = this;
		if(record.get('estornado') == 'S') {
			Ext.getCmp('button_estorno_adiantamentos').setDisabled(true);
			Ext.getCmp('button_imprimir_adiantamentos').setDisabled(true);
			Ext.getCmp('button_vincular_adiantamentos').setDisabled(true);
			Ext.getCmp('button_detalhar_vinculo_adiantamentos').setDisabled(true);
		}
		else {
			if (record.get('valor_utilizado') == 0) {
				Ext.getCmp('button_estorno_adiantamentos').setDisabled(false);
				Ext.getCmp('button_imprimir_adiantamentos').setDisabled(false);
				Ext.getCmp('button_vincular_adiantamentos').setDisabled(false);
				Ext.getCmp('button_detalhar_vinculo_adiantamentos').setDisabled(false);
			}
			else {
				Ext.getCmp('button_estorno_adiantamentos').setDisabled(true);
				Ext.getCmp('button_imprimir_adiantamentos').setDisabled(false);
				Ext.getCmp('button_vincular_adiantamentos').setDisabled(false);
				Ext.getCmp('button_detalhar_vinculo_adiantamentos').setDisabled(false);

				if (record.get('saldo') == 0) {
					Ext.getCmp('button_vincular_adiantamentos').setDisabled(true);
				}
			}
		}
	},

	imprimirAdiantamento: function(button){
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			window.open('server/modulos/adiantamentos/imprime.php?id='+record.get('id'));
		}
		else{
			info(this.titleErro, this.detalharErroGrid);
			return true;
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.adiantamentos.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}
});