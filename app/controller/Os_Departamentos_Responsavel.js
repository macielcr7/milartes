/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.controller.Os_Departamentos_Responsavel', {
	extend: 'Ext.app.Controller',
	mixins: {
		controls: 'ShSolutions.controller.Util'
	},

	tabela: 'Os_Departamentos_Responsavel',
	
	refs: [
		{
			ref: 'list',
			selector: 'os_departamentos_responsavellist gridpanel'
		},
		{
			ref: 'form',
			selector: 'addos_departamentos_responsavelwin form'
		},
		{
			ref: 'filterBtn',
			selector: 'os_departamentos_responsavellist button[action=filtrar]'
		},
		{
			ref: 'filterWin',
			selector: 'filteros_departamentos_responsavelwin'
		},
		{
			ref: 'filterForm',
			selector: 'filteros_departamentos_responsavelwin form'
		},
		{
			ref: 'addWin',
			selector: 'addos_departamentos_responsavelwin'
		}
	],
	
	models: [
		'ModelComboLocal',
		'ModelOs_Departamentos_Responsavel'
	],
	stores: [
		'StoreComboUsuariosDeptoProdutosResponsavel',
		'StoreComboOs_Categorias',
		'StoreComboUsuarios',
		'StoreComboEmpresasSistemaResponsavelDepto',
		'StoreComboUsuarios',
		'StoreOs_Departamentos_Responsavel',
		'StoreComboProdutos_CategoriasOs'
	],
	
	views: [
		'os_departamentos_responsavel.List',
		'os_departamentos_responsavel.Filtro',
		'os_departamentos_responsavel.Edit'
	],

	init: function(application) {
		this.control({
			'os_departamentos_responsavellist gridpanel': {
				afterrender: this.getPermissoes,
				render: this.gridLoad,
				select: this.RegistroSelecionado
			},
			'os_departamentos_responsavellist button[action=filtrar]': {
				click: this.btStoreLoadFielter
			},
			'os_departamentos_responsavellist button[action=adicionar]': {
				click: this.add
			},
			'os_departamentos_responsavellist button[action=editar]': {
				click: this.btedit
			},
			'os_departamentos_responsavellist button[action=deletar]': {
				click: this.btdel
			},
			'addos_departamentos_responsavelwin button[action=salvar]': {
				click: this.update
			},
			'addos_departamentos_responsavelwin button[action=resetar]': {
				click: this.reset
			},
			'addos_departamentos_responsavelwin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'addos_departamentos_responsavelwin form fieldcontainer button[action=reset_combo]': {
				click: this.resetCombo
			},
			'addos_departamentos_responsavelwin form fieldcontainer button[action=add_win]': {
				click: this.getAddWindow
			},
			'filteros_departamentos_responsavelwin form fieldcontainer combobox': {
				change: this.enableButton,
				render: this.comboLoad
			},
			'filteros_departamentos_responsavelwin button[action=resetar_filtro]': {
				click: this.resetFielter
			},
			'filteros_departamentos_responsavelwin button[action=filtrar_busca]': {
				click: this.setFielter
			},
			'filteros_departamentos_responsavelwin': {
				show: this.filterSetFields
			}
		});
	},
	
	edit: function(grid, record) {
		var me = this;
		me.getDesktopWindow('AddOs_Departamentos_ResponsavelWin', 'Os_Departamentos_Responsavel', 'os_departamentos_responsavel.Edit', function(){
			me.getAddWin().setTitle('Alteração: Responsável Pelo Departamento em OS');
			me.getValuesForm(me.getForm(), me.getAddWin(), record.get('id'), 'server/modulos/os_departamentos_responsavel/list.php', function(data){
				
				Ext.getCmp('id_depto_os_departamentos_responsavel').setValue(parseInt(data.id_depto));
				var id_empresa = [data.id_empresa];
				Ext.getCmp('id_empresa_os_departamentos_responsavel').setValue(id_empresa);
			});
			Ext.getCmp('action_os_departamentos_responsavel').setValue('EDITAR');
		});
	},

	del: function(grid, record, button) {
		var me = this;
		me.deleteAjax(grid, 'os_departamentos_responsavel', {
			action: 'DELETAR',
			id: record.get('id')
		}, button, false);

	},

	btedit: function(button) {
		if (this.getList().selModel.hasSelection()) {
			var record = this.getList().getSelectionModel().getLastSelected();
			this.edit(this.getList(), record);
		}
		else{
			info(this.titleErro, this.editErroGrid);
			return true;
		}
	},

	btdel: function(button) {
		var me = this;
		if (me.getList().selModel.hasSelection()) {
			var record = me.getList().getSelectionModel().getLastSelected();

			Ext.Msg.confirm('Confirmar', 'Deseja deletar: '+record.get('id')+'?', function(btn){
				if(btn=='yes'){
					me.del(me.getList(), record, button);
				}
			});
		}
		else{
			info(this.titleErro, this.delErroGrid);
			return true;
		}
	},

	add: function(button) {
		var me = this;
		me.getDesktopWindow('Os_Departamentos_Responsavel', 'Os_Departamentos_Responsavel', 'os_departamentos_responsavel.Edit', function(){
			var codigo = me.getList().store.proxy.extraParams.id_depto;
			Ext.getCmp('id_depto_os_departamentos_responsavel').setValue(codigo);
		});
	},

	update: function(button) {
		var me = this;
		me.saveForm(me.getList(), me.getForm(), me.getAddWin(), button, false, false);
	},

	RegistroSelecionado: function(){
		if(this.existPermissao('os_departamentos_responsavel', 'editar')==true){
			Ext.getCmp('button_edit_os_departamentos_responsavel').setDisabled(false);
		}

		if(this.existPermissao('os_departamentos_responsavel', 'deletar')==true){
			Ext.getCmp('button_del_os_departamentos_responsavel').setDisabled(false);
		}
	},

	btStoreLoadFielter: function(button){
		var win = this.getFilterWin();
		if(!win) var win = Ext.create('ShSolutions.view.os_departamentos_responsavel.Filtro', {
			animateTarget: button.getEl()
		});
		win.show();
	}

});
