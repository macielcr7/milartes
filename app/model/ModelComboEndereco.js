/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelComboEndereco', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'tipo',
			type: 'string'
		},
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'ponto_referencia',
			type: 'string'
		}
	]
});