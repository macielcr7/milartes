/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelOrcamentos_Observacoes', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'orcamento_id',
			type: 'int'
		},
		{
			name: 'ambiente_id',
			type: 'int'
		}, 
		{
			name: 'nro_item',
			type: 'int'
		},
		{
			name: 'descricao',   
			type: 'string'
		},
		{
			name: 'tipo',   
			type: 'string'
		}
	]
});