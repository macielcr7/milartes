/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProdutos_Cores', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id_produtos_cores',								 
			type: 'int'
		},				
		{
			name: 'id_produto',								 
			type: 'int'
		},				
		{
			name: 'descricao',								 
			type: 'string'
		},				
		{
			name: 'id_cor',								 
			type: 'int'
		}				
    ]
});