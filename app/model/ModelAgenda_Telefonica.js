/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAgenda_Telefonica', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_contato',
			type: 'int'
		},
		{
			name: 'tipo_contato',
			type: 'string'
		},
		{
			name: 'nome_completo',
			type: 'string'
		},
		{
			name: 'razao_social',
			type: 'string'
		},
		{
			name: 'nome_fantasia',
			type: 'string'
		},
		{
			name: 'pessoa_contato',
			type: 'string'
		},
		{
			name: 'sexo',
			type: 'string'
		},
		{
			name: 'profissao',
			type: 'string'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'loc_nome',
			type: 'string'
		},
		{
			name: 'cidade',
			type: 'string'
		},
		{
			name: 'bairro_nome',
			type: 'string'
		},
		{
			name: 'bairro',
			type: 'string'
		},
		{
			name: 'fones', 
			type: 'string'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'nro_end',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'ponto_ref',
			type: 'string'
		},
		{
			name: 'cx_postal',
			type: 'string'
		},	
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'string'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'string'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'observacoes',
			type: 'string'
		}
	]
});