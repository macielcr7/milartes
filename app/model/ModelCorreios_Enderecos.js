/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCorreios_Enderecos', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id',								 
			type: 'int'
		},				
		{
			name: 'descricao',								 
			type: 'string'
		},				
		{
			name: 'uf_sigla',								 
			type: 'string'
		},				
		{
			name: 'loc_nome',								 
			type: 'string'
		},				
		{
			name: 'localidade_id',								 
			type: 'int'
		},				
		{
			name: 'tipo',								 
			type: 'string'
		},				
		{
			name: 'nome',								 
			type: 'string'
		},				
		{
			name: 'bairro_nome',								 
			type: 'string'
		},				
		{
			name: 'bairro_id_inicial',								 
			type: 'int'
		},				
		{
			name: 'bairro_nome',								 
			type: 'string'
		},				
		{
			name: 'bairro_id_final',								 
			type: 'int'
		},				
		{
			name: 'cep',								 
			type: 'string'
		},				
		{
			name: 'complemento',								 
			type: 'string'
		},				
		{
			name: 'loteamento',								 
			type: 'string'
		},				
		{
			name: 'ponto_referencia',								 
			type: 'string'
		},				
		{
			name: 'ativo',								 
			type: 'string'
		},				
		{
			name: 'cadastrado_por',								 
			type: 'string'
		},				
		{
			name: 'data_cadastro_time',			
			dateFormat: 'H:i:s',					 
			type: 'date'
		},				
		{
			name: 'data_cadastro_date',			
			dateFormat: 'Y-m-d',					 
			type: 'date'
		},				
		{
			name: 'data_cadastro',			
			dateFormat: 'Y-m-d H:i:s',					 
			type: 'date'
		},				
		{
			name: 'alterado_por',								 
			type: 'int'
		}				
    ]
});