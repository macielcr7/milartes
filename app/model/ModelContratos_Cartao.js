/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelContratos_Cartao', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'conta_bancaria',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'cod_empresa',
			type: 'int'
		},
		{
			name: 'nome_empresa',
			type: 'string'
		},
/*
		{
			name: 'agencia',
			type: 'string'
		},
		{
			name: 'conta_corrente',
			type: 'string'
		},
*/
		{
			name: 'banco_agencia_conta',
			type: 'string'
		},
		{
			name: 'tipo',
			type: 'string'
		},
		{
			name: 'nro_max_parcelas',
			type: 'int'
		},
		{
			name: 'taxa1',
			type: 'string'
		},
		{
			name: 'taxa2',
			type: 'string'
		},
		{
			name: 'taxa3',
			type: 'string'
		},
		{
			name: 'taxa4',
			type: 'string'
		},
		{
			name: 'taxa5',
			type: 'string'
		},
		{
			name: 'taxa6',
			type: 'string'
		},
		{
			name: 'taxa7',
			type: 'string'
		},
		{
			name: 'taxa8',
			type: 'string'
		},
		{
			name: 'taxa9',
			type: 'string'
		},
		{
			name: 'taxa10',
			type: 'string'
		},
		{
			name: 'taxa11',
			type: 'string'
		},
		{
			name: 'taxa12',
			type: 'string'
		},
		{
			name: 'taxa13',
			type: 'string'
		},
		{
			name: 'taxa14',
			type: 'string'
		},
		{
			name: 'taxa15',
			type: 'string'
		},
		{
			name: 'taxa16',
			type: 'string'
		},
		{
			name: 'taxa17',
			type: 'string'
		},
		{
			name: 'taxa18',
			type: 'string'
		},
		{
			name: 'taxa19',
			type: 'string'
		},
		{
			name: 'taxa20',
			type: 'string'
		},
		{
			name: 'taxa21',
			type: 'string'
		},
		{
			name: 'taxa22',
			type: 'string'
		},
		{
			name: 'taxa23',
			type: 'string'
		},
		{
			name: 'taxa24',
			type: 'string'
		},
		{
			name: 'dias_credito_parc_vista',
			type: 'int'
		},
		{
			name: 'dias_credito_parc_estab',
			type: 'int'
		},
		{
			name: 'tipo_calculo',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		}
	]
});