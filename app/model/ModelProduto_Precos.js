/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProduto_Precos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_produto_precos',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'id_categoria_precos',
			type: 'int'
		},
		{
			name: 'descricao_completa',
			type: 'string'
		},
		{
			name: 'id_produto',
			type: 'int'
		},
		{
			name: 'valor',
			type: 'float'
		},
		{
			name: 'margem_lucro',
			type: 'string'
		},
		{
			name: 'total_custo2',
			type: 'string'
		}
	]
});