/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelRecibos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'recebido_de_nome',
			type: 'string'
		},
		{
			name: 'recebido_de_docto',
			type: 'string'
		},
		{
			name: 'valor_recibo',
			type: 'string'
		},
		{
			name: 'empresa_nome',
			type: 'string'
		},
		{
			name: 'data_recibo',
			type: 'string'
		},
		{
			name: 'referente',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		},
		{
			name: 'docto',
			type: 'string'
		}
	]
});