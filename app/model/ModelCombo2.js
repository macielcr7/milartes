/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCombo2', {
	extend: 'Ext.data.Model',

	internalId: 'id',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'descricao_list',
			type: 'string'
		},
		{
			name: 'cod_convenio',
			type: 'int'
		},
		{
			name: 'categoria',
			type: 'int'
		}
	]
});