/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProdutos_Orcamentos_Rodape', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id_produtos_orcamentos_rodape', 
			type: 'int'
		},
		{
			name: 'id_produto', 
			type: 'int'
		},
		{
			name: 'id_orcamento', 
			type: 'int'
		},
		{
			name: 'id_cor', 
			type: 'int'
		},
		{
			name: 'cor', 
			type: 'string'
		},
		{
			name: 'possui_ambiente', 
			type: 'string'
		},
		{
			name: 'css_cor', 
			type: 'string'
		},
		{
			name: 'produto', 
			type: 'string'
		},
		{
			name: 'tipo_produto', 
			type: 'string'
		},
		{
			name: 'quantidade', 
			type: 'float'
		},
		{
			name: 'qtd_metros_porcento', 
			type: 'float'
		},
		{
			name: 'qtd_barras', 
			type: 'int'
		},
		{
			name: 'total_area', 
			type: 'float'
		},
		{
			name: 'valor_unitario',
			type: 'float'
		},
		{
			name: 'desconto_real',
			type: 'float'
		},
		{
			name: 'valor_bruto',
			type: 'float'
		},
		{
			name: 'valor_total',
			type: 'float'
		},
		{
			name: 'sigla_um',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'cidade',
			type: 'string'
		},
		{
			name: 'bairro_nome',
			type: 'string'
		},
		{
			name: 'bairro',
			type: 'int'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'nro_end',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'ponto_ref',
			type: 'string'
		},
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'item_nro',
			type: 'int'
		},
		{
			name: 'total_de_itens',
			type: 'int'
		},
		{
			name: 'id_os',
			type: 'int'
		}
	]
});