/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.model.ModelFeriados', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_feriado',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'data_feriado',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		}
	]
});