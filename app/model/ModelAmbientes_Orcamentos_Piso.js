/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAmbientes_Orcamentos_Piso', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_ambientes_orcamentos_piso',
			type: 'int'
		},
		{
			name: 'id_produtos_orcamentos_piso',
			type: 'int'
		},
		{
			name: 'ordem',
			type: 'int'
		},
		{
			name: 'id_ambiente_pai',
			type: 'int'
		},
		{
			name: 'desc_pai',
			type: 'string'
		},
		{
			name: 'sn_l',
			type: 'string'
		},
		{
			name: 'tipo_acessorio',
			type: 'string'
		},
		{
			name: 'ambiente',
			type: 'string'
		},
		{
			name: 'descricao_pecas',
			type: 'string'
		},
		{
			name: 'qtd_pecas',
			type: 'int'
		},
		{
			name: 'qtd_metros',
			type: 'float'
		},
		{
			name: 'comprimento',
			type: 'float'
		},
		{
			name: 'largura',
			type: 'float'
		},
		{
			name: 'comprimento_real',
			type: 'float'
		},
		{
			name: 'largura_real',
			type: 'float'
		},
		{
			name: 'area',
			type: 'float'
		},
		{
			name: 'observacoes',
			type: 'string'
		}
	]
});