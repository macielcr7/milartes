/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAgenda_Telefonica_Contato', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'controle',
			type: 'int'
		},
		{
			name: 'cod_cliente',
			type: 'int'
		},
		{
			name: 'tipo_contato',
			type: 'string'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'observacao',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'alterado_por',
			type: 'int'
		}
	]
});