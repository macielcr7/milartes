/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelUsuarios_Empresas_Sistema', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'controle',								 
			type: 'int'
		},
		{
			name: 'id_empresa',								 
			type: 'int'
		},			
		{
			name: 'nome',								 
			type: 'string'
		},				
		{
			name: 'id_usuario',								 
			type: 'int'
		},				
		{
			name: 'razao_social',								 
			type: 'string'
		},				
		{
			name: 'id_empresa',								 
			type: 'int'
		},				
		{
			name: 'cadastrado_por',								 
			type: 'int'
		},				
		{
			name: 'data_cadastro_time',			
			dateFormat: 'H:i:s',					 
			type: 'date'
		},				
		{
			name: 'data_cadastro_date',			
			dateFormat: 'Y-m-d',					 
			type: 'date'
		},				
		{
			name: 'data_cadastro',			
			dateFormat: 'Y-m-d H:i:s',					 
			type: 'date'
		},				
		{
			name: 'alterado_por',								 
			type: 'int'
		}				
    ]
});