/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelEmpresas_Sistema', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_empresa',
			type: 'int'
		},
		{
			name: 'razao_social',
			type: 'string'
		},
		{
			name: 'nome_fantasia',
			type: 'string'
		},
		{
			name: 'cnpj',
			type: 'string'
		},
		{
			name: 'pessoa_contato',
			type: 'string'
		},
		{
			name: 'ramo_atividade',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'num_end',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'bairro',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'cidade',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'email',
			type: 'string'
		},
		{
			name: 'site',
			type: 'string'
		},
		{
			name: 'fone1',
			type: 'string'
		},
		{
			name: 'fone2',
			type: 'string'
		},
		{
			name: 'fone3',
			type: 'string'
		},
		{
			name: 'fone4',
			type: 'string'
		},
		{
			name: 'observacoes',
			type: 'auto'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		}
	]
});