/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelVendas_Custos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_venda',
			type: 'int'
		},
		{
			name: 'cod_item_venda',
			type: 'int'
		},
		{
			name: 'cod_produto',
			type: 'int'
		},
		{
			name: 'cod_cor',
			type: 'int'
		},
		{
			name: 'cor',
			type: 'string'
		},
		{
			name: 'css_cor', 
			type: 'string'
		},
		{
			name: 'quantidade',
			type: 'float'
		},
		{
			name: 'valor_unitario',
			type: 'float'
		},
		{
			name: 'acrescimo_produto',
			type: 'float'
		},
		{
			name: 'valor_total',
			type: 'float'
		},
		{
			name: 'cod_instalador',
			type: 'int'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'valor_liquido_venda',
			type: 'float'
		},
		{
			name: 'sigla_um',
			type: 'string'
		},
		{
			name: 'produto',
			type: 'string'
		},
		{
			name: 'observacoes', 
			type: 'string'
		},
		{
			name: 'venda_data_instalacao_inicio', 
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'venda_data_instalacao_fim', 
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cod_instalador',
			type: 'int'
		}
	]
});