/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCaixa_Itens', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'caixa_nro',
			type: 'int'
		},
		{
			name: 'categoria',
			type: 'int'
		},
		{
			name: 'id_item',
			type: 'int'
		},
		{
			name: 'cod_venda',
			type: 'int'
		},
		{
			name: 'nro_parcelas_cartao',
			type: 'int'
		},
		{
			name: 'data_hora_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_hora_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_hora',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'tipo_lancamento',
			type: 'int'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'id_tipo',
			type: 'int'
		},
		{
			name: 'forma_pagto',
			type: 'int'
		},
		{
			name: 'valor',
			type: 'float'
		},
		{
			name: 'valor_soma',
			type: 'float'
		},
		{
			name: 'cancelado',
			type: 'string'
		},
		{
			name: 'data_cancelamento_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cancelamento_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cancelamento',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cancelado_por',
			type: 'int'
		},
		{
			name: 'tipo_venda',
			type: 'string'
		},
		{
			name: 'cod_cliente',
			type: 'int'
		},
		{
			name: 'observacoes',
			type: 'string'
		},
		{
			name: 'forma_pagto_descricao',
			type: 'string'
		},
		{
			name: 'nome_cliente',
			type: 'string'
		},
		{
			name: 'cancelado_por_nome',
			type: 'string'
		}
	]
});