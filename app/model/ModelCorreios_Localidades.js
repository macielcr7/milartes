/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCorreios_Localidades', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'uf_sigla',
			type: 'string'
		},
		{
			name: 'loc_nome_abreviado',
			type: 'string'
		},
		{
			name: 'loc_nome',
			type: 'string'
		},
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'situacao_localidade',
			type: 'int'
		},
		{
			name: 'loc_tipo',
			type: 'string'
		},
		{
			name: 'subordinacao_id',
			type: 'int'
		},
		{
			name: 'cod_ibge',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',	
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'alterado_por',
			type: 'int'
		}
    ]
});