/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCaixas', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cx_nro',
			type: 'int'
		},
		{
			name: 'data_abertura_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_abertura_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_abertura',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'nome_user_abertura',
			type: 'string'
		},
		{
			name: 'nome_user_fechamento',
			type: 'string'
		},
		{
			name: 'usuario_abertura',
			type: 'int'
		},
		{
			name: 'saldo_inicial',
			type: 'float'
		},
		{
			name: 'obs_saldo_inicial',
			type: 'string'
		},
		{
			name: 'data_fechamento_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_fechamento_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_fechamento',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'usuario_fechamento',
			type: 'int'
		},
		{
			name: 'conferencia_saldo_final',
			type: 'float'
		},
		{
			name: 'obs_saldo_final',
			type: 'string'
		},
		{
			name: 'situacao',
			type: 'string'
		}
	]
});