/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelItens_Orcamento', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_itens_orcamento',
			type: 'int'
		},
		{
			name: 'id_produto',
			type: 'int'
		},
		{
			name: 'item_nro',
			type: 'int'
		},
		{
			name: 'id_orcamento',
			type: 'int'
		},
		{
			name: 'produto',
			type: 'string'
		},
		{
			name: 'id_cor', 
			type: 'int'
		},
		{
			name: 'cor', 
			type: 'string'
		},
		{
			name: 'css_cor', 
			type: 'string'
		},
		{
			name: 'sigla_um',
			type: 'string'
		},
		{
			name: 'quantidade',
			type: 'float'
		},
		{
			name: 'quantidade2',
			type: 'string'
		},
		{
			name: 'valor_unitario',
			type: 'float'
		},
		{
			name: 'valor_total',
			type: 'float'
		},
		{
			name: 'observacoes',
			type: 'string'
		}
	]
});