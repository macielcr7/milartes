/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.model.ModelCfop', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_fiscal',
			type: 'int'
		},
		{
			name: 'tipo',
			type: 'string'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'detalhamento',
			type: 'string'
		},
		{
			name: 'operacao',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		}
	]
});