/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelVeiculos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'grupo',
			type: 'string'
		},
		{
			name: 'renavam',
			type: 'string'
		},
		{
			name: 'placa',
			type: 'string'
		},
		{
			name: 'dt_inicio_controle',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'sigla',
			type: 'string'
		},
		{
			name: 'unidade_medida',
			type: 'int'
		},
		{
			name: 'hodometro_inicial',
			type: 'string'
		},
		{
			name: 'combustivel1',
			type: 'string'
		},
		{
			name: 'capacidade1',
			type: 'float'
		},
		{
			name: 'consumo1',
			type: 'float'
		},
		{
			name: 'combustivel2',
			type: 'string'
		},
		{
			name: 'capacidade2',
			type: 'float'
		},
		{
			name: 'consumo2',
			type: 'float'
		},
		{
			name: 'combustivel3',
			type: 'string'
		},
		{
			name: 'capacidade3',
			type: 'float'
		},
		{
			name: 'consumo3',
			type: 'float'
		},
		{
			name: 'capacidade_transporte',
			type: 'float'
		},
		{
			name: 'tara',
			type: 'float'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		}
	]
});