/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProdutos_Categorias', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_categoria',
			type: 'int'
		},
		{
			name: 'id_parent',
			type: 'int'
		},
		{
			name: 'parent',
			type: 'string'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'aliquota_comissao',
			type: 'float'
		},
		{
			name: 'desconto_maximo',
			type: 'float'
		},
		{
			name: 'meta_tag_keywords',
			type: 'string'
		},
		{
			name: 'meta_tag_description',
			type: 'string'
		},
		{
			name: 'cod_depto',
			type: 'int'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		}
	]
});