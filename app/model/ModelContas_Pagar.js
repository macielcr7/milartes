/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelContas_Pagar', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_empresa',
			type: 'string'
		},
		{
			name: 'codigo_barras',
			type: 'string'
		},
		{
			name: 'nome_empresa',
			type: 'string'
		},
		{
			name: 'cod_fornecedor',
			type: 'string'
		},
		{
			name: 'nome_fornecedor',
			type: 'string'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'tipo_conta',
			type: 'string'
		},
		{
			name: 'documento',
			type: 'string'
		},
		{
			name: 'quitado',
			type: 'string'
		},
		{
			name: 'vencimento',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'nf_nro',
			type: 'string'
		},
		{
			name: 'valor_nf',
			type: 'float'
		},
		{
			name: 'tipo_pagto',
			type: 'string'
		},
		{
			name: 'valor_parcela',
			type: 'float'
		},
		{
			name: 'parcela_nro',
			type: 'string'
		},
		{
			name: 'saldo_a_pagar',
			type: 'float'
		},
		{
			name: 'observacoes',
			type: 'string'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'alterado_por_nome',
			type: 'string'
		},
		{
			name: 'body',
			type: 'string'
		}
	]
});