/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelOs_Categorias', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id',
			type: 'int'
		},	
		{
			name: 'descricao',
			type: 'string'
		},	
		{
			name: 'status',
			type: 'string'
		},	
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},	
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},	
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro2',
			type: 'string'
		}				
    ]
});