﻿/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelFornecedores_Endereco', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'controle',
			type: 'int'
		},
		{
			name: 'cod_fornecedor',
			type: 'int'
		},
		{
			name: 'tipo_endereco',
			type: 'string'
		},	
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'loc_nome',
			type: 'string'
		},
		{
			name: 'cidade',
			type: 'string'
		},
		{
			name: 'bairro_nome',
			type: 'string'
		},
		{
			name: 'bairro',
			type: 'string'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'logradouro',
			type: 'string'
		},
		{
			name: 'num_end',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'ponto_referencia',
			type: 'string'
		},
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'cx_postal',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'alterado_por',
			type: 'int'
		}
	]
});