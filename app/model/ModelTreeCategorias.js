Ext.define('ShSolutions.model.ModelTreeCategorias', {
    extend: 'Ext.data.Model',
    alias: 'model.treedepartamentos',

    fields: [
        {
            name: 'leaf',
            type: 'boolean'
        },
        {
            name: 'text'
        },
        {
            name: 'cod_categoria',
            type: 'int'
        }
    ]
});