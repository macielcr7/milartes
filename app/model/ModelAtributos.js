/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAtributos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_atributos',
			type: 'int'
		},
		{
			name: 'atributo',
			type: 'string'
		},
		{
			name: 'sigla',
			type: 'string'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		}
	]
});