/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProdutos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'descricao_completa',
			type: 'string'
		},
		{
			name: 'descricao_abreviada',
			type: 'string'
		},
		{
			name: 'descricao_impressao',
			type: 'string'
		},
		{
			name: 'tipo_produto',
			type: 'string'
		},
		{
			name: 'departamento',
			type: 'int'
		},
		{
			name: 'descricao_departamento',
			type: 'string'
		},
		{
			name: 'categoria',
			type: 'int'
		},
		{
			name: 'descricao_categoria',
			type: 'string'
		},
		{
			name: 'monta_nome_fornecedor',
			type: 'string'
		},
		{
			name: 'fornecedor',
			type: 'int'
		},
		{
			name: 'setor_estoque',
			type: 'int'
		},
		{
			name: 'descricao_setor_estoque',
			type: 'string'
		},
		{
			name: 'fabricante_marca',
			type: 'int'
		},
		{
			name: 'descricao_marca',
			type: 'string'
		},
		{
			name: 'sigla',
			type: 'string'
		},
		{
			name: 'unidade_medida_descricao',
			type: 'string'
		},
		{
			name: 'unidade_medida',
			type: 'int'
		},
		{
			name: 'ncm',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'preco_custo',
			type: 'float'
		},
		{
			name: 'preco_custo_servico',
			type: 'float'
		},
		{
			name: 'preco_venda',
			type: 'float'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'dobra_quantidade',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		}
	]
});