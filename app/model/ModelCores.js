/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCores', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id_cores',								 
			type: 'int'
		},				
		{
			name: 'descricao',								 
			type: 'string'
		},				
		{
			name: 'codigo',								 
			type: 'string'
		}				
    ]
});