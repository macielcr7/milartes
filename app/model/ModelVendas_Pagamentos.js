/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelVendas_Pagamentos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_venda',
			type: 'int'
		},
		{
			name: 'forma_pagamento',
			type: 'int'
		},
		{
			name: 'categoria_pagamento',
			type: 'int'
		},
		{
			name: 'forma_pagto',
			type: 'int'
		},
		{
			name: 'nro_parcelas_cartao',
			type: 'int'
		},
		{
			name: 'forma_pagamento_descricao',
			type: 'string'
		},
		{
			name: 'valor_pago',
			type: 'float'
		},
		{
			name: 'observacao',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'estornado',
			type: 'string'
		},
		{
			name: 'data_estorno',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'estornado_por_nome',
			type: 'string'
		},
		{
			name: 'nro_parcelas',
			type: 'int'
		}
	]
});