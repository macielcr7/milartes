/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelLogs_Sistema', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'controle',
			type: 'int'
		},
		{
			name: 'chave',
			type: 'string'
		},
		{
			name: 'tabela',
			type: 'string'
		},
		{
			name: 'tabela_cod',
			type: 'int'
		},
		{
			name: 'acao',
			type: 'string'
		},
		{
			name: 'campo',
			type: 'string'
		},
		{
			name: 'alterado_de',
			type: 'auto'
		},
		{
			name: 'alterado_para',
			type: 'auto'
		},
		{
			name: 'usuario',
			type: 'int'
		},
		{
			name: 'usuario2',
			type: 'string'
		},
		{
			name: 'data_time',
			dateFormat: 'H:i:s',	 
			type: 'date'
		},
		{
			name: 'data_date',
			dateFormat: 'Y-m-d',	 
			type: 'date'
		},
		{
			name: 'data',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		}				
	]
});