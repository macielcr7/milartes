/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCombo', {
	extend: 'Ext.data.Model',

	internalId: 'id',

	fields: [
		{
			name: 'id'//,
			//type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'descricao_list',
			type: 'string'
		},
		{
			name: 'cnpj',
			type: 'string'
		},
		{
			name: 'docto',
			type: 'string'
		},
		{
			name: 'endereco_completo',
			type: 'string'
		}
	]
});