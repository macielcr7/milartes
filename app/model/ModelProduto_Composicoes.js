/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProduto_Composicoes', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id_produto_composicoes',								 
			type: 'int'
		},
		{
			name: 'id_produto',								 
			type: 'int'
		},	
		{
			name: 'quantidade',								 
			type: 'float'
		},				
		{
			name: 'produto',								 
			type: 'string'
		},				
		{
			name: 'composicao',								 
			type: 'string'
		},				
		{
			name: 'id_composicao',								 
			type: 'int'
		}				
    ]
});