/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelOs', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'id2',
			type: 'string'
		},
		{
			name: 'titulo',
			type: 'string'
		},
		{
			name: 'conteudo',
			type: 'auto'
		},
		{
			name: 'nome_usuario_abertura',
			type: 'string'
		},
		{
			name: 'usuario_abertura',
			type: 'int'
		},
		{
			name: 'nome_usuario_responsavel',
			type: 'string'
		},
		{
			name: 'usuario_responsavel',
			type: 'int'
		},
		{
			name: 'urgencia',
			type: 'string'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'departamento',
			type: 'int'
		},
		{
			name: 'categoria_descricao',
			type: 'string'
		},
		{
			name: 'categoria',
			type: 'int'
		},
		{
			name: 'local_descricao',
			type: 'string'
		},
		{
			name: 'local',
			type: 'int'
		},
		{
			name: 'data_abertura_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_abertura_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_abertura',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'status',
			type: 'string'
		},
		{
			name: 'nome_usuario_fechamento',
			type: 'string'
		},
		{
			name: 'usuario_fechamento',
			type: 'int'
		},
		{
			name: 'data_fechamento_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_fechamento_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_fechamento',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'dt_ultimo_comentario_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'dt_ultimo_comentario_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'dt_ultimo_comentario',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cidade',
			type: 'int'
		},
		{
			name: 'loc_nome_abreviado',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'estado_descricao',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'endereco_num',
			type: 'string'
		},
		{
			name: 'endereco_complemento',
			type: 'string'
		},
		{
			name: 'endereco_referencia',
			type: 'string'
		},
		{
			name: 'endereco_bairro',
			type: 'int'
		},
		{
			name: 'endereco_bairro_nome',
			type: 'string'
		},
		{
			name: 'fone1',
			type: 'string'
		},
		{
			name: 'fone2',
			type: 'string'
		},
		{
			name: 'fone3',
			type: 'string'
		},
		{
			name: 'fone4',
			type: 'string'
		},
		{
			name: 'fone5',
			type: 'string'
		},
		{
			name: 'nome_fantasia',
			type: 'string'
		},
		{
			name: 'empresa',
			type: 'int'
		},
		{
			name: 'verificado_fatura',
			type: 'string'
		},
		{
			name: 'data_agendada_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_agendada_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_agendada',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'depto_categoria',
			type: 'string'
		},
		{
			name: 'endereco_completo',
			type: 'string'
		},
		{
			name: 'data_abertura2',
			type: 'string'
		},
		{
			name: 'data_agendada2',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'string'
		},
		{
			name: 'cliente',
			type: 'int'
		},
		{
			name: 'nome_cliente',
			type: 'string'
		}
	]
});