/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelVeiculos_Combustivel', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_veiculo',
			type: 'int'
		},
		{
			name: 'hodometro',
			type: 'string'
		},
		{
			name: 'tipo_combustivel',
			type: 'string'
		},
		{
			name: 'quantidade',
			type: 'string'
		},
		{
			name: 'valor_abastecimento',
			type: 'float'
		},
		{
			name: 'data_abastecimento',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'quem_abasteceu',
			type: 'string'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'distancia_percorrida',
			type: 'string'
		},
		{
			name: 'consumo_medio',
			type: 'string'
		}
	]
});