/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAtributos_Produto', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_atributos_produto',
			type: 'int'
		},
		{
			name: 'id_atributo_categoria',
			type: 'int'
		},
		{
			name: 'id_produto',
			type: 'int'
		},
		{
			name: 'valor',
			type: 'string'
		},
		{
			name: 'descricao_atributo',
			type: 'string'
		}
	]
});