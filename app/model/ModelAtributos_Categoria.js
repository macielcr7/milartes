/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAtributos_Categoria', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_atributos_categoria',
			type: 'int'
		},
		{
			name: 'atributo',
			type: 'string'
		},
		{
			name: 'id_atributo',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'id_categoria',
			type: 'int'
		}
	]
});