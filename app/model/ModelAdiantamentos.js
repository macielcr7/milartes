/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAdiantamentos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cliente',
			type: 'int'
		},
		{
			name: 'valor',
			type: 'float'
		},
		{
			name: 'descricao_pagto',
			type: 'string'
		},
		{
			name: 'forma_pagto',
			type: 'int'
		},
		{
			name: 'valor_utilizado',
			type: 'float'
		},
		{
			name: 'saldo',
			type: 'float'
		},
		{
			name: 'observacao',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'cadastrado_por_desc',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'alterado_por_desc',
			type: 'string'
		},
		{
			name: 'estornado',
			type: 'string'
		},
		{
			name: 'data_estorno_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_estorno_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_estorno',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'estornado_por',
			type: 'int'
		},
		{
			name: 'estornado_por_desc',
			type: 'string'
		},
		{
			name: 'categoria_pagamento',
			type: 'int'
		},
	]
});