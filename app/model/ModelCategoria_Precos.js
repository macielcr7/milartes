/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelCategoria_Precos', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id_categoria_precos',								 
			type: 'int'
		},				
		{
			name: 'descricao',								 
			type: 'string'
		}				
    ]
});