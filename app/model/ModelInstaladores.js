/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelInstaladores', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_instalador',
			type: 'int'
		},
		{
			name: 'nome_completo',
			type: 'string'
		},
		{
			name: 'apelido',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'fone1',
			type: 'string'
		},
		{
			name: 'fone2',
			type: 'string'
		},
		{
			name: 'fone3',
			type: 'string'
		},
		{
			name: 'fone4',
			type: 'string'
		},
		{
			name: 'fone5',
			type: 'string'
		},
		{
			name: 'obs_fone1',
			type: 'string'
		},
		{
			name: 'obs_fone2',
			type: 'string'
		},
		{
			name: 'obs_fone3',
			type: 'string'
		},
		{
			name: 'obs_fone4',
			type: 'string'
		},
		{
			name: 'obs_fone5',
			type: 'string'
		},
		{
			name: 'produtos_que_instala',
			type: 'string'
		},
		{
			name: 'fones',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		},
		{
			name: 'obs_fone',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		}
	]
});