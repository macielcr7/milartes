/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelContas_Pagar_Pagtos', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_conta',
			type: 'int'
		},
		{
			name: 'tipo_pagto',
			type: 'int'
		},
		{
			name: 'valor_pago',
			type: 'float'
		},
		{
			name: 'observacao',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'data_pagto',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		}
	]
});