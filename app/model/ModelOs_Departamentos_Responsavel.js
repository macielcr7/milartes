/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelOs_Departamentos_Responsavel', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'nome_departamento',
			type: 'string'
		},
		{
			name: 'id_depto',
			type: 'int'
		},
		{
			name: 'responsavel',
			type: 'string'
		},
		{
			name: 'id_usuario',
			type: 'int'
		},
		{
			name: 'nome_fantasia',
			type: 'string'
		},
		{
			name: 'id_empresa',
			type: 'int'
		},
		{
			name: 'cadastrado_por',
			type: 'string'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		}
	]
});