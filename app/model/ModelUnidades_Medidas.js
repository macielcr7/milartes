/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelUnidades_Medidas', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'unidade_medida',
			type: 'string'
		},
		{
			name: 'sigla',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		}
	]
});