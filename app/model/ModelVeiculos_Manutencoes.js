/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelVeiculos_Manutencoes', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_veiculo',
			type: 'int'
		},
		{
			name: 'hodometro',
			type: 'string'
		},
		{
			name: 'tipo_manutencao',
			type: 'string'
		},
		{
			name: 'valor_manutencao',
			type: 'float'
		},
		{
			name: 'data_manutencao',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'detalhamento',
			type: 'auto'
		},
		{
			name: 'oficina',
			type: 'int'
		},
		{
			name: 'oficina_nome',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		}
	]
});