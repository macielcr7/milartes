/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAmbientes_Orcamentos_Rodape', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_ambientes_orcamentos_rodape',
			type: 'int'
		},
		{
			name: 'id_produtos_orcamentos_rodape',
			type: 'int'
		},
		{
			name: 'ordem',
			type: 'int'
		},
		{
			name: 'ambiente',
			type: 'string'
		},
		{
			name: 'observacoes',
			type: 'string'
		},
		{
			name: 'qtd_paredes',
			type: 'int'
		},
		{
			name: 'comprimento_1',
			type: 'float'
		},
		{
			name: 'comprimento_real_1',
			type: 'float'
		},
		{
			name: 'comprimento_2',
			type: 'float'
		},
		{
			name: 'comprimento_real_2',
			type: 'float'
		},
		{
			name: 'comprimento_3',
			type: 'float'
		},
		{
			name: 'comprimento_real_3',
			type: 'float'
		},
		{
			name: 'comprimento_4',
			type: 'float'
		},
		{
			name: 'comprimento_real_4',
			type: 'float'
		},
		{
			name: 'comprimento_5',
			type: 'float'
		},
		{
			name: 'comprimento_real_5',
			type: 'float'
		},
		{
			name: 'comprimento_6',
			type: 'float'
		},
		{
			name: 'comprimento_real_6',
			type: 'float'
		},
		{
			name: 'comprimento_7',
			type: 'float'
		},
		{
			name: 'comprimento_real_7',
			type: 'float'
		},
		{
			name: 'comprimento_8',
			type: 'float'
		},
		{
			name: 'comprimento_real_8',
			type: 'float'
		},
		{
			name: 'comprimento_9',
			type: 'float'
		},
		{
			name: 'comprimento_real_9',
			type: 'float'
		},
		{
			name: 'comprimento_10',
			type: 'float'
		},
		{
			name: 'comprimento_real_10',
			type: 'float'
		},
		{
			name: 'comprimento_11',
			type: 'float'
		},
		{
			name: 'comprimento_real_11',
			type: 'float'
		},
		{
			name: 'comprimento_12',
			type: 'float'
		},
		{
			name: 'comprimento_real_12',
			type: 'float'
		},
		{
			name: 'comprimento_13',
			type: 'float'
		},
		{
			name: 'comprimento_real_13',
			type: 'float'
		},
		{
			name: 'comprimento_14',
			type: 'float'
		},
		{
			name: 'comprimento_real_14',
			type: 'float'
		},
		{
			name: 'comprimento_15',
			type: 'float'
		},
		{
			name: 'comprimento_real_15',
			type: 'float'
		},
		{
			name: 'comprimento_16',
			type: 'float'
		},
		{
			name: 'comprimento_real_16',
			type: 'float'
		},
		{
			name: 'comprimento_17',
			type: 'float'
		},
		{
			name: 'comprimento_real_17',
			type: 'float'
		},
		{
			name: 'comprimento_18',
			type: 'float'
		},
		{
			name: 'comprimento_real_18',
			type: 'float'
		},
		{
			name: 'comprimento_19',
			type: 'float'
		},
		{
			name: 'comprimento_real_19',
			type: 'float'
		},
		{
			name: 'comprimento_20',
			type: 'float'
		},
		{
			name: 'comprimento_real_20',
			type: 'float'
		},
		{
			name: 'comprimento_21',
			type: 'float'
		},
		{
			name: 'comprimento_real_21',
			type: 'float'
		},
		{
			name: 'area',
			type: 'float'
		}
	]
});