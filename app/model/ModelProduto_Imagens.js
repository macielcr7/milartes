/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelProduto_Imagens', {
    extend: 'Ext.data.Model',

    fields: [
		{
			name: 'id_produto_imagens',								 
			type: 'int'
		},				
		{
			name: 'descricao_completa',								 
			type: 'string'
		},				
		{
			name: 'id_produto',								 
			type: 'int'
		},				
		{
			name: 'src',								 
			type: 'string'
		}				
    ]
});