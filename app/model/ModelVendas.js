/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelVendas', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id_orcamento',
			type: 'int'
		},
		{
			name: 'id_orcamentista',
			type: 'int'
		},
		{
			name: 'id_os',
			type: 'int'
		},
		{
			name: 'venda_cod_orcamento',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'validade_orcamento',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'id_cliente',
			type: 'int'
		},
		{
			name: 'cliente',
			type: 'string'
		},
		{
			name: 'cliente_descricao',
			type: 'string'
		},
		{
			name: 'desconto_real',
			type: 'float'
		},
		{
			name: 'desconto_porcento',
			type: 'float'
		},
		{
			name: 'tipo_orcamento',
			type: 'int'
		},
		{
			name: 'acrescimo_real',
			type: 'float'
		},
		{
			name: 'acrescimo_porcento',
			type: 'float'
		},
		{
			name: 'valor_bruto',
			type: 'float'
		},
		{
			name: 'valor_total',
			type: 'float'
		},
		{
			name: 'total_adiantado',
			type: 'float'
		},
		{
			name: 'total_pago',
			type: 'float'
		},
		{
			name: 'saldo_a_receber',
			type: 'float'
		},
		{
			name: 'aos_cuidados_de',
			type: 'string'
		},
		{
			name: 'observacoes',
			type: 'string'
		},
		{
			name: 'endereco_completo',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'cidade',
			type: 'string'
		},
		{
			name: 'bairro_nome',
			type: 'string'
		},
		{
			name: 'bairro',
			type: 'int'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'nro_end',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'ponto_ref',
			type: 'string'
		},
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'convertido_venda',
			type: 'string'
		},
		{
			name: 'data_venda',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'eh_venda',
			type: 'string'
		},
		{
			name: 'fone1',
			type: 'string'
		},
		{
			name: 'fone2',
			type: 'string'
		},
		{
			name: 'fone3',
			type: 'string'
		},
		{
			name: 'fone4',
			type: 'string'
		},
		{
			name: 'fone5',
			type: 'string'
		},
		{
			name: 'fone1_obs',
			type: 'string'
		},
		{
			name: 'fone2_obs',
			type: 'string'
		},
		{
			name: 'fone3_obs',
			type: 'string'
		},
		{
			name: 'fone4_obs',
			type: 'string'
		},
		{
			name: 'fone5_obs',
			type: 'string'
		},
		{
			name: 'venda_finalizada',
			type: 'string'
		},
		{
			name: 'venda_finalizada_em',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		}
	]
});