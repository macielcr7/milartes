/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.model.ModelContas_Bancarias', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_empresa',
			type: 'string'
		},
		{
			name: 'cod_banco',
			type: 'string'
		},
		{
			name: 'nome_banco',
			type: 'string'
		},
		{
			name: 'gerente_banco',
			type: 'string'
		},
		{
			name: 'agencia_conta',
			type: 'string'
		},
		{
			name: 'agencia',
			type: 'string'
		},
		{
			name: 'agencia_digito',
			type: 'string'
		},
		{
			name: 'conta_corrente',
			type: 'string'
		},
		{
			name: 'conta_corrente_digito',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		}
	]
});