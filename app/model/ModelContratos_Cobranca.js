/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelContratos_Cobranca', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'conta_bancaria',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'cod_empresa',
			type: 'int'
		},
		{
			name: 'nome_empresa',
			type: 'string'
		},
		{
			name: 'banco',
			type: 'string'
		},
		{
			name: 'agencia',
			type: 'string'
		},
		{
			name: 'conta_corrente',
			type: 'string'
		},
		{
			name: 'banco_agencia_conta',
			type: 'string'
		},
		{
			name: 'layout_cobranca',
			type: 'string'
		},
		{
			name: 'operacao',
			type: 'string'
		},
		{
			name: 'convenio',
			type: 'string'
		},
		{
			name: 'convenio_digito',
			type: 'string'
		},
		{
			name: 'contrato',
			type: 'string'
		},
		{
			name: 'carteira',
			type: 'string'
		},
		{
			name: 'carteira_variacao',
			type: 'string'
		},
		{
			name: 'carteira_tipo',
			type: 'string'
		},
		{
			name: 'taxa_boleto',
			type: 'string'
		},
		{
			name: 'taxa_modalidate',
			type: 'string'
		},
		{
			name: 'cadastrado_por_nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'int'
		},
		{
			name: 'alterado_por',
			type: 'int'
		},
		{
			name: 'data_cadastro',
			type: 'string'
		},
		{
			name: 'ativo',
			type: 'string'
		}
	]
});