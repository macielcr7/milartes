/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelClientes', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'cod_cliente',
			type: 'int'
		},
		{
			name: 'tipo_cliente',
			type: 'string'
		},
		{
			name: 'nome_completo',
			type: 'string'
		},
		{
			name: 'razao_social',
			type: 'string'
		},
		{
			name: 'nome_fantasia',
			type: 'string'
		},
		{
			name: 'pessoa_contato',
			type: 'string'
		},
		{
			name: 'data_nascimento',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'sexo',
			type: 'string'
		},
		{
			name: 'cpf',
			type: 'string'
		},
		{
			name: 'cnpj',
			type: 'string'
		},
		{
			name: 'ie',
			type: 'string'
		},
		{
			name: 'im',
			type: 'string'
		},
		{
			name: 'identidade',
			type: 'string'
		},
		{
			name: 'profissao',
			type: 'string'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'estado',
			type: 'string'
		},
		{
			name: 'loc_nome',
			type: 'string'
		},
		{
			name: 'cidade',
			type: 'string'
		},
		{
			name: 'bairro_nome',
			type: 'string'
		},
		{
			name: 'bairro',
			type: 'int'
		},
		{
			name: 'fones', 
			type: 'string'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'endereco',
			type: 'string'
		},
		{
			name: 'nro_end',
			type: 'string'
		},
		{
			name: 'loteamento',
			type: 'string'
		},
		{
			name: 'complemento',
			type: 'string'
		},
		{
			name: 'ponto_ref',
			type: 'string'
		},
		{
			name: 'cx_postal',
			type: 'string'
		},	
		{
			name: 'cep',
			type: 'string'
		},
		{
			name: 'data_cadastro_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_cadastro_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_cadastro',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'cadastrado_por',
			type: 'string'
		},
		{
			name: 'nome',
			type: 'string'
		},
		{
			name: 'alterado_por',
			type: 'string'
		},
		{
			name: 'data_alteracao_time',
			dateFormat: 'H:i:s',
			type: 'date'
		},
		{
			name: 'data_alteracao_date',
			dateFormat: 'Y-m-d',
			type: 'date'
		},
		{
			name: 'data_alteracao',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'situacao_cadastral',
			type: 'string'
		},
		{
			name: 'fone1',
			type: 'string'
		},
		{
			name: 'fone1_obs',
			type: 'string'
		},
		{
			name: 'fone2',
			type: 'string'
		},
		{
			name: 'fone2_obs',
			type: 'string'
		},
		{
			name: 'fone3',
			type: 'string'
		},
		{
			name: 'fone3_obs',
			type: 'string'
		},
		{
			name: 'fone4',
			type: 'string'
		},
		{
			name: 'fone4_obs',
			type: 'string'
		},
		{
			name: 'fone5',
			type: 'string'
		},
		{
			name: 'fone5_obs',
			type: 'string'
		},
		{
			name: 'email_nfe',
			type: 'string'
		},
		{
			name: 'id_categoria_precos',
			type: 'string'
		}
	]
});