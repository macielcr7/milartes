/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelComboCategorias', {
	extend: 'Ext.data.Model',

	internalId: 'id',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'descricao',
			type: 'string'
		},
		{
			name: 'descricao_list',
			type: 'string'
		}
	]
});