/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.model.ModelAdiantamentosVinculo', {
	extend: 'Ext.data.Model',

	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'cod_adiantamento',
			type: 'int'
		},
		{
			name: 'cod_venda',
			type: 'int'
		},
		{
			name: 'tipo_orcamento',
			type: 'int'
		},
		{
			name: 'valor_vinculado',
			type: 'float'
		},
		{
			name: 'valor_total_venda',
			type: 'float'
		},
		{
			name: 'data_vinculo',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'vinculado_por',
			type: 'int'
		},
		{
			name: 'vinculado_por_desc',
			type: 'string'
		},
		{
			name: 'cancelado',
			type: 'string'
		},
		{
			name: 'desvinculado_por',
			type: 'int'
		},
		{
			name: 'desvinculado_por_desc',
			type: 'string'
		},
		{
			name: 'venda_finalizada',
			type: 'string'
		},
		{
			name: 'data_desvinculo',
			dateFormat: 'Y-m-d H:i:s',
			type: 'date'
		},
		{
			name: 'desvinculo_motivo',
			type: 'string'
		}
	]
});