/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.contratos_cobranca.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.contratos_cobrancalist',
	requires: [
		'ShSolutions.store.StoreContratos_Cobranca'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'contratos_cobranca',

	id: 'List-Contratos_Cobranca',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 400,
	title: 'Contratos de Cobrança',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridContratos_Cobranca',
					store: 'StoreContratos_Cobranca',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'ID',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cod_empresa',
							hidden: true,
							text: 'Empresa Cod',
							width: 90
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_empresa',
							text: 'Empresa',
							width: 400
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'banco_agencia_conta',
							text: 'Banco / Agência / Conta',
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'layout_cobranca',
							text: 'Layout',
							width: 60
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 120
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'convenio',
							text: 'Convênio',
							width: 80,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'contrato',
							text: 'Contrato',
							width: 80,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'carteira',
							text: 'Carteira / Variação',
							width: 120
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
								}
							},
							width: 50
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 130
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreContratos_Cobranca',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_contratos_cobranca',
									iconCls: 'bt_add',
									disabled: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_contratos_cobranca',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_contratos_cobranca',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_contratos_cobranca',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_contratos_cobranca').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_contratos_cobranca',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativo',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});