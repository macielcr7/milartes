/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contratos_cobranca.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcontratos_cobrancawin',

	id: 'AddContratos_CobrancaWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	title: 'Cadastro de Contratos de Cobrança',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormContratos_Cobranca',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/contratos_cobranca/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContratosContaBancaria',
									name: 'conta_bancaria',
									allowBlank: false,
									margin: '0 5 0 0',
									id: 'conta_bancaria_contratos_cobranca',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Conta Bancária',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'descricao',
									allowBlank: false,
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Descrição'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContratosCobrancaLayout',
									name: 'layout_cobranca',
									id: 'layout_cobranca_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									fieldLabel: 'Layout de Cobrança'
								},
								{
									xtype: 'textfield',
									name: 'operacao',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'operacao_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Operação'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'convenio',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'convenio_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Convênio'
								},
								{
									xtype: 'textfield',
									name: 'convenio_digito',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'convenio_digito_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Dígito Convênio'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'carteira',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'carteira_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Carteira'
								},
								{
									xtype: 'textfield',
									name: 'carteira_variacao',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'carteira_variacao_digito_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Variação da Carteira'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'carteira_tipo',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'carteira_tipo_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Tipo de Carteira'
								},
								{
									xtype: 'textfield',
									name: 'contrato',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'contrato_contratos_cobranca',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Contrato'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '100%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'taxa_boleto',
											allowBlank: true,
											margin: '0 5 0 0',
											flex: 1,
											mask: 'R$ #9.999.990,00',
											money: true,
											returnWithMask: true,
											plugins: 'textmask',
											id: 'taxa_boleto_contratos_cobranca',
											fieldStyle: 'text-transform:uppercase',
											anchor: '100%',
											fieldLabel: 'Taxa de Uso do Boleto'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboContratosCobrancaModalidadeTaxa',
											name: 'taxa_modalidade',
											id: 'taxa_modalidade_contratos_cobranca',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											margins: '0 5 0 0',
											fieldLabel: 'Modalidade da Taxa'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '25%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											id: 'ativo_contratos_cobranca',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											value: 'S',
											anchor: '100%',
											margin: '0 5 0 0',
											fieldLabel: 'Ativo'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'id',
									hidden: true,
									margin: '0 5 0 0',
									id: 'id_contratos_cobranca',
									value: 0,
									anchor: '100%',
									fieldLabel: 'id'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_contratos_cobranca',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contratos_cobranca',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_contratos_cobranca',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});