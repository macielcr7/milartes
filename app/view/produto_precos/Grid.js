/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.produto_precos.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridproduto_precoslist',
	requires: [
		'ShSolutions.store.StoreProduto_Precos'
	],
	id: 'GridProduto_Precos',
	store: 'StoreProduto_Precos',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	columns: [
		{xtype: 'rownumberer'},
		{
			xtype: 'gridcolumn',
			dataIndex: 'descricao',
			text: 'Categoria Preços',
			width: 200
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'valor',
			renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
			text: 'Valor de Venda',
			width: 120
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'margem_lucro',
			text: 'Margem de Lucro',
			width: 220
		}
	],
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_produto_precos',
					iconCls: 'bt_add',
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_produto_precos',
					iconCls: 'bt_edit',
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_produto_precos',
					iconCls: 'bt_del',
					action: 'deletar',
					text: 'Deletar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});