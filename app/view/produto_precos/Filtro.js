/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_precos.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filterproduto_precoswin',

    id: 'FilterProduto_PrecosWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Produto_Precos',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterProduto_Precos',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboCategoria_Precos',
                                    name: 'id_categoria_precos',
									id: 'id_categoria_precos_filter_produto_precos',
									button_id: 'button_id_categoria_precos_filter_produto_precos',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Categoria Preços'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_categoria_precos_filter_produto_precos',
                                    combo_id: 'id_categoria_precos_filter_produto_precos',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboProdutos',
                                    name: 'id_produto',
									id: 'id_produto_filter_produto_precos',
									button_id: 'button_id_produto_filter_produto_precos',
									flex: 1,
									anchor: '100%',
									fieldLabel: ' Produto'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_produto_filter_produto_precos',
                                    combo_id: 'id_produto_filter_produto_precos',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
							xtype: 'textfield',
							name: 'valor',
							id: 'valor_filter_produto_precos',							
							anchor: '100%',
							fieldLabel: 'Valor'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_produto_precos',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
