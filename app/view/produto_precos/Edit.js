/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_precos.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addproduto_precoswin',

	id: 'AddProduto_PrecosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 350,
	
	title: 'Cadastro de Preços de Vendas',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProduto_Precos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produto_precos/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCategoria_Precos',
									name: 'id_categoria_precos',
									id: 'id_categoria_precos_produto_precos',
									button_id: 'button_id_categoria_precos_produto_precos',
									flex: 1,									
									anchor: '100%',
									fieldLabel: 'Categoria Preços'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_id_categoria_precos_produto_precos',
									combo_id: 'id_categoria_precos_produto_precos',
									action: 'reset_combo'
								},
								{
									xtype: 'buttonadd',
									tabela: 'Categoria_Precos',
									action: 'add_win'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'valor',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							id: 'valor_produto_precos',
							anchor: '100%',							
							fieldLabel: 'Valor',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 1000);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'id_produto',
							hidden: true,
							fieldLabel: 'ID Produto',
							id: 'id_produto_produto_precos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_produto_precos',
							hidden: true,
							fieldLabel: 'ID',
							id: 'id_produto_precos_produto_precos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_produto_precos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produto_precos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produto_precos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
