/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_precos.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produto_precoslist',
    requires: [
    	'ShSolutions.store.StoreProduto_Precos'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'produto_precos',

    id: 'List-Produto_Precos',
    layout: {
        type: 'fit'
    },
    height: 350,
    title: 'Listagem de Produto_Precos',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'gridproduto_precoslist'
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


