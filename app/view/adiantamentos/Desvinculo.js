/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.Desvinculo', {
	extend: 'Ext.window.Window',
	alias: 'widget.addadiantamentosdesvincularvalorwin',

	id: 'Desvinculo-Adiantamento',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 400,
	title: 'Desvinculando Adiantamento da Venda',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormDesvinculoAdiantamento',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/adiantamentos_uso/save.php',
					items: [
						{
							xtype: 'textarea',
							name: 'desvinculo_motivo',
							id: 'desvinculo_motivo_diantamento',
							anchor: '100%',
							fieldStyle: 'text-transform:uppercase',
							fieldLabel: 'Motivo do Desvínculo',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 1000);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'valor_vinculado',
							hidden: true,
							id: 'valor_vinculado_adiantamentos_desvincular',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_desvincular_adiantamentos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'cod_adiantamento',
							hidden: true,
							id: 'cod_adiantamento_desvincular_adiantamentos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_desvincular_adiantamento',
							value: 'DESVINCULAR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_desvinculo_adiantamento',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_desvinculo_adiantamento',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});