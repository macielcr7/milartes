/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.Vincular', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.adiantamentos_vincularlist',
	requires: [
		'ShSolutions.store.StoreAdiantamentosVincular'
	],
	iconCls: 'vincular_adiantamento',
	id: 'List-Adiantamentos_Vincular',
	layout: {
		type: 'fit'
	},
	width: 980,
	height: 400,
	title: 'Vendas em Aberto para o Cliente Selecionado',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridAdiantamentos_Vincular',
					store: 'StoreAdiantamentosVincular',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					selModel: { 
						selType: 'checkboxmodel',
						mode: 'SINGLE',
						allowDeselect: true
					},
					columns: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'id_orcamento',
							text: 'ID Venda',
							sortable: false,
							fixed: true,
							width: 80
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_venda',
							hidden: false,
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Venda',
							sortable: false,
							fixed: true,
							width: 150
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'tipo_orcamento',
							sortable: false,
							fixed: true,
							text: 'Tipo de Orçamento',
							renderer: function(v, r, m){
								if(v==0){
									return '<font color=\"black\">GERAL</font>';
								} else if(v==1) {
									return '<font color=\"black\">FORRO</font>';
								} else if(v==2) {
									return '<font color=\"black\">PERSIANA</font>';
								} else if(v==3) {
									return '<font color=\"black\">PISO</font>';
								} else if(v==4) {
									return '<font color=\"black\">RODAPÉ/SANCA/VISTA/FAIXA</font>';
								} else {
									return v;
								}
							},
							width: 290
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'valor_total',
							text: 'Valor Total',
							width: 100,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
								if(v > 0){
									return '<font color=\"black\"><b>'+value+'</b></font>';
								} else {
									return value;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'total_adiantado',
							text: 'Total Adiantado',
							hidden: false,
							width: 100,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
								if(v > 0){
									return '<font color=\"black\"><b>'+value+'</b></font>';
								} else {
									return value;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'total_pago',
							text: 'Total Pago',
							hidden: false,
							width: 100,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
								if(v > 0){
									return '<font color=\"black\"><b>'+value+'</b></font>';
								} else {
									return value;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'saldo_a_receber',
							text: 'Saldo à Receber',
							hidden: false,
							width: 120,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
								if(v > 0){
									return '<font color=\"blue\"><b>'+value+'</b></font>';
								} else {
									return value;
								}
							}
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreAdiantamentosVincular',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_vincular_adiantamento_adiantamentos',
									iconCls: 'vincular_adiantamento',
									hidden: false,
									disabled: true,
									action: 'vincular_valor',
									text: 'Vincular Valor'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});