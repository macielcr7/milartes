/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.VincularValor', {
    extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addadiantamentosvincularvalorwin',

	id: 'List-Adiantamentos_Vincular_Valor',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 210,
	title: 'Vincular Valor',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAdiantamentosVincularValor',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/adiantamentos_uso/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'valor_disponivel',
							flex: 1,
							id: 'valor_disponivel_adiantamentos_vincular',
							allowBlank: false,
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							readOnly: true,
							fieldLabel: 'Valor Disponível'
						},
						{
							xtype: 'textfield',
							name: 'saldo_a_receber',
							flex: 1,
							id: 'saldo_a_receber_adiantamentos_vincular',
							allowBlank: false,
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							readOnly: true,
							fieldLabel: 'Saldo a Receber'
						},
						{
							xtype: 'textfield',
							name: 'valor',
							flex: 1,
							id: 'valor_adiantamentos_vincular',
							allowBlank: false,
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							fieldLabel: 'Valor à Vincular',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 800);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'id_venda',
							hidden: true,
							fieldLabel: 'ID Venda',
							id: 'id_venda_adiantamentos_vincular',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_adiantamento',
							hidden: true,
							fieldLabel: 'ID Adiantamento',
							id: 'id_adiantamento_adiantamentos_vincular',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_adiantamentos_vincular',
							value: 'VINCULAR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_adiantamentos_vincular',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_adiantamentos_vincular',
							iconCls: 'bt_save',
							action: 'salvar',
							disabled: true,
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});