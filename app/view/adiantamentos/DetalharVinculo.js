/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.DetalharVinculo', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.adiantamentos_detalhar_vinculoslist',
	requires: [
		'ShSolutions.store.StoreAdiantamentosDetalharVinculo'
	],
	iconCls: 'detalhar_vinculo_adiantamento',
	id: 'List-Adiantamentos_Detalhamento_Vinculos',
	layout: {
		type: 'fit'
	},
	width: 1020,
	height: 400,
	title: 'Detalhamento do Uso do Adiantamento Selecionado',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridAdiantamentos_Detalhar_Vinculos',
					store: 'StoreAdiantamentosDetalharVinculo',
					viewConfig: {
						autoScroll: true,
						loadMask: false,
						getRowClass: function(record) { 
							if(record.get('cancelado')=='S'){
								return 'vermelho';
							}
							else{
								return '';
							}

							if(record.get('venda_finalizada')=='S'){
								return 'verde';
							}
							else{
								return '';
							}
						}
					},
					selModel: { 
						selType: 'checkboxmodel',
						mode: 'SINGLE',
						allowDeselect: true
					},
					columns: [
						{
							xtype: 'datecolumn',
							dataIndex: 'data_vinculo',
							hidden: false,
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Vínculo',
							sortable: false,
							fixed: true,
							width: 125
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'vinculado_por_desc',
							text: 'Vinculado Por',
							sortable: false,
							fixed: true,
							width: 250
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'tipo_orcamento',
							sortable: false,
							fixed: true,
							text: 'Tipo de Venda',
							renderer: function(v, r, m){
								if(v==0){
									return '<font color=\"black\">GERAL</font>';
								} else if(v==1) {
									return '<font color=\"black\">FORRO</font>';
								} else if(v==2) {
									return '<font color=\"black\">PERSIANA</font>';
								} else if(v==3) {
									return '<font color=\"black\">PISO</font>';
								} else if(v==4) {
									return '<font color=\"black\">RODAPÉ/SANCA/VISTA/FAIXA</font>';
								} else {
									return v;
								}
							},
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cod_venda',
							text: 'Venda Nro',
							sortable: false,
							fixed: true,
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'valor_total_venda',
							text: 'Valor Venda',
							width: 90,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
								if(v > 0){
									return '<font color=\"black\"><b>'+value+'</b></font>';
								} else {
									return value;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'valor_vinculado',
							text: 'Valor Vinculado',
							width: 100,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
								if(v > 0){
									return '<font color=\"black\"><b>'+value+'</b></font>';
								} else {
									return value;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cancelado',
							sortable: false,
							fixed: true,
							text: 'Cancelado',
							renderer: function(v, r, m){
								if(v=='S'){
									return '<font color=\"black\">SIM</font>';
								} else if(v=='N') {
									return '<font color=\"black\">NÃO</font>';
								} else {
									return v;
								}
							},
							width: 100
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_desvinculo',
							hidden: false,
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Desvinculado Em',
							sortable: false,
							fixed: true,
							width: 125
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'desvinculo_motivo',
							text: 'Motivo do Desvínculo',
							sortable: false,
							fixed: false,
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'desvinculado_por_desc',
							text: 'Desvinculado Por',
							sortable: false,
							fixed: false,
							width: 200
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreAdiantamentosDetalharVinculo',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_desvincular_detalhamento_adiantamento',
									iconCls: 'vincular_adiantamento',
									hidden: false,
									disabled: true,
									action: 'desvincular',
									text: 'Desvincular Adiantamento'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});