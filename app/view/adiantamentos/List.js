/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.adiantamentoslist',
	requires: [
		'ShSolutions.store.StoreAdiantamentos'
	],

	iconCls: 'adiantamentos',
	height: 350,
	id: 'GridAdiantamentos',
	store: 'StoreAdiantamentos',
	viewConfig: {
		autoScroll: true,
		loadMask: false,
		getRowClass: function(record) { 
			if(record.get('estornado')=='S'){
				return 'vermelho';
			}
			else{
				return '';
			}
		}
	},
	title: 'Adiantamentos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'id',
					format: '0',
					hidden: true,
					text: 'Controle',
					width: 65
				},
				{
					xtype: 'numbercolumn',
					dataIndex: 'cliente',
					format: '0',
					hidden: true,
					text: 'Cliente',
					width: 140
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Pagamento',
					width: 130
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Pago',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'descricao_pagto',
					text: 'Forma Pagto',
					width: 180
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_utilizado',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Utilizado',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'saldo',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Saldo',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacao',
					text: 'Observação',
					width: 200
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cadastrado_por_desc',
					text: 'Cadastrado Por',
					width: 200
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'estornado',
					text: 'Estornado',
					renderer: function(v){
						switch(v){
							case 'S':
							return 'SIM';
							break;
							case 'N':
							return 'NÃO';
							break;
						}
					},
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'estornado_por_desc',
					text: 'Estornado Por',
					width: 200
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_estorno',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Estorno',
					width: 110
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreAdiantamentos',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_adiantamentos',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						/*{
							xtype: 'button',
							id: 'button_edit_adiantamentos',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},*/
						{
							xtype: 'button',
							id: 'button_estorno_adiantamentos',
							iconCls: 'estorno_adiantamento1',
							hidden: true,
							disabled: true,
							action: 'estornar',
							text: 'Estonar'
						},
						{
							xtype: 'button',
							id: 'button_imprimir_adiantamentos',
							iconCls: 'bt_imprimir2',
							action: 'imprimir',
							disabled: true,
							text: 'Imprimir Recibo do Adiantamento'
						},
						{
							xtype: 'button',
							id: 'button_vincular_adiantamentos',
							iconCls: 'vincular_adiantamento',
							hidden: true,
							disabled: true,
							action: 'vincular',
							text: 'Vincular Adiantamento'
						},
						{
							xtype: 'button',
							id: 'button_detalhar_vinculo_adiantamentos',
							iconCls: 'detalhar_vinculo_adiantamento',
							hidden: true,
							disabled: true,
							action: 'detalhar_vinculo',
							text: 'Detalhar Uso do Adiantamento'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});