/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.Filtro', {
    extend: 'Ext.window.Window',
	alias: 'widget.filteradiantamentoswin',
	id: 'FilterAdiantamentosWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	title: 'Filtro de Adiantamentos',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterAdiantamentos',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'cliente',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'cliente_filter_adiantamentos',
									anchor: '100%',
									fieldLabel: 'Cliente'
								},

								{
									xtype: 'textfield',
									name: 'valor',																		
									flex: 1,
									id: 'valor_filter_adiantamentos',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Valor'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboFormas_Pagto',
											name: 'forma_pagto',
											id: 'forma_pagto_filter_adiantamentos',
											button_id: 'button_forma_pagto_filter_adiantamentos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Forma Pagto'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_forma_pagto_filter_adiantamentos',
											combo_id: 'forma_pagto_filter_adiantamentos',
											action: 'reset_combo'
										},
									]
								},

								{
									xtype: 'textfield',
									name: 'valor_utilizado',																		
									flex: 1,
									id: 'valor_utilizado_filter_adiantamentos',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Valor Utilizado'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'textfield',
									name: 'saldo',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'saldo_filter_adiantamentos',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Saldo'
								},

								{
									xtype: 'textfield',
									name: 'observacao',																		
									flex: 1,
									id: 'observacao_filter_adiantamentos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Observacao'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},									
									margin: '0 5 0 0',									
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Cadastro',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cadastro_date_filter_adiantamentos',
											name: 'data_cadastro_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro_time_filter_adiantamentos',
											name: 'data_cadastro_time',
											hideLabel: true
										}
									]
								},

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Alteracao',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_alteracao_date_filter_adiantamentos',
											name: 'data_alteracao_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_alteracao_time_filter_adiantamentos',
											name: 'data_alteracao_time',
											hideLabel: true
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'cadastrado_por',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'cadastrado_por_filter_adiantamentos',
									anchor: '100%',
									fieldLabel: 'Cadastrado Por'
								},

								{
									xtype: 'numberfield',
									name: 'alterado_por',																		
									flex: 1,
									id: 'alterado_por_filter_adiantamentos',
									anchor: '100%',
									fieldLabel: 'Alterado Por'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboEstornadoAdiantamentos',
											name: 'estornado',
											loadDisabled: true,
											id: 'estornado_filter_adiantamentos',
											button_id: 'button_estornado_filter_adiantamentos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Estornado'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_estornado_filter_adiantamentos',
											combo_id: 'estornado_filter_adiantamentos',
											action: 'reset_combo'
										}
									]
								},

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Estorno',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_estorno_date_filter_adiantamentos',
											name: 'data_estorno_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_estorno_time_filter_adiantamentos',
											name: 'data_estorno_time',
											hideLabel: true
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'estornado_por',																		
									flex: 1,
									id: 'estornado_por_filter_adiantamentos',
									anchor: '100%',
									fieldLabel: 'Estornado Por'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_adiantamentos',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Resetar Filtro'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});
