/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.adiantamentos.Edit', {
    extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addadiantamentoswin',

	id: 'AddAdiantamentosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Adiantamentos',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAdiantamentos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/adiantamentos/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cliente',
									margin: '0 5 0 0',
									flex: 1,
									hidden: true,
									id: 'cliente_adiantamentos',
									anchor: '100%',
									fieldLabel: 'Cliente'
								},
								{
									xtype: 'textfield',
									name: 'valor',
									flex: 1,
									id: 'valor_adiantamentos',
									allowBlank: false,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Valor',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 800);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboFormas_Pagto',
									name: 'forma_pagto',
									id: 'forma_pagto_adiantamentos',
									flex: 1,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Forma de Pagamento',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'numberfield',
							name: 'nro_parcelas',
							anchor: '100%',
							loadDisabled: true,
							disabled: true,
							hidden: true,
							allowBlank: false,
							minValue: 1,
							id: 'nro_parcelas_adiantamentos',
							fieldLabel: 'Nro de Parcelas'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'observacao',
									flex: 1,
									id: 'observacao_adiantamentos',
									anchor: '100%',
									fieldLabel: 'Observações',
									allowBlank: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							fieldLabel: 'ID',
							id: 'id_adiantamentos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_adiantamentos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_adiantamentos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_adiantamentos').setDisabled(false);
									}, 2500);
								}
							},
							//action: 'resetar',
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_adiantamentos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});