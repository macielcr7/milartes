if (userID == '2' || userID == '11'){
	var papelParede = '<img src="resources/wallpapers/anderson3.jpg">';
}
else{
	var papelParede = '<img src="resources/wallpapers/desktop-wallpaper.jpg">';
}
	Ext.define('ShSolutions.view.Desktop', {
	extend: 'Ext.container.Viewport',
	alias: 'widget.desktopwin',
	
	requires: [
		'ShSolutions.plugins.StartMenu',
		'ShSolutions.plugins.TrayClock'
	],
	
	renderTo: Ext.getBody(),
	
	layout: {
		type: 'fit'
	},

	initComponent: function() {
		var me = this;

		Ext.Ajax.on( {
			beforerequest : function(){
				Ext.getCmp('ajax_connect').addCls('ajax_connect');
			},
			requestcomplete : function(){
				if(Ext.Object.getSize(Ext.Ajax.requests)<=1){
					Ext.getCmp('ajax_connect').removeCls('ajax_connect');
				}
			}
		});

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'panel',
					id: 'panel-desktop',
					layout: {
						type: 'fit'
					},
					border: false,
					xTickSize: 1,
					yTickSize: 1,
					dockedItems: [
						{
							xtype: 'toolbar',
							id: 'toolbar-taskbar-desktop',
							cls: 'ux-taskbar',
							dock: 'bottom',
							items: [
								{
									xtype: 'button',
									cls: 'ux-start-button',
									id: 'start-menu-desktop',
									iconCls: 'ux-start-button-icon',
									menuAlign: 'bl-tl',
									menu: {
										xtype: 'startmenu',
										id: 'startmenu-desktop',
										title: userName,
										iconCls: 'usuario',
										height: 300
									},
									text: 'Menu Iniciar'
								},
								{
									xtype: 'toolbar',
									id: 'toolbar-quick-win',
									minWidth: 20,
									width: 60,
									enableOverflow: true
								},
								{
									xtype: 'splitter',
									cls: 'x-toolbar-separator x-toolbar-separator-horizontal',
									height: 14,
									width: 2,
									html: '&#160;'
								},
								{
									xtype: 'toolbar',
									cls: 'ux-desktop-windowbar',
									items: [ '&#160;' ],
									layout: { overflowHandler: 'Scroller' },
									id: 'toolbar-button-win',
									flex: 1
								},
								{
									xtype: 'tbseparator'
								},
								{
									xtype: 'toolbar',
									width: 250,
									items: [
										{
											xtype: 'splitbutton',
											tooltip: { 
												text: 'Empresa Atual ('+Ext.util.Cookies.get('descricao_empresa_atual')+')', 
												align: 'bl-tl' 
											},
											iconCls: 'view-empresa',
											id: 'view-empresa',
											handler: function(){

											},
											menu: new Ext.menu.Menu({
												id:	'view-empresa-menu',
												items: [
													{
														text: Ext.util.Cookies.get('descricao_empresa_atual'),
														id: 'view-empresa-item-'+ Ext.util.Cookies.get('id_empresa_atual'),
														iconCls: 'view-empresa'
													}
												]
											})
										},
										{
											xtype: 'trayclock',
											flex: 1
										},
/*
										{
											xtype: 'button',
											tooltip: { 
												text: 'Mostrar Área de Trabalho', 
												align: 'bl-tl' 
											},
											iconCls: 'view-desktop',
											id: 'view-desktop'
										},
*/
										{
											xtype: 'container',
											id:'ajax_connect',
											width:20,
											height:16
										}
									]
								}
							]
						}
					],
					items: [
						{
							xtype: 'component',
							cls: 'ux-wallpaper',
							id: 'component-wallpaper',
							stretch: false,
							wallpaper: null,
							stateful  : true,
							stateId  : 'desk-wallpaper',
							html: papelParede
						},
						{
							xtype: 'component',
							id: 'icon-desktop-view',
							style: {
								position: 'absolute'
							},
							x: 0, 
							y: 0
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});