/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_imagens.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addproduto_imagenswin',

	id: 'AddProduto_ImagensWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Imagens',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProduto_Imagens',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produto_imagens/save.php',
					items: [
						{
							xtype: 'filefield',
							buttonText: '',
							buttonConfig: {
								iconCls: 'upload-icon'
							},
							name: 'src',
							id: 'src_produto_imagens',
							anchor: '100%',                         
							fieldLabel: 'Imagem'
						},
						{
							xtype: 'hidden',
							name: 'id_produto',
							id: 'id_produto_produto_imagens',
							hidden: true,
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutosCores2',
									name: 'id_cor',
									id: 'id_cor_produto_imagens',
									anchor: '100%',
									allowBlank: true,
									loadDisabled: true,
									fieldLabel: 'Cor'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									id: 'button_id_cor_produto_imagens',
									combo_id: 'id_cor_produto_imagens',
									action: 'reset_combo'
								}
							]
						},

						{
							xtype: 'hidden',
							name: 'id_produto_imagens',
							hidden: true,
							id: 'id_produto_imagens_produto_imagens',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_produto_imagens',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produto_imagens',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produto_imagens',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
