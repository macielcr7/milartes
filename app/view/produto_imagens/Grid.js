/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.produto_imagens.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridproduto_imagenslist',
	requires: [
		'ShSolutions.store.StoreProduto_Imagens'
	],
	id: 'GridProduto_Imagens',
   	store: 'StoreProduto_Imagens',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,			
	columns: [
		{xtype: 'rownumberer'},
		{
			xtype: 'gridcolumn',
			dataIndex: 'src',
			text: 'Imagem',
			width: 140
		}
	],
    dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_produto_imagens',
					iconCls: 'bt_add',									
					action: 'adicionar',
					text: 'Adicionar'
				},
				/*{
					xtype: 'button',
					id: 'button_edit_produto_imagens',
					iconCls: 'bt_edit',							
					action: 'editar',
					text: 'Editar'
				},*/
				{
					xtype: 'button',
					id: 'button_del_produto_imagens',
					iconCls: 'bt_del',							
					action: 'deletar',
					text: 'Deletar'
				}								
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});