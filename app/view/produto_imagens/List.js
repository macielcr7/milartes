/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_imagens.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produto_imagenslist',
    requires: [
    	'ShSolutions.store.StoreProduto_Imagens'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'produto_imagens',

    id: 'List-Produto_Imagens',
    layout: {
        type: 'fit'
    },
    height: 350,
    title: 'Listagem de Produto_Imagens',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'gridproduto_imagenslist'
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


