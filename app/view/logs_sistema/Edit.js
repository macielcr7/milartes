/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.logs_sistema.Edit', {
    extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addlogs_sistemawin',

    id: 'AddLogs_SistemaWin',
    layout: {
        type: 'fit'
    },
	maximizable: false,
    minimizable: true,
    title: 'Cadastro de Logs_Sistema',

    initComponent: function() {
        var me = this;


        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'FormLogs_Sistema',
                    bodyPadding: 10,
                    autoScroll: true,
                    method: 'POST',
                    url : 'server/modulos/logs_sistema/save.php',
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'chave',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'chave_logs_sistema',
									anchor: '100%',									
									fieldLabel: 'Chave'									
								},								
								{
									xtype: 'textfield',
									name: 'tabela',								    								    
								    flex: 1,
									id: 'tabela_logs_sistema',
									anchor: '100%',									
									fieldLabel: 'Tabela'									
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'tabela_cod',								    
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'tabela_cod_logs_sistema',
									anchor: '100%',									fieldLabel: 'Tabela Cod'
								},								
								{
									xtype: 'textfield',
									name: 'acao',								    								    
								    flex: 1,
									id: 'acao_logs_sistema',
									anchor: '100%',									
									fieldLabel: 'Ação'									
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'campo',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'campo_logs_sistema',
									anchor: '100%',									
									fieldLabel: 'Campo'									
								},								
								{
									xtype: 'textfield',
									name: 'alterado_de',								    								    
								    flex: 1,
									id: 'alterado_de_logs_sistema',
									anchor: '100%',									
									fieldLabel: 'Alterado De'									
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'alterado_para',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'alterado_para_logs_sistema',
									anchor: '100%',									
									fieldLabel: 'Alterado Para'									
								},								
								{
									xtype: 'numberfield',
									name: 'usuario',								    								    
								    flex: 1,
									id: 'usuario_logs_sistema',
									anchor: '100%',									fieldLabel: 'Usuario'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_date_logs_sistema',
											name: 'data_date',
											margins: '0 5 0 0',											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_time_logs_sistema',
											name: 'data_time',											hideLabel: true
										}
									]
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'controle',
							hidden: true,
							id: 'controle_logs_sistema',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_logs_sistema',
							value: 'INSERIR',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            id: 'button_resetar_logs_sistema',
                            iconCls: 'bt_cancel',
                            action: 'resetar',
                            text: 'Resetar'
                        },
                        {
                            xtype: 'button',
                            id: 'button_salvar_logs_sistema',
                            iconCls: 'bt_save',
                            action: 'salvar',
                            text: 'Salvar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);

    }

});
