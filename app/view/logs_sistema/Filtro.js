/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.logs_sistema.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filterlogs_sistemawin',

    id: 'FilterLogs_SistemaWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro de Logs_Sistema',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterLogs_Sistema',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'chave',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'chave_filter_logs_sistema',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Chave'
								},								
								{
									xtype: 'textfield',
									name: 'tabela',								    								    
								    flex: 1,
									id: 'tabela_filter_logs_sistema',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Tabela'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'tabela_cod',								    
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'tabela_cod_filter_logs_sistema',
									anchor: '100%',
									fieldLabel: 'Tabela Cod'
								},								
								{
									xtype: 'textfield',
									name: 'acao',								    								    
								    flex: 1,
									id: 'acao_filter_logs_sistema',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Ação'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'campo',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'campo_filter_logs_sistema',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Campo'
								},								
								{
									xtype: 'textfield',
									name: 'alterado_de',								    								    
								    flex: 1,
									id: 'alterado_de_filter_logs_sistema',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Alterado De'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'alterado_para',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'alterado_para_filter_logs_sistema',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Alterado Para'
								},								
								{
									xtype: 'numberfield',
									name: 'usuario',								    								    
								    flex: 1,
									id: 'usuario_filter_logs_sistema',
									anchor: '100%',
									fieldLabel: 'Usuario'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_date_filter_logs_sistema',
											name: 'data_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_time_filter_logs_sistema',
											name: 'data_time',
											hideLabel: true
										}
									]
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_logs_sistema',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
