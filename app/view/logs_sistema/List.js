/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.logs_sistema.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.logs_sistemalist',
    requires: [
    	'ShSolutions.view.logs_sistema.LogsSistemaList'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'logs_sistema',

    id: 'List-Logs_Sistema',
    layout: {
        type: 'fit'
    },
    height: 350,
    title: 'Listagem de Logs_Sistema',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'gridlogs_sistemalist'
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


