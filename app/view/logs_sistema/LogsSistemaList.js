/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.logs_sistema.LogsSistemaList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridlogs_sistemalist',
	requires: [
		'ShSolutions.store.StoreLogs_Sistema'
	],

	id: 'GridLogs_Sistema',
	store: 'StoreLogs_Sistema',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},

	columns: [
		{
			xtype: 'numbercolumn',
			dataIndex: 'controle',
			hidden: true,
			format: '0',
			text: 'Controle',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'chave',
			hidden: true,
			text: 'Chave',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'tabela',
			hidden: true,
			text: 'Tabela',
			width: 140
		},
		{
			xtype: 'datecolumn',
			dataIndex: 'data',
			format: 'd/m/Y H:i:s',
			renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
			text: 'Data',
			width: 140
		},
		{
			xtype: 'numbercolumn',
			dataIndex: 'usuario',
			format: '0',
			hidden: true,
			text: 'Usuario',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'usuario2',
			text: 'Usuario',
			width: 140
		},
		{
			xtype: 'numbercolumn',
			dataIndex: 'tabela_cod',
			format: '0',
			text: 'Tabela Cod',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'acao',
			text: 'Ação',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'campo',
			text: 'Campo',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'alterado_de',
			text: 'Alterado De',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'alterado_para',
			text: 'Alterado Para',
			width: 140
		}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: 'StoreLogs_Sistema',
			dock: 'bottom'
		}
	],

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			
		});

		me.callParent(arguments);
	}

});