/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.fornecedores.Edit', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addfornecedoreswin',

	requires: [
		'ShSolutions.store.StoreFornecedores_Anotacoes',
		'ShSolutions.store.StoreFornecedores_Contato',
		'ShSolutions.store.StoreFornecedores_Endereco',
		'ShSolutions.store.StoreLogs_Sistema'
	],

	id: 'AddFornecedoresWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 900,
	title: 'Cadastro de Fornecedores',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralFornecedores',
					items: [
						{
							xtype: 'form',
							title:'Formulario Principal',
							id: 'FormFornecedores',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/fornecedores/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'tipo_fornecedor',	
											margin: '0 5 0 0',
											flex: 1,
											hidden: true,
											id: 'tipo_fornecedor_fornecedores',
											anchor: '100%',
											fieldLabel: 'Tipo Fornecedor'
										},	
										{
											xtype: 'textfield',
											name: 'razao_social',
											margin: '0 5 0 0',
											flex: 1,
											id: 'razao_social_fornecedores',
											anchor: '100%',
											fieldLabel: 'Razão Social',
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'nome_completo',
											margin: '0 5 0 0',
											hidden: true,
											flex: 1,
											id: 'nome_completo_fornecedores',
											anchor: '100%',	
											fieldLabel: 'Nome Completo', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'nome_fantasia',
											flex: 1,
											allowBlank: true,
											id: 'nome_fantasia_fornecedores',
											anchor: '100%',
											fieldLabel: 'Nome Fantasia', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											hidden: true,
											width: 110,
											mask: '99/99/9999',
											returnWithMask: true,
											plugins: 'textmask',
											allowBlank: true,
											name: 'data_nascimento',
											id: 'data_nascimento_fornecedores',
											fieldLabel: 'Data Nascimento',
											validator: function(value){
												if(value!="" && !validDate(value)){
													return 'Data Inválida...';
												}
												else{
													return true;
												}
											}
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									id: 'fieldcontainer_jf',
									items: [
										{
											xtype: 'textfield',
											name: 'pessoa_contato',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'pessoa_contato_fornecedores',
											anchor: '100%',
											fieldLabel: 'Pessoa de Contato', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'cnpj',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cnpj_fornecedores',
											anchor: '100%',
											mask: '99.999.999/9999-99',
											plugins: 'textmask',
											fieldLabel: 'CNPJ',
											validator: function(value){
												if(value!="" && !validCNPJ(value)){
													return 'CNPJ Inválido...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'textfield',
											name: 'im',
											flex: 1,
											allowBlank: true,
											id: 'im_fornecedores',
											anchor: '100%',	
											fieldLabel: 'Insc. Municipal'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									hidden: true,
									id: 'fieldcontainer_ff',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {

												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboSexoFornecedores',
													name: 'sexo',
													allowBlank: true,
													loadDisabled: true,
													id: 'sexo_fornecedores',
													button_id: 'button_sexo_fornecedores',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Sexo',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_sexo_fornecedores',
													combo_id: 'sexo_fornecedores',
													action: 'reset_combo'
												}
											]
										},
										{
											xtype: 'textfield',
											name: 'cpf',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cpf_fornecedores',
											anchor: '100%',
											mask: '999.999.999-99',
											plugins: 'textmask',
											fieldLabel: 'CPF',
											validator: function(value){
												if(value!="" && !validCPF(value)){
													return 'CPF Inválido...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'textfield',
											name: 'identidade',
											flex: 1,
											allowBlank: true,
											id: 'identidade_fornecedores',
											anchor: '100%',
											fieldLabel: 'Identidade'
										}
										
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProfissaoFornecedores',
											margin: '0 5 0 0',
											hidden: true,
											allowBlank: true,
											flex: 1,
											triggerAction:'all',
											typeAhead:false,
											mode:'remote',
											minChars:2,
											forceSelection: false,
											hideTrigger:true,
											queryMode: 'remote',
											anchor: '100%',
											name: 'profissao',
											id: 'profissao_fornecedores',
											fieldLabel: 'Profissão', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'ie',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'ie_fornecedores',
											anchor: '100%',	
											fieldLabel: 'Insc. Estadual'
										},							
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Estados',
													name: 'estado',
													allowBlank: true,
													id: 'estado_fornecedores',
													button_id: 'button_estado_fornecedores',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Estado',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_estado_fornecedores',
													combo_id: 'estado_fornecedores',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Estados',
													action: 'add_win'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Localidades',
													name: 'cidade',
													allowBlank: true,
													loadDisabled: true,
													minChars:2,
													disabled: true,
													id: 'cidade_fornecedores',
													button_id: 'button_cidade_fornecedores',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Cidade',
													//hideTrigger:false,
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_cidade_fornecedores',
													combo_id: 'cidade_fornecedores',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Localidades',
													action: 'add_win'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Enderecos',
													name: 'endereco',
													loadDisabled: true,
													allowBlank: true,
													hideTrigger: false,
													minChars:2,
													queryMode: 'remote',
													disabled: true,
													id: 'endereco_fornecedores',
													button_id: 'button_endereco_fornecedores',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Endereço',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_endereco_fornecedores',
													combo_id: 'endereco_fornecedores',
													handler: function() {
														Ext.getCmp('endereco_fornecedores').setValue('');
														Ext.getCmp('bairro_nome_fornecedores').setValue('');
														Ext.getCmp('bairro_fornecedores').setValue('');
														Ext.getCmp('cep_fornecedores').setValue('');
														Ext.getCmp('ponto_ref_fornecedores').setValue('');
														Ext.getCmp('loteamento_fornecedores').setValue('');
													}
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Enderecos',
													action: 'add_win'
												}
											]
										},	
										{
											xtype: 'textfield',
											name: 'bairro_nome',
											width: 200,
											margin: '0 5 0 0',	
											allowBlank: true,
											readOnly: true,
											id: 'bairro_nome_fornecedores',
											fieldLabel: 'Bairro',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'bairro',
											flex: 1,
											hidden: true,
											allowBlank: true,
											id: 'bairro_fornecedores',
											anchor: '100%',
											fieldLabel: 'Bairro Id'
										},
										{
											xtype: 'textfield',
											name: 'nro_end',
											width: 80,
											allowBlank: true,
											id: 'nro_end_fornecedores',
											fieldLabel: 'Nro End'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'complemento',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'complemento_fornecedores',
											anchor: '100%',
											fieldLabel: 'Complemento',
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'ponto_ref',	
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'ponto_ref_fornecedores',
											anchor: '100%',
											fieldLabel: 'Ponto Ref', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'loteamento',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'loteamento_fornecedores',
											anchor: '100%',
											fieldLabel: 'Loteamento',
											maxLength: 50,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cep',
											margin: '0 5 0 0',
											id: 'cep_fornecedores',
											mask: '99.999-999',
											plugins: 'textmask',
											allowBlank: true, 
											flex: 1,
											anchor: '100%',
											fieldLabel: 'CEP'
										},
										{
											xtype: 'textfield',
											name: 'cx_postal',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cx_postal_fornecedores',
											anchor: '100%',
											fieldLabel: 'Cx Postal',
											maxLength: 10,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboSituacaoCadastralFornecedores',
													name: 'situacao_cadastral',
													loadDisabled: true,
													allowBlank: true,
													id: 'situacao_cadastral_fornecedores',
													button_id: 'button_situacao_cadastral_fornecedores',
													flex: 1,
													value: 'N',
													anchor: '100%', 
													fieldLabel: 'Situação Cadastral'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_situacao_cadastral_fornecedores',
													combo_id: 'situacao_cadastral_fornecedores',
													action: 'reset_combo'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cadastrado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'cadastrado_por_fornecedores',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Cadastro Efetuado por:'
										},
										{
											xtype: 'textfield',
											name: 'alterado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'alterado_por_fornecedores',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Última Alteração Cadastral Efetuada por:'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'cod_fornecedor',
									hidden: true,
									id: 'cod_fornecedor_fornecedores',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'action',
									hidden: true,
									id: 'action_fornecedores',
									value: 'INSERIR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'fornecedores_anotacoeslist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'fornecedores_contatolist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'fornecedores_enderecolist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridlogs_sistemalist',
							id: 'GridFornecedor_Logs_Sistema',
							store: 'StoreFornecedor_Logs_Sistema',
							title: 'Logs do Fornecedor',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_fornecedores',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_fornecedores',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});