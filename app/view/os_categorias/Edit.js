/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_categorias.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addos_categoriaswin',

	id: 'AddOs_CategoriasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Categoria de OS',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormOs_Categorias',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/os_categorias/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_os_categorias',
							anchor: '100%',							
							fieldLabel: 'Descrição',
							maxLength: 100,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboStatusOs_Categorias',
									name: 'status',
									loadDisabled: true,
									id: 'status_os_categorias',
									button_id: 'button_status_os_categorias',
									flex: 1,
									anchor: '100%',								
									fieldLabel: 'Status',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_status_os_categorias',
									combo_id: 'status_os_categorias',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_os_categorias',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_os_categorias',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_os_categorias',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_os_categorias',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});