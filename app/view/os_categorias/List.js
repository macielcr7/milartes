/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_categorias.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.os_categoriaslist',
	requires: [
		'ShSolutions.store.StoreOs_Categorias'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'os_categorias',

	id: 'List-Os_Categorias',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Categorias de OS',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridOs_Categorias',
					store: 'StoreOs_Categorias',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					forceFit: true,
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'Id',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'status',
							text: 'Status',
							renderer: function(v){
								switch(v){
									case 'ATIVO':
									return 'ATIVO';
									break;
									case 'SUSPENSO':
									return 'SUSPENSO';
									break;
								}
							},
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'data_cadastro2',
							text: 'Data Cadastro',
							width: 100
						}
				
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreOs_Categorias',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_os_categorias',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_os_categorias',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_os_categorias',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});