/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridorcamentoslist',
	requires: [
		'ShSolutions.store.StoreOrcamentos'
	],
	id: 'GridOrcamentos',
	store: 'StoreOrcamentos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false,
				getRowClass: function(record) { 
					if(record.get('convertido_venda')=='S'){
						return 'laranja';
					}
					else{
						return '';
					}
				}
			},
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'id_orcamento',
					text: 'ID',
					sortable: false,
					fixed: true,
					width: 50
				},
				{
				xtype: 'gridcolumn',
				dataIndex: 'tipo_orcamento',
				text: 'Tipo',
				hidden: true,
				sortable: true,
				width: 120,
				renderer: function(v, r, m){
					if(v=='0'){
						return 'Geral';
					}
					else if(v=='1'){
						return 'Forro';
					}
					else if(v=='2'){
						return 'Persianas';
					}
					else if(v=='3'){
						return 'Pisos';
					}
					else if(v=='4'){
						return 'Rodapés/Sancas';
					}
					else{
						return v;
					}
				}
			},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y',
					renderer : Ext.util.Format.dateRenderer('d/m/Y'),
					text: 'Data Cadastro',
					hidden: false,
					sortable: false,
					fixed: true,
					width: 90
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_venda',
					hidden: true,
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Venda',
					sortable: false,
					fixed: true,
					width: 125
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cliente',
					text: 'Cliente',
					sortable: false,
					fixed: true,
					width: 640
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cliente_descricao',
					text: 'Cliente Descrição',
					sortable: false,
					fixed: true,
					width: 300
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_total',
					text: 'Valor Total',
					width: 100,
					sortable: false,
					fixed: true,
					renderer: function(v, r, m){
						var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
						if(v > 0){
							return '<font color=\"black\"><b>'+value+'</b></font>';
						} else {
							return value;
						}
					}
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreOrcamentos',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_orcamentos',
							text: 'Adicionar',
							iconCls: 'bt_add',
							action: 'adicionar',
							menu: {
								xtype: 'menu',
								width: 220,
								items: [
									{
										xtype: 'menuitem',
										action: 'geral',
										text: 'Geral'
									},
									{
										xtype: 'menuitem',
										action: 'forro',
										text: 'Forros / Divisórias (PVC)'
									},
									{
										xtype: 'menuitem',
										action: 'rodape',
										text: 'Rodapés, Sancas, Vistas, Faixas'
									},
									{
										xtype: 'menuitem',
										action: 'piso',
										text: 'Pisos (Laminado/Vinílico)'
									}
								]
							}
						},
						{
							xtype: 'button',
							id: 'button_edit_orcamentos',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_orcamentos',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						},
						{
							xtype: 'button',
							id: 'button_copiar_orcamentos',
							iconCls: 'bt_copiar',
							hidden: true,
							disabled: true,
							action: 'copiar_orcamento',
							text: 'Gera Cópia'
						},
						{
							xtype: 'button',
							id: 'button_pdf_orcamentos',
							iconCls: 'bt_pdf',
							action: 'gerar_pdf',
							hidden: true,
							text: 'Gerar PDF'
						},
						{
							xtype: 'button',
							id: 'button_pagamento_orcamentos',
							iconCls: 'bt_totalizar_periodos_caixas',
							hidden: true,
							disabled: true,
							action: 'efetua_pagamento',
							text: 'Pagamentos'
						},
						{
							xtype: 'button',
							id: 'button_imprimir_orcamentos',
							text: 'Imprimir',
							iconCls: 'bt_imprimir2',
							disabled: true,
							action: 'imprimir_orcamento',
							menu: {
								xtype: 'menu',
								width: 220,
								items: [
									{
										xtype: 'menuitem',
										action: 'impressao_cliente',
										text: 'Versão Para Cliente'
									},
									{
										xtype: 'menuitem',
										action: 'impressao_deposito',
										text: 'Versão para Depósito/Corte'
									}
								]
							}
						},
						{ 
							xtype: 'tbfill' 
						},
						{
							xtype: 'button',
							id: 'button_converte1_orcamentos',
							iconCls: 'converte_venda',
							hidden: false,
							disabled: true,
							action: 'converte_em_venda',
							text: 'Converte em Venda'
						}/*,
						{
							xtype: 'button',
							id: 'button_filter_atributos',
							iconCls: 'bt_lupa',
							action: 'filtrar',
							text: 'Filtrar'
						}*/
					]
				}
			]
		});
		me.callParent(arguments);
	}
});