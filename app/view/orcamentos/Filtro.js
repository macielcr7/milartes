/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filterorcamentoswin',

    id: 'FilterOrcamentosWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro de Orcamentos',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterOrcamentos',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'datefield',
									format: 'd/m/Y',									
									margin: '0 5 0 0',									
									flex: 1,								
									name: 'data_saida',
									id: 'data_saida_filter_orcamentos',
									anchor: '100%',
									fieldLabel: 'Data Saida'
								},								
								{
									xtype: 'numberfield',
									name: 'id_cliente',								    								    
								    flex: 1,
									id: 'id_cliente_filter_orcamentos',
									anchor: '100%',
									fieldLabel: 'Cliente'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'id_vendedor',								    
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'id_vendedor_filter_orcamentos',
									anchor: '100%',
									fieldLabel: 'Vendedor'
								},								
								{
									xtype: 'textfield',
									name: 'cliente_descricao',								    								    
								    flex: 1,
									id: 'cliente_descricao_filter_orcamentos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Cliente Descrição'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'desconto',								    
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'desconto_filter_orcamentos',
									anchor: '100%',
									fieldLabel: 'Desconto'
								},								
								{
									xtype: 'textfield',
									name: 'acrescimo',								    								    
								    flex: 1,
									id: 'acrescimo_filter_orcamentos',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Acrescimo'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'valor_total',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'valor_total_filter_orcamentos',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Valor Total'
								},								
								{
									xtype: 'textfield',
									name: 'aos_cuidados_de',								    								    
								    flex: 1,
									id: 'aos_cuidados_de_filter_orcamentos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Aos Cuidados De'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'observacoes',								    								    
								    flex: 1,
									id: 'observacoes_filter_orcamentos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Observações'
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_orcamentos',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
