/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.orcamentos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.orcamentoslist',
	requires: [
		'ShSolutions.store.StoreOrcamentos'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'orcamentos',

	id: 'List-Orcamentos',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 550,
	title: 'Orçamentos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridorcamentoslist'
				}
			]
		});

		me.on('close', function(){
			Ext.getCmp('GridOrcamentos').store.sort('id_orcamento', 'DESC');
		});

		me.callParent(arguments);
	}
});