/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
if (screen.width >= 1110)
{
	var tamanho = 1110;
	var tamanho2 = tamanho-20;
} else {
	var tamanho = 1000;
	var tamanho2 = tamanho-20;
}

Ext.define('ShSolutions.view.orcamentos.Pagamentos', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addorcamentoswin',

	id: 'AddVendasPagamentosWin',
	layout: {
		type: 'fit'
	},
	maximizable: true,
	minimizable: true,
	closable: false,
	height: 600,
	width: tamanho,
	title: 'Pagamento da Venda',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormVendas',
					bodyPadding: 5,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/orcamentosXXX/save.php',
					items: [
						{
							region: 'north',
							width: tamanho2,
							height: 280,
							border: 0,
							items: [
								{
									xtype: 'form',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													margin: '0 5 0 0',
													id: 'Xfieldcontainer_id_orcamentista_orcamentos_forro',
													layout: {
														align: 'stretch',
														type: 'hbox'
													},
													items: [

													]
												},

												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													flex: 1,
													margin: '0 5 0 0',
													id: 'Xfieldcontainer_id_cliente_orcamentos_forro',
													layout: {
														align: 'stretch',
														type: 'hbox'
													},
													items: [

													]
												}
											]
										},
										{
											xtype: 'fieldset',
											title: 'Produtos',
											items: [
												{
													//xtype: 'gridprodutos_orcamentolist',
													loadDisabled: false,
													height: 150
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'valor_bruto',
													margin: '0 5 0 0',
													flex: 1,
													value: 0,
													id: 'valor_bruto_vendas',
													anchor: '100%',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													readOnly: true,
													fieldStyle: 'background-color: #FFFFCC; background-image: none;',
													fieldLabel: 'Sub-Total'
												},
												{
													xtype: 'textfield',
													name: 'acrescimo_real',
													margin: '0 5 0 0',
													flex: 1,
													value: 0,
													allowBlank: true,
													id: 'acrescimo_real_vendas',
													anchor: '100%',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													fieldLabel: 'Acrescimo (R$)'
												},
												{
													xtype: 'numberfield',
													name: 'acrescimo_porcento',
													margin: '0 5 0 0',
													flex: 1,
													value: 0,
													minValue: 0,
													enableKeyEvents: true,
													forcePrecision: true,
													decimalPrecision: 20,
													allowBlank: true,
													id: 'acrescimo_porcento_vendas',
													anchor: '100%',
													fieldLabel: 'Acrescimo (%)'
												},
												{
													xtype: 'textfield',
													name: 'desconto_real',
													flex: 1,
													value: 0,
													allowBlank: true,
													margin: '0 5 0 0',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													id: 'Xdesconto_real_vendas',
													anchor: '100%',
													fieldLabel: 'Desconto (R$)'
												},
												{
													xtype: 'numberfield',
													name: 'desconto_porcento',
													margin: '0 5 0 0',
													flex: 1,
													minValue: 0,
													value: 0,
													allowBlank: true,
													enableKeyEvents: true,
													forcePrecision: true,
													decimalPrecision: 20,
													id: 'desconto_porcento_vendas',
													anchor: '100%',
													fieldLabel: 'Desconto (%)'
												},
												{
													xtype: 'textfield',
													name: 'valor_total',
													flex: 1,
													value: 0,
													id: 'valor_total_vendas',
													anchor: '100%',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													fieldStyle: 'background-color: #FFFF99; background-image: none;',
													fieldLabel: 'Valor Total'
												}
											]
										}
									]
								}
							]
						},
						{
							region: 'south',
							width: tamanho2,
							height: 250,
							layout: 'accordion',
							items: [
								{
									xtype: 'form',
									title: '<b>Endereço para Entrega<b>',
									bodyPadding: 5,
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'aos_cuidados_de',
													flex: 1,
													margin: '0 5 0 0',
													id: 'Xaos_cuidados_de_orcamentos_forro',
													anchor: '100%',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Aos Cuidados De'
												}
											]
										}
									]
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id_orcamento',
							hidden: true,
							id: 'Xid_orcamento_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'total_de_itens',
							hidden: true,
							id: 'Xtotal_de_itens_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'Xaction_orcamentos_forro',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'Xbutton_resetar_orcamentos_forro',
							iconCls: 'bt_cancel',
							action: 'resetar',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'Xbutton_salvar_orcamentos_forro',
							iconCls: 'bt_save',
							disabled: true,
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});
