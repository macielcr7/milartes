/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos_categoria.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.atributos_categorialist',
    requires: [
    	'ShSolutions.store.StoreAtributos_Categoria'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'atributos_categoria',

    id: 'List-Atributos_Categoria',
    layout: {
        type: 'fit'
    },
    height: 350,
    title: 'Listagem de Atributos_Categoria',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'gridpanel',
    		    	id: 'GridAtributos_Categoria',
    		        store: 'StoreAtributos_Categoria',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					forceFit: true,			
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_atributos_categoria',
							hidden: true,
							format: '0',
							text: 'Id Atributos Categoria',					
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'atributo',
							text: 'Atributo',					
							width: 140
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_atributo',
							hidden: true,
							format: '0',
							text: 'Atributo',					
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Categoria',					
							width: 140
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_categoria',
							hidden: true,
							format: '0',
							text: 'Categoria',					
							width: 140
						}
                
					],
    	            dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreAtributos_Categoria',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_atributos_categoria',
									iconCls: 'bt_add',									
									hidden: true,									
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_atributos_categoria',
									iconCls: 'bt_edit',									
									hidden: true,									
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_atributos_categoria',
									iconCls: 'bt_del',									
									hidden: true,									
									action: 'deletar',
									text: 'Deletar'
								},
								{
									xtype: 'button',
									id: 'button_filter_atributos_categoria',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}								
							]
						}
					]
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


