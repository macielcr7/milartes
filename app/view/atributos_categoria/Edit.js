/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos_categoria.Edit', {
    extend: 'Ext.window.Window',
	alias: 'widget.addatributos_categoriawin',

    id: 'AddAtributos_CategoriaWin',
    layout: {
        type: 'fit'
    },
	maximizable: false,
    minimizable: true,
	
    title: 'Cadastro de Atributos_Categoria',

    initComponent: function() {
        var me = this;


        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'FormAtributos_Categoria',
                    bodyPadding: 10,
                    autoScroll: true,
                    method: 'POST',
                    url : 'server/modulos/atributos_categoria/save.php',
                    items: [
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboAtributos',
                                    name: 'id_atributo',
									id: 'id_atributo_atributos_categoria',
									button_id: 'button_id_atributo_atributos_categoria',
									flex: 1,									
									anchor: '100%',
									fieldLabel: 'Atributo'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_atributo_atributos_categoria',
                                    combo_id: 'id_atributo_atributos_categoria',
                                    action: 'reset_combo'
                                },
                                {
                                    xtype: 'buttonadd',
									tabela: 'Atributos',
                                    action: 'add_win'
                                }
                            ]
                        },
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboProdutos_Categorias',
                                    name: 'id_categoria',
									id: 'id_categoria_atributos_categoria',
									button_id: 'button_id_categoria_atributos_categoria',
									flex: 1,									
									anchor: '100%',
									fieldLabel: 'Categoria'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_categoria_atributos_categoria',
                                    combo_id: 'id_categoria_atributos_categoria',
                                    action: 'reset_combo'
                                },
                                {
                                    xtype: 'buttonadd',
									tabela: 'Produtos_Categorias',
                                    action: 'add_win'
                                }
                            ]
                        },
						{
							xtype: 'hidden',
							name: 'id_atributos_categoria',
							hidden: true,
							id: 'id_atributos_categoria_atributos_categoria',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_atributos_categoria',
							value: 'INSERIR',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            id: 'button_resetar_atributos_categoria',
                            iconCls: 'bt_cancel',
                            action: 'resetar',
                            text: 'Resetar'
                        },
                        {
                            xtype: 'button',
                            id: 'button_salvar_atributos_categoria',
                            iconCls: 'bt_save',
                            action: 'salvar',
                            text: 'Salvar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);

    }

});
