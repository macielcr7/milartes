/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos_categoria.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filteratributos_categoriawin',

    id: 'FilterAtributos_CategoriaWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Atributos_Categoria',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterAtributos_Categoria',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboAtributos',
                                    name: 'id_atributo',
									id: 'id_atributo_filter_atributos_categoria',
									button_id: 'button_id_atributo_filter_atributos_categoria',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Atributo'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_atributo_filter_atributos_categoria',
                                    combo_id: 'id_atributo_filter_atributos_categoria',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboProdutos_Categorias',
                                    name: 'id_categoria',
									id: 'id_categoria_filter_atributos_categoria',
									button_id: 'button_id_categoria_filter_atributos_categoria',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Categoria'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_categoria_filter_atributos_categoria',
                                    combo_id: 'id_categoria_filter_atributos_categoria',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_atributos_categoria',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
