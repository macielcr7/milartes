/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addveiculoswin',
	requires: [
		'ShSolutions.store.StoreLogs_Sistema'
	],

	id: 'AddVeiculosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Veículos',
	width: 800,

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralVeiculos',
					items: [
						{
							xtype: 'form',
							title:'Formulário Principal',
							id: 'FormVeiculos',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/veiculos/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'descricao',
											margin: '0 5 0 0',
											flex: 1,
											id: 'descricao_veiculos',
											anchor: '100%',
											fieldLabel: 'Descrição',
											fieldStyle: 'text-transform:uppercase',
											listeners: {
												afterrender: function(fld) {
													fld.focus(false, 2000);
												}
											}
										},
										{
											xtype: 'combobox',
											name: 'grupo',
											store: 'StoreComboGrupoVeiculos',
											loadDisabled: true,
											margin: '0 5 0 0',
											flex: 1,
											id: 'grupo_veiculos',
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Grupo'
										},
										{
											xtype: 'textfield',
											name: 'renavam',
											width: 100,
											margin: '0 5 0 0',
											id: 'renavam_veiculos',
											anchor: '100%',
											fieldLabel: 'Renavam',
											maxLength: 20,
											enforceMaxLength: true,
											plugins: 'textmask',
											mask: '99999999999',
											money: false,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'placa',
											width: 80,
											margin: '0 5 0 0',
											id: 'placa_veiculos',
											anchor: '100%',
											fieldLabel: 'Placa',
											maxLength: 8,
											enforceMaxLength: true,
											plugins: 'textmask',
											mask: 'AAA-9999',
											money: false,
											returnWithMask: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'combobox',
											width: 80,
											store: 'StoreComboAtivoVeiculos',
											name: 'ativo',
											loadDisabled: true,
											id: 'ativo_veiculos',
											button_id: 'button_ativo_veiculos',
											value: 'S',
											anchor: '100%',
											allowBlank: true,
											fieldLabel: 'Ativo',
											fieldStyle: 'text-transform:uppercase'
										},
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboUnidades_Medidas',
											name: 'unidade_medida',
											id: 'unidade_medida_veiculos',
											width: 165,
											anchor: '100%',
											fieldLabel: 'Unidade Medida',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											margin: '0 5 0 0',
											tabela: 'Unidades_Medidas',
											action: 'add_win'
										},
										{
											xtype: 'textfield',
											name: 'hodometro_inicial',
											width: 175,
											mask: '#990.000',
											plugins: 'textmask',
											money: true,
											margin: '0 5 0 0',
											id: 'hodometro_inicial_veiculos',
											anchor: '100%',
											fieldLabel: 'Hodômetro Inicial'
										},
										{
											xtype: 'textfield',
											name: 'capacidade_transporte',
											margin: '0 5 0 0',
											width: 270,
											mask: '#990.000',
											plugins: 'textmask',
											money: true,
											id: 'capacidade_transporte_veiculos',
											anchor: '100%',
											allowBlank: true,
											fieldLabel: 'Capacidade de Transporte (KG)'
										},
										{
											xtype: 'textfield',
											name: 'tara',
											width: 105,
											mask: '#990.000',
											plugins: 'textmask',
											money: true,
											id: 'tara_veiculos',
											anchor: '100%',
											allowBlank: true,
											fieldLabel: 'Tara'
										}
									]
								},
								{
									xtype: 'fieldset',
									anchor: '100%',
									collapsible: true,
									autoScroll: true,
									layout: 'anchor',
									title: 'Combustível Principal',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													name: 'combustivel1',
													store: 'StoreComboCombustivelVeiculos',
													loadDisabled: true,
													margin: '0 5 0 0',
													flex: 1,
													id: 'combustivel1_veiculos',
													anchor: '100%',
													fieldLabel: 'Descrição',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'capacidade1',
													margin: '0 5 0 0',
													flex: 1,
													id: 'capacidade1_veiculos',
													anchor: '100%',
													allowBlank: true,
													fieldLabel: 'Capacidade (L)',
													mask: '999.999.990,00',
													money: true,
													plugins: 'textmask'
												},
												{
													xtype: 'textfield',
													name: 'consumo1',
													flex: 1,
													id: 'consumo1_veiculos',
													anchor: '100%',
													allowBlank: true,
													fieldLabel: 'Consumo (KM/L)',
													mask: '999.999.990,00',
													money: true,
													plugins: 'textmask'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldset',
									anchor: '100%',
									collapsible: true,
									autoScroll: true,
									layout: 'anchor',
									title: 'Combustível 2',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													name: 'combustivel2',
													store: 'StoreComboCombustivelVeiculos',
													loadDisabled: true,
													margin: '0 5 0 0',
													flex: 1,
													allowBlank: true,
													id: 'combustivel2_veiculos',
													anchor: '100%',
													fieldLabel: 'Descrição',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'capacidade2',
													margin: '0 5 0 0',
													flex: 1,
													id: 'capacidade2_veiculos',
													anchor: '100%',
													allowBlank: true,
													fieldLabel: 'Capacidade (L)',
													mask: '999.999.990,00',
													money: true,
													plugins: 'textmask'
												},
												{
													xtype: 'textfield',
													name: 'consumo2',
													flex: 1,
													id: 'consumo2_veiculos',
													anchor: '100%',
													allowBlank: true,
													fieldLabel: 'Consumo (KM/L)',
													mask: '999.999.990,00',
													money: true,
													plugins: 'textmask'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldset',
									anchor: '100%',
									collapsible: true,
									autoScroll: true,
									layout: 'anchor',
									title: 'Combustível 3',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													name: 'combustivel3',
													store: 'StoreComboCombustivelVeiculos',
													loadDisabled: true,
													margin: '0 5 0 0',
													flex: 1,
													allowBlank: true,
													id: 'combustivel3_veiculos',
													anchor: '100%',
													fieldLabel: 'Descrição',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'capacidade3',
													margin: '0 5 0 0',
													flex: 1,
													id: 'capacidade3_veiculos',
													anchor: '100%',
													allowBlank: true,
													fieldLabel: 'Capacidade (L)',
													mask: '999.999.990,00',
													money: true,
													plugins: 'textmask'
												},
												{
													xtype: 'textfield',
													name: 'consumo3',
													flex: 1,
													id: 'consumo3_veiculos',
													anchor: '100%',
													allowBlank: true,
													fieldLabel: 'Consumo (KM/L)',
													mask: '999.999.990,00',
													money: true,
													plugins: 'textmask'
												}
											]
										}
									]
								},
								{
									xtype: 'hidden',
									name: 'id',
									hidden: true,
									id: 'id_veiculos',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'hidden',
									name: 'action',
									hidden: true,
									id: 'action_veiculos',
									value: 'INSERIR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'gridveiculos_combustivellist',
							title: 'Abastecimentos',
							iconCls: 'veiculos_combustivel',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridveiculos_manutencoeslist',
							title: 'Manutenções do Veículo',
							iconCls: 'veiculos_manutencoes',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridlogs_sistemalist',
							id: 'GridVeiculos_Logs_Sistema',
							store: 'StoreVeiculos_Logs_Sistema',
							title: 'Logs do Veículo',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_veiculos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_veiculos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});