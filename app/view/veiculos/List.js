/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.veiculos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.veiculoslist',
	requires: [
		'ShSolutions.store.StoreVeiculos'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'veiculos',
	id: 'List-Veiculos',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 450,
	title: 'Veículos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridveiculoslist'
				}
			]
		});
		me.callParent(arguments);
	}
});