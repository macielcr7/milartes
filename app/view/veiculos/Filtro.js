/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filterveiculoswin',

	id: 'FilterVeiculosWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	title: 'Filtro de Veiculos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterVeiculos',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Descrição'
								},
								{
									xtype: 'textfield',
									name: 'grupo',
									flex: 1,
									id: 'grupo_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Grupo'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'placa',
									margin: '0 5 0 0',
									flex: 1,
									id: 'placa_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Placa'
								},
								{
									xtype: 'datefield',
									format: 'd/m/Y',
									flex: 1,
									name: 'dt_inicio_controle',
									id: 'dt_inicio_controle_filter_veiculos',
									anchor: '100%',
									fieldLabel: 'Início do Controle'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboUnidades_Medidas',
											name: 'unidade_medida',
											id: 'unidade_medida_filter_veiculos',
											button_id: 'button_unidade_medida_filter_veiculos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Unidade Medida'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_unidade_medida_filter_veiculos',
											combo_id: 'unidade_medida_filter_veiculos',
											action: 'reset_combo'
										},
									]
								},
								{
									xtype: 'numberfield',
									name: 'hodometro_inicial',
									flex: 1,
									id: 'hodometro_inicial_filter_veiculos',
									anchor: '100%',
									fieldLabel: 'Hodômetro Inicial'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'combustivel1',
									margin: '0 5 0 0',
									flex: 1,
									id: 'combustivel1_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Combustível Principal'
								},
								{
									xtype: 'textfield',
									name: 'capacidade1',
									flex: 1,
									id: 'capacidade1_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Capacidade'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'consumo1',
									margin: '0 5 0 0',
									flex: 1,
									id: 'consumo1_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Consumo'
								},
								{
									xtype: 'textfield',
									name: 'combustivel2',
									flex: 1,
									id: 'combustivel2_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Combustível 2'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'capacidade2',
									margin: '0 5 0 0',
									flex: 1,
									id: 'capacidade2_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Capacidade'
								},
								{
									xtype: 'textfield',
									name: 'consumo2',
									flex: 1,
									id: 'consumo2_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Consumo'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'combustivel3',
									margin: '0 5 0 0',
									flex: 1,
									id: 'combustivel3_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Combustível'
								},
								{
									xtype: 'textfield',
									name: 'capacidade3',
									flex: 1,
									id: 'capacidade3_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Capacidade'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'consumo3',
									margin: '0 5 0 0',
									flex: 1,
									id: 'consumo3_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Consumo'
								},
								{
									xtype: 'textfield',
									name: 'capacidade_transporte',
									flex: 1,
									id: 'capacidade_transporte_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Capacidade de Transporte (KG)'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'tara',
									margin: '0 5 0 0',
									flex: 1,
									id: 'tara_filter_veiculos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Tara'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivoVeiculos',
											name: 'ativo',
											loadDisabled: true,
											id: 'ativo_filter_veiculos',
											button_id: 'button_ativo_filter_veiculos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Ativo'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_filter_veiculos',
											combo_id: 'ativo_filter_veiculos',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									margin: '0 5 0 0',
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',
									fieldLabel: 'Data de Cadastro',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cadastro_date_filter_veiculos',
											name: 'data_cadastro_date',
											margins: '0 5 0 0',

											allowBlank: true,
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro_time_filter_veiculos',
											name: 'data_cadastro_time',

											allowBlank: true,
											hideLabel: true
										}
									]
								},

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',
									fieldLabel: 'Cadastrado Por',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'cadastrado_por_date_filter_veiculos',
											name: 'cadastrado_por_date',
											margins: '0 5 0 0',

											allowBlank: true,
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'cadastrado_por_time_filter_veiculos',
											name: 'cadastrado_por_time',

											allowBlank: true,
											hideLabel: true
										}
									]
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'alterado_por',
									margin: '0 5 0 0',
									flex: 1,
									id: 'alterado_por_filter_veiculos',
									anchor: '100%',
									fieldLabel: 'Alterado Por'
								},

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',
									fieldLabel: 'Data Alteracao',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_alteracao_date_filter_veiculos',
											name: 'data_alteracao_date',
											margins: '0 5 0 0',

											allowBlank: true,
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_alteracao_time_filter_veiculos',
											name: 'data_alteracao_time',

											allowBlank: true,
											hideLabel: true
										}
									]
								}

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_veiculos',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Resetar Filtro'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});