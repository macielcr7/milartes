/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.veiculos.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridveiculoslist',
	requires: [
		'ShSolutions.store.StoreVeiculos'
	],
	id: 'GridVeiculos',
	store: 'StoreVeiculos',
	height: 350,

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'id',
					hidden: true,
					format: '0',
					text: 'Id',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'descricao',
					text: 'Descrição',
					width: 170
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'grupo',
					text: 'Grupo',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'placa',
					text: 'Placa',
					width: 80
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'renavam',
					text: 'Renavam',
					width: 100
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'dt_inicio_controle',
					format: 'd/m/Y',
					renderer : Ext.util.Format.dateRenderer('d/m/Y'),
					text: 'Início do Controle',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'hodometro_inicial',
					format: '0',
					text: 'Hodômetro Inicial',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'combustivel1',
					text: 'Combustível Principal',
					width: 130
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ativo',
					text: 'Ativo',
					renderer: function(v){
						switch(v){
							case 'S':
							return 'SIM';
							break;
							case 'N':
							return 'NÃO';
							break;
						}
					},
					width: 60
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data de Cadastro',
					width: 130
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cadastrado_por_nome',
					text: 'Cadastrado Por',
					width: 190
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreVeiculos',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_veiculos',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_veiculos',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_veiculos',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Desativar'
						},
						{
							xtype: 'button',
							iconCls: 'veiculos_combustivel',
							id: 'button_abastecimentos_veiculos',
							hidden: true,
							disabled: true,
							action: 'abastecimentos',
							text: 'Abastecimentos'
						},
						{
							xtype: 'button',
							iconCls: 'veiculos_manutencoes',
							id: 'button_manutecoes_veiculos',
							hidden: true,
							disabled: true,
							action: 'manutencoes',
							text: 'Manutenções'
						},
						{ 
							xtype: 'tbfill'
						},
						{
							text:'Filtrar Status',
							iconCls: 'bt_status',
							menu: {
								items: [
									{
										text: 'Todos',
										checked: false,
										value: 'X',
										group: 'status',
										action: 'filtrar_status'
									},
									{
										text: 'Ativos',
										checked: true,
										value: 'S',
										group: 'status',
										action: 'filtrar_status'
									},
									{
										text: 'Desativados',
										checked: false,
										value: 'N',
										group: 'status',
										action: 'filtrar_status'
									}
								]
							}
						},
						{
							xtype: 'button',
							id: 'button_filter_veiculos',
							iconCls: 'bt_lupa',
							action: 'filtrar',
							text: 'Filtrar'
						}
					]
				}
			]
			







		});

		me.callParent(arguments);
	}
});