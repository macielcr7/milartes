/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.empresas_sistema.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addempresas_sistemawin',

	id: 'AddEmpresas_SistemaWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Empresas Sistema',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormEmpresas_Sistema',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/empresas_sistema/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'razao_social',
									margin: '0 5 0 0',
									flex: 1,
									id: 'razao_social_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Razão Social',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'nome_fantasia',
									flex: 1,
									id: 'nome_fantasia_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Nome Fantasia'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cnpj',
									margin: '0 5 0 0',
									flex: 1,
									id: 'cnpj_empresas_sistema',
									anchor: '100%',
									mask: '99.999.999/9999-99',
									plugins: 'textmask',
									fieldLabel: 'Cnpj'
								},
								{
									xtype: 'textfield',
									name: 'pessoa_contato',
									flex: 1,
									id: 'pessoa_contato_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Contato'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'ramo_atividade',
									margin: '0 5 0 0',
									flex: 1,
									id: 'ramo_atividade_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Ramo Atividade'
								},
								{
									xtype: 'textfield',
									name: 'endereco',
									flex: 1,
									id: 'endereco_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Endereço'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'num_end',
									margin: '0 5 0 0',
									flex: 1,
									id: 'num_end_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Num End'
								},
								{
									xtype: 'textfield',
									allowBlank: true,
									name: 'complemento',
									flex: 1,
									id: 'complemento_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Complemento'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'bairro',
									margin: '0 5 0 0',
									flex: 1,
									id: 'bairro_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Bairro'
								},
								{
									xtype: 'textfield',
									name: 'loteamento',
									allowBlank: true,
									flex: 1,
									id: 'loteamento_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Loteamento'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cidade',
									margin: '0 5 0 0',
									flex: 1,
									id: 'cidade_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Cidade'
								},
								{
									xtype: 'textfield',
									name: 'estado',
									flex: 1,
									id: 'estado_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Estado',
									maxLength: 2,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cep',
									margin: '0 5 0 0',
									flex: 1,
									id: 'cep_empresas_sistema',
									anchor: '100%',
									mask: '99.999-999',
									plugins: 'textmask',
									fieldLabel: 'Cep'
								},
								{
									xtype: 'textfield',
									name: 'email',
									allowBlank: true,
									flex: 1,
									id: 'email_empresas_sistema',
									anchor: '100%',		
									validator: function(value){
										if(value!="" && !isEmail(value)){
											return 'E-mail Inválido...';
										}
										else{
											return true;
										}
									},
									fieldLabel: 'Email'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'site',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'site_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Site'
								},
								{
									xtype: 'textfield',
									name: 'fone1',
									mask: '(99) 99999-9999',
									plugins: 'textmask',
									flex: 1,
									id: 'fone1_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Fone1',
									listeners: {
										focus: function(field){
											field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
										},
										blur: function(field, eventObject){
											var value = field.getValue();

											if(value.length <= 10){
												field.setMask('(99) 9999-9999');
											}else{
												field.setMask('(99) 99999-9999');
											}
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone2',
									mask: '(99) 99999-9999',
									plugins: 'textmask',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone2_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Fone2',
									listeners: {
										focus: function(field){
											field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
										},
										blur: function(field, eventObject){
											var value = field.getValue();

											if(value.length <= 10){
												field.setMask('(99) 9999-9999');
											}else{
												field.setMask('(99) 99999-9999');
											}
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'fone3',
									mask: '(99) 99999-9999',
									plugins: 'textmask',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone3_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Fone3',
									listeners: {
										focus: function(field){
											field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
										},
										blur: function(field, eventObject){
											var value = field.getValue();

											if(value.length <= 10){
												field.setMask('(99) 9999-9999');
											}else{
												field.setMask('(99) 99999-9999');
											}
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'fone4',
									mask: '(99) 99999-9999',
									plugins: 'textmask',
									allowBlank: true,
									flex: 1,
									id: 'fone4_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Fone4',
									listeners: {
										focus: function(field){
											field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
										},
										blur: function(field, eventObject){
											var value = field.getValue();

											if(value.length <= 10){
												field.setMask('(99) 9999-9999');
											}else{
												field.setMask('(99) 99999-9999');
											}
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									height: 60,
									name: 'observacoes',
									allowBlank: true,
									flex: 1,
									id: 'observacoes_empresas_sistema',
									anchor: '100%',
									fieldLabel: 'Observações'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'cod_empresa',
							hidden: true,
							id: 'cod_empresa_empresas_sistema',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_empresas_sistema',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_empresas_sistema',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_empresas_sistema',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});