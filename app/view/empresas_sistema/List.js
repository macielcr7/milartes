/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.empresas_sistema.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.empresas_sistemalist',
	requires: [
		'ShSolutions.store.StoreEmpresas_Sistema'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'empresas_sistema',

	id: 'List-Empresas_Sistema',
	layout: {
		type: 'fit'
	},
	width: 1050,
	height: 400,
	title: 'Empresas do Sistema',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridEmpresas_Sistema',
					store: 'StoreEmpresas_Sistema',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
								
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_empresa',
							format: '0',
							text: 'Cod Empresa',
							width: 90
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'razao_social',
							text: 'Razão Social',
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_fantasia',
							text: 'Nome Fantasia',
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cnpj',
							renderer : Ext.util.Format.maskRenderer('99.999.999/9999-99'),
							text: 'CNPJ',
							width: 120
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'pessoa_contato',
							text: 'Contato',
							width: 140
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cadastro',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Cadastro',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Empresa Ativa',
							width: 140,
							renderer: function(v, r, m){
								if(v=='N'){
									return '<font color=\"red\">Não</font>';
								}
								else if(v=='S'){
									return '<font color=\"green\">Sim</font>';
								}
								else{
									return v;
								}
							}
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreEmpresas_Sistema',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_empresas_sistema',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_empresas_sistema',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_empresas_sistema',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar Empresa'
								},
								{
									xtype: 'button',
									id: 'button_filter_empresas_sistema',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});