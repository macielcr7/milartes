/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.empresas_sistema.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filterempresas_sistemawin',

    id: 'FilterEmpresas_SistemaWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro de Empresas Sistema',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterEmpresas_Sistema',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'razao_social',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'razao_social_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Razão Social',
		                            listeners: {
		                                afterrender: function(fld) {
		                                    fld.focus(false, 1000);
		                                }
									}
								},								
								{
									xtype: 'textfield',
									name: 'nome_fantasia',								    								    
								    flex: 1,
									id: 'nome_fantasia_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Nome Fantasia'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cnpj',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'cnpj_filter_empresas_sistema',									
									mask: '99.999.999/9999-99',
									plugins: 'textmask',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Cnpj'
								},								
								{
									xtype: 'textfield',
									name: 'pessoa_contato',								    								    
								    flex: 1,
									id: 'pessoa_contato_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Contato'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'ramo_atividade',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'ramo_atividade_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Ramo Atividade'
								},								
								{
									xtype: 'textfield',
									name: 'endereco',								    								    
								    flex: 1,
									id: 'endereco_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Endereço'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'num_end',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'num_end_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Num End'
								},								
								{
									xtype: 'textfield',
									name: 'complemento',								    								    
								    flex: 1,
									id: 'complemento_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Complemento'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'bairro',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'bairro_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Bairro'
								},								
								{
									xtype: 'textfield',
									name: 'loteamento',								    								    
								    flex: 1,
									id: 'loteamento_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Loteamento'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cidade',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'cidade_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Cidade'
								},								
								{
									xtype: 'textfield',
									name: 'estado',								    								    
								    flex: 1,
									id: 'estado_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Estado'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cep',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'cep_filter_empresas_sistema',									
									mask: '99.999-999',
									plugins: 'textmask',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Cep'
								},								
								{
									xtype: 'textfield',
									name: 'email',								    								    
								    flex: 1,
									id: 'email_filter_empresas_sistema',		
									validator: function(value){
										if(!isEmail(value)){
											return 'E-mail Inválido...';
										}
										else{
											return true;
										}
									},																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Email'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'site',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'site_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Site'
								},								
								{
									xtype: 'textfield',
									name: 'fone1',								    								    
								    flex: 1,
									id: 'fone1_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Fone1'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone2',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'fone2_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Fone2'
								},								
								{
									xtype: 'textfield',
									name: 'fone3',								    								    
								    flex: 1,
									id: 'fone3_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Fone3'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone4',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'fone4_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Fone4'
								},								
								{
									xtype: 'textfield',
									name: 'observacoes',								    								    
								    flex: 1,
									id: 'observacoes_filter_empresas_sistema',																											
									allowBlank: true,
									anchor: '100%',
									fieldLabel: 'Observações'
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_empresas_sistema',
							allowBlank: true,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
