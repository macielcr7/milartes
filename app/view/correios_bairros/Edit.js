﻿/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_bairros.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addcorreios_bairroswin',

	id: 'AddCorreios_BairrosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Bairros',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCorreios_Bairros',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/correios_bairros/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,    
									margin: '0 5 0 0',    
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Estados',
											name: 'uf_sigla',
											id: 'uf_sigla_correios_bairros',
											button_id: 'button_uf_sigla_correios_bairros',
											flex: 1,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Uf Sigla'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_uf_sigla_correios_bairros',
											combo_id: 'uf_sigla_correios_bairros',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Estados',
											action: 'add_win'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Localidades',
											name: 'localidade_id',
											id: 'localidade_id_correios_bairros',
											button_id: 'button_localidade_id_correios_bairros',
											flex: 1,
											loadDisabled: true,
											disabled: true,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Localidade'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_localidade_id_correios_bairros',
											combo_id: 'localidade_id_correios_bairros',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Localidades',
											action: 'add_win'
										}
									]
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'bairro_nome',
									margin: '0 5 0 0',
									flex: 1,
									id: 'bairro_nome_correios_bairros',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Bairro Nome'
								},
								{
									xtype: 'textfield',
									name: 'bairro_nome_abreviado',
									flex: 1,
									allowBlank: true,
									id: 'bairro_nome_abreviado_correios_bairros',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Bairro Nome Abreviado'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%',	
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboAtivo',
									name: 'ativo',
									value: 'S',
									loadDisabled: true,
									id: 'ativo_Correios_Bairros',
									button_id: 'button_ativo_Correios_Bairros',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Ativo',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_ativo_Correios_Bairros',
									combo_id: 'ativo_Correios_Bairros',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_correios_bairros',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_correios_bairros',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_correios_bairros',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_correios_bairros',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});