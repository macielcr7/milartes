/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_bairros.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filtercorreios_bairroswin',

    id: 'FilterCorreios_BairrosWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro de Correios_Bairros',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterCorreios_Bairros',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    
								    margin: '0 5 0 0',								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Estados',
		                                    name: 'uf_sigla',
											id: 'uf_sigla_filter_correios_bairros',
											button_id: 'button_uf_sigla_filter_correios_bairros',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Uf Sigla'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_uf_sigla_filter_correios_bairros',
		                                    combo_id: 'uf_sigla_filter_correios_bairros',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        },		                        
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Localidades',
		                                    name: 'localidade_id',
											id: 'localidade_id_filter_correios_bairros',
											button_id: 'button_localidade_id_filter_correios_bairros',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Localidade Id'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_localidade_id_filter_correios_bairros',
		                                    combo_id: 'localidade_id_filter_correios_bairros',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        }		                        

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'bairro_nome',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'bairro_nome_filter_correios_bairros',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Bairro Nome'
								},								
								{
									xtype: 'textfield',
									name: 'bairro_nome_abreviado',								    								    
								    flex: 1,
									id: 'bairro_nome_abreviado_filter_correios_bairros',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Bairro Nome Abreviado'
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_correios_bairros',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
