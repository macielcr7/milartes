/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.fornecedores_contato.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addfornecedores_contatowin',

	id: 'AddFornecedores_ContatoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Contato',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFornecedores_Contato',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/fornecedores_contato/save.php',
					items: [
						{
							xtype: 'hidden',
							name: 'cod_fornecedor',
							id: 'cod_fornecedor_fornecedores_contato',
							anchor: '100%',
							fieldLabel: 'Cod Fornecedor'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboTipo_ContatoFornecedoresContato',
									name: 'tipo_contato',
									loadDisabled: true,
									value: 'TELEFONE',
									id: 'tipo_contato_fornecedores_contato',
									button_id: 'button_tipo_contato_fornecedores_contato',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Tipo Contato'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_tipo_contato_fornecedores_contato',
									combo_id: 'tipo_contato_fornecedores_contato',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_tel_fornecedores_contato',
							anchor: '100%',
							mask: '(99) 99999-9999',
							plugins: 'textmask',
							fieldLabel: 'Descrição',
							listeners: {
								focus: function(field){
									field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
								},
								blur: function(field, eventObject){
									var value = field.getValue();

									if(value.length <= 10){
										field.setMask('(99) 9999-9999');
									}else{
										field.setMask('(99) 99999-9999');
									}
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_tel_ddg_fornecedores_contato',
							anchor: '100%',
							hidden: true,
							disabled: true,
							mask: '9999 999-9999',
							plugins: 'textmask',
							fieldLabel: 'Descrição '
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							hidden: true,
							disabled: true,
							id: 'descricao_fornecedores_contato',
							anchor: '100%',
							fieldLabel: 'Descrição',
							validator: function(value){
								if(value!=""){
									if(Ext.getCmp('tipo_contato_fornecedores_contato').getValue()=='EMAIL'){
										if(!isEmail(value)){
											return 'Email Inválido';
										}
									}
									else if(Ext.getCmp('tipo_contato_fornecedores_contato').getValue()=='SITE'){
										if(!isValidURL(value)){
											return 'URL Inválida Formato Correto: http://www.exemplo.com.br';
										}
									}
								}
								return true;
							}
						},
						{
							xtype: 'textarea',
							name: 'observacao',
							allowBlank: true,
							id: 'observacao_fornecedores_contato',
							anchor: '100%',
							fieldLabel: 'Observação', 
							maxLength: 255,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'hidden',
							name: 'controle',
							hidden: true,
							id: 'controle_fornecedores_contato',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_fornecedores_contato',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_fornecedores_contato',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_fornecedores_contato',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});