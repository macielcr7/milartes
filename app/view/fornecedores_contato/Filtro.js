/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.fornecedores_contato.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filterfornecedores_contatowin',

    id: 'FilterFornecedores_ContatoWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Contato',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterFornecedores_Contato',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'hidden',
							allowDecimals: false,
							name: 'cod_fornecedor',
							id: 'cod_fornecedor_filter_fornecedores_contato',
							anchor: '100%',
							fieldLabel: 'Cod Fornecedor'
						},
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,                              
                            flex: 1,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'StoreComboTipo_ContatoFornecedoresContato',
                                    name: 'tipo_contato',
                                    loadDisabled: true,
                                    id: 'tipo_contato_filter_fornecedores_contato',
                                    button_id: 'button_tipo_contato_filter_fornecedores_contato',
                                    flex: 1,
                                    anchor: '100%',                                         
                                    fieldLabel: 'Tipo Contato'
                                },
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_tipo_contato_filter_fornecedores_contato',
                                    combo_id: 'tipo_contato_filter_fornecedores_contato',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_filter_fornecedores_contato',							
							anchor: '100%',
							fieldLabel: 'Descrição'
						},
						{
							xtype: 'textarea',
							name: 'observacao',
							id: 'observacao_filter_fornecedores_contato',							
							anchor: '100%',
							fieldLabel: 'Observação'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_fornecedores_contato',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
