/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.fornecedores_contato.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.fornecedores_contatolist',
	requires: [
		'ShSolutions.store.StoreFornecedores_Contato'
	],
	
	id: 'GridFornecedores_Contato',
	store: 'StoreFornecedores_Contato',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,
	height: 350,
	title: 'Contatos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'controle',
					hidden: true,
					format: '0',
					text: 'Controle',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_contato',
					text: 'Tipo Contato',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'descricao',
					text: 'Descrição',
					width: 140,
					renderer : function(v, m, r){
						if(r.get('tipo_contato') == 'TELEFONE'){
							return v.mask('(__) ____-____');
						}
						else{
							return v;
						}
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacao',
					text: 'Observação',
					width: 140
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Cadastro',
					width: 140
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreFornecedores_Contato',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_fornecedores_contato',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_fornecedores_contato',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_fornecedores_contato',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});