/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.itens_orcamento.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filteritens_orcamentowin',

    id: 'FilterItens_OrcamentoWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Itens_Orcamento',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterItens_Orcamento',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'numberfield',
							allowDecimals: false,
							name: 'id_produto',
							id: 'id_produto_filter_itens_orcamento',
							anchor: '100%',
							fieldLabel: 'Produto'
						},
						{
							xtype: 'numberfield',
							allowDecimals: false,
							name: 'quantidade',
							id: 'quantidade_filter_itens_orcamento',
							anchor: '100%',
							fieldLabel: 'Quantidade'
						},
						{
							xtype: 'textfield',
							name: 'valor_unitario',
							id: 'valor_unitario_filter_itens_orcamento',							
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,							
							anchor: '100%',
							fieldLabel: 'Valor Unitario'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_itens_orcamento',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
