/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.itens_orcamento.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.itens_orcamentolist',
    requires: [
    	'ShSolutions.store.StoreItens_Orcamento'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'itens_orcamento',

    id: 'List-Itens_Orcamento',
    layout: {
        type: 'fit'
    },
    height: 350,
    title: 'Listagem de Itens_Orcamento',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'griditens_orcamentolist'
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


