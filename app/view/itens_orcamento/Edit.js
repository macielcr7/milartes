/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.itens_orcamento.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.additens_orcamentowin',

	id: 'AddItens_OrcamentoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 600,
	title: 'Itens do Orçamento',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormItens_Orcamento',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/itens_orcamento/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutos',
									name: 'id_produto',
									id: 'id_produto_itens_orcamento',
									button_id: 'button_id_produto_itens_orcamento',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Produto',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(true, 800);
										}
									},
									queryMode: 'remote',
									triggerAction: 'all',
									typeAhead: false,
									minChars: 3/*,
									typeAhead: false,
									
									listConfig: {
										loadingText: 'Procurando...',
										emptyText: 'Nenhum Resultado Encontrado...',

										// Custom rendering template for each item
										getInnerTpl: function() {
											return '<a class="search-item" href="#">' +
												'<h3><span>data<br />by maciel</span>{descricao}</h3>' +
											'</a>';
										}
									},
									pageSize: 20*/
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_id_produto_itens_orcamento',
									combo_id: 'id_produto_itens_orcamento',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutosCores',
									name: 'id_cor',
									loadDisabled: true,
									disabled: true,
									hidden: true,
									id: 'cor_itens_orcamento',
									anchor: '100%',
									fieldLabel: 'Cor'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'quantidade',
									id: 'quantidade_itens_orcamento',
									margin: '0 5 0 0',
									flex: 1,
									anchor: '100%',
									minValue: 0.1,
									selectOnFocus: true,
									fieldLabel: 'Quantidade'
								},
								{
									xtype: 'textfield',
									name: 'valor_unitario',
									id: 'valor_unitario_itens_orcamento',
									flex: 1,
									anchor: '100%',	
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Valor Unitário'
								}
							]
						},
						{
							xtype: 'textarea',
							name: 'observacoes',
							allowBlank: true,
							id: 'observacoes_itens_orcamento',
							anchor: '100%',
							fieldLabel: 'Observações', 
							maxLength: 255,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'hidden',
							name: 'id_itens_orcamento',
							hidden: true,
							id: 'id_itens_orcamento_itens_orcamento',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'item_nro',
							hidden: true,
							value: '0',
							id: 'item_nro_itens_orcamento',
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_itens_orcamento',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_itens_orcamento',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_itens_orcamento').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_itens_orcamento',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});