/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.itens_orcamento.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.griditens_orcamentolist',
	requires: [
		'ShSolutions.store.StoreItens_Orcamento'
	],
	id: 'GridItens_Orcamento',
	store: 'StoreItens_Orcamento',
	height: 200,

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'item_nro',
					text: '#',
					anchor: '100%',
					hidden: false,
					width: 40,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'produto',
					flex: 1,
					anchor: '100%',
					text: 'Produto',
					renderer: function(v, m, r){
						var descricao_produto = v;
						var nome_cor = "";

						if (r.get('cor') != "") {
							nome_cor += ' > <span style="background-color: '+r.get('css_cor')+'; padding:2px"> <b>'+r.get('cor')+'</b></span>';
						}

						return descricao_produto += nome_cor;
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'quantidade',
					text: 'Quantidade',	
					anchor: '100%',
					width: 100,
					renderer: function(v, m, r){
						if(r.get('sigla_um')=='M²' || r.get('sigla_um')=='M' || r.get('sigla_um')=='KM' || r.get('sigla_um')=='L'){
							return converteFloatMoeda(v)+' '+r.get('sigla_um');
						}
						else{
							return v+' '+r.get('sigla_um');
						}
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_unitario',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Unitário',
					anchor: '100%',
					width: 100,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_total',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Total',
					anchor: '100%',
					width: 120,
					sortable: false,
					fixed: true
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_itens_orcamento',
							iconCls: 'bt_add',
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_itens_orcamento',
							iconCls: 'bt_edit',
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_itens_orcamento',
							iconCls: 'bt_del',
							action: 'deletar',
							text: 'Deletar'
						},
						{
							xtype: 'button',
							id: 'button_copiar_produtos_itens_orcamento',
							iconCls: 'bt_copiar',
							//hidden: true,
							action: 'copiar_item_orcamento',
							text: 'Copia o Ítem'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});