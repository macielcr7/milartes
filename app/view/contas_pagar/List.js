/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

if (screen.width >= 1350)
{
	var tamanho = 1350;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.contas_pagar.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.contas_pagarlist',
	requires: [
		'ShSolutions.store.StoreContas_Pagar'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'contas_pagar',
	id: 'List-Contas_Pagar',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 600,
	title: 'Contas à Pagar',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridContas_Pagar',
					store: 'StoreContas_Pagar',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					features: [{
						ftype: 'groupingsummary',
						groupHeaderTpl: '{name}',
						//groupHeaderTpl: '{name}, ({rows.length} Duplicatas{[values.rows.length > 1 ? "s" : ""]})',
						hideGroupedHeader: false,
						enableGroupingMenu: true
					}],

					plugins: [{
			            ptype: 'rowexpander',
			            rowBodyTpl : new Ext.XTemplate(
			                '<div class="container-sub-grid">',
								'<table cellspacing="0">',
							        '<thead>',
							            '<tr style="background:#eeeeee;">',
							                '<th>Tipo Pagamento</th>',
							                '<th>Data Pagamento</th>',
							                '<th>Valor Pago</th>',
							                '<th>Obs.</th>',
							            '</tr>',
							        '</thead>',
							        '<tbody>',
							        	'{body}',
							        '</tbody>',
							    '</table>',
							'</div>'
						)
			        }],

					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'ID',
							width: 30
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'codigo_barras',
							hidden: true,
							text: 'codigo_barras',
							width: 30
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_empresa',
							text: 'Empresa',
							width: 220
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_fornecedor',
							text: 'Fornecedor',
							width: 250,
							summaryType: 'count',
							summaryRenderer: function(value, summaryData, dataIndex) {
								return ((value === 0 || value > 1) ? '(' + value + ' Duplicatas)' : '(1 Duplicata)');
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 290
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'vencimento',
							text: 'Vencimento',
							format: 'd/m/Y',
							renderer : Ext.util.Format.dateRenderer('d/m/Y'),
							width: 100
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'valor_parcela',
							text: 'Valor Duplicata',
							renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
							width: 90,
							summaryType: function(records){
								var i = 0,
								length = records.length,
								total = 0,
								record;

								for (; i < length; ++i) {
									record = records[i];
									total += record.get('valor_parcela');
								}
								return total;
							},
							summaryRenderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true)
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'saldo_a_pagar',
							text: 'Saldo à Pagar',
							renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
							width: 90,
							summaryType: function(records){
								var i = 0,
								length = records.length,
								total = 0,
								record;

								for (; i < length; ++i) {
									record = records[i];
									total += record.get('saldo_a_pagar');
								}
								return total;
							},
							summaryRenderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true)
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'observacoes',
							text: 'Observações',
							width: 150
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 95
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreContas_Pagar',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_contas_pagar',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_contas_pagar',
									iconCls: 'bt_edit',
									action: 'editar',
									hidden: true,
									disabled: true,
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_contas_pagar',
									iconCls: 'bt_del',
									action: 'deletar',
									hidden: true,
									disabled: true,
									text: 'Remover'
								},
								{
									xtype: 'button',
									id: 'button_codigo_barras_contas_pagar',
									iconCls: 'codigo_barras',
									//hidden: true,
									disabled: true,
									action: 'codigo_barras',
									text: 'Pegar Código de Barras'
								},
								{
									xtype: 'button',
									id: 'button_pagar_conta_contas_pagar',
									iconCls: 'bt_pgto_conta',
									hidden: true,
									disabled: true,
									action: 'pagar_conta',
									text: 'Pagar Conta Selecionada'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_contas_pagar',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_contas_pagar').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_contas_pagar',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{
									xtype: 'button',
									id: 'button_diversos_contas_pagar',
									iconCls: 'bt_relatorios',
									text: 'Relatórios',
									menu: [
										{
											text: 'Contas Cadastradas no Período',
											iconCls: 'bt_contas_pagar_rel_periodo',
											id: 'button_relatorios_contas_cadastradas_contas_pagar',
											hidden: true,
											action: 'relatorios_contas_cadastradas'
										},
										{
											text: 'Pagamentos Efetuados no Período',
											iconCls: 'bt_contas_pagar_rel_pagtos',
											id: 'button_relatorios_contas_pagtos_efetuados_contas_pagar',
											hidden: true,
											action: 'relatorios_contas_pagtos_efetuados'
										}
									]
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});