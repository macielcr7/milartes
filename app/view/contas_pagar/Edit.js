/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contas_pagar.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcontas_pagarwin',

	requires: [
		'ShSolutions.store.StoreContas_Pagar_Pagtos'
	],

	id: 'EditContas_PagarWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	height: 440,
	title: 'Alteração da Contas à Pagar',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralContasPagar',
					items: [
						{
							xtype: 'form',
							title:'Formulário Principal',
							id: 'FormContas_Pagar_Edit',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/contas_pagar/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboContasPagarEmpresas',
											name: 'cod_empresa',
											allowBlank: false,
											id: 'cod_empresa_contas_pagar_edit',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Empresa'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboContasPagarFornecedores',
											allowBlank: false,
											flex: 1,
											queryMode:'remote',
											triggerAction:'all',
											typeAhead:false,
											minChars:1,
											anchor: '100%',
											name: 'cod_fornecedor',
											id: 'cod_fornecedor_contas_pagar_edit',
											fieldLabel: 'Fornecedor',
											fieldStyle: 'text-transform:uppercase',
											listeners: {
												afterrender: function(fld) {
													fld.focus(false, 1000);
												}
											}
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'descricao',
											margin: '0 5 0 0',
											allowBlank: false,
											maxLength: 255,
											enforceMaxLength: true,
											flex: 1,
											id: 'descricao_contas_pagar_edit',
											fieldStyle: 'text-transform:uppercase',
											anchor: '100%',
											fieldLabel: 'Descrição'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboContasPagarTipoConta',
											allowBlank: false,
											width: 250,
											triggerAction:'all',
											typeAhead:false,
											mode:'remote',
											minChars:2,
											forceSelection: false,
											//hideTrigger:true,
											queryMode: 'remote',
											anchor: '100%',
											name: 'tipo_conta',
											id: 'tipo_conta_contas_pagar_edit',
											fieldLabel: 'Tipo da Conta/Centro de Custos', 
											maxLength: 50,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'nf_nro',
											allowBlank: true,
											readOnly: true,
											margin: '0 5 0 0',
											id: 'nf_nro_contas_pagar_edit',
											fieldStyle: 'text-transform:uppercase',
											width: 120,
											fieldLabel: 'NF Nro'
										},
										{
											xtype: 'textfield',
											name: 'valor_nf',
											margin: '0 5 0 0',
											allowBlank: false,
											readOnly: true,
											id: 'valor_nf_contas_pagar_edit',
											width: 120,
											mask: '#9.999.990,00',
											money: true,
											plugins: 'textmask',
											fieldLabel: 'Valor NF'
										},
										{
											xtype: 'textfield',
											anchor: '100%',
											margin: '0 5 0 0',
											readOnly: true,
											id: 'parcelas_parcelamento_edit',
											name: 'parcela_nro',
											fieldLabel: 'Nro de Parcelas'
										},
										{
											xtype: 'datefield',
											width: 130,
											format: 'd/m/Y',
											allowBlank: false,
											margin: '0 5 0 0',
											flex: 1,
											name: 'vencimento',
											id: 'vencimento_contas_pagar_edit',
											fieldLabel: 'Vencimento da Parcela'
										},
										{
											xtype: 'textfield',
											name: 'valor_parcela',
											allowBlank: false,
											id: 'valor_parcela_contas_pagar_edit',
											flex: 1,
											mask: '#9.999.990,00',
											money: true,
											plugins: 'textmask',
											fieldLabel: 'Valor da Parcela'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'codigo_barras',
									allowBlank: true,
									id: 'codigo_barras_contas_pagar_edit',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Código de Barras'
								},
								{
									xtype: 'textarea',
									name: 'observacoes',
									allowBlank: true,
									id: 'observacoes_contas_pagar_edit',
									anchor: '100%',
									fieldLabel: 'Observações',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cadastrado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'cadastrado_por_contas_pagar_edit',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Cadastro Efetuado por:'
										},
										{
											xtype: 'textfield',
											name: 'alterado_por_nome',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'alterado_por_contas_pagar_edit',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Última Alteração Efetuada por:'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'id',
									hidden: true,
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'id_contas_pagar_edit',
									value: '',
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'action',
									hidden: true,
									margin: '0 5 0 0',
									id: 'action_contas_pagar_edit',
									value: 'EDITAR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'contas_pagar_pagtoslist',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contas_pagar_edit',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_contas_pagar_edit',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});