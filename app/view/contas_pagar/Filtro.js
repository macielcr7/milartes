/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

var now = new Date();
var now2 = new Date();
var month = now.getMonth();
var mes = now.setMonth(month - 6);

Ext.define('ShSolutions.view.contas_pagar.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filtercontas_pagarwin',

	id: 'FilterContas_PagarWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	
	title: 'Filtrar Contas á Pagar',
	width: 600,
	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterContas_Pagar',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContasPagarEmpresas',
									name: 'cod_empresa',
									allowBlank: true,
									id: 'cod_empresa_filter_contas_pagar',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Empresa'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									allowBlank: true,
									flex: 1,
									id: 'descricao_filter_contas_pagar',
									anchor: '100%',
									fieldLabel: 'Descrição',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContasPagarFornecedores',
									allowBlank: true,
									flex: 1,
									queryMode:'remote',
									triggerAction:'all',
									typeAhead:false,
									minChars:1,
									id: 'fornecedor_filter_contas_pagar',
									anchor: '100%',
									name: 'fornecedor',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Fornecedor'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											mask: '99/99/9999',
											plugins: 'textmask',
											returnWithMask: true,
											id: 'data_cadastro1_filter_contas_pagar',
											name: 'data_cadastro1',
											flex: 1,
											allowBlank: true,
											margins: '0 5 0 0',
											fieldLabel: 'Período de Cadastro - De',
											value: Ext.Date.format(now, 'd/m/Y'),
											validator: function(value){
												if(value!="" && !validDate(value)){
													return 'Data Inválida...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'textfield',
											mask: '99/99/9999',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro2_filter_contas_pagar',
											name: 'data_cadastro2',
											allowBlank: true,
											fieldLabel: 'Período de Cadastro - Até',
											value: Ext.Date.format(now2, 'd/m/Y'),
											validator: function(value){
												if(value!="" && !validDate(value)){
													return 'Data Inválida...';
												}
												else{
													return true;
												}
											}
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboStatusConta',
									allowBlank: true,
									name: 'status_conta',
									id: 'status_conta_filter_contas_pagar',
									button_id: 'button_status_conta_filter_contas_pagar',
									flex: 1,
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Status Conta',
									value: 'T'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_status_conta_filter_contas_pagar',
									combo_id: 'status_conta_filter_contas_pagar',
									action: 'reset_combo'
								}
							]
						},
						

						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_contas_pagar',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action2',
							hidden: true,
							id: 'action2_filter_contas_pagar',
							allowBlank: false,
							value: 'CONTAS_CADASTRADAS',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Limpar Filtros'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});