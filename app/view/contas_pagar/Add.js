/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contas_pagar.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcontas_pagarwin',

	requires: [
		'ShSolutions.store.StoreContas_Pagar_Pagtos'
	],

	id: 'AddContas_PagarWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	height: 540,
	title: 'Cadastro de Contas à Pagar',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormContas_Pagar',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/contas_pagar/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContasPagarEmpresas',
									name: 'cod_empresa',
									allowBlank: false,
									id: 'cod_empresa_contas_pagar',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Empresa'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContasPagarFornecedores',
									allowBlank: false,
									flex: 1,
									queryMode:'remote',
									triggerAction:'all',
									typeAhead:false,
									minChars:1,
									anchor: '100%',
									name: 'cod_fornecedor',
									id: 'cod_fornecedor_contas_pagar',
									fieldLabel: 'Fornecedor',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 800);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									allowBlank: false,
									maxLength: 255,
									enforceMaxLength: true,
									flex: 1,
									id: 'descricao_contas_pagar',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Descrição'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboContasPagarTipoConta',
									allowBlank: false,
									width: 250,
									triggerAction:'all',
									typeAhead:false,
									mode:'remote',
									minChars: 2,
									forceSelection: false,
									//hideTrigger:true,
									queryMode: 'remote',
									anchor: '100%',
									name: 'tipo_conta',
									id: 'tipo_conta_contas_pagar',
									fieldLabel: 'Tipo da Conta/Centro de Custos', 
									maxLength: 50,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'nf_nro',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'nf_nro_contas_pagar',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									fieldLabel: 'NF Nro'
								},
								{
									xtype: 'datefield',
									width: 130,
									format: 'd/m/Y',
									value: new Date(),
									allowBlank: false,
									margin: '0 5 0 0',
									flex: 1,
									name: 'vencimento',
									id: 'vencimento_contas_pagar',
									fieldLabel: '1º Vencimento',
									mask: '99/99/9999',
									returnWithMask: true,
									plugins: 'textmask',
									validator: function(value){
										if(value!="" && !validDate(value)){
											return 'Data Inválida...';
											Ext.getCmp('parcelas_parcelamento').setReadOnly(true);
										}
										else{
											Ext.getCmp('parcelas_parcelamento').setReadOnly(false);
											return true;
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'valor_nf',
									allowBlank: false,
									id: 'valor_contas_pagar',
									margin: '0 5 0 0',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									mask: '#9.999.990,00',
									money: true,
									plugins: 'textmask',
									fieldLabel: 'Valor NF'
								},
								{
									xtype: 'numberfield',
									anchor: '100%',
									readOnly: true,
									id: 'parcelas_parcelamento',
									name: 'parcelas',
									fieldLabel: 'Nro de Parcelas',
									value: 0,
									maxValue: 100,
									minValue: 1
								}
							]
						},
						{
							xtype: 'fieldset',
							anchor: '100%',
							id: 'fieldset_parcelamento',
							collapsible: true,
							autoScroll: true,
							height: 200,
							title: 'Data de Parcelas'
						},
						{
							xtype: 'textarea',
							name: 'observacoes',
							allowBlank: true,
							id: 'observacoes_contas_pagar',
							anchor: '100%',
							fieldLabel: 'Observações',
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							allowBlank: true,
							margin: '0 5 0 0',
							id: 'id_contas_pagar',
							value: '',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_contas_pagar',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contas_pagar',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_contas_pagar',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});