/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.orcamentos_observacoes.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridorcamentos_observacoeslist',
	requires: [
		'ShSolutions.store.StoreOrcamentos_Observacoes'
	],
	id: 'GridOrcamentos_Observacoes',
	store: 'StoreOrcamentos_Observacoes',

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	//forceFit: true,
	columns: [
		//{xtype: 'rownumberer'},
		{
			xtype: 'gridcolumn',
			dataIndex: 'id',
			text: 'ID',
			width: 60,
			hidden: true,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'produto_id',
			text: 'Produto ID',
			width: 60,
			hidden: true,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'nro_item',
			text: 'Ítem Nro',
			width: 60,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'gridcolumn',
			width: 80,
			dataIndex: 'tipo',
			text: 'Exibir em',
			sortable: false,
			fixed: true,
			renderer: function(v, r, m){
				if(v=='G'){
					return 'GERAL';
				}
				else if(v=='D'){
					return 'DEPÓSITO';
				}
				else if(v=='A'){
					return 'DEP / GERAL';
				}
				else{
					return v;
				}
			}
		},
		{
			xtype: 'gridcolumn',
			width: 910,
			dataIndex: 'descricao',
			text: 'descricao',
			sortable: false,
			fixed: true
		}
	],
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_orcamentos_observacoes',
					iconCls: 'bt_add',
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_orcamentos_observacoes',
					iconCls: 'bt_edit',
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_orcamentos_observacoes',
					iconCls: 'bt_del',
					action: 'deletar',
					text: 'Deletar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});