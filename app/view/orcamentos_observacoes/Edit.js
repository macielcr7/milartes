/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_observacoes.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addorcamentos_observacoeswin',

	id: 'AddOrcamentos_ObservacoesWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 580,
	
	title: 'Cadastro de Observações',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormOrcamentos_Observacoes',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/orcamentos_observacoes/save.php',
					items: [
						{
							xtype: 'htmleditor',
							name: 'descricao',
							allowBlank: false,
							id: 'descricao_orcamentos_observacoes',
							anchor: '100%',
							height: 150,
							labelAlign: 'top',
							labelStyle: 'font-weight: bold;font-size: 11px;',
							fieldLabel: 'Observação/Descrição',
							fieldStyle: 'text-transform:uppercase',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 1200);
								}
							}
						},
						{
							xtype: 'combobox',
							store: 'StoreComboOrcamentosObservacoesTipo',
							name: 'tipo',
							allowBlank: false,
							id: 'tipo_orcamentos_observacoes',
							flex: 1,
							value: 'A',
							anchor: '100%', 
							fieldLabel: 'Exibir Observação para'
						},
						{
							xtype: 'textfield',
							name: 'orcamento_id',
							id: 'orcamento_id_orcamentos_observacoes',
							hidden: true,
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'numberfield',
							name: 'nro_item',
							fieldLabel: 'Ítem Número', 
							hidden: false,
							value: 0,
							id: 'nro_item_orcamentos_observacoes',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							value: 0,
							id: 'id_orcamentos_observacoes',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_orcamentos_observacoes',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_orcamentos_observacoes',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_orcamentos_observacoes').setDisabled(false);
									}, 2500);
								}
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_orcamentos_observacoes',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});