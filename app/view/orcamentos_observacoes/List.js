/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_observacoes.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.orcamentos_observacoeslist',
	requires: [
		'ShSolutions.store.StoreOrcamentos_Observacoes'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'orcamentos_observacoes',

	id: 'List-Orcamentos_Observacoes',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Listagem de Orcamentos_Observacoes',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridorcamentos_observacoeslist'
				}
			]
		});

		me.callParent(arguments);
	}
});