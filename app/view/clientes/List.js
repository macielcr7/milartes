/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.clientes.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.clienteslist',
	requires: [
		'ShSolutions.store.StoreClientes'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'clientes',

	id: 'List-Clientes',
	layout: {
		type: 'fit'
	},

	width: tamanho,
	height: 550,
	title: 'Clientes',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridClientes',
					store: 'StoreClientes',
					viewConfig: {
						autoScroll: true,
						loadMask: false,
						getRowClass: function(record) { 
							if(record.get('situacao_cadastral')=='E'){
								return 'vermelho';
							}
							else if(record.get('situacao_cadastral')=='P'){
								return 'laranja_claro';
							}
							else if(record.get('situacao_cadastral')=='R'){
								return 'vermelho2';
							}
							else{
								return '';
							}
						}
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_cliente',
							format: '0',
							text: 'Cod',
							width: 40
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_completo',
							text: 'Nome / Razão Social',
							width: 250,
							renderer : function(v, m, r){
								if(r.get('tipo_cliente')=='F'){
									return v;
								}
								else{
									if (r.get('nome_fantasia') != "") {
										var texto = r.get('razao_social')+' ('+r.get('nome_fantasia')+')';
										return texto;
									}
									else
									{
										return r.get('razao_social');
									}
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cpf',
							renderer : function(v, m, r){
								if(r.get('tipo_cliente')=='F') {
									if(r.get('cpf')!=null && r.get('cpf').length==11) {
										return formata_cpf_cnpj(r.get('cpf'));
									}
								} 
								else {
									if(r.get('cnpj')!=null && r.get('cnpj').length==14) {
										return formata_cpf_cnpj(r.get('cnpj'));
									}
								}
							},
							text: 'CPF / CNPJ',
							width: 115
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'loc_nome',
							text: 'Cidade / Estado',
							width: 130
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cidade',
							hidden: true,
							text: 'Cidade',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome',
							text: 'Endereço',
							width: 250
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'endereco',
							hidden: true,
							text: 'Endereço',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'bairro_nome',
							text: 'Bairro / Loteamento',
							width: 160
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'bairro',
							hidden: true,
							text: 'Bairro',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'fones',
							text: 'Telefone(s)',
							width: 110
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'situacao_cadastral',
							text: 'Situação Cadastral',
							width: 120,
							renderer: function(v, r, m){
								if(v=='E'){
									return '<font color=\"red\">EXCLUÍDO</font>';
								}
								else if(v=='N'){
									return '<font color=\"green\">NORMAL</font>';
								}
								else if(v=='P'){
									return '<font color=\"red\">PENDÊNCIA FINANCEIRA</font>';
								}
								else if(v=='R'){
									return '<font color=\"red\">RECUSADO</font>';
								}
								else{
									return v;
								}
							}
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreClientes',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_clientes1',
									iconCls: 'bt_add',
									type: 'F',
									hidden: true,
									action: 'adicionar_pf',
									text: 'Adicionar PF'
								},
								{
									xtype: 'button',
									id: 'button_add_clientes2',
									iconCls: 'bt_add',
									hidden: true,
									type: 'J',
									action2: 'adicionar_cli',
									action: 'adicionar_pj',
									text: 'Adicionar PJ'
								},
								{
									xtype: 'button',
									id: 'button_edit_clientes',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_clientes',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Deletar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_clientes',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
												Ext.getCmp('button_filter_clientes').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_clientes',
									iconCls: 'bt_lupa2',
									action: 'filtro',
									text: 'Buscar'
								},
								{
									xtype: 'button',
									id: 'button_pdf_clientes',
									iconCls: 'bt_pdf',
									hidden: true,
									action: 'gerar_pdf',
									text: 'Gerar PDF'
								},
								{ 
									xtype: 'tbfill' 
								},
								{
									xtype: 'button',
									id: 'button_filter_clientes2',
									iconCls: 'bt_filtro',
									action: 'filtrar',
									hidden: true,
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});