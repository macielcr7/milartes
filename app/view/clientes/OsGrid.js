/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.clientes.OsGrid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridclientesoslist',
	requires: [
		'ShSolutions.store.StoreClientesOs'
	],

	id: 'GridClientesOs',
	store: 'StoreClientesOs',
	title: 'OS do Cliente',
	
	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			height: 350,
			
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},

			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'id2',
					text: 'OS Nro',
					width: 60
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'id',
					text: 'ID',
					hidden: true,
					width: 60
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'status',
					text: 'Status',	
					renderer: function(v, r, m){
						if(v=='NOVO'){
							return '<font color=\"orange\">NOVO</font>';
						} else if(v=='CANCELADO') {
							return '<font color=\"red\">CANCELADO</font>';
						} else if(v=='FINALIZADO') {
							return '<font color=\"green\">FINALIZADO</font>';
						} else if(v=='ATRASADO') {
							return '<font color=\"orange\">ATRASADO</font>';
						} else if(v=='REABERTA') {
							return '<font color=\"orange\">REABERTA</font>';
						} else {
							return v;
						}
					},
					width: 80
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'titulo',
					text: 'Título',
					width: 350
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'depto_categoria',
					text: 'Departamento/Categoria',
					width: 160
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'data_agendada2',
					text: 'Data Agendada',
					width: 150
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome_usuario_responsavel',
					text: 'Responsável Pela OS',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'urgencia',
					text: 'Prioridade',
					width: 90,
					renderer: function(v, r, m){
						if(v=='1'){
							return 'NORMAL';
						} else if(v=='2') {
							return '<font color=\"red\"><b>URGENTE</b></font>';
						} else {
							return v;
						}
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome_usuario_abertura',
					text: 'OS Aberta Por',
					width: 120
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_abertura',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Abertura',
					width: 120
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome_usuario_fechamento',
					text: 'OS Fechada Por',
					width: 120
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_fechamento',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Fechamento',
					width: 120
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreClientesOs',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_os',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Cancelar OS'
						},
						{
							xtype: 'button',
							iconCls: 'bt_respostas_os',
							hidden: true,
							disabled: true,
							action: 'os_respostas',
							text: 'Respostas da OS Selecionada'
						},
						{
							xtype: 'button',
							id: 'button_finalizar_os',
							iconCls: 'bt_finalizar_os',
							hidden: true,
							disabled: true,
							action: 'finalizar',
							text: 'Finalizar OS'
						},
						{
							xtype: 'button',
							id: 'button_reabrir_os',
							iconCls: 'bt_reabre_os',
							hidden: true,
							disabled: true,
							action: 'reabrir',
							text: 'Reabrir OS'
						},
						{ 
							xtype: 'tbfill' 
						},
						{
							xtype: 'button',
							id: 'button_filter_os',
							iconCls: 'bt_lupa',
							action: 'filtrar',
							hidden: true,
							text: 'Filtrar'
						},
						{
							xtype: 'button',
							id: 'button_diversos_os',
							iconCls: 'bt_acao2',
							text: 'Diversos',
							hidden: true,
							menu: [
								{
									text: 'Filtros',
									iconCls: 'bt_lupa',
									id: 'button_filter2_os',
									hidden: true,
									action: 'filtrar'
								},
								{
									text: 'Departamentos / Responsáveis',
									iconCls: 'bt_os_deptos',
									id: 'button_os_departamentos',
									hidden: true,
									action: 'os_departamentos'
								},
								{
									text: 'Categorias',
									iconCls: 'bt_os_categorias',
									id: 'button_os_categorias',
									hidden: true,
									action: 'os_categorias'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});