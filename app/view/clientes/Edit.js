/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.clientes.Edit', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addclienteswin',

	requires: [
		'ShSolutions.store.StoreClientes_Anotacoes',
		'ShSolutions.store.StoreClientes_Contato',
		'ShSolutions.store.StoreClientes_Endereco',
		'ShSolutions.store.StoreLogs_Sistema',
		'ShSolutions.store.StoreClientesOs',
		'ShSolutions.store.StoreAdiantamentos',
	],

	id: 'AddClientesWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 900,
	title: 'Cadastro de Clientes',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralCliente',
					items: [
						{
							xtype: 'form',
							title:'Formulário Principal',
							id: 'FormClientes',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/clientes/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'tipo_cliente',
											margin: '0 5 0 0',
											flex: 1,
											hidden: true,
											id: 'tipo_cliente_clientes',
											anchor: '100%',
											fieldLabel: 'Tipo Cliente'
										},	
										{
											xtype: 'textfield',
											name: 'razao_social',
											margin: '0 5 0 0',
											flex: 1,
											id: 'razao_social_clientes',
											anchor: '100%',
											fieldLabel: 'Razão Social',
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'nome_completo',
											margin: '0 5 0 0',
											hidden: true,
											flex: 1,
											id: 'nome_completo_clientes',
											anchor: '100%',	
											fieldLabel: 'Nome Completo', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'nome_fantasia',
											flex: 1,
											allowBlank: true,
											id: 'nome_fantasia_clientes',
											anchor: '100%',
											fieldLabel: 'Nome Fantasia', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											hidden: true,
											width: 110,
											mask: '99/99/9999',
											returnWithMask: true,
											plugins: 'textmask',
											allowBlank: true,
											name: 'data_nascimento',
											id: 'data_nascimento_clientes',
											fieldLabel: 'Data Nascimento',
											validator: function(value){
												if(value!="" && !validDate(value)){
													return 'Data Inválida...';
												}
												else{
													return true;
												}
											}
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									id: 'fieldcontainer_j',
									items: [
										{
											xtype: 'textfield',
											name: 'pessoa_contato',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'pessoa_contato_clientes',
											anchor: '100%',
											fieldLabel: 'Pessoa de Contato', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'cnpj',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cnpj_clientes',
											anchor: '100%',
											mask: '99.999.999/9999-99',
											plugins: 'textmask',
											fieldLabel: 'CNPJ',
											validator: function(value){
												if(value!="" && !validCNPJ(value)){
													return 'CNPJ Inválido...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'textfield',
											name: 'im',
											flex: 1,
											allowBlank: true,
											id: 'im_clientes',
											fieldLabel: 'Insc. Municipal'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									hidden: true,
									id: 'fieldcontainer_f',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {

												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboSexoClientes',
													name: 'sexo',
													allowBlank: true,
													loadDisabled: true,
													id: 'sexo_clientes',
													button_id: 'button_sexo_clientes',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Sexo',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_sexo_clientes',
													combo_id: 'sexo_clientes',
													action: 'reset_combo'
												}
											]
										},
										{
											xtype: 'textfield',
											name: 'cpf',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cpf_clientes',
											anchor: '100%',
											mask: '999.999.999-99',
											plugins: 'textmask',
											fieldLabel: 'CPF',
											validator: function(value){
												if(value!="" && !validCPF(value)){
													return 'CPF Inválido...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'textfield',
											name: 'identidade',
											flex: 1,
											allowBlank: true,
											id: 'identidade_clientes',
											anchor: '100%',
											fieldLabel: 'Identidade'
										}
										
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProfissaoClientes',
											margin: '0 5 0 0',
											hidden: true,
											allowBlank: true,
											flex: 1,
											triggerAction:'all',
											typeAhead:false,
											mode:'remote',
											minChars:2,
											forceSelection: false,
											hideTrigger:true,
											queryMode: 'remote',
											anchor: '100%',
											name: 'profissao',
											id: 'profissao_clientes',
											fieldLabel: 'Profissão', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'ie',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'ie_clientes',
											anchor: '100%',	
											fieldLabel: 'Insc. Estadual'
										},							
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Estados',
													name: 'estado',
													allowBlank: false,
													id: 'estado_clientes',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Estado',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Estados',
													action: 'add_win'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Localidades',
													name: 'cidade',
													allowBlank: false,
													loadDisabled: true,
													minChars:2,
													disabled: true,
													id: 'cidade_clientes',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Cidade',
													//hideTrigger:false,
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Localidades',
													action: 'add_win'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Enderecos',
													name: 'endereco',
													loadDisabled: true,
													allowBlank: true,
													hideTrigger: false,
													minChars:2,
													queryMode: 'remote',
													disabled: true,
													id: 'endereco_clientes',
													button_id: 'button_endereco_clientes',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Endereço',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_endereco_clientes',
													combo_id: 'endereco_clientes',
													handler: function() {
														Ext.getCmp('endereco_clientes').setValue('');
														Ext.getCmp('bairro_nome_clientes').setValue('');
														Ext.getCmp('bairro_clientes').setValue('');
														Ext.getCmp('cep_clientes').setValue('');
														Ext.getCmp('ponto_ref_clientes').setValue('');
														Ext.getCmp('loteamento_clientes').setValue('');
													}
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Enderecos',
													action: 'add_win'
												}
											]
										},	
										{
											xtype: 'textfield',
											name: 'bairro_nome',
											width: 200,
											margin: '0 5 0 0',	
											allowBlank: true,
											readOnly: true,
											id: 'bairro_nome_clientes',
											fieldLabel: 'Bairro',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'bairro',
											flex: 1,
											hidden: true,
											allowBlank: true,
											id: 'bairro_clientes',
											anchor: '100%',
											fieldLabel: 'Bairro Id'
										},
										{
											xtype: 'textfield',
											name: 'nro_end',
											width: 80,
											allowBlank: true,
											id: 'nro_end_clientes',
											fieldLabel: 'Nro End'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'complemento',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'complemento_clientes',
											anchor: '100%',
											fieldLabel: 'Complemento',
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'ponto_ref',	
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'ponto_ref_clientes',
											anchor: '100%',
											fieldLabel: 'Ponto Ref', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'loteamento',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'loteamento_clientes',
											anchor: '100%',
											fieldLabel: 'Loteamento',
											maxLength: 50,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cep',
											margin: '0 5 0 0',
											id: 'cep_clientes',
											mask: '99.999-999',
											plugins: 'textmask',
											allowBlank: true, 
											flex: 1,
											anchor: '100%',
											fieldLabel: 'CEP'
										},
										{
											xtype: 'textfield',
											name: 'cx_postal',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cx_postal_clientes',
											anchor: '100%',
											fieldLabel: 'Cx Postal',
											maxLength: 10,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboSituacaoCadastralClientes',
													name: 'situacao_cadastral',
													loadDisabled: true,
													allowBlank: true,
													id: 'situacao_cadastral_clientes',
													flex: 1,
													value: 'N',
													anchor: '100%', 
													fieldLabel: 'Situação Cadastral'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'email_nfe',
											id: 'email_nfe_clientes',
											fieldLabel: 'E-Mail NFe',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											validator: function(value){
												if(value!="" && !isEmail(value)){
													return 'E-mail Inválido...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCategoria_Precos',
													name: 'id_categoria_precos',
													allowBlank: false,
													id: 'id_categoria_precos_clientes',
													flex: 1,
													value: '1',
													anchor: '100%',
													fieldLabel: 'Categoria Preços'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cadastrado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'cadastrado_por_clientes',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Cadastro Efetuado por:'
										},
										{
											xtype: 'textfield',
											name: 'alterado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'alterado_por_clientes',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Última Alteração Cadastral Efetuada por:'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'cod_cliente',
									hidden: true,
									id: 'cod_cliente_clientes',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'action',
									hidden: true,
									id: 'action_clientes',
									value: 'INSERIR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'clientes_contatolist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'adiantamentoslist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridclientesoslist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridvendaslist',
							disabled: true,
							title: 'Vendas do Cliente',
							height: 350,
							loadDisabled: true
						},
						{
							xtype: 'gridorcamentoslist',
							disabled: true,
							title: 'Orçamentos do Cliente',
							height: 350,
							loadDisabled: true
						},
						{
							xtype: 'clientes_anotacoeslist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'clientes_enderecolist',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridlogs_sistemalist',
							id: 'GridClientes_Logs_Sistema',
							store: 'StoreClientes_Logs_Sistema',
							title: 'Logs do Cliente',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_clientes',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_clientes').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_clientes',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});