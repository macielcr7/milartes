/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.clientes.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filterclienteswin',

	id: 'FilterOsWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	
	title: 'Filtrar Clientes',
	width: 600,
	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterOs',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'nome',
									allowBlank: true,
									flex: 1,
									id: 'nome_filter_cliente',
									anchor: '100%',
									fieldLabel: 'Nome Cliente',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Estados',
									name: 'estado',
									allowBlank: true,
									flex: 1,
									margin: '0 5 0 0',
									id: 'estado_filter_clientes',
									button_id: 'button_estado_filter_clientes',
									anchor: '100%',
									fieldLabel: 'Estado',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_estado_filter_clientes',
									combo_id: 'estado_filter_clientes',
									action: 'reset_combo'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Localidades',
									name: 'cidade',
									allowBlank: true,
									loadDisabled: true,
									minChars:2,
									disabled: true,
									flex: 1,
									id: 'cidade_filter_clientes',
									button_id: 'button_cidade_filter_clientes',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Cidade'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_cidade_filter_clientes',
									combo_id: 'cidade_filter_clientes',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Enderecos',
									name: 'endereco',
									loadDisabled: true,
									allowBlank: true,
									hideTrigger: false,
									minChars: 2,
									queryMode: 'remote',
									disabled: true,
									id: 'endereco_filter_clientes',
									button_id: 'button_endereco_filter_clientes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Endereço',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_endereco_filter_clientes',
									combo_id: 'endereco_filter_clientes',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_clientes',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Limpar Filtros'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});