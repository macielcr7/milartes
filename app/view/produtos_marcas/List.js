/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/


Ext.define('ShSolutions.view.produtos_marcas.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produtos_marcaslist',
	requires: [
		'ShSolutions.store.StoreProdutos_Marcas'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'produtos_marcas',

	id: 'List-Produtos_Marcas',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Marcas de Produtos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridProdutos_Marcas',
					store: 'StoreProdutos_Marcas',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_marca',
							hidden: true,
							format: '0',
							text: 'Cod Marca',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 545
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
								}
							},
							width: 70
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 150
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreProdutos_Marcas',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_produtos_marcas',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_produtos_marcas',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_produtos_marcas',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_produtos_marcas',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_produtos_marcas').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_produtos_marcas',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativos',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});