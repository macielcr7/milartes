/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.clientes_contato.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.clientes_contatolist',
	requires: [
		'ShSolutions.store.StoreClientes_Contato'
	],

	id: 'GridClientes_Contato',
	store: 'StoreClientes_Contato',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,
	height: 350,
	title: 'Contatos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'controle',
					hidden: true,
					format: '0',
					text: 'Controle',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_contato',
					text: 'Tipo Contato',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'descricao',
					text: 'Descrição',
					width: 140,
					renderer : function(v, m, r){
						//var x = "";
						if(r.get('tipo_contato') == 'TELEFONE'){
							if(v.length==10){
								return v.mask('(__) ____-____');
								//x += v.mask('(__) ____-____');
								//x += "<br>X"
							}
							else if(v.length==11){
								return v.mask('(__) _____-____');
							}
							else{
								return v;
							}
						}
						else{
							return v;
						}
						//return x;
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacao',
					text: 'Observação',
					width: 140
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Cadastro',
					width: 140
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreClientes_Contato',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_clientes_contato',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_clientes_contato',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_clientes_contato',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});