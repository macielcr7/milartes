/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.fornecedores_anotacoes.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.fornecedores_anotacoeslist',
	requires: [
		'ShSolutions.store.StoreFornecedores_Anotacoes'
	],
	
	iconCls: 'fornecedores_anotacoes',
	height: 350,
	id: 'GridFornecedores_Anotacoes',
	store: 'StoreFornecedores_Anotacoes',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,	
	title: 'Anotações',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'controle',
					format: '0',
					text: 'Controle',
					width: 60
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'anotacao',
					text: 'Anotação',
					width: 440
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Cadastro',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ativo',
					text: 'Ativo',	
					renderer: function(v){
						switch(v){
							case 'S':
							return 'SIM';
							break;
							case 'N':
							return 'NÃO';
							break;
						}
					},
					width: 50
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreFornecedores_Anotacoes',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_fornecedores_anotacoes',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_fornecedores_anotacoes',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_fornecedores_anotacoes',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});