/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.formas_pagto.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addformas_pagtowin',

	id: 'AddFormas_PagtoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 500,
	title: 'Cadastro de Forma de Pagamento',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFormas_Pagto',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/formas_pagto/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_formas_pagto',
									anchor: '100%',
									fieldLabel: 'Descrição',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'combobox',
									store: 'StoreComboFormasPagtoTipo',
									name: 'tipo',
									id: 'tipo_formas_pagto',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									value: 'V',
									anchor: '100%',
									fieldLabel: 'Tipo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboFormasPagtoCategoria',
									name: 'categoria',
									id: 'categoria_formas_pagto',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									margin: '0 5 0 0',
									value: '1',
									anchor: '100%',
									fieldLabel: 'Categoria'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboAtivo',
									name: 'ativo',
									id: 'ativo_formas_pagto',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									value: 'S',
									anchor: '100%',
									fieldLabel: 'Ativo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							id: 'convenio_formas_pagto',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cod_convenio',
									margin: '0 5 0 0',
									flex: 1,
									id: 'cod_convenio_formas_pagto',
									allowBlank: true,
									anchor: '100%',
									value: '',
									fieldLabel: 'Convênio'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							margin: '0 5 0 0',
							id: 'id_formas_pagto',
							value: 0,
							anchor: '100%',
							fieldLabel: 'Cod Feriado'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_formas_pagto',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_formas_pagto',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_formas_pagto',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});