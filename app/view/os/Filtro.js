/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

var now = new Date();
var now2 = new Date();
var month = now.getMonth();
var mes = now.setMonth(month - 6);

Ext.define('ShSolutions.view.os.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filteroswin',

	id: 'FilterOsWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	
	title: 'Filtrar Ordem de Serviço',
	width: 600,
	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterOs',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'titulo',
									allowBlank: true,
									flex: 1,
									id: 'titulo_filter_os',
									anchor: '100%',
									fieldLabel: 'Título',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProdutos_CategoriasOs',
											name: 'departamento',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'departamento_filter_os',
											button_id: 'button_departamento_filter_os',
											flex: 1,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Departamento'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											margin: '0 5 0 0',
											hidden: true,
											id: 'button_departamento_filter_os',
											combo_id: 'departamento_filter_os',
											action: 'reset_combo'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboOs_Categorias',
											name: 'categoria',
											allowBlank: true,
											id: 'categoria_filter_os',
											button_id: 'button_categoria_filter_os',
											flex: 1,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Categoria'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_categoria_filter_os',
											combo_id: 'categoria_filter_os',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboUsuarios',
											name: 'usuario_abertura',
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'usuario_abertura_filter_os',
											button_id: 'button_usuario_abertura_filter_os',
											flex: 1,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Usuario Abertura'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											margin: '0 5 0 0',
											hidden: true,
											id: 'button_usuario_abertura_filter_os',
											combo_id: 'usuario_abertura_filter_os',
											action: 'reset_combo'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboUsuarios',
											name: 'usuario_responsavel',
											allowBlank: true,
											id: 'usuario_responsavel_filter_os',
											button_id: 'button_usuario_responsavel_filter_os',
											flex: 1,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Usuario Responsavel'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_usuario_responsavel_filter_os',
											combo_id: 'usuario_responsavel_filter_os',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											mask: '99/99/9999',
											plugins: 'textmask',
											returnWithMask: true,
											id: 'data_abertura1_filter_os',
											name: 'data_abertura1',
											flex: 1,
											allowBlank: true,
											margins: '0 5 0 0',
											fieldLabel: 'Período de Abertura - De',
											value: Ext.Date.format(now, 'd/m/Y'),
											validator: function(value){
												if(value!="" && !validDate(value)){
													return 'Data Inválida...';
												}
												else{
													return true;
												}
											}
										},
										{
											xtype: 'textfield',
											mask: '99/99/9999',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_abertura2_filter_os',
											name: 'data_abertura2',
											allowBlank: true,
											fieldLabel: 'Período de Abertura - Até',
											value: Ext.Date.format(now2, 'd/m/Y'),
											validator: function(value){
												if(value!="" && !validDate(value)){
													return 'Data Inválida...';
												}
												else{
													return true;
												}
											}
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboStatusOs',
									allowBlank: true,
									margins: '0 5 0 0',
									name: 'status_igual',
									loadDisabled: true,
									id: 'status_igual_filter_os',
									button_id: 'button_status_igual_filter_os',
									flex: 1,
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Status Igual'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_status_igual_filter_os',
									combo_id: 'status_igual_filter_os',
									action: 'reset_combo'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboStatusOsDiferente',
									allowBlank: true,
									name: 'status_diferente',
									id: 'status_diferente_filter_os',
									button_id: 'button_status_diferente_filter_os',
									flex: 1,
									//value: '1',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Status Diferente'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_status_diferente_filter_os',
									combo_id: 'status_diferente_filter_os',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Estados',
									name: 'estado',
									allowBlank: true,
									flex: 1,
									margin: '0 5 0 0',
									id: 'estado_filter_os',
									button_id: 'button_estado_filter_os',
									anchor: '100%',
									fieldLabel: 'Estado',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_estado_filter_os',
									combo_id: 'estado_filter_os',
									action: 'reset_combo'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Localidades',
									name: 'cidade',
									allowBlank: true,
									loadDisabled: true,
									minChars:2,
									disabled: true,
									flex: 1,
									id: 'cidade_filter_os',
									button_id: 'button_cidade_filter_os',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Cidade'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_cidade_filter_os',
									combo_id: 'cidade_filter_os',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Enderecos',
									name: 'endereco',
									loadDisabled: true,
									allowBlank: true,
									hideTrigger: false,
									minChars:2,
									queryMode: 'remote',
									disabled: true,
									id: 'endereco_filter_os',
									button_id: 'button_endereco_filter_os',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Endereço',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_endereco_filter_os',
									combo_id: 'endereco_filter_os',
									action: 'reset_combo'
								}
							]
						},

						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'endereco_bairro',
									margin: '0 5 0 0',
									flex: 1,
									id: 'endereco_bairro_filter_os',
									allowBlank: true,
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									button_id: 'button_bairro_filter_os',
									fieldLabel: 'Bairro'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_bairro_filter_os',
									combo_id: 'endereco_bairro_filter_os',
									action: 'reset_combo'
								},
								{
									xtype: 'textfield',
									name: 'telefone',
									flex: 1,
									id: 'telefone_filter_os',
									mask: '(99) 9999-9999',
									plugins: 'textmask',
									allowBlank: true,
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									button_id: 'button_telefone_filter_os',
									fieldLabel: 'Telefone'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_telefone_filter_os',
									combo_id: 'telefone_filter_os',
									action: 'reset_combo'
								}
							]
						},

						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_os',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Limpar Filtros'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});