/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1320)
{
	var tamanho = 1320;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.os.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.oslist',
	requires: [
		'ShSolutions.store.StoreOs'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'os',

	id: 'List-Os',
	layout: {
		type: 'fit'
	},

	width: tamanho,
	height: 550,
	title: 'Ordens de Serviços Pendentes',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridoslist'
				}
			]
		});
		me.callParent(arguments);
	}
});