/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os.Edit', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addoswin',

	requires: [
		'ShSolutions.store.StoreLogs_Sistema'
	],

	id: 'AddOsWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 900,
	title: 'Cadastro de Os',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralOs',
					items: [
						{
							xtype: 'form',
							title:'Formulário Principal',
							id: 'FormOs',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/os/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboOsEmpresas',
													name: 'empresa',
													margin: '0 5 0 0',
													width: 250,
													allowBlank: false,
													id: 'empresa_os',
													flex: 1,
													anchor: '100%',
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Empresa'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboUrgenciaOs',
													name: 'urgencia',
													margin: '0 5 0 0',
													width: 100,
													allowBlank: false,
													id: 'urgencia_os',
													value: '1',
													flex: 1,
													anchor: '100%',
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Urgência'
												}
											]
										},
										{
											xtype: 'textfield',
											name: 'titulo',
											flex: 1,
											allowBlank: false,
											id: 'titulo_os',
											anchor: '100%',
											fieldLabel: 'Título',
											fieldStyle: 'text-transform:uppercase',
											listeners: {
												afterrender: function(fld) {
													fld.focus(true, 1500);
												}
											}
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboProdutos_CategoriasOs',
													name: 'departamento',
													id: 'departamento_os',
													flex: 1,
													margin: '0 5 0 0',
													width: 200,
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Departamento'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboOs_Categorias',
													name: 'categoria',
													id: 'categoria_os',
													flex: 1,
													width: 200,
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Categoria'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Estados',
													name: 'estado',
													allowBlank: false,
													id: 'estado_os',
													flex: 1,
													width: 170,
													fieldLabel: 'Estado',
													fieldStyle: 'text-transform:uppercase'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Localidades',
													name: 'cidade',
													allowBlank: false,
													loadDisabled: true,
													minChars:2,
													disabled: true,
													id: 'cidade_os',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Cidade',
													//hideTrigger:false,
													fieldStyle: 'text-transform:uppercase'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Enderecos',
											name: 'endereco',
											margin: '0 5 0 0',
											loadDisabled: true,
											allowBlank: true,
											hideTrigger: false,
											minChars:2,
											queryMode: 'remote',
											disabled: true,
											id: 'endereco_os',
											//button_id: 'button_endereco_os',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Endereço',
											fieldStyle: 'text-transform:uppercase'
										},
										{

											xtype: 'textfield',
											name: 'endereco_bairro_nome',
											width: 200,
											margin: '0 5 0 0',
											allowBlank: true,
											readOnly: true,
											id: 'endereco_bairro_nome_os',
											fieldLabel: 'Bairro',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'endereco_bairro',
											flex: 1,
											hidden: true,
											allowBlank: true,
											id: 'endereco_bairro_os',
											anchor: '100%',
											fieldLabel: 'Bairro Id'
										},
										{
											xtype: 'textfield',
											name: 'endereco_num',
											allowBlank: true,
											id: 'endereco_num_os',
											width: 80,
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Nro End'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'endereco_complemento',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'endereco_complemento_os',
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Complemento',
											maxLength: 255,
											enforceMaxLength: true
										},
										{
											xtype: 'textfield',
											name: 'endereco_referencia',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'endereco_referencia_os',
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											maxLength: 255,
											enforceMaxLength: true,
											fieldLabel: 'Ponto de Referência'
										},
										{
											xtype: 'textfield',
											name: 'endereco_loteamento',
											flex: 1,
											allowBlank: true,
											id: 'endereco_loteamento_os',
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											maxLength: 50,
											enforceMaxLength: true,
											fieldLabel: 'Loteamento'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'hbox'
											},
											flex: 1,
											anchor: '100%',
											margins: '0 3 0 0',
											labelAlign: 'top',
											labelStyle: 'font-weight: bold;font-size: 11px;',
											fieldLabel: 'Fone 1',
											items: [
												{
													xtype: 'textfield',
													width: 103,
													allowBlank: true,
													margins: '0 5 0 0',
													hideLabel: true,
													name: 'fone1',
													id: 'fone1_os',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 1',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 174,
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone1_obs',
													id: 'fone1_obs_os',
													hideLabel: true
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'hbox'
											},
											flex: 1,
											anchor: '100%',
											margins: '0 3 0 0',
											labelAlign: 'top',
											labelStyle: 'font-weight: bold;font-size: 11px;',
											fieldLabel: 'Fone 2',
											items: [
												{
													xtype: 'textfield',
													width: 103,
													allowBlank: true,
													margins: '0 5 0 0',
													hideLabel: true,
													name: 'fone2',
													id: 'fone2_os',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 2',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 174,
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone2_obs',
													id: 'fone2_obs_os',
													margins: '0 5 0 0',
													hideLabel: true
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'hbox'
											},
											flex: 1,
											anchor: '100%',
											labelAlign: 'top',
											labelStyle: 'font-weight: bold;font-size: 11px;',
											fieldLabel: 'Fone 3',
											items: [
												{
													xtype: 'textfield',
													width: 103,
													allowBlank: true,
													margins: '0 5 0 0',
													hideLabel: true,
													name: 'fone3',
													id: 'fone3_os',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 3',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													flex: 1,
													anchor: '100%',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone3_obs',
													id: 'fone3_obs_os',
													hideLabel: true
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'hbox'
											},
											flex: 1,
											labelAlign: 'top',
											margins: '0 5 0 0',
											labelStyle: 'font-weight: bold;font-size: 11px;',
											fieldLabel: 'Fone 4',
											items: [
												{
													xtype: 'textfield',
													width: 103,
													allowBlank: true,
													margins: '0 5 0 0',
													hideLabel: true,
													name: 'fone4',
													id: 'fone4_os',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 4',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 174,
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone4_obs',
													id: 'fone4_obs_os',
													hideLabel: true
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'hbox'
											},
											flex: 1,
											anchor: '100%',
											margins: '0 4 0 0',
											labelAlign: 'top',
											labelStyle: 'font-weight: bold;font-size: 11px;',
											fieldLabel: 'Fone 5',
											items: [
												{
													xtype: 'textfield',
													width: 103,
													allowBlank: true,
													margins: '0 5 0 0',
													hideLabel: true,
													name: 'fone5',
													id: 'fone5_os',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 5',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 174,
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone5_obs',
													id: 'fone5_obs_os',
													hideLabel: true
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'hbox'
											},
											flex: 1,
											anchor: '100%',
											labelAlign: 'top',
											labelStyle: 'font-weight: bold;font-size: 11px;',
											fieldLabel: 'Data Agendada',
											items: [
												{
													xtype: 'datefield',
													width: 130,
													format: 'd/m/Y',
													value: new Date(),
													allowBlank: true,
													margins: '0 5 0 0',
													name: 'data_agendada_date',
													id: 'data_agendada_date_os',
													fieldLabel: 'Data Agendada',
													hideLabel: true
												},
												{
													xtype: 'textfield',
													mask: '99:99',
													width: 50,
													maxLength: 5,
													allowBlank: true,
													enforceMaxLength: true,
													fieldStyle: 'text-transform:uppercase',
													plugins: 'textmask',
													returnWithMask: true,
													id: 'data_agendada_time_os',
													name: 'data_agendada_time',
													hideLabel: true
												}
											]
										}
									]
								},
								{
									xtype: 'textareafield',
									name: 'conteudo',
									flex: 1,
									allowBlank: true,
									id: 'conteudo_os',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Observações'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboOsResponsavel',
											name: 'usuario_responsavel',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'usuario_responsavel_os',
											flex: 1,
											hidden: true,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Responsável Pela OS'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboStatusOs',
											name: 'status',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'status_os',
											hidden: true,
											width: 100,
											flex: 1,
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Status'
										},
										{
											xtype: 'textfield',
											name: 'cadastrado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'cadastrado_por_os',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Cadastro por:'
										},
										{
											xtype: 'textfield',
											name: 'alterado_por',
											flex: 1,
											anchor: '100%',
											allowBlank: true,
											id: 'alterado_por_os',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Última Alteração:'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'cliente',
									hidden: true,
									allowBlank: true,
									id: 'cliente_os',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'nome_cliente',
									hidden: true,
									allowBlank: true,
									id: 'nome_cliente_os',
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'id',
									hidden: true,
									id: 'id_os',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'action',
									hidden: true,
									id: 'action_os',
									value: 'INSERIR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'gridlogs_sistemalist',
							id: 'GridOSLogs_Sistema',
							store: 'StoreOS_Logs_Sistema',
							title: 'Logs da OS',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridos_respostaslist',
							title: 'Respostas da OS',
							iconCls: 'bt_respostas_os',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_os',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_os').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_os',
							iconCls: 'bt_save',
							action: 'salvar',
							//hidden: false,
							disabled: false,
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});