/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os.Cancelamento', {
	extend: 'Ext.window.Window',
	alias: 'widget.addoswin',

	id: 'Cancelamento-Os',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 400,
	title: 'Cancelamento de OS',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCancelamentoOs',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/os/acoes.php',
					items: [
						{
							xtype: 'textarea',
							name: 'motivo_cancelamento',
							id: 'motivo_cancelamento_os',
							anchor: '100%',
							fieldStyle: 'text-transform:uppercase',
							fieldLabel: 'Motivo do Cancelamento',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 1000);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'cod_os',
							hidden: true,
							id: 'id_os',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_os',
							value: 'DELETAR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_os',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_os',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});