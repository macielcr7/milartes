/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridoslist',
	requires: [
		'ShSolutions.store.StoreOs'
	],
	id: 'GridOs',
	store: 'StoreOs',
	height: 350,

	initComponent: function() {
		var me = this;
		var selModel = Ext.create('Ext.selection.CheckboxModel', {
			mode: 'MULTI'
		});

		Ext.applyIf(me, {
			selModel: selModel,
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			features: [{
				ftype: 'groupingsummary',
				groupHeaderTpl: '{name}, ({rows.length} Orçamento{[values.rows.length > 1 ? "s" : ""]})',
				hideGroupedHeader: false,
				enableGroupingMenu: true
			}],
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'id2',
					text: 'OS Nro',
					width: 55
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'id',
					text: 'ID',
					hidden: true,
					width: 60
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'status',
					text: 'Status',	
					renderer: function(v, r, m){
						if(v=='NOVO'){
							return '<font color=\"orange\">NOVO</font>';
						} else if(v=='CANCELADO') {
							return '<font color=\"red\">CANCELADO</font>';
						} else if(v=='FINALIZADO') {
							return '<font color=\"green\">FINALIZADO</font>';
						} else if(v=='ATRASADO') {
							return '<font color=\"orange\">ATRASADO</font>';
						} else if(v=='REABERTA') {
							return '<font color=\"orange\">REABERTA</font>';
						} else {
							return v;
						}
					},
					width: 70
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'titulo',
					text: 'Título',
					width: 450
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'depto_categoria',
					text: 'Categoria/Departamento',
					width: 195
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'data_agendada2',
					text: 'Data Agendada',
					width: 150
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome_usuario_responsavel',
					text: 'Responsável Pela OS',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'urgencia',
					text: 'Prioridade',
					width: 90,
					renderer: function(v, r, m){
						if(v=='1'){
							return 'NORMAL';
						} else if(v=='2') {
							return '<font color=\"red\"><b>URGENTE</b></font>';
						} else {
							return v;
						}
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome_usuario_abertura',
					text: 'OS Aberta Por',
					width: 130
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreOs',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_os',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_os',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_os',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Cancelar OS'
						},
						{
							xtype: 'button',
							id: 'button_os_respostas_os',
							iconCls: 'bt_respostas_os',
							hidden: true,
							disabled: true,
							action: 'os_respostas',
							text: 'Respostas da OS Selecionada'
						},
						{
							xtype: 'button',
							id: 'button_finalizar_os',
							iconCls: 'bt_finalizar_os',
							hidden: true,
							disabled: true,
							action: 'finalizar',
							text: 'Finalizar OS'
						},
						{
							xtype: 'button',
							id: 'button_reabrir_os',
							iconCls: 'bt_reabre_os',
							hidden: true,
							disabled: true,
							action: 'reabrir',
							text: 'Reabrir OS'
						},
						{ 
							xtype: 'tbfill' 
						},
						{
							xtype: 'combobox',
							hideLabel: true,
							store: 'StoreComboUsuarioResponsavel',
							valueField: 'id',
							displayField: 'descricao',
							typeAhead: true,
							queryMode: 'remote',
							triggerAction: 'all',
							emptyText:'Responsável Pela OS...',
							selectOnFocus:true,
							width: 180,
							allowBlank: true,
							id: 'filtra_responsavel',
							action: 'filtrar2'
						},
						{
							xtype: 'button',
							id: 'button_filter_os',
							name: 'filtro_1',
							iconCls: 'bt_lupa',
							action: 'filtrar',
							hidden: true,
							text: 'Filtrar'
						},
						{
							xtype: 'button',
							id: 'button_diversos_os',
							iconCls: 'bt_acao2',
							text: 'Diversos',
							menu: [
								{
									text: 'Filtros',
									iconCls: 'bt_lupa3',
									id: 'button_filter2_os',
									hidden: true,
									name: 'filtro_2',
									action: 'filtrar'
								},
								{
									text: 'Departamentos / Responsáveis',
									iconCls: 'bt_os_deptos',
									id: 'button_os_departamentos',
									hidden: true,
									action: 'os_departamentos'
								},
								{
									text: 'Categorias',
									iconCls: 'bt_os_categorias',
									id: 'button_os_categorias',
									hidden: true,
									action: 'os_categorias'
								}
							]
						},
						{
							xtype: 'button',
							id: 'button_pdf_os',
							iconCls: 'bt_imprimir2',
							action: 'gerar_pdf',
							text: 'Imprimir OS Selecionadas'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});