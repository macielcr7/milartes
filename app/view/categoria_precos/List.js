/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.categoria_precos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.categoria_precoslist',
	requires: [
		'ShSolutions.store.StoreCategoria_Precos'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'categoria_precos',

	id: 'List-Categoria_Precos',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Listagem de Categoria Preços',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridCategoria_Precos',
					store: 'StoreCategoria_Precos',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					forceFit: true,
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_categoria_precos',
							hidden: true,
							format: '0',
							text: 'Id Categoria Precos',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 140
						}
				
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreCategoria_Precos',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_categoria_precos',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_categoria_precos',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_categoria_precos',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Remover'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});