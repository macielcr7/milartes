/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.categoria_precos.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcategoria_precoswin',

	id: 'AddCategoria_PrecosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 500,
	title: 'Cadastro de Tabela de Preços',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCategoria_Precos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/categoria_precos/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_categoria_precos',
							anchor: '100%',
							fieldLabel: 'Descrição',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 400);
								}
							}
						},
						{
							xtype: 'hidden',
							name: 'id_categoria_precos',
							hidden: true,
							id: 'id_categoria_precos_categoria_precos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_categoria_precos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_categoria_precos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_categoria_precos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});