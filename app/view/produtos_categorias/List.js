/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.produtos_categorias.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produtos_categoriaslist',
	requires: [
		'ShSolutions.store.StoreProdutos_Categorias'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'produtos_categorias',
	id: 'List-Produtos_Categorias',
	layout: {
		type: 'fit'
	},
	height: 500,
	width: 1024,
	title: 'Categorias de Produtos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridProdutos_Categorias',
					store: 'StoreProdutos_Categorias',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_categoria',
							hidden: true,
							format: '0',
							text: 'Cod Categoria',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'parent',
							text: 'Categoria Pai',
							width: 150
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 285
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'aliquota_comissao',
							text: 'Alíquota de Comissão',
							renderer: function(value) {
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(value);
								return value + ' %';
							},
							width: 130
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'desconto_maximo',
							text: 'Desconto Máximo',
							renderer: function(value) {
								var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(value);
								return value + ' %';
							},
							width: 130
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
							
								}
							},
							width: 70
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 150
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreProdutos_Categorias',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_produtos_categorias',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_produtos_categorias',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_produtos_categorias',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'button',
									id: 'button_responsavel_os_produtos_categorias',
									iconCls: 'bt_responsavel_depto',
									hidden: true,
									disabled: true,
									action: 'responsavel_os',
									text: 'Responsável em OS'
								},
								{
									xtype: 'button',
									id: 'button_propriedades_produtos_categorias',
									iconCls: 'bt_acao',
									hidden: true,
									disabled: true,
									action: 'propriedades',
									text: 'Propriedades'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_produtos_categorias',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_produtos_categorias').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_produtos_categorias',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativo',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});