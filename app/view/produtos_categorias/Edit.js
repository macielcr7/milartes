/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produtos_categorias.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addprodutos_categoriaswin',
	id: 'AddProdutos_CategoriasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 350,
	title: 'Cadastro de Categoria de Produto',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProdutos_Categorias',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produtos_categorias/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_produtos_categorias',
									anchor: '100%',
									fieldLabel: 'Descrição',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutos_Categorias',
									name: 'id_parent',
									allowBlank: true,
									id: 'id_parent_categoria',
									flex: 1,
									listConfig: {
						                getInnerTpl: function() {
						                    return '{descricao_list}';
						                }
						            },
									anchor: '100%',
									fieldLabel: 'Categoria Pai'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboAtributos',
									name: 'id_atributos[]',
									multiSelect: true,
									allowBlank: true,
									id: 'id_atributos_categorias',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Atributos'
								},
								{
									xtype: 'buttonadd',
									tabela: 'Atributos',
									action: 'add_win'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'aliquota_comissao',
									margin: '0 5 0 0',
									allowBlank: true,
									flex: 1,
									mask: '999.999.999,99',
									money: true,
									plugins: 'textmask',
									id: 'aliquota_comissao_produtos_categorias',
									anchor: '100%',
									fieldLabel: 'Alíquota de Comissão %'
								},
								{
									xtype: 'textfield',
									name: 'desconto_maximo',
									margin: '0 5 0 0',
									allowBlank: true,
									flex: 1,
									mask: '999.999.999,99',
									money: true,
									plugins: 'textmask',
									id: 'desconto_maximo_produtos_categorias',
									anchor: '100%',
									fieldLabel: 'Desconto Máximo %'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'meta_tag_keywords',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									id: 'meta_tag_keywords_produtos_categorias',
									anchor: '100%',
									fieldLabel: 'Meta Tag KeyWords (separar por vírgula)'
								}
							]
						},						
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'filefield',
									name: 'imagem',
									margin: '0 5 0 0',
									allowBlank: true,
									flex: 1,
									id: 'imagem_produtos_categorias',
									anchor: '100%',
									emptyText: 'Selecione uma Imagem',
									fieldLabel: 'Imagem da Categoria',
									buttonText: '',
									buttonConfig: {
										iconCls: 'upload-icon'
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'meta_tag_description',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									id: 'meta_tag_description_produtos_categorias',
									anchor: '100%',
									fieldLabel: 'Meta Tag Description'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											id: 'ativo_produtos_categorias',
											button_id: 'button_ativo_produtos_categorias',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											value: 'S',
											anchor: '100%',
											fieldLabel: 'Ativo'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_produtos_categorias',
											combo_id: 'ativo_produtos_categorias',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'cod_categoria',
							hidden: true,
							margin: '0 5 0 0',
							id: 'cod_categoria_produtos_categorias',
							value: 0,
							anchor: '100%',
							fieldLabel: 'cod depto'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_produtos_categorias',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produtos_categorias',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produtos_categorias',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});