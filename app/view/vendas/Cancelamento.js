/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas.Cancelamento', {
	extend: 'Ext.window.Window',
	alias: 'widget.addvendaswin',

	id: 'Cancelamento-Vendas',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 400,
	title: 'Cancelamento de Venda',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCancelamentoVendas',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/orcamentos/delete.php',
					items: [
						{
							xtype: 'textarea',
							name: 'motivo_cancelamento',
							id: 'motivo_cancelamento_vendas',
							anchor: '100%',
							fieldStyle: 'text-transform:uppercase',
							fieldLabel: 'Motivo do Cancelamento',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 1000);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'id_orcamento',
							hidden: true,
							id: 'id_orcamentos_vendas',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'venda_cod_orcamento',
							hidden: true,
							id: 'venda_cod_orcamento_vendas',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_vendas',
							value: 'DELETAR_VENDA',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_vendas',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_vendas',
							iconCls: 'bt_save',
							action: 'salvar_cancelamento',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});