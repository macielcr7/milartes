/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.vendas.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.vendaslist',
	requires: [
		'ShSolutions.store.StoreVendas'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'vendas',

	id: 'List-Vendas',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 550,
	title: 'Vendas que Você efetuou',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridvendaslist'
				}
			]
		});

		me.callParent(arguments);
	}
});