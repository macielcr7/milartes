/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addvendaswin',

	id: 'AddVendasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 350,
	title: 'Efetuando Pagamento....',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormVendas',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/vendas/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'saldo_a_receber',
									flex: 1,
									id: 'saldo_a_receber_vendas',
									allowBlank: true,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									readOnly: true,
									fieldLabel: 'Saldo a Receber'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'valor_pago',
									flex: 1,
									id: 'valor_pago_vendas',
									allowBlank: false,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Valor',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 800);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboFormas_Pagto',
									name: 'forma_pagto',
									id: 'forma_pagto_vendas',
									flex: 1,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Forma de Pagamento',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'observacao',
									flex: 1,
									id: 'observacao_vendas',
									anchor: '100%',
									fieldLabel: 'Observacao',
									allowBlank: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							fieldLabel: 'ID',
							id: 'id_vendas',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'cliente',
							margin: '0 5 0 0',
							flex: 1,
							hidden: true,
							id: 'cod_cliente_vendas',
							anchor: '100%',
							fieldLabel: 'Cod Cliente'
						},
						{
							xtype: 'textfield',
							name: 'cod_venda',
							margin: '0 5 0 0',
							flex: 1,
							hidden: true,
							id: 'cod_venda_vendas',
							anchor: '100%',
							fieldLabel: 'Cod Venda'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_vendas',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contas_pagar_pagtosvendas',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_vendas',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});