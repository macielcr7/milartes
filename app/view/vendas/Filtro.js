/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filtervendaswin',

	id: 'FilterOsWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	
	title: 'Filtrar Clientes',
	width: 600,
	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterOs',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cliente_descricao',
									allowBlank: true,
									flex: 1,
									id: 'cliente_descricao_filter_vendas',
									anchor: '100%',
									fieldLabel: 'Cliente Descrição',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboUsuariosVendas',
									name: 'cadastrado_por',
									allowBlank: true,
									flex: 1,
									margin: '0 5 0 0',
									id: 'filtra_cadastrado_por_vendas',
									button_id: 'button_cadastrado_por_filter_vendas',
									anchor: '100%',
									fieldLabel: 'Cadastrado Por',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: false,
									id: 'button_cadastrado_por_filter_vendas',
									combo_id: 'filtra_cadastrado_por_vendas',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							labelAlign: 'top',
							labelStyle: 'font-weight: bold;font-size: 11px;',
							fieldLabel: 'Data Venda - Início',
							items: [
								{
									xtype: 'datefield',
									format: 'd/m/Y',
									flex: 1,
									id: 'data_venda_date_inicio_filter_vendas',
									name: 'data_venda_date_inicio',
									margins: '0 5 0 0',
									hideLabel: true
								},
								{
									xtype: 'textfield',
									mask: '99:99:99',
									plugins: 'textmask',
									returnWithMask: true,
									flex: 1,
									id: 'data_venda_time_inicio_filter_vendas',
									name: 'data_venda_time_inicio',
									hideLabel: true
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							labelAlign: 'top',
							labelStyle: 'font-weight: bold;font-size: 11px;',
							fieldLabel: 'Data Venda - Fim',
							items: [
								{
									xtype: 'datefield',
									format: 'd/m/Y',
									flex: 1,
									id: 'data_venda_date_filter_fim_vendas',
									name: 'data_venda_date_fim',
									margins: '0 5 0 0',
									hideLabel: true
								},
								{
									xtype: 'textfield',
									mask: '99:99:99',
									plugins: 'textmask',
									returnWithMask: true,
									flex: 1,
									id: 'data_venda_time_filter_fim_vendas',
									name: 'data_venda_time_fim',
									hideLabel: true
								}
							]
						},






						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Estados',
									name: 'estado',
									allowBlank: true,
									flex: 1,
									margin: '0 5 0 0',
									id: 'estado_filter_vendas',
									button_id: 'button_estado_filter_vendas',
									anchor: '100%',
									fieldLabel: 'Estado',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_estado_filter_vendas',
									combo_id: 'estado_filter_vendas',
									action: 'reset_combo'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Localidades',
									name: 'cidade',
									allowBlank: true,
									loadDisabled: true,
									minChars:2,
									disabled: true,
									flex: 1,
									id: 'cidade_filter_vendas',
									button_id: 'button_cidade_filter_vendas',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Cidade'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_cidade_filter_vendas',
									combo_id: 'cidade_filter_vendas',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCorreios_Enderecos',
									name: 'endereco',
									loadDisabled: true,
									allowBlank: true,
									hideTrigger: false,
									minChars: 2,
									queryMode: 'remote',
									disabled: true,
									id: 'endereco_filter_vendas',
									button_id: 'button_endereco_filter_vendas',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Endereço',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									margin: '0 5 0 0',
									hidden: true,
									id: 'button_endereco_filter_vendas',
									combo_id: 'endereco_filter_vendas',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_vendas',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Limpar Filtros'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});