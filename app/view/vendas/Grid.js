/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridvendaslist',
	requires: [
		'ShSolutions.store.StoreVendas'
	],
	id: 'GridVendas',
	store: 'StoreVendas',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			features: [{
				ftype: 'summary',
				dock: 'bottom'
			}],
			columns: [
			{
				xtype: 'gridcolumn',
				dataIndex: 'id_orcamento',
				text: 'ID',
				sortable: true,
				fixed: true,
				width: 50
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'tipo_orcamento',
				text: 'Tipo',
				hidden: true,
				sortable: true,
				width: 120,
				renderer: function(v, r, m){
					if(v=='0'){
						return 'Geral';
					}
					else if(v=='1'){
						return 'Forro';
					}
					else if(v=='2'){
						return 'Persianas';
					}
					else if(v=='3'){
						return 'Pisos';
					}
					else if(v=='4'){
						return 'Rodapés/Sancas';
					}
					else{
						return v;
					}
				}
			},
			{
				xtype: 'datecolumn',
				dataIndex: 'data_venda',
				format: 'd/m/Y H:i:s',
				renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
				text: 'Data Venda',
				sortable: true,
				fixed: true,
				width: 125
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'cliente',
				text: 'Cliente',
				sortable: false,
				fixed: true,
				width: 385
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'cliente_descricao',
				text: 'Cliente Descrição',
				sortable: false,
				fixed: true,
				width: 230
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'valor_bruto',
				text: 'Sub Total',
				hidden: true,
				width: 100,
				sortable: false,
				fixed: true,
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					return value.toLocaleString('pt-BR');
				},
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"black\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'desconto_porcento',
				text: '% Desconto',
				hidden: true,
				width: 100,
				sortable: false,
				fixed: true,
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"black\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'acrescimo_porcento',
				text: '% Acréscimo',
				hidden: true,
				width: 100,
				sortable: false,
				fixed: true,
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"black\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'valor_total',
				text: 'Valor Total',
				width: 100,
				sortable: false,
				fixed: true,
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					return value.toLocaleString('pt-BR');
				},
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"black\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'total_adiantado',
				text: 'Total Adiantado',
				width: 100,
				sortable: false,
				fixed: true,
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					return value.toLocaleString('pt-BR');
				},
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"black\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'total_pago',
				text: 'Total Pago',
				width: 90,
				sortable: false,
				fixed: true,
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					return value.toLocaleString('pt-BR');
				},
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"black\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'saldo_a_receber',
				text: 'Saldo à Receber',
				width: 110,
				sortable: false,
				fixed: true,
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					return value.toLocaleString('pt-BR');
				},
				renderer: function(v, r, m){
					var value = Ext.util.Format.maskRenderer('#9.999.990,00', true)(v);
					if(v > 0){
						return '<font color=\"blue\"><b>'+value+'</b></font>';
					} else {
						return value;
					}
				}
			}
		],
		dockedItems: [
			{
				xtype: 'pagingtoolbar',
				displayInfo: true,
				store: 'StoreVendas',
				dock: 'bottom'
			},
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						id: 'button_add_vendas',
						text: 'Adicionar',
						iconCls: 'bt_add',
						action: 'adicionar',
						menu: {
							xtype: 'menu',
							width: 220,
							items: [
								{
									xtype: 'menuitem',
									action: 'geral',
									text: 'Geral'
								},
								{
									xtype: 'menuitem',
									action: 'forro',
									text: 'Forros / Divisórias (PVC)'
								},
								{
									xtype: 'menuitem',
									action: 'rodape',
									text: 'Rodapés, Sancas, Vistas, Faixas'
								},
								{
									xtype: 'menuitem',
									action: 'piso',
									text: 'Pisos (Laminado/Vinílico)'
								}
							]
						}
					},
					{
						xtype: 'button',
						id: 'button_edit_vendas',
						iconCls: 'bt_edit',
						//hidden: true,
						disabled: true,
						action: 'editar',
						text: 'Editar'
					},
					{
						xtype: 'button',
						id: 'button_del_vendas',
						iconCls: 'bt_del',
						//hidden: true,
						disabled: true,
						action: 'deletar',
						text: 'Deletar'
					},
					{
						xtype: 'button',
						id: 'button_pagamento_vendas',
						iconCls: 'bt_totalizar_periodos_caixas',
						//hidden: true,
						disabled: true,
						action: 'efetua_pagamento',
						text: 'Pagamentos'
					},
					{
						xtype: 'button',
						id: 'button_imprimir_vendas',
						text: 'Imprimir',
						iconCls: 'bt_imprimir2',
						disabled: true,
						action: 'imprimir_venda',
						menu: {
							xtype: 'menu',
							width: 320,
							items: [
								{
									xtype: 'menuitem',
									action: 'impressao_cliente',
									text: 'Versão Para Cliente'
								},
								{
									xtype: 'menuitem',
									action: 'impressao_deposito',
									text: 'Versão para Depósito/Corte'
								},
								{
									xtype: 'menuitem',
									action: 'impressao_deposito2',
									text: 'Versão 2 para Depósito/Corte'
								},
								{
									xtype: 'menuitem',
									action: 'impressao_detalhado',
									text: 'Versão Completa com Pagamentos'
								}
							]
						}
					},
					{ 
						xtype: 'tbfill' 
					},
					{
						xtype: 'button',
						id: 'button_converte_vendas',
						iconCls: 'converte_venda',
						//hidden: false,
						disabled: true,
						action: 'converte_em_orcamento',
						text: 'Cria Cópia como Orçamento'
					},
					{
						xtype: 'button',
						id: 'button_custos_venda',
						iconCls: 'categoria_precos',
						//hidden: false,
						disabled: true,
						action: 'ver_custos',
						text: 'Revisa Custos da Venda'
					},
					{
						xtype: 'button',
						id: 'button_filter_vendas',
						iconCls: 'bt_filtro',
						action: 'filtrar',
						hidden: false,
						text: 'Filtrar'
					}
				]
			}
		]
		});

		me.callParent(arguments);
	}
});