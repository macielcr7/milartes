/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.view.cfop.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcfopwin',

	id: 'AddCfopWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	title: 'Cadastro de CFOP',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCfop',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/cfop/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							//autoHeight: true,
							layout: {
								//align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cod_fiscal',
									allowBlank: false,
									maxLength: 4,
									enforceMaxLength: true,
									margin: '0 5 0 0',
									id: 'cod_fiscal_cfop',
									width: '60',
									fieldLabel: 'Código Fiscal',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 800);
										}
									}
								},
								{
									xtype: 'combobox',
									store: 'StoreComboTipoCfop',
									name: 'tipo',
									margin: '0 5 0 0',
									allowBlank: false,
									loadDisabled: true,
									id: 'tipo_cfop',
									button_id: 'button_tipo_cfop',
									width: '60',
									fieldLabel: 'Tipo',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'combobox',
									store: 'StoreComboOperacaoCfop',
									name: 'operacao',
									margin: '0 5 0 0',
									allowBlank: false,
									loadDisabled: true,
									id: 'operacao_cfop',
									button_id: 'button_operacao_cfop',
									width: '60',
									fieldLabel: 'Operação',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_cfop',
									anchor: '100%',
									fieldLabel: 'Descrição',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									rows: 9,
									name: 'detalhamento',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'detalhamento_cfop',
									anchor: '100%',
									fieldLabel: 'Detalhamento',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_cfop',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_cfop',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_cfop',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});