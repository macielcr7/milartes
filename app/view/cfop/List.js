/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/


Ext.define('ShSolutions.view.cfop.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.cfoplist',
	requires: [
		'ShSolutions.store.StoreCfop'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'cfop',

	id: 'List-Cfop',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'CFOPs',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridCfop',
					store: 'StoreCfop',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_fiscal',
							hidden: false,
							format: '0',
							text: 'Cod Fiscal',
							width: 85
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'tipo',
							text: 'Tipo',
							width: 60,
							renderer: function(v, r, m){
								if(v=='E'){
									return 'ENTRADA';
								}
								else if(v=='S'){
									return 'SAÍDA';
								}
								else{
									return v;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 370
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'operacao',
							text: 'Operação',
							width: 120,
							renderer: function(v, r, m){
								if(v=='DE'){
									return 'DENTRO DO ESTADO';
								}
								else if(v=='FE'){
									return 'FORA DO ESTADO';
								}
								else{
									return v;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 130
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreCfop',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_cfop',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_cfop',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_cfop',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Remover'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_cfop',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_cfop').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_cfop',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});