/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/

Ext.define('ShSolutions.view.feriados.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addferiadoswin',

	id: 'AddFeriadosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 500,
	title: 'Cadastro de Feriado',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFeriados',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/feriados/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_feriados',
									anchor: '100%',
									fieldLabel: 'Descrição',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'data_feriado',
									margin: '0 5 0 0',
									allowBlank: true,
									flex: 1,
									id: 'data_feriado_feriados',
									fieldLabel: 'Data do Feriado',
									fieldStyle: 'text-transform:uppercase',
									width: 110,
									mask: '99/99/9999',
									returnWithMask: true,
									plugins: 'textmask',
									validator: function(value){
										if(value!="" && !validDate(value)){
											return 'Data InvÃ¡lida...';
										}
										else{
											return true;
										}
									}
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'cod_feriado',
							hidden: true,
							margin: '0 5 0 0',
							id: 'cod_feriado_feriados',
							value: 0,
							anchor: '100%',
							fieldLabel: 'Cod Feriado'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_feriados',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_feriados',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_feriados',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});