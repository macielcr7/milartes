/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/


Ext.define('ShSolutions.view.feriados.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.feriadoslist',
	requires: [
		'ShSolutions.store.StoreFeriados'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'feriados',

	id: 'List-Feriados',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Feriados Bancários',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridFeriados',
					store: 'StoreFeriados',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_feriado',
							hidden: true,
							format: '0',
							text: 'Cod Tabela',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao',
							text: 'Descrição',
							width: 410
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'data_feriado',
							text: 'Data do Feriado',
							width: 160
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 195
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreFeriados',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_feriados',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_feriados',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_feriados',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Remover'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_feriados',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_feriados').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_feriados',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});