/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.recibos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.reciboslist',
	requires: [
		'ShSolutions.store.StoreRecibos'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'recibos',

	id: 'List-Recibos',
	layout: {
		type: 'fit'
	},
	width: 1000,
	height: 450,
	title: 'Recibos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridRecibos',
					store: 'StoreRecibos',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'ID',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'empresa_nome',
							text: 'Empresa',
							width: 260
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'recebido_de_nome',
							text: 'Emitido Para',
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'referente',
							text: 'Referente',
							renderer: function (value){
								return '<div style="white-space:normal !important;">'+ value +'</div>';
							},
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'valor_recibo',
							text: 'Valor do Recibo',
							width: 100
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'data_recibo',
							text: 'Data Recibo',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 145
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreRecibos',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_recibos',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_recibos',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_recibos',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Remover'
								},
								{
									xtype: 'button',
									id: 'button_imprimir_recibos',
									iconCls: 'bt_imprimir2',
									hidden: true,
									disabled: true,
									action: 'imprimir_recibo',
									text: 'Imprimir Recibo'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_recibos',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_recibos').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_recibos',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});