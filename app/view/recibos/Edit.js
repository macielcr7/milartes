/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.recibos.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addreciboswin',
	id: 'AddRecibosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 500,
	title: 'Cadastro de Recibos',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormRecibos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/recibos/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'data_recibo',
									margin: '0 5 0 0',
									mask: '99/99/9999',
									returnWithMask: true,
									plugins: 'textmask',
									allowBlank: false,
									flex: 1,
									id: 'data_recibo_recibos',
									anchor: '100%',
									fieldLabel: 'Data do Recibo',
									validator: function(value){
										if(value!="" && !validDate(value)){
											return 'Data Inválida...';
										}
										else{
											return true;
										}
									},
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'valor_recibo',
									margin: '0 5 0 0',
									allowBlank: false,
									flex: 1,
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									id: 'valor_recibo_recibos',
									anchor: '100%',
									fieldLabel: 'Valor do Recibo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboRecibosDeNome',
									margin: '0 5 0 0',
									allowBlank: false,
									flex: 1,
									triggerAction:'all',
									typeAhead:false,
									mode:'remote',
									minChars:2,
									forceSelection: false,
									hideTrigger: true,
									queryMode: 'remote',
									loadDisabled: true,
									anchor: '100%',
									name: 'recebido_de_nome',
									id: 'recebido_de_nome_recibos',
									fieldLabel: 'Recebemos De (nome)', 
									maxLength: 255,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase',
									listConfig: {
										loadingText: 'Procurando...',
										emptyText: 'Nenhum Resultado Encontrado...',
										getInnerTpl: function() {
											var tpl =
												'<div>' +
												'<tpl if="docto == \'\'">' +
													'{id}' +
												'<tpl else>' +
													'{id}<br>{docto}' +
												'</tpl>' +
												'</div>';
											return tpl;
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'recebido_de_docto',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									id: 'recebido_de_docto_recibos',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Recebemos De (CPF/RG)'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'referente',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									id: 'referente_recibos',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Referente á'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							margin: '0 5 0 0',
							id: 'id_recibos',
							value: 0,
							anchor: '100%',
							fieldLabel: 'id'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_recibos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_recibos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_recibos').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_recibos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});