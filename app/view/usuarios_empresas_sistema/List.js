/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.usuarios_empresas_sistema.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.usuarios_empresas_sistemalist',
	requires: [
		'ShSolutions.store.StoreUsuarios_Empresas_Sistema'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'usuarios_empresas_sistema',

	id: 'List-Usuarios_Empresas_Sistema',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Empresas Associadas ao Usuário Selecionado',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridUsuarios_Empresas_Sistema',
					store: 'StoreUsuarios_Empresas_Sistema',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					forceFit: true,
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'controle',
							hidden: true,
							format: '0',
							text: 'Controle',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome',
							hidden: true,
							text: 'Usuario',
							width: 140
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_usuario',
							hidden: true,
							format: '0',
							text: 'Usuario',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'razao_social',
							text: 'Empresa',
							width: 370
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_empresa',
							hidden: true,
							format: '0',
							text: 'Empresa',
							width: 140
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cadastro',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Associa&ccedil;&atilde;o',
							width: 120
						}
				
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreUsuarios_Empresas_Sistema',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_usuarios_empresas_sistema',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_del_usuarios_empresas_sistema',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Remover'
								}								
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});