/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.usuarios_empresas_sistema.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filterusuarios_empresas_sistemawin',

    id: 'FilterUsuarios_Empresas_SistemaWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Usuarios Empresas Sistema',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterUsuarios_Empresas_Sistema',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboUsuarios',
                                    name: 'id_usuario',
									id: 'id_usuario_filter_usuarios_empresas_sistema',
									button_id: 'button_id_usuario_filter_usuarios_empresas_sistema',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Usuario'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_usuario_filter_usuarios_empresas_sistema',
                                    combo_id: 'id_usuario_filter_usuarios_empresas_sistema',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboEmpresas_Sistema',
                                    name: 'id_empresa',
									id: 'id_empresa_filter_usuarios_empresas_sistema',
									button_id: 'button_id_empresa_filter_usuarios_empresas_sistema',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Empresa'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_empresa_filter_usuarios_empresas_sistema',
                                    combo_id: 'id_empresa_filter_usuarios_empresas_sistema',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_usuarios_empresas_sistema',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
