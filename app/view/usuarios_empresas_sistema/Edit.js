/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.usuarios_empresas_sistema.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addusuarios_empresas_sistemawin',

	id: 'AddUsuarios_Empresas_SistemaWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 600,
	title: 'Usuários / Empresas',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormUsuarios_Empresas_Sistema',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/usuarios_empresas_sistema/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'id_usuario',
							hidden: true,
							id: 'id_usuario_usuarios_empresas_sistema',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboEmpresas_Sistema2',
									name: 'id_empresa',
									loadDisabled: true,
									id: 'id_empresa_usuarios_empresas_sistema',
									button_id: 'button_id_empresa_usuarios_empresas_sistema',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Empresa'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_id_empresa_usuarios_empresas_sistema',
									combo_id: 'id_empresa_usuarios_empresas_sistema',
									action: 'reset_combo'
								},
								{
									xtype: 'buttonadd',
									tabela: 'Empresas_Sistema',
									action: 'add_win'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'controle',
							hidden: true,
							id: 'controle_usuarios_empresas_sistema',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_usuarios_empresas_sistema',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_usuarios_empresas_sistema',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Fechar/Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_usuarios_empresas_sistema',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});