/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_respostas.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addos_respostaswin',

	id: 'AddOs_RespostasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 600,
	height: 300,
	title: 'Cadastrar Resposta para a OS',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormOs_Respostas',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/os_respostas/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboUsuarios',
									name: 'usuario',
									id: 'usuario_os_respostas',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Usuário',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'textareafield',
							name: 'conteudo',
							id: 'conteudo_os_respostas',
							anchor: '100%',
							allowBlank: false,
							height: 165,
							fieldLabel: 'Descrição',
							fieldStyle: 'text-transform:uppercase',
							listeners: {
								afterrender: function(fld) {
									fld.focus(true, 500);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'os_nro',
							hidden: true,
							id: 'os_os_respostas',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_os_respostas',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_os_respostas',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_os_respostas',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_os_respostas').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_os_respostas',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
