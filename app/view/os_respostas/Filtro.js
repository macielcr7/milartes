/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_respostas.Filtro', {
	extend: 'Ext.window.Window',
	alias: 'widget.filteros_respostaswin',

	id: 'FilterOs_RespostasWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	
	title: 'Filtro de Os_Respostas',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterOs_Respostas',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboUsuarios',
									name: 'usuario',
									id: 'usuario_filter_os_respostas',
									button_id: 'button_usuario_filter_os_respostas',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Usuario'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_usuario_filter_os_respostas',
									combo_id: 'usuario_filter_os_respostas',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboOs',
									name: 'os',
									id: 'os_filter_os_respostas',
									button_id: 'button_os_filter_os_respostas',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Os'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_os_filter_os_respostas',
									combo_id: 'os_filter_os_respostas',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'conteudo',
							id: 'conteudo_filter_os_respostas',
							anchor: '100%',
							fieldLabel: 'Conteudo'
						},
						{
							xtype: 'fieldcontainer',
							anchor: '100%',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							labelAlign: 'top',
							labelStyle: 'font-weight: bold;font-size: 11px;',
							fieldLabel: 'Data Hora',
							items: [
								{
									xtype: 'datefield',
									format: 'd/m/Y',
									flex: 1,
									id: 'data_hora_date_filter_os_respostas',
									name: 'data_hora_date',
									margins: '0 5 0 0',
									hideLabel: true
								},
								{
									xtype: 'textfield',
									mask: '99:99:99',
									plugins: 'textmask',
									returnWithMask: true,
									flex: 1,
									id: 'data_hora_time_filter_os_respostas',
									name: 'data_hora_time',
									hideLabel: true
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_os_respostas',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							id: 'button_resetar_clientes',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});
