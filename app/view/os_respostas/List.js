/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_respostas.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.os_respostaslist',
	requires: [
		'ShSolutions.store.StoreOs_Respostas'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'bt_respostas_os',

	id: 'List-Os_Respostas',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Respostas da OS Selecionada',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridos_respostaslist'
				}
			]
		});

		me.callParent(arguments);
	}
});