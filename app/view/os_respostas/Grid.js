/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.os_respostas.Grid', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.gridos_respostaslist',
	requires: [
		'ShSolutions.store.StoreOs_Respostas'
	],
	id: 'GridOs_Respostas',
	store: 'StoreOs_Respostas',
	height: 350,

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			loadDisabled: true,
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			forceFit: true,
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'id',
					hidden: true,
					format: '0',
					text: 'Id',
					width: 140
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data/Hora',
					width: 120
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome_usuario',
					text: 'Usuario',
					width: 140
				},
				{
					xtype: 'numbercolumn',
					dataIndex: 'usuario',
					hidden: true,
					format: '0',
					text: 'Usuario',
					width: 140
				},
				{
					xtype: 'numbercolumn',
					dataIndex: 'os_nro',
					hidden: true,
					format: '0',
					text: 'Nro OS',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'conteudo',
					text: 'Descrição',
					width: 380
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreOs_Respostas',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_os_respostas',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_os_respostas',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_os_respostas',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});