/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_estados.Edit', {
    extend: 'Ext.window.Window',
	alias: 'widget.addcorreios_estadoswin',

    id: 'AddCorreios_EstadosWin',
    layout: {
        type: 'fit'
    },
	maximizable: false,
    minimizable: true,

    title: 'Cadastro de Estados',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'FormCorreios_Estados',
                    bodyPadding: 10,
                    autoScroll: true,
                    method: 'POST',
                    url : 'server/modulos/correios_estados/save.php',
                    items: [
						{
							xtype: 'textfield',
							name: 'uf',
							id: 'uf_correios_estados',
							anchor: '100%',
							fieldLabel: 'UF - 2 Caracteres',
							maxLength: 2,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_correios_estados',
							anchor: '100%',							
							fieldLabel: 'Descrição',
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%',	
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboAtivo',
									name: 'ativo',
									value: 'S',
									loadDisabled: true,
									id: 'ativo_Correios_Estados',
									button_id: 'button_ativo_Correios_Estados',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Ativo',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_ativo_Correios_Estados',
									combo_id: 'ativo_Correios_Estados',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_correios_estados',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_correios_estados',
							value: 'INSERIR',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            id: 'button_resetar_correios_estados',
                            iconCls: 'bt_cancel',
                            handler: function() {
                            	me.close(this);
                            },
                            //action: 'resetar',
                            text: 'Cancelar'
                        },
                        {
                            xtype: 'button',
                            id: 'button_salvar_correios_estados',
                            iconCls: 'bt_save',
                            action: 'salvar',
                            text: 'Salvar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);

    }

});
