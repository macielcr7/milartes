/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.setor_estoque.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addsetor_estoquewin',

	id: 'AddSetor_EstoqueWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 500,
	title: 'Cadastro de Setor de Estoque',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormSetor_Estoque',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/setor_estoque/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_setor_estoque',
									anchor: '100%',
									fieldLabel: 'Descrição',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'combobox',
									store: 'StoreComboAtivo',
									name: 'ativo',
									id: 'ativo_setor_estoque',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									value: 'S',
									anchor: '100%',
									fieldLabel: 'Ativo'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							margin: '0 5 0 0',
							id: 'id_setor_estoque',
							value: 0,
							anchor: '100%',
							fieldLabel: 'Cod Feriado'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_setor_estoque',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_setor_estoque',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_setor_estoque',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});