/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.fornecedores_endereco.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.fornecedores_enderecolist',
	requires: [
		'ShSolutions.store.StoreFornecedores_Endereco'
	],
	
	iconCls: 'fornecedores_endereco',

	id: 'GridFornecedores_Endereco',
	store: 'StoreFornecedores_Endereco',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	height: 350,
	title: 'Endereços',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'controle',
					hidden: true,
					format: '0',
					text: 'Controle',
					width: 70
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_endereco',
					text: 'Tipo Endereço',
					width: 90
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'loc_nome',
					text: 'Cidade / Estado',
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nome',
					text: 'Endereço',
					width: 350
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'bairro_nome',
					text: 'Bairro / Loteamento',
					width: 170
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ponto_referencia',
					text: 'Ponto de Referência',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cep',
					renderer : Ext.util.Format.maskRenderer('99.999-999'),
					text: 'Cep',
					width: 70
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cx_postal',
					text: 'Cx Postal',
					width: 65
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Cadastro',
					width: 120
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreFornecedores_Endereco',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_fornecedores_endereco',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_fornecedores_endereco',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_fornecedores_endereco',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});