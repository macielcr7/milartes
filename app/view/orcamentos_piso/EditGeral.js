/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_piso.EditGeral', {
	extend: 'Ext.window.Window',
	alias: 'widget.addageral_orcamentos_pisowin',

	id: 'AddGeral_Orcamentos_PisoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 550,
	title: 'Geral',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormGeral_Orcamentos_Piso',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produtos_orcamentos_piso/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutos',
									name: 'id_produto',
									id: 'id_produto_geral_orcamentos_piso',
									flex: 1,
									loadDisabled: true,
									queryMode: 'remote',
									triggerAction: 'all',
									typeAhead: false,
									minChars: 3,
									anchor: '100%',
									fieldLabel: 'Produto',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(true, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									//sandriii
									xtype: 'combobox',
									store: 'StoreComboProdutosCores',
									name: 'id_cor',
									loadDisabled: true,
									disabled: true,
									hidden: true,
									id: 'cor_geral_orcamentos_piso',
									anchor: '100%',
									fieldLabel: 'Cor'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'quantidade',
									id: 'quantidade_geral_orcamentos_piso',
									margin: '0 5 0 0',
									flex: 1,
									anchor: '100%',
									minValue: 0.1,
									selectOnFocus: true,
									fieldLabel: 'Quantidade'
								},
								{
									xtype: 'textfield',
									name: 'valor_unitario',
									id: 'valor_unitario_geral_orcamentos_piso',
									flex: 1,
									anchor: '100%',	
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Valor Unitário'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'possui_ambiente',
							hidden: true,
							id: 'possui_ambiente_geral_orcamentos_piso',
							value: 'N',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_produtos_orcamentos_piso',
							hidden: true,
							id: 'id_produtos_orcamentos_piso_geral_orcamentos_piso',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'item_nro',
							hidden: true,
							value: '0',
							id: 'item_nro_geral_orcamentos_piso',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_geral_orcamentos_piso',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_geral_orcamentos_piso',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_geral_orcamentos_piso',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});