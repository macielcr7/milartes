/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.orcamentos_piso.GridAmbientes', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridambientes_orcamentos_pisolist',
	requires: [
		'ShSolutions.store.StoreAmbientes_Orcamentos_Piso'
	],
	id: 'GridAmbientes_Orcamentos_Piso',
	store: 'StoreAmbientes_Orcamentos_Piso',
	height: 500,

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			forceFit: true,			
			columns: [
				{xtype: 'rownumberer'},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ordem',
					text: 'ID',
					width: 60,
					hidden: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ambiente',
					text: 'Ambiente/Descrição',					
					width: 140,
					renderer: function(v, m, r){
						if(r.get('sn_l')=='S'){
							if(r.get('desc_pai')!=null && r.get('desc_pai')!=undefined){
								return r.get('desc_pai')+' > '+v;
							}
						}

						return v;
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'comprimento',
					text: 'Comprimento Arred. (m)',
					width: 140,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'comprimento_real',
					text: 'Comprimento Real (m)',
					hidden: true,
					width: 140,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'largura',
					text: 'Largura Arred. (m)',
					width: 110,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'largura_real',
					text: 'Largura Real (m)',
					hidden: true,
					width: 140,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'area',
					text: 'Total (m²)',
					width: 80,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacoes',
					text: 'Observações',
					width: 180,
					sortable: false,
					fixed: true
				}
        
			],
            dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_ambientes_orcamentos_piso',
							iconCls: 'bt_add',								
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_ambientes_orcamentos_piso',
							iconCls: 'bt_edit',							
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_ambientes_orcamentos_piso',
							iconCls: 'bt_del',							
							action: 'deletar',
							text: 'Deletar'
						}			
					]
				}
			]
		});

		me.callParent(arguments);
	}
});