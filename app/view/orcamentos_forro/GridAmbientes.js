/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.orcamentos_forro.GridAmbientes', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridambientes_orcamentos_forrolist',
	requires: [
		'ShSolutions.store.StoreAmbientes_Orcamentos_Forro'
	],
	id: 'GridAmbientes_Orcamentos_Forro',
	store: 'StoreAmbientes_Orcamentos_Forro',
	height: 500,

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			forceFit: true,			
			columns: [
				{xtype: 'rownumberer'},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ordem',
					text: 'ID',
					width: 60,
					hidden: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ambiente',
					text: 'Ambiente/Descrição',					
					width: 140,
					renderer: function(v, m, r){
						if(r.get('sn_l')=='S'){
							if(r.get('desc_pai')!=null && r.get('desc_pai')!=undefined){
								return r.get('desc_pai')+' > '+v;
							}
						}

						return v;
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_acessorio',
					text: 'Acessório',
					hidden: false,
					width: 120,
					renderer: function(v, r, m){
						if(v=='MC'){
							return 'MEIA CANA';
						}
						else if(v=='RF'){
							return 'RODA-FORRO (PERFIL U)';
						}
						else if(v=='EH'){
							return 'EMENDA H';
						}
						else if(v=='EHF'){
							return 'EMENDA H FLEXÍVEL';
						}
						else if(v=='SA'){
							return '<font color=\"red\">SEM ACESSÓRIO</font>';
						}
						else{
							return v;
						}
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'comprimento',
					text: 'Comprimento Arred. (m)',
					width: 140,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'comprimento_real',
					text: 'Comprimento Real (m)',
					hidden: true,
					width: 140,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'largura',
					text: 'Largura Arred. (m)',
					width: 110,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'largura_real',
					text: 'Largura Real (m)',
					hidden: true,
					width: 140,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'area',
					text: 'Total (m²)',
					width: 80,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'descricao_pecas',
					text: 'Qtd de Peças',
					width: 100,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacoes',
					text: 'Observações',
					width: 180,
					sortable: false,
					fixed: true
				}
        
			],
            dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_ambientes_orcamentos_forro',
							iconCls: 'bt_add',								
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_ambientes_orcamentos_forro',
							iconCls: 'bt_edit',							
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_ambientes_orcamentos_forro',
							iconCls: 'bt_del',							
							action: 'deletar',
							text: 'Deletar'
						}			
					]
				}
			]
		});

		me.callParent(arguments);
	}
});