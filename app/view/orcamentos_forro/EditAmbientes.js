/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_forro.EditAmbientes', {
	extend: 'Ext.window.Window',
	alias: 'widget.addambientes_orcamentos_forrowin',

	id: 'AddAmbientes_Orcamentos_ForroWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Ambiente Orçamento - Forro',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAmbientes_Orcamentos_Forro',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/ambientes_orcamentos_forro/save.php',
					items: [
						{
							xtype: 'combobox',
							store: 'StoreAmbientesNomes',
							allowBlank: false,
							flex: 1,
							triggerAction: 'all',
							typeAhead: false,
							mode: 'remote',
							minChars: 2,
							forceSelection: false,
							hideTrigger: false,
							queryMode: 'remote',
							anchor: '100%',
							name: 'ambiente',
							id: 'ambiente_ambientes_orcamentos_forro',
							fieldLabel: 'Ambiente', 
							maxLength: 100,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase',
							listeners: {
								afterrender: function(fld) {
									fld.focus(true, 1100);
								},
								change:function(field){
									if (field.getValue() != null) {
										field.setValue(field.getValue().toUpperCase());
									}
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'comprimento_real',
							id: 'comprimento_ambientes_orcamentos_forro',
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							selectOnFocus: true,
							fieldLabel: 'Comprimento'
						},
						{
							xtype: 'textfield',
							name: 'largura_real',
							id: 'largura_ambientes_orcamentos_forro',
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							selectOnFocus: true,
							fieldLabel: 'Largura'
						},
						{
							xtype: 'combobox',
							store: 'StoreComboAmbienteOrcamentoForroL',
							name: 'sn_l',
							id: 'sn_l_ambientes_orcamentos_forro',
							flex: 1,
							value: 'N',
							anchor: '100%',
							fieldLabel: 'L'
						},
						{
							xtype: 'combobox',
							store: 'StoreComboTipoAcessorioAmbienteOrcamentoForro',
							name: 'tipo_acessorio',
							id: 'tipo_acessorio_ambientes_orcamentos_forro',
							flex: 1,
							value: 'MC',
							anchor: '100%',
							fieldLabel: 'Tipo de Acessório'
						},
						{
							xtype: 'textarea',
							name: 'observacoes',
							allowBlank: true,
							id: 'observacoes_ambientes_orcamentos_forro',
							anchor: '100%',
							fieldLabel: 'Observações', 
							maxLength: 255,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'textfield',
							name: 'id_ambiente_pai',
							hidden: true,
							id: 'id_ambiente_pai_ambientes_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_ambientes_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_ambientes_orcamentos_forro',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_ambientes_orcamentos_forro',
							iconCls: 'bt_cancel',
							action: 'resetar',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_ambientes_orcamentos_forro',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});