/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.orcamentos_forro.GridProdutos', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridprodutos_orcamentos_forrolist',
	requires: [
		'ShSolutions.store.StoreProdutos_Orcamentos_Forro'
	],
	id: 'GridProdutos_Orcamentos_Forro',
	store: 'StoreProdutos_Orcamentos_Forro',
	height: 200,

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			columns: [
/*				{
					xtype: 'rownumberer'
				},
*/
				{
					xtype: 'gridcolumn',
					dataIndex: 'item_nro',
					text: '#',
					anchor: '100%',
					hidden: false,
					width: 40,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'produto',
					flex: 1,
					anchor: '100%',
					text: 'Produto',
					renderer: function(v, m, r){
						var descricao_produto = v;
						var nome_cor = "";
						var produto_pai = r.get('produto_pai');

						if(produto_pai != undefined && produto_pai != null && produto_pai != "") {
							produto_pai += ' > ';
						}
						
						if (r.get('cor') != "") {
							nome_cor += ' > <span style="background-color: '+r.get('css_cor')+'; padding:2px"> <b>'+r.get('cor')+'</b></span>';
						}

						//return produto_pai += descricao_produto += nome_cor;
						return descricao_produto += nome_cor;
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'quantidade',
					text: 'Quantidade',	
					anchor: '100%',
					width: 100,
					renderer: function(v, m, r){
						//if(r.get('tipo_produto')=='A'){ inverter o convertFloatMoeda
						if(r.get('sigla_um')=='M²' || r.get('sigla_um')=='M' || r.get('sigla_um')=='KM' || r.get('sigla_um')=='L'){
							return converteFloatMoeda(v)+' '+r.get('sigla_um');
						}
						else{
							return v+' '+r.get('sigla_um');
						}
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_unitario',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Unitário',
					anchor: '100%',
					width: 100,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_total',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Total',
					anchor: '100%',
					width: 120,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_produto',
					text: 'Tipo Produto',
					anchor: '100%',
					width: 140,
					renderer: function(v, m, r){
						if(v=='A'){
							return 'Acessório';
						}
						else if(v=='P'){
							return 'Produto';
						}
						else if(v=='I'){
							return 'Produto/Instalado';
						}
						else if(v=='S'){
							return 'Serviço';
						}
					},
					sortable: false,
					fixed: true
				}
		
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_produtos_orcamentos_forro',
							iconCls: 'bt_add',
							action: 'adicionar',
							text: 'Produto/Produto Instalado'
						},
						{
							xtype: 'button',
							id: 'button_add_acessorio_produtos_orcamentos_forro',
							iconCls: 'bt_add',
							action: 'add_acessorio',
							text: 'Adicionar Acessório/Produto'
						},
						{
							xtype: 'button',
							id: 'button_add_geral_produtos_orcamentos_forro',
							iconCls: 'bt_add',
							action: 'add_geral',
							text: 'Adicionar Geral'
						},
						{
							xtype: 'button',
							id: 'button_edit_produtos_orcamentos_forro',
							iconCls: 'bt_edit',
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_produtos_orcamentos_forro',
							iconCls: 'bt_del',
							action: 'deletar',
							text: 'Deletar'
						},
						{
							xtype: 'button',
							id: 'button_copiar_produtos_orcamentos_forro',
							iconCls: 'bt_copiar',
							//hidden: true,
							action: 'copiar_item_orcamento',
							text: 'Copia o Ítem'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});