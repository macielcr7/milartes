/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_forro.EditProdutos', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addprodutos_orcamentos_forrowin',

	id: 'AddProdutos_Orcamentos_ForroWin',
	layout: {
		type: 'fit'
	},
	maximizable: true,
	minimizable: true,
	closable: false,
	width: 1000,
	
	title: 'Cadastro de Produto Orçamento - Forro',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProdutos_Orcamentos_Forro',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produtos_orcamentos_forro/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									margins: '0 5 0 0',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											id: 'step_1_produtos_orcamentos_forro',
											margins: '0 5 0 0',
											anchor: '100%', 
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												/*{
													xtype: 'combobox',
													store: 'StoreComboProdutosForrosssssss',
													name: 'id_produtosssssssss',
													id: 'id_produto_produtos_orcamentos_forrosssssssssss',
													button_id: 'button_id_produto_produtos_orcamentos_forro',
													flex: 1,
													loadDisabled: true,
													anchor: '100%',
													margins: '0 5 0 0',
													fieldLabel: 'Produto',
													listeners: {
														afterrender: function(fld) {
															fld.focus(true, 800);
														}
													}
												},*/
												{
													xtype: 'combobox',
													store: 'StoreComboProdutosForro',
													name: 'id_produto',
													id: 'id_produto_produtos_orcamentos_forro',
													flex: 1,
													loadDisabled: true,
													queryMode: 'remote',
													triggerAction: 'all',
													typeAhead: false,
													minChars: 3,
													anchor: '100%',
													margins: '0 5 0 0',
													fieldLabel: 'Produto',
													fieldStyle: 'text-transform:uppercase',
													selectOnFocus: true,
													listeners: {
														afterrender: function(fld) {
															fld.focus(true, 1300);
														}
													}
												},
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													margins: '0 5 0 0',
													layout: {
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboProdutosCores',
															name: 'id_cor',
															id: 'id_cor_produtos_orcamentos_forro',
															loadDisabled: true, 
															width: 120,
															selectOnFocus: true,
															fieldLabel: 'Cor'
														}
													]
												},
												{
													xtype: 'textfield',
													name: 'quantidade',
													id: 'quantidade_produtos_orcamentos_forro',
													disabled: true,
													hidden: true,
													margins: '0 5 0 0',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													selectOnFocus: true,
													selectOnFocus: true,
													fieldLabel: 'Quantidade'
												},
												{
													xtype: 'textfield',
													name: 'valor_unitario',
													id: 'valor_unitario_produtos_orcamentos_forro',
													allowBlank: true,
													width: 90,
													margins: '0 5 0 0',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													selectOnFocus: true,
													fieldLabel: 'Valor Unitário'
												}
											]
										},
										{
											xtype: 'buttonadd',
											id: 'proximo_step_2_produtos_orcamentos_forro',
											text: 'Próximo',
											action: 'add_ambientes'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldset',
							id: 'step_2_produtos_orcamentos_forro',
							disabled: true,
							title: 'Ambientes',
							items: [
								{
									xtype: 'gridambientes_orcamentos_forrolist',
									loadDisabled: true,
									height: 350
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									margins: '0 5 0 0',
									layout: {
										align: 'right',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'container',
											width: 860,
											height: 1
										},
										{
											xtype: 'textfield',
											name: 'total_area',
											id: 'total_m2_orcamentos_forro',
											allowBlank: true,
											readOnly: true,
											flex: 1,
											anchor: '100%',
											mask: '#9.999.990,00',
											plugins: 'textmask',
											money: true,
											fieldStyle: 'background-color: #FFFF99; background-image: none;',
											fieldLabel: 'Total em (m²)'
										}
									]
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id_produtos_orcamentos_forro',
							hidden: true,
							id: 'id_produtos_orcamentos_forro_produtos_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_orcamento',
							hidden: true,
							id: 'id_orcamento_produtos_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'item_nro',
							hidden: true,
							value: '0',
							id: 'item_nro_produtos_orcamentos_forro',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'possui_ambiente',
							hidden: true,
							id: 'possui_ambiente_produtos_orcamentos_forro',
							value: 'S',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_produtos_orcamentos_forro',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produtos_orcamentos_forro',
							iconCls: 'bt_cancel',
							action: 'cancelar_ambientes',
							/*handler: function() {
								Ext.Msg.confirm('Confirmar', 'Deseja Cancelar?', function(btn){
									if(btn=='yes'){
										me.close(btn);
									}
								});
							},*/
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produtos_orcamentos_forro',
							iconCls: 'bt_save',
							action: 'salvar',
							disabled: true,
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
