/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_forro.EditAcessorios', {
	extend: 'Ext.window.Window',
	alias: 'widget.addacessorios_orcamentos_forrowin',

	id: 'AddAcessorios_Orcamentos_ForroWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 550,
	title: 'Acessórios',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAcessorios_Orcamentos_Forro',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produtos_orcamentos_forro/save.php',
					items: [
						{
							xtype: 'combobox',
							store: 'StoreComboProdutosAcessoriosForro',
							name: 'id_produto',
							id: 'id_produto_acessorios_orcamentos_forro',
							button_id: 'button_id_produto_acessorios_orcamentos_forro',
							flex: 1,
							loadDisabled: true,
							queryMode: 'remote',
							triggerAction: 'all',
							typeAhead: false,
							minChars: 3,
							anchor: '100%',
							margins: '0 5 0 0',
							fieldLabel: 'Tipo de Acessório',
							fieldStyle: 'text-transform:uppercase',
							listeners: {
								afterrender: function(fld) {
									fld.focus(true, 1000);
								}
							}
						},
						{
							xtype: 'combobox',
							store: 'StoreComboProdutosCores',
							name: 'id_cor',
							loadDisabled: true,
							disabled: true,
							hidden: true,
							id: 'cor_acessorios_orcamentos_forro',
							anchor: '100%',
							fieldLabel: 'Cor'
						},
						{
							xtype: 'numberfield',
							name: 'quantidade',
							//regex: new RegExp('^\\d{1,}[,05]*$'),
							id: 'quantidade_acessorios_orcamentos_forro',
							anchor: '100%',
							minValue: 0.1,
							selectOnFocus: true,
							//step: 0.5,
							fieldLabel: 'Quantidade'
						},
						{
							xtype: 'textfield',
							name: 'valor_unitario',
							id: 'valor_unitario_acessorios_orcamentos_forro',
							anchor: '100%',
							flex: 1,
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							selectOnFocus: true,
							fieldLabel: 'Valor Unitário'
						},
						{
							xtype: 'textfield',
							name: 'id_produtos_orcamentos_forro_pai',
							hidden: true,
							id: 'id_produto_pai_acessorios_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_produtos_orcamentos_forro',
							hidden: true,
							id: 'id_acessorios_orcamentos_forro',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'item_nro',
							hidden: true,
							value: '0',
							id: 'item_nro_acessorios_orcamentos_forro',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_acessorios_orcamentos_forro',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_acessorios_orcamentos_forro',
							iconCls: 'bt_cancel',
							action: 'resetar',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_acessorios_orcamentos_forro',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});