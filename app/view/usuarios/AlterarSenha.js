/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.usuarios.AlterarSenha', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addusuarioswin',

	id: 'TrocarSenhaUsuariosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Alterar Senha do Usuário',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormUsuarios',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/usuarios/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'senha',
									margin: '0 5 0 0',
									flex: 1,
									id: 'senha_usuarios',
									allowEmpty: false,
									anchor: '100%',
									fieldLabel: 'Nova Senha',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_usuarios',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_usuarios',
							value: 'ALTERAR_SENHA',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_usuarios',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_usuarios',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});