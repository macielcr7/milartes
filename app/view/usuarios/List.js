/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.usuarios.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.usuarioslist',
	requires: [
		'ShSolutions.store.StoreUsuarios'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'usuarios',

	id: 'List-Usuarios',
	layout: {
		type: 'fit'
	},
	width: 1000,
	height: 500,
	title: 'Usuários do Sistema',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridUsuarios',
					store: 'StoreUsuarios',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							format: '0',
							text: 'Id',
							width: 40
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome',
							text: 'Nome',
							width: 240
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'perfil',
							text: 'Perfil',
							width: 100
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'perfil_id',
							hidden: true,
							format: '0',
							text: 'Perfil',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'email',
							text: 'Email',
							width: 170
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'login',
							text: 'Login',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'webmail_email',
							text: 'Webmail - Email',
							width: 120
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'webmail_senha',
							text: 'Senha Webmail',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'administrador',
							text: 'Administrador',
							renderer: function(v){
								switch(v){
									case '1':
									return 'Sim';
									break;
									case '2':
									return 'Não';
									break;
								}
							},
							width: 85
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'status',
							text: 'Status',
							renderer: function(v){
								switch(v){
									case '1':
									return 'Ativo';
									break;
									case '2':
									return 'Desativado';
									break;
								}
							},
							width: 70
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreUsuarios',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_usuarios',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_usuarios',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_usuarios',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar Usuário'
								},
								{
									xtype: 'button',
									id: 'button_alterar_senha_usuarios',
									iconCls: 'alterar_senha',
									hidden: true,
									disabled: true,
									action: 'alterar_senha',
									text: 'Trocar Senha do Usuário'
								},
								{
									xtype: 'button',
									id: 'button_modulos_usuarios',
									iconCls: 'bt_modulos_usuario',
									hidden: true,
									disabled: true,
									action: 'modulos',
									text: 'Permissões do Usuário'
								},
								{
									xtype: 'button',
									id: 'button_empresas_usuarios',
									iconCls: 'bt_empresas_usuario',
									hidden: true,
									disabled: true,
									action: 'empresas_usuarios',
									text: 'Empresas Associadas ao Usuário'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Status',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: '1',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: '2',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_usuarios',
									iconCls: 'bt_lupa',
									hidden: true,
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});