/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.unidades_medidas.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addunidades_medidaswin',

	id: 'AddUnidades_MedidasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 500,
	title: 'Cadastro de Unidade de Medida',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormUnidades_Medidas',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/unidades_medidas/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'sigla',
									margin: '0 5 0 0',
									allowBlank: false,
									id: 'sigla_unidades_medidas',
									width: 70,
									maxLength: 2,
									enforceMaxLength: true,
									fieldLabel: 'Sigla',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'textfield',
									name: 'unidade_medida',
									margin: '0 5 0 0',
									flex: 1,
									id: 'unidade_medida_unidades_medidas',
									anchor: '100%',
									fieldLabel: 'Descrição',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '16%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											id: 'ativo_unidades_medidas',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											value: 'S',
											anchor: '100%',
											margin: '0 5 0 0',
											fieldLabel: 'Ativo'
										}
									]
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							margin: '0 5 0 0',
							id: 'id_unidades_medidas',
							value: 0,
							anchor: '100%',
							fieldLabel: 'ID'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_unidades_medidas',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_unidades_medidas',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_unidades_medidas',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});