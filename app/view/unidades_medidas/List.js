/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.unidades_medidas.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.unidades_medidaslist',
	requires: [
		'ShSolutions.store.StoreUnidades_Medidas'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'unidades_medidas',
	id: 'List-Unidades_Medidas',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Unidades de Medidas',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridUnidades_Medidas',
					store: 'StoreUnidades_Medidas',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'ID',
							width: 60
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'sigla',
							text: 'Sigla',
							width: 60
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'unidade_medida',
							text: 'Unidade de Medida',
							width: 480
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
								}
							},
							width: 70
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 155
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreUnidades_Medidas',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_unidades_medidas',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_unidades_medidas',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_unidades_medidas',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_unidades_medidas',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_unidades_medidas').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_unidades_medidas',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativo',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});