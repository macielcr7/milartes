/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contas_bancarias.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcontas_bancariaswin',

	id: 'AddContas_BancariasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	title: 'Cadastro de Contas Bancárias',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormContas_Bancarias',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/contas_bancarias/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContasBancariasEmpresas',
									name: 'cod_empresa',
									allowBlank: false,
									id: 'cod_empresa_contas_bancarias',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Empresa',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [	
								{
									xtype: 'textfield',
									name: 'cod_banco',
									margin: '0 5 0 0',
									maxLength: 3,
									enforceMaxLength: true,
									id: 'cod_banco_contas_bancarias',
									width: 100,
									fieldLabel: 'Código do Banco',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'textfield',
									name: 'nome_banco',
									allowBlank: false,
									maxLength: 50,
									enforceMaxLength: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'nome_banco_contas_bancarias',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Nome do Banco'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'gerente_banco',
							allowBlank: true,
							margin: '0 5 0 0',
							id: 'gerente_banco_contas_bancarias',
							fieldStyle: 'text-transform:uppercase',
							anchor: '100%',
							fieldLabel: 'Gerente(s)'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'agencia',
									maxLength: 4,
									enforceMaxLength: true,
									allowBlank: false,
									margin: '0 5 0 0',
									flex: 1,
									id: 'agencia_contas_bancarias',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Agência'
								},
								{
									xtype: 'textfield',
									name: 'agencia_digito',
									maxLength: 2,
									enforceMaxLength: true,
									allowBlank: false,
									margin: '0 5 0 0',
									flex: 1,
									id: 'agencia_digito_contas_bancarias',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Dígito'
								},
								{
									xtype: 'textfield',
									name: 'conta_corrente',
									allowBlank: false,
									margin: '0 5 0 0',
									flex: 1,
									id: 'conta_corrente_contas_bancarias',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Conta Corrrente'
								},
								{
									xtype: 'textfield',
									name: 'conta_corrente_digito',
									allowBlank: false,
									maxLength: 2,
									enforceMaxLength: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'conta_corrente_digito_contas_bancarias',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Dígito'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											id: 'ativo_contas_bancarias',
											button_id: 'button_ativo_contas_bancarias',
											fieldStyle: 'text-transform:uppercase',
											width: 100,
											value: 'S',
											anchor: '100%',
											fieldLabel: 'Ativo'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_contas_bancarias',
											combo_id: 'ativo_contas_bancarias',
											action: 'reset_combo'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'id',
									hidden: true,
									margin: '0 5 0 0',
									id: 'id_contas_bancarias',
									value: 0,
									anchor: '100%',
									fieldLabel: 'ID'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_contas_bancarias',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contas_bancarias',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_contas_bancarias',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});