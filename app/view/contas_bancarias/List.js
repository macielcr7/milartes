/**
*	@Autor: Anderson Sandri
*	@Email: andersonsandri@gmail.com
**/


Ext.define('ShSolutions.view.contas_bancarias.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.contas_bancariaslist',
	requires: [
		'ShSolutions.store.StoreContas_Bancarias'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'contas_bancarias',

	id: 'List-Contas_Bancarias',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Contas Bancárias',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridContas_Bancarias',
					store: 'StoreContas_Bancarias',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'ID',
							width: 30
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cod_banco',
							text: 'Cód. Banco',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_banco',
							text: 'Nome Banco',
							width: 170
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'gerente_banco',
							text: 'Gerente',
							width: 175
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'agencia_conta',
							text: 'Agência / Conta',
							width: 150
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
							
								}
							},
							width: 50
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 140
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreContas_Bancarias',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_contas_bancarias',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_contas_bancarias',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_contas_bancarias',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'textfield',
									name: 'query',
								    allowBlank: true,
									id: 'query_contas_bancarias',
		                            enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
		                            listeners: {
		                                afterrender: function(fld) {
		                                    fld.focus(false, 1000);
		                                },
		                                keypress : function(textfield,eventObject){
		                                    if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_contas_bancarias').fireEvent('click');
		                                    }
		                                }
		                            }
								},
								{
									xtype: 'button',
									id: 'button_filter_contas_bancarias',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativo',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});