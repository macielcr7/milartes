/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.clientes_endereco.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addclientes_enderecowin',

	id: 'AddClientes_EnderecoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Endereço',
	width: 600,
	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormClientes_Endereco',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/clientes_endereco/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'hidden',
									name: 'cod_cliente',
									id: 'cod_cliente_clientes_endereco',
									fieldLabel: 'Cod Cliente'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboTipo_EnderecoClientes',
											margin: '0 5 0 0',
											name: 'tipo_endereco',
											id: 'tipo_endereco_clientes_endereco',
											button_id: 'button_tipo_endereco_clientes_endereco',
											flex: 1,
											value: 'RESIDENCIAL',
											anchor: '100%',
											fieldLabel: 'Tipo de Endereço', 
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_tipo_endereco_clientes_endereco',
											combo_id: 'tipo_endereco_clientes_endereco',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Estados',
											name: 'estado',
											id: 'estado_clientes_endereco',
											button_id: 'button_estado_clientes_endereco',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Estado', 
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_estado_clientes_endereco',
											combo_id: 'estado_clientes_endereco',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Estados',
											action: 'add_win'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Localidades',
											name: 'cidade',
											loadDisabled: true,
											disabled: true,
											minChars:2,
											id: 'cidade_clientes_endereco',
											button_id: 'button_cidade_clientes_endereco',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Cidade', 
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_cidade_clientes_endereco',
											combo_id: 'cidade_clientes_endereco',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Localidades',
											action: 'add_win'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [	
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Enderecos',
											name: 'logradouro',
											id: 'logradouro_clientes_endereco',
											button_id: 'button_logradouro_clientes_endereco',
											flex: 1,
											disabled: true,
											loadDisabled: true,
											queryMode: 'remote',
											anchor: '100%',
											fieldLabel: 'Endereço', 
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_logradouro_clientes_endereco',
											combo_id: 'logradouro_clientes_endereco',
											handler: function() {
												Ext.getCmp('endereco_clientes').setValue('');
												Ext.getCmp('bairro_nome_clientes').setValue('');
												Ext.getCmp('bairro_clientes').setValue('');
												Ext.getCmp('cep_clientes').setValue('');
												Ext.getCmp('ponto_ref_clientes').setValue('');
												Ext.getCmp('loteamento_clientes').setValue('');
											}
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Enderecos',
											action: 'add_win'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'bairro_nome',
									flex: 1,
									margin: '0 5 0 0',	
									allowBlank: true,
									readOnly: true,
									id: 'bairro_nome_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Bairro',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'textfield',
									name: 'bairro',
									flex: 1,
									hidden: true,
									allowBlank: true,
									id: 'bairro_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Bairro Id'
								},
								{
									xtype: 'textfield',
									name: 'num_end',
									flex: 1,
									id: 'num_end_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Num End'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'complemento',
									margin: '0 5 0 0',
									flex: 1,
									id: 'complemento_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Complemento', 
									allowBlank: true,
									maxLength: 100,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								},								
								{
									xtype: 'textfield',
									name: 'loteamento',
									flex: 1,
									id: 'loteamento_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Loteamento', 
									allowBlank: true,
									maxLength: 100,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'ponto_referencia',
									flex: 1,
									id: 'ponto_referencia_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Ponto de Referência', 
									allowBlank: true,
									maxLength: 255,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cep',
									margin: '0 5 0 0',
									allowBlank: true,
									id: 'cep_clientes_endereco',
									mask: '99.999-999',
									plugins: 'textmask',
									allowBlank: true, 
									flex: 1,
									anchor: '100%',
									fieldLabel: 'CEP'
								},
								{
									xtype: 'textfield',
									name: 'cx_postal',
									flex: 1,
									id: 'cx_postal_clientes_endereco',
									anchor: '100%',
									fieldLabel: 'Cx Postal', 
									allowBlank: true,
									maxLength: 10,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'controle',
							hidden: true,
							id: 'controle_clientes_endereco',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_clientes_endereco',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_clientes_endereco',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_clientes_endereco',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
