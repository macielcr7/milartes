/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addatributoswin',

	id: 'AddAtributosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Atributos',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAtributos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/atributos/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'atributo',
							id: 'atributo_atributos',
							anchor: '100%',							
							fieldLabel: 'Atributo'
						},
						{
							xtype: 'textfield',
							name: 'sigla',
							id: 'sigla_atributos',
							anchor: '100%',									
							allowBlank: true,							
							fieldLabel: 'Sigla'
						},
						{
							xtype: 'combobox',
							store: 'StoreComboAtivo',
							name: 'ativo',
							loadDisabled: true,
							id: 'ativo_atributos',
							flex: 1,
							anchor: '100%',
							value: 'S',
							fieldLabel: 'Atributo Ativo'
						},
						{
							xtype: 'hidden',
							name: 'id_atributos',
							hidden: true,
							id: 'id_atributos_atributos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_atributos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_atributos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_atributos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});