/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.atributoslist',
	requires: [
		'ShSolutions.store.StoreAtributos'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'atributos',

	id: 'List-Atributos',
	layout: {
		type: 'fit'
	},
	height: 500,
	width: 1180,
	title: 'Atributos para as Categorias de Produtos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridAtributos',
					store: 'StoreAtributos',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					forceFit: true,
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_atributos',
							format: '0',
							text: 'ID',
							width: 40
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'atributo',
							text: 'Atributo',
							width: 250
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'sigla',
							text: 'Sigla',
							width: 200
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'Sim';
									break;
									case 'N':
									return 'Não';
									break;
								}
							},
							width: 60
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cadastro',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data de Cadastro',
							width: 130
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrado Por',
							width: 190
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreAtributos',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_atributos',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_atributos',
									iconCls: 'bt_edit',
									hidden: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_atributos',
									iconCls: 'bt_del',
									hidden: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'button',
									id: 'button_filter_atributos',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});