/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.cores.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcoreswin',
	id: 'AddCoresWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Cores',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCores',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/cores/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_cores',
							anchor: '100%',
							fieldLabel: 'Descrição',
							listeners: {
								afterrender: function(fld) {
									fld.focus(true, 800);
								}
							}
						},
						{
							xtype: 'textfield',
							name: 'codigo',
							id: 'codigo_cores',
							anchor: '100%',
							value: '#FFFFFF',
							fieldLabel: 'Codigo'
						},
						{
							xtype: 'hidden',
							name: 'id_cores',
							hidden: true,
							id: 'id_cores_cores',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_cores',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_cores',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_cores',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});