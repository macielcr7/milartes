/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_departamentos_responsavel.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addos_departamentos_responsavelwin',

	id: 'AddOs_Departamentos_ResponsavelWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro: Responsavel pelo Departamento em OS',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormOs_Departamentos_Responsavel',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/os_departamentos_responsavel/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProdutos_CategoriasOs',
											name: 'id_depto',
											id: 'id_depto_os_departamentos_responsavel',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Departamento'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboUsuariosDeptoProdutosResponsavel',
											name: 'id_usuario',
											id: 'id_usuario_os_departamentos_responsavel',
											flex: 1,
											value: 'ADMINISTRADOR',
											anchor: '100%',
											fieldLabel: 'Usuário Responsável'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							anchor: '50%',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboEmpresasSistemaResponsavelDepto',
											name: 'id_empresa[]',
											multiSelect: true,
											id: 'id_empresa_os_departamentos_responsavel',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Empresa'
										}
									]
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_os_departamentos_responsavel',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_os_departamentos_responsavel',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_os_respostas',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_os_departamentos_responsavel',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});