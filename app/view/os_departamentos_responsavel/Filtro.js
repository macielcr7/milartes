/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_departamentos_responsavel.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filteros_departamentos_responsavelwin',

    id: 'FilterOs_Departamentos_ResponsavelWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro: OS - Responsável Pelo Departamento',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterOs_Departamentos_Responsavel',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    
								    margin: '0 5 0 0',								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboOs_Categorias',
		                                    name: 'id_categoria',
											id: 'id_categoria_filter_os_departamentos_responsavel',
											button_id: 'button_id_categoria_filter_os_departamentos_responsavel',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Categoria'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_id_categoria_filter_os_departamentos_responsavel',
		                                    combo_id: 'id_categoria_filter_os_departamentos_responsavel',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        },		                        
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboUsuarios',
		                                    name: 'id_usuario',
											id: 'id_usuario_filter_os_departamentos_responsavel',
											button_id: 'button_id_usuario_filter_os_departamentos_responsavel',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Usuario'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_id_usuario_filter_os_departamentos_responsavel',
		                                    combo_id: 'id_usuario_filter_os_departamentos_responsavel',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        }		                        

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    
								    margin: '0 5 0 0',								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboEmpresas_Sistema',
		                                    name: 'id_empresa',
											id: 'id_empresa_filter_os_departamentos_responsavel',
											button_id: 'button_id_empresa_filter_os_departamentos_responsavel',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Empresa'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_id_empresa_filter_os_departamentos_responsavel',
		                                    combo_id: 'id_empresa_filter_os_departamentos_responsavel',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        },		                        
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboUsuarios',
		                                    name: 'quem_incluiu',
											id: 'quem_incluiu_filter_os_departamentos_responsavel',
											button_id: 'button_quem_incluiu_filter_os_departamentos_responsavel',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Quem Incluiu'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_quem_incluiu_filter_os_departamentos_responsavel',
		                                    combo_id: 'quem_incluiu_filter_os_departamentos_responsavel',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        }		                        

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Inclusão',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_inclusao_date_filter_os_departamentos_responsavel',
											name: 'data_inclusao_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_inclusao_time_filter_os_departamentos_responsavel',
											name: 'data_inclusao_time',
											hideLabel: true
										}
									]
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_os_departamentos_responsavel',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
