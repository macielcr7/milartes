/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.os_departamentos_responsavel.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.os_departamentos_responsavellist',
	requires: [
		'ShSolutions.store.StoreOs_Departamentos_Responsavel'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'os_departamentos_responsavel',
	id: 'List-Os_Departamentos_Responsavel',
	layout: {
		type: 'fit'
	},
	width: 980,
	height: 400,
	title: 'Responsável pelo Departamento em OS',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridOs_Departamentos_Responsavel',
					store: 'StoreOs_Departamentos_Responsavel',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'Id',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_departamento',
							text: 'Departamento',
							width: 190
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_fantasia',
							text: 'Empresa',
							width: 230
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'responsavel',
							text: 'Responsável',
							width: 200
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'id_empresa',
							hidden: true,
							format: '0',
							text: 'Empresa',
							width: 50
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'data_cadastro',
							text: 'Data Cadastrado',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por',
							text: 'Cadastrado Por',
							width: 200
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreOs_Departamentos_Responsavel',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_os_departamentos_responsavel',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_os_departamentos_responsavel',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_os_departamentos_responsavel',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Deletar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});