/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.agenda_telefonica.Edit', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addagenda_telefonicawin',

	requires: [
		'ShSolutions.store.StoreAgenda_Telefonica_Contato'
	],

	id: 'AddAgenda_TelefonicaWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 900,
	title: 'Cadastro de Contato Telefônico',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralAgendaTelefonica',
					items: [
						{
							xtype: 'form',
							title:'Formulario Principal',
							id: 'FormAgenda_Telefonica',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/agenda_telefonica/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'tipo_contato',
											margin: '0 5 0 0',
											flex: 1,
											hidden: true,
											id: 'tipo_contato_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Tipo Contato'
										},	
										{
											xtype: 'textfield',
											name: 'razao_social',
											margin: '0 5 0 0',
											flex: 1,
											id: 'razao_social_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Razão Social',
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'nome_fantasia',
											flex: 1,
											allowBlank: true,
											id: 'nome_fantasia_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Nome Fantasia', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									id: 'fieldcontainer_jAgendaTelefone',
									items: [
										{
											xtype: 'textfield',
											name: 'pessoa_contato',
											flex: 1,
											allowBlank: true,
											id: 'pessoa_contato_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Pessoa de Contato', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									hidden: true,
									id: 'fieldcontainer_fAgendaTelefone',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {

												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'nome_completo',
													margin: '0 5 0 0',
													hidden: true,
													flex: 1,
													id: 'nome_completo_agenda_telefonica',
													anchor: '100%',	
													fieldLabel: 'Nome Completo', 
													maxLength: 255,
													enforceMaxLength: true,
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'combobox',
													store: 'StoreComboSexo',
													name: 'sexo',
													allowBlank: true,
													loadDisabled: true,
													id: 'sexo_agenda_telefonica',
													value: 'M',
													width: '100',
													fieldLabel: 'Sexo',
													fieldStyle: 'text-transform:uppercase'
												}
											]
										}										
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProfissaoAgenda_Telefonica',
											margin: '0 5 0 0',
											hidden: true,
											allowBlank: true,
											flex: 1,
											triggerAction:'all',
											typeAhead:false,
											mode:'remote',
											minChars:2,
											forceSelection: false,
											hideTrigger:true,
											queryMode: 'remote',
											anchor: '100%',
											name: 'profissao',
											id: 'profissao_agenda_telefonica',
											fieldLabel: 'Profissão', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Estados',
													name: 'estado',
													allowBlank: true,
													id: 'estado_agenda_telefonica',
													button_id: 'button_estado_agenda_telefonica',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Estado',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_estado_agenda_telefonica',
													combo_id: 'estado_agenda_telefonica',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Estados',
													action: 'add_win'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Localidades',
													name: 'cidade',
													allowBlank: true,
													loadDisabled: true,
													minChars:2,
													disabled: true,
													id: 'cidade_agenda_telefonica',
													button_id: 'button_cidade_agenda_telefonica',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Cidade',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_cidade_agenda_telefonica',
													combo_id: 'cidade_agenda_telefonica',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Localidades',
													action: 'add_win'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboCorreios_Enderecos',
													name: 'endereco',
													loadDisabled: true,
													allowBlank: true,
													hideTrigger: false,
													minChars:2,
													queryMode: 'remote',
													disabled: true,
													id: 'endereco_agenda_telefonica',
													button_id: 'button_endereco_agenda_telefonica',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Endereço',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_endereco_agenda_telefonica',
													combo_id: 'endereco_agenda_telefonica',
													handler: function() {
														Ext.getCmp('endereco_agenda_telefonica').setValue('');
														Ext.getCmp('bairro_nome_agenda_telefonica').setValue('');
														Ext.getCmp('bairro_agenda_telefonica').setValue('');
														Ext.getCmp('cep_agenda_telefonica').setValue('');
														Ext.getCmp('ponto_ref_agenda_telefonica').setValue('');
														Ext.getCmp('loteamento_agenda_telefonica').setValue('');
													}
												},
												{
													xtype: 'buttonadd',
													tabela: 'Correios_Enderecos',
													action: 'add_win'
												}
											]
										},	
										{
											xtype: 'textfield',
											name: 'bairro_nome',
											width: 200,
											margin: '0 5 0 0',	
											allowBlank: true,
											readOnly: true,
											id: 'bairro_nome_agenda_telefonica',
											fieldLabel: 'Bairro',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'bairro',
											flex: 1,
											hidden: true,
											allowBlank: true,
											id: 'bairro_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Bairro Id'
										},
										{
											xtype: 'textfield',
											name: 'nro_end',
											width: 80,
											allowBlank: true,
											id: 'nro_end_agenda_telefonica',
											fieldLabel: 'Nro End'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'complemento',
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'complemento_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Complemento',
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'ponto_ref',	
											margin: '0 5 0 0',
											flex: 1,
											allowBlank: true,
											id: 'ponto_ref_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Ponto Ref', 
											maxLength: 255,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'textfield',
											name: 'loteamento',
											flex: 1,
											allowBlank: true,
											id: 'loteamento_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Loteamento',
											maxLength: 50,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cep',
											margin: '0 5 0 0',
											id: 'cep_agenda_telefonica',
											mask: '99.999-999',
											plugins: 'textmask',
											allowBlank: true, 
											flex: 1,
											anchor: '100%',
											fieldLabel: 'CEP'
										},
										{
											xtype: 'textfield',
											name: 'cx_postal',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'cx_postal_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Cx Postal',
											maxLength: 10,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboAtivo',
													name: 'ativo',
													loadDisabled: true,
													allowBlank: true,
													id: 'ativo_agenda_telefonica',
													flex: 1,
													value: 'S',
													anchor: '100%', 
													fieldLabel: 'Ativo'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'palavras_chaves',
											flex: 1,
											allowBlank: true,
											id: 'palavras_chaves_agenda_telefonica',
											anchor: '100%',
											fieldLabel: 'Palavras Chaves (separar por vírgula)', 
											maxLength: 100,
											enforceMaxLength: true,
											fieldStyle: 'text-transform:uppercase'
										}
									]
								},
								{
									xtype: 'textarea',
									name: 'observacoes',
									allowBlank: true,
									id: 'observacoes_agenda_telefonica',
									anchor: '100%',
									fieldLabel: 'Observações',
									fieldStyle: 'text-transform:uppercase'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'cadastrado_por',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											allowBlank: true,
											id: 'cadastrado_por_agenda_telefonica',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Cadastro Efetuado por:'
										},
										{
											xtype: 'textfield',
											name: 'alterado_por',
											flex: 1,
											anchor: '100%',
											allowBlank: true,
											id: 'alterado_por_agenda_telefonica',
											readOnly: true,
											hidden: true,
											fieldLabel: 'Última Alteração Cadastral Efetuada por:'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'cod_contato',
									hidden: true,
									id: 'cod_contato_agenda_telefonica',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'textfield',
									name: 'action',
									hidden: true,
									id: 'action_agenda_telefonica',
									value: 'INSERIR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'agenda_telefonica_contatolist',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_agenda_telefonica',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_agenda_telefonica',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});