/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1220)
{
	var tamanho = 1220;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.agenda_telefonica.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.agenda_telefonicalist',
	requires: [
		'ShSolutions.store.StoreAgenda_Telefonica'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'agenda_telefonica',

	id: 'List-Agenda_Telefonica',
	layout: {
		type: 'fit'
	},

	width: tamanho,
	height: 550,
	title: 'Agenda Telefônica',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridAgenda_Telefonica',
					store: 'StoreAgenda_Telefonica',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
								
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_contato',
							format: '0',
							text: 'Cod',
							width: 40
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_completo',
							text: 'Nome / Razão Social',
							width: 365,
							renderer : function(v, m, r){
								if(r.get('tipo_contato')=='F'){
									return v;
								}
								else{
									if (r.get('nome_fantasia') != "") {
										var texto = r.get('razao_social')+' ('+r.get('nome_fantasia')+')';
										return texto;
									}
									else
									{
										return r.get('razao_social');
									}
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'loc_nome',
							text: 'Cidade / Estado',
							width: 130
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cidade',
							hidden: true,
							text: 'Cidade',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome',
							text: 'Endereço',
							width: 250
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'endereco',
							hidden: true,
							text: 'Endereço',
							width: 150
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'bairro_nome',
							text: 'Bairro / Loteamento',
							width: 160
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'bairro',
							hidden: true,
							text: 'Bairro',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'fones',
							text: 'Telefone(s)',
							width: 240
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreAgenda_Telefonica',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_agenda_telefonica1',
									iconCls: 'bt_add',
									type: 'F',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar PF'
								},
								{
									xtype: 'button',
									id: 'button_add_agenda_telefonica2',
									iconCls: 'bt_add',
									hidden: true,
									type: 'J',
									action2: 'adicionar_cli',
									action: 'adicionar',
									text: 'Adicionar PJ'
								},
								{
									xtype: 'button',
									id: 'button_edit_agenda_telefonica',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_agenda_telefonica',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Deletar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_agenda_telefonica',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 200);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_agenda_telefonica').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_agenda_telefonica',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{
									xtype: 'button',
									id: 'button_pdf_agenda_telefonica',
									iconCls: 'bt_pdf',
									hidden: true,
									action: 'gerar_pdf',
									text: 'Gerar PDF'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});