/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos_combustivel.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addveiculos_combustivelwin',

	id: 'AddVeiculos_CombustivelWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Abastecimentos',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormVeiculos_Combustivel',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/veiculos_combustivel/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'hodometro',
									flex: 1,
									margin: '0 5 0 0',
									mask: '#9.999.990.000',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									id: 'hodometro_veiculos_combustivel',
									anchor: '100%',
									fieldLabel: 'Hodômetro',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'combobox',
									name: 'tipo_combustivel',
									store: 'StoreComboCombustivelVeiculos',
									loadDisabled: true,
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: false,
									id: 'tipo_combustivel_veiculos_combustivel',
									anchor: '100%',
									fieldLabel: 'Tipo Combustivel',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboOsResponsavel',
									name: 'quem_abasteceu',
									margin: '0 5 0 0',
									allowBlank: false,
									id: 'quem_abasteceu_veiculos_combustivel',
									flex: 1,
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Abastecido Por (Motorista)'
								},
								{
									xtype: 'textfield',
									name: 'quantidade',
									flex: 1,
									id: 'quantidade_veiculos_combustivel',
									anchor: '100%',
									mask: '#9.999.990,000',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Quantidade'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'valor_abastecimento',
									margin: '0 5 0 0',
									flex: 1,
									id: 'valor_abastecimento_veiculos_combustivel',
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Valor Abastecimento'
								},
								{
									xtype: 'textfield',
									flex: 1,
									mask: '99/99/9999',
									returnWithMask: true,
									plugins: 'textmask',
									allowBlank: false,
									name: 'data_abastecimento',
									id: 'data_abastecimento_veiculos_combustivel',
									anchor: '100%',
									fieldLabel: 'Data Abastecimento',
									validator: function(value){
										if(value!="" && !validDate(value)){
											return 'Data Inválida...';
										}
										else{
											return true;
										}
									}
								}
							]
						},
						{
							xtype: 'numberfield',
							name: 'cod_veiculo',
							margin: '0 5 0 0',
							flex: 1,
							hidden: true,
							id: 'cod_veiculo_veiculos_combustivel',
							anchor: '100%',
							fieldLabel: 'Cod Veículo'
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_veiculos_combustivel',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_veiculos_combustivel',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_veiculos_combustivel',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_veiculos_combustivel',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});