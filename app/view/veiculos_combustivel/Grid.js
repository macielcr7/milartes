/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.veiculos_combustivel.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridveiculos_combustivellist',
	requires: [
		'ShSolutions.store.StoreVeiculos_Combustivel'
	],

	id: 'GridVeiculos_Combustivel',
	store: 'StoreVeiculos_Combustivel',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},

	columns: [
		{
			xtype: 'numbercolumn',
			dataIndex: 'id',
			hidden: true,
			format: '0',
			text: 'Id',
			width: 140
		},
		{
			xtype: 'numbercolumn',
			dataIndex: 'cod_veiculo',
			format: '0',
			hidden: true,
			text: 'Cod Veiculo',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'hodometro',
			text: 'Hodômetro',
			width: 90
		},
		{
			xtype: 'datecolumn',
			dataIndex: 'data_abastecimento',
			format: 'd/m/Y',
			renderer : Ext.util.Format.dateRenderer('d/m/Y'),
			text: 'Data Abastecimento',
			width: 120
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'valor_abastecimento',
			renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
			text: 'Valor Abastecimento',
			width: 120
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'tipo_combustivel',
			text: 'Tipo Combustível',
			width: 105
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'quantidade',
			text: 'Quantidade',
			width: 80
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'distancia_percorrida',
			text: 'Distância Percorrida',
			width: 120
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'consumo_medio',
			text: 'Consumo Médio',
			width: 100
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'quem_abasteceu',
			text: 'Abastecido Por (Motorista)',
			width: 180
		},
		{
			xtype: 'datecolumn',
			dataIndex: 'data_cadastro',
			format: 'd/m/Y H:i:s',
			renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
			text: 'Data Cadastro',
			width: 140
		}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: 'StoreVeiculos_Combustivel',
			dock: 'bottom'
		},
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_veiculos_combustivel',
					iconCls: 'bt_add',
					hidden: true,
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_veiculos_combustivel',
					iconCls: 'bt_edit',
					hidden: true,
					disabled: true,
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_veiculos_combustivel',
					iconCls: 'bt_del',
					hidden: true,
					disabled: true,
					action: 'deletar',
					text: 'Deletar'
				},
				{
					xtype: 'button',
					id: 'button_filter_veiculos_combustivel',
					iconCls: 'bt_lupa',
					action: 'filtrar',
					text: 'Filtrar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});