/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos_combustivel.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.veiculos_combustivellist',
	requires: [
		'ShSolutions.store.StoreVeiculos_Combustivel'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'veiculos_combustivel',

	id: 'List-Veiculos_Combustivel',
	layout: {
		type: 'fit'
	},
	height: 350,
	width: 1100,
	title: 'Abastecimentos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridveiculos_combustivellist'
				}
			]
		});

		me.callParent(arguments);
	}
});