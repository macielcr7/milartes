/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contas_pagar_pagtos.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.contas_pagar_pagtoslist',
	requires: [
		'ShSolutions.store.StoreContas_Pagar_Pagtos'
	],
	
	id: 'GridContas_Pagar_Pagtos',
	store: 'StoreContas_Pagar_Pagtos',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,
	height: 350,
	title: 'Pagtos Efetuados',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'id',
					hidden: true,
					format: '0',
					text: 'ID',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_pagto',
					text: 'Tipo Pagamento',
					width: 150,
					renderer: function(v, r, m){
						if(v=='1'){
							return 'DINHEIRO';
						}
						else if(v=='2'){
							return 'CHEQUE(S)';
						}
						else if(v=='3'){
							return 'DEPÓSITO/TRANSFERÊNCIA BANCÁRIA';
						}
						else if(v=='4'){
							return 'BOLETO/DUPLICATA';
						}
						else if(v=='5'){
							return 'PERMUTA';
						}
						else{
							return v;
						}
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_pago',
					text: 'Valor Pago',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					width: 90
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'data_pagto',
					text: 'Data Pagamento',
					format: 'd/m/Y',
					renderer : Ext.util.Format.dateRenderer('d/m/Y'),
					width: 110
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacao',
					text: 'Observações',
					width: 160
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'data_cadastro',
					text: 'Data Cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					width: 120
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'cadastrado_por_nome',
					text: 'Cadastrador Por',
					width: 130
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreContas_Pagar_Pagtos',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_contas_pagar_pagtos',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_del_contas_pagar_pagtos',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});