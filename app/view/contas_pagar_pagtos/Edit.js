/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contas_pagar_pagtos.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcontas_pagar_pagtoswin',

	id: 'AddContas_Pagar_PagtosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Pagamento de Contas à Pagar',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormContas_Pagar_Pagtos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/contas_pagar_pagtos/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'cod_conta',
							hidden: true,
							id: 'cod_conta_contas_pagar_pagtos',
							anchor: '100%',
							fieldLabel: 'Cod Conta'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboTipo_ContaContasPagar',
									name: 'tipo_pagto',
									loadDisabled: true,
									value: '',
									id: 'tipo_pagto_contas_pagar_pagtos',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Tipo de Pagamento',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'data_pagto',
									margin: '0 5 0 0',
									id: 'data_pagto_contas_pagar_pagtos',
									flex: 1,
									anchor: '100%',
									mask: '99/99/9999',
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Data Pagamento',
									validator: function(value){
										if(value!="" && !validDate(value)){
											return 'Data Inválida...';
										}
										else{
											return true;
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'valor_pago',
									allowBlank: false,
									id: 'valor_pago_contas_pagar_pagtos',
									flex: 1,
									mask: '#9.999.990,00',
									money: true,
									plugins: 'textmask',
									fieldLabel: 'Valor Pago'
								}
							]
						},
						{
							xtype: 'textarea',
							name: 'observacao',
							allowBlank: true,
							id: 'observacao_contas_pagar_pagtos',
							anchor: '100%',
							fieldLabel: 'Observação', 
							maxLength: 255,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_contas_pagar_pagtos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_contas_pagar_pagtos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contas_pagar_pagtos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_contas_pagar_pagtos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});