/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas_pagamentos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.vendas_pagamentoslist',
	requires: [
		'ShSolutions.store.StoreVendas_Pagamentos'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'vendas_pagamentos',

	id: 'List-Vendas_Pagamentos',
	layout: {
		type: 'fit'
	},
	height: 350,
	width: 900,
	title: 'Pagamentos Efetuados',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridvendas_pagamentoslist'
				}
			]
		});

		me.callParent(arguments);
	}
});