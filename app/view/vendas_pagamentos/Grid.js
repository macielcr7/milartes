/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.vendas_pagamentos.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridvendas_pagamentoslist',
	requires: [
		'ShSolutions.store.StoreVendas_Pagamentos'
	],
	id: 'GridVendas_Pagamentos',
	store: 'StoreVendas_Pagamentos',

	viewConfig: {
		autoScroll: true,
		loadMask: false,
		getRowClass: function(record) { 
			if(record.get('estornado')=='S'){
				return 'vermelho';
			}
			else{
				return '';
			}
		}
	},
	//forceFit: true,
	columns: [
		{
			xtype: 'datecolumn',
			dataIndex: 'data_cadastro',
			format: 'd/m/Y H:i:s',
			renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
			text: 'Data Cadastro',
			sortable: false,
			fixed: true,
			width: 130
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'valor_pago',
			renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
			text: 'Valor Pago',
			width: 100,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'forma_pagamento_descricao',
			text: 'Forma de Pagamento',
			width: 180,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'observacao',
			text: 'Observação',
			width: 300,
			sortable: false,
			fixed: false
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'cadastrado_por_nome',
			text: 'Cadastrado Por',
			width: 200,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'datecolumn',
			dataIndex: 'data_estorno',
			format: 'd/m/Y H:i:s',
			renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
			text: 'Data Estorno',
			width: 120,
			sortable: false,
			fixed: true
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'estornado_por_nome',
			text: 'Estornado Por',
			width: 200,
			sortable: false,
			fixed: true
		}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: 'StoreVendas_Pagamentos',
			dock: 'bottom'
		},
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_vendas_pagamentos',
					iconCls: 'bt_add',
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_estorna_vendas_pagamentos',
					iconCls: 'estorno_adiantamento1',
					action: 'estornar',
					disabled: true,
					text: 'Estonar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});