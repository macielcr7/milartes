/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
var data_desejada2 = new Date();
data_desejada2.setDate(data_desejada2.getDate());
//console.log(data_desejada2);

Ext.define('ShSolutions.view.vendas_pagamentos.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addvendas_pagamentoswin',

	id: 'AddVendas_PagamentosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 350,
	title: 'Efetuando Pagamento....',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormVendas_Pagamentos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/vendas_pagamentos/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'saldo_a_receber',
									flex: 1,
									id: 'saldo_a_receber_vendas_pagamentos',
									allowBlank: true,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									readOnly: true,
									fieldLabel: 'Saldo a Receber'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'valor_pago',
									flex: 1,
									id: 'valor_pago_vendas_pagamentos',
									allowBlank: false,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Valor',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboFormas_Pagto',
									name: 'forma_pagto',
									id: 'forma_pagto_vendas_pagamentos',
									flex: 1,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Forma de Pagamento',
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							id: 'fieldcontainer_vendas',
							items: [
								{
									xtype: 'numberfield',
									name: 'nro_parcelas',
									anchor: '100%',
									loadDisabled: true,
									disabled: true,
									hidden: true,
									allowBlank: false,
									minValue: 1,
									margin: '0 40 0 0',
									width: 110,
									id: 'nro_parcelas_vendas_pagamentos',
									fieldLabel: 'Nro de Parcelas'
								},
								{
									xtype: 'datefield',
									format: 'd/m/Y',
									value: data_desejada2,
									hidden: true,
									allowBlank: false,
									name: 'data_venda_cartao',
									id: 'data_venda_cartao_vendas_pagamentos',
									fieldLabel: 'Data Autorização Cartão'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'observacao',
									flex: 1,
									id: 'observacao_vendas_pagamentos',
									anchor: '100%',
									fieldLabel: 'Observacao',
									allowBlank: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							fieldLabel: 'ID',
							id: 'id_vendas_pagamentos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'cliente',
							margin: '0 5 0 0',
							flex: 1,
							hidden: true,
							id: 'cod_cliente_vendas_pagamentos',
							anchor: '100%',
							fieldLabel: 'Cod Cliente'
						},
						{
							xtype: 'textfield',
							name: 'cod_venda',
							margin: '0 5 0 0',
							flex: 1,
							hidden: true,
							id: 'cod_venda_vendas_pagamentos',
							anchor: '100%',
							fieldLabel: 'Cod Venda'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_vendas_pagamentos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_vendas_pagamentos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_vendas_pagamentos').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_vendas_pagamentos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});