/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixa_itens.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filtercaixa_itenswin',
	id: 'FilterCaixa_ItensWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	title: 'Filtro de Caixa_Itens',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterCaixa_Itens',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'caixa_nro',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'caixa_nro_filter_caixa_itens',
									anchor: '100%',
									fieldLabel: 'Caixa Nro'
								},

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Hora',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_hora_date_filter_caixa_itens',
											name: 'data_hora_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_hora_time_filter_caixa_itens',
											name: 'data_hora_time',
											hideLabel: true
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'tipo_lancamento',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'tipo_lancamento_filter_caixa_itens',
									anchor: '100%',
									fieldLabel: 'Tipo Lancamento'
								},

								{
									xtype: 'numberfield',
									name: 'id_tipo',																		
									flex: 1,
									id: 'id_tipo_filter_caixa_itens',
									anchor: '100%',
									fieldLabel: 'Id Tipo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'forma_pagto',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'forma_pagto_filter_caixa_itens',
									anchor: '100%',
									fieldLabel: 'Forma Pagto'
								},

								{
									xtype: 'textfield',
									name: 'valor',																		
									flex: 1,
									id: 'valor_filter_caixa_itens',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Valor'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'textfield',
									name: 'cancelado',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'cancelado_filter_caixa_itens',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Cancelado'
								},

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Cancelamento',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cancelamento_date_filter_caixa_itens',
											name: 'data_cancelamento_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cancelamento_time_filter_caixa_itens',
											name: 'data_cancelamento_time',
											hideLabel: true
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'cancelado_por',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'cancelado_por_filter_caixa_itens',
									anchor: '100%',
									fieldLabel: 'Cancelado Por'
								},

								{
									xtype: 'textfield',
									name: 'tipo_venda',																		
									flex: 1,
									id: 'tipo_venda_filter_caixa_itens',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Tipo Venda'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'numberfield',
									name: 'cod_cliente',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'cod_cliente_filter_caixa_itens',
									anchor: '100%',
									fieldLabel: 'Cod Cliente'
								},

								{
									xtype: 'textfield',
									name: 'observacoes',																		
									flex: 1,
									id: 'observacoes_filter_caixa_itens',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Observacoes'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_caixa_itens',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Resetar Filtro'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});
