/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixa_itens.Totais', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addcaixa_itenswin',
	requires: [
		'ShSolutions.store.StoreCaixa_Itens'
	],

	iconCls: 'caixa',
	id: 'Totais-Caixa_Itens',
	layout: {
		type: 'fit'
	},
	maximizable: true,
	minimizable: true,
	width: 900,
	height: 500,
	title: 'Conferência dos Valores no Caixa',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralCaixaItensConferencia',
					items: [
						{
							title: 'Resumo Financeiro',
							xtype: 'box',
							autoEl: {
								tag: 'iframe',
								src: 'server/modulos/caixa_itens/movimentacao.php?nro_caixa='+NroCaixaDesejado2+'&tipo=resumo'
							}
						},
						{
							xtype: 'gridcaixaitensretiradaslist',
							loadDisabled: true
						},
						{
							xtype: 'gridcaixaitenssuprimentoslist',
							loadDisabled: true
						},
						{
							xtype: 'gridcaixaitensvendaslist',
							loadDisabled: true
						},
						{
							xtype: 'gridcaixaitensadiantamentoslist',
							loadDisabled: true
						},
						{
							xtype: 'gridcaixaitensmeiospagtoslist',
							loadDisabled: true
						}/*,
						{
							title: 'Meios de Pagamentos',
							xtype: 'box',
							autoEl: {
								tag: 'iframe',
								src: 'server/modulos/caixa_itens/movimentacao.php?nro_caixa='+NroCaixaDesejado2+'&tipo=meios_pagtos'
							}
						},
							//so para admin
						{
							title: 'Vendas por Categorias',
							html: 'Falta Fazer.... Esperar o Módulo de Vendas'
							//id: 'caixa_itenslist',
							//store: 'StoreClientes_Logs_Sistema',
							//disabled: true,
							//loadDisabled: true
						}*/
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'button',
							id: 'button_imprimir_totais_caixa',
							iconCls: 'bt_imprimir2',
							action: 'imprimir_totais',
							text: 'Imprimir'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});