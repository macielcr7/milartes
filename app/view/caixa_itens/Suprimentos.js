/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixa_itens.Suprimentos', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridcaixaitenssuprimentoslist',
	requires: [
		'ShSolutions.store.StoreCaixaItensSuprimentos'
	],

	id: 'GridCaixaItensSuprimentos',
	store: 'StoreCaixaItensSuprimentos',
	title: 'Suprimentos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			height: 350,
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			features: [{
				ftype: 'summary'
			}],
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'id',
					text: 'ID',
					width: 60
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_hora',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data',
					width: 130,
					//sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'forma_pagto_descricao',
					text: 'Forma de Pagamento',
					width: 180,
					//sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor',
					align: 'right',
					style: 'text-align:right',
					renderer: valores_negativo_grid_caixa,
					text: 'Valor',
					width: 90,
					//sortable: false,
					fixed: true,
					summaryType: 'sum',
					summaryRenderer: Ext.util.Format.maskRenderer('#9.999.990,00', true)
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacoes',
					text: 'Observações',
					width: 400,
					//sortable: true,
					fixed: false
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreCaixaItensSuprimentos',
					dock: 'bottom'
				}
			]
		});

		me.callParent(arguments);
	}

});