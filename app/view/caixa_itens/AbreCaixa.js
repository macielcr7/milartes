/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixa_itens.AbreCaixa', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addcaixa_itenswin',

	id: 'AddCaixa_ItensWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Abertura do Caixa',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCaixa_Itens',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/caixas/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'saldo_inicial',
							id: 'saldo_inicial_caixa',
							allowBlank: true,
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							fieldLabel: 'Saldo Inicial',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 800);
								}
							}
						},
						{
							xtype: 'textarea',
							name: 'obs_saldo_inicial',
							id: 'obs_saldo_inicial_caixa',
							allowBlank: true,
							anchor: '100%',
							fieldStyle: 'text-transform:uppercase',
							fieldLabel: 'Observações'
						},
						{
							xtype: 'textfield',
							name: 'cx_nro',
							hidden: true,
							id: 'cx_nro_caixa',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_caixa',
							value: 'ABRIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_caixa',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_caixa',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});