/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
var data_desejada2 = new Date();
data_desejada2.setDate(data_desejada2.getDate());

Ext.define('ShSolutions.view.caixa_itens.AdicionaValores', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addcaixa_itenswin',

	id: 'AddDinCaixaWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 400,
	title: 'Adicionar Valores ao Caixa',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCaixa_Itens',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/caixa_itens/save.php',
					items: [
						{
							xtype: 'textfield',
							name: 'valor',
							id: 'valor_valores_caixa_itens',
							allowBlank: false,
							hidden: false,
							anchor: '100%',
							mask: '#9.999.990,00',
							plugins: 'textmask',
							money: true,
							fieldLabel: 'Valor',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 800);
								}
							}
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboFormas_Pagto',
									name: 'forma_pagto',
									id: 'forma_pagto_valores_caixa_itens',
									allowBlank: false,
									flex: 1,
									value: 1,
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Forma de Pagamento'
								}
							]
						},
						{
							xtype: 'numberfield',
							name: 'nro_parcelas',
							anchor: '100%',
							loadDisabled: true,
							disabled: true,
							hidden: true,
							allowBlank: false,
							minValue: 1,
							id: 'nro_parcelas_valores_caixa_itens',
							fieldLabel: 'Nro de Parcelas'
						},
						{
							xtype: 'datefield',
							format: 'd/m/Y',
							value: data_desejada2,
							hidden: true,
							allowBlank: false,
							name: 'data_venda_cartao',
							id: 'data_venda_cartao_valores_caixa_itens',
							fieldLabel: 'Data Autorização Cartão'
						},
						{
							xtype: 'textarea',
							name: 'observacoes',
							id: 'observacoes_valores_caixa_itens',
							anchor: '100%',
							allowBlank: true,
							fieldStyle: 'text-transform:uppercase',
							fieldLabel: 'Comentário/Observações'
						},
						{
							xtype: 'textfield',
							name: 'caixa_nro',
							hidden: true,
							id: 'cx_nro_valores_caixa_itens',
							fieldLabel: 'Caixa Nro (cuidado ao alterar)',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_valores_caixa_itens',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_valores_caixa_itens',
							value: 'ADD_VALORES',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_caixa_itens',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_caixa_itens',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});