/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixa_itens.MeiosPagamentos', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridcaixaitensmeiospagtoslist',
	requires: [
		'ShSolutions.store.StoreCaixaItensMeiosPagtos'
	],

	id: 'GridCaixaItensMeiosPagtos',
	store: 'StoreCaixaItensMeiosPagtos',
	title: 'Meios de Pagamentos (Vendas+Adiantamentos)',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			height: 350,
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			features: [{
				ftype: 'summary'
			}],
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'forma_pagto_descricao',
					text: 'Forma de Pagamento',
					width: 300,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_soma',
					align: 'right',
					style: 'text-align:right',
					renderer: valores_negativo_grid_caixa,
					text: 'Valor',
					width: 150,
					sortable: false,
					fixed: true,
					summaryType: 'sum',
					summaryRenderer: Ext.util.Format.maskRenderer('#9.999.990,00', true)
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreCaixaItensMeiosPagtos',
					dock: 'bottom'
				}
			]
		});

		me.callParent(arguments);
	}

});