/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.caixa_itens.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.caixa_itenslist',
	requires: [
		'ShSolutions.store.StoreCaixa_Itens'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'caixa_itens',
	id: 'List-Caixa_Itens',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 550,
	title: 'Ítens do Caixa',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridCaixa_Itens',
					store: 'StoreCaixa_Itens',
					viewConfig: {
						autoScroll: true,
						loadMask: false,
						getRowClass: function(record) { 
							if(record.get('cancelado')=='S'){
								return 'vermelho';
							}
							else{
								return '';
							}
						}
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: false,
							format: '0',
							text: 'Id',
							width: 60,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'caixa_nro',
							format: '0',
							text: 'Caixa Nro',
							hidden: true,
							width: 70,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_hora',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data',
							width: 130,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'tipo_lancamento',
							format: '0',
							text: 'Tipo do Lancamento',
							width: 130,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								if(v=='0'){
									return 'SUPRIMENTO';
								}
								else if(v=='1'){
									return 'RETIRADA';
								}
								else if(v=='2'){
									return 'VENDA';
								}
								else if(v=='3'){
									return 'ADIANTAMENTO';
								}
								else if(v=='4'){
									return 'DEVOLUÇÃO';
								}
								else{
									return v;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'forma_pagto_descricao',
							text: 'Forma de Pagamento',
							width: 180,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'valor',
							align: 'right',
							style: 'text-align:right',
							renderer: valores_negativo_grid_caixa,
							text: 'Valor',
							width: 90,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'tipo_venda',
							text: 'Tipo de Venda',
							width: 90,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								if(v=='V'){
									return 'À VISTA';
								}
								else if(v=='P'){
									return 'À PRAZO';
								}
								else{
									return v;
								}
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_cliente',
							text: 'Cliente',
							width: 250,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'observacoes',
							text: 'Observações',
							width: 200,
							sortable: true,
							fixed: false
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrado Por',
							width: 250,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cancelado',
							text: 'Cancelado',
							width: 80,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								if(v=='S'){
									return 'SIM';
								}
								else if(v=='N'){
									return 'NÃO';
								}
								else{
									return v;
								}
							}
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cancelamento',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Cancelamento',
							width: 130,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cancelado_por_nome',
							text: 'Cancelado Por',
							width: 230,
							sortable: false,
							fixed: true
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreCaixa_Itens',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_abrir_caixa',
									iconCls: 'bt_abrir_caixa',
									hidden: true,
									disabled: true,
									action: 'abrir_caixa',
									text: 'Abrir Caixa'
								},
								{
									xtype: 'button',
									id: 'button_fechar_caixa',
									iconCls: 'bt_fechar_caixa',
									hidden: true,
									disabled: true,
									action: 'fechar_caixa',
									text: 'Fechar Caixa'
								},
								{
									xtype: 'button',
									id: 'button_suprimento_caixa',
									iconCls: 'bt_add_din_caixa',
									hidden: true,
									disabled: true,
									action: 'add_din_caixa',
									text: 'Adicionar Dinheiro'
								},
								{
									xtype: 'button',
									id: 'button_retirada_caixa',
									iconCls: 'br_retirar_din_caixa',
									hidden: true,
									disabled: true,
									action: 'retirar_din_caixa',
									text: 'Retirar Dinheiro'
								},

								{
									xtype: 'button',
									id: 'button_edit_caixa_itens',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar Transação'
								},
								{
									xtype: 'button',
									id: 'button_del_caixa_itens',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Cancelar Transação'
								},
								{
									xtype: 'button',
									id: 'button_total_caixa_itens',
									iconCls: 'ver_totais',
									hidden: true,
									disabled: true,
									action: 'ver_totais',
									text: 'Ver Total'
								}/*,
								{
									xtype: 'button',
									id: 'button_filter_caixa_itens',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}*/
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});