/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_localidades.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.correios_localidadeslist',
	requires: [
		'ShSolutions.store.StoreCorreios_Localidades'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'correios_localidades',

	id: 'List-Correios_Localidades',
	layout: {
		type: 'fit'
	},
	height: 450,
	width: 900,
	title: 'Cidades - Correios',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridCorreios_Localidades',
					store: 'StoreCorreios_Localidades',
					viewConfig: {
						autoScroll: true,
						loadMask: false,
						getRowClass: function(record) { 
							if(record.get('ativo')=='N'){
								return 'amarelo';
							}
							else{
								return '';
							}
						}
					},

					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'Id',	
							width: 60
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'uf_sigla',
							hidden: true,
							text: 'Estado',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'loc_nome',
							text: 'Nome',
							width: 350
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cep',
							renderer : Ext.util.Format.maskRenderer('99.999-999'),
							text: 'Cep',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cod_ibge',
							text: 'Cod IBGE',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							width: 60,		
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
							
								}
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por',
							text: 'Cadastrado Por',
							width: 140
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cadastro',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Cadastro',
							width: 160
						}                
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreCorreios_Localidades',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_correios_localidades',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_correios_localidades',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_correios_localidades',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'button',
									id: 'button_add_bairros_correios_localidades',
									iconCls: 'bt_detalhar',
									hidden: true,
									disabled: true,
									action: 'list_bairros',
									text: 'Exibir Bairros'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_correios_localidades',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 200);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_correios_localidades').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_correios_localidades',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});