/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_localidades.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filtercorreios_localidadeswin',

    id: 'FilterCorreios_LocalidadesWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro de Correios_Localidades',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterCorreios_Localidades',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    
								    margin: '0 5 0 0',								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Estados',
		                                    name: 'uf_sigla',
											id: 'uf_sigla_filter_correios_localidades',
											button_id: 'button_uf_sigla_filter_correios_localidades',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Uf Sigla'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_uf_sigla_filter_correios_localidades',
		                                    combo_id: 'uf_sigla_filter_correios_localidades',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        },		                        
								{
									xtype: 'textfield',
									name: 'loc_nome_abreviado',								    								    
								    flex: 1,
									id: 'loc_nome_abreviado_filter_correios_localidades',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Nome Abreviado'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'loc_nome',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'loc_nome_filter_correios_localidades',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Nome'
								},								
								{
									xtype: 'textfield',
									name: 'cep',								    								    
								    flex: 1,
									id: 'cep_filter_correios_localidades',									
									mask: '99.999-999',
									plugins: 'textmask',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Cep'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'situacao_localidade',								    
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'situacao_localidade_filter_correios_localidades',
									anchor: '100%',
									fieldLabel: 'Situação'
								},								
								{
									xtype: 'textfield',
									name: 'loc_tipo',								    								    
								    flex: 1,
									id: 'loc_tipo_filter_correios_localidades',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Tipo'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'subordinacao_id',								    
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'subordinacao_id_filter_correios_localidades',
									anchor: '100%',
									fieldLabel: 'Subordinação Id'
								},								
								{
									xtype: 'textfield',
									name: 'cod_ibge',								    								    
								    flex: 1,
									id: 'cod_ibge_filter_correios_localidades',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Cod Ibge'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'ativo',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'ativo_filter_correios_localidades',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Ativo'
								},								
								{
									xtype: 'numberfield',
									name: 'cadastrado_por',								    								    
								    flex: 1,
									id: 'cadastrado_por_filter_correios_localidades',
									anchor: '100%',
									fieldLabel: 'Cadastrado Por'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},									
									margin: '0 5 0 0',									
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Cadastro',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cadastro_date_filter_correios_localidades',
											name: 'data_cadastro_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro_time_filter_correios_localidades',
											name: 'data_cadastro_time',
											hideLabel: true
										}
									]
								},								
								{
									xtype: 'numberfield',
									name: 'alterado_por',								    								    
								    flex: 1,
									id: 'alterado_por_filter_correios_localidades',
									anchor: '100%',
									fieldLabel: 'Alterado Por'
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_correios_localidades',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
