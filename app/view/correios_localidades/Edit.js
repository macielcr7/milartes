/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_localidades.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addcorreios_localidadeswin',

	id: 'AddCorreios_LocalidadesWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Localidades',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCorreios_Localidades',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/correios_localidades/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Estados',
											name: 'uf_sigla',
											id: 'uf_sigla_correios_localidades',
											button_id: 'button_uf_sigla_correios_localidades',
											flex: 1,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Estado'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_uf_sigla_correios_localidades',
											combo_id: 'uf_sigla_correios_localidades',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Estados',
											action: 'add_win'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'loc_nome_abreviado',
									flex: 1,
									id: 'loc_nome_abreviado_correios_localidades',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Nome Abreviado'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'loc_nome',
									margin: '0 5 0 0',
									flex: 1,
									id: 'loc_nome_correios_localidades',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Nome'
								},
								{
									xtype: 'textfield',
									name: 'cep', 
									allowBlank: true,
									flex: 1,
									id: 'cep_correios_localidades',
									anchor: '100%',
									mask: '99.999-999',
									plugins: 'textmask',
									fieldLabel: 'CEP'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboSituacaoLocalidadeCorreiosLocalidades',
											name: 'situacao_localidade',
											id: 'situacao_localidade_correios_localidades',
											button_id: 'button_situacao_localidade_correios_localidades',
											flex: 1,
											value: '1',
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Situação'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_situacao_localidade_correios_localidades',
											combo_id: 'situacao_localidade_correios_localidades',
											action: 'reset_combo'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboLocTipoCorreiosLocalidades',
											name: 'loc_tipo',
											loadDisabled: true,
											id: 'loc_tipo_correios_localidades',
											button_id: 'button_loc_tipo_correios_localidades',
											flex: 1,
											value: 'M',
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Tipo'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_loc_tipo_correios_localidades',
											combo_id: 'loc_tipo_correios_localidades',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'subordinacao_id',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									hidden: true,
									id: 'subordinacao_id_correios_localidades',
									anchor: '100%',
									fieldLabel: 'Subordinação Id'
								},
								{
									xtype: 'textfield',
									name: 'cod_ibge',
									margin: '0 5 0 0',
									flex: 1,
									id: 'cod_ibge_correios_localidades',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Código IBGE'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,                                   
									flex: 1,
									anchor: '100%', 
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											value: 'S',
											loadDisabled: true,
											id: 'ativo_Correios_Localidades',
											button_id: 'button_ativo_Correios_Localidades',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Ativo',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_Correios_Localidades',
											combo_id: 'ativo_Correios_Localidades',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_correios_localidades',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_correios_localidades',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_correios_localidades',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_correios_localidades',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});