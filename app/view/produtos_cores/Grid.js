/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.produtos_cores.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridprodutos_coreslist',
	requires: [
		'ShSolutions.store.StoreProdutos_Cores'
	],
	id: 'GridProdutos_Cores',
	store: 'StoreProdutos_Cores',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,
	columns: [
		{xtype: 'rownumberer'},
		{
			xtype: 'gridcolumn',
			dataIndex: 'descricao',
			text: 'Cor',
			width: 140
		}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: 'StoreProdutos_Cores',
			dock: 'bottom'
		},
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_produtos_cores',
					iconCls: 'bt_add',
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_produtos_cores',
					iconCls: 'bt_edit',
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_produtos_cores',
					iconCls: 'bt_del',
					action: 'deletar',
					text: 'Deletar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});