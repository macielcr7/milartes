/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produtos_cores.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produtos_coreslist',
	requires: [
		'ShSolutions.store.StoreProdutos_Cores'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'produtos_cores',

	id: 'List-Produtos_Cores',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Listagem de Produtos/Cores',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridprodutos_coreslist'
				}
			]
		});

		me.callParent(arguments);
	}
});