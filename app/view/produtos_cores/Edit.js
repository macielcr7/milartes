/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produtos_cores.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addprodutos_coreswin',

	id: 'AddProdutos_CoresWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Produtos_Cores',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProdutos_Cores',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produtos_cores/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCores',
									name: 'id_cor',
									id: 'id_cor_produtos_cores',
									button_id: 'button_id_cor_produtos_cores',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Cor',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 700);
										}
									}
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_id_cor_produtos_cores',
									combo_id: 'id_cor_produtos_cores',
									action: 'reset_combo'
								},
								{
									xtype: 'buttonadd',
									tabela: 'Cores',
									action: 'add_win'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id_produto',
							id: 'id_produto_produtos_cores',
							hidden: true,
							fieldLabel: 'ID Produto',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_produtos_cores',
							hidden: true,
							fieldLabel: 'ID',
							id: 'id_produtos_cores_produtos_cores',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_produtos_cores',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produtos_cores',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produtos_cores',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
