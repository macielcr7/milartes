/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.instaladores.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.instaladoreslist',
	requires: [
		'ShSolutions.store.StoreInstaladores'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'instaladores',

	id: 'List-Instaladores',
	layout: {
		type: 'fit'
	},
	width: 1000,
	height: 550,
	title: 'Instaladores',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridInstaladores',
					store: 'StoreInstaladores',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cod_instalador',
							hidden: true,
							format: '0',
							text: 'Código',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_completo',
							text: 'Nome Instalador',
							width: 150
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'apelido',
							text: 'Apelido',
							width: 90
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'produtos_que_instala',
							text: 'Produtos que Instala',
							width: 180,
							renderer: function (value){
								return '<div style="white-space:normal !important;">'+ value +'</div>';
							}
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'endereco',
							text: 'Endereço',
							width: 180
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'fones',
							text: 'Telefones',
							width: 190,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
								}
							},
							width: 50
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrador Por',
							width: 120
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreInstaladores',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_instaladores',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_instaladores',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_instaladores',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_instaladores',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 200);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
											Ext.getCmp('button_filter_instaladores').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_instaladores',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativo',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});