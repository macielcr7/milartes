/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.instaladores.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addinstaladoreswin',

	id: 'AddInstaladoresWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	title: 'Cadastro de Instaladores',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormInstaladores',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/instaladores/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'nome_completo',
									margin: '0 5 0 0',
									flex: 1,
									id: 'nome_completo_instaladores',
									anchor: '100%',
									fieldLabel: 'Nome Completo',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'apelido',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'apelido_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Apelido'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'endereco',
							allowBlank: true,
							margin: '0 5 0 0',
							id: 'endereco_instaladores',
							fieldStyle: 'text-transform:uppercase',
							anchor: '100%',
							fieldLabel: 'Endereço'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone1',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone1_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									mask: '(99) 9999-9999',
									plugins: 'textmask',
									fieldLabel: 'Fone 1'
								},
								{
									xtype: 'textfield',
									name: 'obs_fone1',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'obs_fone1_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Observação'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone2',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone2_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									mask: '(99) 9999-9999',
									plugins: 'textmask',
									fieldLabel: 'Fone 2'
								},
								{
									xtype: 'textfield',
									name: 'obs_fone2',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'obs_fone2_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Observação'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone3',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone3_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									mask: '(99) 9999-9999',
									plugins: 'textmask',
									fieldLabel: 'Fone 3'
								},
								{
									xtype: 'textfield',
									name: 'obs_fone3',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'obs_fone3_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Observação'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone4',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone4_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									mask: '(99) 9999-9999',
									plugins: 'textmask',
									fieldLabel: 'Fone 4'
								},
								{
									xtype: 'textfield',
									name: 'obs_fone4',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'obs_fone4_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Observação'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'fone5',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'fone5_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									mask: '(99) 9999-9999',
									plugins: 'textmask',
									fieldLabel: 'Fone 5'
								},
								{
									xtype: 'textfield',
									name: 'obs_fone5',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									id: 'obs_fone5_instaladores',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Observação'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textareafield',
									name: 'produtos_que_instala',
									flex: 1,
									allowBlank: true,
									id: 'produtos_que_instala_os',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Produtos que Instala'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											id: 'ativo_instaladores',
											button_id: 'button_ativo_instaladores',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											value: 'S',
											anchor: '100%',
											fieldLabel: 'Ativo'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_instaladores',
											combo_id: 'ativo_instaladores',
											action: 'reset_combo'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'cod_instalador',
									hidden: true,
									margin: '0 5 0 0',
									id: 'cod_instalador_instaladores',
									value: 0,
									anchor: '100%',
									fieldLabel: 'cod instalador'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_instaladores',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_instaladores',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_instaladores',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});