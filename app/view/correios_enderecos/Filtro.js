/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_enderecos.Filtro', {
    extend: 'ShSolutions.view.WindowMedium',
    alias: 'widget.filtercorreios_enderecoswin',

    id: 'FilterCorreios_EnderecosWin',
    layout: {
        type: 'fit'
    },
    modal: true,
    minimizable: false,
	
    title: 'Filtro de Correios_Enderecos',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterCorreios_Enderecos',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    
								    margin: '0 5 0 0',								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Estados',
		                                    name: 'uf_sigla',
											id: 'uf_sigla_filter_correios_enderecos',
											button_id: 'button_uf_sigla_filter_correios_enderecos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Uf Sigla'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_uf_sigla_filter_correios_enderecos',
		                                    combo_id: 'uf_sigla_filter_correios_enderecos',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        },		                        
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Localidades',
		                                    name: 'localidade_id',
											id: 'localidade_id_filter_correios_enderecos',
											button_id: 'button_localidade_id_filter_correios_enderecos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Localidade Id'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_localidade_id_filter_correios_enderecos',
		                                    combo_id: 'localidade_id_filter_correios_enderecos',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        }		                        

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'tipo',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'tipo_filter_correios_enderecos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Tipo'
								},								
								{
									xtype: 'textfield',
									name: 'nome',								    								    
								    flex: 1,
									id: 'nome_filter_correios_enderecos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Nome'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    
								    margin: '0 5 0 0',								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Bairros',
		                                    name: 'bairro_id_inicial',
											id: 'bairro_id_inicial_filter_correios_enderecos',
											button_id: 'button_bairro_id_inicial_filter_correios_enderecos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Bairro Id Inicial'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_bairro_id_inicial_filter_correios_enderecos',
		                                    combo_id: 'bairro_id_inicial_filter_correios_enderecos',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        },		                        
								{
		                            xtype: 'fieldcontainer',
		                            autoHeight: true,								    								    
								    flex: 1,
		                            layout: {
		                                align: 'stretch',
		                                type: 'hbox'
		                            },
		                            items: [
										{
											xtype: 'combobox',
		                                    store: 'StoreComboCorreios_Bairros',
		                                    name: 'bairro_id_final',
											id: 'bairro_id_final_filter_correios_enderecos',
											button_id: 'button_bairro_id_final_filter_correios_enderecos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Bairro Id Final'
										},
		                                {
		                                    xtype: 'buttonadd',
		                                    iconCls: 'bt_cancel',
		                                    hidden: true,
		                                    id: 'button_bairro_id_final_filter_correios_enderecos',
		                                    combo_id: 'bairro_id_final_filter_correios_enderecos',
		                                    action: 'reset_combo'
		                                },
		                            ]
		                        }		                        

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cep',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'cep_filter_correios_enderecos',									
									mask: '99.999-999',
									plugins: 'textmask',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Cep'
								},								
								{
									xtype: 'textfield',
									name: 'complemento',								    								    
								    flex: 1,
									id: 'complemento_filter_correios_enderecos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Complemento'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'loteamento',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'loteamento_filter_correios_enderecos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Loteamento'
								},								
								{
									xtype: 'textfield',
									name: 'ponto_referencia',								    								    
								    flex: 1,
									id: 'ponto_referencia_filter_correios_enderecos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Ponto Referencia'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'ativo',									
								    margin: '0 5 0 0',								    
								    flex: 1,
									id: 'ativo_filter_correios_enderecos',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Ativo'
								},								
								{
									xtype: 'numberfield',
									name: 'cadastrado_por',								    								    
								    flex: 1,
									id: 'cadastrado_por_filter_correios_enderecos',
									anchor: '100%',
									fieldLabel: 'Cadastrado Por'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},									
									margin: '0 5 0 0',									
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Cadastro',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cadastro_date_filter_correios_enderecos',
											name: 'data_cadastro_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro_time_filter_correios_enderecos',
											name: 'data_cadastro_time',
											hideLabel: true
										}
									]
								},								
								{
									xtype: 'numberfield',
									name: 'alterado_por',								    								    
								    flex: 1,
									id: 'alterado_por_filter_correios_enderecos',
									anchor: '100%',
									fieldLabel: 'Alterado Por'
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_correios_enderecos',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
