/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_enderecos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.correios_enderecoslist',
	requires: [
		'ShSolutions.store.StoreCorreios_Enderecos'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'correios_enderecos',

	id: 'List-Correios_Enderecos',
	layout: {
		type: 'fit'
	},
	height: 450,
	width: 900,
	title: 'Endere&ccedil;os - Correios',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridCorreios_Enderecos',
					store: 'StoreCorreios_Enderecos',
					viewConfig: {
						autoScroll: true,
						loadMask: false,
						getRowClass: function(record) { 
							if(record.get('ativo')=='N'){
								return 'amarelo';
							}
							else{
								return '';
							}
						}
					},
								
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: true,
							format: '0',
							text: 'Id',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'uf_sigla',
							hidden: true,
							text: 'Uf Sigla',
							width: 140
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'localidade_id',
							hidden: true,
							format: '0',
							text: 'Localidade Id',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'tipo',
							text: 'Tipo',
							width: 70
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome',
							text: 'Nome',
							width: 240
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'bairro_id_inicial',
							hidden: true,
							format: '0',
							text: 'Bairro Id Inicial',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'loteamento',
							text: 'Loteamento',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ponto_referencia',
							text: 'Ponto de Referência',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cep',
							renderer : Ext.util.Format.maskRenderer('99.999-999'),
							text: 'Cep',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'complemento',
							text: 'Complemento',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							width: 50,
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'SIM';
									break;
									case 'N':
									return 'NÃO';
									break;
							
								}
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por',
							text: 'Cadastrado Por',
							width: 140
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cadastro',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Cadastro',
							width: 120
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreCorreios_Enderecos',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_correios_enderecos',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_correios_enderecos',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_correios_enderecos',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_correios_enderecos',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 200);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
												Ext.getCmp('button_filter_correios_enderecos').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_correios_enderecos',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								}						
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});