/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.correios_enderecos.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addcorreios_enderecoswin',

	id: 'AddCorreios_EnderecosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Endereços',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCorreios_Enderecos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/correios_enderecos/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Estados',
											name: 'uf_sigla',
											id: 'uf_sigla_correios_enderecos',
											button_id: 'button_uf_sigla_correios_enderecos',
											flex: 1,
											anchor: '100%',	
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Uf Sigla'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_uf_sigla_correios_enderecos',
											combo_id: 'uf_sigla_correios_enderecos',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Estados',
											action: 'add_win'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Localidades',
											name: 'localidade_id',
											id: 'localidade_id_correios_enderecos',
											button_id: 'button_localidade_id_correios_enderecos',
											flex: 1,
											loadDisabled: true,
											disabled: true,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Localidade'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_localidade_id_correios_enderecos',
											combo_id: 'localidade_id_correios_enderecos',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Localidades',
											action: 'add_win'
										}
									]
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboCorreios_Bairros',
											name: 'bairro_id_inicial',
											id: 'bairro_id_inicial_correios_enderecos',
											button_id: 'button_bairro_id_inicial_correios_enderecos',
											flex: 1,
											loadDisabled: true,
											disabled: true,
											anchor: '100%',
											fieldStyle: 'text-transform:uppercase',
											fieldLabel: 'Bairro'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_bairro_id_inicial_correios_enderecos',
											combo_id: 'bairro_id_inicial_correios_enderecos',
											action: 'reset_combo'
										},
										{
											xtype: 'buttonadd',
											tabela: 'Correios_Bairros',
											action: 'add_win'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								type: 'hbox'
							},
							items: [
							{
								xtype: 'combobox',
								store: 'StoreComboTipoCorreiosEnderecos2',
								name: 'tipo',
								loadDisabled: true,
								allowBlank: true,
								id: 'tipo_correios_enderecos',
								button_id: 'button_tipo_correios_enderecos',
								flex: 1,
								value: 'RUA',
								anchor: '100%',	
								fieldStyle: 'text-transform:uppercase',
								fieldLabel: 'Tipo Endere&ccedil;o'
							},
							{
								xtype: 'buttonadd',
								iconCls: 'bt_cancel',
								hidden: true,
								id: 'button_tipo_correios_enderecos',
								combo_id: 'tipo_correios_enderecos',
								action: 'reset_combo'
							}]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'nome',
									flex: 1,
									id: 'nome_correios_enderecos',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Nome/Descri&ccedil;&atilde;o'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'cep',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									id: 'cep_correios_enderecos',
									anchor: '100%',
									mask: '99.999-999',
									plugins: 'textmask',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Cep'
								},
								{
									xtype: 'textfield',
									name: 'complemento',
									flex: 1,
									allowBlank: true,
									id: 'complemento_correios_enderecos',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Complemento'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'loteamento',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									id: 'loteamento_correios_enderecos',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Loteamento'
								},
								{
									xtype: 'textfield',
									name: 'ponto_referencia',
									flex: 1,
									allowBlank: true,
									id: 'ponto_referencia_correios_enderecos',
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Ponto Referencia'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									anchor: '100%',	
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											value: 'S',
											loadDisabled: true,
											id: 'ativo_Correios_Enderecos',
											button_id: 'button_ativo_Correios_Enderecos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Ativo',
											fieldStyle: 'text-transform:uppercase'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_Correios_Enderecos',
											combo_id: 'ativo_Correios_Enderecos',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_correios_enderecos',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_correios_enderecos',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_correios_enderecos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Fechar/Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_correios_enderecos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
