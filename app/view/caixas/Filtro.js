/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixas.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filtercaixaswin',
	id: 'FilterCaixasWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	title: 'Filtro de Caixas',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterCaixas',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},									
									margin: '0 5 0 0',									
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Abertura',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_abertura_date_filter_caixas',
											name: 'data_abertura_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_abertura_time_filter_caixas',
											name: 'data_abertura_time',
											hideLabel: true
										}
									]
								},

								{
									xtype: 'fieldcontainer',
									autoHeight: true,									
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboUsuarios',
											name: 'usuario_abertura',
											id: 'usuario_abertura_filter_caixas',
											button_id: 'button_usuario_abertura_filter_caixas',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Usuario Abertura'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_usuario_abertura_filter_caixas',
											combo_id: 'usuario_abertura_filter_caixas',
											action: 'reset_combo'
										},
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'textfield',
									name: 'saldo_inicial',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'saldo_inicial_filter_caixas',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Saldo Inicial'
								},

								{
									xtype: 'textfield',
									name: 'obs_saldo_inicial',																		
									flex: 1,
									id: 'obs_saldo_inicial_filter_caixas',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Obs Saldo Inicial'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},									
									margin: '0 5 0 0',									
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Fechamento',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_fechamento_date_filter_caixas',
											name: 'data_fechamento_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_fechamento_time_filter_caixas',
											name: 'data_fechamento_time',
											hideLabel: true
										}
									]
								},

								{
									xtype: 'numberfield',
									name: 'usuario_fechamento',																		
									flex: 1,
									id: 'usuario_fechamento_filter_caixas',
									anchor: '100%',
									fieldLabel: 'Usuario Fechamento'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'textfield',
									name: 'conferencia_saldo_final',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'conferencia_saldo_final_filter_caixas',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Conferencia Saldo Final'
								},

								{
									xtype: 'textfield',
									name: 'obs_saldo_final',																		
									flex: 1,
									id: 'obs_saldo_final_filter_caixas',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Obs Saldo Final'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'textfield',
									name: 'situacao',																		
									flex: 1,
									id: 'situacao_filter_caixas',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Situacao'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_caixas',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Resetar Filtro'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});
