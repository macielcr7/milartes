/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

if (screen.width >= 1210)
{
	var tamanho = 1210;
} else {
	var tamanho = 1000;
}

Ext.define('ShSolutions.view.caixas.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.caixaslist',
	requires: [
		'ShSolutions.store.StoreCaixas'
	],

	maximizable: true,
	minimizable: true,
	iconCls: 'caixas',
	id: 'List-Caixas',
	layout: {
		type: 'fit'
	},
	width: tamanho,
	height: 550,
	title: 'Caixas',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridpanel',
					id: 'GridCaixas',
					store: 'StoreCaixas',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},

					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'cx_nro',
							hidden: false,
							format: '0',
							text: 'Cx Nro',
							width: 70
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_abertura',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Abertura',
							width: 130,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_user_abertura',
							text: 'Usuario Abertura',
							width: 140,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'saldo_inicial',
							renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
							text: 'Saldo Inicial',
							width: 90,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'obs_saldo_inicial',
							text: 'Obs Saldo Inicial',
							width: 120,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_fechamento',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Fechamento',
							width: 130,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nome_user_fechamento',
							text: 'Usuario Fechamento',
							width: 140,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'conferencia_saldo_final',
							renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
							text: 'Conferência Saldo Final',
							width: 140,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'obs_saldo_final',
							text: 'Obs Saldo Final',
							width: 120,
							sortable: false,
							fixed: true
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'situacao',
							text: 'Situação',
							width: 100,
							sortable: false,
							fixed: true,
							renderer: function(v, r, m){
								if(v=='A'){
									return '<font color=\"orange\">ABERTO</font>';
								}
								else if(v=='F'){
									return 'FECHADO';
								}
								else{
									return v;
								}
							}
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreCaixas',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_caixas',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_caixas',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_caixas',
									iconCls: 'bt_del',
									hidden: true,
									action: 'deletar',
									text: 'Deletar'
								},
								{
									xtype: 'button',
									id: 'button_filter_caixas',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{
									xtype: 'button',
									id: 'button_pdf_caixas',
									iconCls: 'bt_pdf',
									action: 'gerar_pdf',
									text: 'Gerar PDF'
								},
								{
									xtype: 'button',
									id: 'button_total_caixas',
									iconCls: 'ver_totais',
									//hidden: true,
									action: 'ver_totais',
									text: 'Ver Total'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Status',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Caixa Aberto',
												checked: true,
												value: 'A',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Caixa Fechado',
												checked: false,
												value: 'F',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});