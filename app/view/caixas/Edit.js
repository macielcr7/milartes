/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.caixas.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addcaixaswin',

	id: 'AddCaixasWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 300,
	title: 'Cadastro de Caixas',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormCaixas',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/caixas/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'saldo_inicial',
									flex: 1,
									id: 'saldo_inicial_caixas',
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Saldo Inicial do Caixa'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'obs_saldo_inicial',
									flex: 1,
									id: 'obs_saldo_inicial_caixas',
									anchor: '100%',
									allowBlank: true,
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Obs Saldo Inicial'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'conferencia_saldo_final',
									flex: 1,
									id: 'conferencia_saldo_final_caixas',
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									allowBlank: true,
									fieldLabel: 'Conferencia Saldo Final'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'obs_saldo_final',
									flex: 1,
									id: 'obs_saldo_final_caixas',
									anchor: '100%',
									allowBlank: true,
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Obs Saldo Final'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboCaixasSituacao',
									name: 'situacao',
									flex: 1,
									id: 'situacao_caixas',
									anchor: '100%',
									fieldLabel: 'Situacao'
								}
							]
						},
						{
							xtype: 'hidden',
							name: 'cx_nro',
							hidden: true,
							id: 'cx_nro_caixas',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_caixas',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_caixas',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_caixas',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});