/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.agenda_telefonica_contato.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addagenda_telefonica_contatowin',

	id: 'AddAgenda_Telefonica_ContatoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Contato',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAgenda_Telefonica_Contato',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/agenda_telefonica_contato/save.php',
					items: [
						{
							xtype: 'hidden',
							name: 'cod_contato',
							id: 'cod_contato_agenda_telefonica_contato',
							anchor: '100%',
							fieldLabel: 'Cod Contato'
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboTipo_ContatoClientesContato',
									name: 'tipo_contato',
									loadDisabled: true,
									value: 'TELEFONE',
									id: 'tipo_contato_agenda_telefonica_contato',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Tipo Contato'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_tel_agenda_telefonica_contato',
							anchor: '100%',	  
							mask: '(99) 9999-9999',
							plugins: 'textmask',
							fieldLabel: 'Descrição'
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							id: 'descricao_tel_ddg_agenda_telefonica_contato',
							anchor: '100%',
							hidden: true,
							disabled: true,
							mask: '9999 999-9999',
							plugins: 'textmask',
							fieldLabel: 'Descrição '
						},
						{
							xtype: 'textfield',
							name: 'descricao',
							hidden: true,
							disabled: true,
							id: 'descricao_agenda_telefonica_contato',
							anchor: '100%',							
							fieldLabel: 'Descrição',
							validator: function(value){
								if(value!=""){
									if(Ext.getCmp('tipo_contato_agenda_telefonica_contato').getValue()=='EMAIL'){
										if(!isEmail(value)){
											return 'Email Inválido';
										}
									}
									else if(Ext.getCmp('tipo_contato_agenda_telefonica_contato').getValue()=='SITE'){
										if(!isValidURL(value)){
											return 'URL Inválida Formato Correto: http://www.exemplo.com.br';
										}
									}
									   
								}
								return true;
							}
						},
						{
							xtype: 'textarea',
							name: 'observacao',
							allowBlank: true,
							id: 'observacao_agenda_telefonica_contato',
							anchor: '100%',
							fieldLabel: 'Observa&ccedil;&atilde;o', 
							maxLength: 255,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'hidden',
							name: 'controle',
							hidden: true,
							id: 'controle_agenda_telefonica_contato',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_agenda_telefonica_contato',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_agenda_telefonica_contato',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_agenda_telefonica_contato',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});