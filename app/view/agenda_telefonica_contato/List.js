/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.agenda_telefonica_contato.List', {
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.agenda_telefonica_contatolist',
	requires: [
		'ShSolutions.store.StoreAgenda_Telefonica_Contato'
	],
	
	id: 'GridAgenda_Telefonica_Contato',
	store: 'StoreAgenda_Telefonica_Contato',
	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,
	iconCls: 'contatos',
	height: 350,
	title: 'Telefones, E-Mails, Sites...',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'controle',
					hidden: true,
					format: '0',
					text: 'Controle',
					width: 140
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'tipo_contato',
					text: 'Tipo Contato',
					width: 100
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'descricao',
					text: 'Descrição',
					width: 140,
					renderer : function(v, m, r){
						if(r.get('tipo_contato') == 'TELEFONE'){
							return v.mask('(__) ____-____');
						}
						else{
							return v;
						}
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacao',
					text: 'Observação',
					width: 220
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'data_cadastro',
					format: 'd/m/Y H:i:s',
					renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
					text: 'Data Cadastro',
					width: 100
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreAgenda_Telefonica_Contato',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_agenda_telefonica_contato',
							iconCls: 'bt_add',
							hidden: true,
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_agenda_telefonica_contato',
							iconCls: 'bt_edit',
							hidden: true,
							disabled: true,
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_agenda_telefonica_contato',
							iconCls: 'bt_del',
							hidden: true,
							disabled: true,
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});