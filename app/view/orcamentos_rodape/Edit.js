/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/
if (screen.width >= 1110)
{
	var tamanho = 1110;
} else {
	var tamanho = 1000;
}

var data_desejada = new Date();
data_desejada.setDate(data_desejada.getDate() + 10);

Ext.define('ShSolutions.view.orcamentos_rodape.Edit', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addorcamentos_rodapewin',

	id: 'AddOrcamentosRodapeWin',
	layout: {
		type: 'fit'
	},
	maximizable: true,
	minimizable: true,
	closable: false,
	height: 600,
	closable: false,
	width: tamanho,
	title: 'Orçamento - Rodapé',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormOrcamentosRodape',
					bodyPadding: 5,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/orcamentos_rodape/save.php',
					items: [
						{
							region: 'north',
							width: tamanho-20,
							height: 280,
							border: 0,
							items: [
								{
									xtype: 'form',
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													margin: '0 5 0 0',
													id: 'fieldcontainer_id_orcamentista_orcamentos_rodape',
													layout: {
														align: 'stretch',
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboOrcamentista',
															name: 'id_orcamentista',
															id: 'id_orcamentista_orcamentos_rodape',
															width: 170,
															anchor: '100%',
															fieldLabel: 'Orçamentista'
														}
													]
												},
												{
													xtype: 'numberfield',
													name: 'id_os',
													allowBlank: true,
													margin: '0 5 0 0',
													hidden: false,
													id: 'id_os_orcamentos_rodape',
													anchor: '100%',
													width: 60,
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'OS Nro',
													listeners: {
														afterrender: function(fld) {
															//fld.focus(true, 1500);
															var c = setInterval(function(){
																lastVal = fld.value;
																clearInterval(c);
															}, 2000);
														},
														focus: function() {
															lastVal = this.value;
														},
														blur: function() {
															if (lastVal != this.value){
																control.getController('OrcamentosRodape').loadDadosOS(Ext.getCmp('id_os_orcamentos_rodape'));
															}
														}
													}
												},
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													flex: 1,
													margin: '0 5 0 0',
													id: 'fieldcontainer_id_cliente_orcamentos_rodape',
													layout: {
														align: 'stretch',
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboOrcamentosClientes',
															name: 'id_cliente',
															allowBlank: false,
															hideTrigger: true,
															forceSelection: false,
															typeAhead: true,
															minChars: 4,
															queryMode: 'remote',
															loadDisabled: true,
															id: 'id_cliente_orcamentos_rodape',
															button_id: 'button_id_cliente_orcamentos_rodape',
															flex: 1,
															anchor: '100%',
															fieldLabel: 'Cliente',
															fieldStyle: 'text-transform:uppercase',
															listeners: {
																afterrender: function(fld) {
																	fld.focus(true, 800);
																}
															},
															listConfig: {
																loadingText: 'Procurando...',
																emptyText: 'Nenhum Resultado Encontrado...',
																getInnerTpl: function() {
																	 return '<table width="100%"><tr align="left" valign="top"><td><b>{descricao}</b></td></tr>' + 
																	 '<tr valign="top"><td><i>{endereco_completo}</i></td></tr><tr><td height="2">&nbsp;</td></tr></table>';
																}
															},
															pageSize: 30
														},
														{
															xtype: 'buttonadd',
															iconCls: 'bt_cancel',
															hidden: true,
															id: 'button_id_cliente_orcamentos_rodape',
															combo_id: 'id_cliente_orcamentos_rodape',
															action: 'reset_combo'
														}
													]
												},
												{
													xtype: 'textfield',
													name: 'cliente_descricao',
													flex: 1,
													margin: '0 5 0 0',
													hidden: true,
													disabled: true,
													id: 'cliente_descricao_orcamentos_rodape',
													anchor: '100%',
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Cliente'
												},
												{
													xtype:'checkbox',
													checked: true,
													width: 130,
													margin: '0 5 0 0',
													labelAlign: 'top',
													labelStyle: 'font-weight: bold;font-size: 11px;',
													anchor: '100%',
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Cliente Cadastrado?',
													boxLabel: 'Sim',
													uncheckedValue: '2',
													id: 'cliente_cadastrado_orcamentos_rodape',
													name: 'cliente_cadastrado',
													inputValue: '1'
												},
												{
													xtype: 'datefield',
													format: 'd/m/Y',
													margin: '0 5 0 0',
													value: data_desejada,
													name: 'validade_orcamento',
													id: 'validade_orcamento_orcamentos_rodape',
													anchor: '100%',
													width: 150,
													fieldLabel: 'Validade Até'
												},
												{
													xtype: 'combobox',
													store: 'StoreComboOrcamentosVendas',
													name: 'eh_venda',
													id: 'eh_venda_orcamentos_rodape',
													width: 100,
													anchor: '100%',
													readOnly: true,
													fieldLabel: 'Tipo'
												}
											]
										},
										{
											xtype: 'fieldset',
											title: 'Produtos',
											items: [
												{
													xtype: 'gridprodutos_orcamentos_rodapelist',
													loadDisabled: false,
													height: 150
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'valor_bruto',
													margin: '0 5 0 0',
													flex: 1,
													value: 0,
													id: 'valor_bruto_orcamentos_rodape',
													anchor: '100%',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													readOnly: true,
													fieldStyle: 'background-color: #FFFFCC; background-image: none;',
													fieldLabel: 'Sub-Total'
												},
												{
													xtype: 'textfield',
													name: 'acrescimo_real',
													margin: '0 5 0 0',
													flex: 1,
													value: 0,
													allowBlank: true,
													id: 'acrescimo_real_orcamentos_rodape',
													anchor: '100%',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													fieldLabel: 'Acrescimo (R$)'
												},
												{
													xtype: 'numberfield',
													name: 'acrescimo_porcento',
													margin: '0 5 0 0',
													flex: 1,
													value: 0,
													minValue: 0,
													enableKeyEvents: true,
													forcePrecision: true,
													decimalPrecision: 20,
													allowBlank: true,
													id: 'acrescimo_porcento_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Acrescimo (%)'
												},
												{
													xtype: 'textfield',
													name: 'desconto_real',
													flex: 1,
													value: 0,
													allowBlank: true,
													margin: '0 5 0 0',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													id: 'desconto_real_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Desconto (R$)'
												},
												{
													xtype: 'numberfield',
													name: 'desconto_porcento',
													margin: '0 5 0 0',
													flex: 1,
													minValue: 0,
													value: 0,
													allowBlank: true,
													enableKeyEvents: true,
													forcePrecision: true,
													decimalPrecision: 20,
													id: 'desconto_porcento_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Desconto (%)'
												},
												{
													xtype: 'textfield',
													name: 'valor_total',
													flex: 1,
													value: 0,
													id: 'valor_total_orcamentos_rodape',
													anchor: '100%',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													fieldStyle: 'background-color: #FFFF99; background-image: none;',
													fieldLabel: 'Valor Total'
												}
											]
										}
									]
								}
							]
						},
						{
							region: 'south',
							width: tamanho-20,
							height: 250,
							layout: 'accordion',
							items: [
								{
									xtype: 'form',
									title: '<b>Endereço para Entrega<b>',
									bodyPadding: 5,
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'aos_cuidados_de',
													flex: 1,
													margin: '0 5 0 0',
													id: 'aos_cuidados_de_orcamentos_rodape',
													anchor: '100%',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													fieldLabel: 'Aos Cuidados De'
												},
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													margin: '0 5 0 0',
													flex: 1,
													layout: {
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboCorreios_Estados',
															name: 'estado',
															allowBlank: false,
															id: 'estado_orcamentos_rodape',
															flex: 1,
															anchor: '100%',
															fieldLabel: 'Estado',
															fieldStyle: 'text-transform:uppercase'
														},
														{
															xtype: 'buttonadd',
															tabela: 'Correios_Estados',
															action: 'add_win'
														}
													]
												},
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													flex: 1,
													layout: {
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboCorreios_Localidades',
															name: 'cidade',
															allowBlank: false,
															loadDisabled: true,
															minChars:2,
															disabled: true,
															id: 'cidade_orcamentos_rodape',
															flex: 1,
															anchor: '100%',
															fieldLabel: 'Cidade',
															//hideTrigger:false,
															fieldStyle: 'text-transform:uppercase'
														},
														{
															xtype: 'buttonadd',
															tabela: 'Correios_Localidades',
															action: 'add_win'
														}
													]
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													margin: '0 5 0 0',
													flex: 1,
													layout: {
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboCorreios_Enderecos',
															name: 'endereco',
															loadDisabled: true,
															allowBlank: true,
															hideTrigger: false,
															minChars:2,
															queryMode: 'remote',
															disabled: true,
															id: 'endereco_orcamentos_rodape',
															button_id: 'button_endereco_orcamentos_rodape',
															flex: 1,
															anchor: '100%',
															fieldLabel: 'Endereço',
															fieldStyle: 'text-transform:uppercase'
														},
														{
															xtype: 'buttonadd',
															iconCls: 'bt_cancel',
															hidden: true,
															id: 'button_endereco_orcamentos_rodape',
															combo_id: 'endereco_orcamentos_rodape',
															handler: function() {
																Ext.getCmp('endereco_orcamentos_rodape').setValue('');
																Ext.getCmp('bairro_nome_orcamentos_rodape').setValue('');
																Ext.getCmp('bairro_orcamentos_rodape').setValue('');
																Ext.getCmp('cep_orcamentos_rodape').setValue('');
																Ext.getCmp('ponto_ref_orcamentos_rodape').setValue('');
																Ext.getCmp('loteamento_orcamentos_rodape').setValue('');
															}
														},
														{
															xtype: 'buttonadd',
															tabela: 'Correios_Enderecos',
															action: 'add_win'
														}
													]
												},	
												{
													xtype: 'textfield',
													name: 'bairro_nome',
													width: 200,
													margin: '0 5 0 0',	
													allowBlank: true,
													readOnly: true,
													id: 'bairro_nome_orcamentos_rodape',
													fieldLabel: 'Bairro',
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'bairro',
													flex: 1,
													hidden: true,
													allowBlank: true,
													id: 'bairro_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Bairro Id'
												},
												{
													xtype: 'textfield',
													name: 'nro_end',
													width: 80,
													allowBlank: true,
													id: 'nro_end_orcamentos_rodape',
													fieldLabel: 'Nro End'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'complemento',
													margin: '0 5 0 0',
													flex: 1,
													allowBlank: true,
													id: 'complemento_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Complemento',
													maxLength: 255,
													enforceMaxLength: true,
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'ponto_ref',	
													margin: '0 5 0 0',
													flex: 1,
													allowBlank: true,
													id: 'ponto_ref_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Ponto Ref', 
													maxLength: 255,
													enforceMaxLength: true,
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'loteamento',
													margin: '0 5 0 0',
													flex: 1,
													allowBlank: true,
													id: 'loteamento_orcamentos_rodape',
													anchor: '100%',
													fieldLabel: 'Loteamento',
													maxLength: 50,
													enforceMaxLength: true,
													fieldStyle: 'text-transform:uppercase'
												},
												{
													xtype: 'textfield',
													name: 'cep',
													width: 100,
													allowBlank: true,
													id: 'cep_orcamentos_rodape',
													mask: '99.999-999',
													plugins: 'textmask',
													fieldLabel: 'CEP'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													width: 100,
													allowBlank: true,
													margins: '0 5 0 0',
													//hideLabel: true,
													name: 'fone1',
													id: 'fone1_orcamentos_rodape',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 1',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 107,
													margins: '0 5 0 0',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone1_obs',
													id: 'fone1_obs_orcamentos_rodape',
													fieldLabel: 'Obs'
												},
												{
													xtype: 'textfield',
													width: 100,
													allowBlank: true,
													margins: '0 5 0 0',
													//hideLabel: true,
													name: 'fone2',
													id: 'fone2_orcamentos_rodape',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 2',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 107,
													margins: '0 5 0 0',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone2_obs',
													id: 'fone2_obs_orcamentos_rodape',
													fieldLabel: 'Obs'
												},
												{
													xtype: 'textfield',
													width: 100,
													allowBlank: true,
													margins: '0 5 0 0',
													//hideLabel: true,
													name: 'fone3',
													id: 'fone3_orcamentos_rodape',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 3',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 107,
													margins: '0 5 0 0',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone3_obs',
													id: 'fone3_obs_orcamentos_rodape',
													fieldLabel: 'Obs'
												},
												{
													xtype: 'textfield',
													width: 100,
													allowBlank: true,
													margins: '0 5 0 0',
													//hideLabel: true,
													name: 'fone4',
													id: 'fone4_orcamentos_rodape',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 4',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 106,
													margins: '0 5 0 0',
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone4_obs',
													id: 'fone4_obs_orcamentos_rodape',
													fieldLabel: 'Obs'
												},
												{
													xtype: 'textfield',
													width: 100,
													allowBlank: true,
													margins: '0 5 0 0',
													//hideLabel: true,
													name: 'fone5',
													id: 'fone5_orcamentos_rodape',
													mask: '(99) 99999-9999',
													plugins: 'textmask',
													fieldLabel: 'Fone 5',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														focus: function(field){
															field.setMask('(99) 99999-9999'); // Coloquei isto pois depois que eu coloco o telefone de 8 digitos se tento colocar o de 9, a mascara de 9 dígitos não volta
														},
														blur: function(field, eventObject){
															var value = field.getValue();

															if(value.length <= 10){
																field.setMask('(99) 9999-9999');
															}else{
																field.setMask('(99) 99999-9999');
															}
														}
													}
												},
												{
													xtype: 'textfield',
													width: 106,
													allowBlank: true,
													fieldStyle: 'text-transform:uppercase',
													name: 'fone5_obs',
													id: 'fone5_obs_orcamentos_rodape',
													fieldLabel: 'Obs'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											layout: {
												type: 'hbox'
											},
											items: [
												{
													xtype: 'radiogroup',
													fieldLabel: 'Buscar Endereço',
													hideLabel: true,
													columns: 2,
													//flex: 1,
													id: 'buscar_endereco_orcamentos_rodape',
													items: [
														{
															xtype: 'radiofield',
															boxLabel: 'Buscar Endereço da OS',
															name: 'buscar_endereco',
															inputValue: 'OS',
															width: 200
														},
														{
															xtype: 'radiofield',
															boxLabel: 'Buscar Endereço do Cadastro do Cliente',
															name: 'buscar_endereco',
															inputValue: 'CADASTRO_CLIENTE',
															width: 300
														}
													],
													listeners: {
														change: function(field, newValue, oldValue) {
															var value = newValue.buscar_endereco;
															if (Ext.isArray(value)) {
																return;
															}
															if (value == 'OS') {
																control.getController('OrcamentosRodape').loadEnderecoOS(Ext.getCmp('id_os_orcamentos_rodape').getValue());
															}
															else {
																control.getController('OrcamentosRodape').loadEnderecoCliente(Ext.getCmp('id_cliente_orcamentos_rodape').getValue());
															}
														}
													}
												}
											]
										}
									]
								},
								{
									xtype: 'panel',
									title: '<b>Observações</b>',
									bodyPadding: 5,
									items: [
										{
											xtype: 'gridorcamentos_observacoeslist',
											anchor: '98%',
											height: 170,
											loadDisabled: true
										}
									]
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id_orcamento',
							hidden: true,
							id: 'id_orcamento_orcamentos_rodape',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'total_de_itens',
							hidden: true,
							id: 'total_de_itens_orcamentos_rodape',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'data_venda',
							allowBlank: true,
							hidden: true,
							id: 'data_venda_orcamentos_rodape',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_orcamentos_rodape',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_orcamentos_rodape',
							iconCls: 'bt_cancel',
							action: 'resetar',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_orcamentos_rodape').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_orcamentos_rodape',
							iconCls: 'bt_save',
							action: 'salvar',
							disabled: true,
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.on('beforeclose', function(){
			if (Ext.getCmp('eh_venda_orcamentos_rodape').getValue() == "N") {
				control.getController('Orcamentos').checkDelete(Ext.getCmp('id_orcamento_orcamentos_rodape').getValue());
			}
			else {
				control.getController('Vendas').checkDelete(Ext.getCmp('id_orcamento_orcamentos_rodape').getValue());
			}
		});

		me.callParent(arguments);
	}
});