/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_rodape.EditAmbientes', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addambientes_orcamentos_rodapewin',

	id: 'AddAmbientes_Orcamentos_RodapeWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	
	title: 'Cadastro de Ambiente Orçamento - Rodape',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormAmbientes_Orcamentos_Rodape',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/ambientes_orcamentos_rodape/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreAmbientesNomes',
									allowBlank: false,
									flex: 1,
									triggerAction: 'all',
									typeAhead: false,
									mode: 'remote',
									minChars: 2,
									forceSelection: false,
									hideTrigger: false,
									queryMode: 'remote',
									anchor: '100%',
									name: 'ambiente',
									id: 'ambiente_ambientes_orcamentos_rodape',
									fieldLabel: 'Ambiente', 
									maxLength: 100,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(true, 1000);
										},
										change:function(field){
											if (field.getValue() != null) {
												field.setValue(field.getValue().toUpperCase());
											}
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_1',
									id: 'comprimento_1_ambientes_orcamentos_rodape',
									anchor: '100%',
									flex: 1,
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 1'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_2',
									id: 'comprimento_2_ambientes_orcamentos_rodape',
									anchor: '100%',
									flex: 1,
									allowBlank: true,
									mask: '#9.999.990,00',
									margin: '0 5 0 0',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 2'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_3',
									id: 'comprimento_3_ambientes_orcamentos_rodape',
									anchor: '100%',
									flex: 1,
									allowBlank: true,
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 3'
								}
							]
						},	
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_4',
									id: 'comprimento_4_ambientes_orcamentos_rodape',
									anchor: '100%',
									margin: '0 5 0 0',
									flex: 1,
									allowBlank: true,
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 4'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_5',
									id: 'comprimento_5_ambientes_orcamentos_rodape',
									anchor: '100%',
									flex: 1,
									allowBlank: true,
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 5'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_6',
									id: 'comprimento_6_ambientes_orcamentos_rodape',
									anchor: '100%',
									allowBlank: true,
									flex: 1,
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 6'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_7',
									id: 'comprimento_7_ambientes_orcamentos_rodape',
									anchor: '100%',
									flex: 1,
									allowBlank: true,
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 7'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_8',
									id: 'comprimento_8_ambientes_orcamentos_rodape',
									anchor: '100%',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 8'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_9',
									id: 'comprimento_9_ambientes_orcamentos_rodape',
									anchor: '100%',
									flex: 1,
									allowBlank: true,
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 9'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_10',
									id: 'comprimento_10_ambientes_orcamentos_rodape',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 10'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_11',
									id: 'comprimento_11_ambientes_orcamentos_rodape',
									allowBlank: true,
									margin: '0 5 0 0',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 11'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_12',
									id: 'comprimento_12_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 12'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_13',
									id: 'comprimento_13_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 13'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_14',
									id: 'comprimento_14_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 14'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_15',
									id: 'comprimento_15_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 15'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_16',
									id: 'comprimento_16_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 16'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_17',
									id: 'comprimento_17_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 17'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_18',
									id: 'comprimento_18_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 18'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'comprimento_real_19',
									id: 'comprimento_19_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 19'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_20',
									id: 'comprimento_20_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									margin: '0 5 0 0',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 20'
								},
								{
									xtype: 'textfield',
									name: 'comprimento_real_21',
									id: 'comprimento_21_ambientes_orcamentos_rodape',
									allowBlank: true,
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Comprimento 21'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '100%',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'observacoes',
									flex: 1,
									allowBlank: true,
									id: 'observacoes_ambientes_orcamentos_rodape',
									anchor: '100%',
									maxLength: 255,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Observações'
								}
							]
						},						
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_ambientes_orcamentos_rodape',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_ambientes_orcamentos_rodape',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_ambientes_orcamentos_rodape',
							iconCls: 'bt_cancel',
							action: 'resetar',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_ambientes_orcamentos_rodape',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});