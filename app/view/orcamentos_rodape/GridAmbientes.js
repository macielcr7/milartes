/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.orcamentos_rodape.GridAmbientes', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridambientes_orcamentos_rodapelist',
	requires: [
		'ShSolutions.store.StoreAmbientes_Orcamentos_Rodape'
	],
	id: 'GridAmbientes_Orcamentos_Rodape',
	store: 'StoreAmbientes_Orcamentos_Rodape',
	height: 500,

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			forceFit: true,			
			columns: [
				{xtype: 'rownumberer'},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ordem',
					text: 'Ordem',
					hidden: true,
					width: 60,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'qtd_paredes',
					text: 'Qtd Paredes',
					width: 100,					
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'ambiente',
					text: 'Ambiente/Descrição',					
					width: 140,
					sortable: false,
					fixed:true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'area',
					text: 'Total (m)',
					width: 80,
					renderer: function(v){
						return converteFloatMoeda(v);
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacoes',
					text: 'Observações',
					width: 180,
					sortable: false,
					fixed: true
				}
			],
            dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_ambientes_orcamentos_rodape',
							iconCls: 'bt_add',								
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_ambientes_orcamentos_rodape',
							iconCls: 'bt_edit',							
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_ambientes_orcamentos_rodape',
							iconCls: 'bt_del',							
							action: 'deletar',
							text: 'Deletar'
						}			
					]
				}
			]
		});

		me.callParent(arguments);
	}
});