/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.orcamentos_rodape.EditProdutos', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.addprodutos_orcamentos_rodapewin',

	id: 'AddProdutos_Orcamentos_RodapeWin',
	layout: {
		type: 'fit'
	},
	maximizable: true,
	minimizable: true,
	closable: false,
	width: 1000,
	
	title: 'Cadastro de Produto Orçamento - Rodape',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProdutos_Orcamentos_Rodape',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produtos_orcamentos_rodape/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									margins: '0 5 0 0',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											id: 'step_1_produtos_orcamentos_rodape',
											margins: '0 5 0 0',
											anchor: '100%', 
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboProdutosRodape',
													name: 'id_produto',
													id: 'id_produto_produtos_orcamentos_rodape',
													flex: 1,
													loadDisabled: true,
													queryMode: 'remote',
													triggerAction: 'all',
													typeAhead: false,
													minChars: 3,
													anchor: '100%',
													margins: '0 5 0 0',
													fieldLabel: 'Produto',
													fieldStyle: 'text-transform:uppercase',
													listeners: {
														afterrender: function(fld) {
															fld.focus(true, 1000);
														}
													}
												},
												{
													xtype: 'fieldcontainer',
													autoHeight: true,
													margins: '0 5 0 0',
													layout: {
														type: 'hbox'
													},
													items: [
														{
															xtype: 'combobox',
															store: 'StoreComboProdutosCores',
															name: 'id_cor',
															id: 'id_cor_produtos_orcamentos_rodape',
															loadDisabled: true, 
															width: 120,
															fieldLabel: 'Cor'
														}
													]
												},
												{
													xtype: 'textfield',
													name: 'quantidade',
													id: 'quantidade_produtos_orcamentos_rodape',
													disabled: true,
													hidden: true,
													margins: '0 5 0 0',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													selectOnFocus: true,
													fieldLabel: 'Quantidade'
												},
												{
													xtype: 'textfield',
													name: 'valor_unitario',
													id: 'valor_unitario_produtos_orcamentos_rodape',
													allowBlank: true,
													width: 90,
													margins: '0 5 0 0',
													mask: '#9.999.990,00',
													plugins: 'textmask',
													money: true,
													selectOnFocus: true,
													fieldLabel: 'Valor Unitário'
												}
											]
										},
										{
											xtype: 'buttonadd',
											id: 'proximo_step_2_produtos_orcamentos_rodape',
											text: 'Proximo',
											action: 'add_ambientes'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldset',
							id: 'step_2_produtos_orcamentos_rodape',
							disabled: true,
							title: 'Ambientes',
							items: [
								{
									xtype: 'gridambientes_orcamentos_rodapelist',
									loadDisabled: true,
									height: 200
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									margins: '0 5 0 0',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'numberfield',
											name: 'qtd_barras',
											margin: '0 5 0 0',
											flex: 1,
											value: 0,
											minValue: 0,
											id: 'qtd_barras_orcamentos_rodape',
											anchor: '100%',
											fieldLabel: 'Qtd. Barras'
										},
										{

											xtype: 'numberfield',
											name: 'qtd_metros_porcento',
											margin: '0 5 0 0',
											value: 0,
											minValue: 0,
											forcePrecision: true,
											decimalPrecision: 20,
											allowBlank: true,
											readOnly: true,
											flex: 1,
											id: 'qtd_metros_porcento_orcamentos_rodape',
											anchor: '100%',
											fieldLabel: 'Acréscimo (m) em %'
										},
										{
											xtype: 'textfield',
											name: 'total_area',
											id: 'total_area_orcamentos_rodape',
											allowBlank: true,
											readOnly: true,
											flex: 1,
											anchor: '100%',
											mask: '#9.999.990,00',
											plugins: 'textmask',
											money: true,
											fieldLabel: 'Total da Area em (m)'
										}
									]
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'id_produtos_orcamentos_rodape',
							hidden: true,
							id: 'id_produtos_orcamentos_rodape_produtos_orcamentos_rodape',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'id_orcamento',
							hidden: true,
							id: 'id_orcamento_produtos_orcamentos_rodape',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'item_nro',
							hidden: true,
							value: '0',
							id: 'item_nro_produtos_orcamentos_rodape',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'possui_ambiente',
							hidden: true,
							id: 'possui_ambiente_produtos_orcamentos_rodape',
							value: 'S',
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_produtos_orcamentos_rodape',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produtos_orcamentos_rodape',
							iconCls: 'bt_cancel',
							/*handler: function() {
								Ext.Msg.confirm('Confirmar', 'Deseja Cancelar?', function(btn){
									if(btn=='yes'){
										me.close(btn);
									}
								});
							},*/
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produtos_orcamentos_rodape',
							iconCls: 'bt_save',
							action: 'salvar',
							disabled: true,
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);

	}

});
