/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos_manutencoes.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filterveiculos_manutencoeswin',

	id: 'FilterVeiculos_ManutencoesWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	title: 'Filtro de Veiculos_Manutencoes',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterVeiculos_Manutencoes',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'cod_veiculo',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'cod_veiculo_filter_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Cod Veiculo'
								},								
								{
									xtype: 'numberfield',
									name: 'hodometro',																		
									flex: 1,
									id: 'hodometro_filter_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Hodômetro'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'tipo_combustivel',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'tipo_combustivel_filter_veiculos_manutencoes',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Tipo Combustivel'
								},								
								{
									xtype: 'textfield',
									name: 'quantidade',																		
									flex: 1,
									id: 'quantidade_filter_veiculos_manutencoes',																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Quantidade'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'valor_abastecimento',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'valor_abastecimento_filter_veiculos_manutencoes',									
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,																											
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Valor Abastecimento'
								},								
								{
									xtype: 'datefield',
									format: 'd/m/Y',																		
									flex: 1,								
									name: 'data_abastecimento',
									id: 'data_abastecimento_filter_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Data Abastecimento'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},									
									margin: '0 5 0 0',									
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Cadastro',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cadastro_date_filter_veiculos_manutencoes',
											name: 'data_cadastro_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro_time_filter_veiculos_manutencoes',
											name: 'data_cadastro_time',
											hideLabel: true
										}
									]
								},								
								{
									xtype: 'numberfield',
									name: 'cadastrado_por',																		
									flex: 1,
									id: 'cadastrado_por_filter_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Cadastrado Por'
								}								

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'alterado_por',									
									margin: '0 5 0 0',									
									flex: 1,
									id: 'alterado_por_filter_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Alterado Por'
								},								
								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},																		
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',			    
									fieldLabel: 'Data Alteracao',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_alteracao_date_filter_veiculos_manutencoes',
											name: 'data_alteracao_date',
											margins: '0 5 0 0',
											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_alteracao_time_filter_veiculos_manutencoes',
											name: 'data_alteracao_time',
											hideLabel: true
										}
									]
								}								

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_veiculos_manutencoes',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Resetar Filtro'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}

});
