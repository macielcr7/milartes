/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos_manutencoes.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addveiculos_manutencoeswin',

	id: 'AddVeiculos_ManutencoesWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	title: 'Cadastro de Manuteções',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormVeiculos_Manutencoes',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/veiculos_manutencoes/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'hodometro',
									flex: 1,
									margin: '0 5 0 0',
									mask: '#9.999.990.000',
									plugins: 'textmask',
									money: true,
									allowBlank: false,
									id: 'hodometro_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Hodômetro',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'combobox',
									store: 'StoreComboVeiculosManutencoes',
									allowBlank: true,
									flex: 1,
									triggerAction:'all',
									typeAhead:false,
									mode:'remote',
									minChars:2,
									forceSelection: false,
									//hideTrigger:true,
									queryMode: 'remote',
									anchor: '100%',
									name: 'tipo_manutencao',
									id: 'tipo_combustivel_veiculos_manutencoes',
									fieldLabel: 'Tipo de Manutenção', 
									maxLength: 100,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboFornecedores',
									name: 'oficina',
									allowBlank: false,
									id: 'oficina_veiculos_manutencoes',
									flex: 1,
									anchor: '100%',
									fieldStyle: 'text-transform:uppercase',
									fieldLabel: 'Oficina'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'valor_manutencao',
									margin: '0 5 0 0',
									flex: 1,
									id: 'valor_abastecimento_veiculos_manutencoes',
									anchor: '100%',
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									fieldLabel: 'Valor Manutenção'
								},								
								{
									xtype: 'textfield',
									flex: 1,
									mask: '99/99/9999',
									returnWithMask: true,
									plugins: 'textmask',
									allowBlank: false,
									name: 'data_manutencao',
									id: 'data_abastecimento_veiculos_manutencoes',
									anchor: '100%',
									fieldLabel: 'Data Manutenção',
									validator: function(value){
										if(value!="" && !validDate(value)){
											return 'Data Inválida...';
										}
										else{
											return true;
										}
									}
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textarea',
									name: 'detalhamento',
									allowBlank: true,
									id: 'detalhamento_veiculos_manutencoes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Detalhamento do Serviço/Peças',
									maxLength: 255,
									enforceMaxLength: true,
									fieldStyle: 'text-transform:uppercase'
								},
							]
						},
						{
							xtype: 'numberfield',
							name: 'cod_veiculo',
							margin: '0 5 0 0',
							flex: 1,
							hidden: true,
							id: 'cod_veiculo_veiculos_manutencoes',
							anchor: '100%',
							fieldLabel: 'Cod Veículo'
						},
						{
							xtype: 'hidden',
							name: 'id',
							hidden: true,
							id: 'id_veiculos_manutencoes',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_veiculos_manutencoes',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_veiculos_manutencoes',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							//action: 'resetar',
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_veiculos_manutencoes',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});