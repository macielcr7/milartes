/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.veiculos_manutencoes.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridveiculos_manutencoeslist',
	requires: [
		'ShSolutions.store.StoreVeiculos_Manutencoes'
	],

	id: 'GridVeiculos_Manutencoes',
	store: 'StoreVeiculos_Manutencoes',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},

	columns: [
		{
			xtype: 'numbercolumn',
			dataIndex: 'id',
			hidden: true,
			format: '0',
			text: 'Id',
			width: 140
		},
		{
			xtype: 'numbercolumn',
			dataIndex: 'cod_veiculo',
			format: '0',
			hidden: true,
			text: 'Cod Veiculo',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'hodometro',
			text: 'Hodômetro',
			width: 90
		},
		{
			xtype: 'datecolumn',
			dataIndex: 'data_manutencao',
			format: 'd/m/Y',
			renderer : Ext.util.Format.dateRenderer('d/m/Y'),
			text: 'Data Manutenção',
			width: 120
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'valor_manutencao',
			renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
			text: 'Valor Manutenção',
			width: 120
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'tipo_manutencao',
			text: 'Tipo Manutenção',
			width: 220
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'oficina_nome',
			text: 'Oficina',
			width: 340
		},
		{
			xtype: 'datecolumn',
			dataIndex: 'data_cadastro',
			format: 'd/m/Y H:i:s',
			renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
			text: 'Data Cadastro',
			width: 140
		}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: 'StoreVeiculos_Manutencoes',
			dock: 'bottom'
		},
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_veiculos_manutencoes',
					iconCls: 'bt_add',
					hidden: true,
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_veiculos_manutencoes',
					iconCls: 'bt_edit',
					hidden: true,
					disabled: true,
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_veiculos_manutencoes',
					iconCls: 'bt_del',
					hidden: true,
					disabled: true,
					action: 'deletar',
					text: 'Deletar'
				},
				{
					xtype: 'button',
					id: 'button_filter_veiculos_manutencoes',
					iconCls: 'bt_lupa',
					action: 'filtrar',
					text: 'Filtrar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});