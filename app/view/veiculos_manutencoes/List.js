/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.veiculos_manutencoes.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.veiculos_manutencoeslist',
	requires: [
		'ShSolutions.store.StoreVeiculos_Manutencoes'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'veiculos_manutencoes',

	id: 'List-Veiculos_Manutencoes',
	layout: {
		type: 'fit'
	},
	height: 350,
	width: 1100,
	title: 'Abastecimentos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridveiculos_manutencoeslist'
				}
			]
		});

		me.callParent(arguments);
	}
});