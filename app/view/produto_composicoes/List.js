/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_composicoes.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produto_composicoeslist',
    requires: [
    	'ShSolutions.store.StoreProduto_Composicoes'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'produto_composicoes',

    id: 'List-Produto_Composicoes',
    layout: {
        type: 'fit'
    },
    height: 350,
    title: 'Listagem de Produto_Composicoes',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'gridproduto_composicoeslist'
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


