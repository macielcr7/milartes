/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.produto_composicoes.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridproduto_composicoeslist',
	requires: [
		'ShSolutions.store.StoreProduto_Composicoes'
	],
	id: 'GridProduto_Composicoes',
   	store: 'StoreProduto_Composicoes',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,			
	columns: [
		{xtype: 'rownumberer'},
		{
			xtype: 'gridcolumn',
			dataIndex: 'id_composicao',
			text: 'ID do Produto',
			width: 50
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'composicao',
			text: 'Composição',
			width: 200
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'quantidade',
			text: 'Quantidade',
			renderer: Ext.util.Format.maskRenderer('#9.999.990,0000', true),
			width: 80
		}
	],
    dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_produto_composicoes',
					iconCls: 'bt_add',
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_produto_composicoes',
					iconCls: 'bt_edit',							
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_produto_composicoes',
					iconCls: 'bt_del',							
					action: 'deletar',
					text: 'Deletar'
				}								
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});