/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_composicoes.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filterproduto_composicoeswin',

    id: 'FilterProduto_ComposicoesWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Produto_Composicoes',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterProduto_Composicoes',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboProdutos',
                                    name: 'id_produto',
									id: 'id_produto_filter_produto_composicoes',
									button_id: 'button_id_produto_filter_produto_composicoes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Produto'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_produto_filter_produto_composicoes',
                                    combo_id: 'id_produto_filter_produto_composicoes',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
                            xtype: 'fieldcontainer',
                            autoHeight: true,
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
									xtype: 'combobox',
                                    store: 'StoreComboProdutos',
                                    name: 'id_composicao',
									id: 'id_composicao_filter_produto_composicoes',
									button_id: 'button_id_composicao_filter_produto_composicoes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Composição'
								},
                                {
                                    xtype: 'buttonadd',
                                    iconCls: 'bt_cancel',
                                    hidden: true,
                                    id: 'button_id_composicao_filter_produto_composicoes',
                                    combo_id: 'id_composicao_filter_produto_composicoes',
                                    action: 'reset_combo'
                                }
                            ]
                        },
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_produto_composicoes',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
