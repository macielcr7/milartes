/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produto_composicoes.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addproduto_composicoeswin',

	id: 'AddProduto_ComposicoesWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 400,
	
	title: 'Cadastro de Composições',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormProduto_Composicoes',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/produto_composicoes/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutos',
									name: 'id_composicao',
									id: 'id_composicao_produto_composicoes',
									button_id: 'button_id_composicao_produto_composicoes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Composição'
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_id_composicao_produto_composicoes',
									combo_id: 'id_composicao_produto_composicoes',
									action: 'reset_combo'
								},
								{
									xtype: 'buttonadd',
									tabela: 'Produtos',
									action: 'add_win'
								}
							]
						},
						/*{
							xtype: 'numberfield',
							name: 'quantidadeXX',
							decimalPrecision:2,
							allowDecimals:true,
							alwaysDisplayDecimals: true,
							useThousandSeparator : true,
							thousandSeparator:'.',
							decimalSeparator :',',
							id: 'quantidade_composicoesXX',
							anchor: '100%',
							fieldLabel: 'QuantidadeXX'
						},*/
						{
							xtype: 'textfield',
							name: 'quantidade',
							id: 'quantidade_composicoes',
							anchor: '100%',
							mask: '#9.999.990,0000',
							plugins: 'textmask',
							money: true,    
							fieldLabel: 'Quantidade'
						},
						{
							xtype: 'hidden',
							name: 'id_produto',
							id: 'id_produto_produto_composicoes',
							hidden: true,
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'id_produto_composicoes',
							hidden: true,
							id: 'id_produto_composicoes_produto_composicoes',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_produto_composicoes',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produto_composicoes',
							iconCls: 'bt_cancel',
							action: 'resetar',
							text: 'Resetar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produto_composicoes',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});