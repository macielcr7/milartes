/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.clientes_anotacoes.Edit', {
    extend: 'Ext.window.Window',
	alias: 'widget.addclientes_anotacoeswin',

    id: 'AddClientes_AnotacoesWin',
    layout: {
        type: 'fit'
    },
	maximizable: false,
    minimizable: true,
	
    title: 'Cadastro de Anotações',

    initComponent: function() {
        var me = this;


        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'FormClientes_Anotacoes',
                    bodyPadding: 10,
                    autoScroll: true,
                    method: 'POST',
                    url : 'server/modulos/clientes_anotacoes/save.php',
                    items: [
						{
							xtype: 'hidden',
							name: 'cod_cliente',
							id: 'cod_cliente_clientes_anotacoes',
							anchor: '100%',						
							fieldLabel: 'Cod Cliente'
						},
						{
							xtype: 'textarea',
							name: 'anotacao',
							id: 'anotacao_clientes_anotacoes',
							anchor: '100%',							
							fieldLabel: 'Anotação',
							fieldStyle: 'text-transform:uppercase'
						},
						{
	                        xtype: 'fieldcontainer',
	                        autoHeight: true,								    
						    flex: 1,
						    anchor: '100%',	
	                        layout: {
	                            align: 'stretch',
	                            type: 'hbox'
	                        },
	                        items: [
	                            {
									xtype: 'combobox',
	                                store: 'StoreComboAtivoClientesAnotacoes',
	                                name: 'ativo',
	                                value: 'S',
									loadDisabled: true,
									id: 'ativo_clientes_anotacoes',
									button_id: 'button_ativo_clientes_anotacoes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Ativo',
									fieldStyle: 'text-transform:uppercase'
								},
	                            {
	                                xtype: 'buttonadd',
	                                iconCls: 'bt_cancel',
	                                hidden: true,
	                                id: 'button_ativo_clientes_anotacoes',
	                                combo_id: 'ativo_clientes_anotacoes',
	                                action: 'reset_combo'
	                            }
	                        ]
	                    },
						{
							xtype: 'hidden',
							name: 'controle',
							hidden: true,
							id: 'controle_clientes_anotacoes',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_clientes_anotacoes',
							value: 'INSERIR',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            id: 'button_resetar_clientes_anotacoes',
                            iconCls: 'bt_cancel',
                            handler: function() {
                            	me.close(this);
                            },
                            //action: 'resetar',
                            text: 'Cancelar'
                        },
                        {
                            xtype: 'button',
                            id: 'button_salvar_clientes_anotacoes',
                            iconCls: 'bt_save',
                            action: 'salvar',
                            text: 'Salvar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);

    }

});
