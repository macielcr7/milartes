/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.clientes_anotacoes.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filterclientes_anotacoeswin',

    id: 'FilterClientes_AnotacoesWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Anotações',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterClientes_Anotacoes',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'hidden',
							allowDecimals: false,
							name: 'cod_cliente',
							id: 'cod_cliente_filter_clientes_anotacoes',
							anchor: '100%',
							fieldLabel: 'Cod Cliente'
						},
						{
							xtype: 'textarea',
							name: 'anotacao',
							id: 'anotacao_filter_clientes_anotacoes',							
							anchor: '100%',
							fieldLabel: 'Anota&ccedil;&atilde;o',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 1000);
								}
							}
						},
						{
	                        xtype: 'fieldcontainer',
	                        autoHeight: true,								    
						    flex: 1,
						    anchor: '100%',	
	                        layout: {
	                            align: 'stretch',
	                            type: 'hbox'
	                        },
	                        items: [
	                            {
									xtype: 'combobox',
	                                store: 'StoreComboAtivoClientesAnotacoes',
	                                name: 'ativo',
	                                allowBlank: true,
									loadDisabled: true,
									id: 'ativo_filter_clientes_anotacoes',
									button_id: 'button_ativo_filter_clientes_anotacoes',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Ativo'
								},
	                            {
	                                xtype: 'buttonadd',
	                                iconCls: 'bt_cancel',
	                                hidden: true,
	                                id: 'button_ativo_filter_clientes_anotacoes',
	                                combo_id: 'ativo_filter_clientes_anotacoes',
	                                action: 'reset_combo'
	                            }
	                        ]
	                    },
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_clientes_anotacoes',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            handler: function() {
                            	me.close(this);
                            },
                            text: 'Cancelar/Fechar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
