/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.calendario.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.calendariolist',

	id: 'List-Calendario',
	layout: {
		type: 'fit'
	},
	maximized: true,
	maximizable: true,
	minimizable: true,
	title: 'Calendario',

	requires: [
		'Ext.container.Viewport',
        'Ext.layout.container.Border',
        'Extensible.calendar.CalendarPanel',
        'Extensible.calendar.gadget.CalendarListPanel',
        'Extensible.calendar.data.MemoryCalendarStore',
        'Extensible.calendar.data.MemoryEventStore',
        'Extensible.example.calendar.data.Events',
        'Extensible.example.calendar.data.Calendars'
	],

	calendarStore: null,
	eventStore: null,
	apiBase: null,

	startRemote(){
		Extensible.calendar.data.EventMappings = {
			EventId:     {name: 'EventId', mapping:'id', type:'string'},
			CalendarId:  {name: 'CalendarId', mapping: 'calendar_id', type: 'string'},
			Title:       {name: 'Title', mapping: 'title'},
			StartDate:   {name: 'Start', mapping: 'start', type: 'date', dateFormat: 'c'},
			EndDate:     {name: 'End', mapping: 'end', type: 'date', dateFormat: 'c'},
			Location:    {name: 'Location', mapping: 'location'},
			Notes:       {name: 'Notes', mapping: 'notes'},
			Url:         {name: 'Url', mapping: 'url'},
			IsAllDay:    {name: 'IsAllDay', mapping: 'all_day', type: 'boolean'},
			Reminder:    {name: 'Reminder', mapping: 'reminder'}
		};
		Extensible.calendar.data.EventModel.reconfigure();

		this.calendarStore = Ext.create('Extensible.calendar.data.MemoryCalendarStore', {
			autoLoad: true,
			proxy: {
				type: 'ajax',
				url: 'server/modulos/calendario/list.php',
				noCache: false,
				
				reader: {
					type: 'json',
					root: 'dados'
				}
			}
		});

		this.apiBase = 'server/modulos/calendario/event.php?action=';
		this.eventStore = Ext.create('Extensible.calendar.data.EventStore', {
			autoLoad: true,
			proxy: {
				type: 'ajax',
				noCache: false,
				pageParam: null,
				startParam: null,
				limitParam: null,
				
				api: {
					read:    this.apiBase + 'load',
					create:  this.apiBase + 'add',
					update:  this.apiBase + 'update',
					destroy: this.apiBase + 'delete'
				},
				reader: {
					type: 'json',
					root: 'dados'
				},
				writer: {
					type: 'json',
					nameProperty: 'mapping'
				}
			},

			listeners: {
				'write': function(store, operation) {
					var title = Ext.value(operation.records[0].data[Extensible.calendar.data.EventMappings.Title.name], '(No title)');
					switch(operation.action){
						case 'create':
							info('Add', 'Added "' + title + '"');
							break;
						case 'update':
							info('Update', 'Updated "' + title + '"');
							break;
						case 'destroy':
							info('Delete', 'Deleted "' + title + '"');
							break;
					}
				}
			}
		});
	},

	initComponent: function() {
		this.startRemote();
		var startDay = 0;
		
        /*this.calendarStore = Ext.create('Extensible.calendar.data.MemoryCalendarStore', {
            data: Extensible.example.calendar.data.Calendars.getData()
        });
        this.eventStore = Ext.create('Extensible.calendar.data.MemoryEventStore', {
            data: Extensible.example.calendar.data.Events.getData(),
            autoMsg: false
        });*/
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					id: 'app-center',
					title: '...', 
					layout: 'border',
					listeners: {
						'afterrender': function(){
							Ext.getCmp('app-center').header.addCls('app-center-header');
						}
					},
					items: [
						{
							id:'app-west',
							region: 'west',
							width: 179,
							border: false,
							items: [
								{
									xtype: 'datepicker',
									id: 'app-nav-picker',
									cls: 'ext-cal-nav-picker',
									startDay: startDay,
									listeners: {
										'select': {
											fn: function(dp, dt){
												Ext.getCmp('app-calendar').setStartDate(dt);
											},
											scope: this
										}
									}
								},
								{
									xtype: 'extensible.calendarlist',
									store: this.calendarStore,
									border: false,
									width: 178
								}
							]
						},
						{
							xtype: 'extensible.calendarpanel',
							eventStore: this.eventStore,
							calendarStore: this.calendarStore,
							border: false,
							id:'app-calendar',
							region: 'center',
							activeItem: 1, // month view
							startDay: startDay,

							// Any generic view options that should be applied to all sub views:
							viewConfig: {
								//enableFx: false,
								//ddIncrement: 10, //only applies to DayView and subclasses, but convenient to put it here
								//viewStartHour: 6,
								//viewEndHour: 18,
								//minEventDisplayMinutes: 15
								showTime: false
							},

							// View options specific to a certain view (if the same options exist in viewConfig
							// they will be overridden by the view-specific config):
							monthViewCfg: {
								showHeader: true,
								showWeekLinks: true,
								showWeekNumbers: true
							},

							multiWeekViewCfg: {
								//weekCount: 3
							},

							// Some optional CalendarPanel configs to experiment with:
							//readOnly: true,
							//showDayView: false,
							//showMultiDayView: true,
							//showWeekView: false,
							//showMultiWeekView: false,
							//showMonthView: false,
							//showNavBar: false,
							//showTodayText: false,
							//showTime: false,
							//editModal: true,
							//enableEditDetails: false,
							//title: 'My Calendar', // the header of the calendar, could be a subtitle for the app

							listeners: {
								'eventclick': {
									fn: function(vw, rec, el){
										this.clearMsg();
									},
									scope: this
								},
								'eventover': function(vw, rec, el){
									//console.log('Entered evt rec='+rec.data[Extensible.calendar.data.EventMappings.Title.name]', view='+ vw.id +', el='+el.id);
								},
								'eventout': function(vw, rec, el){
									//console.log('Leaving evt rec='+rec.data[Extensible.calendar.data.EventMappings.Title.name]+', view='+ vw.id +', el='+el.id);
								},
								'eventadd': {
									fn: function(cp, rec){
										this.showMsg('Event '+ rec.data[Extensible.calendar.data.EventMappings.Title.name] +' was added');
									},
									scope: this
								},
								'eventupdate': {
									fn: function(cp, rec){
										this.showMsg('Event '+ rec.data[Extensible.calendar.data.EventMappings.Title.name] +' was updated');
									},
									scope: this
								},
								'eventcancel': {
									fn: function(cp, rec){
										// edit canceled
									},
									scope: this
								},
								'viewchange': {
									fn: function(p, vw, dateInfo){
										if(dateInfo){
											this.updateTitle(dateInfo.viewStart, dateInfo.viewEnd);
										}
									},
									scope: this
								},
								'dayclick': {
									fn: function(vw, dt, ad, el){
										this.clearMsg();
									},
									scope: this
								},
								'rangeselect': {
									fn: function(vw, dates, onComplete){
										this.clearMsg();
									},
									scope: this
								},
								'eventcopy': {
									fn: function(vw, rec){
										this.onEventCopyOrMove(rec, 'copy');
									},
									scope: this
								},
								'eventmove': {
									fn: function(vw, rec){
										this.onEventCopyOrMove(rec, 'move');
									},
									scope: this
								},
								'eventresize': {
									fn: function(vw, rec){
										rec.commit();
										this.showMsg('Event '+ rec.data[Extensible.calendar.data.EventMappings.Title.name] +' was updated');
									},
									scope: this
								},
								'eventdelete': {
									fn: function(win, rec){
										this.eventStore.remove(rec);
										this.showMsg('Event '+ rec.data[Extensible.calendar.data.EventMappings.Title.name] +' was deleted');
									},
									scope: this
								},
								'initdrag': {
									fn: function(vw){
										// do something when drag starts
									},
									scope: this
								}
							}
						}
					]
            	}
			]
		});

		me.callParent(arguments);
	},

	updateTitle: function(startDt, endDt){
        var p = Ext.getCmp('app-center'),
            fmt = Ext.Date.format;

        if(Ext.Date.clearTime(startDt).getTime() === Ext.Date.clearTime(endDt).getTime()){
            p.setTitle(fmt(startDt, 'F j, Y'));
        }
        else if(startDt.getFullYear() === endDt.getFullYear()){
            if(startDt.getMonth() === endDt.getMonth()){
                p.setTitle(fmt(startDt, 'F j') + ' - ' + fmt(endDt, 'j, Y'));
            }
            else{
                p.setTitle(fmt(startDt, 'F j') + ' - ' + fmt(endDt, 'F j, Y'));
            }
        }
        else{
            p.setTitle(fmt(startDt, 'F j, Y') + ' - ' + fmt(endDt, 'F j, Y'));
        }
    },

    // Handle event moves or copies generically
    onEventCopyOrMove: function(rec, mode) {
        var mappings = Extensible.calendar.data.EventMappings,
            time = rec.data[mappings.IsAllDay.name] ? '' : ' \\a\\t g:i a',
            action = mode === 'copy' ? 'copied' : 'moved';

        rec.commit();

        this.showMsg('Event '+ rec.data[mappings.Title.name] +' was ' + action + ' to '+
            Ext.Date.format(rec.data[mappings.StartDate.name], ('F jS'+time)));
    },

    // This is an application-specific way to communicate CalendarPanel event messages back to the user.
    // This could be replaced with a function to do "toast" style messages, growl messages, etc. This will
    // vary based on application requirements, which is why it's not baked into the CalendarPanel.
    showMsg: function(msg){
        //Ext.fly('app-msg').update(msg).removeCls('x-hidden');
    },

    clearMsg: function(){
        //Ext.fly('app-msg').update('').addCls('x-hidden');
    }
});