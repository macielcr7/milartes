/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas_custos.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addvendas_custoswin',

	id: 'AddVendas_CustosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 600,
	title: 'Ítem da Venda',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormVendas_Custos',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/vendas_custos/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutosCusto',
									name: 'cod_produto',
									id: 'cod_produto_vendas_custos',
									button_id: 'button_cod_produto_vendas_custos',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Produto',
									fieldStyle: 'text-transform:uppercase',
									listeners: {
										afterrender: function(fld) {
											fld.focus(true, 800);
										}
									},
									queryMode: 'remote',
									triggerAction: 'all',
									typeAhead: false,
									minChars: 3
								},
								{
									xtype: 'buttonadd',
									iconCls: 'bt_cancel',
									hidden: true,
									id: 'button_cod_produto_vendas_custos',
									combo_id: 'cod_produto_vendas_custos',
									action: 'reset_combo'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboProdutosCores',
									name: 'id_cor',
									id: 'cor_vendas_custos',
									margin: '0 5 0 0',
									loadDisabled: true,
									disabled: true,
									hidden: true,
									anchor: '100%',
									fieldLabel: 'Cor'
								},
								{
									xtype: 'numberfield',
									name: 'quantidade',
									id: 'quantidade_vendas_custos',
									margin: '0 5 0 0',
									flex: 1,
									anchor: '100%',
									minValue: 0.1,
									selectOnFocus: true,
									fieldLabel: 'Quantidade'
								},
								{
									xtype: 'textfield',
									name: 'valor_unitario',
									id: 'valor_unitario_vendas_custos',
									hidden: true,
									flex: 1,
									anchor: '100%',	
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									fieldLabel: 'Valor Unitário'
								}
							]
						},

						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							flex: 1,
							anchor: '100%', 
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'acrescimo_produto',
									id: 'acrescimo_produto_vendas_custos',
									flex: 1,
									margin: '0 5 0 0',
									anchor: '100%',	
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									allowBlank: true,
									fieldLabel: 'Acréscimo para o Produto'
								},
								{
									xtype: 'textfield',
									name: 'acrescimo_mo',
									id: 'acrescimo_mo_vendas_custos',
									flex: 1,
									anchor: '100%',	
									mask: '#9.999.990,00',
									plugins: 'textmask',
									money: true,
									selectOnFocus: true,
									allowBlank: true,
									fieldLabel: 'Acréscimo para a Mão de Obra'
								}
							]
						},


						{
							xtype: 'textarea',
							name: 'observacoes',
							allowBlank: true,
							id: 'observacoes_vendas_custos',
							anchor: '100%',
							fieldLabel: 'Observações', 
							maxLength: 255,
							enforceMaxLength: true,
							fieldStyle: 'text-transform:uppercase'
						},
						{
							xtype: 'textfield',
							name: 'id',
							hidden: true,
							id: 'id_vendas_custos',
							value: 0,
							fieldLabel: 'ID', 
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'cod_venda',
							hidden: true,
							id: 'cod_venda_vendas_custos',
							value: 0,
							fieldLabel: 'Cod Venda', 
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'cod_item_venda',
							hidden: true,
							id: 'cod_item_venda_vendas_custos',
							value: 0,
							fieldLabel: 'Cod Item Venda', 
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							id: 'action_vendas_custos',
							value: 'INSERIR',
							fieldLabel: 'Ação', 
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_vendas_custos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_vendas_custos').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_vendas_custos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});
		me.callParent(arguments);
	}
});