/**
*   @Autor: Maciel Sousa
*   @Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.vendas_custos.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridvendas_custoslist',
	requires: [
		'ShSolutions.store.StoreVendas_Custos'
	],
	id: 'GridVendas_Custos',
	store: 'StoreVendas_Custos',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			viewConfig: {
				autoScroll: true,
				loadMask: false
			},
			features: [{
				ftype: 'summary',
				dock: 'bottom'
			}],
			columns: [
				{
					xtype: 'numbercolumn',
					dataIndex: 'id',
					format: '0',
					text: 'Id',
					hidden: true,
					width: 40
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'produto',
					width: 400,
					text: 'Produto',
					renderer: function(v, m, r){
						var descricao_produto = v;
						var nome_cor = "";

						if (r.get('cor') != "") {
							nome_cor += ' > <span style="background-color: '+r.get('css_cor')+'; padding:2px"> <b>'+r.get('cor')+'</b></span>';
						}

						return descricao_produto += nome_cor;
					},
					sortable: false,
					summaryType: function(records){
						var custoVenda = 0;
						var texto = '';
						var total_venda = '';
						var lucro = '';
						var porcentagem = '';
						var me = this;
						Ext.Array.forEach(records, function (record){
							//console.log(record);
							custoVenda += record.data.valor_total;
							texto = '<br /><br />Data Instalação: '+record.data.data_instalacao_início+' até '+record.data.data_instalacao_fim+'<br />';
							texto = texto+'Instalador: '+record.data.nome_instalador;
							texto = texto+'<br /><br /><br />Total da Venda.....: '+record.data.valor_liquido_venda.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+'<br />';
							total_venda = record.data.valor_liquido_venda;
						});
						texto = texto+'Total de Custo......: ';
						texto = texto.concat(custoVenda.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
						lucro = total_venda - custoVenda;
						porcentagem = ((total_venda/custoVenda)-1)*100;
						texto = texto.concat('<br />Lucro de.................: '+lucro.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+' ('+porcentagem.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+'%)');
						return texto;
					},
					summaryRenderer: function(value, summaryData, dataIndex) {
						return value;
					}
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'quantidade',
					text: 'Quantidade',	
					anchor: '100%',
					width: 80,
					renderer: function(v, m, r){
						if(r.get('sigla_um')=='M²' || r.get('sigla_um')=='M' || r.get('sigla_um')=='KM' || r.get('sigla_um')=='L'){
							return converteFloatMoeda(v)+' '+r.get('sigla_um');
						}
						else{
							return v+' '+r.get('sigla_um');
						}
					},
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_unitario',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Unitário',
					anchor: '100%',
					hidden: true,
					width: 80,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'valor_total',
					renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
					text: 'Valor Total',
					anchor: '100%',
					hidden: true,
					width: 100,
					sortable: false,
					fixed: true
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'observacoes',
					text: 'Observações',
					anchor: '100%',
					sortable: false,
					flex: 1,
					anchor: '100%',
				}
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					displayInfo: true,
					store: 'StoreVendas_Custos',
					dock: 'bottom'
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							xtype: 'button',
							id: 'button_add_vendas_custos',
							iconCls: 'bt_add',
							action: 'adicionar',
							text: 'Adicionar'
						},
						{
							xtype: 'button',
							id: 'button_edit_vendas_custos',
							iconCls: 'bt_edit',
							action: 'editar',
							text: 'Editar'
						},
						{
							xtype: 'button',
							id: 'button_del_vendas_custos',
							iconCls: 'bt_del',
							action: 'deletar',
							text: 'Deletar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});