/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.vendas_custos.List', {
    extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.vendas_custoslist',
    requires: [
    	'ShSolutions.store.StoreVendas_Custos'
    ],
	
    maximizable: true,
    minimizable: true,
    iconCls: 'vendas_custos',

    id: 'List-Vendas_Custos',
    layout: {
        type: 'fit'
    },
    height: 500,
    width: 1100,
    title: 'Conferindo Produtos da Venda....',

    initComponent: function() {
    	var me = this;
    	Ext.applyIf(me, {
    		items: [
    		    {
    		    	xtype: 'gridvendas_custoslist'
    		    }
    		]
    	});

    	me.callParent(arguments);
    }
});


