/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos_produto.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.atributos_produtolist',
	requires: [
		'ShSolutions.store.StoreAtributos_Produto'
	],
	
	maximizable: true,
	minimizable: true,
	iconCls: 'atributos_produto',

	id: 'List-Atributos_Produto',
	layout: {
		type: 'fit'
	},
	height: 350,
	title: 'Listagem dos Atributos do Produto',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'gridatributos_produtolist'
				}
			]
		});

		me.callParent(arguments);
	}
});