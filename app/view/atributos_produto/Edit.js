/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos_produto.Edit', {
    extend: 'Ext.window.Window',
	alias: 'widget.addatributos_produtowin',

    id: 'AddAtributos_ProdutoWin',
    layout: {
        type: 'fit'
    },
	maximizable: false,
    minimizable: true,
	
    title: 'Cadastro de Atributos_Produto',

    initComponent: function() {
        var me = this;


        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'FormAtributos_Produto',
                    bodyPadding: 10,
                    autoScroll: true,
                    method: 'POST',
                    url : 'server/modulos/atributos_produto/save.php',
                    items: [
						{
							xtype: 'numberfield',
							name: 'id_atributo_categoria',
							id: 'id_atributo_categoria_atributos_produto',
							anchor: '100%',						
							fieldLabel: 'Atributo Categoria'
						},
						{
							xtype: 'textfield',
							name: 'valor',
							id: 'valor_atributos_produto',
							anchor: '100%',							
							fieldLabel: 'Valor'
						},
						{
							xtype: 'hidden',
							name: 'id_atributos_produto',
							hidden: true,
							id: 'id_atributos_produto_atributos_produto',
							value: 0,
							anchor: '100%'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_atributos_produto',
							value: 'INSERIR',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            id: 'button_resetar_atributos_produto',
                            iconCls: 'bt_cancel',
                            action: 'resetar',
                            text: 'Resetar'
                        },
                        {
                            xtype: 'button',
                            id: 'button_salvar_atributos_produto',
                            iconCls: 'bt_save',
                            action: 'salvar',
                            text: 'Salvar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);

    }

});
