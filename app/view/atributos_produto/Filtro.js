/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.atributos_produto.Filtro', {
    extend: 'Ext.window.Window',
    alias: 'widget.filteratributos_produtowin',

    id: 'FilterAtributos_ProdutoWin',
    layout: {
        type: 'fit'
    },
	modal: true,
    minimizable: false,
    
    title: 'Filtro de Atributos_Produto',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
        	items: [
                {
                    xtype: 'form',
                    id: 'FormFilterAtributos_Produto',
                    bodyPadding: 10,
                    autoScroll: true,
                    items: [
						{
							xtype: 'numberfield',
							allowDecimals: false,
							name: 'id_atributo_categoria',
							id: 'id_atributo_categoria_filter_atributos_produto',
							anchor: '100%',
							fieldLabel: 'Atributo Categoria'
						},
						{
							xtype: 'textfield',
							name: 'valor',
							id: 'valor_filter_atributos_produto',							
							anchor: '100%',
							fieldLabel: 'Valor'
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_atributos_produto',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_cancel',
                            action: 'resetar_filtro',
                            text: 'Resetar Filtro'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'bt_lupa',
                            action: 'filtrar_busca',
                            text: 'Filtrar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
