/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/


Ext.define('ShSolutions.view.atributos_produto.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.gridatributos_produtolist',
	requires: [
		'ShSolutions.store.StoreAtributos_Produto'
	],
	id: 'GridProdutos_Cores',
	store: 'StoreAtributos_Produto',
	height: 350,

	viewConfig: {
		autoScroll: true,
		loadMask: false
	},
	forceFit: true,
	columns: [
		{
			xtype: 'numbercolumn',
			dataIndex: 'id_atributos_produto',
			hidden: true,
			format: '0',
			text: 'Id Atributos Produto',
			width: 140
		},
		{
			xtype: 'numbercolumn',
			dataIndex: 'id_atributo_categoria',
			format: '0',
			text: 'Atributo Categoria',
			width: 140
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'valor',
			text: 'Valor',
			width: 140
		}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: 'StoreAtributos_Produto',
			dock: 'bottom'
		},
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					id: 'button_add_atributos_produto',
					iconCls: 'bt_add',
					hidden: true,
					action: 'adicionar',
					text: 'Adicionar'
				},
				{
					xtype: 'button',
					id: 'button_edit_atributos_produto',
					iconCls: 'bt_edit',
					hidden: true,
					action: 'editar',
					text: 'Editar'
				},
				{
					xtype: 'button',
					id: 'button_del_atributos_produto',
					iconCls: 'bt_del',
					hidden: true,
					action: 'deletar',
					text: 'Deletar'
				},
				{
					xtype: 'button',
					id: 'button_filter_atributos_produto',
					iconCls: 'bt_lupa',
					action: 'filtrar',
					text: 'Filtrar'
				}
			]
		}
	],

	initComponent: function() {
		var me = this;
		
		Ext.applyIf(me, {
		});

		me.callParent(arguments);
	}
});