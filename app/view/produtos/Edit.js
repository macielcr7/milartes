/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produtos.Edit', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.addprodutoswin',

	id: 'AddProdutosWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 590,
	height: 500,
	title: 'Cadastro de Produtos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'tabpanel',
					margins: 5,
					region: 'center',
					activeTab: 0,
					id: 'TabPanelCentralProdutos',
					items: [
						{
							xtype: 'form',
							title:'Formulário Principal',
							id: 'FormProdutos',
							bodyPadding: 10,
							autoScroll: true,
							method: 'POST',
							url : 'server/modulos/produtos/save.php',
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'descricao_completa',
											margin: '0 5 0 0',
											flex: 1,
											id: 'descricao_completa_produtos',
											anchor: '100%',
											fieldLabel: 'Descrição/Título Completo',
											listeners: {
												afterrender: function(fld) {
													fld.focus(true, 1000);
												}
											}
										},
										{
											xtype: 'textfield',
											name: 'descricao_abreviada',
											flex: 1,
											id: 'descricao_abreviada_produtos',
											anchor: '100%',
											fieldLabel: 'Descrição/Título Abreviado (NF)'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'textfield',
													name: 'descricao_impressao',
													flex: 1,
													id: 'descricao_impressao_produtos',
													anchor: '100%',
													fieldLabel: 'Descrição para Impressão Interna'
												}
											]
										}

									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboProdutos_Categorias',
													name: 'categoria',
													id: 'categoria_produtos',
													button_id: 'button_categoria_produtos',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Categoria'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_categoria_produtos',
													combo_id: 'categoria_produtos',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Produtos_Categorias',
													action: 'add_win'
												}
											]
										}

									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboFornecedores',
													name: 'fornecedor',
													id: 'fornecedor_produtos',
													button_id: 'button_fornecedor_produtos',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Fornecedor'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_fornecedor_produtos',
													combo_id: 'fornecedor_produtos',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Fornecedores',
													action: 'add_win'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboSetor_Estoque',
													name: 'setor_estoque',
													id: 'setor_estoque_produtos',
													button_id: 'button_setor_estoque_produtos',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Setor de Estoque'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_setor_estoque_produtos',
													combo_id: 'setor_estoque_produtos',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Setor_Estoque',
													action: 'add_win'
												}
											]
										}

									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboProdutos_Marcas',
													name: 'fabricante_marca',
													id: 'fabricante_marca_produtos',
													button_id: 'button_fabricante_marca_produtos',
													flex: 1,
													allowBlank: false,
													anchor: '100%',
													fieldLabel: 'Marca'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_fabricante_marca_produtos',
													combo_id: 'fabricante_marca_produtos',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Produtos_Marcas',
													action: 'add_win'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboUnidades_Medidas',
													name: 'unidade_medida',
													id: 'unidade_medida_produtos',
													button_id: 'button_unidade_medida_produtos',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Unidade Medida'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_unidade_medida_produtos',
													combo_id: 'unidade_medida_produtos',
													action: 'reset_combo'
												},
												{
													xtype: 'buttonadd',
													tabela: 'Unidades_Medidas',
													action: 'add_win'
												}
											]
										}

									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'ncm',
											margin: '0 5 0 0',
											flex: 1,
											id: 'ncm_produtos',
											anchor: '100%',
											maxLength: 10,
											enforceMaxLength: true,
											fieldLabel: 'NCM',
											value: '111'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboAtivoProdutos',
													name: 'ativo',
													loadDisabled: true,
													id: 'ativo_produtos',
													button_id: 'button_ativo_produtos',
													flex: 1,
													anchor: '100%',
													value: 'S',
													fieldLabel: 'Produto Ativo'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_ativo_produtos',
													combo_id: 'ativo_produtos',
													action: 'reset_combo'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											margin: '0 5 0 0',	
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboControlar_EstoqueProdutos',
													name: 'controlar_estoque',
													loadDisabled: true,
													id: 'controlar_estoque_produtos',
													button_id: 'button_controlar_estoque_produtos',
													flex: 1,
													anchor: '100%',		
													fieldLabel: 'Controlar Estoque',
													value: 'S'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_controlar_estoque_produtos',
													combo_id: 'controlar_estoque_produtos',
													action: 'reset_combo'
												}
											]
										},
										{
											xtype: 'textfield',
											name: 'peso_bruto',
											flex: 1,
											id: 'peso_bruto_produtos',
											anchor: '100%',
											value: '1',
											fieldLabel: 'Peso Bruto'
										}

									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'peso_liquido',
											margin: '0 5 0 0',
											flex: 1,
											id: 'peso_liquido_produtos',
											anchor: '100%',
											value: '1',
											fieldLabel: 'Peso Liquido'
										},
										{
											xtype: 'numberfield',
											name: 'prazo_garantia',
											flex: 1,
											id: 'prazo_garantia_produtos',
											anchor: '100%',
											value: '1',
											fieldLabel: 'Prazo Garantia'
										}

									]
								},
								{
									xtype: 'fieldset',
									title: 'Atributos',
									collapsible: true,
									id: 'panel_attrs',
									defaultType: 'textfield',
									defaults: {anchor: '100%'}
								},
								{
									xtype: 'hidden',
									name: 'id',
									hidden: true,
									id: 'id_produtos',
									value: 0,
									anchor: '100%'
								},
								{
									xtype: 'hidden',
									name: 'action',
									hidden: true,
									id: 'action_produtos',
									value: 'INSERIR',
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'form',
							layout: {
								type: 'form'
							},
							id: 'FormDadosComplementares',
							title:'Dados Complementares',
							bodyPadding: 10,
							autoScroll: true,
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'numberfield',
											name: 'quantidade_minima',
											margin: '0 5 0 0',
											flex: 1,
											id: 'quantidade_minima_produtos',
											anchor: '100%',
											value: '1000',
											fieldLabel: 'Quantidade Minima'
										},
										{
											xtype: 'numberfield',
											name: 'quantidade_disponivel',
											margin: '0 5 0 0',
											flex: 1,
											id: 'quantidade_disponivel_produtos',
											value: '1000',
											anchor: '100%',
											fieldLabel: 'Quantidade Disponivel'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboSimNao',
											name: 'dobra_quantidade',
											allowBlank: false,
											id: 'dobra_quantidade_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Dobra Quantidade'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'desconto_maximo',
											margin: '0 5 0 0',
											flex: 1,
											mask: '#9.999.990,00',
											plugins: 'textmask',
											money: true,
											id: 'desconto_maximo_produtos',
											anchor: '100%',
											value: '1',
											fieldLabel: 'Desconto Maximo'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboTipoProdutoProdutos',
													name: 'tipo_produto',
													loadDisabled: true,
													allowBlank: false,
													id: 'tipo_produto_produtos',
													flex: 1,
													anchor: '100%',
													fieldLabel: 'Tipo do Produto'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									anchor: '100%',
									items: [
										{
											xtype: 'textfield',
											name: 'preco_custo',
											flex: 1,
											allowBlank: false,
											mask: '#9.999.990,00',
											plugins: 'textmask',
											//hidden: true,
											money: true,
											id: 'preco_custo_produtos',
											anchor: '100%',
											fieldLabel: 'Preço de Custo'
										},
										{
											xtype: 'textfield',
											name: 'preco_custo_servico',
											margin: '0 0 0 5',
											flex: 1,
											//hidden: true,
											mask: '#9.999.990,00',
											plugins: 'textmask',
											money: true,
											id: 'preco_custo_servico_produtos',
											anchor: '100%',
											fieldLabel: 'Preço de Custo (Serviço)'
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											margin: '0 5 0 0',
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboComissionar_VendedorProdutos',
													name: 'comissionar_vendedor',
													loadDisabled: true,
													id: 'comissionar_vendedor_produtos',
													button_id: 'button_comissionar_vendedor_produtos',
													flex: 1,
													value: 'S',
													anchor: '100%',		
													fieldLabel: 'Comissionar Vendedor'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_comissionar_vendedor_produtos',
													combo_id: 'comissionar_vendedor_produtos',
													action: 'reset_combo'
												}
											]
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboComissionar_TipoProdutos',
													name: 'comissionar_tipo',
													loadDisabled: true,
													allowBlank: true,
													value: 'MOEDA',
													id: 'comissionar_tipo_produtos',
													button_id: 'button_comissionar_tipo_produtos',
													flex: 1,
													anchor: '100%',		
													fieldLabel: 'Tipo de Comissão'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_comissionar_tipo_produtos',
													combo_id: 'comissionar_tipo_produtos',
													action: 'reset_combo'
												}
											]
										}
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'valor_comissao',
											flex: 1,
											allowBlank: true,
											margin: '0 5 0 0',
											id: 'valor_comissao_produtos',
											mask: '#9.999.990,00',
											plugins: 'textmask',
											money: true,
											fieldLabel: 'Valor Comissão (Moeda)'
										},
										{
											xtype: 'textfield',
											name: 'valor_comissao',
											flex: 1,
											allowBlank: true,
											maxValue: 100,
											disabled: true,
											hidden: true,
											margin: '0 5 0 0',
											id: 'valor_comissao_porcentagem_produtos',
											mask: '#9.999.990,00',
											plugins: 'textmask',
											money: true,
											fieldLabel: 'Valor Comissão (Porcentagem)'
										},
										{
											xtype: 'fieldcontainer',
											autoHeight: true,
											flex: 1,
											layout: {
												align: 'stretch',
												type: 'hbox'
											},
											items: [
												{
													xtype: 'combobox',
													store: 'StoreComboPossui_ComposicaoProdutos',
													name: 'possui_composicao',
													loadDisabled: true,
													id: 'possui_composicao_produtos',
													button_id: 'button_possui_composicao_produtos',
													flex: 1,
													value: 'N',
													anchor: '100%',		
													fieldLabel: 'Possui Composição?'
												},
												{
													xtype: 'buttonadd',
													iconCls: 'bt_cancel',
													hidden: true,
													id: 'button_possui_composicao_produtos',
													combo_id: 'possui_composicao_produtos',
													action: 'reset_combo'
												}
											]
										}
									]
								},
								{
									xtype: 'gridproduto_composicoeslist',
									anchor: '98%',
									disabled: true,
									height: 150,
									loadDisabled: true
								}
							]
						},
						{
							xtype: 'gridprodutos_coreslist',
							disabled: true,
							title: 'Cores do Produto',
							loadDisabled: true
						},
						{
							xtype: 'gridproduto_precoslist',
							disabled: true,
							title: 'Preços de Venda',
							loadDisabled: true
						},
						{
							xtype: 'gridproduto_imagenslist',
							title: 'Imagens do Produto',
							disabled: true,
							loadDisabled: true
						},
						{
							xtype: 'gridlogs_sistemalist',
							id: 'GridProdutos_Logs_Sistema',
							store: 'StoreProdutos_Logs_Sistema',
							title: 'Logs do Produto',
							disabled: true,
							loadDisabled: true
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_produtos',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							disabled: true,
							listeners: {
								afterrender: function(fld) {
									setTimeout(function(){
										Ext.getCmp('button_resetar_produtos').setDisabled(false);
									}, 2500);
								}
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_produtos',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});