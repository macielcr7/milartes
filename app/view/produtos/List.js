/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

//largura do grid
if (screen.width < 1290) {
	var tamanhoX = 1100;
} else if (screen.width < 1370) {
	var tamanhoX = 1250;
} else if (screen.width < 1930) {
	var tamanhoX = 1500;
} else {
	var tamanhoX = 1000;
}

//altura do grid
if (screen.height < 770) {
	var tamanhoY = 600;
} else if (screen.height < 1025) {
	var tamanhoY = 880;
} else if (screen.height < 1090) {
	var tamanhoY = 900;
} else {
	var tamanhoY = 500;
}


Ext.define('ShSolutions.view.produtos.List', {
	extend: 'ShSolutions.view.WindowBig',
	alias: 'widget.produtoslist',
	requires: [
		'ShSolutions.store.StoreProdutos'
	],

	maximizable: true,
	minimizable: true,
	maximized: true,
	iconCls: 'produtos',
	id: 'List-Produtos',
	layout: {
		type: 'border'
	},
	width: tamanhoX,
	height: tamanhoY,
	title: 'Listagem de Produtos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'panel',
					margins: '5 0 5 5',
					title: 'Departamentos/Categorias',
					width: 180,
					split: true,
					region: 'west',
					layout: {
						type:'vbox',
						//padding:'5',
						align:'stretch'
					},
					items: [
						{
							xtype: 'treepanel',
							flex: 1,
							id: 'TreeCategorias',
							//title: 'Categorias',
							useArrows: true,
							store: 'StoreTreeCategorias',
							bodyBorder: false,
							border: false,
							//padding: '5 2 2 2',
							viewConfig: {

							}
						}
					]
				},
				{
					xtype: 'gridpanel',
					margins: '5 5 5 0',
					region: 'center',
					id: 'GridProdutos',
					loadDisabled: true,
					store: 'StoreProdutos',
					viewConfig: {
						autoScroll: true,
						loadMask: false
					},
					columns: [
						{
							xtype: 'numbercolumn',
							dataIndex: 'id',
							hidden: false,
							format: '0',
							text: 'ID',
							width: 40
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao_marca',
							text: 'Marca',
							width: 110
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao_completa',
							text: 'Descrição/Título Completo',
							width: 500
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'unidade_medida_descricao',
							text: 'Unidade Medida',
							width: 110
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao_categoria',
							text: 'Categoria',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'descricao_setor_estoque',
							text: 'Setor de Estoque',
							width: 110
						},
						{
							xtype: 'numbercolumn',
							dataIndex: 'quantidade_disponivel',
							format: '0',
							text: 'Qtde Disp.',
							width: 80
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ativo',
							text: 'Ativo',
							renderer: function(v){
								switch(v){
									case 'S':
									return 'Sim';
									break;
									case 'N':
									return 'Não';
									break;
								}
							},
							width: 60
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'data_cadastro',
							format: 'd/m/Y H:i:s',
							renderer : Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
							text: 'Data Cadastro',
							width: 140
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'cadastrado_por_nome',
							text: 'Cadastrado Por',
							width: 140
						}
					],
					dockedItems: [
						{
							xtype: 'pagingtoolbar',
							displayInfo: true,
							store: 'StoreProdutos',
							dock: 'bottom'
						},
						{
							xtype: 'toolbar',
							dock: 'top',
							items: [
								{
									xtype: 'button',
									id: 'button_add_produtos',
									iconCls: 'bt_add',
									hidden: true,
									action: 'adicionar',
									text: 'Adicionar'
								},
								{
									xtype: 'button',
									id: 'button_edit_produtos',
									iconCls: 'bt_edit',
									hidden: true,
									disabled: true,
									action: 'editar',
									text: 'Editar'
								},
								{
									xtype: 'button',
									id: 'button_del_produtos',
									iconCls: 'bt_del',
									hidden: true,
									disabled: true,
									action: 'deletar',
									text: 'Desativar'
								},
								{
									xtype: 'button',
									id: 'button_filter_produtos',
									iconCls: 'bt_lupa',
									action: 'filtrar',
									text: 'Filtrar'
								},
								{
									xtype: 'button',
									id: 'button_copiar_produtos',
									iconCls: 'bt_copiar',
									hidden: true,
									disabled: true,
									action: 'copiar_produto',
									text: 'Copiar Produto'
								},
								{
									xtype: 'button',
									id: 'button_pdf_produtos',
									iconCls: 'bt_pdf',
									action: 'gerar_pdf',
									text: 'Gerar PDF'
								},
								{
									xtype: 'textfield',
									name: 'query',
									allowBlank: true,
									id: 'query_produtos',
									enableKeyEvents: true, 
									fieldStyle: 'text-transform:uppercase', 
									listeners: {
										afterrender: function(fld) {
															fld.focus(true, 1500);
										},
										keypress : function(textfield,eventObject){
											if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
												Ext.getCmp('button_filter_grid_produtos').fireEvent('click');
											}
										}
									}
								},
								{
									xtype: 'button',
									id: 'button_filter_grid_produtos',
									iconCls: 'bt_lupa2',
									action: 'filtro_grid',
									text: 'Buscar'
								},
								{ 
									xtype: 'tbfill'
								},
								{
									text:'Filtrar Ativo',
									iconCls: 'bt_status',
									menu: {
										items: [
											{
												text: 'Todos',
												checked: false,
												value: 'X',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Ativos',
												checked: true,
												value: 'S',
												group: 'status',
												action: 'filtrar_status'
											},
											{
												text: 'Desativados',
												checked: false,
												value: 'N',
												group: 'status',
												action: 'filtrar_status'
											}
										]
									}
								}
							]
						}
					]
				},
				{
					region: 'south',
					collapsible: true,
					split: true,
					height: 200,
					minHeight: 120,
					title: 'Informações',
					layout: {
						type: 'border',
						padding: 5
					},
					items: [
						{
							title: 'Cores Disponíveis',
							region: 'center',
							minWidth: 350,
							layout: 'fit',
							split: true,
							items: [
								{
									xtype: 'gridpanel',
									id: 'ProdutoCoresLocal',
									loadDisabled: true,
									border: false,
									region: 'center',
									store: 'StoreProdutos_Cores',
									viewConfig: {
										autoScroll: true,
										loadMask: false
									},
									forceFit: true,
									columns: [
										{
											xtype: 'gridcolumn',
											dataIndex: 'descricao',
											text: 'Cor',
											sortable: false,
											fixed: true,
											width: 140
										}
									]
								}
							]
						},
						{
							title: 'Atributos',
							region: 'east',
							flex: 1,
							minWidth: 300,
							layout: 'fit',
							split: true,
							items: [
								{
									xtype: 'gridpanel',
									id: 'ProdutoAtributosLocal',
									loadDisabled: true,
									border: false,
									region: 'center',
									store: 'StoreAtributos_Produto',
									viewConfig: {
										autoScroll: true,
										loadMask: false
									},
									forceFit: true,
									columns: [
										{
											xtype: 'gridcolumn',
											dataIndex: 'descricao_atributo',
											text: 'Descrição',
											sortable: false,
											fixed: true,
											width: 230
										},
										{
											xtype: 'gridcolumn',
											dataIndex: 'valor',
											sortable: false,
											fixed: true,
											renderer: Ext.util.Format.maskRenderer('#9.999.990,00', true),
											text: 'Tamanho',
											width: 70
										}
									]
								}
							]
						},
						{
							title: 'Tabela de Preços',
							region: 'west',
							flex: 1,
							minWidth: 350,
							layout: 'fit',
							split: true,
							items: [
								{
									xtype: 'gridpanel',
									id: 'ProdutoPrecosLocal',
									loadDisabled: true,
									border: false,
									store: 'StoreProduto_Precos',
									viewConfig: {
										layout: 'fit',
										autoScroll: true,
										loadMask: false,
										getRowClass: function(record) { 
											if(record.get('id_categoria_precos')=='1'){
												return 'verde_precos';
											}
											else {
												return 'texto_precos';
											}
										}
									},
									forceFit: true,			
									columns: [
										{
											xtype: 'gridcolumn',
											dataIndex: 'descricao',
											sortable: false,
											fixed: true,
											text: 'Categoria Preços',
											width: 140
										},
										{
											xtype: 'gridcolumn',
											dataIndex: 'valor',
											sortable: false,
											fixed: true,
											renderer: Ext.util.Format.maskRenderer('R$ #9.999.990,00', true),
											text: 'Valor',
											width: 140
										},
										{
											xtype: 'gridcolumn',
											dataIndex: 'total_custo2',
											hidden: true,
											text: '##',
											sortable: false,
											fixed: true,
											width: 60
										}
									]
								}
							]
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});