/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.produtos.Filtro', {
	extend: 'ShSolutions.view.WindowMedium',
	alias: 'widget.filterprodutoswin',

	id: 'FilterProdutosWin',
	layout: {
		type: 'fit'
	},
	modal: true,
	minimizable: false,
	title: 'Filtro de Produtos',

	initComponent: function() {
		var me = this;
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormFilterProdutos',
					bodyPadding: 10,
					autoScroll: true,
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'descricao_completa',
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_completa_filter_produtos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Descrição/Título Completo'
								},
								{
									xtype: 'textfield',
									name: 'descricao_abreviada',
									flex: 1,
									id: 'descricao_abreviada_filter_produtos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'Descrição/Título Abreviado (NF)'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProdutos_Categorias',
											name: 'categoria',
											id: 'categoria_filter_produtos',
											button_id: 'button_categoria_filter_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Categoria'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_categoria_filter_produtos',
											combo_id: 'categoria_filter_produtos',
											action: 'reset_combo'
										},
									]
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboFornecedores',
											name: 'fornecedor',
											id: 'fornecedor_filter_produtos',
											button_id: 'button_fornecedor_filter_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Fornecedor'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_fornecedor_filter_produtos',
											combo_id: 'fornecedor_filter_produtos',
											action: 'reset_combo'
										},
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboSetor_Estoque',
											name: 'setor_estoque',
											id: 'setor_estoque_filter_produtos',
											button_id: 'button_setor_estoque_filter_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Setor de Estoque'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_setor_estoque_filter_produtos',
											combo_id: 'setor_estoque_filter_produtos',
											action: 'reset_combo'
										},
									]
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									margin: '0 5 0 0',
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboProdutos_Marcas',
											name: 'fabricante_marca',
											id: 'fabricante_marca_filter_produtos',
											button_id: 'button_fabricante_marca_filter_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Marca'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_fabricante_marca_filter_produtos',
											combo_id: 'fabricante_marca_filter_produtos',
											action: 'reset_combo'
										},
									]
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboUnidades_Medidas',
											name: 'unidade_medida',
											id: 'unidade_medida_filter_produtos',
											button_id: 'button_unidade_medida_filter_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Unidade Medida'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_unidade_medida_filter_produtos',
											combo_id: 'unidade_medida_filter_produtos',
											action: 'reset_combo'
										},
									]
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'ncm',
									margin: '0 5 0 0',
									flex: 1,
									id: 'ncm_filter_produtos',
									allowBlank: false,
									anchor: '100%',
									fieldLabel: 'NCM'
								},
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboAtivoProdutos',
											name: 'ativo',
											loadDisabled: true,
											id: 'ativo_filter_produtos',
											button_id: 'button_ativo_filter_produtos',
											flex: 1,
											anchor: '100%',
											fieldLabel: 'Ativo'
										},
										{
											xtype: 'buttonadd',
											iconCls: 'bt_cancel',
											hidden: true,
											id: 'button_ativo_filter_produtos',
											combo_id: 'ativo_filter_produtos',
											action: 'reset_combo'
										}
									]
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [

								{
									xtype: 'fieldcontainer',
									anchor: '100%',
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									margin: '0 5 0 0',
									flex: 1,
									labelAlign: 'top',
									labelStyle: 'font-weight: bold;font-size: 11px;',
									fieldLabel: 'Data Cadastro',
									items: [
										{
											xtype: 'datefield',
											format: 'd/m/Y',
											flex: 1,
											id: 'data_cadastro_date_filter_produtos',
											name: 'data_cadastro_date',
											margins: '0 5 0 0',

											hideLabel: true
										},
										{
											xtype: 'textfield',
											mask: '99:99:99',
											plugins: 'textmask',
											returnWithMask: true,
											flex: 1,
											id: 'data_cadastro_time_filter_produtos',
											name: 'data_cadastro_time',

											hideLabel: true
										}
									]
								},
								{
									xtype: 'numberfield',
									name: 'cadastrado_por',
									flex: 1,
									id: 'cadastrado_por_filter_produtos',
									anchor: '100%',
									fieldLabel: 'Cadastrado Por'
								}

							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'numberfield',
									name: 'alterado_por',
									flex: 1,
									id: 'alterado_por_filter_produtos',
									anchor: '100%',
									fieldLabel: 'Alterado Por'
								}

							]
						},
						{
							xtype: 'hidden',
							name: 'action',
							hidden: true,
							id: 'action_filter_produtos',
							allowBlank: false,
							value: 'FILTER',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							action: 'resetar_filtro',
							text: 'Resetar Filtro'
						},
						{
							xtype: 'button',
							iconCls: 'bt_lupa',
							action: 'filtrar_busca',
							text: 'Filtrar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});