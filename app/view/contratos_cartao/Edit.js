/**
*	@Autor: Maciel Sousa
*	@Email: macielcr7@gmail.com
**/

Ext.define('ShSolutions.view.contratos_cartao.Edit', {
	extend: 'Ext.window.Window',
	alias: 'widget.addcontratos_cartaowin',

	id: 'AddContratos_CartaoWin',
	layout: {
		type: 'fit'
	},
	maximizable: false,
	minimizable: true,
	width: 780,
	title: 'Cadastro de Contratos de Cartão',

	initComponent: function() {
		var me = this;


		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id: 'FormContratos_Cartao',
					bodyPadding: 10,
					autoScroll: true,
					method: 'POST',
					url : 'server/modulos/contratos_cartao/save.php',
					items: [
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContratosContaBancaria',
									name: 'conta_bancaria',
									allowBlank: false,
									margin: '0 5 0 0',
									id: 'conta_bancaria_contratos_cartao',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Conta Bancária',
									listeners: {
										afterrender: function(fld) {
											fld.focus(false, 1000);
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'descricao',
									allowBlank: false,
									margin: '0 5 0 0',
									flex: 1,
									id: 'descricao_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									anchor: '100%',
									fieldLabel: 'Descrição'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'combobox',
									store: 'StoreComboContratosCartaoTipo',
									name: 'tipo',
									allowBlank: false,
									id: 'tipo_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									value: 'C',
									anchor: '100%',
									margin: '0 5 0 0',
									fieldLabel: 'Tipo'
								},
								{
									xtype: 'textfield',
									name: 'nro_max_parcelas',
									maskRe: /[0-9.]/,
									maxLength: 2,
									enforceMaxLength: true,
									allowBlank: false,
									margin: '0 5 0 0',
									id: 'nro_max_parcelas_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Nº Máximo Parcelas'
								},
								{
									xtype: 'textfield',
									name: 'dias_credito_parc_vista',
									maskRe: /[0-9.]/,
									allowBlank: false,
									margin: '0 5 0 0',
									maxLength: 2,
									enforceMaxLength: true,
									id: 'dias_credito_parc_vista_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Dias para Crédito (1ª Parcela)'
								},
								{
									xtype: 'textfield',
									name: 'dias_credito_parc_estab',
									maskRe: /[0-9.]/,
									allowBlank: false,
									margin: '0 5 0 0',
									maxLength: 2,
									enforceMaxLength: true,
									id: 'dias_credito_parc_estab_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									fieldLabel: 'Dias para Crédito (Parc. Loja)'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'taxa1',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa1_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 1 Parcela:'
								},
								{
									xtype: 'textfield',
									name: 'taxa2',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa2_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 2 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa3',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa3_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 3 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa4',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa4_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 4 Parcelas:'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'taxa5',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa5_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 5 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa6',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa6_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 6 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa7',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa7_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 7 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa8',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa8_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 8 Parcelas:'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'taxa9',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa9_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 9 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa10',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa10_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 10 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa11',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa11_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 11 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa12',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa12_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 12 Parcelas:'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'taxa13',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa13_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 13 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa14',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa14_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 14 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa15',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa15_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 15 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa16',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa16_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 16 Parcelas:'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'taxa17',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa17_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 17 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa18',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa18_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 18 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa19',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa19_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 19 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa20',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa20_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 20 Parcelas:'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							layout: {
								type: 'hbox'
							},
							items: [
								{
									xtype: 'textfield',
									name: 'taxa21',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa21_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 21 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa22',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa22_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 22 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa23',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa23_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 23 Parcelas:'
								},
								{
									xtype: 'textfield',
									name: 'taxa24',
									allowBlank: true,
									margin: '0 5 0 0',
									id: 'taxa24_contratos_cartao',
									fieldStyle: 'text-transform:uppercase',
									flex: 1,
									anchor: '100%',
									mask: '#9.999.990,00',
									money: true,
									returnWithMask: true,
									plugins: 'textmask',
									fieldLabel: 'Taxa quando 24 Parcelas:'
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							autoHeight: true,
							anchor: '50%',
							margins: '0 5 0 0',
							layout: {
								align: 'stretch',
								type: 'hbox'
							},
							items: [
								{
									xtype: 'fieldcontainer',
									autoHeight: true,
									flex: 1,
									layout: {
										align: 'stretch',
										type: 'hbox'
									},
									items: [
										{
											xtype: 'combobox',
											store: 'StoreComboContratosCartaoTipoCalculo',
											name: 'tipo_calculo',
											id: 'tipo_calculo_contratos_cartao',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											anchor: '100%',
											margin: '0 5 0 0',
											fieldLabel: 'Tipo Cálculo Parcelas'
										},
										{
											xtype: 'combobox',
											store: 'StoreComboAtivo',
											name: 'ativo',
											id: 'ativo_contratos_cartao',
											fieldStyle: 'text-transform:uppercase',
											flex: 1,
											value: 'S',
											anchor: '100%',
											margin: '0 5 0 0',
											fieldLabel: 'Ativo'
										}
									]
								},
								{
									xtype: 'textfield',
									name: 'id',
									hidden: true,
									margin: '0 5 0 0',
									id: 'id_contratos_cartao',
									value: 0,
									anchor: '100%',
									fieldLabel: 'id'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'action',
							hidden: true,
							margin: '0 5 0 0',
							id: 'action_contratos_cartao',
							value: 'INSERIR',
							anchor: '100%'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							id: 'button_resetar_contratos_cartao',
							iconCls: 'bt_cancel',
							handler: function() {
								me.close(this);
							},
							text: 'Cancelar/Fechar'
						},
						{
							xtype: 'button',
							id: 'button_salvar_contratos_cartao',
							iconCls: 'bt_save',
							action: 'salvar',
							text: 'Salvar'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	}
});