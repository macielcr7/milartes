Ext.define('ShSolutions.view.Login', {
	extend: 'Ext.window.Window',
	alias: 'widget.addloginwin',

	modal: true,
	draggable: false,
	closable: false,
	resizable: false,
	id: 'AddLoginWin',
	width: 400,
	height: 350,
	layout: {
		type: 'border'
	},
	title: 'Login do Sistema',

	initComponent: function() {
		var me = this;

		Ext.applyIf(me, {
			items: [
				{
					xtype: 'panel',
					border: false,
					id: 'panel_img',
					region: 'north',
					html: '<div style="text-align:center;padding:3px"><img src="resources/images/logoLogin.png"/></div>',
					height: 100
				},
				{
					xtype: 'form',
					border: false,
					autoScroll: true,
					region: 'center',
					id: 'FormLogin',
					bodyPadding: 10,
					method: 'POST',
					url: 'server/modulos/login.php',
					items: [
						{
							xtype: 'textfield',
							id: 'LoginLogin',
							name: 'login',
							fieldLabel: 'Login',
							allowBlank: false,
							labelWidth: 60,
							anchor: '100%',
							listeners: {
								afterrender: function(fld) {
									fld.focus(false, 500);
								}
							}
						},
						{
							xtype: 'textfield',
							id: 'SenhaLogin',
							inputType: 'password',
							name: 'senha',
							fieldLabel: 'Senha',
							labelWidth: 60,
							allowBlank: false,
							anchor: '100%',
							enableKeyEvents: true,
							listeners: {
								keypress : function(textfield,eventObject){
									if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
										me.LoginSys();
									}
								}
							}
						},
						{
							xtype: 'combobox',
							store: 'StoreComboUsuarios_Empresas_Sistema',
							name: 'empresa_sistema',
							loadDisabled: true,
							disabled: true,
							hidden: true,
							id: 'empresa_sistemaLogin',
							anchor: '100%',
							fieldLabel: 'Empresa',
							listeners: {
								change: {
									fn: function(combo){
										Ext.util.Cookies.set('id_empresa_atual', combo.getValue());
										Ext.util.Cookies.set('descricao_empresa_atual', combo.getRawValue());
										Ext.util.Cookies.set('cnpj_empresa_atual', combo.store.getById(combo.getValue()));
										window.location = 'index.php'; 
									}
								}
							}
						},
						{
							xtype: 'hiddenfield',
							hidden: true,
							name: 'action',
							id: 'action_login',
							value: 'LOGIN'
						}
					]
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'bt_cancel',
							text: 'Limpar Campos',
							handler: function(){
								Ext.getCmp('FormLogin').getForm().reset();
							}
						},
						{
							xtype: 'tbseparator'
						},
						{
							xtype: 'button',
							iconCls: 'bt_login',
							text: 'Conectar',
							listeners: {
								click: {
									fn: me.LoginSys,
									scope: me
								}
							}
						}
					]
				}
			]
		});
		me.callParent(arguments);
	},

	LoginSys: function(button, e, options) {
		if(!Ext.getCmp('FormLogin').getForm().isValid()){
			return true;
		}
		Ext.getCmp('FormLogin').getEl().mask('Aguarde...');
		Ext.getCmp('FormLogin').getForm().submit({
			success: function(f, o){
				if(o.result.success===true){
					if(o.result.empresas.length>1){
						Ext.getCmp('empresa_sistemaLogin').store.loadData(o.result.empresas);
						Ext.getCmp('empresa_sistemaLogin').setDisabled(false);
						Ext.getCmp('empresa_sistemaLogin').setVisible(true);
						Ext.getCmp('FormLogin').getEl().unmask();
						Ext.getCmp('empresa_sistemaLogin').focus(false, 100);
					}
					else{
						Ext.util.Cookies.set('id_empresa_atual', o.result.empresas[0].id);
						Ext.util.Cookies.set('descricao_empresa_atual', o.result.empresas[0].descricao);
						window.location = 'index.php';
					}
				}
				else{
					Ext.Msg.alert('Erro!', o.result.msg);
					Ext.getCmp('FormLogin').getEl().unmask();
				}
			},
			failure: function(f, o){
				console.info(o);
				Ext.Msg.alert('Erro!', o.result.msg);
				Ext.getCmp('FormLogin').getEl().unmask();
			}
		});
	}
});