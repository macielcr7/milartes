<?php
	@session_start();
	header("Content-Type: text/html; charset=utf-8");

	$versao = '2.0.1';
	$build  = '(build 20130930)';
	$sistema = "Mil Artes";
	$user = null;
	if(!isset($_SESSION['SESSION_USUARIO'])){
		header("Location: login.php");
	}
	else{
		$model = json_encode(unserialize($_SESSION['MODEL_PERMISSOES']));
		$user = unserialize($_SESSION['SESSION_USUARIO']);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $sistema." ".$versao." ".$build; ?></title>
	<link rel="stylesheet" type="text/css" href="ext/resources/css/ext-all.css"/>
	<link rel="stylesheet" type="text/css" href="resources/css/desktop.css"/>
	<link rel="stylesheet" type="text/css" href="resources/css/icons.css"/>
	<link rel="stylesheet" type="text/css" href="resources/css/alert.css"/>
	<link rel="stylesheet" type="text/css" href="resources/css/extensible-all.css"/>
	<style>
		.x-calendar-list, .x-calendar-list .x-panel-body{
			height:100%!important;
		}
		.ext-cal-picker-mainicon{
			top:16px;
		}
	</style>
</head>
<body>
	<div id="loading-mask" style=""></div>
	<div id="loading">
		<div id='loading-indicator' class="loading-indicator">
			<img src="resources/images/loading.gif" style="margin-right:8px;float:left;vertical-align:top;"/>
			<div id="txt-indicator" style=" padding-top:30px;">
				<?php echo $sistema." ".$versao." ".$build; ?><br>
				<span id="loading-msg">Carregando Estilos e Imagens, Aguarde...</span>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		document.getElementById('loading-msg').innerHTML = 'Carregando Framework, Aguarde...';
		var NameApp = 'ShSolutions';
		var TITULO_SYSTEM = '<?php echo $sistema; ?>';
		var key = <?php echo $model; ?>;
		var userID = '<?php echo $user['id'];?>';
		var userName = '<?php echo $user['nome'];?>';
		var webmailEmail = '<?php echo $user['webmail_email'];?>';
		var webmailSenha = '<?php echo $user['webmail_senha'];?>';
		var NroCaixaDesejado = '0';
		var NroCaixaDesejado2 = '0';
		var userData = <?php echo json_encode($user, true);?>;
		window.empresa_sistemaAll = <?php echo json_encode($user['empresas']); ?>;
	</script>
	<script type="text/javascript" src="ext/ext-all.js"></script>
	<script type="text/javascript" src="ext/extensible-all.js"></script>
	<script type="text/javascript" src="ext/RowExpander.js"></script>
	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Carregando Tradutor, Aguarde...';</script>
	<script type="text/javascript" src="ext/locale/ext-lang-pt_BR.js"></script>
	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Carregando Plugins, Aguarde...';</script>

	<script type="text/javascript" src="resources/plugins/Notification.js"></script>
	<script type="text/javascript" src="resources/plugins/Alert.js"></script>
	<script type="text/javascript" src="resources/plugins/TextMask.js"></script>
	<script type="text/javascript" src="resources/plugins/functions.js"></script>
	<script type="text/javascript" src="resources/plugins/jquery.js"></script>
	<script type="text/javascript" src="resources/plugins/ext/locale/extensible-lang-pt_BR.js"></script>

	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Carregando M&oacute;dulos, Aguarde...';</script>

	<script type="text/javascript" src='app.js'></script>
	<script>
	$(document).unbind('keydown').bind('keydown', function (event) {
		var doPrevent = false;
		if (event.keyCode === 8) {
			var d = event.srcElement || event.target;
			if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE')) 
				 || d.tagName.toUpperCase() === 'TEXTAREA') {
				doPrevent = d.readOnly || d.disabled;
			}
			else {
				doPrevent = true;
			}
		}

		if (doPrevent) {
			event.preventDefault();
		}
	});
	</script>   
</body>
</html>
