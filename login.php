<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Login - Sistema</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<noscript>
  <meta http-equiv="Refresh" content="1; url=javascript.html">
</noscript>
<link rel="stylesheet" type="text/css" href="ext/resources/css/ext-all.css"/>	
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
<script type="text/javascript" src="ext/ext-all.js"></script>
<script type="text/javascript" src="ext/locale/ext-lang-pt_BR.js"></script>
<script type="text/javascript" src="resources/plugins/functions.js"></script>

<script type="text/javascript">
	Ext.Loader.setConfig({
		enabled: true,
		paths: {
			'ShSolutions': 'app',
			'ShSolutions.plugins': 'resources/plugins'
		}
	});

	Ext.require([
		'ShSolutions.store.StoreComboUsuarios_Empresas_Sistema',
		'ShSolutions.view.Login'
	]);
	
	Ext.onReady(function(){
		Ext.application({
		    name: 'ShSolutions',
		    views: ['Login'],
		    models: ['ModelCombo'],
		    stores: ['StoreComboUsuarios_Empresas_Sistema'],
		    autoCreateViewport: false,
		    launch: function() {
		    	var me = this;
		    	Ext.create('ShSolutions.view.Login').show();
		    }

		});

  	});
</script>
</head>
<body>
</body>
</html>
