				<div id="banner-wrapper">
					<div id="banner" class="box container">
						<div class="row">
							<div class="7u 12u(medium)">
								<h2>Forro de PVC Itajaí e Camboriú</h2>
								<p>Várias Cores e Modelos para deixar seusambientes mais aconchegantes</p>
							</div>
							<div class="5u 12u(medium)">
								<ul>
									<a href="tel:04732464706"><li class="button big icon">Itajaí: 3246-4706</li></a>
									<a href="tel:04733650706"><li class="button alt big icon">Camboriú: 3365-0706</li></a>
								</ul>
							</div>
						</div>
					</div>
				</div>