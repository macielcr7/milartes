<?php 
$TituloPagina = "Sancas em EVA";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
							<h2>Sancas em E.V.A</h2><br><br>
							<p>Indicado para paredes Curvas.<br>
								Acompanha as deformidades da parede.<br>
								Vida útil ilimitada.<br>
								Ótima qualidade.<br>Produto Ecológico.
								Pode ser Fábricado em Diversas cores.<br>
								É dispensado o uso de pregos e parafusos. Praticidade na Instalação!<br>
								Matéria Prima: E.V.A vúlgo (Borracha)
							</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/SA20.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>SA 20 - BRANCA</h2>
												<p>ALTURA: 8 CM<br>
													ESPESSURA: 15MM (1,5 CM)<br>
													COMPRIMENTO: 3 M</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/SA22.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>SA 22 - BRANCA</h2>
												<p>ALTURA: 5 CM<br>
													ESPESSURA: 15MM (1,5 CM)<br>
													COMPRIMENTO: 3 M</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/SA23.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>SA 23 - BRANCA</h2>
												<p>ALTURA: 10 CM<br>
													ESPESSURA: 15MM (1,5 CM)<br>
													COMPRIMENTO: 3 M</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/SA25.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>SA 25 - BRANCA</h2>
												<p>ALTURA: 7 CM<br>
													ESPESSURA: 15MM (1,5 CM)<br>
													COMPRIMENTO: 3 M</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/SA26.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>SA 26 - BRANCA</h2>
												<p>ALTURA: 14 CM<br>
													ESPESSURA: 15MM (1,5 CM)<br>
													COMPRIMENTO: 3 M</p>
											</header>
										</div>
									</section>

							</div>

						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>