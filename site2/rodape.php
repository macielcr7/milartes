				<div id="footer-wrapper">
					<footer id="footer" class="container">
						<div class="row">
							<div class="3u 6u(medium) 12u$(small)">
								<!-- Links -->
									<section class="widget links">
										<h3>Loja Itajaí</h3>
										<p>Rua Ver. Telêmaco de Oliviera, 65<br />
										Cordeiros - Itajaí - SC<br />
										<a href=" https://api.whatsapp.com/send?phone=554732464706">(47) 3246-4706<img align="bottom" src="images/whatsapp-logo-1.png" alt="" /></a></p>
									</section>

							</div>
							<div class="3u 6u$(medium) 12u$(small)">
								<!-- Links -->
									<section class="widget links">
										<h3>Loja Camboriú</h3>
										<p>Av. Santa Cataria, 507<br />
										Centro - Camboriú - SC<br />
										<a href=" https://api.whatsapp.com/send?phone=554733650706">(47) 3365-0706<img align="bottom" src="images/whatsapp-logo-1.png" alt="" /></a></p>
									</section>

							</div>
							<div class="3u 6u$(medium) 12u$(small)">
								<!-- Contact -->
									<section class="widget contact last">
										<h3>Contact Us</h3>
										<ul>
											<li><a href="https://www.google.com/maps/search/mil+artes+decoracoes+itajai+e+camboriu" class="icon fa-map-marker" target="_new"><span class="label">Google Maps</span></a></li>
											<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
											<!-- 
											<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
											<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
											<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>-->
										</ul>
									</section>
							</div>
						</div>
						<div class="row">
							<div class="12u">
								<div id="copyright">
									<ul class="menu">
										<li>&copy; Todos os Direitos Reservados</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
				</div>
			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>