<?php 
$TituloPagina = "Rodapés em Poliestireno";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
								<h2>Rodapé de Poliestireno</h2>
								<p>O rodapé poliestireno tem a principal função de proteger o canto da parede que fica rente ao piso - dando o acabamento ideal para a casa. Reforçado, ele é bem simples, leve e resistente à umidade, o que facilita na hora da limpeza: você pode lavar o chão com água e produtos, sem problemas!</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP100.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 100 - Rodapé Branco com Friso</h2>
												<p>ALTURA: 50 MM (5 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP101.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 101 - Rodapé Branco com Friso</h2>
												<p>ALTURA: 70 MM (7 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP102.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 102 - Rodapé Branco Liso</h2>
												<p>ALTURA: 70 MM (7 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP103.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 103 - Rodapé Branco com Friso</h2>
												<p>ALTURA: 100 MM (10 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP104.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 104 - Rodapé Branco com Friso</h2>
												<p>ALTURA: 150 MM (15 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP105.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 105 - Rodapé Branco Liso</h2>
												<p>ALTURA: 50 MM (5 CM)<br>
													ESPESSURA: 13 MM (1,3 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>


				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP106.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 106 - Rodapé Branco Liso</h2>
												<p>ALTURA: 70 MM (7 CM)<br>
													ESPESSURA: 13 MM (1,3 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RP107.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RP/BR 104 - Rodapé Branco Liso</h2>
												<p>ALTURA: 100 MM (10 CM)<br>
													ESPESSURA: 13 MM (1,3 CM) <br>
													COMPRIMENTO: 2,40 METROS</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>