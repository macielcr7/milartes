<?php 
$TituloPagina = "Forro PVC";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
								<h2>Forro PVC Tradicional</h2>
								<p>O Forro PVC Modelo Tradicional é indicado para ambientes internos, sacadas e abas de construções residenciais, comerciais, industriais e para utilização em áreas amplas proporcionando segurança, economia, praticidade de instalação e manutenção, deixando o ambiente moderno com sofisticação e requinte.</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/gemini-branco.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Branco Gelo</h2>
												<p>
													R$ 10,90 m² (pagamento em dinheiro)<br>
													R$ 12,90 m² (cartão de débito ou crédito)<br><br>
													Forro Instalado a partir de R$ 25,00 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/gemini-cerejeira.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Cerejeira</h2>
												<p>
													R$ 16,11 m² (pagamento em dinheiro)<br>
													R$ 17,90 m² (cartão de débito ou crédito)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/gemini-jatoba.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Jatobá</h2>
												<p>
													R$ 16,11 m² (pagamento em dinheiro)<br>
													R$ 17,90 m² (cartão de débito ou crédito)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>