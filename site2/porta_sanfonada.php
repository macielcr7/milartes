<?php 
$TituloPagina = "Portas Sanfonadas";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
							<h2>Portas Sanfonadas</h2><br><br>
							<p>
								Fabricada com perfis de PVC mais sólidos e compactos a Plast Porta possui maior flexibilidade, resistência e durabilidade e sua formulação exclusiva assegura maior fixação e uniformidade das cores e menor grau de transparência em suas lâminas, atendendo também as exigências do mercado internacional.<br>
								Além do ganho de espaço a Plast Porta é fácil de limpar e sua instalação é muito simples. Seu deslizamento suave e silencioso e a facilidade de manuseio são garantidos pelos pinos de sustentação produzidos com material antiaderente e com baixo nível de atrito. 
							</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/porta_sanf_branca.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>PORTA SANFORNADA - BRANCA</h2>
												<p>ALTURA: 2,10 M<br>
													LARGURA: DE 60 CM ATÉ 252 CM<br>
													COM FECHADURA</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/porta_sanf_imbuia.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>PORTA SANFONADA - IMBÚIA</h2>
												<p>ALTURA: 2,10 M<br>
													LARGURA: DE 60 CM ATÉ 252 CM<br>
													COM FECHADURA</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/porta_sanf_cerejeira.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>PORTA SANFONADA - CEREJEIRA</h2>
												<p>ALTURA: 2,10 M<br>
													LARGURA: DE 60 CM ATÉ 252 CM<br>
													COM FECHADURA</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/porta_sanf_mogno.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>PORTA SANFONADA - MÓGNO</h2>
												<p>ALTURA: 2,10 M<br>
													LARGURA: DE 60 CM ATÉ 252 CM<br>
													COM FECHADURA</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/porta_sanf_cinza.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>PORTA SANFONADA - CINZA</h2>
												<p>ALTURA: 2,10 M<br>
													LARGURA: DE 60 CM ATÉ 252 CM<br>
													COM FECHADURA</p>
											</header>
										</div>
									</section>

							</div>

						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>