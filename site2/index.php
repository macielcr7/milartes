<?php 
$TituloPagina = "Forro PVC Itajaí e Camboriú";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<?php include('banner.php'); ?>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="forro_gemini.php" class="image featured"><img src="images/forro002.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Forro PVC Tradicional</h2>
												<p>Tradicional</p>
											</header>
											<p>O Forro PVC Modelo Tradicional é indicado para ambientes internos, sacadas e abas de construções residenciais, comerciais, industriais e para utilização em áreas amplas proporcionando segurança, economia, praticidade de instalação e manutenção, deixando o ambiente moderno com sofisticação e requinte.</p>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="forro_laminado.php" class="image featured"><img src="images/laminados.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Forro Painel Laminado</h2>
												<p>Elegância</p>
											</header>
											<p>Painéis em PVC que podem ser usados como painéis decorativos nas paredes e forro, renovando ambientes<br></p>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="forro_laminado_lambrie.php" class="image featured"><img src="images/forro004.png" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Forro Laminado Lambri</h2>
												<p>Elegância</p>
											</header>
											<p>Cria áreas decorativas elegantes e com muito mais personalidade;
											Dispensa manutenção e pintura<br>
											Fácil de lavar<br>
											Excelente estética</p>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em Breve...')"><img src="images/forro005.png" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Foro REVID</h2>
												<p>A Evolução dos Revestimentos de Policloreto de Vinila</p>
											</header>
											<p>Revestimentos que aliam Cuidado Artesanal à Tecnologia de Ponta</p>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em Breve...')"><img src="images/forro006.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Forro Relevo</h2>
												<p>Alto Relevo</p>
											</header>
											<p>Com os forros relevo de PVC você transforma ambientes em espaços elegantes e aconchegantes. Com um relevo exclusivo, trazemos até você elegância aliada a versatilidade do PVC</p>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/forro007.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Forros Texturizados</h2>
												<p>Elegantes e Aconchegantes</p>
											</header>
											<p>	Com os forros texturizados de PVC você transforma ambientes em espaços elegantes e aconchegantes. Disponíveis em diversas cores, trazemos até você a elegância da madeira e a versatilidade do PVC</p>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>
<?php include('rodape.php'); ?>