<?php 
$TituloPagina = "Rodapés em EVA";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
							<h2>Rodapé de E.V.A</h2><br><br>
							<p>Indicado para paredes Curvas.<br>
								Acompanha as deformidades da parede.<br>
								Vida útil ilimitada.<br>
								Ótima qualidade.<br>Produto Ecológico.
								Pode ser Fábricado em Diversas cores.<br>
								É dispensado o uso de pregos e parafusos. Praticidade na Instalação!<br>
								Matéria Prima: E.V.A vúlgo (Borracha)
							</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO01.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 01 - QUADRADO</h2>
												<p>ALTURA: 80 MM (8 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: BRANCO</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO01.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 02 - QUADRADO</h2>
												<p>ALTURA: 100 MM (10 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: BRANCO</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO04.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 03 - COM NEGATIVO</h2>
												<p>ALTURA: 100 MM (10 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: BRANCO</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO04.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 04 - COM NEGATIVO</h2>
												<p>ALTURA: 140 MM (14 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: BRANCO</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO05.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 05</h2>
												<p>ALTURA: 140 MM (14 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: BRANCO</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO10.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 10</h2>
												<p>ALTURA: 60 MM (6 CM)<br>
													ESPESSURA: 9 MM (0,9 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: MARFIM MADEIRA</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>


				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO13.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 13</h2>
												<p>ALTURA: 80 MM (8 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: PÁTINA CINZA</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO14.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 14</h2>
												<p>ALTURA: 80 MM (8 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: PÁTINA MARROM</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO17.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 17</h2>
												<p>ALTURA: 60 MM (6 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: MARFIM LISO</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO18.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 18</h2>
												<p>ALTURA: 50 MM (5 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: MÓGNO</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO19.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 19</h2>
												<p>ALTURA: 70 MM (7 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: CEREJEIRA</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/RO20.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>RO 20</h2>
												<p>ALTURA: 100 MM (10 CM)<br>
													ESPESSURA: 15 MM (1,5 CM) <br>
													COMPRIMENTO: 2,50 METROS<br>
													COR DA IMAGEM: PATINA BEGE (COR NÃO DISPONÍVEL)</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>