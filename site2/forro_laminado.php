<?php 
$TituloPagina = "Forro PVC - Laminado";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
								<h2>Forro Painel Laminado</h2>
								<p>Painéis em PVC que podem ser usados como painéis decorativos nas paredes e forro, renovando ambientes.</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-imbuia.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro Imbuia</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-carvalho-maple.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro Carvalho Maple</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-york.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro York</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-canela.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro Canela</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-cedro.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro Cedro</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-champagne.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro Champagne</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-master-angelim.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Plasforro Angelim</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>