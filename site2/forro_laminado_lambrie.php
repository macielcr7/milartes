<?php 
$TituloPagina = "Forro PVC - Laminado Lambri";
include('cabecalho.php'); ?>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<!--
							<div id="logo">
								<h1><a href="index.html">Verti</a></h1>
								<span>by HTML5 UP</span>
							</div>
							-->

						<!-- Nav -->
							<?php include('menu.php'); ?>
					</header>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div id="banner2" class="box container">
						<div class="row">
								<h2>Forro Painel Laminado</h2>
								<p>Cria áreas decorativas elegantes e com muito mais personalidade<br>
								Dispensa manutenção e pintura</p>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-imbuia.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>Imbuia</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-carvalho-maple.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>Carvalho Maple</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-york.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>York</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-canela.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>Canela</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-cedro.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>Cedro</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-champagne.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>Champagne</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured" onClick="alert('Em breve mais fotos...')"><img src="images/painel-laminado-lambri-angelim.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Laminado Lambrie Plasforro<br>Angelim</h2>
												<p>
													R$ 49,90 m² (pagamento em dinheiro)
												</p>
											</header>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

<?php include('rodape.php'); ?>